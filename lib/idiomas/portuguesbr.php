<?
/**
* Idiomas e tradu&ccedil;&atilde;o das tabelas do banco de dados
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  engine
*/

$ling['idioma']="Portugues - Brasil";
$ling['xml']="pt-br";

// ------------------------------------------------------------------------
//                    DEFINI&Ccedil;&Otilde;ES DAS TABELAS DO SISTEMA
// ------------------------------------------------------------------------
$tdb=array();

define("AFASTAMENTOS", "afastamentos" );
$tdb[AFASTAMENTOS]['DESC']="Afastamentos";
$tdb[AFASTAMENTOS]['dba']=0;
$tdb[AFASTAMENTOS]['MID_FUNCIONARIO']="Funcion&aacute;rio";
$tdb[AFASTAMENTOS]['DATA_INICIAL']="Data Sa&iacute;da";
$tdb[AFASTAMENTOS]['DATA_FINAL']="Data Retorno";
$tdb[AFASTAMENTOS]['MOTIVO']="Motivo";

define("AREAS", "areas" );
$tdb[AREAS]['DESC']="Localiza&ccedil;&atilde;o 1";
$tdb[AREAS]['dba']=0;
$tdb[AREAS]['COD']="Tag";
$tdb[AREAS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[AREAS]['MID_EMPRESA']="Empresa";

define("ATIVIDADES", 'atividades');
$tdb[ATIVIDADES]['DESC']="Atividades";
$tdb[ATIVIDADES]['DESC_CAMPO']="NUMERO";
$tdb[ATIVIDADES]['dba']=0;
$tdb[ATIVIDADES]['MID_PLANO_PADRAO']="Plano Padr&atilde;o";
$tdb[ATIVIDADES]['TAREFA']="Tarefa";
$tdb[ATIVIDADES]['PARTE']="Parte a Verificar";
$tdb[ATIVIDADES]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[ATIVIDADES]['INSTRUCAO_DE_TRABALHO']="Instru&ccedil;&atilde;o de Trabalho";
$tdb[ATIVIDADES]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[ATIVIDADES]['TEMPO_PREVISTO']="Tempo Previsto";
$tdb[ATIVIDADES]['QUANTIDADE_MO']="Quantidade de Funcion&aacute;rios";
$tdb[ATIVIDADES]['ESPECIALIDADE']="Especialidade";
$tdb[ATIVIDADES]['NUMERO']="N&ordm;";
$tdb[ATIVIDADES]['PERIODICIDADE']="Periodicidade";
$tdb[ATIVIDADES]['FREQUENCIA']="Frequencia";
$tdb[ATIVIDADES]['CONTADOR']="Contador";
$tdb[ATIVIDADES]['DISPARO']="Disparo";
$tdb[ATIVIDADES]['MID_MATERIAL']="Material";
$tdb[ATIVIDADES]['QUANTIDADE']="Quantidade";
$tdb[ATIVIDADES]['MAQUINA_PARADA']="Exige Parada?";


define("AUSENCIAS", "ausencia");
$tdb[AUSENCIAS]['DESC']="Aus&ecirc;ncias";
$tdb[AUSENCIAS]['dba']=0;
$tdb[AUSENCIAS]['MID_FUNCIONARIO']="Funcion&aacute;rio";
$tdb[AUSENCIAS]['HORA_INICIAL']="Hor&aacute;rio Sa&iacute;da";
$tdb[AUSENCIAS]['HORA_FINAL']="Hor&aacute;rio Retorno";
$tdb[AUSENCIAS]['DATA']="Data";
$tdb[AUSENCIAS]['MOTIVO']="Motivo";

define("CAUSA", "causa");
$tdb[CAUSA]['DESC']="Causa";
$tdb[CAUSA]['dba']=0;
$tdb[CAUSA]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[CAUSA]['TEXTO']="Texto";
$tdb[CAUSA]['MID_EMPRESA']="Empresa";

define("CENTRO_DE_CUSTO", "centro_de_custo");
$tdb[CENTRO_DE_CUSTO]['DESC']="Centro de Custo";
$tdb[CENTRO_DE_CUSTO]['dba']=0;
$tdb[CENTRO_DE_CUSTO]['MID_EMPRESA']="Empresa";
$tdb[CENTRO_DE_CUSTO]['COD']="C&oacute;digo";
$tdb[CENTRO_DE_CUSTO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[CENTRO_DE_CUSTO]['PAI']="Pai";

define("CONTROLE_CONTADOR", "controle_contador");
$tdb[CONTROLE_CONTADOR]['DESC']="Controle de Disparos por Contador";
$tdb[CONTROLE_CONTADOR]['dba']=0;
$tdb[CONTROLE_CONTADOR]['MID_PLANO']="Plano";
$tdb[CONTROLE_CONTADOR]['TIPO']="Tipo";
$tdb[CONTROLE_CONTADOR]['DATA']="Data";
$tdb[CONTROLE_CONTADOR]['VALOR']="Valor Atual";
$tdb[CONTROLE_CONTADOR]['DISPAROS']="Objeto de Manuten&ccedil;&atilde;o";

define("DEFEITO", "defeito");
$tdb[DEFEITO]['DESC']="Defeito";
$tdb[DEFEITO]['dba']=0;
$tdb[DEFEITO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[DEFEITO]['TEXTO']="Texto";
$tdb[DEFEITO]['MID_EMPRESA']="Empresa";

define("EMPRESAS", "empresas");
$tdb[EMPRESAS]['DESC']="Empresas";
$tdb[EMPRESAS]['dba']=0;
$tdb[EMPRESAS]['EMP_MATRIZ']="Matriz";
$tdb[EMPRESAS]['COD']="C&oacute;digo";
$tdb[EMPRESAS]['NOME']="Nome";
$tdb[EMPRESAS]['FANTASIA']="Fantasia";
$tdb[EMPRESAS]['CNPJ']="Cnpj";
$tdb[EMPRESAS]['IE']="Inscri&ccedil;&atilde;o Estadual";
$tdb[EMPRESAS]['ENDERECO']="Endere&ccedil;o";
$tdb[EMPRESAS]['NUMERO']="N&uacute;mero";
$tdb[EMPRESAS]['COMPLEMENTO']="Complemento";
$tdb[EMPRESAS]['BAIRRO']="Bairro";
$tdb[EMPRESAS]['CEP']="Cep";
$tdb[EMPRESAS]['CIDADE']="Cidade";
$tdb[EMPRESAS]['UF']="UF";
$tdb[EMPRESAS]['PAIS']="Pais";
$tdb[EMPRESAS]['TELEFONE_1']="Telefone 1";
$tdb[EMPRESAS]['TELEFONE_2']="Telefone 2";
$tdb[EMPRESAS]['FAX']="Fax";
$tdb[EMPRESAS]['OBSERVACAO']="Observa&ccedil;&atilde;o";



// TEM QUE ARRUMAR NO BANCO
define("EQUIPAMENTOS", "equipamentos");
$tdb[EQUIPAMENTOS]['DESC']="Componente";
$tdb[EQUIPAMENTOS]['dba']=0;
$tdb[EQUIPAMENTOS]['COD']="C&oacute;digo";
$tdb[EQUIPAMENTOS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[EQUIPAMENTOS]['FAMILIA']="Fam&iacute;lia";
$tdb[EQUIPAMENTOS]['FICHA_TECNICA']="Dados Tecnicos";
$tdb[EQUIPAMENTOS]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[EQUIPAMENTOS]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[EQUIPAMENTOS]['MID_EMPRESA']="Empresa";
$tdb[EQUIPAMENTOS]['FABRICANTE']="Fabricante";
$tdb[EQUIPAMENTOS]['FORNECEDOR']="Fornecedor";
$tdb[EQUIPAMENTOS]['MARCA']="Marca";
$tdb[EQUIPAMENTOS]['MODELO']="Modelo";
$tdb[EQUIPAMENTOS]['NSERIE']="N&uacute;mero de S&eacute;rie";
$tdb[EQUIPAMENTOS]['POTENCIA']="Pot&ecirc;ncia";
$tdb[EQUIPAMENTOS]['PRESSAO']="Press&atilde;o";
$tdb[EQUIPAMENTOS]['CORRENTE']="Corrente";
$tdb[EQUIPAMENTOS]['TENSAO']="Tens&atilde;o";
$tdb[EQUIPAMENTOS]['VAZAO']="Vaz&atilde;o";
$tdb[EQUIPAMENTOS]['MID_STATUS']="Status";

define("EQUIPAMENTOS_FAMILIA", "equipamentos_familia");
$tdb[EQUIPAMENTOS_FAMILIA]['DESC']="Fam&iacute;lia de Componentes";
$tdb[EQUIPAMENTOS_FAMILIA]['dba']=0;
$tdb[EQUIPAMENTOS_FAMILIA]['COD']="C&oacute;digo";
$tdb[EQUIPAMENTOS_FAMILIA]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[EQUIPAMENTOS_FAMILIA]['MID_EMPRESA']="Empresa";

define("EQUIPAMENTOS_MATERIAL", "equipamentos_material");
$tdb[EQUIPAMENTOS_MATERIAL]['DESC']="Pe&ccedil;as do Componente";
$tdb[EQUIPAMENTOS_MATERIAL]['dba']=0;
$tdb[EQUIPAMENTOS_MATERIAL]['MID_MATERIAL']="Material";
$tdb[EQUIPAMENTOS_MATERIAL]['MID_EQUIPAMENTO']="Componente";
$tdb[EQUIPAMENTOS_MATERIAL]['QUANTIDADE']="Quantidade";
$tdb[EQUIPAMENTOS_MATERIAL]['MID_CLASSE']="Classifica&ccedil;&atilde;o";

define("EQUIPAMENTOS_STATUS", "equipamentos_status");
$tdb[EQUIPAMENTOS_STATUS]['DESC']="Status de Componentes";
$tdb[EQUIPAMENTOS_STATUS]['dba']=0;
$tdb[EQUIPAMENTOS_STATUS]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("EQUIPES", "equipes");
$tdb[EQUIPES]['DESC']="Equipes";
$tdb[EQUIPES]['dba']=0;
$tdb[EQUIPES]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[EQUIPES]['TIPO']="Complemento";
$tdb[EQUIPES]['FORNECEDOR']="Fornecedor";
$tdb[EQUIPES]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[EQUIPES]['MID_EMPRESA']="Empresa";

define("ESPECIALIDADES", "especialidades");
$tdb[ESPECIALIDADES]['DESC']="Especialidades";
$tdb[ESPECIALIDADES]['dba']=0;
$tdb[ESPECIALIDADES]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[ESPECIALIDADES]['MID_EMPRESA']="Empresas";


define("ESTOQUE_ENTRADA", "estoque_entrada");

$tdb[ESTOQUE_ENTRADA]['DESC']="Entrada de Materiais";
$tdb[ESTOQUE_ENTRADA]['dba']=0;
$tdb[ESTOQUE_ENTRADA]['MID_MATERIAL']="Material";
$tdb[ESTOQUE_ENTRADA]['CENTRO_DE_CUSTO']="Centro de Custo";
$tdb[ESTOQUE_ENTRADA]['DATA']="Data";
$tdb[ESTOQUE_ENTRADA]['HORA']="Hora";
$tdb[ESTOQUE_ENTRADA]['MOTIVO']="Motivo";
$tdb[ESTOQUE_ENTRADA]['DOCUMENTO']="Documento";
$tdb[ESTOQUE_ENTRADA]['QUANTIDADE']="Quantidade";
$tdb[ESTOQUE_ENTRADA]['VALOR_UNITARIO']="Valor Unit&aacute;rio";
$tdb[ESTOQUE_ENTRADA]['VALOR_TOTAL']="Valor Total";
$tdb[ESTOQUE_ENTRADA]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[ESTOQUE_ENTRADA]['MID_ALMOXARIFADO']="Almoxarifado";

define("ESTOQUE_SAIDA", "estoque_saida");
$tdb[ESTOQUE_SAIDA]['DESC']="Sa&iacute;da de Materiais";
$tdb[ESTOQUE_SAIDA]['dba']=0;
$tdb[ESTOQUE_SAIDA]['MID_MATERIAL']="Material";
$tdb[ESTOQUE_SAIDA]['CENTRO_DE_CUSTO']="Centro de Custo";
$tdb[ESTOQUE_SAIDA]['DATA']="Data";
$tdb[ESTOQUE_SAIDA]['HORA']="Hora";
$tdb[ESTOQUE_SAIDA]['MOTIVO']="Motivo";
$tdb[ESTOQUE_SAIDA]['QUANTIDADE']="Quantidade";
$tdb[ESTOQUE_SAIDA]['ORDEM']="Ordem";
$tdb[ESTOQUE_SAIDA]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[ESTOQUE_SAIDA]['MID_ALMOXARIFADO']="Almoxarifado";

define("FORNECEDORES","fornecedores");
$tdb[FORNECEDORES]['DESC']="Fornecedores";
$tdb[FORNECEDORES]['dba']=0;
$tdb[FORNECEDORES]['NOME']="Nome";
$tdb[FORNECEDORES]['FANTASIA']="Fantasia";
$tdb[FORNECEDORES]['CNPJ']="Cnpj";
$tdb[FORNECEDORES]['CGC']="Inscri&ccedil;&atilde;o Estadual";
$tdb[FORNECEDORES]['ENDERECO']="Endere&ccedil;o";
$tdb[FORNECEDORES]['NUMERO']="N&uacute;mero";
$tdb[FORNECEDORES]['COMPLEMENTO']="Complemento";
$tdb[FORNECEDORES]['BAIRRO']="Bairro";
$tdb[FORNECEDORES]['CEP']="Cep";
$tdb[FORNECEDORES]['CIDADE']="Cidade";
$tdb[FORNECEDORES]['UF']="UF";
$tdb[FORNECEDORES]['TELEFONE_1']="Telefone 1";
$tdb[FORNECEDORES]['TELEFONE_2']="Telefone 2";
$tdb[FORNECEDORES]['TELEFONE_3']="Telefone 3";
$tdb[FORNECEDORES]['FAX']="Fax";
$tdb[FORNECEDORES]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[FORNECEDORES]['SITE']="Site";
$tdb[FORNECEDORES]['E_MAIL']="E-mail";

define("FERIADOS", "feriados");
$tdb[FERIADOS]['DESC']="Dias n&atilde;o produtivos";
$tdb[FERIADOS]['dba']=0;
$tdb[FERIADOS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[FERIADOS]['DATA']="Data";

define("FABRICANTES","fabricantes");
$tdb[FABRICANTES]['DESC']="Fabricantes";
$tdb[FABRICANTES]['dba']=0;
$tdb[FABRICANTES]['NOME']="Nome";
$tdb[FABRICANTES]['CNPJ']="Cnpj";
$tdb[FABRICANTES]['IE']="Inscri&ccedil;&atilde;o Estadual";
$tdb[FABRICANTES]['ENDERECO']="Endere&ccedil;o";
$tdb[FABRICANTES]['BAIRRO']="Bairro";
$tdb[FABRICANTES]['CEP']="Cep";
$tdb[FABRICANTES]['CIDADE']="Cidade";
$tdb[FABRICANTES]['ESTADO']="UF";
$tdb[FABRICANTES]['PAIS']="Pais";
$tdb[FABRICANTES]['TELEFONE']="Telefone";
$tdb[FABRICANTES]['FAX']="Fax";
$tdb[FABRICANTES]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[FABRICANTES]['SITE']="Site";
$tdb[FABRICANTES]['EMAIL']="E-mail";

define("FUNCIONARIOS", "funcionarios");
$tdb[FUNCIONARIOS]['DESC']="Funcion&aacute;rios";
$tdb[FUNCIONARIOS]['dba']=0;
$tdb[FUNCIONARIOS]['NOME']="Nome";
$tdb[FUNCIONARIOS]['RG']="RG";
$tdb[FUNCIONARIOS]['CPF']="CPF";
$tdb[FUNCIONARIOS]['FORNECEDOR']="Fornecedor";
$tdb[FUNCIONARIOS]['DATA_NASCIMENTO']="Data de Nascimento";
$tdb[FUNCIONARIOS]['MATRICULA']="Matricula";
$tdb[FUNCIONARIOS]['ESPECIALIDADE']="Especialidade";
$tdb[FUNCIONARIOS]['NIVEL']="N&iacute;vel";
$tdb[FUNCIONARIOS]['EQUIPE']="Equipe";
$tdb[FUNCIONARIOS]['TURNO']="Turno";
$tdb[FUNCIONARIOS]['CENTRO_DE_CUSTO']="Centro de Custo";
$tdb[FUNCIONARIOS]['VALOR_HORA']="Valor Hora";
$tdb[FUNCIONARIOS]['ENDERECO']="Endere&ccedil;o";
$tdb[FUNCIONARIOS]['BAIRRO']="Bairro";
$tdb[FUNCIONARIOS]['CEP']="Cep";
$tdb[FUNCIONARIOS]['CIDADE']="Cidade";
$tdb[FUNCIONARIOS]['UF']="UF";
$tdb[FUNCIONARIOS]['TELEFONE']="Telefone";
$tdb[FUNCIONARIOS]['CELULAR']="Celular";
$tdb[FUNCIONARIOS]['EMAIL']="Email";
$tdb[FUNCIONARIOS]['SITUACAO']="Situa&ccedil;&atilde;o";
$tdb[FUNCIONARIOS]['DATA_ADMISSAO']="Data de Admiss&atilde;o";
$tdb[FUNCIONARIOS]['DATA_DEMISSAO']="Data de Demiss&atilde;o";
$tdb[FUNCIONARIOS]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[FUNCIONARIOS]['MID_EMPRESA']="Empresa";

define("FUNCIONARIOS_SITUACAO", "funcionarios_status");
$tdb[FUNCIONARIOS_SITUACAO]['DESC']="Situa&ccedil;&atilde;o";
$tdb[FUNCIONARIOS_SITUACAO]['dba']=0;
$tdb[FUNCIONARIOS_SITUACAO]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("GRUPO_GRAFICO", "grupo_grafico");
$tdb[GRUPO_GRAFICO]['DESC']="Grupo de Gr&aacute;ficos";
$tdb[GRUPO_GRAFICO]['dba']=0;
$tdb[GRUPO_GRAFICO]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("LANCA_CONTADOR", "lanca_contador");
$tdb[LANCA_CONTADOR]['DESC']="Lan&ccedil;amento de Contadores";
$tdb[LANCA_CONTADOR]['dba']=0;
$tdb[LANCA_CONTADOR]['DATA']="Data";
$tdb[LANCA_CONTADOR]['VALOR']="Valor";
$tdb[LANCA_CONTADOR]['MID_CONTADOR']="Contador";
$tdb[LANCA_CONTADOR]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[LANCA_CONTADOR]['MID_USUARIO'] = 'Usu&aacute;rio';
$tdb[LANCA_CONTADOR]['STATUS']="Status";

define("LINK_ROTAS", "link_rotas");
$tdb[LINK_ROTAS]['DESC']="Defini&ccedil;&atilde;o da Rota";
$tdb[LINK_ROTAS]['dba']=0;
$tdb[LINK_ROTAS]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[LINK_ROTAS]['MID_PONTO']="Ponto";
$tdb[LINK_ROTAS]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[LINK_ROTAS]['MID_PLANO']="Rota";
$tdb[LINK_ROTAS]['TAREFA']="Tarefa";
$tdb[LINK_ROTAS]['NUMERO']="N&ordm;";
$tdb[LINK_ROTAS]['MID_MATERIAL']="Material";
$tdb[LINK_ROTAS]['QUANTIDADE']="Qtd. por Ponto";
$tdb[LINK_ROTAS]['PERIODICIDADE']="Periodicidade";
$tdb[LINK_ROTAS]['FREQUENCIA']="Frequencia";
$tdb[LINK_ROTAS]['CONTADOR']="Contador";
$tdb[LINK_ROTAS]['DISPARO']="Disparo";
$tdb[LINK_ROTAS]['TIPO']="Tipo";
$tdb[LINK_ROTAS]['ESPECIALIDADE']="Especialidade";
$tdb[LINK_ROTAS]['TEMPO_PREVISTO']="Tempo Previsto";
$tdb[LINK_ROTAS]['QUANTIDADE_MO']="Quantidade de Funcion&aacute;rios";
$tdb[LINK_ROTAS]['STATUS']="Status";

/*
define("LOGS","logs");
$tdb[LOGS]['DESC']="Registros do Sistema";
$tdb[LOGS]['dba']=0;
$tdb[LOGS]['USUARIO']="Usu&aacute;rio";
$tdb[LOGS]['IP']="IP";
$tdb[LOGS]['DATA']="Data";
$tdb[LOGS]['HORA']="Hora";
$tdb[LOGS]['TIPO']="Tipo";
$tdb[LOGS]['TEXTO']="Texto";
$tdb[LOGS]['TABELA']="Tabela";
*/

define("LOGS_TIPOS","logs_tipos");
$tdb[LOGS_TIPOS]['DESC']="Tipos de Log";
$tdb[LOGS_TIPOS]['dba']=0;
$tdb[LOGS_TIPOS]['DESCRICAO']="Descri&ccedil;&atilde;o";


define("MAQUINAS", "maquinas");
$tdb[MAQUINAS]['DESC']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS]['DESC_CAMPO']="COD";
$tdb[MAQUINAS]['dba']=0;
$tdb[MAQUINAS]['COD']="Tag";
$tdb[MAQUINAS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MAQUINAS]['MODELO']="Modelo";
$tdb[MAQUINAS]['NUMERO_DE_SERIE']="N&uacute;mero de S&eacute;rie";
$tdb[MAQUINAS]['ANO_DE_FABRICACAO']="Data de Fabrica&ccedil;&atilde;o";
$tdb[MAQUINAS]['NUMERO_DO_PATRIMONIO']="N&uacute;mero de Patrim&ocirc;nio";
$tdb[MAQUINAS]['GARANTIA']="Garantia";
$tdb[MAQUINAS]['DADOS_TECNICO']="Dados T&eacute;cnicos";
$tdb[MAQUINAS]['FAMILIA']="Fam&iacute;lia";
$tdb[MAQUINAS]['CENTRO_DE_CUSTO']="Centro de Custo";
$tdb[MAQUINAS]['CLASSE']="Classe";
$tdb[MAQUINAS]['STATUS']="Status";
$tdb[MAQUINAS]['LOCALIZACAO_FISICA']="Localiza&ccedil;&atilde;o F&iacute;sica";
$tdb[MAQUINAS]['FABRICANTE']="Fabricante";
$tdb[MAQUINAS]['FORNECEDOR']="Fornecedor preferencial";
$tdb[MAQUINAS]['MID_SETOR']="Localiza&ccedil;&atilde;o 2";
$tdb[MAQUINAS]['MID_EMPRESA']="Empresa";

define("MAQUINAS_CLASSE", "maquinas_classe");
$tdb[MAQUINAS_CLASSE]['DESC']="Classe de Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_CLASSE]['dba']=0;
$tdb[MAQUINAS_CLASSE]['COD']="C&oacute;digo";
$tdb[MAQUINAS_CLASSE]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("MAQUINAS_CONJUNTO", "maquinas_conjunto");
$tdb[MAQUINAS_CONJUNTO]['DESC']="Posi&ccedil;&otilde;es";
$tdb[MAQUINAS_CONJUNTO]['dba']=0;
$tdb[MAQUINAS_CONJUNTO]['TAG']="Tag";
$tdb[MAQUINAS_CONJUNTO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MAQUINAS_CONJUNTO]['COMPLEMENTO']="Complemento";
$tdb[MAQUINAS_CONJUNTO]['FICHA_TECNICA']="Dados Tecnicos";
$tdb[MAQUINAS_CONJUNTO]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_CONJUNTO]['FABRICANTE']="Fabricante";
$tdb[MAQUINAS_CONJUNTO]['MARCA']="Marca";
$tdb[MAQUINAS_CONJUNTO]['MODELO']="Modelo";
$tdb[MAQUINAS_CONJUNTO]['NSERIE']="N&uacute;mero de S&eacute;rie";
$tdb[MAQUINAS_CONJUNTO]['POTENCIA']="Pot&ecirc;ncia";
$tdb[MAQUINAS_CONJUNTO]['PRESSAO']="Press&atilde;o";
$tdb[MAQUINAS_CONJUNTO]['CORRENTE']="Corrente";
$tdb[MAQUINAS_CONJUNTO]['TENSAO']="Tens&atilde;o";
$tdb[MAQUINAS_CONJUNTO]['VAZAO']="Vaz&atilde;o";
$tdb[MAQUINAS_CONJUNTO]['MID_CONJUNTO']="Posi&ccedil;&atilde;o Pai";
$tdb[MAQUINAS_CONJUNTO]['TIPO']="Tipo";

define("MAQUINAS_CONJUNTO_MATERIAL", "maquinas_conjunto_material");
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['DESC']="Pe&ccedil;as da Posi&ccedil;&atilde;o";
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']=0;
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_MATERIAL']="Material";
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['QUANTIDADE']="Quantidade";
$tdb[MAQUINAS_CONJUNTO_MATERIAL]['MID_CLASSE']="Classifica&ccedil;&atilde;o";

define("MAQUINAS_CONTADOR", "maquinas_contador");
$tdb[MAQUINAS_CONTADOR]['DESC']="Contadores";
$tdb[MAQUINAS_CONTADOR]['dba']=0;
$tdb[MAQUINAS_CONTADOR]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MAQUINAS_CONTADOR]['TIPO']="Tipo";
$tdb[MAQUINAS_CONTADOR]['ZERA']="Zera em";
$tdb[MAQUINAS_CONTADOR]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";

define("MAQUINAS_CONTADOR_TIPO", "maquinas_contador_tipo");
$tdb[MAQUINAS_CONTADOR_TIPO]['DESC']="Tipos de Contadores";
$tdb[MAQUINAS_CONTADOR_TIPO]['dba']=0;
$tdb[MAQUINAS_CONTADOR_TIPO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MAQUINAS_CONTADOR_TIPO]['UNIDADE']="Unidade";

// MAQUINA DISPONIBILIDADE
define("MAQUINAS_DISPONIBILIDADE", "maquinas_disponibilidade");
$tdb[MAQUINAS_DISPONIBILIDADE]['DESC']="Tempo de Opera&ccedil;&atilde;o do Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_DISPONIBILIDADE]['dba']=0;
$tdb[MAQUINAS_DISPONIBILIDADE]['MES']="Mes";
$tdb[MAQUINAS_DISPONIBILIDADE]['ANO']="Ano";
$tdb[MAQUINAS_DISPONIBILIDADE]['HORAS']="Horas Opera&ccedil;&atilde;o(Decimal)";
$tdb[MAQUINAS_DISPONIBILIDADE]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";

define("MAQUINAS_FAMILIA", "maquinas_familia");
$tdb[MAQUINAS_FAMILIA]['DESC']="Fam&iacute;lia de Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_FAMILIA]['dba']=0;
$tdb[MAQUINAS_FAMILIA]['COD']="C&oacute;digo";
$tdb[MAQUINAS_FAMILIA]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MAQUINAS_FAMILIA]['MID_EMPRESA']="Empresa";

define("MAQUINAS_FORNECEDOR", "maquinas_fornecedor");
$tdb[MAQUINAS_FORNECEDOR]['DESC']="Fornecedores do Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_FORNECEDOR]['dba']=0;
$tdb[MAQUINAS_FORNECEDOR]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_FORNECEDOR]['MID_FORNECEDOR']="Fornecedor";
$tdb[MAQUINAS_FORNECEDOR]['OBS']="Observa&ccedil;&atilde;o";

define("MAQUINAS_PARADA", "maquina_parada");
$tdb[MAQUINAS_PARADA]['DESC']="Parada";
$tdb[MAQUINAS_PARADA]['dba']=0;
$tdb[MAQUINAS_PARADA]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("MAQUINAS_STATUS", "maquinas_status");
$tdb[MAQUINAS_STATUS]['DESC']="Status do Objeto de Manuten&ccedil;&atilde;o";
$tdb[MAQUINAS_STATUS]['dba']=0;
$tdb[MAQUINAS_STATUS]['DESCRICAO']="Descri&ccedil;&atilde;o";


define("MATERIAIS", "materiais");
$tdb[MATERIAIS]['DESC']="Materiais";
$tdb[MATERIAIS]['dba']=0;
$tdb[MATERIAIS]['COD']="C&oacute;digo";
$tdb[MATERIAIS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MATERIAIS]['FAMILIA']="Fam&iacute;lia";
$tdb[MATERIAIS]['MID_SUBFAMILIA']="Sub-Fam&iacute;lia";
$tdb[MATERIAIS]['UNIDADE']="Unidade";
$tdb[MATERIAIS]['LOCALIZACAO']="Localiza&ccedil;&atilde;o";
$tdb[MATERIAIS]['ESTOQUE_MINIMO']="Estoque Min&iacute;mo";
$tdb[MATERIAIS]['ESTOQUE_MAXIMO']="Estoque M&aacute;ximo";
$tdb[MATERIAIS]['ESTOQUE_ATUAL']="Estoque Total";
$tdb[MATERIAIS]['CUSTO_UNITARIO']="Custo Unit&aacute;rio";
$tdb[MATERIAIS]['FORNECEDOR']="Fornecedor";
$tdb[MATERIAIS]['FABRICANTE']="Fabricante";
$tdb[MATERIAIS]['COMPLEMENTO']="Complemento";
$tdb[MATERIAIS]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[MATERIAIS]['MID_EMPRESA']="Empresa";
$tdb[MATERIAIS]['MID_CRITICIDADE']="Criticidade";

define("MATERIAIS_CRITICIDADE", "materiais_criticidade");
$tdb[MATERIAIS_CRITICIDADE]['DESC']="Criticidade de Materiais";
$tdb[MATERIAIS_CRITICIDADE]['dba']=0;
$tdb[MATERIAIS_CRITICIDADE]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("MATERIAIS_CLASSE", "materiais_classe");
$tdb[MATERIAIS_CLASSE]['DESC']="Classifica&ccedil;&atilde;o das Pe&ccedil;as";
$tdb[MATERIAIS_CLASSE]['dba']=0;
$tdb[MATERIAIS_CLASSE]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("MATERIAIS_FAMILIA", "materiais_familia");
$tdb[MATERIAIS_FAMILIA]['DESC']="Fam&iacute;lia de Materiais";
$tdb[MATERIAIS_FAMILIA]['dba']=0;
$tdb[MATERIAIS_FAMILIA]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MATERIAIS_FAMILIA]['COD']="C&oacute;digo";

define("MATERIAIS_UNIDADE", "materiais_unidade");
$tdb[MATERIAIS_UNIDADE]['DESC']="Unidades";
$tdb[MATERIAIS_UNIDADE]['dba']=0;
$tdb[MATERIAIS_UNIDADE]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MATERIAIS_UNIDADE]['COD']="C&oacute;digo";

define("MATERIAIS_SUBFAMILIA", "materiais_subfamilia");
$tdb[MATERIAIS_SUBFAMILIA]['DESC']="Sub-Fam&iacute;lia de Materiais";
$tdb[MATERIAIS_SUBFAMILIA]['dba']=0;
$tdb[MATERIAIS_SUBFAMILIA]['COD']="C&oacute;digo";
$tdb[MATERIAIS_SUBFAMILIA]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[MATERIAIS_SUBFAMILIA]['MID_FAMILIA']="Fam&iacute;lia";

define("MOV_EQUIPAMENTO", "mov_equipamento");
$tdb[MOV_EQUIPAMENTO]['DESC']="Movimenta&ccedil;&atilde;o Componente";
$tdb[MOV_EQUIPAMENTO]['dba']=0;
$tdb[MOV_EQUIPAMENTO]['MID_EQUIPAMENTO']="Componente";
$tdb[MOV_EQUIPAMENTO]['MID_CONJUNTO']="Posi&ccedil;&atilde;o de Origem";
$tdb[MOV_EQUIPAMENTO]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o de Origem";
$tdb[MOV_EQUIPAMENTO]['MID_DCONJUNTO']="Posi&ccedil;&atilde;o de Destino";
$tdb[MOV_EQUIPAMENTO]['MID_DMAQUINA']="Objeto de Manuten&ccedil;&atilde;o de Destino";
$tdb[MOV_EQUIPAMENTO]['MOTIVO']="Motivo";
$tdb[MOV_EQUIPAMENTO]['DATA']="Data";

define("MOV_MAQUINA", "mov_maquina");
$tdb[MOV_MAQUINA]['DESC']="Movimenta&ccedil;&atilde;o de Objeto de Manuten&ccedil;&atilde;o";
$tdb[MOV_MAQUINA]['dba']=0;
$tdb[MOV_MAQUINA]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o de Origem";
$tdb[MOV_MAQUINA]['MID_SETOR']="Localiza&ccedil;&atilde;o de Origem";
$tdb[MOV_MAQUINA]['MID_DMSETOR']="Localiza&ccedil;&atilde;o de Destino";
$tdb[MOV_MAQUINA]['MOTIVO']="Motivo";
$tdb[MOV_MAQUINA]['DATA']="Data";
$tdb[MOV_MAQUINA]['SEMANA']="Semana";

define("NATUREZA_SERVICOS", "natureza_servicos");
$tdb[NATUREZA_SERVICOS]['DESC']="Natureza dos Servi&ccedil;os";
$tdb[NATUREZA_SERVICOS]['dba']=0;
$tdb[NATUREZA_SERVICOS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[NATUREZA_SERVICOS]['MID_EMPRESA']="Empresa";

define("ORDEM", "ordem");
$tdb[ORDEM]['dba']=0;
$tdb[ORDEM]['DESC']="Ordens";
$tdb[ORDEM]['DESC_CAMPO']="NUMERO";
$tdb[ORDEM]['MID_USUARIO']="Usu&acute;rio";
$tdb[ORDEM]['MID_EMPRESA']="Empresa";
$tdb[ORDEM]['MID_AREA']="Localiza&ccedil;&atilde;o 1";
$tdb[ORDEM]['MID_SETOR']="Localiza&ccedil;&atilde;o 2";
$tdb[ORDEM]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[ORDEM]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[ORDEM]['CENTRO_DE_CUSTO']="Centro de Custo";
$tdb[ORDEM]['MID_EQUIPAMENTO']="Componente";
$tdb[ORDEM]['MID_FORNECEDOR']="Fornecedor";
$tdb[ORDEM]['MID_CLASSE']="Classe";
$tdb[ORDEM]['MID_FABRICANTE']="Fabricante";
$tdb[ORDEM]['MID_MAQUINA_FAMILIA']="Fam&iacute;lia de Objeto de Manuten&ccedil;&atilde;o";
$tdb[ORDEM]['SOLICITANTE']="Solicitante";
$tdb[ORDEM]['TIPO']="Tipo de Programa&ccedil;&atilde;o";
$tdb[ORDEM]['MID_PROGRAMACAO']="Programa&ccedil;&atilde;o";
$tdb[ORDEM]['NUMERO']="N&uacute;mero";
$tdb[ORDEM]['RESPONSAVEL']="Respons&aacute;vel";
$tdb[ORDEM]['TEXTO']="Texto da OS";
$tdb[ORDEM]['TIPO_SERVICO']="Tipo de Servi&ccedil;o";
$tdb[ORDEM]['NATUREZA']="Natureza do Servi&ccedil;o";
$tdb[ORDEM]['STATUS']="Situa&ccedil;&atilde;o";
$tdb[ORDEM]['DATA_PROG']="Data Prog.";
$tdb[ORDEM]['HORA_ABRE']="Hora Abre.";
$tdb[ORDEM]['DATA_ABRE']="Data Abre.";
$tdb[ORDEM]['DATA_INICIO']="Data &Iacute;nicio";
$tdb[ORDEM]['HORA_INICIO']="Hora Iacute;nicio";
$tdb[ORDEM]['DATA_FINAL']="Data Final";
$tdb[ORDEM]['HORA_FINAL']="Hora Final";
$tdb[ORDEM]['TEMPO_TOTAL']="Tempo Total";
$tdb[ORDEM]['CAUSA']="Causa";
$tdb[ORDEM]['CAUSA_TEXTO']="Texto Causa";
$tdb[ORDEM]['DEFEITO']="Defeito";
$tdb[ORDEM]['DEFEITO_TEXTO']="Texto Defeito";
$tdb[ORDEM]['SOLUCAO']="Solu&ccedil;&atilde;o";
$tdb[ORDEM]['SOLUCAO_TEXTO']="Texto Solu&ccedil;&atilde;o";
$tdb[ORDEM]['MID_SPEED_CHECK']="Plano Checklist";

/* SALVA A OS NO HIST�RICO AP�S REMO��O DO SISTEMA */
define("ORDEM_REMOVIDA", "ordem_removida");
$tdb[ORDEM_REMOVIDA]['dba']=0;
$tdb[ORDEM_REMOVIDA]['DESC']="Ordens removidas do sistema";
$tdb[ORDEM_REMOVIDA]['DESC_CAMPO']="NUMERO";
$tdb[ORDEM_REMOVIDA]['NUMERO']="N&uacute;mero";
$tdb[ORDEM_REMOVIDA]['DATA_PROG']="Data prog.";
$tdb[ORDEM_REMOVIDA]['HORA_ABRE']="Hora Abre.";
$tdb[ORDEM_REMOVIDA]['DATA_ABRE']="Data Abre.";
$tdb[ORDEM_REMOVIDA]['USUARIO']="Usu&acute;rio";
$tdb[ORDEM_REMOVIDA]['MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[ORDEM_REMOVIDA]['CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[ORDEM_REMOVIDA]['TIPO_SERVICO']="Tipo de Servi&ccedil;o";
$tdb[ORDEM_REMOVIDA]['TIPO']="Tipo de Programa&ccedil;&atilde;o";
$tdb[ORDEM_REMOVIDA]['PLANO']="Plano";
$tdb[ORDEM_REMOVIDA]['TEXTO']="Texto da OS";
$tdb[ORDEM_REMOVIDA]['MOTIVO_REMOVE']="Motivo da remo��o";
$tdb[ORDEM_REMOVIDA]['DATA_HORA_REMOVE']="Data e hora remo&ccedil;&atilde;o";
$tdb[ORDEM_REMOVIDA]['MID_USUARIO_REMOVE']="Usu&aacute;rio remo&ccedil;&atilde;o";

/**
 * Compatibilidade
 *
 */
define("ORDEM_PLANEJADO", ORDEM);
define("ORDEM_PLANEJADO_STATUS", "ordem_status");
define("ORDEM_PLANEJADO_MADODEOBRA", "ordem_maodeobra");
define("ORDEM_PLANEJADO_MATERIAL", "ordem_material");
define("ORDEM_PLANEJADO_MAQ", "ordem_maq");
define("ORDEM_PLANEJADO_PRED", "ordem_pred");
define("ORDEM_PLANEJADO_PREV", "ordem_prev");
define("ORDEM_PLANEJADO_LUB", "ordem_lub");
define("ORDEM_PLANEJADO_CUSTOS", "ordem_custos");
define("ORDEM_PLANEJADO_MAQ_PARADA", "ordem_maq_parada");
define("ORDEM_PLANEJADO_REPROGRAMA", "ordem_reprograma");

define ("LANC_PREDITIVA",'lanca_preditiva');
define ("LANCA_PREDITIVA",'lanca_preditiva');
$tdb[LANC_PREDITIVA]['DESC']="Lan&ccedil;amento de Preditiva";
$tdb[LANC_PREDITIVA]['dba']=0;
$tdb[LANC_PREDITIVA]['DATA']='Data';
$tdb[LANC_PREDITIVA]['VALOR']='Valor';
$tdb[LANC_PREDITIVA]['MID_PONTO']='Ponto';

define("ORDEM_STATUS", "ordem_status");
$tdb[ORDEM_STATUS]['DESC']="Status da Ordem";
$tdb[ORDEM_STATUS]['dba']=0;
$tdb[ORDEM_STATUS]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("ORDEM_MADODEOBRA", "ordem_maodeobra");
define("ORDEM_MAODEOBRA", "ordem_maodeobra");
$tdb[ORDEM_MADODEOBRA]['DESC']="M&atilde;o-de-obra alocada";
$tdb[ORDEM_MADODEOBRA]['dba']=0;
$tdb[ORDEM_MADODEOBRA]['MID_ORDEM']="Programa&ccedil;&atilde;o";
$tdb[ORDEM_MADODEOBRA]['MID_FUNCIONARIO']="Funcion&aacute;rio";
$tdb[ORDEM_MADODEOBRA]['CUSTO_EXTRA']="Extra";
$tdb[ORDEM_MADODEOBRA]['DATA_INICIO']="Data Inicio";
$tdb[ORDEM_MADODEOBRA]['DATA_FINAL']="Data Final";
$tdb[ORDEM_MADODEOBRA]['HORA_INICIO']="Hora In&iacute;cio";
$tdb[ORDEM_MADODEOBRA]['HORA_FINAL']="Hora Final";
$tdb[ORDEM_MADODEOBRA]['CUSTO']="Custo Total";

define("ORDEM_MATERIAL", "ordem_material");
$tdb[ORDEM_MATERIAL]['DESC']="Materiais utilizados";
$tdb[ORDEM_MATERIAL]['dba']=0;
$tdb[ORDEM_MATERIAL]['MID_ORDEM']="Ordem";
$tdb[ORDEM_MATERIAL]['MID_MATERIAL']="Material";
$tdb[ORDEM_MATERIAL]['QUANTIDADE']="Quantidade";
$tdb[ORDEM_MATERIAL]['CUSTO_UNITARIO']="Custo unit&aacute;rio";
$tdb[ORDEM_MATERIAL]['CUSTO_TOTAL']="Custo total";
$tdb[ORDEM_MATERIAL]['MID_ALMOXARIFADO']="Almoxarifado";


define("ORDEM_PREV", "ordem_prev");
$tdb[ORDEM_PREV]['DESC']="Atividades";
$tdb[ORDEM_PREV]['dba']=0;
$tdb[ORDEM_PREV]['MID_ORDEM']="Ordem";
$tdb[ORDEM_PREV]['MID_ATV']="Atividade";
$tdb[ORDEM_PREV]['MID_PLANO_PADRAO']="Plano Padr&atilde;o";
$tdb[ORDEM_PREV]['TAREFA']="Tarefa";
$tdb[ORDEM_PREV]['PARTE']="Parte a Verificar";
$tdb[ORDEM_PREV]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[ORDEM_PREV]['INSTRUCAO_DE_TRABALHO']="Instru&ccedil;&atilde;o de Trabalho";
$tdb[ORDEM_PREV]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[ORDEM_PREV]['ESPECIALIDADE']="Especialidade";
$tdb[ORDEM_PREV]['TEMPO_PREVISTO']="Tempo Previsto";
$tdb[ORDEM_PREV]['QUANTIDADE_MO']="Quantidade de Funcion&aacute;rios";
$tdb[ORDEM_PREV]['NUMERO']="N&ordm;";
$tdb[ORDEM_PREV]['PERIODICIDADE']="Periodicidade";
$tdb[ORDEM_PREV]['FREQUENCIA']="Frequencia";
$tdb[ORDEM_PREV]['CONTADOR']="Contador";
$tdb[ORDEM_PREV]['DISPARO']="Disparo";
$tdb[ORDEM_PREV]['MID_MATERIAL']="Material";
$tdb[ORDEM_PREV]['QUANTIDADE']="Quantidade";
$tdb[ORDEM_PREV]['MAQUINA_PARADA']="Exige Parada?";


define("ORDEM_LUB", "ordem_lub");
$tdb[ORDEM_LUB]['DESC']="Atividades";
$tdb[ORDEM_LUB]['dba']=0;
$tdb[ORDEM_LUB]['MID_ORDEM']="Ordem";
$tdb[ORDEM_LUB]['MID_ATV']="Atividade";
$tdb[ORDEM_LUB]['MID_EMPRESA']="Empresa";
$tdb[ORDEM_LUB]['MID_AREA']="Localiza&ccedil;&atilde;o 1";
$tdb[ORDEM_LUB]['MID_SETOR']="Localiza&ccedil;&atilde;o 2";
$tdb[ORDEM_LUB]['CENTRO_DE_CUSTO']="Centro de Custo";
$tdb[ORDEM_LUB]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[ORDEM_LUB]['MID_PONTO']="Ponto";
$tdb[ORDEM_LUB]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[ORDEM_LUB]['MID_PLANO']="Rota";
$tdb[ORDEM_LUB]['TAREFA']="Tarefa";
$tdb[ORDEM_LUB]['NUMERO']="N&ordm;";
$tdb[ORDEM_LUB]['MID_MATERIAL']="Material";
$tdb[ORDEM_LUB]['QUANTIDADE']="Qtd. por Ponto";
$tdb[ORDEM_LUB]['PERIODICIDADE']="Periodicidade";
$tdb[ORDEM_LUB]['FREQUENCIA']="Frequ&ecirc;ncia";
$tdb[ORDEM_LUB]['CONTADOR']="Contador";
$tdb[ORDEM_LUB]['DISPARO']="Disparo";
$tdb[ORDEM_LUB]['TIPO']="Tipo";
$tdb[ORDEM_LUB]['ESPECIALIDADE']="Especialidade";
$tdb[ORDEM_LUB]['TEMPO_PREVISTO']="Tempo Previsto";
$tdb[ORDEM_LUB]['QUANTIDADE_MO']="Quantidade de Funcion&aacute;rios";
$tdb[ORDEM_LUB]['STATUS']="Status";

define("ORDEM_CUSTOS", "ordem_custos");
$tdb[ORDEM_CUSTOS]['DESC']="Outros custos";
$tdb[ORDEM_CUSTOS]['dba']=0;
$tdb[ORDEM_CUSTOS]['MID_ORDEM']="Ordem";
$tdb[ORDEM_CUSTOS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[ORDEM_CUSTOS]['CUSTO']="Custo";


define("ORDEM_MAQ_PARADA", "ordem_maq_parada");
$tdb[ORDEM_MAQ_PARADA]['DESC']="Parada de Objeto de manuten&ccedil;&atilde;o";
$tdb[ORDEM_MAQ_PARADA]['dba']=0;
$tdb[ORDEM_MAQ_PARADA]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[ORDEM_MAQ_PARADA]['DATA_INICIO']="Data Inicio";
$tdb[ORDEM_MAQ_PARADA]['DATA_FINAL']="Data Final";
$tdb[ORDEM_MAQ_PARADA]['HORA_INICIO']="Hora In&iacute;cio";
$tdb[ORDEM_MAQ_PARADA]['HORA_FINAL']="Hora Final";
$tdb[ORDEM_MAQ_PARADA]['TEMPO']="Tempo";

define("ORDEM_REPROGRAMA", "ordem_reprograma");
$tdb[ORDEM_REPROGRAMA]['DESC']="Reprograma&ccedil;&atilde;o";
$tdb[ORDEM_REPROGRAMA]['dba']=0;
$tdb[ORDEM_REPROGRAMA]['MID_ORDEM']="Ordem";
$tdb[ORDEM_REPROGRAMA]['MOTIVO']="Motivo";
$tdb[ORDEM_REPROGRAMA]['DATA']="Data";
$tdb[ORDEM_REPROGRAMA]['DATA_ORIGINAL']="Data Original";

define('ORDEM_MO_PREVISTO', 'ordem_mo_previsto');
$tdb[ORDEM_MO_PREVISTO]['DESC']="M&atilde;o-de-obra prevista por especialidade";
$tdb[ORDEM_MO_PREVISTO]['dba']=0;
$tdb[ORDEM_MO_PREVISTO]['MID_ORDEM']="O.S.";
$tdb[ORDEM_MO_PREVISTO]['MID_ESPECIALIDADE']="Especialidade";
$tdb[ORDEM_MO_PREVISTO]['QUANTIDADE']="Quantidade";
$tdb[ORDEM_MO_PREVISTO]['TEMPO']="Tempo";
$tdb[ORDEM_MO_PREVISTO]['CUSTO']="Custo";

define('ORDEM_MO_ALOC', 'ordem_mo_aloc');
$tdb[ORDEM_MO_ALOC]['DESC']="M&atilde;o-de-obra prevista por funcion&aacute;rio";
$tdb[ORDEM_MO_ALOC]['dba']=0;
$tdb[ORDEM_MO_ALOC]['MID_ORDEM']="O.S.";
$tdb[ORDEM_MO_ALOC]['MID_FUNCIONARIO']="Funcion&aacute;rio";
$tdb[ORDEM_MO_ALOC]['MID_ESPECIALIDADE']="Especialidade";
$tdb[ORDEM_MO_ALOC]['TEMPO']="Tempo";

define("ORDEM_MAT_PREVISTO", "ordem_mat_previsto");
$tdb[ORDEM_MAT_PREVISTO]['DESC']="Materiais previstos";
$tdb[ORDEM_MAT_PREVISTO]['dba']=0;
$tdb[ORDEM_MAT_PREVISTO]['MID_ORDEM']="Ordem";
$tdb[ORDEM_MAT_PREVISTO]['MID_MATERIAL']=$tdb[MATERIAIS]['DESC'];
$tdb[ORDEM_MAT_PREVISTO]['QUANTIDADE']="Quantidade";
$tdb[ORDEM_MAT_PREVISTO]['CUSTO']="Custo";


define("PENDENCIAS", "pendencias");
$tdb[PENDENCIAS]['DESC']="Pend&ecirc;ncias / Notas";
$tdb[PENDENCIAS]['dba']=0;
$tdb[PENDENCIAS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[PENDENCIAS]['MID_ORDEM']="Ordem";
$tdb[PENDENCIAS]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[PENDENCIAS]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[PENDENCIAS]['MID_EQUIPAMENTO']="Componente";
$tdb[PENDENCIAS]['DATA']="Data";
$tdb[PENDENCIAS]['MID_ORDEM_EXC']="Ordem Execudata";
$tdb[PENDENCIAS]['NUMERO']="N&ordm;";



define("PLANO_PADRAO", "plano_padrao");
$tdb[PLANO_PADRAO]['DESC']="Plano Padr&atilde;o da Preventiva";
$tdb[PLANO_PADRAO]['DESC_CAMPO']="DESCRICAO";
$tdb[PLANO_PADRAO]['dba']=0;
$tdb[PLANO_PADRAO]['MID_EMPRESA']="Empresa";
$tdb[PLANO_PADRAO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[PLANO_PADRAO]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[PLANO_PADRAO]['MAQUINA_FAMILIA']="Fam&iacute;lia de Objeto de Manuten&ccedil;&atilde;o";
$tdb[PLANO_PADRAO]['EQUIPAMENTO_FAMILIA']="Fam&iacute;lia Componentes";
$tdb[PLANO_PADRAO]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[PLANO_PADRAO]['TIPO']="Aplica&ccedil;&atilde;o";


define("PLANO_ROTAS", "plano_rotas");
$tdb[PLANO_ROTAS]['DESC']="Plano de Rotas";
$tdb[PLANO_ROTAS]['dba']=0;
$tdb[PLANO_ROTAS]['MID_EMPRESA']="Empresa";
$tdb[PLANO_ROTAS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[PLANO_ROTAS]['OBSERVACAO']="Observa&ccedil;&atilde;o";

define("PROGRAMACAO", "programacao");
$tdb[PROGRAMACAO]['DESC']="Programa&ccedil;&atilde;o";
$tdb[PROGRAMACAO]['dba']=0;
$tdb[PROGRAMACAO]['TIPO']="Tipo";
$tdb[PROGRAMACAO]['MID_PLANO']="Plano";
$tdb[PROGRAMACAO]['HORARIO']="Hor&aacute;rio";
$tdb[PROGRAMACAO]['DATA_INICIAL']="Data Inicio";
$tdb[PROGRAMACAO]['DATA_FINAL']="Data Final";
$tdb[PROGRAMACAO]['MID_MAQUINA']="Destino da Manuten&ccedil;&atilde;o";
$tdb[PROGRAMACAO]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[PROGRAMACAO]['STATUS']="Status";
$tdb[PROGRAMACAO]['MOTIVO']="Motivo";

define("PROGRAMACAO_MATERIAIS", "programacao_materiais");
$tdb[PROGRAMACAO_MATERIAIS]['DESC']="Aloca&ccedil;&atilde;o de Materiais";
$tdb[PROGRAMACAO_MATERIAIS]['dba']=0;
$tdb[PROGRAMACAO_MATERIAIS]['MID_PROGRAMACAO']="Programa&ccedil;&atilde;o";
$tdb[PROGRAMACAO_MATERIAIS]['MID_MATERIAL']="Material";
$tdb[PROGRAMACAO_MATERIAIS]['QUANTIDADE']="Quantidade";

define("PROGRAMACAO_MAODEOBRA", "programacao_maodeobra");
$tdb[PROGRAMACAO_MAODEOBRA]['DESC']="Aloca&ccedil;&atilde;o de M&atilde;o de Obra";
$tdb[PROGRAMACAO_MAODEOBRA]['dba']=0;
$tdb[PROGRAMACAO_MAODEOBRA]['MID_PROGRAMACAO']="Programa&ccedil;&atilde;o";
$tdb[PROGRAMACAO_MAODEOBRA]['MID_EQUIPE']="Equipe";
$tdb[PROGRAMACAO_MAODEOBRA]['TEMPO']="Tempo";

define("PROGRAMACAO_PERIODICIDADE", "programacao_periodicidade");
$tdb[PROGRAMACAO_PERIODICIDADE]['DESC']="Periodicidade";
$tdb[PROGRAMACAO_PERIODICIDADE]['dba']=0;
$tdb[PROGRAMACAO_PERIODICIDADE]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("PROGRAMACAO_TIPO", "programacao_tipo");
$tdb[PROGRAMACAO_TIPO]['DESC']="Tipos de Programa&ccedil;&atilde;o";
$tdb[PROGRAMACAO_TIPO]['dba']=0;
$tdb[PROGRAMACAO_TIPO]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("PONTOS_PREDITIVA", "pontos_preditiva");
$tdb[PONTOS_PREDITIVA]['DESC']="Pontos de Monitoramento";
$tdb[PONTOS_PREDITIVA]['dba']=0;
$tdb[PONTOS_PREDITIVA]['PONTO']="Ponto";
$tdb[PONTOS_PREDITIVA]['GRANDEZA']="Grandeza";
$tdb[PONTOS_PREDITIVA]['VALOR_MINIMO']="Valor M&iacute;nimo";
$tdb[PONTOS_PREDITIVA]['VALOR_MAXIMO']="Valor Maximo";
$tdb[PONTOS_PREDITIVA]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[PONTOS_PREDITIVA]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[PONTOS_PREDITIVA]['MID_EQUIPAMENTO']="Componente";
$tdb[PONTOS_PREDITIVA]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";



define("REQUISICAO","requisicao");
$tdb[REQUISICAO]['DESC']="Requisi&ccedil;&atilde;o de Material";
$tdb[REQUISICAO]['dba']=0;
$tdb[REQUISICAO]['DATA']="Data";
$tdb[REQUISICAO]['HORA']="Hora";
$tdb[REQUISICAO]['MOTIVO']="Motivo";
$tdb[REQUISICAO]['ORDEM']="Ordem";
$tdb[REQUISICAO]['PRAZO']="Prazo";
$tdb[REQUISICAO]['STATUS']="Status";
$tdb[REQUISICAO]['MATERIAIS']="Materiais";
$tdb[REQUISICAO]['FUNCIONARIO']="Funcionario";
$tdb[REQUISICAO]['USUARIO']="Usuario";
$tdb[REQUISICAO]['MID_ALMOXARIFADO']="Almoxarifado";

define("REQUISICAO_RESPOSTA","requisicao_resposta");
$tdb[REQUISICAO_RESPOSTA]['DESC']="Resposta da Requisi&ccedil;&atilde;o";
$tdb[REQUISICAO_RESPOSTA]['dba']=0;
$tdb[REQUISICAO_RESPOSTA]['DATA']="Data";
$tdb[REQUISICAO_RESPOSTA]['MID_REQUISICAO']="Requisi&ccedil;&atilde;o";
$tdb[REQUISICAO_RESPOSTA]['USUARIO']="Usuario";
$tdb[REQUISICAO_RESPOSTA]['HORA']="Hora";
$tdb[REQUISICAO_RESPOSTA]['TEXTO']="Texto";


define("ROTEIRO_ROTAS","roteiro_rotas");
$tdb[ROTEIRO_ROTAS]['DESC']="Defini&ccedil;&atilde;o de Roteiro";
$tdb[ROTEIRO_ROTAS]['dba']=0;
$tdb[ROTEIRO_ROTAS]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[ROTEIRO_ROTAS]['POSICAO']="Posi&ccedil;&atilde;o";
$tdb[ROTEIRO_ROTAS]['MID_PLANO']="Rota";

define("SETORES", "setores");
$tdb[SETORES]['DESC']="Localiza&ccedil;&atilde;o 2";
$tdb[SETORES]['dba']=0;
$tdb[SETORES]['COD']="Tag";
$tdb[SETORES]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[SETORES]['MID_AREA']="Localiza&ccedil;&atilde;o 1";

define("SOLICITACOES", "solicitacoes");
$tdb[SOLICITACOES]['DESC']="Solicita&ccedil;&atilde;o";
$tdb[SOLICITACOES]['dba']=0;
$tdb[SOLICITACOES]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[SOLICITACOES]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[SOLICITACOES]['STATUS']="Situa&ccedil;&atilde;o";
$tdb[SOLICITACOES]['TEXTO']="Necessidade";
$tdb[SOLICITACOES]['DATA']="Data";
$tdb[SOLICITACOES]['HORA']="Hora";
$tdb[SOLICITACOES]['USUARIO']="Solicitante";
$tdb[SOLICITACOES]['DESCRICAO']="Assunto";
$tdb[SOLICITACOES]['STATUS']="Status";
$tdb[SOLICITACOES]['NUMERO']="N&ordm;";
$tdb[SOLICITACOES]['PRODUCAO']="Produ&ccedil;&atilde;o";
$tdb[SOLICITACOES]['SEGURANCA']="Seguran&ccedil;a";

define("SOLICITACAO_RESPOSTA", "solicitacao_resposta");
$tdb[SOLICITACAO_RESPOSTA]['DESC']="Resposta da Solicita&ccedil;&atilde;o";
$tdb[SOLICITACAO_RESPOSTA]['dba']=0;
$tdb[SOLICITACAO_RESPOSTA]['MID_SOLICITACAO']="Solicita&ccedil;&atilde;o";
$tdb[SOLICITACAO_RESPOSTA]['USUARIO']="Usu&aacute;rio";
$tdb[SOLICITACAO_RESPOSTA]['DATA']="Data";
$tdb[SOLICITACAO_RESPOSTA]['TEXTO']="Resposta";

define("SOLICITACAO_SITUACAO_PRODUCAO", "solicitacao_situacao_producao");
$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESC']="Criticidade da Produ&ccedil;&atilde;o";
$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['dba']=0;
$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['NIVEL']="Nivel";

define("SOLICITACAO_SITUACAO_SEGURANCA", "solicitacao_situacao_seguranca");
$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESC']="Criticidade da Seguran&ccedil;a";
$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['dba']=0;
$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['NIVEL']="Nivel";

define("SOLUCAO", "solucao");
$tdb[SOLUCAO]['DESC']="Solu&ccedil;&atilde;o";
$tdb[SOLUCAO]['dba']=0;
$tdb[SOLUCAO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[SOLUCAO]['TEXTO']="Texto";
$tdb[SOLUCAO]['MID_EMPRESA']="Empresa";

define("TIPOS_SERVICOS", "tipos_servicos");
$tdb[TIPOS_SERVICOS]['DESC']="Tipos de Servi&ccedil;os";
$tdb[TIPOS_SERVICOS]['dba']=0;
$tdb[TIPOS_SERVICOS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[TIPOS_SERVICOS]['SPEED_CHECK']= "Speed check";
$tdb[TIPOS_SERVICOS]['MID_EMPRESA']="Empresa";
$tdb[TIPOS_SERVICOS]['TROCA']="Servi&ccedil;o de troca de moldes";
$tdb[TIPOS_SERVICOS]['MID_PLANO']="Plano";

define("TIPO_ROTAS", "tipo_rotas");
$tdb[TIPO_ROTAS]['DESC']="Tipos de Rotas";
$tdb[TIPO_ROTAS]['dba']=0;
$tdb[TIPO_ROTAS]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("TIPO_PLANOS", "tipo_planos");
$tdb[TIPO_PLANOS]['DESC']="Tipo";
$tdb[TIPO_PLANOS]['dba']=0;
$tdb[TIPO_PLANOS]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("TIPO_LUBRIFICANTE", "tipo_lubrificante");
$tdb[TIPO_LUBRIFICANTE]['DESC']="Tipo de Lubrificante";
$tdb[TIPO_LUBRIFICANTE]['dba']=0;
$tdb[TIPO_LUBRIFICANTE]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("PONTOS_LUBRIFICACAO", "pontos_lubrificacao");
$tdb[PONTOS_LUBRIFICACAO]['DESC']="Pontos de Lubrifica&ccedil;&atilde;o";
$tdb[PONTOS_LUBRIFICACAO]['dba']=0;
$tdb[PONTOS_LUBRIFICACAO]['MID_MAQUINA']="Objeto de Manuten&ccedil;&atilde;o";
$tdb[PONTOS_LUBRIFICACAO]['MID_CONJUNTO']="Posi&ccedil;&atilde;o";
$tdb[PONTOS_LUBRIFICACAO]['PONTO']="Ponto";
$tdb[PONTOS_LUBRIFICACAO]['NPONTO']="N&uacute;mero de Pontos";
$tdb[PONTOS_LUBRIFICACAO]['METODO']="M&eacute;todo";






define("TURNOS", "turnos");
$tdb[TURNOS]['DESC']="Turnos";
$tdb[TURNOS]['dba']=0;
$tdb[TURNOS]['DESCRICAO']="Turno";
$tdb[TURNOS]['MID_EMPRESA']="Empresa";

define("TURNOS_HORARIOS", "turnos_horarios");
$tdb[TURNOS_HORARIOS]['DESC']="Hor&aacute;rios";
$tdb[TURNOS_HORARIOS]['dba']=0;
$tdb[TURNOS_HORARIOS]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[TURNOS_HORARIOS]['HORA_INICIAL']="Hor&aacute;rio Inical";
$tdb[TURNOS_HORARIOS]['HORA_FINAL']="Hor&aacute;rio Final";
$tdb[TURNOS_HORARIOS]['HORA_TOTAL']="Horario Total";
$tdb[TURNOS_HORARIOS]['DIA']="Descri&ccedil;&atilde;o";
$tdb[TURNOS_HORARIOS]['MID_TURNO']="Turno";


define("USUARIOS", "usuarios");
$tdb[USUARIOS]['DESC']="Usu&aacute;rios do Sistema";
$tdb[USUARIOS]['dba']=0;
$tdb[USUARIOS]['NOME']="Nome";
$tdb[USUARIOS]['EMAIL']="E-mail";
$tdb[USUARIOS]['USUARIO']="Usu&aacute;rio";
$tdb[USUARIOS]['MID_FUNCIONARIO']="Funcion&aacute;rio";
$tdb[USUARIOS]['CELULAR']="Celular";
$tdb[USUARIOS]['SENHA']="Senha";
$tdb[USUARIOS]['TIPO']="Tipo";
$tdb[USUARIOS]['GRUPO']="Grupo";

define("USUARIOS_GRUPOS", "usuarios_grupos");
$tdb[USUARIOS_GRUPOS]['DESC']="Grupos de Usu&aacute;rios";
$tdb[USUARIOS_GRUPOS]['dba']=0;
$tdb[USUARIOS_GRUPOS]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("USUARIOS_ONLINE", "usuarios_online");
$tdb[USUARIOS_ONLINE]['DESC']="Usu&aacute;rios Online";
$tdb[USUARIOS_ONLINE]['dba']=0;
$tdb[USUARIOS_ONLINE]['NOME']="Nome";
$tdb[USUARIOS_ONLINE]['USUARIO']="Usu&aacute;rio";
$tdb[USUARIOS_ONLINE]['IP']="IP";
$tdb[USUARIOS_ONLINE]['TEMPO']="Tempo";

define("USUARIOS_PERMISSAO", "usuarios_permissao");
$tdb[USUARIOS_PERMISSAO]['DESC']="Permiss&atilde;o por Grupo";
$tdb[USUARIOS_PERMISSAO]['dba']=0;
$tdb[USUARIOS_PERMISSAO]['GRUPO']="Grupo";
$tdb[USUARIOS_PERMISSAO]['MODULO']="Modulo";
$tdb[USUARIOS_PERMISSAO]['PERMISSAO']="Permiss&atilde;o";

define("USUARIOS_PERMISSAO_TIPO", "usuarios_permissao_tipo");
$tdb[USUARIOS_PERMISSAO_TIPO]['DESC']="Tipos de Permiss&atilde;o";
$tdb[USUARIOS_PERMISSAO_TIPO]['dba']=0;
$tdb[USUARIOS_PERMISSAO_TIPO]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("USUARIOS_MODULOS", "usuarios_modulos");
$tdb[USUARIOS_MODULOS]['DESC']="M&oacute;dulos";
$tdb[USUARIOS_MODULOS]['dba']=0;
$tdb[USUARIOS_MODULOS]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("USUARIOS_PERMISAO_SOLICITACAO", "usuarios_permisao_solicitacao");
$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC']="Permiss&atilde;o por Empresa";
$tdb[USUARIOS_PERMISAO_SOLICITACAO]['dba']=0;
$tdb[USUARIOS_PERMISAO_SOLICITACAO]['USUARIO']="Usu&aacute;rio";
$tdb[USUARIOS_PERMISAO_SOLICITACAO]['MID_EMPRESA']="Empresa";

define("USUARIOS_PERMISAO_CENTRODECUSTO", "usuarios_permisao_centrodecusto");
$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC']="Solicita&ccedil;&atilde;o por Centro de Custo";
$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']=0;
$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['USUARIO']="Usu&aacute;rio";
$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['CENTRO_DE_CUSTO']="Centro de Custo";

define("INTEGRADOR", "integrador");
$tdb[INTEGRADOR]['DESC']="Log de Integra&ccedil;&atilde;o";
$tdb[INTEGRADOR]['dba']=0;
$tdb[INTEGRADOR]['TIPO']="Tipo";
$tdb[INTEGRADOR]['ESQUEMA']="Conte&uacute;do Esquema";
$tdb[INTEGRADOR]['ESQUEMA_ARQUIVO']="Esquema";
$tdb[INTEGRADOR]['ULTIMO_EVENTO']="&Uacute;ltima A&ccedil;&atilde;o";
$tdb[INTEGRADOR]['CSV']="Texto CSV";
$tdb[INTEGRADOR]['RESULTADO']="Registros Afetados";
$tdb[INTEGRADOR]['OBS']="Observa&ccedil;&otilde;es";

define("INTEGRACAO_TIPOS", "integracao_tipos");
$tdb[INTEGRACAO_TIPOS]['DESC']="Tipos de Integra&ccedil;&atilde;o";
$tdb[INTEGRACAO_TIPOS]['dba']=0;
$tdb[INTEGRACAO_TIPOS]['DESCRICAO']="Tipo";

define("INTEGRACAO_ESQUEMAS", "integracao_esquemas");
$tdb[INTEGRACAO_ESQUEMAS]['DESC']="Agendamento de Esquemas";
$tdb[INTEGRACAO_ESQUEMAS]['dba']=0;
$tdb[INTEGRACAO_ESQUEMAS]['ESQUEMA']="Esquema";
$tdb[INTEGRACAO_ESQUEMAS]['DATA_INICIO']="Data In&iacute;o";
$tdb[INTEGRACAO_ESQUEMAS]['ULTIMO_EVENTO']="&Uacute;ltima A&ccedil;&atilde;o";
$tdb[INTEGRACAO_ESQUEMAS]['STATUS']="Status";

define("ALMOXARIFADO", "almoxarifado");
$tdb[ALMOXARIFADO]['DESC']="Almoxarifados";
$tdb[ALMOXARIFADO]['dba']=0;
$tdb[ALMOXARIFADO]['MID_EMPRESA']="Empresa";
$tdb[ALMOXARIFADO]['COD']="C&oacute;digo";
$tdb[ALMOXARIFADO]['DESCRICAO']="Descri&ccedil;&atilde;o";

define("MATERIAIS_ALMOXARIFADO", "materiais_almoxarifado");
$tdb[MATERIAIS_ALMOXARIFADO]['DESC']="Estoque dos Almoxarifados";
$tdb[MATERIAIS_ALMOXARIFADO]['dba']=0;
$tdb[MATERIAIS_ALMOXARIFADO]['MID_MATERIAL']="Material";
$tdb[MATERIAIS_ALMOXARIFADO]['MID_ALMOXARIFADO']="Almoxarifado";
$tdb[MATERIAIS_ALMOXARIFADO]['ESTOQUE_ATUAL']="Estoque Atual";


define("SPEED_CHECK", "speed_check");
define("SPEED_CHECK_ATIVIDADES", "speed_check_atividades");
define("ORDEM_SPEED_CHECK", "ordem_speed_check");
define("ETAPAS_PLANO", "etapas_plano");


// TABELA ETAPAS DO PLANO
$tdb[ETAPAS_PLANO]['DESC']="Etapas do Plano";
$tdb[ETAPAS_PLANO]['dba']=0;
$tdb[ETAPAS_PLANO]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[ETAPAS_PLANO]['ORDEM']="Ordem";

// SPEED CHECK
$tdb[SPEED_CHECK]['DESC']="Plano de Checklist";
$tdb[SPEED_CHECK]['dba']=0;
$tdb[SPEED_CHECK]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[SPEED_CHECK]['OBSERVACAO']="Observa&ccedil;&atilde;o";
$tdb[SPEED_CHECK]['MID_EMPRESA']=$tdb[EMPRESAS]['DESC'];
$tdb[SPEED_CHECK]['MID_EQUIPE']="Equipe respons&aacute;vel";
$tdb[SPEED_CHECK]['MID_FAMILIA']="Fam&iacute;lia de Obj. Manut.";
$tdb[SPEED_CHECK]['MID_FAMILIA_COMPONENTE']="Fam&iacute;lia de Componente";
$tdb[SPEED_CHECK]['MID_TIPO_PLANO']="Aplica&ccedil;&atilde;o do plano";

// SPEED CHECK ATIVIDADES
$tdb[SPEED_CHECK_ATIVIDADES]['DESC']="Atividades checklist";
$tdb[SPEED_CHECK_ATIVIDADES]['dba']=0;
$tdb[SPEED_CHECK_ATIVIDADES]['NUMERO']="N&uacute;mero";
$tdb[SPEED_CHECK_ATIVIDADES]['DESCRICAO']="Descri&ccedil;&atilde;o";
$tdb[SPEED_CHECK_ATIVIDADES]['ETAPA']="Etapa";
$tdb[SPEED_CHECK_ATIVIDADES]['TEMPO_PREVISTO']="Tempo previsto";
$tdb[SPEED_CHECK_ATIVIDADES]['SPEED_CHECK_ID']="Plano de Checklist";

// ORDEM SPEED CHECK
$tdb[ORDEM_SPEED_CHECK]['dba'] = 0;
$tdb[ORDEM_SPEED_CHECK]['MID_ORDEM'] = 'Ordem de servi&ccedil;o';
$tdb[ORDEM_SPEED_CHECK]['MID_SPEED_ATV'] = 'Atividade de checkclist';
$tdb[ORDEM_SPEED_CHECK]['COMENTARIO'] = 'Coment&aacute;rio';
$tdb[ORDEM_SPEED_CHECK]['DIAGNOSTICO'] = 'Diagn&oacute;stico';
$tdb[ORDEM_SPEED_CHECK]['NUMERO'] = 'N&uacute;mero';
$tdb[ORDEM_SPEED_CHECK]['DESCRICAO'] = 'Descri&ccedil;&atilde;o';
$tdb[ORDEM_SPEED_CHECK]['ETAPA'] = 'Etapa';
$tdb[ORDEM_SPEED_CHECK]['TEMPO_PREVISTO'] = 'Tempo previsto';



// ------------------------------------------------------------------------
//                          ERROS VARIADOS
// ------------------------------------------------------------------------

// Banco de dados
$ling['bd01']="N&atilde;o foi poss&iacute;vel carregar o driver do banco de dados";
$ling['bd02']="N&atilde;o foi poss&iacute;vel conectar no banco de dados";
$ling['bd03']="MySQL";
$ling['bd04']="MSSQL";
$ling['bd05']="Oracle";

// Temas
$ling['tema01']="Imposs&iacute;vel continuar, arquivo de layout n&atilde;o pode ser carregado.";

// Modulos
$ling['mod01']="Imposs&iacute;vel continuar, arquivo de configura&ccedil;&atilde;o n&atilde;o pode ser carregado";

// Autentifica&ccedil;&atilde;o
$ling['autent01']="Imposs&iacute;vel continuar, arquivo de autentifica&ccedil;&atilde;o n&atilde;o pode ser carregado.";
$ling['autent02']="Usu&aacute;rio ou senha inv&aacute;lido.";
$ling['autent03']="Dados inv&aacute;lidos. Suas informa&ccedil;&otilde;es de sua conex&atilde;o, foram enviadas ao administrador";
$ling['autent04']="Sua sess&atilde;o foi fechada por estar inativa &agrave; muito tempo.";
$ling['autent05']="<strong>Sistema tempor&Aacute;riamente fora do ar devido a atualiza&Ccedil;&Otilde;es, duvidas ligar para o telefone <br> (41) 3282-7000, Obrigado.<strong>";

// Erros ao carregar arquivo
$ling['err01']="N&atilde;o foi poss&iacute;vel carregar um arquivo ou mais arquivos";
$ling['err02']="Parametros Inv&aacute;lidos ou Insuficientes";
$ling['err03']="Arquivo muito grande, tamanho m&aacute;ximo permitido &eacute;";
$ling['err04']="N&atilde;o &eacute; permitido anexar arquivos executaveis";
$ling['err05']="Foi imposs&iacute;vel mover o arquivo para a pasta";
$ling['err06']="Nenhum arquivo foi encontrado";
$ling['err07']="Esse componente n&atilde;o tem local definido";
$ling['err08']="A senha digitada n&atilde;o coincide";
$ling['err09']="Objeto n&atilde;o encontrado";
$ling['err10']="Erro: tabela n&atilde;o encontrada";
$ling['err11']="Erro ao enviar o e-mail.";
$ling['err12']="N&atilde;o foi poss&iacute;vel salvar o arquivo.";
$ling['err13']="N&atilde;o foi poss&iacute;vel salvar log no banco de dados";
$ling['err14']="Erro: N&atilde;o foi poss&iacute;vel abrir o modelo xml";
$ling['err15']="Erro: ao conectar com a base de dados.";
$ling['err16']="Erro: Usu&aacute;rio n&atilde;o permitido.";
$ling['err17']="Ocorreu um erro durante o processamento dessa consulta.";
$ling['err18']="Ocorreu um erro durante o processamento da coluna mestre.";
$ling['err19']="Erro: Layout n&atilde;o definido.";
$ling['err20']="Erro: Fun&ccedil;&atilde;o criaCod";
$ling['err21']="Erro: Voc&ecirc; precisa selecionar um Objeto de Manuten&ccedil;&atilde;o, para isso retorne ao apontamento da OS.";
$ling['err22']="Erro: Ao aprovar solicita&ccedil;&atilde;o.";
$ling['erro_alert_formselect']="Ocorreram erros durante o processo:\n";
$ling['erro_log']="Erro ao salvar o log do sistema.";
$ling['erro_os_nao_encontrada']="Nenhum registro encontrado com esse n&uacute;mero de OS.";


//Alertas
$ling['nao_possivel_editar_localizacao1'] = "Existe(m) %s objeto(s) cadastrado(s) nessa localiza&ccedil;&atilde;o 2, movimente-os antes de alterar a localiza&ccedil;&atilde;o 1.";
$ling['tente_novamente']="Tente novamente.";
$ling['indiq_camp_serem_salvo_usando_virgula']="Indique os campos a serem salvos usando o campo 'Campos', separe os nomes por v&iacute;rgula.";
$ling['tabela_ind_nao_existe']="A tabela indicada n&atilde;o existe, indique uma tabela v&aacute;lida.";
$ling['tab_v3_tem']="Tabelas que s&oacute; o V3 tem";
$ling['tab_tem']="Tabelas que s&oacute; o";



// ------------------------------------------------------------------------
//                          LEGENDAS DOS ICONES, FIELDSET's, BOTOES
// ------------------------------------------------------------------------
$ling['editar']="Editar";
$ling['copiar']="Copiar";
$ling['remover']="Remover";
$ling['novo']="Novo Cadastro";
$ling['relatorio']="Relat&oacute;rio";
$ling['detalha']="Detalhamento";
$ling['localizar']="Localizar";
$ling['personalizar']="Personalizar";
$ling['itens_por_pagina']="Itens por p&aacute;gina";
$ling['adicionar_campo']="Adicionar Campo";
$ling['opcoes']="Op&ccedil;&otilde;es";
$ling['paginas']="P&aacute;ginas";
$ling['remover_coluna']="Remover Coluna";
$ling['definir_ordem']="Clique para definir a ordem";
$ling['registros_encontrados']="Registros encontrados";
$ling['fechar']="Fechar";
$ling['minimizar']="Minimizar/Maximizar";
$ling['calendario']="Calend&aacute;rio";
$ling['atividades']="Atividades";
$ling['msgcopiar']="Essa op&ccedil;&atilde;o permite copiar toda estrutura de seu ".$tdb[MAQUINAS]['DESC']." para outro, ou para um novo objeto.";
$ling['fildset_local3']="Informa&ccedil;&otilde;es T&eacute;cnicas";
$ling['buscar']="Buscar";
$ling['cadastro_global'] = "CADASTRO GLOBAL";
$ling['select_opcao']="SELECIONE UMA OP&Ccedil;&atilde;O";

// ------------------------------------------------------------------------
//                            MODULO PRINCIPAL
// ------------------------------------------------------------------------

// Menu/Guia
$ling['prin00']="Principal";
$ling['prin01']="Resumo";
$ling['prin02']="Favoritos";
$ling['prin03']="Agenda";
$ling['prin04']="Integra&Ccedil;&Atilde;o";
$ling['prin05']="Adm. do Sistema";
$ling['prin06']="Registros";

$ling['admsis']="Administrac&atilde;o do Sistema";
$ling['usuarios']="Usu&aacute;rios do Sistema";
$ling['usuarios_grupos']="Grupos de Usu&aacute;rios";
$ling['permissao']="Definir Permiss&otilde;es";

// GRAFICOS RESUMO
$ling['prin_graf_col_mestre']="Nenhuma Coluna Mestre foi definida, favor use a op&ccedil;&atilde;o editar tabela para modificar suas informa&ccedil;&otilde;es";
$ling['prin_graf_col_dados']="Nenhuma Coluna de Dados foi definida, favor use a op&ccedil;&atilde;o editar tabela para modificar suas informa&ccedil;&otilde;es";
$ling['prin_graf_filtro']="filtro";
$ling['prin_graf_condicao']="condicao";
$ling['prin_graf_campo']="campo";
$ling['prin_graf_valor']="valor";
$ling['prin_graf_operador']="operador";
$ling['prin_graf_nenhum_dado']="Nenhum dado encontrado";

$ling['prin_mat_ponto_compra']="Materiais em ponto de compra";
$ling['prin_solicitacoes']="Solicita&ccedil;&otilde;es";
$ling['prin_orndes_atraso']="Ordens em atraso";
$ling['prin_rota']="Rota";




// ------------------------------------------------------------------------
//                            MODULO CADASTROOcorreu um erro durante o processamento da coluna mestre
// ------------------------------------------------------------------------

// Menu/Guia
$ling['cada00']="Cadastro";
$ling['cada01']="Estrutura Geral";
$ling['cada02']="M&atilde;o de Obra";
$ling['cada03']="Materiais";
$ling['cada04']="Tabelas";

// Estrutura
$ling['titulo_tabela']="Tabela";

$ling['visao_geral']="Vis&atilde;o Geral";
$ling['novo_emp']="Nova Empresa";
$ling['editar_emp']="Editar Empresa";
$ling['remover_emp']="Remover Empresa";
$ling['tabela_emp']="Tabela de Empresas";

$ling['novo_local1']="Nova Localiza&ccedil;&atilde;o 1";
$ling['editar_local1']="Editar Localiza&ccedil;&atilde;o 1";
$ling['remover_local1']="Remover Localiza&ccedil;&atilde;o 1";
$ling['tabela_local1']="Tabela da Localiza&ccedil;&atilde;o 1";

$ling['novo_local2']="Nova Localiza&ccedil;&atilde;o 2";
$ling['editar_local2']="Editar Localiza&ccedil;&atilde;o 2";
$ling['remover_local2']="Remover Localiza&ccedil;&atilde;o 2";
$ling['tabela_local2']="Tabela da Localiza&ccedil;&atilde;o 2";

$ling['novo_local3']="Novo Objeto de Manuten&ccedil;&atilde;o";
$ling['editar_local3']="Editar Objeto de Manuten&ccedil;&atilde;o";
$ling['remover_local3']="Remover Objeto de Manuten&ccedil;&atilde;o";
$ling['move_local3']="Mover Objeto de Manuten&ccedil;&atilde;o";
$ling['tabela_local3']="Tabela dos Objeto de Manuten&ccedil;&atilde;o";

$ling['inativo']="Inativo";
$ling['ativo']="Ativo";

$ling['tabela_preditiva']="Pontos de Monitoramento";
$ling['tabela_lubrificacao']="Pontos de Lubrifica&ccedil;&atilde;o";
$ling['tabela_contador']="Contadores";
$ling['anexar_arq']="Anexar Arquivos/Imagens";
$ling['anexo_titulo']="Arquivos/Imagens";
$ling['pecas_conj']="Pe&ccedil;as";

$ling['posicao_va']="POSI&Ccedil;&otilde;ES";
$ling['lub_va']="PONTOS DE LUBRIFICA&Ccedil;&atilde;O";
$ling['pred_va']="PONTOS PREDTIVA";
$ling['cont_va']="CONTADORES";

$ling['posicoes']="Posi&ccedil;&otilde;es";

$ling['tabela_equip']="Tabela ".$tdb[EQUIPAMENTOS]['DESC'];
$ling['move_equip']="Mover ".$tdb[EQUIPAMENTOS]['DESC'];
$ling['move_maq']="Mover ".$tdb[MAQUINAS]['DESC'];
$ling['editar_equip']="Editar ".$tdb[EQUIPAMENTOS]['DESC'];
$ling['pecas_equip']="Pe&ccedil;as";

$ling['selecionar_equip']="Selecione o Componente";
$ling['selecionar_maq']="Selecione a Objeto de Manuten&ccedil;&atilde;o";
$ling['selecionar_destino']="Selecione o Destino";
$ling['motivo']="Motivo";
$ling['equip_mover_sucess']="Componente Movimentado com Sucesso";
$ling['maq_mover_sucess']="Objeto de Manuten&ccedil;&atilde;o Movimentado com Sucesso";
$ling['maq_inativa']="Esse Objeto de Manuten&ccedil;&atilde;o esta inativo";
$ling['maq_nao_alocadas']="OBJETOS DE MANUTEN&Ccedil;&Atilde;O N&Atilde;O ALOCADOS";
$ling['equip_nao_alocadas']="COMPONENTES N&Atilde;O ALOCADOS";

$ling['maodeobra']="M&atilde;o de Obra";
$ling['funcionarios']="Funcion&aacute;rios";
$ling['prestador']="Prestador de Servi&ccedil;o";
$ling['turnos']="Turnos";
$ling['equipes']="Equipes";
$ling['ausencia']="Aus&ecirc;ncias";
$ling['ferias']="Afastamentos";

$ling['tabela_fam_maq']='Fam&iacute;lia de Objeto de Manuten&ccedil;&atilde;o';
$ling['tabela_cla_maq']='Objeto de Manuten&ccedil;&atilde;o';
$ling['tabela_ccusto']='Centro de Custo';
$ling['tabela_fam_equip']='Fam&iacute;lia de Componentes';
$ling['tabela_fam_mat']='Fam&iacute;lia de Materiais';
$ling['tabela_subfam_mat']='Sub-Fam&iacute;lia de Materiais';
$ling['tabela_unidade']='Unidades';
$ling['tabela_cont']='Tipos de Contadores';
$ling['tabela_esp']='Especialidades';
$ling['tabela_fer']='Dias n&atilde;o produtivos';
$ling['tabela_fab']='Fabricantes';
$ling['tabela_forn']='Fornecedores';
$ling['tabela_tipos_serv']='Tipos de Servi&ccedil;os';
$ling['tabela_natur_serv']='Natureza dos Servi&ccedil;os';
$ling['tabela_historico']='Hist&oacute;rico da Objeto de Manuten&ccedil;&atilde;o';
$ling['tabela_linha']='Controle de Linha de Produ&ccedil;&atilde;o';
$ling['tabela_causa']='Causa';
$ling['tabela_defeito']='Defeito';
$ling['tabela_solucao']='Solu&ccedil;&atilde;o';
$ling['tabela_sol_producao']=$tdb[SOLICITACAO_SITUACAO_PRODUCAO]['DESC'];
$ling['tabela_sol_seguranca']=$tdb[SOLICITACAO_SITUACAO_SEGURANCA]['DESC'];
$ling['confirma_remover']="DESEJA REALMENTE REMOVER ESSE ITEM?";

$ling['use_campo_pesquisa']="USE O CAMPO AO LADO PARA LOCALIZAR...";
$ling['pressione_enter']="Pressione a tecla Enter para realizar a busca";

// AQUI � UTILIZADO ACENTO PORQUE NO ALERT() DO JAVA SCRIPT ELE N�O RECONHECE A FORMATA��O HTML
$ling['limite_empresas']="A EMPRESA FOI CADASTRADA E O NUMERO M�XIMO DE EMPRESAS PERMITIDO NESTA LICEN�A FOI ATINGIDO.";
$ling['sel_pos']="Selecione uma Posi&ccedil;&atilde;o";

// Empresa Cadastrada Como Matriz.
$emp_mostra = "SEM MATRIZ CADASTRADA";



// ------------------------------------------------------------------------
//                            MODULO ESTOQUE
// ------------------------------------------------------------------------

// Menu/Guia
$ling['est01']="Materiais";
$ling['est02']="Movimenta&ccedil;&atilde;o";
$ling['est03']="Requisi&ccedil;&atilde;o";
$ling['est04']="Almoxarifado";


$ling['estoque_entrada']="Entrada de Materiais";
$ling['estoque_saida']="Sa&iacute;da de Materais";
$ling['mat_abaixo_minimo']="Materiais Abaixo do Estoque Minimo";
$ling['mat_acima_maximo']="Materiais Acima do Estoque M&aacute;ximo";

$ling['req_pendente']="Requisi&ccedil;&otilde;es Pendentes";
$ling['req_aceita']="Requisi&ccedil;&otilde;es Aceitas";
$ling['req_negada']="Requisi&ccedil;&otilde;es Rejeitadas";
$ling['req_resp']="Requisi&ccedil;&otilde;es Respondidas";


// ------------------------------------------------------------------------
//                            MODULO PLANEJAMENTO
// ------------------------------------------------------------------------

// Menu/Guia
$ling['plan00']="Planejamento";
$ling['plan01']="Preventiva";
$ling['plan02']="Rotas";
$ling['plan03']="Solicita&ccedil;&atilde;o";
$ling['plan04']="Programa&ccedil;&atilde;o";
$ling['plan05']="Cronograma";

$ling['plan_checklist']="Checklist";
$ling['checklist_programacao']="Programa&ccedil;&atilde;o do Checklist";
$ling['checklist_programacao_cancela']="Cancelar Programa&ccedil;&atilde;o do Checklist";
$ling['checklist_programacao_cancela_confirma']="TEM CERTEZA QUE DESEJA CANCELAR ESTA PROGRAMA&Ccedil;&Atilde;O?";
$ling['checklist_apontar']="Apontamento do Checklist";
$ling['checklist_pendencias']="Tarefa com Pend&ecirc;ncias";
$ling['checklist_aponta_abertas']="Apontamentos Abertos";
$ling['checklist_aponta_fechadas']="Apontamentos Fechados";

$ling['plano']="Plano";
$ling['copiar_plano']="Copiar Plano";
$ling['conf_copiar_plano']="DESEJA REALMENTE COPIAR ESSE PLANO?";
$ling['fim_copiar_plano']="Plano Copiado com sucesso!";
$ling['acessar_plano_copiado']="Clique aqui para acessar o item copiado";

$ling['rotas']="Planejamento das Rotas";
$ling['def_rota']="Rota";
$ling['inspecao']="Rotas de Inspe&ccedil;&atilde;o";
$ling['rotas_lub']="Rotas de Lubrifica&ccedil;&atilde;o";
$ling['rotas_pred']="Rotas de Monitoramento";
$ling['option_rotas']="Rotas";
$ling['option_prev']="Preventiva";
$ling['localizar_obj']="Localizar Objeto de Manuten&ccedil;&atilde;o";
$ling['localizar_rota']="Localizar Rotas";
$ling['localizar_obj_alt']="Use esse campo para buscar um objeto de manuten&ccedil;&atilde;o";

$ling['prog_recursos']="Aloca&ccedil;&atilde;o de Materiais e M&atilde;o de Obra.";
$ling['prog_contador']="Programa&ccedil;&atilde;o por Disparo";
$ling['prog_data']="Programa&ccedil;&atilde;o por Data";
$ling['prog_objeto']="Objeto de Manuten&ccedil;&atilde;o";
$ling['prog_maodeobra']="Alocar M&atilde;o de Obra";
$ling['prog_material']="Alocar Material";
$ling['detalha_prog']="Recursos da Prog.";
$ling['programacao_titulo']="Vis&atilde;o Geral das Programa&ccedil;&otilde;es";
$ling['programacao_titulo2']="Vis&atilde;o em Cronograma";
$ling['va_programacao']="Vis&atilde;o Geral";
$ling['tabela_programacao']="Vis&atilde;o Geral das Programa&ccedil;&otilde;es";
$ling['tabela_programacao2']="Vis&atilde;o em Cronograma";

$ling['sol_pendente']="Solicita&ccedil;&otilde;es Pendentes";
$ling['sol_aceita']="Solicita&ccedil;&otilde;es Aceitas";
$ling['sol_negada']="Solicita&ccedil;&otilde;es Rejeitadas";
$ling['sol_resp']="Solicita&ccedil;&otilde;es Respondidas";

$ling['prog_ok']="PROGRAMA&Ccedil;&Atilde;O CONCLU&Iacute;DA COM SUCESSO";
$ling['tipo_prog']="Tipo de Programa&ccedil;&atilde;o";
$ling['tipo']="Tipo";
$ling['datas']="Per&iacute;odo";
$ling['data_inicio']="Data In&iacute;cio";
$ling['data_fim']="Data Final";
$ling['data_aponta']="Data de Apontamento";
$ling['horario']="Hor&aacute;rio";
$ling['hora_abre']="Hora de Abertura";
$ling['aloca_maodeobra']="Aloca&ccedil;&atilde;o de M&atilde;o de Obra";
$ling['aloca_material']="Aloca&ccedil;&atilde;o de Materiais";
$ling['tempo']="Tempo";
$ling['tempo_h']="Tempo (h)";
$ling['prog_material']="Material";
$ling['qtd']="Quantidade";
$ling['adicionar']="Adicionar";
$ling['tipo_plano']="Usar qual tipo de plano?";
$ling['selecione']="Selecione";
$ling['plano']="Plano";
$ling['plano_padrao']="Plano Padr&atilde;o";
$ling['checklist']="Checklist";
$ling['prog_sel_obj']="Selecione o(s) Objetos de Manuten&ccedil;&atilde;o / Componentes";
$ling['prog_env_form']="Gerar Programa&ccedil;&atilde;o";
$ling['data_inicial_invalida']="Data Inicial Inv&aacute;lida";
$ling['data_prog_invalida']="Data programada inv&aacute;lida";
$ling['data_invalida']="Data Inv&aacute;lida";
$ling['data_final_invalida']="Data Final Inv&aacute;lida";

$ling['atividade'] = "Instru&ccedil;&atilde;o N&ordm;";
$ling['observacoes'] = "Observa&ccedil;&otilde;es";

$ling['destino']="Destino da Manuten&ccedil;&atilde;o";
$ling['destino2']="Destino da Manuten&ccedil;&atilde;o"; // sem htmlentities

// Filtro programa��o
$ling['procurar_por'] = "procure pelo codigo ou descri&ccedil;&atilde;o...";

// preditiva
$ling['preditiva']="Monitoramento";
$ling['pontos_preditiva']="Pontos de Monitoramento";
$ling['novo_lanc_preditiva']="Novo apontamento";
$ling['preditiva_aponta']="Apontamentos de Monitoramento";
$ling['pontos_preditiva_aponta']="Apontamento: Pontos de Monitoramento";


$ling['lanc_pontos_preditiva_aponta']="LAN&ccedil;AMENTO DA ANALISE PREDITIVA";
$ling['plano_aplicacao']="Aplica&ccedil;&atilde;o do Plano";
$ling['plano_info']="Informa&ccedil;&otilde;es do Plano";
$ling['plano_peri']="Periodicidade / Contador";
$ling['plano_prev_mo']="Previs&atilde;o de M&atilde;o-de-obra";
$ling['plano_prev_mat']="Previs&atilde;o de Material";
$ling['plano_ponto']="Ponto";
$ling['prog_ajuste']="Ajuste da Programa&ccedil;&atilde;o";
$ling['plano_atv_lub']="Nova Ativ. de Lubrifica&ccedil;&atilde;o";
$ling['plano_atv_insp']="Nova Ativ. de Inspe&ccedil;&atilde;o";

// Msgs da guia estrutura

// ------------------------------------------------------------------------
//                            MODULO ORDEM
// ------------------------------------------------------------------------

// Menu/Guia
$ling['ord00']="Ordens";
$ling['ord01']="Carteira de Servi&ccedil;os";
$ling['ord02']="Servi&ccedil;os Emergenciais";
$ling['imprimir_ordens']="Imprimir Ordens";
$ling['imprimir_ordens_abertas']="Imprimir Ordens Abertas";

$ling['carteira_servico']="Carteira de Servi&ccedil;os";
$ling['notas_pendencias']="Notas & Pend&ecirc;ncias";
$ling['imprimir_ordens']="Impress&atilde;o de O.S.";

$ling['os_abertas']="OS Abertas";
$ling['ordens_abertas']="Ordens Abertas";
$ling['ordens_fechadas']="Ordens Fechadas";
$ling['ordens_canceladas']="Ordens Canceladas";
$ling['ordens_programadas']="Ordens Programadas";
$ling['sol_criticidade']="Criticidade";

$ling['ord_cancelamento']="Cancelamento de OS.";
$ling['ord_reaberta']="Re-abertura de OS.";
$ling['ord_resp']="Atribuindo responsavel pela OS.";
$ling['ord_rel']="Reprogramando OS.";
$lind['ord_lista_semana'] = "Mostrar programa&ccedil;&atilde;o semanal?";
$ling['ord_ap']="Apontamentos da Ordem";
$ling['ord_serv']="Servi&ccedil;o";
$ling['ord_reprogramar']="Reprogramar ordens selecionadas para:";
$ling['ord_reprogramar2']="Reprogramar";
$ling['ord_atribui']="Atribuir ordens selecionadas ao respons&aacute;vel";
$ling['orndes']="Ordens de Servi&ccedil;o";
$ling['ord_abrir']="Abrir Nova Ordem";

$ling['programacao_mo']="Programa&ccedil;&atilde;o de M&atilde;o-de-obra";

// ------------------------------------------------------------------------
//                            MODULO RELATORIOS
// ------------------------------------------------------------------------

// Menu/Guia
$ling['re00']="Relat&oacute;rios";
$ling['campos_para_relatorio']="Escolha os campos que ser&atilde;o usados nesse relat&oacute;rio";
$ling['ordernar']="Ordenar por";
$ling['qto_itens']="Quantidade de registros";
$ling['sem_criterios']="Sem Crit&eacute;rios";
$ling['criterio']="Crit&eacute;rio";
$ling['igual']="Igual";
$ling['diferente']="Diferente";
$ling['maior']="Menor";
$ling['menor']="Menor";
$ling['contem']="Cont&eacute;m";
$ling['filtros']="Defina os filtros";
$ling['filtro']="Filtro";
$ling['filtros']="Filtros";
$ling['papel_orientacao']="Orienta&ccedil;&atilde;o da folha(V&aacute;lido somente para relat&oacute;rios no Word)";
$ling['papel_paisagem']="Paisagem";
$ling['papel_retrato']="Retrato";
$ling['relatorio_html']="Gerar Relat&oacute;rio no Navegador";
$ling['relatorio_doc']="Gerar Relat&oacute;rio no MS Word";
$ling['sem_filtro']="Sem Filtro";
$ling['todos']="Todos";
$ling['erro_nenhum_campo']="Nenhum campo foi selecionado. Use a op&ccedil;&atilde;o voltar de seu navegador e tente novamente";

$ling['relatorio_local1']="Relat&oacute;rio de Localiza&ccedil;&atilde;o 1";
$ling['relatorio_local2']="Relat&oacute;rio de Localiza&ccedil;&atilde;o 2";
$ling['relatorio_obj_man']="Relat&oacute;rio de Objeto de Manuten&ccedil;&atilde;o";
$ling['relatorio_equip']="Relat&oacute;rio de Componentes";

$ling['mov_equip']="Movimenta&ccedil;&atilde;o de Componentes";
$ling['mov_obj_man']="Movimenta&ccedil;&atilde;o de Objeto de Manuten&ccedil;&atilde;o";

$ling['relatorio_fam_maq']="Relat&oacute;rio de Fam&iacute;lia de Objeto de Manuten&ccedil;&atilde;o";
$ling['relatorio_cla_maq']="Relat&oacute;rio de Objeto de Manuten&ccedil;&atilde;o";
$ling['relatorio_ccusto']="Relat&oacute;rio de Centro de Custo";
$ling['relatorio_fam_equip']="Relat&oacute;rio de Fam&iacute;lia de Componentes";
$ling['relatorio_fam_mat']="Relat&oacute;rio de Fam&iacute;lia de Materiais";
$ling['relatorio_unidade']="Relat&oacute;rio de Unidade";
$ling['relatorio_tipos_contadores']="Relat&oacute;rio de Tipo de Contadores";
$ling['relatorio_especialidades']="Relat&oacute;rio de Especialidades";
$ling['relatorio_feriados']="Relat&oacute;rio de Dias N&atilde, Produtivos";
$ling['relatorio_fabricantes']="Relat&oacute;rio de Fabricantes";
$ling['relatorio_fornecedores']="Relat&oacute;rio de Fornecedores";
$ling['relatorio_tipo_serv']="Relat&oacute;rio de Tipos de Servi&ccedil;os";
$ling['relatorio_natur_serv']="Relat&oacute;rio de Natureza dos Servi&ccedil;os";
$ling['relatorio_causa']="Relat&oacute;rio de Causa";
$ling['relatorio_defeito']="Relat&oacute;rio de Defeito";
$ling['relatorio_solucao']="Relat&oacute;rio de Solu&ccedil;&atilde;o";
$ling['relatorio_estrutural']="Relat&oacute;rio da Estrutura Geral";
$ling['relatorio_album_img']="Relat&oacute;rio de Imagens dos Objetos de Manuten&ccedil;&atilde;o";
$ling['relatorio_materias_preco_almoxarifado1'] = 'Pre&ccedil;o unit&aacute;rio ultima compra por almoxarifado.';
$ling['relatorio_materias_preco_almoxarifado2'] = 'Pre&ccedil;o unit&aacute;rio da ultima compra.';

$ling['rel_lanca_contador']="Relat&oacute;rio de Lan&ccedil;amento de Contadores";
$ling['rel_carteira_serv']="Relat&oacute;rio da Carteira de Servi&ccedil;o";
$ling['rel_custo_os']="Relat&oacute;rio de Custo de O.S.";
$ling['rel_geral_os']="Relat&oacute;rio Geral de O.S.";
$ling['rel_mao_de_obra']="Relat&oacute;rio de M&atilde;o de Obra";
$ling['rel_programacao']="Relat&oacute;rio de Programa&ccedil;&atilde;o";
$ling['rel_planos']="Relat&oacute;rio - Planos";
$ling['rel_planos_rota']="Relat&oacute;rio - Planos de Rota";
$ling['rel_localizacao_materiais']="Relat&oacute;rio Localiza&ccedil;&atilde;o de Materiais";
$ling['rel_localizacao_pecas']="Relat&oacute;rio de Materiais por Localiza&ccedil;&atilde;o";
$ling['rel_ponto_compra_mat']="Relat&oacute;rio de Ponto de Compra para Materiais";
$ling['rel_monitoramento']="Relat&oacute;rio de Monitoramento";
$ling['relatorio_geral_os']="Relat&oacute;rio Geral de O.S";
$ling['relat_mao_obra']="Relat&oacute;rio M&atilde;o de Obra";
$ling['relatorio_progre']="Relat&oacute;rio de Programa&ccedil;&atilde;o";

$ling['cronograma_diario_func']="Cronograma Di&aacute;rio dos Funcion&aacute;rios";
$ling['cronograma_preven_anual']="Cronograma Preventivo Anual";
$ling['crono_rota_anual']=" Cronograma de Rotas Anual";

$ling['tempoexec']="Tempo aproximado de execu&ccedil;&atilde;o";


// ------------------------------------------------------------------------

// Formul&aacute;rios
$ling['form01']="Esse &eacute; obrigatorio para conclus&atilde;o do registro";
$ling['form02']="J&aacute; existem um valor id&ecirc;ntico a esse registrado";
$ling['nao_e_numero']="O valor digitado n&atilde;o &eacute; um n&uacute;mero!";
$ling['confirmar_senha']="Confirme";
$ling['permissao1']="Administrador";
$ling['permissao2']="Supervisor";
$ling['permissao3']="Usu&aacute;rio";

// --------------------------------------------------------------------------
// NOMES DOS MESES E DIAS DA SEMANA
// --------------------------------------------------------------------------

$ling['dias'] = "Dias";
$ling['mes_label'] = "M&ecirc;s";
$ling['ano'] = "Ano";

// MESES
$ling_meses[1] = "Janeiro";
$ling_meses[2] = "Fevereiro";
$ling_meses[3] = "Mar&ccedil;o";
$ling_meses[4] = "Abril";
$ling_meses[5] = "Maio";
$ling_meses[6] = "Junho";
$ling_meses[7] = "Julho";
$ling_meses[8] = "Agosto";
$ling_meses[9] = "Setembro";
$ling_meses[10] = "Outubro";
$ling_meses[11] = "Novembro";
$ling_meses[12] = "Dezembro";

$ling['mes'][1]='Janeiro';
$ling['mes'][2]='Fevereiro';
$ling['mes'][3]='Mar&ccedil;o';
$ling['mes'][4]='Abril';
$ling['mes'][5]='Maio';
$ling['mes'][6]='Junho';
$ling['mes'][7]='Julho';
$ling['mes'][8]='Agosto';
$ling['mes'][9]='Setembro';
$ling['mes'][10]='Outubro';
$ling['mes'][11]='Novembro';
$ling['mes'][12]='Dezembro';

$ling['mes_parc'][1]='Jan';
$ling['mes_parc'][2]='Fev';
$ling['mes_parc'][3]='Mar';
$ling['mes_parc'][4]='Abr';
$ling['mes_parc'][5]='Mai';
$ling['mes_parc'][6]='Jun';
$ling['mes_parc'][7]='Jul';
$ling['mes_parc'][8]='Ago';
$ling['mes_parc'][9]='Set';
$ling['mes_parc'][10]='Out';
$ling['mes_parc'][11]='Nov';
$ling['mes_parc'][12]='Dez';


$ling['semana'][1]='Dom';
$ling['semana'][2]='Seg';
$ling['semana'][3]='Ter';
$ling['semana'][4]='Qua';
$ling['semana'][5]='Qui';
$ling['semana'][6]='Sex';
$ling['semana'][7]='Sab';

$ling['semana_desc']="Semana";

// ---------------------------------------------------------------------------
// TEXTOS
// ---------------------------------------------------------------------------
// Texto do Modulo Principal
$ling['permissao_sol']='Permiss&atilde;o por Empresa';
$ling['permissao_cc']='Permiss&atilde;o por Centro de Custo';
$ling['adicionar_evento']="Adicionar Evento";
$ling['adicionar_evento_peri']="Adicionar Evento Peri&oacute;dico";
$ling['agenda_compromissos']="Agenda de Compromissos";
$ling['ver_evento']="Ver Evento";
$ling['eventos_periodicos']="Eventos periodicos";
$ling['adicionar_evento_dia']="Adicionar evento neste dia";
$ling['adicionar_evento_para_dia']="Adicionar evento para o dia";
$ling['preencha_a_data_corretamente']="Preencha a data corretamente";
$ling['preencha_a_hora_corretamente']="Preencha a hora corretamente";
$ling['preencha_um_titulo']="Preencha um t&iacute;tulo";
$ling['agenda_titulo']="T&iacute;tulo";

$ling['esq_importar'] = "Esquemas de Importa&ccedil;&atilde;o";
$ling['esq_exportar'] = "Esquemas de Exporta&ccedil;&atilde;o";
$ling['log_integrar'] = "Log de Integra&ccedil;&atilde;o";
$ling['agen_integracao'] = "Agendamento";
$ling['selecione_grupo'] = "Selecione um grupo";

$ling['dados_ult_dias'] = "Dados dos �ltimos 30 dias";


// Texto do Modulos/Cadastro
$ling['deseja_copiar_equip']='DESEJA REALMENTE COPIAR ESSE COMPONENTE?';
$ling['objeto_copiado']='Objeto Copiado com sucesso!';
$ling['clique_acessar_item']='Clique aqui para acessar o item copiado';
$ling['cadastrar']='Cadastrar';
$ling['deseja_remover_turno']='DESEJA REALMENTE REMOVER ESSE TURNO?';
$ling['horario_inicial']='Horario Inicial';
$ling['horario_final']='Horario Final';
$ling['horario_total']='Horario Total';
$ling['atualizar']='Atualizar';
$ling['movimentar']='Movimentar';
$ling['todas_empresas']="Todas as Empresas";
$ling['empresas_cadastradas']="N&Atilde;O EXISTEM EMPRESAS CADASTRADAS";
$ling['definir_prog']="Clique para definir a programa&ccedil;&atilde;o";
$ling['prin_sair']="Sair";
$ling['conf_manusis']="Configura&ccedil;&otilde;es do Manusis";
$ling['selecionar_equip']="Selecione o Componente";
$ling['equip_mover_sucess']="Componente Movimentado com Sucesso";
$ling['tabela_fam_equip']='Familia de Componentes';
$ling['tabela_fer']='Dias N&atilde;o Produtivos';
$ling['estoque']="Estoque";
$ling['os']="OS";

// roteiro
$ling['roteiro']="Roteiro";
$ling['def_roteiro']="Definir Roteiro";
$ling['roteiro_mover']="Mover";
$ling['roteiro_posicao']="Ordena&ccedil;&atilde;o";
$ling['maior']="Maior";
$ling['relatorio_equip']="Relatorio de Componentes";
$ling['mov_equip']="Movimenta&ccedil;&atilde;o de Componentes";
$ling['relatorio_fam_equip']="Relatorio de Familia de Componentes";
$ling['relatorio_feriados']="Relatorio de Dias N&atilde;o Produtivos";
$ling['atividade_f'] = "Figura N&ordm;";
$ling['atividade_n'] = "Atividade N&ordm;";
$ling['agrupar_por']="Agrupar por";
$ling['nenhum']="Nenhum";

$ling['backup']="Backup do Banco de Dados";
$ling['faz_backup']="Fazer Backup do Banco de Dados";
$ling['restaura_backup']="Restaurar Backup do Banco de Dados";

$ling['previsto_vs_executado'] = 'Previsto x Executado';
$ling['setando_contador_inicial'] = "Setando o contador inicial";
$ling['log_ultimas_alteracao'] = "&Uacute;timas altera&ccedil;&otilde;es na tabela";

// DATA E HORA 
$ling['data_hora']="Data/hora";
$ling['data_inicial_maior_data_final']="A data inicial &eacute; maior que a data final";
$ling['hora_inicial_e_invalida']="A hora inicial &eacute; inv&aacute;lida";
$ling['hora_final_e_invalida']="A hora final &eacute; inv&aacute;lida";
$ling['hora_invalida']="A hora &eacute; inv&aacute;lida";
$ling['data']="Data";
$ling['data_ab_inva']="Data de abertura inv�lida";
$ling['data_ab_nao_pod_ser_maior_q_data_prog']="A data de abertura n&atilde;o pode ser maior que a data programada.";
$ling['hora_abertura']="Hora de Abertura:";
$ling['hora_ini']="Hora Inic&iacute;o";
$ling['data_inicial']="Data Inicial";
$ling['hora_final']="Hora Final";
$ling['DATA_DE_ABERTURA']="DATA DE ABERTURA";
$ling['data_de_termino']="DATA DE TERMINO";
$ling['hora_inicio']="Hora In&iacute;cio";
$ling['DATA_INICIO_M']="DATA IN&Iacute;CIO";
$ling['DATA_FINAL_M']="DATA FINAL";
$ling['HORA_INICIO_M']="HORA IN&Iacute;CIO";
$ling['HORA_FINAL_M']="HORA FINAL";
$ling['data_preenche']="Preencha uma data";
$ling['data_formato_invalida']="Formato inv&aacute;lido para a data";
$ling['a_data']="A data";
$ling['a_data_invalida']="&eacute; inv&aacute;lida";
$ling['data_abre']="Data Abre.";
$ling['data_abertura']="Data Abertura";
$ling['data_programada']="Data Programada";
$ling['data_reprog']="DATA REPROG.";

// UTILIZADO EM DIVERSOS LUGARES (GEN�RICOS)
$ling['obj']="Objeto";
$ling['assunto']= "Assunto";
$ling['necessidade']= "Necessidade";
$ling['dados_sol']= "Dados Solicitante";
$ling['telefone']= "Telefone";
$ling['email']= "E-Mail";
$ling['resp_mot']="Resposta/Motivo";
$ling['resp']="Respostas";
$ling['Nordem']="N&ordm;";
$ling['informacoes_gerais']="Informa&ccedil;&eth;es Gerais";
$ling['tipo_do_servico']="Tipo do Servi&ccedil;o:";
$ling['Natureza']="Natureza";
$ling['natureza_m']="NATUREZA";
$ling['solicitante']="Solicitante";
$ling['responsavel']="Respons&aacute;vel:";
$ling['observacoes']="Observa&ccedil;&eth;es";
$ling['atividade_a']="Atividade";
$ling['funcionario']="Funcion&aacute;rio:";
$ling['FUNCIONARIO_M']="FUNCION&Aacute;RIO";
$ling['nome']="Nome";
$ling['equipe']="Equipe";
$ling['EQUIPE_M']="EQUIPE";
$ling['TEMPO_PARADA_OBJ_MANUTENCAO']="TEMPO DE PARADA DO OBJETO DE MANUTEN&Ccedil;&Atilde;O";
$ling['OBJ_MANUTENCAO']="OBJETO DE MANUTEN&Ccedil;&Atilde;O";
$ling['filtrar']= "Filtrar";
$ling['atribuir']="Atribuir";
$ling['serv_fechado']="Servi&ccedil;os Fechados";
$ling['sistema_busca_somente_ordens_realizada_prog']="Esse sistema busca pelo conte&uacute;do de servi&ccedil;os(Ordens de Servi&ccedil;o) realizados ou programados, apresentando-os em ordem decrescente pela data de programa&ccedil;&atilde;o e/ou abertura. Voc&ecirc; tamb&eacute;m pode obter resultado mais precisos usando a op&ccedil;&atilde;o 'Filtro' logo abaixo.";
$ling['TAMANHO_M']="TAMANHO";
$ling['MAO_DE_OBRA_APONTA']="M&Atilde;O DE OBRA APONTADA";
$ling['campo_e_numerico']=" O campo &eacute; num&eacute;rico";
$ling['tipo_servicos']="Tipo de Servi&ccedil;os";
$ling['somente_sistematico']="Somente Servi&ccedil;os Sistem&aacute;tico";
$ling['somente_nao_sistematico']="Somente Servi&ccedil;os n&atilde;o Sistem&aacute;tico";
$ling['anexos']="Anexos";
$ling['anexos_m']="ANEXOS";
$ling['NADA_ALOCADO']="NADA FOI ALOCADO";
$ling['lanca_contadores']="Lan&ccedil;amento de Contadores";
$ling['sem_permissao']="Usu&aacute;rio sem permiss&atilde;o para acessar essa fun&ccedil;&atilde;o.";
$ling['dados_invalidos']="Dados inv&aacute;lidos";
$ling['valor']="Valor";
$ling['falta_para_zerar']="Falta para Zerar";
$ling['acomp_dispa']="Acompanhamento dos Disparos";
$ling['disparo_em']="Disparo em";
$ling['Qtd_disparos']="Qtd. de Disparos";
$ling['valor_atual']="Valor Atual";
$ling['falta_para_disparo']="Falta para Disparo";
$ling['ordem']="Ordem";
$ling['IP_']="IP";
$ling['Hora_']="Hora";
$ling['Dados_']="Dados";
$ling['erro_durante_proc_prog_']="Ocorreu um erro durante o processamento dessa programa&ccedil;&atilde;o. Favor use a op&ccedil;&atilde;o voltar e verifique suas informa&ccedil;&otilde;es. ";
$ling['gravando_ordem']="Aguarde, gerando Ordens de Servi&ccedil;o";
$ling['obrigatorio']="&eacute; obrigat&oacute;rio";
$ling['tarefa_']="Tarefa";
$ling['especialidade_']="Especialidade";
$ling['periodicidade_']="Periodicidade";
$ling['preen_campos']="Preencha todos os campos";
$ling['Qtde']="Qtde";
$ling['preencha_o_campo_p']="Preencha o campo Prazo.";
$ling['preencha_corretamente_campo_p']="Preencha corretamente o campo Prazo";
$ling['preencha_corretamente']="Preencha corretamente o campo";
$ling['recompensacoes']="Recomenda&ccedil;&otilde;es";
$ling['recompensacoes_m']="RECOMENDA&Ccedil;&Otilde;ES";
$ling['cadastros']="Cadastros";
$ling['graficos']="Gr&aacute;ficos";
$ling['ordens_serv_abertas_obj_atulizadas']="As ordens de servi&ccedil;o abertas para esse objeto, foram atualizadas.";
$ling['campo_motivo_obrigatori']="Campo Motivo &eacute; obrigat&oacute;rio";

$ling['campo_obrigatorio']="Campo obrigat&oacute;rio";

$ling['ATUALIZANDO_PROG_']="ATUALIZANDO PROGRAMA&Ccedil;&Otilde;ES";
$ling['PLANO_M']="PLANO";
$ling['data_ultima_os']="DATA DA ULTIMA OS";
$ling['ROTA_M']="ROTA";
$ling['ordens_criadas']="ORDENS CRIADAS";
$ling['ordens_alteradas']="ORDENS ALTERADAS";
$ling['ordens_removidas']="ORDENS REMOVIDAS";
$ling['aguarde_em_processamento']="AGUARDE, EM PROCESSAMENTO!!!";
$ling['condicao']="Condi&ccedil;&atilde;o";
$ling['inclui_os_titu_campos']="Incluir primeira linha com os t&iacute;tulos dos campos";
$ling['criterios_def']="Defina os crit&eacute;rios";
$ling['sepa_casa_decimal']="Separador de casa decimal";
$ling['dest_dos_dados']="Destino dos dados";
$ling['escolha_um_mode_ou_crie_novo']="Escolha um modelo ao lado, ou crie um novo";
$ling['doc_graf_criado']="Documento do Gr&aacute;fico criado com sucesso! ";
$ling['erro_grava_doc']="Erro ao gravar o documento, favor tentar novamente em alguns instantes";
$ling['anexar_graf']="Anexar Gr&aacute;fico";
$ling['novo_graf']="Novo Gr&aacute;fico";
$ling['ussuario_sem_permissao_graf']="Usu&aacute;rio sem permiss&atilde;o para acessar o Assistente de Gr&aacute;ficos";
$ling['nome_do_arq']="Nome do Arquivo (N&atilde;o use acentos)";
$ling['titu_graf']="Titulo do Gr&aacute;fico";
$ling['grupo_graf']="Grupo do Gr&aacute;fico";
$ling['abrir_grafi']="Abrir Gr&aacute;fico";
$ling['direto_nao_foi_conf']="Esse diret&oacute;rio n&atilde;o foi configurado corretamente";
$ling['parametros']="Par&acirc;metros";
$ling['operador']="Operador";
$ling['campo_']="Campo";
$ling['imp_abrir_XML']="Imposs&iacute;vel abrir o arquivo XML";
$ling['ponto_compra_mat']="Ponto de Compra para Materiais";
$ling['comparativo_defeito']="Comparativo de Defeitos";
$ling['comparativo_de_ser']="Comparativo de Servi&ccedil;os";
$ling['temp_m_reparo']="Tempo M&eacute;dio de Reparo";
$ling['comp_serv_sistem']="Comparativo de Servi&ccedil;os N&atilde;o-Sistem&aacute;ticos";
$ling['comp_func']="Comparativo de Funcion&aacute;rios";
$ling['compa_corre_x_preven']="Comparativo Corretiva x Preventiva";
$ling['compa_pla_x_exe']="Comparativo Planejado x Executado";
$ling['compa_desem_ope']="Comparativo de Desempenho Operacional";
$ling['total_horas']="TOTAL (HORAS)";
$ling['OS']="OS";
$ling['origem_prog']="Origem da Programa&ccedil;&atilde;o";
$ling['localizacoes']="Localiza&ccedil;&otilde;es";
$ling['localizacao_2']="Localiza&ccedil;&atilde;o 2";
$ling['ORDEM_FECHADA']="ORDEM FECHADA";
$ling['def_ponto']="Defini&ccedil;&atilde;o do Ponto";
$ling['legenda']="LEGENDA";

// MANIPULA��O DE ARQUIVOS
$ling['arq_estrutura_nao_pode_ser_carregado']="Imposs&iacute;vel continuar, arquivo de estrutura n&atilde;o pode ser carregado.";
$ling['arq_configuracao_nao_pode_ser_carregado']="Imposs&iacute;vel continuar, arquivo de configura&ccedil;&atilde;o n&atilde;o pode ser carregado.";
$ling['arq_idioma_nao_pode_ser_carregado']="Imposs&iacute;vel continuar, arquivo de idioma n&atilde;o pode ser carregado.";
$ling['arq_disco_loca']="Arquivo em disco local";
$ling['arq_diretorio']="Arquivo (com diret&oacute;rio)";
$ling['arq_inexistente']="Arquivo local: O arquivo n&atilde;o existe";
$ling['arq_vazio']="Arquivo local: O arquivo est&aacute; vazio ";
$ling['arq_nome']="Nome do Arquivo";


$ling['erro_upload_arq']="Ocorreu um erro no Upload do arquivo.";
$ling['ARQUIVO_M']="ARQUIVO";

// ESTOQUE
$ling['almoxarifado']="Almoxarifado";
$ling['ALMOXARIFADO_M']="ALMOXARIFADO";
$ling['MATERIAL']="MATERIAL";
$ling['QUANTIDADE']="QUANTIDADE";
$ling['CUSTO_UNITARIO_RS']="CUSTO UNIT&Aacute;RIO (R$)";
$ling['CUSTO_TOTAL_RS']="CUSTO TOTAL (R$)";
$ling['CUSTO_M']="CUSTO";
$ling['SOMA_TOTAL']="SOMA TOTAL:";

// PROGRAMA��O, PLANOS DE MANUTEN��O E ATIVIDADES
$ling['total_de_atualizados']="Total de atualizados.";
$ling['total_de_no_atualizados']="Total de n&atilde;o atualizados:";
$ling['diferencas_encont']="Diferen&ccedil;as encontradas";
$ling['ATUALIZANDO_PROG']="ATUALIZANDO PROGRAMA&Ccedil;&Atilde;O";
$ling['PROG_ATUALI_COM_SUCESSO']="PROGRAMA&Ccedil;&Otilde;ES ATUALIZADAS COM SUCESSO!!!";
$ling['busca_histo_servi']="Buscar no Hist&oacute;rico de Servi&ccedil;os";
$ling['ativi_preventiva']="Atividades da Preventiva";
$ling['ativi_de_rota']="Atividades de Rota";
$ling['plano_preventiva']="Plano Preventiva";
$ling['plano_rotas']="Plano Rotas";
$ling['TEMPO_M']="TEMPO";
$ling['REPROGRAMACOES']="REPROGRAMA&Ccedil;&Otilde;ES";
$ling['I_M']="I.M.";
$ling['PARTE_POSICAO']="PARTE/POSI&Ccedil;&Atilde;O";
$ling['ESP']="ESP.";
$ling['TEMPO_PREVISTO_M']="TEMPO PREVISTO";
$ling['tempo_previsto_']="Tempo Previsto";
$ling['DIAGNOSTICO']="DIAGN&Oacute;STICO";
$ling['diagnostico_m']="Diagn&oacute;stico";
$ling['N']="N&ordm;";
$ling['POSICAO_M']="POSI&Ccedil;&Atilde;O";
$ling['volta_de_setup_para_nao_alocado_nao_prmitido'] = "Para este tipo de servi&ccedil;o &eacute; necess&aacute;rio selecionar um objeto de destino.";
// SOLICITA��ES, ORDENS, APONTAMENTOS E PEND�NCIAS
$ling['funcionario_ja_possui_apont_neste_periodo']="Esse Funcion&aacute;rio j&aacute; possui apontamento nesse per&iacute;odo";
$ling['custo_hora']="Custo Hora:";
$ling['custo_hora_extra']="Custo Hora Extra:";
$ling['custo_total']="Custo Total";
$ling['qtd_qtd']="Qtd.";
$ling['custo_unico']="Custo Unit&aacute;rio";
$ling['descricao']="Descri&ccedil;&atilde;o";
$ling['descricao_com']="Descri&ccedil;&atilde;o com";
$ling['DESCRICAO_M']="DESCRI&Ccedil;&Atilde;O";
$ling['manutentor']="Manutentor:";
$ling['nova_ordem_serv']="Nova Ordem de Servi&ccedil;o";
$ling['ordem_fechada_e_nao_pode_apontada']="Esta ordem est&aacute; fechada e n�o pode ser apontada";
$ling['ordem_cancelada_e_nao_pode_apontada']="Esta ordem est&aacute; cancelada e n&atilde;o pode ser apontada";
$ling['ordem_serv_nr']="Ordem de Servi�o N&ordm;";
$ling['nao_exist_reg_para_nr_os']="N&atilde;o existem registros para esse n&uacute;mero de OS";
$ling['info_geral']="Info. Geral";
$ling['tempos_recursos']="Tempos/Recursos";
$ling['pendencias']="Pend&ecirc;ncias";
$ling['pendencias_m']="PEND&Ecirc;NCIAS";
$ling['pendencia']="Pend&ecirc;ncia";
$ling['mao_de_obra_prevista']="M&atilde;o de Obra Prevista";
$ling['apenas_os_func_aloca_mostra_use_filtro_eq_ve_dema']="Apenas os funcion&aacute;rios ja alocados s&atilde;o mostrados, use o filtro por equipe para ver os demais.";
$ling['resu_mao_obra_prev']="Resumo M&atilde;o de Obra Prevista";
$ling['preencha_aq_sua_obser_pendenc_cliq_grav']="Preencha aqui sua observa&ccedil;&atilde;o sobre a pend&ecirc;ncia e clique em gravar.";
$ling['ponto_posicao']="Ponto / Posi&ccedil;&atilde;o";
$ling['temp_para_ob_manut']="Tempo de Parada do Objeto de Manuten&ccedil;&atilde;o";
$ling['vali_ord_servi']="Validar Ordem de Servi&ccedil;o";
$ling['outros_custo']="Outros Custos";
$ling['custos']="Custos";
$ling['nao_pode_inferior_previsto']="n&atilde;o pode ser inferior ao previsto";
$ling['nao_foi_preenchida']=" n&atilde;o foi preenchida.";
$ling['reprogramar.ord.servico']="Reprogramar Ordem de Servi�o";
$ling['resumo_ord_servico']="Resumo da Ordem de Servi&ccedil;o";
$ling['ordem_n']="ORDEM N&ordm; ";
$ling['crit_prod']= "Criticidade Produ&ccedil;&atilde;o";
$ling['crit_seg']= "Criticidade Seguran&ccedil;a";
$ling['ORDEM_ORIGEM']="ORDEM ORIGEM";
$ling['ordem_origem_m']="Ordem Origem";
$ling['ORDEM_EXECUTADA']="ORDEM EXECUTADA";
$ling['ord_relprogramada']="O.S. Reprogramada";

// Textos do mfuncoes
$ling['novo_funcionario'] = 'Novo Funcion&aacute;rio';
$ling['nova_prest_serv=']='Novo Prest. de Servi&ccedil;o';
$ling['nova_lub']='Nova Ativ. de Lubrifica&ccedil;&atilde;o';
$ling['nova_insp']="Nova Ativ. de Inspe&ccedil;&atilde;o";
$ling['rel_geral_pecas']='Relatorio Geral de Pe&ccedil;as';
$ling['anexar_pendencias']='Anexar Pendencias';
$ling['tarefa_lubrificacao']='Tarefa de Lubrifica&ccedil;&atilde;o';
$ling['tarefa_inspecao']='Tarefa de Inspe&ccedil;&atilde;o';
$ling['apontamento_dos_recursos']='Apontamento dos Recursos, Paradas, etc.';
$ling['deseja_fechar_ordem']='VOC&Ecirc; DESEJA REALMENTE FECHAR ESSA ORDEM?';
$ling['deseja_reabrir_ordem']='VOC&Ecirc; DESEJA REALMENTE REABRIR ESSA ORDEM?';
$ling['deseja_rejeitar_solic']='VOC&Ecirc; DESEJA REALMENTE REJEITAR ESSA SOLICITA&Ccedil;&Atilde;O? AP&Oacute;S';
$ling['deseja_rejeitar_requisicao']='VOC&Ecirc; DESEJA REALMENTE REJEITAR ESSA REQUISI&Ccedil;&Atilde;O?';
$ling['deseja_aceitar_requisicao']='VOC&Ecirc; DESEJA REALMENTE ACEITAR ESSA REQUISI&Ccedil;&Atilde;O?';
$ling['deseja_remover_arquivo']='VOC&Ecirc; TEM CERTEZA QUE DESEJA REMOVER ESSE ARQUIVO?';
$ling['reabrir']='Reabrir';
$ling['apontamentos']='Apontamentos';
$ling['imprimir_os']='Imprimir OS';
$ling['imprimir']='Imprimir';
$ling['responder']='Responder';
$ling['rejeitar']='Rejeitar';
$ling['aceitar']='Aceitar';
$ling['aceitar_programar']='Aceitar/Programar';
$ling['resposta']='Resposta';
$ling['ordem_servico']='Ordem de Servi&ccedil;o';
$ling['ordem_servico_m']='ORDEM DE SERVI&Ccedil;O';
$ling['servico_sealizado_']='Servi&ccedil;o Realizado&nbsp;&nbsp; Prazo';
$ling['servico_cancelado']='Servi&ccedil;o Cancelado';
$ling['data_execucao']='Data Execu&ccedil;&atilde;o';
$ling['clique_aqui_detalhar']='Clique aqui para detalhar';
$ling['linhas_amarelo_pendencias']="As linhas marcadas em amarelo correspondem as pend&ecirc;ncias anexadas em ordens abertas (servi&ccedil;o n&atilde;o realizado).";
$ling['anexar_em_ordem']='Anexar selecionados em uma ordem de servi&ccedil;o';
$ling['ultimas_alteracoes']='Ultimas altera&ccedil;&otilde;es nessa tabela';
$ling['diretorio_nao_configurado']='Esse tipo de diretorio n&atilde;o foi configurado corretamente';
$ling['arquivo']="Arquivo";
$ling['diretorio']='Diret&oacute;rio';
$ling['tamanho']='Tamanho';
$ling['nao_deletado']=' n&atilde;o p&ocirc;de ser deletado!';
$ling['tabela_nao_configurado']='Tabela de banco de dados n&atilde;o configurado no manusis';
$ling['mot_cancel']="Motivo de cancelamento inv&aacute;lido.";
$ling['cancel_prog']="Cancelando a programa&ccedil;&atilde;o.";
$ling['ret_os']="Retirando a OS gerada.";
$ling['passando_na']="Passando para n&atilde;o alocados.";
$ling['mov_material']="Movimenta&ccedil;&atilde;o de material";
$ling['vz_global']="VISUALIZA&Ccedil;&Atilde;O GLOBAL";
$ling['executadas']="EXECUTADAS";
$ling['n_executadas']="N&Atilde;O-EXECUTADAS";
$ling['anexas_n_executadas']="ANEXAS N&Atilde;O-EXECUTADAS";

// THIAGO EMANUEL
$ling['ord_prox_sem']= "Pr&oacute;xima semana";
$ling['ord_ant_sem']= "Semana anterior";
$ling['ord_visualizar']="Visualizar";
$ling['ord_visualizar_os']="Visualizar OS";
$ling['ord_sem_esp']="SEM ESPECIALIDADE";
$ling['ord_sem_eqp']="SEM EQUIPE";
$ling['ord_tem_prev_hr']="Tempo previsto em horas";
$ling['ord_clic_ver_os']="Clique para ver as O.S. correspondentes";
$ling['ord_status_os']="Situa&ccedil;&atilde;o da O.S.";
$ling['ord_tem_disp_hr']="Tempo dispon&iacute;vel em horas";
$ling['ord_tem_serv_hr']="Tempo de servi&ccedil;o em horas";
$ling['ord_prev_rel'] = "Previsto x Utilizado";
$ling['ord_disp_prev'] = "Dispon&iacute;vel x Previsto";
$ling['ord_mat_prev'] = "Materiais previstos";
$ling['ord_mat_uti'] = "Materiais utilizados";
$ling['ord_total'] = "TOTAL";
$ling['calculadora_back']="Calculadora de Backlog";
$ling['ord_backlog']="Backlog (dia)";
$ling['ord_carga_prev'] = "Carga prevista (h)";
$ling['ord_carga_disp_dia'] = "Disp. por dia m&eacute;dia (h)";
$ling['ord_temp_total_parada'] = "Tempo Total de Parada(h)";
$ling['ord_enviado_sucesso'] = "Enviado com Sucesso";
$ling['ord_pendencia_sucesso'] = "Pend&ecirc;ncia Gravada com Sucesso";
$ling['ord_continua_pendente_n_exec'] = "Continua Pendente/N&atilde;o executada";
$ling['ord_limpando_registros'] = "LIMPANDO OS CADASTROS ANTERIORES";
$ling['ord_imposivel_gravar_peri'] = "Imposs&iacute;vel gravar, j&aacute; existe uma entrada nesse per&iacute;odo";
$ling['ord_temp_total_espec_n_maior_prev'] = "o tempo total por especialidade n&atilde;o pode ser maior que o previsto.";
$ling['ord_alerta_atv_num'] = "As atividades n&ordm;";
$ling['ord_alerta_atv_num_continuacao'] = "necessitam de mais de um profissional.";
$ling['ord_causa_defeito_sol'] = "Defeito, Causa e Solu&ccedil;&atilde;o";
$ling['ord_salvar_ord_serv'] = "Salvar Ordem de Servi&ccedil;o";
$ling['ord_obs_sobre_pendencia'] = "Preencha aqui sua observa&ccedil;&atilde;o sobre a pend&ecirc;ncia e clique em gravar.";
$ling['ord_exec_anexar_pendencias'] = "Executar/Anexar Pend&ecirc;ncias";
$ling['ord_prog_inicial'] = "Programado inicialmente para";
$ling['ord_fechamento_os'] = "Fechamento de OS.";
$ling['ord_material_apontado'] = "MATERIAL APONTADO";
$ling['ord_data_final_programada_maior_estabelecida'] = "DATA FINAL DA PROGRAMA&Ccedil;&Atilde;O &Eacute; MAIOR OU IGUAL A DATA ESTABELECIDA.";
$ling['ord_confirma_cancela'] = "DESEJA REALMENTE CANCELAR ESSA ORDEM?\\nINFORME O MOTIVO:";
$ling['ord_ver_maq_rota'] = "Clique para ver todos os " . $tdb[MAQUINAS]['DESC'];


$ling['sol_duplo_click'] = "Duplo clique para ver as respostas da Solicita&ccedil;&atilde;o";
$ling['sol_aba'] = "Aba";
$ling['sol_seguranca'] = "Seguran&ccedil;a";
$ling['sol_producao'] = "Produ&ccedil;&atilde;o";
$ling['sol_usuario'] = "Usu&aacute;rio";
$ling['sol_objeto'] = "Objeto de Manuten&ccedil;&atilde;o";
$ling['sol_posicao']="Posi&ccedil;&atilde;o";
$ling['sol_prazo']="Prazo";
$ling['sol_programacao']="Programa&ccedil;&atilde;o";
$ling['sol_desc']="Solicita&ccedil;&atilde;o";
$ling['sol_aprovando']="Aprovando Solicita&ccedil;&atilde;o";
$ling['sol_enviar_form']="Enviar Formul&aacute;rio";
$ling['sol_gerada_sucesso']="SUA SOLICITA&Ccedil;&Atilde;O FOI GERADA COM SUCESSO E J&Aacute; SE ENCONTRA EM SUA CARTEIRA DE SERVI&Ccedil;O";

$ling['prog_atualizacao']="Manusis - Atualiza&ccedil;&atilde;o de programa&ccedil;&atilde;o";
$ling['prog_fecha_janela']="Ao fechar a janela a atualiza&ccedil;&atilde;o n&atilde;o ser&aacute; concluida!!";
$ling['prog_ver_ordens']="(clique aqui para ver as Ordens)";
$ling['prog_fecha_janela2']="Clique a aqui para fechar a janela";
$ling['prog_plano_nao_encontrado']="Plano n&atilde;o encontrado";
$ling['prog_lancamento']="Lan&ccedil;amento n&atilde;o encontrado";
$ling['prog_nao_encontrado']="n&atilde;o encontrado";
$ling['prog_tarefa_descrita']="Tarefa n&atilde;o descrita ";
$ling['prog_fechados']="Fechado(s)";
$ling['prog_apontamento']="apontamento(s)";
$ling['prog_fechar_apontamento']="Fechar apontamento deste checklist";
$ling['prog_atencao']="ATEN&Ccedil;&Atilde;O: APONTAMENTOS FECHADOS N&Atilde;O PODEM SER ABERTOS NOVAMENTE. ESTA OPERA&Ccedil;&Atilde;O &Eacute; PERMANENTE. TEM CERTEZA QUE DESEJA FECHAR OS APONTAMENTOS MARCADOS?";
$ling['prog_fechar_apontamento_m']="Fechar apontamento marcados";
$ling['prog_ordem_gerada']="ORDEM GERADA";
$ling['prog_n_ordem_gerada']="ORDEM AINDA N&Atilde;O GERADA";
$ling['prog_programado']="PROGRAMADO";
$ling['prog_atv_modificadas']="Existem atividades neste plano que foram modificadas.";
$ling['prog_atualizar']="para atualizar a programa&ccedil;&atilde;.";
$ling['prog_definidas']="Programa&ccedil;&otilde;es definidas";
$ling['prog_extender']="Extender Programa&ccedil;&atilde;o";
$ling['prog_detalhamento']="Detalhamento da Programa&ccedil;&atilde;o";
$ling['prog_detalha_ord']="Detalhamento O.S.";
$ling['prog_muda_data_progra']="Mudar a data final das programa&ccedil;&otilde;es selecionadas para";
$ling['prog_atu_progra']="Atualizar Programa&ccedil;&otilde;es";
$ling['prog_ordens']="Ordens da Programa&ccedil;&atilde;o";
$ling['prog_deleta_progra']="TODAS AS ORDENS DE SERVI&Ccedil;O RELACIONADAS A ESSA PROGRAMA&Ccedil;&Atilde;O SER&Atilde;O PERDIDAS, DESEJA REALMENTE REMOVER ESSA PROGRAMA&Ccedil;&Atilde;O?";
$ling['prog_cancela_progra']="DESEJA REALMENTE CANCELAR ESSA PROGRAMA&Ccedil;&Atilde;O?\\nInforme o motivo:";

$ling['expo_tempo_esperado']="Tempo de processamento at&eacute; agora";
$ling['expo_dados_exportados']="Dados exportados da tabela";
$ling['expo_anexo_dados_exportados']="Em anexo, dados da tabela";
$ling['expo_exportados_em']="exportados em";
$ling['expo_esquema']="Esquema";
$ling['expo_enviar_esquema']="Enviar um esquema";
$ling['expo_novo_esquema']="Criar novo esquema";
$ling['expo_baixar_esquema']="Baixar esquema";
$ling['expo_esquemas']="Esquemas";
$ling['expo_email_sucesso']="E-Mail enviado com sucesso!";
$ling['expo_arquivo_sucesso']="Arquivo exportado com sucesso.\n\nPara baixar o arquivo, clique no link abaixo:\n";
$ling['expo_servidor_ftp']="Servidor FTP n&atilde;o encontrado";
$ling['expo_salvar_servidor_ftp']="Salvar em servidor FTP";
$ling['expo_nao_servidor_ftp']="N&atilde;o foi poss&iacute;vel logar no servidor FTP";
$ling['expo_nao_servidor_ftp_envio']="N&atilde;o foi poss&iacute;vel enviar arquivo para servidor FTP";
$ling['expo_servidor_ftp_envio']="Arquivo enviado para servidor FTP com sucesso. Local";
$ling['expo_diretorio']="no diret&oacute;rio";
$ling['expo_tempo_processado']="Tempo total de processamento";
$ling['expo_exportar_dados']="Exportar Dados";
$ling['expo_campos']="Escolha os campos que ser&atilde;o usados";
$ling['expo_criterios_satisfazer']="Satisfazer os crit&eacute;rios";
$ling['expo_host_ftp']="Host FTP";
$ling['expo_dia_mes']="dia do m&ecirc;s (ex: 25)";
$ling['expo_dia_sem_ex']="dia da semana (ex: 1 = domingo, 6 = sexta)";
$ling['expo_mes']="m&ecirc;s (ex: 07)";
$ling['expo_semana_ano']="semana do ano";
$ling['expo_exportar_a']="Exportar a cada";
$ling['expo_salvar_modelo']="Salvar Modelo";
$ling['expo_salvar_modelo_exportar']="Salvar Modelo e Exportar Dados";
$ling['expo_excluir_modelo']="TEM CERTEZA QUE DESEJA EXCLUIR PERMANENTEMENTE ESTE MODELO?\\nESTA OPERA&Ccedil;&Atilde;O N&Atilde;O PODER&Aacute; SER DESFEITA!";
$ling['expo_excluir_modelo2']="Excluir este modelo";
$ling['expo_texto']="Formata&ccedil;&atilde;o de nomes de arquivo: Voc&ecirc; pode inserir os c&oacute;digos abaixo no nome do arquivo, e o c&oacute;digo ser&aacute; substitu&iacute;do pelo valor correspondente";
$ling['expo_arq_origem']="Ler arquivo de origem";
$ling['expo_ins_reg']="Inserir registros";
$ling['expo_analiasndo_sist']="Analisando sistema";

$ling['impo_arq']="Arquivo importado";
$ling['impo_arq_ftp']="Arquivo FTP importado";
$ling['impo_arq_ftp_nao_servidor']="Arquivo FTP: Servidor FTP n&atilde;o encontrado";
$ling['impo_arq_ftp_nao_logar']="Arquivo FTP: N&atilde;o foi poss&iacute;vel logar no servidor FTP";
$ling['impo_arq_ftp_nao_obter']="Arquivo FTP: N&atilde;o foi poss&iacute;vel obter o arquivo.";
$ling['impo_arq_vazio']="Arquivo FTP: O arquivo est&aacute; vazio ";
$ling['impo_arq_possui']="O arquivo possui";
$ling['impo_arq_linhas_branco']="linhas em branco, que foram ignoradas.";
$ling['impo_arq_csv']="Arquivo CSV: O n&uacute;mero de campos &eacute; menor que o n&uacute;mero necess&aacute;rio. S&atilde;o necess&aacute;rios";
$ling['impo_arq_csv_complemento']="campos e pelo menos uma linha do CSV possui apenas";
$ling['impo_arq_vazio']="Arquivo vazio.";
$ling['impo_erro_esquema']="Esquema incorreto, faltando campo na tabela remota";
$ling['impo_erro_bd_destino']="Erro ao buscar no BD destino.";
$ling['impo_erro_csv_tem']="Erro ao salvar arquivo CSV tempor&aacute;rio";
$ling['impo_erro_arq_tem']="Arquivo tempor&aacute;rio: O arquivo est&aacute; vazio";
$ling['impo_erro_bd_vazio']="BD arquivo vazio";
$ling['impo_erro_sincr_bd']="N&atilde;o foi poss&iacute;vel sincronizar os dados";
$ling['impo_erro_interno']="Erro l&oacute;gico interno!";
$ling['impo_erro_arq_zip']="N&atilde;o foi poss&iacute;vel salvar arquivo zip";
$ling['impo_formatacao']="Formata&ccedil;&atilde;o dos dados: campo";
$ling['impo_formatacao_comple']="excedeu o tamanho m&aacute;ximo de";
$ling['impo_na_linha']="na linha";
$ling['impo_com_valor']="com o valor";
$ling['impo_cont_disp']="CONTADOR DISPARADO";
$ling['impo_maq']="A m&aacute;quina";
$ling['impo_maq_nao_cont']="n&atilde;o possui contador. Linha ignorada.";
$ling['impo_nao_lanca_cont']="N&atilde;o foi poss&iacute;vel lan&ccedil;ar contador";
$ling['impo_campo_tab_rem']="Sem campos da tabela remota";
$ling['impo_conclu_importacao']="Importa&ccedil;&atilde;o conclu&iacute;da.\nLinhas importadas";
$ling['impo_conclu_importacao_comple']="\nLinhas n&atilde;o importadas";
$ling['impo_lendo_arq']="Lendo arquivo gravado com";
$ling['impo_no_bd']="no banco de dados";
$ling['impo_nao_arq_bd']="N&atilde;o foi poss&iacute;vel salvar log no banco de dados";
$ling['impo_nao_tab_rel']="N&atilde;o foi poss&iacute;vel salvar tabela relacionada";
$ling['impo_nao_tab']="N&atilde;o foi poss&iacute;vel salvar as altera&ccedil;&otilde;es na tabela";
$ling['impo_nao_imp_dados']="N&atilde;o foi poss&iacute;vel importar os dados";
$ling['impo_dados_sucesso']="Dados importados com sucesso";
$ling['impo_dados']="Importar dados";
$ling['impo_importacao']="Modo de Importa&ccedil;&atilde;o";
$ling['impo_importar_subs']="APENAS IMPORTAR (E SOBRESCREVER)";
$ling['impo_soncronizar']="SINCRONIZAR";
$ling['impo_campo_csv']="N&ordm; Campo do CSV";
$ling['impo_ig_linha']="Ignorar primeira linha, pois cont&eacute;m os t&iacute;tulos";
$ling['impo_baixar_serv_ftp']="Baixar de servidor FTP";
$ling['impo_baixar_bd']="Baixar de banco de dados";
$ling['impo_host_ip']="Host : IP";
$ling['impo_sid_oracle']="SID (Oracle)";
$ling['impo_filtro_where']="Filtros (WHERE)";
$ling['impo_a_cada']="Importar a cada";
$ling['impo_salvar_modelo_importar']="Salvar Modelo e Importar Dados";

$ling['geren_integracao']="Gerenciamento de Integra&ccedil;&atilde;o Autom&aacute;tica";
$ling['geren_integracao_auto']="Gerenciar Servi&ccedil;o Autom&aacute;tico";
$ling['geren_estado_serv']="Estado do servi&ccedil;o";
$ling['geren_ligado']="Ligado";
$ling['geren_desligado']="Desligado";
$ling['geren_parar_serv']="//parar servi&ccedil;o";
$ling['geren_gerenciar']="Gerenciar";
$ling['geren_nao_exec']="Ainda n&atilde;o executado";
$ling['geren_salva_alt']="Salvar Altera&ccedil;&otilde;es";
$ling['geren_agendar_importacao']="Agendar modelo de importa&ccedil;&atilde;o";
$ling['geren_agendar_exportacao']="Agendar modelo de exporta&ccedil;&atilde;o";
$ling['geren_remover_agendamento']="TEM CERTEZA DE QUE DESEJA REMOVER ESTE AGENDAMENTO? A OPERA&Ccedil;&Atilde;O N&Atilde;O PODER&Aacute; SER DESFEITA!";

$ling['rel_gerar_grafco']="Gerar Gr&aacute;fico";
$ling['rel_gerar_cronograma']="Gerar Cronograma";
$ling['rel_todas_acima']="Todas Acima";
$ling['rel_desc_familia']="FAM&Iacute;LIA";
$ling['rel_desc_empresa']="EMPRESA";
$ling['rel_desc_localizacoes']="Localiza&ccedil;&otilde;es";
$ling['rel_desc_loc1_min']="Localiza&ccedil;&atilde;o 1";
$ling['rel_desc_loc2_min']="Localiza&ccedil;&atilde;o 2";
$ling['rel_desc_loc1']="LOCALIZA&Ccedil;&Atilde;O 1";
$ling['rel_desc_loc2']="LOCALIZA&Ccedil;&Atilde;O 2";
$ling['rel_desc_obj']="OBJETO DE MANUTEN&Ccedil;&Atilde;O";
$ling['rel_desc_obj2']="OBJ. DE MANUTEN&Ccedil;&Atilde;O";
$ling['rel_desc_obj2_min']="Obj. de Manuten&ccedil;&atilde;o";
$ling['rel_desc_fam_obj']="Fam. de Obj. de Manuten&ccedil;&atilde;o";
$ling['rel_desc_pos']="POSI&Ccedil;&Atilde;O";
$ling['rel_desc_cc']="CENTRO DE CUSTO";
$ling['rel_desc_cc2']="Centro de Custo";
$ling['rel_desc_def']="DEFEITO";
$ling['rel_desc_cau']="CAUSA";
$ling['rel_desc_solu']="SOLU&Ccedil;&Atilde;O";
$ling['rel_desc_natu']="NATUREZA DOS SERVI&Ccedil;OS";
$ling['rel_desc_resp']="RESPONS&Aacute;VEL";
$ling['rel_desc_num_os']="N&Uacute;MERO OS";
$ling['rel_desc_num_os2']="N&uacute;mero OS";
$ling['rel_desc_tp_serv']="TIPO DE SERVI&Ccedil;O";
$ling['rel_desc_prev']="PREVENTIVA";
$ling['rel_desc_rotas']="ROTAS";
$ling['rel_desc_abertas']="ABERTAS";
$ling['rel_desc_fechadas']="FECHADAS";
$ling['rel_desc_canceladas']="CANCELADAS";
$ling['rel_desc_abertas2']="Abertas";
$ling['rel_desc_fechadas2']="Fechadas";
$ling['rel_desc_canceladas2']="Canceladas";
$ling['rel_desc_todas']="TODAS";
$ling['rel_desc_todas2']="Todas";
$ling['rel_desc_status']="STATUS";
$ling['rel_desc_status_ordem']="Status da Ordem";
$ling['rel_desc_situ'][1]="A";
$ling['rel_desc_situ'][2]="B";
$ling['rel_desc_situ'][3]="C";
$ling['rel_desc_s']="S";
$ling['rel_desc_numero']="NUMERO";
$ling['rel_desc_dt_prog']="DATA PROG.";
$ling['rel_desc_dt_prog_min']="Data Prog.";
$ling['rel_desc_tempo_parada']="TEMPO DE PARADA";
$ling['rel_desc_tempo_serv']="TEMPO DE SERVI&Ccedil;O";
$ling['rel_desc_num_ord']="N&uacute;mero da Ordem";
$ling['rel_desc_defeitos']="Defeitos";
$ling['rel_desc_causas']="Causas";
$ling['rel_desc_todos']="Todos";
$ling['rel_desc_tp_serv2']="Tipo de Servi&ccedil;o";
$ling['rel_desc_periodo']="Per&iacute;odo";
$ling['rel_desc_periodo_op']="Per&iacute;odo (opcional)";
$ling['rel_desc_serv_sist']="Somente Servi&ccedil;os Sistem&aacute;ticos";
$ling['rel_desc_serv_nsist']="Somente Servi&ccedil;os N&atilde;o Sistem&aacute;ticos";
$ling['rel_desc_primeiro']="1&ordm;";
$ling['rel_desc_segundo']="2&ordm;";
$ling['rel_desc_apontamento_disp']="Nenhum apontamento dispon&iacute;vel";
$ling['rel_desc_valor_peri']="Valor total do per&iacute;odo";
$ling['rel_desc_legendas']="Legendas";
$ling['rel_desc_de']="De";
$ling['rel_desc_ate']="At&eacute;";
$ling['rel_desc_mo']="M&Atilde;O DE OBRA";
$ling['rel_desc_extra']="Extra(R$)";
$ling['rel_desc_mat']="MATERIAIS";
$ling['rel_desc_material']="Material";
$ling['rel_desc_custo_total']="Custo Total (R$)";
$ling['rel_desc_custo_total2']="CUSTO TOTAL (R$)";
$ling['rel_desc_total']="Total (R$)";
$ling['rel_desc_custo']="Custo (R$)";
$ling['rel_desc_custo_uni']="Custo Unit&aacute;rio (R$)";
$ling['rel_desc_outros_custos']="OUTROS CUSTOS";
$ling['rel_desc_sifrao']="R$";
$ling['rel_desc_custo_total_os']="Custo Total da O.S. n&ordm;";
$ling['rel_desc_custo_total_mo']="Custo Total M&atilde;o de Obra (R$)";
$ling['rel_desc_custo_total_mat']="Custo Total Material (R$)";
$ling['rel_desc_custo_total_outros']="Custo Total Outros Custos (R$)";
$ling['rel_desc_equipe']="Equipe";
$ling['rel_desc_alcance_cronograma']="Alcance do Cronograma";
$ling['rel_desc_estatistica_geral']="Estat&iacute;stica Geral";
$ling['rel_desc_estatisticas']="Estat&iacute;sticas";
$ling['rel_desc_tempo_serv_h']="Tempo de Servi&iacute;o(horas)";
$ling['rel_desc_sem_parada']="Somente sem parada";
$ling['rel_desc_com_parada']="Somente com parada";
$ling['rel_desc_os_repro']="Apenas O.S. reprogramadas";
$ling['rel_desc_os_num']="N&ordm; O.S.";
$ling['rel_desc_lub']="Lubrifica&ccedil;&atilde;o";
$ling['rel_desc_insp']="Inspe&ccedil;&atilde;o";
$ling['rel_desc_uni']="Uni";
$ling['rel_desc_qtd_os']="Quant. O.S.";
$ling['rel_desc_temp_prev']="Tempo prev. (h)";
$ling['rel_desc_temp_hh']="Tempo HH. (h)";
$ling['rel_desc_temp_serv']="Tempo serv. (h)";
$ling['rel_desc_temp_parada']="Tempo parada (h)";
$ling['rel_desc_custo_mo']="Custo M.O. (R$)";
$ling['rel_desc_custo_mat']="Custo Mat. (R$)";
$ling['rel_desc_custo_outros']="Outros Custo (R$)";
$ling['rel_desc_todas_cancel']="TODAS MENOS AS CANCELADAS";
$ling['rel_desc_mostrar_campos']="Mostrar Campos O.S.";
$ling['rel_desc_campo_considerar']="Campo a considerar";
$ling['rel_desc_plano_removido']="PLANO REMOVIDO";
$ling['rel_desc_atividades']="ATIVIDADES";
$ling['rel_desc_frequencia']="Frequ&ecirc;ncia";
$ling['rel_desc_aloc_mo']="Total alocado de M&atilde;o de Obra";
$ling['rel_desc_total_mat']="Total de Materiais";
$ling['rel_desc_prev_mat']="Previs&atilde;o de Materiais";
$ling['rel_desc_prev_mo']="Previs&atilde;o de M&atilde;o de Obra";
$ling['rel_desc_col_mostradas']="Colunas mostradas";
$ling['rel_desc_loc_mat_texto']="Deixe os campos em branco para incluir todos";
$ling['rel_desc_sem_registros']="N&atilde;o foram encontrados registros de acordo com os filtros.";
$ling['rel_desc_parte_verificar']="Parte a verificar";
$ling['rel_desc_qtd_atv']="Quantidade de atividades";
$ling['rel_desc_qtd_atv_total']="Quantidade total de atividades";
$ling['rel_desc_tmp_total_prev']="Tempo total previsto";
$ling['rel_desc_est_minimo']="Est. Min&iacute;mo";
$ling['rel_desc_est_atual']="Est. Atual";
$ling['rel_desc_vizu_tudo']="Visualizar Tudo";
$ling['rel_desc_serv_realizado']='SERVI&Ccedil;O REALIZADO';
$ling['rel_desc_serv_realizado_pend']='SERVI&Ccedil;O REALIZADO COM PEND&Ecirc;NCIA';
$ling['rel_desc_serv_prog']='SERVI&Ccedil;O PROGRAMADO';
$ling['rel_desc_serv_prog_pend']='SERVI&Ccedil;O PROGRAMADO COM PEND&Ecirc;NCIA';


$ling['rel_desc_texto_ponto_compra_mat']="N&atilde;o h&aacute; ponto de compra para este(s) material(is).";
$ling['rel_desc_consumo_mat_lub']="Consumo de materiais em lubrifica&ccedil;&atilde;o";


$ling['cronograma_diario']="Cronograma Di&aacute;rio";

// DIVERSOS
$ling['item_peca']="Item/Pe&ccedil;a";
$ling['cod']="COD";
$ling['cod2']="C&oacute;d.";
$ling['grafico']="Gr&aacute;fico";
$ling['contador']="CONTADOR";
$ling['intervalo']="INTERVALO";
$ling['manusis_padrao']="Manusis Padr&atilde;o";
$ling['grafico_contador']="Gr&aacute;fico de Contador";
$ling['periodo_opcional']="Per&iacute;odo (opcional)";
$ling['localizacoes']="Localiza&ccedil;&otilde;es";
$ling['manusis']="Manusis";
$ling['exportar_excel']="Exportar para MS Excel";
$ling['clique_aqui']="Clique aqui";
$ling['hora']="Hora";
$ling['hora2']="hora";
$ling['cadastro_sucesso']="Cadastro efetuado com sucesso";
$ling['situacoes']="Situa&ccedil;&atilde;o";
$ling['em_andamento']="Em andamento";
$ling['cancelada']="Cancelada";
$ling['cancelar']="Cancelar";
$ling['concluida']="Conclu&iacute;da";
$ling['concluido']="Conclu&iacute;do";
$ling['concluido2']="conclu&iacute;do";
$ling['motivo_cancel']="Motivo de cancelamento";
$ling['nenhum_filtro']="Nenhum filtro";
$ling['inicializando']="Inicializando";
$ling['processando_aguarde']="Por favor, aguarde. Processando";
$ling['registros']="Registros";
$ling['registro']="Registro";
$ling['modelo']="Modelo";
$ling['tabela']="Tabela";
$ling['todos_si']="TODOS SIMULTANEAMENTE";
$ling['todos2']="TODOS";
$ling['qualquer_um']="QUALQUER UM DELES";
$ling['env_email']="Enviar por email";
$ling['dicas_formatacao']="Dicas de formata&ccedil;&atilde;o";
$ling['senha']="Senha";
$ling['agenda']="Agenda";
$ling['horas']="HORAS";
$ling['horas_min']="Horas";
$ling['semanas']="SEMANAS";
$ling['dias2']="DIAS";
$ling['dia']="Dia";
$ling['criar']="Criar";
$ling['enviar']="Enviar";
$ling['linhas']="linhas";
$ling['de']="de";
$ling['campos']="campos";
$ling['operacoes']="Opera&ccedil;&atilde;o";
$ling['campo_chave']="Campo chave";
$ling['campo_tab']="Campo da tabela";
$ling['uso_separadores']="Obrigat&oacute; usar separadores";
$ling['separador_decimais']="Separador de decimais";
$ling['relacao']="Rela&ccedil;&atilde;o";
$ling['nenhuma']="Nenhuma";
$ling['origem_dados']="Origem dos dados";
$ling['banco']="Banco";
$ling['base']="Base";
$ling['periodo_de']="Per&iacute;odo de";
$ling['clique_fechar']="Clique aqui para fechar";
$ling['quant']="Quant.";
$ling['indicadores']="Indicadores";
$ling['tamanho2']="Tamanho";
$ling['sistematico']="Sistem&aacute;tico";
$ling['preventiva']="Preventiva";
$ling['nao_sistematico']="N&atilde;o Sistem&aacute;tico";
$ling['total']="Total";
$ling['tempo_total_serv']="Tempo Total de Servi&ccedil;o";
$ling['tempo_total']="Tempo Total de Servi&ccedil;o";
$ling['tempo_total2']="Tempo Total";
$ling['servicos']="Servi&ccedil;os";
$ling['linha']="Linha";
$ling['todos_menos_cancel']="TODOS MENOS CANCELADO";
$ling['nao_ok']="&Ntilde; OK";
$ling['ok']="OK";
$ling['total_h']="TOTAL (h)";
$ling['mostrar']="Mostrar";
$ling['numero']="N&uacute;mero";
$ling['origem']="Origem";
$ling['local']="Local";
$ling['unidade']="Unidade";
$ling['parte_ponto']="Parte/Ponto";
$ling['navegador']="Navegador";
$ling['word']="Word";
$ling['incluir_img_anexa']="Incluir imagens anexas";
$ling['gravar']="Gravar";
$ling['extra']="Extra";
$ling['custo']="Custo";
$ling['texto']="Texto";
$ling['texto_m']="TEXTO";
$ling['calcular'] ="Calcular";
$ling['materiais'] ="Materiais";
$ling['fechar_ordem'] ="FECHAR ORDEM";
$ling['usar_somente_este_campo'] ="Usar somente este campo";
$ling['qtd_reg'] ="Qtd. de Registros";
$ling['clic_filtrar_obj'] ="Clique sobre um objeto de manuten&ccedil;&atilde;o para filtrar sua pesquisa.";
$ling['criado_dinamicamente'] ="CRIADO DINAMICAMENTE EM CARGA";
$ling['campo_indicado_n_existe_tab'] ="O campo  indicado n&atilde;o existe na tabela";
$ling['ver'] ="Ver";
$ling['tem'] ="Tem";
$ling['pontos'] ="Pontos";
$ling['deletando'] ="Deletando";
$ling['prazo_m'] ="PRAZO";
$ling['componente'] ="COMPONENTE";
$ling['tipo_m'] ="TIPO";
$ling['solicitante_m'] ="SOLICITANTE";
$ling['checklist_m'] ="CHECKLIST";
$ling['filtros_m'] ="FILTROS";
$ling['sol'] ="SOL.";
$ling['supervisor'] ="Supervisor";
$ling['anexar'] ="Anexar";
$ling['voltar'] ="Voltar";
$ling['new_atv'] ="Nova Atividade de";
$ling['cadastrar_atv'] ="Cadastrar Atividade";
$ling['horario_total_invalido'] = "Hor&aacute;rio total � inv&aacute;lido";


// FORM
$ling['form_formulario_n_encontrado'] ="FORMULARIO N&Atilde;O FOI ENCONTRADO: ";
$ling['form_campo_numerico'] ="O campo &eacute; num&eacute;rico";
$ling['form_empresa_matriz_cadastrada'] ="empresa Matriz ja cadastrada.";
$ling['form_operacao_efetuada_sucesso'] ="Opera&ccedil;&atilde;o Efetuada com Sucesso";


// PARAMETROS
$ling['qtd_prev_por_ponto'] ="Qtd. Prev. por Ponto";
$ling['pendencias_anexas'] ="PEND&Ecirc;NCIAS ANEXAS";
$ling['manusis_ord_serv'] ="MANUSIS ORDENS DE SERVI�O";
$ling['entrar'] ="Entrar";
$ling['selecionar'] ="Selecionar";
$ling['lancar_valor'] ="Lan&ccedil;ar Valor";
$ling['setar_valor_inicial'] ="Setar Valor Inicial";
$ling['marcar_todos'] ="Marcar Todos";
$ling['desmarcar_todos'] ="Desmarcar Todos";
$ling['gerar_os'] ="Gerar OS";
$ling['num_pontos'] ="N&ordm; de Pontos";
$ling['qtde_total'] ="Quantidade Total";
$ling['sel_ponto'] ="Selecione um Ponto";
$ling['param_do_dia'] ="DO DIA";
$ling['param_ao'] ="ao";
$ling['param_fim'] ="Fim";
$ling['param_inicio'] ="In&iacute;cio";

// LOGS
$ling['log_download_comp'] ="Download Completo da semana";
$ling['log_ir'] ="IR";
$ling['log_n_encontrado'] ="Log n&atilde;o encontrado para a semana - ano";
$ling['log_sair_manusis'] ="TEM CERTEZA QUE DESEJA SAIR DO MANUSIS?";

$ling['index_resolucao'] ="O Manusis deve ser visualizado na resolu��o 1024 x 768 ou superior.  Voc&ecirc; est&aacute; usando a resolu&ccedil;&atilde;o";
$ling['index_navegador_acesso'] ="O Manusis deve ser visualizado preferencialmente com o navegador";
$ling['index_navegador_acesso_conti'] =", e n�o pode ser visualizado com vers�es do Internet Explorer anteriores a 6.0. Voc� est� usando Internet Explorer";

// Felipe
$ling['relcronograma_parada'] = "Ver paradas previstas?";
$ling['sim'] = "Sim";
$ling['nao'] = "N&atilde;o";
$ling['erro_reprog1'] = "Imposs&iacute;vel reprogramar as seguintes O.S.:";
$ling['erro_reprog2'] = "A data selecionada &eacute; inferior a data de abertura.";
$ling['ord_abrir_speed_check'] = "Abrir Novo Speed Check";


$ling['ord_abrir_checklist'] = "Abrir Novo Checklist";
$ling['componente_alocado_em'] = "MOLDE ALOCADO EM";
$ling['componente_nao_alocado'] = "COMPONENTE N&Atilde;O ALOCADO";
$ling['molde_ja_alocado'] = 'O Molde selecionado j&aacute; encontra-se no destino informado';
$ling['impossivel_alocar'] = 'Imposs&iacute;vel realizar a movimenta&ccedil;&atilde;o';
$ling['impossivel_abrir_ordem'] = 'Imposs&iacute;vel gerar a O.S';
$ling['molde_possui_os_aberta'] = 'O Molde selecionado possui uma O.S aberta, por favor, feche-a antes de abrir uma nova';

// N�o permitir deletar alguns itens
// ISSO VAI PARA JAVASCRIPT, POR ISSO DEIXE OS ACENTOS!!!!!
$ling['erro_del_empresa'] = "N�O � POSS�VEL REMOVER EMPRESAS SE EXISTIR OBJETOS DE MANUTEN��O LIGADOS A ELA.";

$ling['erro_del_fam_maq'] = "N�O � POSS�VEL REMOVER FAMILIA DE OBJETOS DE MANUTEN��O SE EXISTIR OBJETOS DE MANUTEN��O LIGADOS A ELA.";

$ling['erro_del_maq_patio_setup'] = "A M�QUINA � UTILIZADA EM REGRAS ESPEC�FICAS DO SISTEMA (P�TIO DE SETUP)";

$ling['erro_del_classe_maq'] = "N�O � POSS�VEL REMOVER CLASSE DE OBJETOS DE MANUTEN��O SE EXISTIR OBJETOS DE MANUTEN��O LIGADOS A ELA.";

$ling['erro_del_cc_maq'] = "N�O � POSS�VEL REMOVER CENTROS DE CUSTO SE EXISTIR OBJETOS DE MANUTEN��O LIGADOS A ELE.";

$ling['erro_del_fam_equip'] = "N�O � POSS�VEL REMOVER FAMILIA DE COMPONENTE SE EXISTIR COMPONENTES LIGADOS A ELE.";

$ling['erro_del_fam_mat'] = "N�O � POSS�VEL REMOVER FAMILIA DE MATERIAIS SE EXISTIR MATERIAIS LIGADOS A ELA.";

$ling['erro_del_subfam_mat'] = "N�O � POSS�VEL REMOVER SUB-FAMILIA DE MATERIAIS SE EXISTIR MATERIAIS LIGADOS A ELA.";

$ling['erro_del_plano_padrao'] = "N�O � POSS�VEL REMOVER ESSE PLANO SE EXISTIREM PROGRAMA��ES ATIVAS.";

$ling['erro_del_plano_rota'] = "N�O � POSS�VEL REMOVER ESSE PLANO SE EXISTIREM PROGRAMA��ES ATIVAS.";

$ling['erro_del_usuarios_grupo'] = "ESTE GRUPO � UTILIZADO EM REGRAS ESPEC�FICAS DO SISTEMA";

$ling['erro_del_tipo_servico'] = "ESTE TIPO DE SERVI�O � UTILIZADO EM REGRAS ESPEC�FICAS DO SISTEMA";

$ling['erro_del_fam_maq_dest'] = "FAM�LIA � UTILIZADA EM REGRAS ESPEC�FICAS DO SISTEMA";
