<?php

/**
 * Funcoes gerais Manusis
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @version  3.0
 * @package engine
 * @subpackage  bibliotecas
 */





###########################################################################
#  Recomendacao basica do Fernando:
#  No Zend, Ctrl+Shift+C (Collapse All) e procure a fun&Ccedil;o por categoria
###########################################################################

// CONSTANTES UTEIS
$mktime_1_dia = 60*60*24;


//========
/**
 * Defini&Ccedil;oes para logar
 *
 * Formato:
 * $defLog[TABELA] = array(CAMPOS)
 *
 * Onde:
 * TABELA = Constante definida no portuguesbr.php
 * CAMPOS = Nome dos campos que devem ser salvos
 *
 * PARA SALVAR TODOS OS CAMPOS PREENCHA COM "ALL".
 */

$defLog = array();
// Usuario para uma tabela n�o especificada
$defLog["DEFAULT"] = array("ALL");

//========


function DeletaOrdem($campoMid, $mid) {
    global $dba,$tdb,$manusis;
	
	// Caso n�o tenha vindo da carteira de servi�os
	if ($_SESSION[ManuSess]['os']['motivo_remove_os'] == "") {
		return true;
	}

    // BUSCANDO AS OS
    $sql = "SELECT * FROM " . ORDEM . " WHERE $campoMid = $mid";
    if(! $rs = $dba[$tdb[ORDEM]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
		
		// Salva os dados dessa OS
		$dados                       = array();
		$dados['MID']                = GeraMid(ORDEM_REMOVIDA, 'MID', $tdb[ORDEM_REMOVIDA]['dba']);
		$dados['NUMERO']             = "'" . $rs->fields['NUMERO'] . "'";
		$dados['DATA_PROG']          = "'" . $rs->fields['DATA_PROG'] . "'";
		$dados['HORA_ABRE']          = "'" . $rs->fields['HORA_ABRE'] . "'";
		$dados['DATA_ABRE']          = "'" . $rs->fields['DATA_ABRE'] . "'";
		$dados['USUARIO']            = "'" . VoltaValor(USUARIOS, "NOME", "MID", $rs->fields['USUARIO']) . "'";
		
		$dados['TEXTO']              = "'" . $rs->fields['TEXTO'] . "'";
		$dados['MOTIVO_REMOVE']      = "'" . LimpaTexto(mb_strtoupper($_SESSION[ManuSess]['os']['motivo_remove_os'])) . "'";
		$dados['DATA_HORA_REMOVE']   = "'" . date('Y-m-d H:i:s') . "'";
		$dados['MID_USUARIO_REMOVE'] = (int) $_SESSION[ManuSess]['user']['MID'];
		
		// Apenas OS n�o sistem�ticas tem TIPO DE SERVI�O
		if ($rs->fields['TIPO'] == 0 or $rs->fields['TIPO'] == 4) {
			$dados['TIPO_SERVICO']       = "'" . VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $rs->fields['TIPO_SERVICO']) . "'";
			$dados['TIPO']               = "'" . VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', $rs->fields['TIPO']) . "'";
			$dados['PLANO']              = "NULL";
			$dados['MAQUINA']            = "'" . VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $rs->fields['MID_MAQUINA']) . "'";
			$dados['CONJUNTO']           = "'" . VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $rs->fields['MID_CONJUNTO']) . "'";
		}
		// Sistem�ticas
		else {
			$dados['TIPO_SERVICO']       = "''";
			$dados['TIPO']               = "'" . VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', $rs->fields['TIPO']) . "'";
			
			// Pega o plano
			$plano_mid = VoltaValor(PROGRAMACAO, 'MID_PLANO', 'MID', $rs->fields['MID_PROGRAMACAO']);
			
			// Preventivas
			if ($rs->fields['TIPO'] == 1) {
				$dados['MAQUINA']        = "'" . VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $rs->fields['MID_MAQUINA']) . "'";
				$dados['CONJUNTO']       = "'" . VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $rs->fields['MID_CONJUNTO']) . "'";
				$dados['PLANO']          = "'" . VoltaValor(PLANO_PADRAO, 'DESCRICAO', 'MID', $plano_mid) . "'";
			}
			// Rotas
			else {
				$dados['MAQUINA']        = "''";
				$dados['CONJUNTO']       = "''";
				$dados['PLANO']          = "'" . VoltaValor(PLANO_ROTAS, 'DESCRICAO', 'MID', $plano_mid) . "'";
			}
		}
		
		// Montando o SQL
		$sql2 = "INSERT INTO " . ORDEM_REMOVIDA . " (" . implode(", ", array_keys($dados)) . ") VALUES (" . implode(", ", array_values($dados)) . ")";
		
		if(! $dba[$tdb[ORDEM_REMOVIDA]['dba']] -> Execute($sql2)) {
			erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_REMOVIDA]['dba']] -> ErrorMsg() . "<br />" . $sql);
			return false;
		}
		
		$rs->moveNext();
	}
}


function TimeToFloat($time){
    // Verifica se � mesmo um time
    if (strpos($time, ':') === false) return $time;

    // Dividindo em :
    $time_tmp = explode(':', $time);

    // Realizando os calculos
    // Horas
    $float = $time_tmp[0];

    // Minutos
    if ($time_tmp[1] > 0) {
        $float += $time_tmp[1] / 60;
    }

    // Segundos
    if ($time_tmp[2] > 0) {
        $float += $time_tmp[2] / 60 / 60;
    }

    // Retornando o valor em horas
    return round($float, 2);
}


/*
 *
 * Gera um novo numero de SOLICITA��O, PENDENCIA para a empresa informada.
 *
 * @author Felipe Matos Malinoski <felipe@manusistem.com.br>
 *
 *
 * */

function GeraNumEmp($mid_empresa, $tabela, $campo_num) {
    global $dba,$tdb,$manusis;

    // Codigo da empresa
    $cod_emp = VoltaValor(EMPRESAS, 'COD', 'MID', $mid_empresa);

    $var = "";
    
    if($manusis['empresas'] > 1) $var = " AND NUMERO LIKE '%$cod_emp-%'";


    // Buscando ultimo numero
    $sql = "SELECT $campo_num FROM $tabela WHERE MID_EMPRESA = $mid_empresa $var ORDER BY $campo_num DESC";
    if (! $rs = $dba[$tdb[$tabela]['dba']] -> SelectLimit($sql, 1, 0)) {
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$tabela]['dba']]->ErrorMsg() . "<br />" . $sql);
    }

    // Recuperando o numero
    $os_num = $rs -> fields[$campo_num];



    // Caso tenha permiss�o pra mais de uma empresa
    // adicionar o c�digo da empresa antes do numero da os
    if ($manusis['empresas'] > 1) {
        // Somente parte num�rica
        $nump = explode('-', $os_num);
        $num  = ((int) $nump[1]) + 1;
        $num_novo = $cod_emp . '-' . sprintf("%06s", $num);
    }
    // Se for apenas uma ent�o usar s� o numero
    else {
        $os_num++;
        $num_novo = sprintf("%06s", (int)$os_num);
    }

    return $num_novo;
}


function TiraChaveArray($arrayEntrada) {
    $arrayFinal = array();
    foreach($arrayEntrada as $arrayTemp) {
        $arrayFinal[] = array_values($arrayTemp);
    }

    return $arrayFinal;
}

function VoltaRelacaoTabela($tabela) {
    global $dba,$tdb,$manusis;
    // Resultado
    $tabRel = "";
    // Buscando em todos as tabelas
    foreach ($tdb as $tb => $campos) {
        // Pulando tabela atual
        if($tb == $tabela){
            continue;
        }
        // Buscando nos campos se existe alguma rela��o
        // de cascata com a tabela atual
        foreach ($campos as $campo => $def) {
            $rtb = VoltaRelacao($tb, $campo);
            if(($rtb != "") and ($rtb['tb'] == $tabela)){
                $tabRel[$tb] = $campo;
                break;
            }
        }
    }
    return $tabRel;
}


/*
 *
 * Gera um novo numero de OS para a empresa informada.
 *
 * @author Felipe Matos Malinoski <felipe@manusistem.com.br>
 *
 *
 * */

function GeraNumOS($mid_empresa) {
    global $dba,$tdb,$manusis;

    // Codigo da empresa
    $cod_emp = VoltaValor(EMPRESAS, 'COD', 'MID', $mid_empresa);
    $var = "";
    
    if($manusis['empresas'] > 1) $var = " AND NUMERO LIKE '%$cod_emp-%'";

    // Buscando ultimo numero
    $sql = "SELECT NUMERO FROM " . ORDEM . " WHERE MID_EMPRESA = $mid_empresa $var ORDER BY NUMERO DESC";
    if (! $rs = $dba[$tdb[ORDEM]['dba']] -> SelectLimit($sql, 1, 0)) {
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']]->ErrorMsg() . "<br />" . $sql);
    }

    // Recuperando o numero
    $os_num = $rs -> fields['NUMERO'];


    // Caso tenha permiss�o pra mais de uma empresa
    // adicionar o c�digo da empresa antes do numero da os
    if ($manusis['empresas'] > 1) {
        $nump = explode('-', $os_num);
        $num  = ((int) $nump[1]) + 1;
        $num_novo = $cod_emp . '-' . sprintf("%06s", $num);
    }
    // Se for apenas uma ent�o usar s� o numero
    else {
        $os_num++;
        $num_novo = sprintf("%06s", (int)$os_num);
    }

    return $num_novo;
}

function VoltaTempoParadaPrev($osmid) {
    global $dba,$tdb,$manusis;

    $tem = (int) VoltaValor(ORDEM_PREV, 'MID', array('MID_ORDEM', 'MAQUINA_PARADA'), array($osmid, 1));

    if ($tem) {
        // Buscando tempo total previsto
        $sql = "SELECT SUM(TEMPO) AS TOTAL FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = '$osmid'";
        if(! $rs = $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute($sql)){
            erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
        }
        // RETORNANDO
        return round($rs->fields['TOTAL'], 2);
    }

    return 0;
}


function CustoMoOS($osmid) {
    global $dba,$tdb,$manusis;
    // Buscando tempo total previsto
    $sql = "SELECT SUM(CUSTO + CUSTO_EXTRA) AS TOTAL FROM " . ORDEM_MAODEOBRA . " WHERE MID_ORDEM = '$osmid'";
    if(! $rs = $dba[$tdb[ORDEM_MAODEOBRA]['dba']]->Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MAODEOBRA]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    // RETORNANDO
    return round($rs->fields['TOTAL'], 2);
}

function CustoOutrosOS($osmid) {
    global $dba,$tdb,$manusis;
    // Buscando tempo total previsto
    $sql = "SELECT SUM(CUSTO) AS TOTAL FROM " . ORDEM_CUSTOS . " WHERE MID_ORDEM = '$osmid'";
    if(! $rs = $dba[$tdb[ORDEM_CUSTOS]['dba']]->Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_CUSTOS]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    // RETORNANDO
    return round($rs->fields['TOTAL'], 2);
}

function CustoMatOS($osmid) {
    global $dba,$tdb,$manusis;
    // Buscando tempo total previsto
    $sql = "SELECT SUM(CUSTO_TOTAL) AS TOTAL FROM " . ORDEM_MATERIAL . " WHERE MID_ORDEM = '$osmid'";
    if(! $rs = $dba[$tdb[ORDEM_MATERIAL]['dba']]->Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MATERIAL]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    // RETORNANDO
    return round($rs->fields['TOTAL'], 2);
}

function TempoParadaOS($osmid) {
    global $dba,$tdb,$manusis;
    // Buscando tempo total previsto
    $sql = "SELECT SUM(TEMPO) AS TOTAL FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = '$osmid'";
    if(! $rs = $dba[$tdb[ORDEM_MAQ_PARADA]['dba']]->Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MAQ_PARADA]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    // RETORNANDO
    return round($rs->fields['TOTAL'], 2);
}

function TempoPrevOS($osmid) {
    global $dba,$tdb,$manusis;
    // Buscando tempo total previsto
    $sql = "SELECT SUM(TEMPO) AS TOTAL FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = '$osmid'";
    if(! $rs = $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    // RETORNANDO
    return round($rs->fields['TOTAL'], 2);
}

function TempoHHOS($mid_os, $mid_f=0) {
    global $dba,$tdb,$manusis;
    // Filtros
    // BUSCANDO TEMPO DE SERVI�O PARA ESSE CLIENTE NESSA PROPOSTA
    $sql = "SELECT M.DATA_INICIO, M.HORA_INICIO, M.DATA_FINAL, M.HORA_FINAL FROM " . ORDEM_MADODEOBRA . " M
    WHERE M.MID_ORDEM = $mid_os ".($mid_f ? " AND M.MID_FUNCIONARIO = '$mid_f'" : '');
    if(! $rs = $dba[$tdb[ORDEM]['dba']]->Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    $ts_total = 0;
    while (! $rs->EOF){
        $mk_ini = VoltaTime($rs->fields['HORA_INICIO'], NossaData($rs->fields['DATA_INICIO']));
        $mk_fim = VoltaTime($rs->fields['HORA_FINAL'], NossaData($rs->fields['DATA_FINAL']));
        // Tempo HH em horas
        $ts_total += round(($mk_fim - $mk_ini) / 60 / 60, 2);

        $rs->MoveNext();
    }

    return $ts_total;
}

function Porcentagem($valor,$total) {
    if (!$total) return 0;
    else {
        $tmp = round(($valor/$total)*100,2);
        return $tmp;
    }
}


function BarraProgresso ($porc, $titulo = "", $cor_preenchimento = "", $cor_borda = "", $cor_fundo = "", $cor_fonte = "", $style_extra = "") {

    // Tratando os valores
    $title = "";
    if ($titulo != "") {
        $title = "title=\"" . htmlentities($titulo) . "\"";
    }

    if ($cor_preenchimento == "") {
        $cor_preenchimento = "steelblue";
    }

    if ($cor_borda == "") {
        $cor_borda = "#A9A9A9";
    }

    if ($cor_fundo == "") {
        $cor_fundo = "white";
    }

    if ($cor_fonte == "") {
        $cor_fonte = "black";
    }

    if ($porc > 100) {
        $cor_fonte = "red";
    }

    // Barra pronta
    $barra = "";

    $barra .= "<table width=\"100%\" height=\"20\" border=\"1\" cellpadding=\"0\" cellspacing=\"1\" style=\"border:1px solid $cor_borda; background-color: $cor_fundo; $style_extra\" $title>
    <tr>";

    // Para evitar erros de layout
    if ($porc > 0) {
        $barra .= "<td width=\"$porc%\" style=\"background-color: $cor_preenchimento; border:1px solid $cor_preenchimento; color: $cor_fonte;\" align=\"right\">";

        // Mostrar o valor
        if($porc >= 50) {
            $barra .= "$porc%";
        }
        else {
            $barra .= "&nbsp;";
        }

        $barra .= "</td>";
    }
    if ($porc < 100) {
        $barra .= "<td width=\"" . (100 - $porc) . "%\" style=\"border:0px;\" align=\"left\">";

        // Mostrar o valor
        if($porc < 50) {
            $barra .= "$porc%";
        }
        else {
            $barra .= "&nbsp;";
        }

        $barra .= "</td>";
    }

    $barra .= "</tr>
    </table>";

    return $barra;
}

function VoltaDataSeg($semana, $ano) {
    global $mktime_1_dia;

    // Primeiro dia do ano
    $mk = mktime(0, 0, 0, 1, 1, $ano);

    // Apontando para a primeira segunda do ano
    $dia_sem_ini = date('N', $mk);
    if ($dia_sem_ini != 1) {
        $dias_menos = (1 - $dia_sem_ini) * -1;
        $mk = $mk - ($mktime_1_dia * $dias_menos);
    }
    while((date('W', $mk) != $semana) or (date('o', $mk) != $ano)) {
        $dia_tmp = date('d', $mk);
        $mes_tmp = date('m', $mk);
        $ano_tmp = date('Y', $mk);

        $mk = mktime(0, 0, 0, $mes_tmp, $dia_tmp + 7, $ano_tmp);
    }

    return date('d/m/o', $mk);
}


function VoltaDataDom($semana, $ano) {
    global $mktime_1_dia;

    // Primeiro dia do ano
    $mk = mktime(0, 0, 0, 1, 1, $ano);

    // Apontando para o primeiro domingo do ano
    $dia_sem_ini = date('N', $mk);
    if ($dia_sem_ini != 7) {
        $dias_mais = 7 - $dia_sem_ini;
        $mk = $mk + ($mktime_1_dia * $dias_mais);
    }
    while((date('W', $mk) != $semana) or (date('o', $mk) != $ano)) {
        $dia_tmp = date('d', $mk);
        $mes_tmp = date('m', $mk);
        $ano_tmp = date('Y', $mk);

        $mk = mktime(0, 0, 0, $mes_tmp, $dia_tmp + 7, $ano_tmp);
    }

//    $dia_tmp = date('d', $mk);
//    $mes_tmp = date('m', $mk);
//    $ano_tmp = date('o', $mk);
//    $mk = mktime(0, 0, 0, $mes_tmp, $dia_tmp + 7, $ano_tmp);

    return date('d/m/Y', $mk);
}

function CalculaPorcetagemArray ($valor1, $chave1, $valor2, $chave2) {
    $soma_valor1 = 0;
    foreach ($valor1 as $valor_tmp) {
        if ($chave1 != '') {
            $soma_valor1 += $valor_tmp[$chave1];
        }
        else {
            $soma_valor1 += $valor_tmp;
        }
    }

    $soma_valor2 = 0;
    foreach ($valor2 as $valor_tmp) {
        if ($chave2 != '') {
            $soma_valor2 += $valor_tmp[$chave2];
        }
        else {
            $soma_valor2 += $valor_tmp;
        }
    }

    if (($soma_valor1 == 0) or ($soma_valor2 == 0)) {
        return 0;
    }
    else {
        return round(($soma_valor2 * 100) / $soma_valor1);
    }
}


function CancelaProg($mid_prog, $msg) {
    global $dba,$tdb,$manusis;
    // Tratando a mensagem
    $msg = strtoupper(LimpaTexto($msg));
    if($msg == "") {
        erromsg("Motivo de cancelamento inváido.");
        return false;
    }

    // Buscando OS
    $sql = "SELECT * FROM " . ORDEM . " WHERE STATUS = 1 AND MID_PROGRAMACAO = $mid_prog";
    if(! $rs = $dba[$tdb[ORDEM]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $mid_os = $rs->fields['MID'];
        DelCascata(ORDEM, $mid_os);
        $rs->MoveNext();
    }

    // SETANDO STATUS DA PROGRAMA&Ccedil;&Atilde;O
    $upd = "UPDATE " . PROGRAMACAO . " SET STATUS = 3, MOTIVO = '$msg', DATA_CANCELAMENTO = '".date('Y-m-d h:i:s')."' WHERE MID = $mid_prog";
    if(! $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($upd)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $upd);
        return false;
    }
    else {
        logar(4, $ling['cancel_prog'], PROGRAMACAO, $mid_prog);
    }
}
/**
 *
 * Usado para processar as tarefas ao deletar uma maquina.
 *
 * @param $campoMid campo a ser usado para o delete
 * @param $mid valor
 * @return true para sucesso e false para erro
 */
function DeletaProg($campoMid, $mid) {
    global $dba,$tdb,$manusis;

    // BUSCANDO AS ATIVIDADES NAS OS
    $sql = "SELECT MID FROM " . PROGRAMACAO . " WHERE $campoMid = $mid";
    if(! $rs = $dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $mid_prog = $rs->fields['MID'];

        // mudando os componentes para os n�o alocados
        $upd = "UPDATE " . CONTROLE_CONTADOR . " SET MID_ORDEM = 0 WHERE MID_PROGRAMACAO = $mid_prog";
        if(! $dba[$tdb[CONTROLE_CONTADOR]['dba']] -> Execute($upd)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[CONTROLE_CONTADOR]['dba']] -> ErrorMsg() . "<br />" . $upd);
            return false;
        }
        // Logando
        elseif ($dba[$tdb[CONTROLE_CONTADOR]['dba']] -> Affected_Rows() > 0) {
            logar(4, $ling['ret_os'], CONTROLE_CONTADOR, "MID_PROGRAMACAO", $mid_prog);
        }

        $rs->MoveNext();
    }

    return true;
}

/**
 *
 * Usado para processar as tarefas ao deletar uma atividade de rota.
 *
 * @param $campoMid campo a ser usado para o delete
 * @param $mid valor
 * @return true para sucesso e false para erro
 */
function DeletaLinkRota($campoMid, $mid) {
    global $dba,$tdb,$manusis;

    // Guardando os planos
    $plano_mid = array();

    // BUSCANDO AS ATIVIDADES NAS OS
    $sql = "SELECT MID, MID_PLANO FROM " . LINK_ROTAS . " WHERE $campoMid = $mid";
    if(! $rs = $dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[LINK_ROTAS]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $mid_atv = $rs->fields['MID'];

        // Buscando as atividades de os aberta
        $sql1 = "SELECT P.MID FROM " . ORDEM_LUB . " P, " . ORDEM . " O WHERE P.MID_ORDEM = O.MID AND O.STATUS = 1 AND P.MID_ATV = $mid_atv";
        if(! $rs1 = $dba[$tdb[ORDEM_PREV]['dba']] -> Execute($sql1)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_LUB]['dba']] -> ErrorMsg() . "<br />" . $sql1);
            return false;
        }
        while (! $rs1->EOF) {
            DelCascata(ORDEM_LUB, $rs1->fields['MID']);
            $rs1->MoveNext();
        }

        // Salvando pra depois
        $plano_mid[] = $rs->fields['MID_PLANO'];

        $rs->MoveNext();
    }

    // Limpando OS vazias
    if(count($plano_mid) > 0) {
        $sql = "SELECT MID FROM " . PROGRAMACAO . " WHERE TIPO = 2 AND STATUS = 1 AND MID_PLANO IN (" . implode(", ", $plano_mid) . ")";
        if(! $rs = $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
            return false;
        }
        while(! $rs->EOF) {
            deleta_ordem_vazia($rs->fields['MID'], 2, 0);
            $rs->MoveNext();
        }
    }
    return true;
}

/**
 *
 * Usado para processar as tarefas ao deletar uma atividade de preventiva.
 *
 * @param $campoMid campo a ser usado para o delete
 * @param $mid valor
 * @return true para sucesso e false para erro
 */
function DeletaAtividade($campoMid, $mid) {
    global $dba,$tdb,$manusis;

    // Guardando os planos
    $plano_mid = array();

    // BUSCANDO AS ATIVIDADES NAS OS
    $sql = "SELECT MID, MID_PLANO_PADRAO FROM " . ATIVIDADES . " WHERE $campoMid = $mid";
    if(! $rs = $dba[$tdb[ATIVIDADES]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ATIVIDADES]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $mid_atv = $rs->fields['MID'];

        // Buscando as atividades de os aberta
        $sql1 = "SELECT P.MID FROM " . ORDEM_PREV . " P, " . ORDEM . " O WHERE P.MID_ORDEM = O.MID AND O.STATUS = 1 AND P.MID_ATV = $mid_atv";
        if(! $rs1 = $dba[$tdb[ORDEM_PREV]['dba']] -> Execute($sql1)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_PREV]['dba']] -> ErrorMsg() . "<br />" . $sql1);
            return false;
        }
        while (! $rs1->EOF) {
            DelCascata(ORDEM_PREV, $rs1->fields['MID'], 'MID', false);
            $rs1->MoveNext();
        }

        // Salvando pra depois
        $plano_mid[] = $rs->fields['MID_PLANO_PADRAO'];

        $rs->MoveNext();
    }

    // Limpando OS vazias
    if(count($plano_mid) > 0) {
        $sql = "SELECT MID FROM " . PROGRAMACAO . " WHERE TIPO = 2 AND STATUS = 1 AND MID_PLANO IN (" . implode(", ", $plano_mid) . ")";
        if(! $rs = $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
            return false;
        }
        while(! $rs->EOF) {
            deleta_ordem_vazia($rs->fields['MID'], 1, 0);
            $rs->MoveNext();
        }
    }
    return true;
}

function FiltrosRelatorio($mostra_emp = 1, $mostra_area = 1, $mostra_setor = 1, $mostra_maq = 1, $mostra_fam_maq = 0, $mostra_conj = 0, $mostra_equip = 0, $div_id_filtro = "", $mostra_equipe = 0, $multi = 0) {
    global $dba, $tdb, $manusis;

    // Assim funciona melhor que colocando o valor na assinatura
    if ($div_id_filtro == "") {
        $div_id_filtro = "filtro_relatorio";
    }

    // Valores dos filtros
    $filtro_emp     = (int) $_GET['filtro_emp'];
    $filtro_area    = (int) $_GET['filtro_area'];
    $filtro_setor   = (int) $_GET['filtro_setor'];
    $filtro_maq     = (int) $_GET['filtro_maq'];
    $filtro_conj    = (int) $_GET['filtro_conj'];
    $filtro_equip   = (int) $_GET['filtro_equip'];
    $filtro_maq_fam = (int) $_GET['filtro_maq_fam'];
     if ($multi == 1) {
		$filtro_maq = $_GET['filtro_maq'];
		if (count($filtro_maq)>0) {
			foreach($filtro_maq as $k => $mid_maq){
				$url_maq .= "&filtro_maq[$k]=$mid_maq";
	    	}
		}
		else {
			$url_maq ="&filtro_maq[0]=0"; 	 	
			}
	}
	else{
		$url_maq = "&filtro_maq={$filtro_maq}";
	}

    // Equipe foi colocado depois
    $filtro_equipe = (int) $_GET['filtro_equipe'];

    // Pra n�o da mais problema
    $dir = ($_GET['dir']) ? $_GET['dir'] : 'aa';
    $dir_path = ($_GET['dir']) ? str_replace('a', '.', $_GET['dir']) : '..';

    // FORMATANDO O DESTINO
    $ajaxdestino = "$dir_path/parametros.php";

    if ($mostra_emp) {
        echo "<label class=\"campo_label\" for=\"emp\">" . $tdb[EMPRESAS]['DESC'] . ":</label>";
        FormSelectD("COD", "NOME", EMPRESAS, $filtro_emp, "filtro_emp", "filtro_emp", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_conj=$filtro_conj&filtro_equip=$filtro_equip&filtro_equipe=$filtro_equipe&multi=$multi&filtro_emp=' + this.value)");
        echo "<br clear=\"all\" />";
    }

    if ($mostra_area) {
        // Filtros
        $where_a = "";
        if ($filtro_emp) {
            $where_a = "WHERE MID_EMPRESA = $filtro_emp";
        }

        echo "<label class=\"campo_label\" for=\"filtro_area\">" . $tdb[AREAS]['DESC'] . ":</label>";
        FormSelectD("COD", "DESCRICAO", AREAS, $filtro_area, "filtro_area", "filtro_area", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_emp=$filtro_emp&filtro_setor=$filtro_setor&filtro_conj=$filtro_conj&filtro_equip=$filtro_equip&filtro_equipe=$filtro_equipe&multi=$multi&filtro_area=' + this.value)", $where_a);
        echo "<br clear=\"all\" />";
    }

    if ($mostra_setor) {
        // Filtros
        $where_s = "";
        if ($filtro_area) {
            $where_s = "WHERE MID_AREA = $filtro_area";
        }
        elseif ($filtro_emp) {
            $where_s = "WHERE MID_AREA IN (SELECT MID FROM " . AREAS . " WHERE MID_EMPRESA = $filtro_emp)";
        }

        echo "<label class=\"campo_label\" for=\"filtro_setor\">" . $tdb[SETORES]['DESC'] . ":</label>";
        FormSelectD("COD", "DESCRICAO", SETORES, $filtro_setor, "filtro_setor", "filtro_setor", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_conj=$filtro_conj&filtro_equip=$filtro_equip&filtro_equipe=$filtro_equipe&multi=$multi&filtro_setor=' + this.value)", $where_s);
        echo "<br clear=\"all\" />";
    }

    if ($mostra_fam_maq) {
        if ($filtro_emp) {
            $where_fam = "WHERE MID_EMPRESA=$filtro_emp";
        }

        echo "<label class=\"campo_label\" for=\"filtro_maq_fam\">" . $tdb[MAQUINAS_FAMILIA]['DESC'] . ":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $filtro_maq_fam, "filtro_maq_fam", "filtro_maq_fam", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor{$url_maq}&filtro_conj=$filtro_conj&filtro_equip=$filtro_equip&filtro_equipe=$filtro_equipe&multi=$multi&filtro_maq_fam=' + this.value)", $where_fam);
        echo "<br clear=\"all\" />";
    }

    if ($mostra_maq) {
        // Filtros
        $where_m = "";
        if ($filtro_setor) {
            $where_m = "WHERE MID_SETOR=$filtro_setor";
        }
        elseif ($filtro_area) {
            $where_m = "WHERE MID_SETOR IN (SELECT MID FROM " . SETORES . " WHERE MID_AREA=$filtro_area)";
        }
        elseif ($filtro_emp) {
            $where_m = "WHERE MID_EMPRESA=$filtro_emp";
        }
        if ($filtro_maq_fam) {
            $where_m .= ($where_m == "") ? "WHERE " : " AND ";
            $where_m .= "FAMILIA=$filtro_maq_fam";
        }
        if ($multi == 1) {			
        if (count($filtro_maq)>0 and ! in_array(0, array_values($filtro_maq))) {
			$where_m .= ($where_m == "") ? "WHERE " : " AND ";
			$where_m .=  " MID IN (".implode(',', array_values($filtro_maq)).")" ;
			}
		echo "<label class=\"campo_label\" for=\"filtro_maq\">" . $tdb[MAQUINAS]['DESC'] . ":</label>";
		FormSelectMult('COD', 'DESCRICAO', MAQUINAS, $filtro_maq, 'filtro_maq', 'filtro_maq', 'MID', 0, $where_m ,'COD', '', '360', '100');
		}
		else {		
        echo "<label class=\"campo_label\" for=\"filtro_maq\">" . $tdb[MAQUINAS]['DESC'] . ":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS, $filtro_maq, "filtro_maq", "filtro_maq", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_maq=$filtro_maq&filtro_conj=$filtro_conj&filtro_equip=$filtro_equip&filtro_equipe=$filtro_equipe&multi=$multi&filtro_maq=' + this.value)", $where_m);
        echo "<br clear=\"all\">";
	    }
    }

    if ($mostra_conj) {
        // Filtros
        $where_c = "";
        if (!is_array ($filtro_maq) and ($filtro_maq)) { 
            $where_c = "WHERE MID_MAQUINA = $filtro_maq";
        }

        echo "<label class=\"campo_label\" for=\"filtro_conj\">" . $tdb[MAQUINAS_CONJUNTO]['DESC'] . ":</label>";
        FormSelectD("TAG", "DESCRICAO", MAQUINAS_CONJUNTO, $filtro_conj, "filtro_conj", "filtro_conj", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_equip=$filtro_equip&filtro_equipe=$filtro_equipe&multi=$multi&filtro_conj=' + this.value)", $where_c);
        echo "<br clear=\"all\">";
    }

    if ($mostra_equip) {
        //     Filtros
        $where_e = "";
        if ($filtro_emp) {
            $where_e = "WHERE MID_EMPRESA = $filtro_emp";
        }
        elseif (!is_array ($filtro_maq) and ($filtro_maq)) {
            $where_e = "WHERE MID_MAQUINA = $filtro_maq";
        }
        elseif ($filtro_conj) {
            $where_e = "WHERE MID_CONJUNTO = $filtro_conj";
        }

        echo "<label class=\"campo_label\" for=\"filtro_equip\">" . $tdb[EQUIPAMENTOS]['DESC'] . ":</label>";
        FormSelectD("COD", "DESCRICAO", EQUIPAMENTOS, $filtro_equip, "filtro_equip", "filtro_equip", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_conj=$filtro_conj&filtro_equipe=$filtro_equipe&multi=$multi&filtro_equip=' + this.value)", $where_e);
        echo "<br clear=\"all\">";
    }

    if ($mostra_equipe) {
        //     Filtros
        $where_e = "";
        if ($filtro_emp) {
            $where_e = "WHERE MID_EMPRESA = $filtro_emp";
        }

        echo "<label class=\"campo_label\" for=\"filtro_equipe\">" . $tdb[EQUIPES]['DESC'] . ":</label>";
        FormSelectD("DESCRICAO", "", EQUIPES, $filtro_equipe, "filtro_equipe", "filtro_equipe", "MID", "", "", "atualiza_area2('$div_id_filtro', '$ajaxdestino?id=filtro_relatorio&dir=$dir&mostra_emp=$mostra_emp&mostra_area=$mostra_area&mostra_setor=$mostra_setor&mostra_maq=$mostra_maq&mostra_fam_maq=$mostra_fam_maq&mostra_conj=$mostra_conj&mostra_equip=$mostra_equip&mostra_equipe=$mostra_equipe{$url_maq}&filtro_maq_fam=$filtro_maq_fam&filtro_emp=$filtro_emp&filtro_area=$filtro_area&filtro_setor=$filtro_setor&filtro_conj=$filtro_conj&filtro_equip=$filtro_equip&filtro_equipe=' + this.value)", $where_e);
        echo "<br clear=\"all\">";
    }
}


function VoltaFiltroEmpresaGeral($tabela, $id, $mid_pai = false) {
    global $dba,$tdb,$manusis;
    // Resultado
    $filtro = array ();

    if($tabela == EMPRESAS) {
        return array($id);
    }

    // Buscando em todos as tabelas
    foreach ($tdb[$tabela] as $campo => $desc) {
        $rtb = VoltaRelacao($tabela, $campo);
        // campo que faz a rela&Ccedil;o encontrado
        if(($rtb != "") and ($rtb['fil_empresa'] > 0)){
            // caso for diretamente com a tabela de empresa para por aqui
            if($rtb['tb'] == EMPRESAS) {
                $id_pai = VoltaValor($tabela, $campo, 'MID', $id);

                // Buscando quais cadastros s&Atilde;o relativos as empresas
                $sql = "SELECT MID FROM " . $tabela . " WHERE " . $campo . " = '$id_pai'";
                if (! $rs = $dba[$tdb[$tabela]['dba']] -> Execute($sql)){
                    erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0]->ErrorMsg()."<br />" . $sql);
                }
                while (! $rs->EOF) {
                    $filtro[] = $rs->fields['MID'];
                    $rs->MoveNext();
                }
            }
            // Caso n�o for tenho que fazer uma pesquisa at&Eacute; chegar a tabela de empresas
            else {
                // MID RELACIONADO AO MID INICIAL
                $id_rel = VoltaValor($tabela, $campo, 'MID', $id);
                $filtro = VoltaFiltroEmpresaGeral($rtb['tb'], $id_rel, true);

                // Buscando quais cadastros s�o relativos as empresas
                if (count($filtro) > 0) {
                    $sql = "SELECT MID FROM " . $tabela . " WHERE " . $campo . " IN (" . implode(", ", $filtro) . ")";
                    if (! $rs = $dba[$tdb[$tabela]['dba']] -> Execute($sql)){
                        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $sql);
                    }
                    $filtro = array();
                    while (! $rs->EOF) {
                        $filtro[] = $rs->fields['MID'];
                        $rs->MoveNext();
                    }
                }
                else {
                    $filtro = array();
                }
            }
            break;
        }
    }


    return $filtro;
}


function VoltaCampoFiltroEmpresa($tabela){
    global $tdb;

    // Buscando em todos os campo da tabela
    foreach ($tdb[$tabela] as $campo => $desc) {
        $rtb = VoltaRelacao($tabela, $campo);
        // campo que faz a rela&Ccedil;&Atilde;o encontrado
        if(($rtb != "") and ($rtb['fil_empresa'] > 0)){
            return $campo;
        }
    }

    return false;
}

function VoltaPosForm($form_nome, $campo) {
    global $form;

    // Buscando em todos os campos do formulario
    foreach ($form[$form_nome] as $pos => $def) {
        // campo encontrado
        if($def['campo'] == $campo){
            return $pos;
        }
    }

    return false;
}


/**
 *
 * Usado para processar as tarefas ao deletar uma maquina.
 *
 * @param $campoMid campo a ser usado para o delete
 * @param $mid valor
 * @return true para sucesso e false para erro
 */
function DeletaMaq($campoMid, $mid) {
    global $dba,$tdb,$manusis;

    // mudando os componentes para os n&Atilde;o alocados
    $upd = "UPDATE " . EQUIPAMENTOS . " SET MID_MAQUINA = 0, MID_CONJUNTO = 0 WHERE MID_MAQUINA IN (SELECT MID FROM " . MAQUINAS . " WHERE $campoMid = $mid)";
    if(! $dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute($upd)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[EQUIPAMENTOS]['dba']] -> ErrorMsg() . "<br />" . $upd);
        return false;
    }
    // Logando
    else {
        logar(4, $ling['passando_na'], EQUIPAMENTOS, "MID_MAQUINA", $mid_maq);
    }

    return true;
}

/**
 *
 * Usado para processar as tarefas ao deletar um conjunto.
 *
 * @param $campoMid campo a ser usado para o delete
 * @param $mid valor
 * @return true para sucesso e false para erro
 */
function DeletaConj($campoMid, $mid) {
    global $dba,$tdb,$manusis;

    // mudando os componentes para os n&Atilde;o alocados
    $upd = "UPDATE " . EQUIPAMENTOS . " SET MID_MAQUINA = 0, MID_CONJUNTO = 0 WHERE MID_CONJUNTO IN (SELECT MID FROM " . MAQUINAS_CONJUNTO . " WHERE $campoMid = $mid)";
    if(! $dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute($upd)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[EQUIPAMENTOS]['dba']] -> ErrorMsg() . "<br />" . $upd);
        return false;
    }
    // Logando
    else {
        logar(4, $ling['passando_na'], EQUIPAMENTOS, "MID_CONJUNTO", $mid_maq);
    }

    return true;
}


function DeletaPermissao($campoMid, $mid) {
    global $dba,$tdb,$manusis;
    $sql = "SELECT MID FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO IN (SELECT USUARIO FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE $campoMid = '$mid') AND CENTRO_DE_CUSTO IN (SELECT MID FROM ".CENTRO_DE_CUSTO." WHERE MID_EMPRESA IN (SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE $campoMid = '$mid'))";
    $res = $dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> Execute($sql);
    if(!$res) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    // Logando
    else {
        while(!$res->EOF) {
            $emid = $res->fields('MID');
            DeletaItem(USUARIOS_PERMISAO_CENTRODECUSTO,'MID',$emid);
            $res->MoveNext();
        }
    }
    return true;
}

/**
 *
 * Usado para processar as tarefas ao deletar um almoxarifado.
 *
 * @author Felipe Matos Malinoski <felipe@manusistem.com.br>
 * @param int $mid_almox MID do almoxarifado deletado
 */
function DeletaAlmox($campo_mid, $mid_almox) {
    global $dba,$tdb,$manusis;
    // FEITO DESSA FORMA PARA EVITAR PROBLEMAS DE COMPATIBILIDADE FUTURA
    $sql = "SELECT MID FROM " . ALMOXARIFADO . " WHERE $campo_mid = $mid_almox";
    if(! $rs_almox = $dba[$tdb[ALMOXARIFADO]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ALMOXARIFADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
    }
    while (! $rs_almox->EOF) {
        $mid_almox = $rs_almox->fields['MID'];
        // Buscando quantidade de todos os materiais em estoque
        $sql = "SELECT MID_MATERIAL, ESTOQUE_ATUAL FROM " . MATERIAIS_ALMOXARIFADO . " WHERE MID_ALMOXARIFADO = $mid_almox";
        if(! $rs = $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
        }
        while (! $rs->EOF) {
            // Atualizando o Estoque Total do material
            $sql="UPDATE ".MATERIAIS." SET ESTOQUE_ATUAL = ESTOQUE_ATUAL - {$rs->fields['ESTOQUE_ATUAL']} WHERE MID = {$rs->fields['MID_MATERIAL']}";
            if(! $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> ErrorMsg() . "<br />Linha: " . __LINE__ . "<br />" . $sql);
            }
            // LOGANDO
            logar(4, '', MATERIAIS, $rs->fields['MID_MATERIAL']);
            $rs->MoveNext();
        }
        $rs_almox->MoveNext();
    }
}


/**
 *
 * Usado para substituir os updates feitos direto no form.php e
 * tambem para incluir a funcionalidade de almoxarifado.
 *
 * @author Felipe Matos Malinoski <felipe@manusistem.com.br>
 * @param int $mov 1 para entrada e 2 para saida
 * @param int $mid_almox MID do almoxarifado
 * @param int $mid_material MID do material
 * @param float $quant Quantidade a ser movimentada
 * @param float $custo Custo unitario do material, usado somente na entrada
 * @param int $mid_os MID da OS caso necessario, usado somente na saida
 */
function AtualizaEstoqueAlmox($mov, $mid_almox, $mid_material, $quant, $custo = 0, $mid_os = 0) {
    global $dba,$tdb,$manusis;
    $quant = str_replace(',','.',$quant);
    $custo = str_replace(',','.',$custo);
    // Entrada
    if ($mov == 1) {
        // Mudando estoque total do material
        $sql="UPDATE ".MATERIAIS." SET ESTOQUE_ATUAL = ESTOQUE_ATUAL + $quant, CUSTO_UNITARIO = '$custo' WHERE MID = '$mid_material'";
        if(! $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MATERIAIS]['dba']] -> ErrorMsg() . "<br />" . $sql);
        }
        // LOGANDO
        logar(4, $ling['mov_material'], MATERIAIS, "MID", $mid_material);
        // Verifica se ja exite esse material nesse almox
        $mid_mat_almox = VoltaValor(MATERIAIS_ALMOXARIFADO, 'MID', "MID_ALMOXARIFADO = $mid_almox AND MID_MATERIAL", $mid_material);
        // Salvando valor do almox
        if ($mid_mat_almox != 0) {
            $sql="UPDATE ".MATERIAIS_ALMOXARIFADO." SET ESTOQUE_ATUAL = ESTOQUE_ATUAL + $quant, CUSTO_UNITARIO = '$custo' WHERE MID = '$mid_mat_almox'";
            if(! $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
            }
            // LOGANDO
            logar(4, $ling['mov_material'], MATERIAIS_ALMOXARIFADO, 'MID', $mid_mat_almox);
        }
        // Cadastrando o material no Almox
        else {
            // Valores
            $dados = array (
                'MID'              => GeraMid(MATERIAIS_ALMOXARIFADO, "MID", $tdb[MATERIAIS_ALMOXARIFADO]['dba']),
                'MID_ALMOXARIFADO' => $mid_almox,
                'MID_MATERIAL'     => $mid_material,
                'ESTOQUE_ATUAL'    => $quant,
                'CUSTO_UNITARIO'   => $custo
            );
            // Salvando no Almox
            $sql = "INSERT INTO ".MATERIAIS_ALMOXARIFADO." (" . implode(', ', array_keys($dados)) . ")  VALUES (" . implode(", ", array_values($dados)) . ")";
            if(! $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
            }
            // LOGANDO
            logar(3, $ling['mov_material'], MATERIAIS_ALMOXARIFADO, 'MID', $dados['MID']);
        }
    }
    elseif ($mov == 2) {
        // Atualizando o Estoque Total do material
        $sql="UPDATE ".MATERIAIS." SET ESTOQUE_ATUAL = ESTOQUE_ATUAL - $quant WHERE MID = '$mid_material'";
        if(! $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> ErrorMsg() . "<br />Linha: " . __LINE__ . "<br />" . $sql);
        }
        // LOGANDO
        logar(4, '', MATERIAIS, 'MID', $mid_material);

        // Verifica se ja exite esse material nesse almox
        $mid_mat_almox = VoltaValor(MATERIAIS_ALMOXARIFADO, 'MID', "MID_ALMOXARIFADO = $mid_almox AND MID_MATERIAL", $mid_material);
        // Salvando valor do almox
        if ($mid_mat_almox != 0) {
            $sql="UPDATE ".MATERIAIS_ALMOXARIFADO." SET ESTOQUE_ATUAL = ESTOQUE_ATUAL - $quant WHERE MID = '$mid_mat_almox'";
            if(! $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[MATERIAIS_ALMOXARIFADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
            }
            // LOGANDO
            logar(4, $ling['mov_material'], MATERIAIS_ALMOXARIFADO, 'MID', $mid_mat_almox);
        }

        // Caso seja para uma OS
        if ($mid_os != 0) {
            $dados = array (
                'MID'            => GeraMid(ORDEM_PLANEJADO_MATERIAL,"MID",$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']),
                'MID_ORDEM'      => $mid_os,
                'MID_MATERIAL'   => $mid_material,
                'CUSTO_UNITARIO' => VoltaValor(MATERIAIS, 'CUSTO_UNITARIO', 'MID', $mid_material),
                'CUSTO_TOTAL'    => round(VoltaValor(MATERIAIS, 'CUSTO_UNITARIO', 'MID', $mid_material) * $quant, 2),
                'QUANTIDADE'     => $quant
            );

            // Salvando no Ordem Material
            $sql = "INSERT INTO ".ORDEM_PLANEJADO_MATERIAL." (" . implode(', ', array_keys($dados)) . ")  VALUES (" . implode(", ", array_values($dados)) . ")";
            if(! $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']] -> ErrorMsg() . "<br />" . $sql);
            }
            // LOGANDO
            logar(3, $ling['mov_material'], ORDEM_PLANEJADO_MATERIAL, 'MID', $dados['MID']);
        }
    }
}

function VoltaFiltroEmpresa($tabela, $retorno = 1, $mid_pai = false, $alias = "") {
    global $dba,$tdb,$manusis;
    // Resultado
    $filtro = array ();
    $st_fil = false;

    // Empresas visiveis
    $emp_ok = array();
    if ($_SESSION[ManuSess]['user']['MID'] != "ROOT") {
        $user = (int) $_SESSION[ManuSess]['user']['MID'];
        $sql = "SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE USUARIO = '$user'";
        if(! $resultado=$dba[$tdb[USUARIOS_PERMISAO_SOLICITACAO]['dba']] -> Execute($sql)) {
            erromsg($dba[$tdb[USUARIOS_PERMISAO_SOLICITACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
        }
        else {
            while(! $resultado->EOF){
                $emp_ok[] = $resultado->fields['MID_EMPRESA'];
                $resultado->MoveNext();
            }
        }
    }
    else {
        return ($retorno == 1)? $filtro : "";
    }

    // Sem permi��o para uma empresa especi�fica
    if (count($emp_ok) == 0) {
        return ($retorno == 1)? array('campo' => "MID", 'mid' => array(0), 'tipo' => 1) : "(1 = 2)";
    }

    // Quando a tabela or a propria empresa fica mais facil
    if ($tabela == EMPRESAS) {
        $filtro = array ('campo' => "MID", 'mid' => $emp_ok, 'tipo' => 1);
        if ($retorno == 1) {
            return $filtro;
        }
        else {
            if($emp_ok){
                return "(MID IN (" . implode(', ', $emp_ok) . "))";
            }
            else {
                return "";
            }

        }
    }


    // Buscando em todos as tabelas
    foreach ($tdb[$tabela] as $campo => $desc) {
        $rtb = VoltaRelacao($tabela, $campo);

        // campo que faz a relacao encontrado
        if(($rtb != "") and ($rtb['fil_empresa'] > 0)){
            $st_fil = true;

            // caso for diretamente com a tabela de empresa para por aqui
            if($rtb['tb'] == EMPRESAS) {
                if ($mid_pai){
                    // Buscando quais cadastros s&Atilde;o relativos as empresas
                    $sql = "SELECT MID FROM " . $tabela . " WHERE " . $campo . " IN (" . implode(", ", $emp_ok) . ")";
                    if (! $rs = $dba[$tdb[$tabela]['dba']] -> Execute($sql)){
                        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $sql);
                    }
                    while (! $rs->EOF) {
                        $filtro['mid'][] = $rs->fields['MID'];
                        $rs->MoveNext();
                    }
                }
                else {
                    if($emp_ok){
                        $filtro = array('campo' => $campo, 'mid' => $emp_ok, 'tipo' => $rtb['fil_empresa']);
                    }
                }
            }
            // Caso n&Atilde;o for tenho que fazer uma pesquisa ate chegar a tabela de empresas
            else {
                $filtro = VoltaFiltroEmpresa($rtb['tb'], 1, true);
                $filtro['campo'] = $campo;
                $filtro['tipo']  = $rtb['fil_empresa'];

                // Caso ja esteja no segundo nivel
                if(($mid_pai) and (count($filtro['mid']) > 0)) {
                    // Buscando quais cadastros s�o relativos as empresas
                    $sql = "SELECT MID FROM " . $tabela . " WHERE " . $campo . " IN (" . implode(", ", $filtro['mid']) . ")";
                    if (! $rs = $dba[$tdb[$tabela]['dba']] -> Execute($sql)){
                        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $sql);
                    }
                    $filtro['mid'] = array();
                    while (! $rs->EOF) {
                        $filtro['mid'][] = $rs->fields['MID'];
                        $rs->MoveNext();
                    }
                }
            }
            break;
        }
    }

    // Cadastro n�o e filtrado por empresa
    if(! $st_fil) {
        return ($retorno == 1)? array() : "";
    }

    // Retorno em array ou SQL?
    if($retorno == 2) {
        if(count($filtro) > 0) {
            $filtro_sql = "";

            if($alias != '') {
                $filtro['campo'] = $alias . "." . $filtro['campo'];
            }

            if($filtro['mid']){
                $filtro_sql .= "(" . $filtro['campo'] . " IN (" . implode(', ', $filtro['mid']) . ")";
            }

            // Usado quando o campo n�o e obrigatorio
            if ($filtro['tipo'] == 2 and $filtro['campo']) {
                $filtro_sql .= ($filtro_sql == "")? "(" : " OR ";
                $filtro_sql .= $filtro['campo'] . " = 0 OR " . $filtro['campo'] . " IS NULL";
            }

            if($filtro_sql) {
                $filtro_sql  .= ")";
            }

            // Mudando o valor
            $filtro = $filtro_sql;
        }
        else {
            $filtro = "";
        }
    }

    // EVITANDO ERROS
    if(is_array($filtro) and count($filtro['mid']) == 0) {
        $filtro =  array('campo' => "MID", 'mid' => array(0), 'tipo' => 1);
    }
    elseif (!is_array($filtro) and ($filtro == '')) {
        $filtro = '(1 = 2)';
    }

    return $filtro;
}


/**
 *
 * Retira acentos do texto enviado, de forma incencitiva.
 *
 * @author Felipe Matos Malinoski <felipe@manusis.com.br>
 * @param string $str Texto com acentos
 * @return string Texto sem acentos
 *
 */
function remove_acentos($str) {
    // Letras com acento
    $acento = array('�', '�', '�', '�', '�', '�', '�', '��', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�');
    // Letras sem acento
    $sacento = array('a', 'a', 'a', 'e', 'e', 'o', 'o', 'i', 'u', 'c', 'A', 'A', 'A', 'E', 'E', 'O', 'O', 'I', 'C');
    // fazendo o replace
    return str_replace($acento, $sacento, $str);
}


/**
 *
 * Busca no bd.php por rela��es marcadas como cascata com a tabela informada.
 *
 * @author Felipe Matos Malinoski <felipe@manusis.com.br>
 * @param string $tabela Tabela a ser restreada
 * @return array [TABELA][campo] se encontrar ou nada se n�o achar nada
 *
 */
function VoltaCascata($tabela) {
    global $dba,$tdb,$manusis;
    // Resultado
    $tabCascata = "";
    // Buscando em todos as tabelas
    foreach ($tdb as $tb => $campos) {
        // Pulando tabela atual
        if($tb == $tabela){
            continue;
        }
        // Buscando nos campos se existe alguma rela��o
        // de cascata com a tabela atual
        foreach ($campos as $campo => $def) {
            $rtb = VoltaRelacao($tb, $campo);
            if(($rtb != "") and ($rtb['delcascata'] == 1) and ($rtb['tb'] == $tabela)){
                $tabCascata[$tb] = $campo;
                break;
            }
        }
    }
    return $tabCascata;
}



//### -------------------------------------------------------------
//### FUN�OES REVISADAS
//### -------------------------------------------------------------



// -------------------------------------------------------------
// FUN�OES DE ACESSO A BANCO DE DADOS
// -------------------------------------------------------------
/**
 Gera um ID auto-incremet. essa fun��o foi criada para evitar criar uma chave que apenas o manusis cuide
 n�o confiando nas chaves de tabelas, diminuindo o risco de um usuario corromper os dados do banco
 */
function GeraMid($tb, $campo, $conexao) {
    global $dba;


    // Essas tabelas foram atualizar para utilizar triggers para autoincrement
    $tb_ignore = array(ORDEM_PREV, ORDEM_LUB, ORDEM_MO_ALOC, ORDEM_MO_PREVISTO, ORDEM_MAT_PREVISTO);

    // Caso a tabela seja uma delas retorne 0
    if (in_array($tb, $tb_ignore)) {
        return 0;
    }

    $resultado = $dba[$conexao]->Execute("SELECT MAX($campo) AS $campo FROM $tb ORDER BY $campo DESC");
    $mid = $resultado->fields[$campo];
    return $mid + 1;
}
/**
 Volta o valor de uma tabela em comum, pode ser usado para verificar se o valor j� existe tambem
 */
function VoltaValor($tb,$campo,$alvo,$valor,$conexao = 0, $order = 1, $completa = 1) {
    global $dba,$tdb;
    $conexao = ($conexao != 0)? $conexao : $tdb[$tb]['dba'];

    // ARRAY DE CONDI�OES
    if (is_array($alvo) && is_array($valor)){
        $cond = "";
        foreach ($alvo as $k => $c){
            $valor[$k] = (is_numeric($valor[$k]))? $valor[$k] : "'" . $valor[$k] . "'";
            $cond .= ($cond != "")? " AND " : "";
            $cond .= "$c  = " . $valor[$k] . "";
        }
    }
    else {
        $cond = "$alvo = '$valor'";
    }

    // ORDENA��O
    if ($order == 2) {
        $order = "ORDER BY $campo DESC";
    }
    elseif($order == 1) {
        $order = "ORDER BY $campo ASC";
    }
    else {
        $order = "";
    }



    if (($tb == EMPRESAS) and ($campo == "NOME") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT NOME,COD FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmp=$resultado->fields['COD']."-".$resultado->fields['NOME'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    if (($tb == AREAS) and ($campo == "DESCRICAO") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT DESCRICAO,COD FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmp=$resultado->fields['COD']."-".$resultado->fields['DESCRICAO'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    if (($tb == SETORES) and ($campo == "DESCRICAO") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT DESCRICAO,COD FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmp=$resultado->fields['COD']."-".$resultado->fields['DESCRICAO'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    if (($tb == MAQUINAS) and ($campo == "DESCRICAO") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT DESCRICAO,COD FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmp=$resultado->fields['COD']."-".$resultado->fields['DESCRICAO'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    if (($tb == MATERIAIS) and ($campo == "DESCRICAO") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT DESCRICAO,COD FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmp=$resultado->fields['COD']."-".$resultado->fields['DESCRICAO'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    elseif (($tb == MAQUINAS_CONJUNTO) and ($campo == "DESCRICAO") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT DESCRICAO,TAG FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmp=$resultado->fields['TAG']."-".$resultado->fields['DESCRICAO'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    elseif (($tb == EQUIPAMENTOS) and ($campo == "DESCRICAO") and ($completa == 1)) {
        $resultado=$dba[$conexao] -> Execute("SELECT DESCRICAO,COD,FAMILIA FROM $tb WHERE $cond");
        if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
        else {
            $tmpcod=VoltaValor(EQUIPAMENTOS_FAMILIA,"COD","MID",$resultado->fields['FAMILIA'],$tdb[EQUIPAMENTOS_FAMILIA]['dba']);
            $tmp=$resultado->fields['COD']."-".$resultado->fields['DESCRICAO'];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
    elseif (($tb == EMPRESAS) and ($campo == "DESCRICAO") and ($alvo == "MID") and ($valor == 0)) {
        return ;
    }
    else {
        $resultado=$dba[0] -> Execute("SELECT $campo FROM $tb WHERE $cond");
        if (!$resultado) erromsg("Arquivo: mfuncoes.php Fun&ccedil;&atilde;o: VoltaValor <br />" . $dba[0] -> ErrorMsg()."<br /> SELECT $campo FROM $tb WHERE $cond");
        else {
            $tmp=$resultado->fields[$campo];
            $i=count($resultado -> getrows());
            if ($i >= 1) return $tmp;
            else return false;
        }
    }
}
function VoltaValor2($tb, $campo, $busca, $conexao = 0, $order = "", $completa = 1) {
    global $dba,$tdb;
    $conexao=$tdb[$tb]['dba'];

    if (! is_array($busca)) {
        erromsg("O campo \ $busca  formato inv&aacute;lido");
        return false;
    }

    $cond = "";
    if(count($busca[0]) > 1){
        foreach ($busca as $q){
            $cond .= ($cond != "")? ($q[3] != "")? " " . $q[3] . " " :  " AND " : "";
            $cond .= $q[0];
            $cond .= ($q[2] != "")? " " . $q[2] . " " :  " = " ;
            $cond .= ($q[1] != "")? "'" . $q[1] . "'" : "";
        }
    }
    else{
        $cond .= $busca[0];
        $cond .= ($busca[2] != "")? "  " . $busca[2] . "  " :  "=" ;
        $cond .= ($busca[1] != "")? "'" . $busca[1] . "'" : "";
    }

    if ($order == "") {
        $order = "ORDER BY $campo ASC";
    }
    else {
        $order = "ORDER BY $order";
    }



    $sql = "SELECT $campo FROM $tb WHERE $cond $order";
    $resultado=$dba[$conexao] -> Execute($sql);
    if (!$resultado) erromsg("File: mfuncoes.php Funcao: VoltaValor2 <br />" . $dba[$conexao] -> ErrorMsg()." sql: $sql");
    else {
        $tmp = $resultado->getrows();
        if (count($tmp) == 1) {
            if (count($tmp[0]) == 1) {
                return $tmp[0][$campo];
            }
            else {
                return $tmp[0];
            }
        }
        elseif (count($resultado->fields) == 1){
            foreach ($tmp as $t) {
                $ret[$campo][] = $t[$campo];
            }
            return $ret;
        }
        else {
            return $tmp;
        }
    }


}
function VoltaCampos($tabela) {
    global $dba, $tdb;
    $tmp=$dba[$tdb[$tabela]['dba']] -> Execute("SHOW FIELDS FROM $tabela");
    $campos=array();
    while (!$tmp->EOF) {
        $campo=$tmp->fields;
        $fcampo=array();
        $ecampo = $campo['Field'];
        $fcampo['nome']=$ecampo;
        $etipo = $campo['Type'];
        if (strpos($etipo,'(')) list($etipo,$etam) = split('\(',str_replace(')','',$etipo)); else $etam = '';
        $fcampo['tipo']=$etipo;
        $fcampo['tamanho']=$etam;
        $fcampo['nulo']= ($campo['Null'] == 'YES') ? 1 : 0;
        $fcampo['padrao']=$campo['Default'];
        $fcampo['extra']=$campo['Extra'];

        $campos[$ecampo]=$fcampo;
        $tmp->MoveNext();
    }
    return $campos;
}
/**
 * Mesma coisa que VoltaValor, mas volta um array com todos os campos do registro e n�o apenas um campo
 *
 * @param string $tb - tabela
 * @param string $alvo - campo a comparar
 * @param string $valor - valor a comparar
 * @param int $order - 1 para ASC, 2 para DESC, em branco para ordem original do banco
 * @return array (ou false caso a consulta volte vazia)
 */
function VoltaLinha($tb,$alvo,$valor, $order = '') {
    global $dba,$tdb;
    $conexao=$tdb[$tb]['dba'];

    // ORDENA��O
    if ($order == 2) {
        $order = "ORDER BY $campo DESC";
    }
    elseif($order == 1) {
        $order = "ORDER BY $campo ASC";
    }
    else {
        $order = "";
    }

    $sql = "SELECT * FROM $tb WHERE $alvo = '$valor' $order";
    $resultado=$dba[0] -> Execute($sql);
    if (!$resultado) erromsg("Arquivo: mfuncoes.php Fun&ccedil;&atilde;o: VoltaLinha <br />" . $dba[0] -> ErrorMsg()."<br /> $sql");
    else {
        $tmp=$resultado->fields;
        $i=count($resultado -> getrows());
        if ($i >= 1) return $tmp;
        else return false;
    }
}
/**
 * Retorna o nivel de permiss�o do usuario atual no modulo(ID)
 *
 * @param integer $modulo
 * @return integer
 */
function VoltaPermissao($modulo, $op) {
    global $dba, $tdb, $ling, $manusis;
    $grupo=$_SESSION[ManuSess]['user']['GRUPO'];
    if ($_SESSION[ManuSess]['user']['MID'] == "ROOT"){
        return 1;
    }
    else {

        $sql = "SELECT MID FROM ".USUARIOS_MODULOS . " WHERE ID = {$modulo} AND OP = {$op}";

        $result = $dba[$tdb[USUARIOS_MODULOS]['dba']] -> Execute($sql);
        if(!$result->EOF){
            $data   = $result->fields;
            $modulo = $data['MID'];
        }
        else
          return 0;


        $resultado= $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute("SELECT PERMISSAO FROM ".USUARIOS_PERMISSAO." WHERE GRUPO = '$grupo' AND MODULO = '$modulo'");
        $campo=$resultado->fields;

        return ($campo['PERMISSAO'])?$campo['PERMISSAO']:0;

    }


}
function Setar_Usuario_Online($nome,$usuario,$ip,$tempo) {
    global $dba, $tdb, $ling, $manusis;
    $dba[$tdb[USUARIOS_ONLINE]['dba']] -> Execute("DELETE FROM ".USUARIOS_ONLINE." WHERE USUARIO = '$usuario'");
    $tmp_mid=GeraMid(USUARIOS_ONLINE,"MID",$tdb[USUARIOS_ONLINE]['dba']);
    $dba[$tdb[USUARIOS_ONLINE]['dba']] -> Execute("INSERT INTO ".USUARIOS_ONLINE." VALUES ('$nome','$usuario','$ip','$tempo','$tmp_mid')");
}
function Setar_Usuario_Offline() {
    global $dba, $tdb, $ling, $manusis;
    $resultado=$dba[$tdb[USUARIOS_ONLINE]['dba']] -> Execute("SELECT TEMPO,MID FROM ".USUARIOS_ONLINE."");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tempo=$campo['TEMPO'];
        $mid=$campo['MID'];
        $tempo2=mktime(date('H'),date('i'),date('s'),date('d'),date('m'),date('Y'));
        $tmp=$tempo2 - $tempo;
        if ($tmp > $manusis['sess']['tempo']) {
            $dba[$tdb[USUARIOS_ONLINE]['dba']] -> Execute("DELETE FROM ".USUARIOS_ONLINE." WHERE MID = '$mid'");
        }
        $i++;
        $resultado->MoveNext();
    }
}
/**
 * Retorna um novo codigo de componente, baseando-se num codigo antigo (copia) e o MID novo.
 *
 * @param string $cod_velho Codigo do componente que foi copiado
 * @param integer $mid_novo MID do componente novo
 * @return string
 */
function GeraCodComponente($cod_velho, $mid_novo) {
    if (substr($cod_velho,-1) != ')') $cod_rad = $cod_velho; // se nao termina em parenteses, pula
    else {
        $cod_menos2 = substr($cod_velho,0,strlen($cod_velho)-2);
        if (strpos($cod_menos2,'(') === false) $cod_rad = $cod_velho; // se nao abre parenteses, pula
        else {
            $i = strrpos($cod_menos2,'(');
            $cod_tnum = str_replace(')','',substr($cod_velho,$i+1));
            if ((!(int)$cod_tnum) or ($cod_tnum == $mid_novo)) $cod_rad = $cod_velho;
            else $cod_rad = substr($cod_menos2,0,$i);
        }
    }
    if (!$cod_rad) $cod_rad = $cod_velho;
    return "{$cod_rad}({$mid_novo})";
}

function DeletaItem($tabela, $mid, $valor, $logar = true) {
    global $dba,$tdb;

    if (is_array($mid) && is_array($valor)){
        $cond = "";
        foreach ($mid as $k => $c){
            $valor[$k] = (is_numeric($valor[$k]))? $valor[$k] : "'" . $valor[$k] . "'";
            $cond .= ($cond != "")? " AND " : "";
            $cond .= "$c  = " . $valor[$k] . "";
        }
    }
    else {
        $cond = "$mid = '$valor'";
    }

    // TENHO QUE LOGAR AQUI PARA PODER SALVAR TODOS OS VALORES
    $desc = (($mid != "MID") and (! is_array($mid)))? "QUANDO $mid = $valor:" : "";

    // CASO SEJA PARA LOGAR
    if ($logar) {
        logar(5, $desc, $tabela, $mid, $valor);
    }
    // Verifica se a OS a que pertence esta aberta
    if ($tabela == ORDEM_PREV) {

        if(($mid != 'MID_ORDEM') and ($mid != 'MID')) {
            $cond .= " AND MID_ORDEM IN (SELECT MID FROM " . ORDEM . " WHERE STATUS = 1 AND TIPO = 1)";
        }
    }

    $conexao = (int)$tdb[$tabela]['dba'];
    $resultado = $dba[$conexao]->Execute("DELETE FROM $tabela WHERE $cond");

    if (!$resultado){
        erromsg($dba[$conexao]->ErrorMsg());
        return false;
    }
    else {
        return $dba[$conexao]->Affected_Rows();;
    }
}
function UpdateItem($tabela,$set,$mid,$valor) {
    global $dba,$tdb;
    $valor=(int)$valor;
    $conexao=(int)$tdb[$tabela]['dba'];
    $resultado=$dba[$conexao] -> Execute("UPDATE $tabela SET $set WHERE $mid = '$valor'");
    if (!$resultado) erromsg($dba[$conexao] -> ErrorMsg());
    else {
        logar(4, "", $tabela, $mid, $valor);
        return 1;
    }
}
/**
 * Volta sequencia de OR OR OR OR, exemplo:
 * GeraOR(SETORES,"MID_SETOR","MID","MID_AREA = '2'")
 * faz um SELECT FROM SETORES WHERE MID_AREA = '2' e retornar� uma sequencia MID_SETOR = '{$MID}' OR MID_SETOR = '{$MID}' etc
 *
 * @param string $tabela - tabela onde fazer o SELECT
 * @param string $alvo - campo que volta escrito na lista
 * @param string $valor - campo cujo valor � usado na compara��o
 * @param string $condicao - condi��o a ser inserida no WHERE para filtragem
 */
function GeraOR($tabela,$alvo,$valor,$condicao) {
    global $dba,$tdb;
    $sql="SELECT $valor FROM $tabela WHERE $condicao";
    if (!$res = $dba[$tdb[$tabela]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[$tabela]['dba']] -> ErrorMsg()."<br><br>$sql");
    $tmp = '';
    while (!$res->EOF) {
        $campo=$res -> fields;
        AddStr($tmp,' OR ',"$alvo = '{$campo[$valor]}'");
        $res->MoveNext();
    }
    return $tmp;
}


// -------------------------------------------------------------
// FUN��ES DE INTERFACE
// -------------------------------------------------------------

//# INTERFACE BASICA
function errofatal($msg) {
    global $manusis;
    echo "<html>
<head>
<title>Manusis</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\" />
</head>
<body>
<div id=\"central\">
<br /><br /><br /><br />";
    erromsg($msg);
    echo "<br /><br /><br /><br />
</div>
</body>
</html>";
    exit;
}
function erromsg($msg) {
    global $manusis;
    blocomsg($msg,2);
}
function blocomsg($msg,$tipo) {
    global $manusis;
    // 1 Aten&ccedil;&atilde;o
    // 2 Erro
    // 3 ok
    if ($tipo == 1) $icone="atencao.png";
    elseif ($tipo == 2) $icone="erro.png";
    elseif ($tipo == 3) $icone="atencao.png";
    elseif ($tipo == 4) $icone="atencao_histerica.gif";
    echo "<table width=\"400\" border=\"0\" cellspacing=\"1\" cellpadding=\"5\" align=\"center\" class=\"caixamsg\">
<tr>
<td widht=\"50\"><img src=\"".$manusis['url']."imagens/icones/$icone\" border=\"0\" alt=\"Imagem\" vspace=\"5\" hspace=\"5\" /></td>
<td align=\"left\">$msg</td>
</tr></table>";
}
function alerta_seguranca ($alvo,$err) {

}

//# VIS�O EM ARVORE
/**
 * Retorna um array com inform�oes da Matriz
 *
 * @return array
 */
function ListaMatriz ($mid_emp = 0) {
    global $dba,$tdb;
    // Inicializando
    $i = 0;
    $tmp = array();
    // verifica quais o usuario tem acesso
    $perm_emp = VoltaFiltroEmpresa(EMPRESAS, 1);
    $where = "";
    if (count($perm_emp) > 0) {
        AddStr($where,' AND ',"MID IN (" . implode(', ', $perm_emp['mid']) . ")");
    }
    if ($mid_emp) {
        AddStr($where,' AND ',"MID = $mid_emp");
    }
    if ($where) $where = "WHERE $where";
    $sql = "SELECT NOME, COD, MID FROM ".EMPRESAS." $where ORDER BY COD ASC";
    if(! $resultado=$dba[$tdb[EMPRESAS]['dba']] -> Execute($sql)) {
        erromsg($dba[$tdb[EMPRESAS]['dba']] -> ErrorMsg() . "<br />" . $sql);
    }
    while(! $resultado->EOF){
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['NOME'];
        $tmp[$i]['cod']=$campo['COD'];
        $tmp[$i]['mid']=$campo['MID'];
        $i++;
        $resultado->MoveNext();
    }

    if ($i != 0) {
        return $tmp;
    }
    else {
        return array();
    }
}
/**
 * Retorna um array com COD,DESCRICAO,MID de todas Localiza��es 1 (AREAS)
 *
 * @return array
 */
function ListaArea ($mid_emp = 0, $mid_area = 0){
    global $dba,$tdb;

    if($mid_emp == 0) {
        $filtro = VoltaFiltroEmpresa(AREAS, 2);
        $filtro = ($filtro != "")? "WHERE " . $filtro : "";
    }
    else {
        $filtro = "WHERE MID_EMPRESA = $mid_emp";
    }
    if ($mid_area) {
        $filtro .= ($filtro)?" AND ":"WHERE ";
        $filtro .= "MID = '$mid_area'";
    }

    $resultado=$dba[$tdb[AREAS]['dba']] -> Execute("SELECT DESCRICAO, COD, MID FROM ".AREAS." $filtro ORDER BY COD ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['cod']=$campo['COD'];
        $tmp[$i]['mid']=$campo['MID'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,MID de todas Localiza��es 2 (SETORES) de uma determinada Localiza��o 1 (AREA)
 *
 * @param integer $mid_area
 * @return array
 */
function ListaSetor ($mid_area, $mid_setor = 0){
    global $dba,$tdb;
    $resultado=$dba[$tdb[SETORES]['dba']] -> Execute("SELECT DESCRICAO, COD, MID FROM ".SETORES." WHERE MID_AREA = '$mid_area' ".($mid_setor?"AND MID = '$mid_setor'":'')." ORDER BY COD ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['cod']=$campo['COD'];
        $tmp[$i]['mid']=$campo['MID'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,MODELO,STATUS,MID de todos Objetos de Manuten��o (MAQUINAS) de uma determinada Localiza��o 2 (SETOR)
 *
 * @param integer $mid_setor
 * @return array
 */
function ListaMaquina($mid_setor, $mid_maq=0){
    global $dba,$tdb,$ling;
    $resultado=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD, DESCRICAO, MODELO, STATUS, MID FROM ".MAQUINAS." WHERE MID_SETOR = '$mid_setor' ".($mid_maq?"AND MID = '$mid_maq'":'')." ORDER BY COD ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['cod']=$campo['COD'];
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['modelo']=$campo['MODELO'];
        $tmp[$i]['status']=$campo['STATUS'];
        $tmp[$i]['mid']=$campo['MID'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,MID de todas posi��es (CONJUNTOS) de uma determinado Obj. de Manuten��o (MAQUINAS)
 *
 * @param integer $mid_maq
 * @return array
 */
function ListaConjunto($mid_maq){
    global $dba,$tdb;
    $resultado=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> Execute("SELECT TAG, DESCRICAO, MID, COMPLEMENTO FROM ".MAQUINAS_CONJUNTO." WHERE MID_MAQUINA = '$mid_maq' AND MID_CONJUNTO = '0' ORDER BY TAG ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['tag']=$campo['TAG'];
        $tmp[$i]['mid']=$campo['MID'];
        $tmp[$i]['com']=$campo['COMPLEMENTO'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,MID de todas sub posi��es (CONJUNTOS) de uma posi��o pai
 *
 * @param integer $mid_conj
 * @return array
 */
function ListaSubConjunto($mid_conj){
    global $dba,$tdb;
    $resultado=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> Execute("SELECT TAG, DESCRICAO, MID, COMPLEMENTO FROM ".MAQUINAS_CONJUNTO." WHERE  MID_CONJUNTO = '$mid_conj' ORDER BY TAG ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['tag']=$campo['TAG'];
        $tmp[$i]['mid']=$campo['MID'];
        $tmp[$i]['com']=$campo['COMPLEMENTO'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,MID de todos equipamentos de uma posi��o (CONJUNTOS)
 *
 * @param integer $mid_conj
 * @return array
 */
function ListaEquipamento($mid_conj){
    global $dba,$tdb;
    $resultado=$dba[$tdb[EQUIPAMENTOS]['dba']]-> Execute("SELECT COD, DESCRICAO, MID FROM ".EQUIPAMENTOS." WHERE MID_CONJUNTO = '$mid_conj' ORDER BY COD ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['cod']=$campo['COD'];
        $tmp[$i]['mid']=$campo['MID'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,QUANTIDADE,MID de todos materiais de um equipamento
 *
 * @param integer $mid_equip
 * @return array
 */
function ListaPecaEquipamento($mid){
    global $dba,$tdb;


    $resultado=$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']]-> Execute("SELECT * FROM ".EQUIPAMENTOS_MATERIAL." WHERE MID_EQUIPAMENTO = '$mid' ORDER BY QUANTIDADE ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=VoltaValor(MATERIAIS, "DESCRICAO", "MID", $campo['MID_MATERIAL'], $tdb[MATERIAIS]['dba']);
        $tmp[$i]['classe']=VoltaValor(MATERIAIS_CLASSE, "DESCRICAO", "MID", $campo['MID_CLASSE'], $tdb[MATERIAIS]['dba']);
        $tmp[$i]['qto']=$campo['QUANTIDADE'];
        $tmp[$i]['mid']=$campo['MID_MATERIAL'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com COD,DESCRICAO,QUANTIDADE,MID de todos materiais de um conjunto
 *
 * @param integer $mid_conj
 * @return array
 */
function ListaPecaConjunto($mid_conj){
    global $dba,$tdb;
    $resultado=$dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]-> Execute("SELECT * FROM ".MAQUINAS_CONJUNTO_MATERIAL." WHERE MID_CONJUNTO = '$mid_conj' ORDER BY QUANTIDADE ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['cod']=VoltaValor(MATERIAIS,"COD","MID",$campo['MID_MATERIAL'],$tdb[MATERIAIS]['dba']);
        $tmp[$i]['nome']=VoltaValor(MATERIAIS,"DESCRICAO","MID",$campo['MID_MATERIAL'],$tdb[MATERIAIS]['dba']);
        $tmp[$i]['classe']=VoltaValor(MATERIAIS_CLASSE, "DESCRICAO", "MID", $campo['MID_CLASSE'], $tdb[MATERIAIS]['dba']);
        $tmp[$i]['qto']=$campo['QUANTIDADE'];
        $tmp[$i]['mid']=$campo['MID_MATERIAL'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com as informa�oes dos pontos de lubrifica��o de uma maquina
 *
 * @param integer $mid_maq
 * @return array
 */
function ListaPontoLub($mid_maq){
    global $dba,$tdb;
    $resultado=$dba[$tdb[PONTOS_LUBRIFICACAO]['dba']] -> Execute("SELECT PONTO, MID_CONJUNTO, MID FROM ".PONTOS_LUBRIFICACAO." WHERE MID_MAQUINA = '$mid_maq' ORDER BY PONTO ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['ponto']=$campo['PONTO'];
        $tmp[$i]['mid']=$campo['MID'];
        if ($campo['MID_CONJUNTO'] != "") {
            $re=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> Execute("SELECT TAG FROM ".MAQUINAS_CONJUNTO." WHERE MID = '".$campo['MID_CONJUNTO']."'");
            $tmp[$i]['conj']=$re->fields['TAG'];
        }
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com as informa�oes dos pontos de monitoramento de uma maquina
 *
 * @param integer $mid_maq
 * @return array
 */
function ListaPontoPred($mid_maq){
    global $dba,$tdb;
    $resultado=$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute("SELECT PONTO, MID_CONJUNTO,GRANDEZA, MID FROM ".PONTOS_PREDITIVA." WHERE MID_MAQUINA = '$mid_maq' ORDER BY PONTO ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['ponto']=$campo['PONTO'];
        $tmp[$i]['gra']=$campo['GRANDEZA'];
        $tmp[$i]['mid']=$campo['MID'];
        if ($campo['MID_CONJUNTO'] != "") {
            $re=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> Execute("SELECT TAG FROM ".MAQUINAS_CONJUNTO." WHERE MID = '".$campo['MID_CONJUNTO']."'");
            $tmp[$i]['conj']=$re->fields['TAG'];
        }
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
/**
 * Retorna um array com as informa��es dos contadores de uma maquina
 *
 * @param integer $mid_maq
 * @return array
 */
function ListaContador($mid_maq){
    global $dba,$tdb;
    $resultado=$dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute("SELECT DESCRICAO, MID FROM ".MAQUINAS_CONTADOR." WHERE MID_MAQUINA = '$mid_maq' ORDER BY DESCRICAO ASC");
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['nome']=$campo['DESCRICAO'];
        $tmp[$i]['mid']=$campo['MID'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) {
        return $tmp;
    }
    else {
        return 0;
    }
}
function ListaPlanoProg ($mid){
    global $dba,$tdb;
    $resultado=$dba[$tdb[PROGRAMACAO]['dba']] -> Execute("SELECT * MID FROM ".PROGRAMACAO." WHERE TIPO = '$mid' ORDER CODIGO ASC");
    $i=0;
    $_SESSION[ManuSess]['VoltaValor']=$mid;
    $rtb=VoltaRelacao(PROGRAMACAO,"MID_PLANO");
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tmp[$i]['cod']=$campo['CODIGO'];
        $tmp[$i]['plano']=VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$campo['MID_PLANO'],$rtb['dba']);
        $tmp[$i]['data_inicial']=$campo['DATA_INICIAL'];
        $tmp[$i]['data_final']=$campo['DATA_FINAL'];
        if (($mid == 1) and ($campo['MID_MAQUINA'] != "")) $tmp[$i]['maquina']=VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
        if (($mid == 1) and ($campo['MID_CONJUNTO'] != "")) $tmp[$i]['conjunto']=VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba']);
        $tmp[$i]['mid']=$campo['MID'];
        $resultado->MoveNext();
        $i++;
    }
    if ($i != 0) return $tmp;
    else return 0;
}

//# LISTAGEM
function ListaTabela ($tabela, $mid, $campos, $formulario, $relacao_campo, $relacao_valor, $cadastrar, $relatorio, $editar, $remover, $copiar, $localizar, $personalizar, $opcoes, $rodape, $cabecario,$caso){
    global $dba,$tdb,$manusis,$id,$op,$exe,$oq,$ling,$form,$VoltaTipo;
    $nome_script=$_SERVER['PHP_SELF'];
    $conexao=$tdb[$tabela]['dba'];
    switch (VoltaPermissao($id, $op)){
        case 2:
            $remover="";
            $cadastrar=1;
            if (($caso == "SOLP") or
                ($caso == "SOLA") or
                ($caso == "SOLR") or
                ($caso == "REQP") or
                ($caso == "REQA") or
                ($caso == "REQR") or
                ($caso == "REQRE")){
                $csql="AND USUARIO = '".$_SESSION[ManuSess]['user']['MID']."'";
                $csql2="WHERE USUARIO = '".$_SESSION[ManuSess]['user']['MID']."'";
            }
            break;
        case 3:
            $remover="";
            $cadastrar="";
            $relatorio="";
            $copiar="";
            $editar="";
            $opcoes="";
            if (($caso == "SOLP") or
                ($caso == "SOLA") or
                ($caso == "SOLR") or
                ($caso == "REQP") or
                ($caso == "REQA") or
                ($caso == "REQR") or
                ($caso == "REQRE")){
                $csql="AND USUARIO = '".$_SESSION[ManuSess]['user']['MID']."'";
                $csql2="WHERE USUARIO = '".$_SESSION[ManuSess]['user']['MID']."'";
                if(($caso != "REQP") and ($caso != "REQA") and ($caso != "REQR") and ($caso != "REQRE")){
                    $cadastrar=1;
                    $opcoes=1;
                }
            }
            break;
        case 0:
            if (($caso == "SOLP") or
                ($caso == "SOLA") or
                ($caso == "SOLR") or
                ($caso == "REQP") or
                ($caso == "REQA") or
                ($caso == "REQR") or
                ($caso == "REQRE")){
                $csql="AND USUARIO = '".$_SESSION[ManuSess]['user']['MID']."'";
                $csql2="WHERE USUARIO = '".$_SESSION[ManuSess]['user']['MID']."'";
            }
            break;
        case 1:

            break;

    }
    // Contador para posicionamento das paginas
    if ($_GET['cont'] != "") $_SESSION[ManuSess]['lt'][$tabela]['cont']=(int)$_GET['cont'];
    $cont=(int)$_SESSION[ManuSess]['lt'][$tabela]['cont'];
    if ($cont == "") $cont=0;

    if (($_GET['localizar'] != "") and ($_GET['buscar_texto'] != $_SESSION[ManuSess]['lt'][$tabela]['buscar_texto'])) $cont=0;

    // Localizar Item
    if ($_GET['buscar_texto'] != "") $_SESSION[ManuSess]['lt'][$tabela]['buscar_texto']=LimpaTexto($_GET['buscar_texto']);
    if ($_GET['buscar_texto2'] != "") $_SESSION[ManuSess]['lt'][$tabela]['buscar_texto2']=LimpaTexto($_GET['buscar_texto2']);
    if (($_GET['localizar'] != "") and ($_GET['buscar_texto'] == "")) $_SESSION[ManuSess]['lt'][$tabela]['buscar_texto']="";
    if (($_GET['status_pend'] != "")) $_SESSION[ManuSess]['lt'][$tabela]['status_pend']=LimpaTexto($_GET['status_pend']);


    $buscar_texto=$_SESSION[ManuSess]['lt'][$tabela]['buscar_texto'];
    $buscar_texto2=$_SESSION[ManuSess]['lt'][$tabela]['buscar_texto2'];
    $status_pend=$_SESSION[ManuSess]['lt'][$tabela]['status_pend'];

    // Onde Localizar
    if ($_GET['buscar_em']  != "") $_SESSION[ManuSess]['lt'][$tabela]['buscar_em']=(int)$_GET['buscar_em'];
    $buscar_em=(int)$_SESSION[ManuSess]['lt'][$tabela]['buscar_em'];

    if (($campos[$buscar_em] == "DATA") and ($buscar_texto != "")) {
        $d=explode("/",$buscar_texto);
        $d2=explode("/",$buscar_texto2);
        $buscar_texto=$d[2]."-".$d[1]."-".$d[0];
        $buscar_texto2=$d2[2]."-".$d2[1]."-".$d2[0];
    }

    // Qtos items por pagina
    if ($_GET['buscar_qto']  != "") $_SESSION[ManuSess]['lt'][$tabela]['buscar_qto']=(int)$_GET['buscar_qto'];
    $buscar_qto=(int)$_SESSION[ManuSess]['lt'][$tabela]['buscar_qto'];

    if ($buscar_qto == 0) $buscar_qto=50; // se n�o foi definido, usar o padrao

    // Campo para ordena��o da tabela
    if ($_GET['ordem']  != "") $_SESSION[ManuSess]['lt'][$tabela]['ord']=(int)$_GET['ordem'];
    $ord=(int)$_SESSION[ManuSess]['lt'][$tabela]['ord'];
    $ordem=$campos[$ord];

    if (($ordem == "") and ($caso = "SOLP")) $ordem="NUMERO";
    elseif ($ordem == "") $ordem=$mid;

    // Se n�o foi definido usar chave primaria
    if ($_GET['pordem']  != "") $_SESSION[ManuSess]['lt'][$tabela]['pord']=(int)$_GET['pordem'];
    $pordem=(int)$_SESSION[ManuSess]['lt'][$tabela]['pord'];

    if ($pordem == 1) {
        $pod = "ASC";
        $pod_ord=2;
        $pod_icone="asc.png";
    }
    elseif ($pordem == 2) {
        $pod = "DESC";
        $pod_ord=1;
        $pod_icone="desc.png";
    }
    elseif ($pordem == "") {
        if ($caso == "SOLP") {
            $pod="DESC";
            $pod_ord=2;
            $pod_icone="desc.png";
        }
        else {
            $pod="ASC";
            $pod_ord=1;
            $pod_icone="pi.png";
        }
    }
    // Adiciona ou remove colunas definidas pelo usuario
    if ($_GET['lt_person_campo']) $_SESSION[ManuSess]['lt'][$tabela][$campos[$_GET['campo']]]=1;
    if ($_GET['remover_campo'] != "") $_SESSION[ManuSess]['lt'][$tabela][$campos[$_GET['remover_campo']]]=0;


    // Se o usuario n�o definiu nenhum campo na tabela, listar os 6 primeiros
    $campos_user = $_SESSION[ManuSess]['lt'][$tabela];
    if ($campos_user == "") {
        if (count($campos) <= 6) {
            for ($i=0; $campos[$i] != ""; $i++) $_SESSION[ManuSess]['lt'][$tabela][$campos[$i]]=1;
        }
        else {
            for ($i=0; $i < 6; $i++) $_SESSION[ManuSess]['lt'][$tabela][$campos[$i]]=1;
        }
        $campos_user=$_SESSION[ManuSess]['lt'][$tabela];
    }

    if ($_SESSION[ManuSess]['lt'][$tabela]['campos_total'] == "") $_SESSION[ManuSess]['lt'][$tabela]['campos_total']=$campos;

    $conjunto=$_GET['conjunto']; // Variavel para tratamentos especiais
    if (!$campos) {
        erromsg($ling['err02']);
        exit;
    }
    echo "\n<!--Listando em $tabela-->
    <div id=\"lt\">\n";
    if ($cabecario == 1) {
        echo"<div id=\"lt_cab\">
        <h3>";
        if ($caso == 'REQP') {
            echo $ling['req_pendente'];
        }
        elseif ($caso == 'REQA') {
            echo $ling['req_aceita'];
        }
        elseif ($caso == 'REQR') {
            echo $ling['req_negada'];
        }
        elseif ($caso == 'REQRE') {
            echo $ling['req_resp'];
        }
        elseif($caso == 'PRES'){
            echo $ling['prestador'];
        }
        else {
            echo $tdb[$tabela]['DESC'];
        }
        echo "</h3>\n";
        if ($opcoes == 1) {
            echo "<span id=\"lt_menu\" onmouseover=\"menu_sobre('ltmenu')\" onmouseout=\"menu_fora('ltmenu')\">".$ling['opcoes']."\n<ul id=\"m_ltmenu\">\n";
            if (($cadastrar == 1) and ($relacao_campo == "")) {
                if ($caso == "FUNC") {
                    echo "<li><a href=\"javascript:abre_janela_form('FUNCIONARIO','','$oq','1','$id','$exe','$op',".$form['FUNCIONARIO'][0]['altura'].",".$form['FUNCIONARIO'][0]['largura'].",'','')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['novo_funcionario']." \" /> ".$ling['novo_funcionario']." </a></li>\n";
                }
                elseif($caso == "PRES"){
                    echo "<li><a href=\"javascript:abre_janela_form('FUNCIONARIO2','','$oq','1','$id','$exe','$op',".$form['FUNCIONARIO2'][0]['altura'].",".$form['FUNCIONARIO2'][0]['largura'].",'','')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['nova_prest_serv=']." \" /> ".$ling['nova_prest_serv=']." </a></li>\n";
                }
                else {
                    echo "<li><a href=\"javascript:abre_janela_form('$formulario','','$oq','1','$id','$exe','$op',".$form[$formulario][0]['altura'].",".$form[$formulario][0]['largura'].",'')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['novo']." \" /> ".$ling['novo']." </a></li>\n";
                }
            }
            elseif ($cadastrar == 1) {
                if ($caso == "LROTA") {
                    echo "<li><a href=\"javascript:abre_janela_repeteativ('$oq', 'lub', '$id', '$op','$exe', '$oq')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" {$ling['plano_atv_lub']} \" /> {$ling['plano_atv_lub']} </a></li>\n";

                    echo "<li><a href=\"javascript:abre_janela_repeteativ('$oq','insp', '$id', '$op', '$exe', '$oq')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" {$ling['plano_atv_insp']} \" /> {$ling['plano_atv_insp']} </a></li>\n";
                }
                elseif ($caso == "MAQMAT") {
                    echo "<li><a href=\"javascript:abre_janela_linka_mat('$oq','0')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['novo']." \" /> ".$ling['novo']." </a></li>\n";
                }
                elseif ($caso == "FUNC") {
                    echo "<li><a href=\"javascript:abre_janela_form('FUNCIONARIO','','$oq','1','$id','$exe','$op',".$form['LINK_LUBRIFICACAO'][0]['altura'].",".$form['LINK_LUBRIFICACAO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['novo_funcionario']." \" /> ".$ling['novo_funcionario']." </a></li>\n";
                    echo "<li><a href=\"javascript:abre_janela_form('FUNCIONARIO2','','$oq','1','$id','$exe','$op',".$form['LINK_INSPECAO'][0]['altura'].",".$form['LINK_INSPECAO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['nova_prest_serv=']." \" /> ".$ling['nova_prest_serv=']." </a></li>\n";
                }
                else {
                    echo "<li><a href=\"javascript:abre_janela_form('$formulario','','$oq','1','$id','$exe','$op',".$form[$formulario][0]['altura'].",".$form[$formulario][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['novo']." \" /> ".$ling['novo']." </a></li>\n";
                }

            }
            if ($relatorio == 1) echo "<li><a href=\"relatorio.php?tb=$tabela&con=$conexao\" target=\"_blank\"><img src=\"imagens/icones/22x22/imprimir.png\" border=0 align=\"middle\" alt=\" \" title=\" ".$ling['relatorio']." \" /> ".$ling['relatorio']." </a></li>\n";
            echo "</ul>
            </span>
            </div>
            <br clear=\"all\" />\n";
        }
        else echo "</div>";
        echo "<div id=\"lt_forms\">";
        if ($localizar == 1) {
            echo "
            <form method=\"GET\" action=\"$nome_script\">
            <fieldset>
            <legend>".$ling['localizar']."</legend>";

            if($caso == "PEN"){
                echo "<div style=\"z-index:11;float:left;\"><select name=\"status_pend\" id=\"status_pend\" class=\"campo_select\" >";
                if((int)$status_pend == 0)echo "<option value=\"0\"selected=\"selected\">{$ling['TODAS']}</option>";
                else echo "<option value=\"0\">{$ling['TODAS']}</option>";
                if((int)$status_pend == 1)echo "<option value=\"1\"selected=\"selected\">{$ling['executadas']}</option>";
                else echo "<option value=\"1\">{$ling['executadas']}</option>";
                if((int)$status_pend == 2)echo "<option value=\"2\"selected=\"selected\">{$ling['n_executadas']}</option>";
                else echo "<option value=\"2\">{$ling['n_executadas']}</option>";
                if((int)$status_pend == 3)echo "<option value=\"3\"selected=\"selected\">{$ling['anexas_n_executadas']}</option>";
                else echo "<option value=\"3\">{$ling['anexas_n_executadas']}</option>";
                echo "</select></div>";
            }

            echo "<div id=\"lt_busca\" style=\"float:left;padding-top:2px;\">";

            $defca=$_SESSION[ManuSess]['lt'][$tabela]['campos_total'][$buscar_em];
            if ($defca == "DATA") {
                if ($buscar_texto != "")$datai=NossaData($buscar_texto);
                if ($buscar_texto2 != "") $dataf=NossaData($buscar_texto2);
                echo "Inicio: <input onkeypress=\"return ajustar_data(this, event)\" class=\"campo_text\" type=\"text\" name=\"buscar_texto\" value=\"$datai\" size=\"10\" maxlength=\"10\" />";
                echo "&nbsp;Fim: <input onkeypress=\"return ajustar_data(this, event)\" class=\"campo_text\" type=\"text\" name=\"buscar_texto2\" value=\"$dataf\" size=\"10\" maxlength=\"10\" />";
            }
            else {
                echo "<input class=\"campo_text\" type=\"text\" name=\"buscar_texto\" value=\"$buscar_texto\" size=\"25\" maxlength=\"150\" />";
            }
            echo"</div>\n";

            if (count($campos) == 1) echo "<input type=\"hidden\" name=\"buscar_em\" value=\"".$campos[0]."\" />\n";
            else {
                echo " <select class=\"campo_select\" name=\"buscar_em\" onchange=\"atualiza_area2('lt_busca','parametros.php?id=11&tb=$tabela&oq=$oq&par=' + this.options[this.selectedIndex].value)\">\n";
                for ($i=0; $campos[$i] != ""; $i++) {
                    if ($i == $buscar_em) echo "<option selected=\"selected\" value=\"$i\">".$tdb[$tabela][$campos[$i]]."</option>\n";
                    else echo "<option value=\"$i\">".$tdb[$tabela][$campos[$i]]."</option>\n";
                }
                echo "</select>\n";
            }
            unset($rtb);
            echo "<select class=\"campo_select\" name=\"buscar_qto\"><option value=\"\">".$ling['itens_por_pagina']."</option>\n";
            if ($_GET['buscar_qto'] == 50) echo "<option value=\"50\" selected=\"selected\">50</option>\n";
            else echo "<option value=\"50\">50</option>\n";
            if ($_GET['buscar_qto'] == 75) echo "<option value=\"75\" selected=\"selected\">75</option>\n";
            else echo "<option value=\"75\">75</option>\n";
            if ($_GET['buscar_qto'] == 100) echo "<option value=\"100\" selected=\"selected\">100</option>\n";
            else echo "<option value=\"100\">100</option>\n";
            if ($_GET['buscar_qto'] == 150) echo "<option value=\"150\" selected=\"selected\">150</option>\n";
            else echo "<option value=\"150\">150</option>\n";
            if ($_GET['buscar_qto'] == 200) echo "<option value=\"200\" selected=\"selected\">200</option>\n";
            else echo "<option value=\"200\">200</option>\n";
            if ($_GET['buscar_qto'] == 500) echo "<option value=\"500\" selected=\"selected\">500</option>\n";
            else echo "<option value=\"500\">500</option>\n";
            echo "</select> \n
            <input class=\"botao\" type=\"submit\" name=\"localizar\" value=\"Ok\" />
            <input type=\"hidden\" name=\"id\" value=\"$id\" />
            <input type=\"hidden\" name=\"op\" value=\"$op\" />
            <input type=\"hidden\" name=\"oq\" value=\"$oq\" />
            <input type=\"hidden\" name=\"exe\" value=\"$exe\" />
            </fieldset>
            </form>\n";
        }
        if ($personalizar == 1) {
            echo "<form method=\"GET\" action=\"$nome_script\">
            <fieldset>
            <legend>".$ling['personalizar']."</legend>
            <select class=\"campo_select\" name=\"campo\">\n";
            for ($i=0; $campos[$i] != ""; $i++) {
                if (!$campos_user[$campos[$i]]) echo "<option value=\"$i\">".$tdb[$tabela][$campos[$i]]."</option>\n";
            }
            echo "</select>\n";
            echo "<input class=\"botao\" type=\"submit\" name=\"lt_person_campo\" value=\"".$ling['adicionar_campo']."\" />
            <input type=\"hidden\" name=\"id\" value=\"$id\" />
            <input type=\"hidden\" name=\"op\" value=\"$op\" />
            <input type=\"hidden\" name=\"oq\" value=\"$oq\" />
            <input type=\"hidden\" name=\"exe\" value=\"$exe\" />
            </fieldset>
            </form>\n";
        }
        echo "</div>
        <br clear=\"all\" />";
    }

    echo " <div id=\"lt_tabela\">\n";

    if ($caso == "PEN") {
        echo "<form action=\"\" method=\"POST\" />";
    }

    list($xseg, $seg)=explode(" ", microtime());

    $t1=((float)$xseg + (float)$seg);


    if($caso == "PRES"){
        $csql="AND FORNECEDOR != 0";
        $csql2="WHERE FORNECEDOR != 0";
    }
    elseif($caso == "FUNC"){
        $csql="AND FORNECEDOR = 0";
        $csql2="WHERE FORNECEDOR = 0";
    }


    // FILTRO POR EMPRESA;
    $filtro = VoltaFiltroEmpresa($tabela);
    if (count($filtro) > 0 and $filtro['mid']) {
        $csql .= "AND (" . $filtro['campo'] . " IN (" . implode(', ', $filtro['mid']) . ")";
        $csql2 .= ($csql2 != "")? " AND " : " WHERE ";
        $csql2 .= "(" . $filtro['campo'] . " IN (" . implode(', ', $filtro['mid']) . ")";

        // Usado quando o campo n�o s�o obrigatorios
        if ($filtro['tipo'] == 2) {
            $csql  .= " OR " . $filtro['campo'] . " = 0 OR " . $filtro['campo'] . " IS NULL";
            $csql2 .= " OR " . $filtro['campo'] . " = 0 OR " . $filtro['campo'] . " IS NULL";
        }
        $csql  .= ")";
        $csql2 .= ")";
    }

    if($caso != "PEN"){
        for ($i=0; $campos[$i] != ""; $i++) $sqlc.=$campos[$i].", ";
        $sqlc.="$mid";
    }

    if ($caso == "PEN") {
        if($status_pend != 0){
            if($status_pend == 1){
                // EXECUTAS
                $var = "p.MID_ORDEM_EXC != 0 AND p.MID_ORDEM_EXC = o.MID AND o.STATUS = 2";
                $tabela .= " as p, ".ORDEM_PLANEJADO." as o";
                for ($i=0; $campos[$i] != ""; $i++) $sqlc .= "p.".$campos[$i].", ";
                $sqlc .= "p.$mid";
            }
            elseif($status_pend == 2){
                $var = "MID_ORDEM_EXC = 0 ";
                for ($i=0; $campos[$i] != ""; $i++) $sqlc .= $campos[$i].", ";
                $sqlc .= "$mid";
            }
            elseif($status_pend == 3){
                // ANEXAS N�O-EXECUTADAS
                $var = "p.MID_ORDEM_EXC != 0 AND p.MID_ORDEM_EXC = o.MID AND o.STATUS = 1";
                $tabela.=" as p, ".ORDEM_PLANEJADO." as o";
                for ($i=0; $campos[$i] != ""; $i++) $sqlc .= "p.".$campos[$i].", ";
                $sqlc .= "p.$mid";
            }
            $csql="AND $var";
            $csql2="WHERE $var";
        }else{
            for ($i=0; $campos[$i] != ""; $i++) $sqlc.=$campos[$i].", ";
            $sqlc.="$mid";
        }
    }

    if ($caso == "SOLP") $sqlc.=",STATUS";


    // CASO NAO OCORRA UMA BUSCA
    if ($buscar_texto == "") {
        if ($relacao_campo == "") $sql="SELECT $sqlc FROM $tabela $csql2 ORDER BY $ordem $pod";
        // EXISTE UM RELACIONAMENTO PRE DEFINIDO PELA VAR RELACAO_CAMPO E RELACAO VALOR
        else $sql="SELECT $sqlc FROM $tabela WHERE $relacao_campo = '$relacao_valor' $csql ORDER BY $ordem $pod";
    }
    // OCORREU UMA BUSCA
    else {
        // TRATANDO TABELAS RELACIONADAS E TEXTO DIRETO
        if($caso == "PEN"){
            $tabela1 = PENDENCIAS;
            if($status_pend != 0) $complemento = "p.";
        }
        else $tabela1 = $tabela;

        $recb=VoltaRelacao($tabela1,$campos[$buscar_em]);
        if($recb != ""){
            if($buscar_texto != ""){
                if ($recb['cod'] != "") {
                    $busca[] = array($recb['cod'], "%$buscar_texto%", "LIKE", "OR");
                    $busca[] = array($recb['campo'], "%$buscar_texto%", "LIKE", "OR");

                    $busca_mid = VoltaValor2($recb['tb'], $recb['mid'], $busca, 0);
                }
                else {
                    $busca = array($recb['campo'], "%$buscar_texto%", "LIKE");

                    $busca_mid = VoltaValor2($recb['tb'], $recb['mid'], $busca, 0);
                }

                if (count($busca_mid[$recb['mid']]) > 1) {
                    $busca_mid = implode(", ", $busca_mid[$recb['mid']]);
                }
                
                if ($busca_mid != "") {
                    if (is_array($busca_mid) AND count($busca_mid) > 0) {
                        $query = $campos[$buscar_em] . " IN (" . implode(',', array_map(function ($entry) {
                            return $entry['MID']; }, $busca_mid)) . ")";
                    } elseif (is_array($busca_mid) AND count($busca_mid) == 0) {
                        $query = $campos[$buscar_em] . " IN (0)";
                    } else {
                        $query = $campos[$buscar_em] . " IN (" . $busca_mid . ")";
                    }
                }
                else {
                    $query = "1 = 2";
                }
            }
            else {
                $query = "1 = 1";
            }
        }
        else {
            $query = $campos[$buscar_em] ." LIKE '%" . $buscar_texto . "%'";
        }

        if (($caso == "SOLP") and ($campos[$buscar_em] == "DATA")){
            $sql="SELECT $sqlc FROM $tabela WHERE ".$campos[$buscar_em]." >= '$buscar_texto' or ".$campos[$buscar_em]." >= '$buscar_texto2' $csql ORDER BY $ordem $pod";
        }
        elseif ($caso == "SOLP"){
            $sql="SELECT $sqlc FROM $tabela WHERE ".$campos[$buscar_em]." = '$buscar_texto' $csql ORDER BY $ordem $pod";
        }
        elseif ($relacao_valor == ""){
            $sql="SELECT $sqlc FROM $tabela WHERE $query $csql ORDER BY $ordem $pod";
        }
        else {
            $sql="SELECT $sqlc FROM $tabela WHERE $relacao_campo = '$relacao_valor' AND $query $csql ORDER BY $ordem $pod";
        }
    }

    //echo $sql;
    if (!$resultado=$dba[$conexao] -> SelectLimit($sql,$buscar_qto,$cont)){
        $err = $dba[$conexao] -> ErrorMsg();
        erromsg("$err <br> SQL: $sql");
    }
    else {
        // VOLTAR PARA O NORMAL ASSIM O RESTO AINDA FUNCIONA
        if ($caso == "PEN") {
            $tabela = PENDENCIAS;
        }


        $nresultado=$dba[$conexao] -> Execute($sql);
        $nnlinhas=count($nresultado -> getrows());
        $iii = 1;
        if (($nnlinhas != 0) and ($nnlinhas > $buscar_qto)) {
            echo "<span id=\"lt_paginas\">".$ling['paginas'].": \n";
            for ($nlinhas=0; $nlinhas < $nnlinhas; $nlinhas = $nlinhas + $buscar_qto) {
                if ($cont == $nlinhas) echo "<strong>";
                echo "<a href=\"#\" onclick=\"atualiza_area('corpo','$nome_script?st=1&id=$id&op=$op&exe=$exe&oq=$oq&cont=$nlinhas')\">[ $iii ] </a>\n";
                if ($cont == $nlinhas) echo "</strong>\n";
                $iii++;
            }
            echo "</span>\n";
        }
        echo "<table id=\"lt_tabela_\">
        <tr>\n";

        for ($i=0; $campos[$i] != ""; $i++) {
            if ($campos_user[$campos[$i]] == 1) {
                echo "<th>";

                if ($i != 0) {
                    echo "<a href=\"javascript:atualiza_area('corpo','$nome_script?st=1&id=$id&op=$op&exe=$exe&oq=$oq&remover_campo=$i')\"><img src=\"imagens/icones/del.gif\" border=\"0\" alt=\" \" title=\"".$ling['remover_coluna']."\" /></a> ";
                }

                echo "<a href=\"javascript:atualiza_area('corpo','$nome_script?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordem=$i&pordem=$pod_ord')\" title=\"".$ling['definir_ordem']."\">";

                if ($ord == $i) echo $tdb[$tabela][$campos[$i]]." <img src=\"imagens/icones/$pod_icone\" border=\"0\" alt=\" \" /></a>";
                else echo $tdb[$tabela][$campos[$i]]." <img src=\"imagens/pi.png\" border=\"0\" alt=\" \" /></a>";

                echo "</th>\n";
            }
        }
        echo "<th>".$ling['opcoes']."</th></tr>\n";
        $ii = 0;
        $tr_cor=0;
        while (!$resultado->EOF) {
            $cc_r=$resultado -> fields;

            if (($caso == "PEN") and (VoltaValor(ORDEM_PLANEJADO,"STATUS","MID",$cc_r['MID_ORDEM'],0) == 1)) {
                echo "<tr bgcolor=\"#FFFFC0\">\n";
            }
            else {
                if ($tr_cor == 0) echo "<tr class=\"cor1\">\n";
                else echo "<tr class=\"cor2\">\n";
            }

            for ($i=0; $campos[$i] != ""; $i++) {
                if ($campos_user[$campos[$i]] == 1) {
                    $ctipo = $resultado ->FetchField($i);
                    $ctipo = $resultado->MetaType($ctipo->type);
                    if ($ctipo == "D") $pr = $resultado -> UserDate($cc_r[$campos[$i]],'d/m/Y');
                    else $pr=$cc_r[$campos[$i]];

                    if (($caso == "LROTA") and ($campos[$i] == "TIPO")) {
                        if ($pr == 1) echo "<td><img src=\"imagens/icones/22x22/lubrificacao.png\" border=\"0\" alt=\"".$ling['tarefa_lubrificacao']."\" title=\"".$ling['tarefa_lubrificacao']."\" /></td>\n";
                        if ($pr == 2) echo "<td><img src=\"imagens/icones/22x22/inspecao.png\" border=\"0\" alt=\"".$ling['tarefa_inspecao']."\" title=\"".$ling['tarefa_inspecao']."\" /></td>\n";
                    }
                    elseif (($campos[$i] == "COD") or ($campos[$i] == "TAG")) {
                        echo "<td nowrap=\"nowrap\">".nl2br(htmlentities($pr))."</td>\n";
                    }
                    else {
                        if ($VoltaTipo != "") $_SESSION[ManuSess]['VoltaValor']=$VoltaTipo;

                        // Tabela relacionada
                        $rtb=VoltaRelacao($tabela,$campos[$i]);
                        if ($rtb != "") {
                            if (($campos[$i] == "MID_EMPRESA") and ($pr == 0)) {
                                echo "<td>".$ling['cadastro_global']."</td>\n";
                            }
                            elseif ($rtb['cod'] != "") {
                                echo "<td>".htmlentities(trim(VoltaValor($rtb['tb'],$rtb['cod'],$rtb['mid'],$pr,$rtb['dba']) . "-" . VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$pr,$rtb['dba'], 1, 0)))."</td>\n";
                            }
                            else {
                                echo "<td>".htmlentities(trim(VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$pr,$rtb['dba'])))."</td>\n";
                            }
                        }
                        elseif ($cc_r[$campos[$i]] == "") {
                            echo "<td>&nbsp;</td>\n";
                        }
                        else {
                            echo "<td>".nl2br(htmlentities($pr))."</td>\n";
                        }
                    }
                }
            }
            echo "<td nowrap=\"nowrap\" width=\"150\">\n";

            if ($editar == 1) {
                if (($caso == "LROTA") and ($cc_r['TIPO'] == 1)) {
                    echo "<a href=\"javascript:abre_janela_form('LINK_LUBRIFICACAO','".$cc_r[$mid]."','$oq','2','$id','$exe','$op',".$form['LINK_LUBRIFICACAO'][0]['altura'].",".$form['LINK_LUBRIFICACAO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
                }
                elseif (($caso == "LROTA") and ($cc_r['TIPO'] == 2)) {
                    echo "<a href=\"javascript:abre_janela_form('LINK_INSPECAO','".$cc_r[$mid]."','$oq','2','$id','$exe','$op',".$form['LINK_INSPECAO'][0]['altura'].",".$form['LINK_INSPECAO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
                }
                elseif (($caso == "FUNC") and ($cc_r['FORNECEDOR'] != 0)) {
                    echo "<a href=\"javascript:abre_janela_form('FUNCIONARIO2','".$cc_r[$mid]."','$oq','2','$id','$exe','$op',".$form['FUNCIONARIO2'][0]['altura'].",".$form['FUNCIONARIO2'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
                }
                elseif (($caso == "FUNC") and ($cc_r['FORNECEDOR'] == 0)) {
                    echo "<a href=\"javascript:abre_janela_form('FUNCIONARIO','".$cc_r[$mid]."','$oq','2','$id','$exe','$op',".$form['FUNCIONARIO'][0]['altura'].",".$form['FUNCIONARIO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
                }
                elseif ($caso == 'MAQMAT') {
                    echo "<a href=\"javascript:abre_janela_linka_mat('$oq', '{$cc_r[$mid]}')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
                }
                elseif($caso == 'LOCA1')
                {
                    $qtdMaquinas = volta_qtd_maquianas_areas((int) $cc_r[$mid]);

                    if($qtdMaquinas > 0)
                    {
                      echo '<img src="imagens/icones/22x22/pendencias.png" border="0" align="middle" title="N&atilde;o &eacute; possivel editar, pois a localiza&ccedil;&atilde;o 2 possui '. $qtdMaquinas .' objeto(s) cadastrado(s) &eacute; preciso fazer a movimenta&ccedil;&atilde;o dele(s) primeiro." >' . "\n";
                    }
                    else
                    {
                      echo "<a href=\"javascript:abre_janela_form('$formulario','".$cc_r[$mid]."','$oq','2','$id','$exe','$op',".$form[$formulario][0]['altura'].",".$form[$formulario][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
                    }
                }

                else echo "<a href=\"javascript:abre_janela_form('$formulario','".$cc_r[$mid]."','$oq','2','$id','$exe','$op',".$form[$formulario][0]['altura'].",".$form[$formulario][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/editar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['editar']." \" /></a>\n";
            }
            if ($copiar == 1) {
                if (($caso == "LROTA") and ($cc_r['TIPO'] == 1)) {
                    echo "<a href=\"javascript:abre_janela_form('LINK_LUBRIFICACAO','".$cc_r[$mid]."','$oq','3','$id','$exe','$op',".$form['LINK_LUBRIFICACAO'][0]['altura'].",".$form['LINK_LUBRIFICACAO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/copiar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['copiar']." \" /></a>\n";
                }
                elseif (($caso == "LROTA") and ($cc_r['TIPO'] == 2)) {
                    echo "<a href=\"javascript:abre_janela_form('LINK_INSPECAO','".$cc_r[$mid]."','$oq','3','$id','$exe','$op',".$form['LINK_INSPECAO'][0]['altura'].",".$form['LINK_INSPECAO'][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/copiar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['copiar']." \" /></a>\n";
                }
                else echo "<a href=\"javascript:abre_janela_form('$formulario','".$cc_r[$mid]."','$oq','3','$id','$exe','$op',".$form[$formulario][0]['altura'].",".$form[$formulario][0]['largura'].",'','$relacao_valor')\"><img src=\"imagens/icones/22x22/copiar.png\" border=\"0\" align=\"middle\" alt=\" \" title=\" ".$ling['copiar']." \" /></a>\n";
            }

            if ($caso == "L3") echo "<a href=\"$nome_script?id=$id&op=$op&exe=6&oq=".$cc_r[$mid]."\"><img src=\"imagens/icones/22x22/detalha.png\" border=\"0\" alt=\"".$ling['detalha']."\" title=\" ".$ling['detalha']." \" align=\"middle\" /></a>";
            
            // Detalahamento do PLANO DE SPEED CHECK
            if ($caso == "PSC") {
            	echo "<a href=\"$nome_script?id=$id&op=$op&exe=2&oq=".$cc_r[$mid]."\"><img src=\"imagens/icones/22x22/detalha.png\" border=\"0\" alt=\"".$ling['detalha']."\" title=\" ".$ling['detalha']." \" align=\"middle\" /></a>";
            }
            
            if ($caso == "equip") echo "<a href=\"$nome_script?id=$id&op=$op&exe=7&oq=".$cc_r[$mid]."\"><img src=\"imagens/icones/22x22/detalha.png\" border=\"0\" alt=\"".$ling['detalha']."\" title=\" ".$ling['detalha']." \" align=\"middle\" /></a>";
            if ($caso == "PP") echo "<a href=\"$nome_script?id=$id&op=$op&exe=11&oq=".$cc_r[$mid]."&cont=0\"><img src=\"imagens/icones/22x22/atividades.png\" border=\"0\" alt=\"".$ling['atividades']."\" title=\" ".$ling['atividades']." \" align=\"middle\" /></a>";
            if ($caso == "PCHECK") echo "<a href=\"$nome_script?id=$id&op=$op&exe=11&oq=".$cc_r[$mid]."\"><img src=\"imagens/icones/22x22/checklist_ativ.png\" border=\"0\" alt=\"".$ling['atividades']."\" title=\" ".$ling['atividades']." \" align=\"middle\" /></a>
                                    &nbsp; <a href=\"relatorios/imprimechecklist.php?mid=".$cc_r[$mid]."\" target=\"checklist\"><img src=\"imagens/icones/22x22/imprimir.png\" border=\"0\" alt=\"".$ling['imprimir_checklist']."\" title=\" ".$ling['imprimir_checklist']." \" align=\"middle\" /></a>";
            if ($caso == "PCHECKATIV") echo "<a href=\"javascript:abre_janela_arq('checklist','{$cc_r[$mid]}',$id)\"><img src=\"imagens/icones/22x22/anexar.png\" border=\"0\" title=\"" . $ling['anexar_arq'] . "\" alt=\"" . $ling['anexar_arq'] . "\" align=\"middle\" /></a>";
            if ($caso == "PCHECKP") {
                if ($cc_r['STATUS'] == '0') echo "<a href=\"$nome_script?id=$id&op=$op&exe=$exe&oq=".$cc_r[$mid]."&cancela=1\" onclick=\"return confirma(this, '".$ling['checklist_programacao_cancela_confirma']."')\"><img src=\"imagens/icones/22x22/prog_del.png\" border=\"0\" title=\" ".$ling['checklist_programacao_cancela']." \" align=\"middle\" alt=\" \" /></a>\n";
            }
            if ($caso == "PPRED") echo "<a href=\"$nome_script?id=$id&op=$op&exe=31&oq=".$cc_r[$mid]."\"><img src=\"imagens/icones/22x22/rota.png\" border=\"0\" alt=\"".$ling['def_rota']."\" title=\" ".$ling['def_rota']." \" align=\"middle\" /></a>";
            if ($caso == "PREVATIV") echo "<a href=\"javascript:abre_janela_arq('planopadrao','{$cc_r[$mid]}', $id)\"><img src=\"imagens/icones/22x22/anexar.png\" border=\"0\" title=\"" . $ling['anexar_arq'] . "\" alt=\"" . $ling['anexar_arq'] . "\" align=\"middle\" /></a>";
            if ($caso == "ROTA") echo "<a href=\"$nome_script?id=$id&op=$op&exe=11&oq=".$cc_r[$mid]."&cont=0\"><img src=\"imagens/icones/22x22/rota.png\" border=\"0\" alt=\"".$ling['def_rota']."\" title=\" ".$ling['def_rota']." \" align=\"middle\" /></a>";
            if ($caso == "PROG") echo "<a href=\"$nome_script?id=$id&op=$op&exe=21&oq=".$cc_r[$mid]."\"><img src=\"imagens/icones/22x22/recursos.png\" border=\"0\" alt=\"".$ling['prog_recursos']."\" title=\" ".$ling['prog_recursos']." \" align=\"middle\" /></a>";
            if ($caso == "MAT") echo "<a href=\"javascript:abre_janela_arq('materiais','".$cc_r[$mid]."', $id)\"><img src=\"imagens/icones/22x22/anexar.png\" border=\"0\" title=\"" . $ling['anexar_arq'] . "\" alt=\"" . $ling['anexar_arq'] . "\" align=\"middle\" /></a>";
            elseif (($caso == "SOLP") and (VoltaPermissao($id, $op) == 1)) {
                $tsol=VoltaValor(SOLICITACAO_RESPOSTA,"MID","MID_SOLICITACAO",$cc_r[$mid],$tdb[SOLICITACAO_RESPOSTA]['dba']);
                if ($tsol == "") echo "<a href=\"javascript:abre_janela_form('SOLICITACAO_RESP','','$oq','1','$id','$exe','$op',".$form['SOLICITACAO_RESP'][0]['altura'].",".$form['SOLICITACAO_RESP'][0]['largura'].",'','".$cc_r[$mid]."')\"><img src=\"imagens/icones/22x22/solicitacao_resp.png\" border=0 align=\"middle\" alt=\" ".$ling['responder']." \" title=\" ".$ling['responder']." \" />  </a>\n";
                if ($cc_r['STATUS'] == 0) echo "<a href=\"$nome_script?id=$id&op=$op&exe=$exe&oq=".$cc_r[$mid]."&rejeita_msg=".$cc_r[$mid]."\" onclick=\"return confirma(this, '".$ling['deseja_rejeitar_solic']."')\"><img src=\"imagens/icones/22x22/solicitacao_rejeitada.png\" border=\"0\" title=\" ".$ling['rejeitar']." \" align=\"middle\" alt=\"".$ling['rejeitar']."\" /> </a>\n";
                if ($cc_r['STATUS'] == 0) echo "<a href=\"javascript:abre_janela_sol('".$cc_r[$mid]."')\"><img src=\"imagens/icones/22x22/solicitacao_aceita.png\" border=0 align=\"middle\" alt=\" ".$ling['aceitar_programar']." \" title=\" ".$ling['aceitar_programar']." \" />  </a>\n";
            }
            elseif (($caso == "REQP") and (VoltaPermissao($id, $op) == 1)) {
                $tsol=VoltaValor(REQUISICAO_RESPOSTA,"MID","MID_REQUISICAO",$cc_r[$mid],$tdb[REQUISICAO_RESPOSTA]['dba']);
                if ($tsol == "") echo "<a href=\"javascript:abre_janela_form('REQUISICAO_RESP','','$oq','1','$id','$exe','$op',".$form['REQUISICAO_RESP'][0]['altura'].",".$form['REQUISICAO_RESP'][0]['largura'].",'','".$cc_r[$mid]."')\"><img src=\"imagens/icones/22x22/solicitacao_resp.png\" border=0 align=\"middle\" alt=\" ".$ling['responder']." \" title=\" ".$ling['responder']." \" />  </a>\n";
                echo "<a href=\"$nome_script?id=$id&op=$op&exe=$exe&oq=".$cc_r[$mid]."&rejeita_msg=".$cc_r[$mid]."\" onclick=\"return confirma(this, '".$ling['deseja_rejeitar_requisicao']."')\"><img src=\"imagens/icones/22x22/solicitacao_rejeitada.png\" border=\"0\" title=\" ".$ling['rejeitar']." \" align=\"middle\" alt=\"".$ling['rejeitar']."\" /> </a>\n";
                echo "<a href=\"$nome_script?id=$id&op=$op&exe=$exe&oq=".$cc_r[$mid]."&aceita_msg=".$cc_r[$mid]."\" onclick=\"return confirma(this, '".$ling['deseja_aceitar_requisicao']."')\"><img src=\"imagens/icones/22x22/solicitacao_aceita.png\" border=0 align=\"middle\" alt=\" ".$ling['aceitar']." \" title=\" ".$ling['aceitar']." \" />  </a>\n";
            }


            if ($remover == 1) {
				if($tabela == EQUIPAMENTOS_FAMILIA AND $cc_r[$mid] == 36){
				  // Nao remover	
				}
				elseif($tabela == MAQUINAS_FAMILIA AND $cc_r[$mid] == 74){
					// Nao remover
				}
				else{
            		echo "<a href=\"javascript: confirma_delete('$tabela', '".$cc_r[$mid]."');\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" title=\" ".$ling['remover']." \" align=\"middle\" alt=\" \" /></a>\n";
				}
            }

            echo "</td></tr>\n";
            if ($caso == "SOLP") {
                $sol_resposta_user=VoltaValor(SOLICITACAO_RESPOSTA,"MID","MID_SOLICITACAO",$cc_r[$mid],0);
                if ($sol_resposta_user) {
                    echo "<tr bgcolor=\"#d4d4d4\"><td>&nbsp;</td><td colspan=\"10\"> -> ".$ling['resposta']." : ".htmlentities(VoltaValor(SOLICITACAO_RESPOSTA,"TEXTO","MID_SOLICITACAO",$cc_r[$mid],0))."</td></tr>";
                }
                $tmpsol=$dba[$conexao] -> Execute("SELECT MID FROM ".PROGRAMACAO." WHERE MID_PLANO = '".$cc_r[$mid]."' AND TIPO = '4'");
                if (!$tmpsol) erromsg($dba[$conexao] -> ErrorMsg());
                else $sol_prog=$tmpsol->fields['MID'];
                if ($sol_prog != "") {
                    $os_num=VoltaValor(ORDEM,"NUMERO","MID_PROGRAMACAO",$sol_prog,0);
                    $os_st=VoltaValor(ORDEM,"STATUS","MID_PROGRAMACAO",$sol_prog,0);
                    $os_prazo=NossaData(VoltaValor(ORDEM,"DATA_PROG","MID_PROGRAMACAO",$sol_prog,0));
                    $os_data=NossaData(VoltaValor(ORDEM,"DATA_FINAL","MID_PROGRAMACAO",$sol_prog,0));
                    /*if (!$os_num) {
                     $dba[0] -> Execute("UPDATE ".SOLICITACOES." SET STATUS = '0' WHERE MID = '".$cc_r[$mid]."'");
                     $dba[0] -> Execute("DELETE ".PROGRAMACAO." WHERE MID_PLANO = '".$cc_r[$mid]."'");
                     }*/
                    if (($os_num) and ($os_st == 1)) echo "<tr bgcolor=\"#d4d4d4\"><td>&nbsp;</td><td colspan=\"10\"> -> ".$ling['ordem_servico']."  : $os_num &nbsp;&nbsp;Prazo: $os_prazo - <a href=\"javascript:janela('detalha_sol.php?busca=$sol_prog', 'parm', 500,400)\">".$ling['clique_aqui_detalhar']."<a/></td></tr>";
                    if (($os_num) and ($os_st == 2)) echo "<tr bgcolor=\"#d4d4d4\"><td>&nbsp;</td><td colspan=\"10\"> -> ".$ling['ordem_servico']."  : $os_num <strong> (".$ling['servico_sealizado_prazo'].": $os_prazo &nbsp;&nbsp;".$ling['data_execucao'].": $os_data ) - <a href=\"javascript:janela('detalha_sol.php?busca=$sol_prog', 'parm', 500,400)\">".$ling['clique_aqui_detalhar']."<a/></strong></td></tr>";
                    if (($os_num) and ($os_st == 3)) echo "<tr bgcolor=\"#d4d4d4\"><td>&nbsp;</td><td colspan=\"10\"> -> ".$ling['ordem_servico']."  : $os_num <strong> (".$ling['servico_cancelado'].") - <a href=\"javascript:janela('detalha_sol.php?busca=$sol_prog', 'parm', 500,400)\">".$ling['clique_aqui_detalhar']."<a/> </strong></td></tr>";

                }
            }
            $ii++;
            unset($tdisparo);
            unset($disparo);
            $resultado -> MoveNext();
            if ($tr_cor == 0) $tr_cor=1;
            else $tr_cor=0;
        }
        echo "</table></div>\n";
        list($xseg, $seg) = explode(" ", microtime());
        $t2=((float)$xseg + (float)$seg);
        $tempo=round(($t2 - $t1), 3);
        $tempo=str_replace(",",".",$tempo);
        if ($rodape == 1) {
            echo "<div id=\"lt_rodape\">".$ling['registros_encontrados'].": $nnlinhas ($tempo)\n";

            if (VoltaPermissao($id, $op) == 1) echo " <strong><a href=\"javascript:janela('logs.php?id=$tabela', 'parm', 500,400)\">".$ling['ultimas_alteracoes']."</a></strong>";

            if ($caso == "PEN") {
                echo "<br /><strong><i>".$ling['linhas_amarelo_pendencias']."</i></strong>";
            }

            echo "</div>\n";
        }
    }
    echo "</div>\n";
}





function ListaDirDoc ($tipo, $d, $titulo) {
    global $manusis,$dba,$ling,$id,$op,$exe,$oq,$f;
    $nome_script=$_SERVER['PHP_SELF'];
    $dir=$manusis['dir'][$tipo]."/$d";
    if (!is_dir($manusis['dir'][$tipo])) erromsg($ling['diretorio_nao_configurado']);
    echo "\n
    <div id=\"lt_tabela\">
<h2>$titulo</h2>
<table>

<tr><th>".$ling['arquivo']."</th><th>".$ling['tamanho']."</th><th></th></tr>";
    if (!file_exists($dir)) {
        @mkdir($dir);
        chmod ($dir, 0777);
    }
    $od = opendir($dir);
    $i=0;

    while (false !== ($arq = readdir($od))) {
        $dd=explode(".",$arq);
        if (($dd[1] == "jpg") or ($dd[1] == "png")) {
            echo "<tr class=\"cor1\">
                <td align=\"left\"><img src=\"i.php?i=$dir/$arq&s=120\" border=0 hspace=5 align=\"middle\" \><a href=\"$dir/$arq\" target=\"_blank\">$arq</a></td>
                <td align=\"left\">".de_bytes(filesize("$dir/$arq"))."</td>
                <td align=\"center\">";

            if (VoltaPermissao($id, $op) != 3) {
                echo "<a href=\"form.php?idf=arq&oq=$d&deleta=$tipo&deletaarq=$arq&f=$f\" onclick=\"return confirma(this, '".$ling['deseja_remover_arquivo']."')\"><img src=\"imagens/icones/22x22/del.png\" border=0 /></a>";
            }

            echo "</td></tr>\n";
            $i++;
        }
    }
    closedir($od);
    $od = opendir($dir);

    while (false !== ($arq = readdir($od))) {
        $dd=explode(".",$arq);
        if ($dd[1] == "pdf") $icone="pdf.png";
        elseif ($dd[1] == "doc") $icone="xls.png";
        elseif ($dd[1] == "xls") $icone="png.png";
        elseif (($dd[1] == "zip") or ($dd[1] == "rar") or ($dd[1] == "tar")) $icone="tar.png";
        else $icone="doc.png";
        if (($dd[1] != "jpg") and ($dd[1] != "png") and ($dd[1] != "")) {
            echo "<tr class=\"cor1\">
                <td align=\"left\"><img src=\"imagens/icones/22x22/$icone\" border=\"0\" hspace=\"5\" alt=\" \" align=\"middle\" \> <a href=\"$dir/$arq\" target=\"_blank\">$arq</a></td>
                <td align=\"left\">".de_bytes(filesize("$dir/$arq"))."</td>
                <td align=\"center\">";

            if (VoltaPermissao($id, $op) != 3) {
                echo "<a href=\"form.php?idf=arq&oq=$d&deleta=$tipo&deletaarq=$arq&f=$f\" onclick=\"return confirma(this, '".$ling['deseja_remover_arquivo']."')\"><img src=\"imagens/icones/22x22/del.png\" border=0 /></a>";
            }

            echo "</td></tr>\n";
            $i++;
        }
    }
    if ($i == 0) echo "<tr><td colspan=\"3\" align=\"center\"><address>".$ling['err06']."</address></td></tr>";
    echo "</table></div>";
}

/**
 * Cria um "select" para sele��o multipla.
 * Na verdade o que ele faz � agrupar em uma caixa alguns checkbox seguindo o padr�o de nomes:
 * $campo_nome[numero sequencial]
 *
 * @param string $campo1 Primeiro campo a ser mostrado
 * @param string $campo2 Segundo campo a ser mostrado
 * @param string $tabela Tabela a ser listada, utilize a constante do Manusis
 * @param array  $sel_valor Array com os valores ja selecionados
 * @param string $campo_nome Nome inicial dos campos
 * @param string $campo_id Id inicial dos campos
 * @param string $mid Campo a ser usado como MID da tabela a ser listada
 * @param int    $null 0 = NOT NULL, 1 = NULL
 * @param string $sql_filtro Filtro sql a ser aplicado na tabela a ser listada
 * @param string $ordem ORDER BY a ser aplicado ao select
 * @param int $larguraDiv Largura da div
 * @param int $alturaDiv  Altura da div
 * @return void
 */
function FormSelectMult($campo1, $campo2, $tabela, $sel_valor, $campo_nome, $campo_id, $mid, $null=0, $sql_filtro="", $ordem="", $formulario = "", $larguraDiv = "270", $alturaDiv = "50"){
    global $manusis,$dba,$id,$tdb,$oq,$exe,$op,$form,$ling;

    if(! is_array($sel_valor)){
        $sel_valor= array ($sel_valor);
    }

    $con=(int)$tdb[$tabela]['dba'];
    if ($tdb[$tabela]['dba']){
        $msg_erro.=$ling['tabela_nao_configurado']." ";
    }
    if ($ordem == "") {
        $ordem=$campo1;
    }

    if ($campo2 != ""){
        $sql_campos.="$campo1,$campo2,";
    }
    else {
        $sql_campos.="$campo1,";
    }
    $sql_campos.="$mid";

    $sql="SELECT $sql_campos FROM $tabela $sql_filtro ORDER BY $ordem ASC";

    $resultado=$dba[$con] -> Execute($sql);
    if(!$resultado) {
        $msg_erro.="-".$dba[$con] -> ErrorMsg()."<br />SQL: $sql";
    }

    if ($msg_erro) {
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$con] -> ErrorMsg() . "<br />" . $sql);
    }

    $class = ($null == 1)? "campo_text" : "campo_text_ob";

    // Montando o div que ser�o o conteiner
    echo "<fieldset style=\"padding:0px; border:0px; width:300px;\">";

    // Caso o formulario tenha sido passado
    if ((VoltaPermissao($id, $op) == 1) and ($formulario != "")) {
        echo "<a href=\"javascript:janela('form.php?id=$id&f=$formulario&act=1&form_parm=1&atualiza=$campo_id', 'parm', ".$form[$formulario][0]['largura'].",".$form[$formulario][0]['altura'].")\" style=\"float:right\"><img src=\"imagens/icones/add.png\" border=\"0\"></a>";
    }


    echo "<div class=\"$class\" style=\"vertical-align:top;text-align:left; width:{$larguraDiv}px; height:{$alturaDiv}px;overflow-y:scroll;\">";

    $i = 0;
    while (! $resultado->EOF) {
        $cc = $resultado->fields;

        if ($i > 0) {
            echo "<br clear=\"all\" />";
        }

        $desc = htmlentities($cc[$campo1]);

        $desc .= ($campo2)? "-" . htmlentities($cc[$campo2]) : "";

        $checked = (in_array($cc[$mid], $sel_valor))? "checked=\"cheked\"" : "";


        echo "<input type=\"checkbox\" style=\"position:static;float:none;\" name=\"{$campo_nome}[{$i}]\" id=\"{$campo_id}[{$i}]\" value=\"{$cc[$mid]}\" $checked />
        <label style=\"float:none;widht:100%;margin:0px;\" for=\"{$campo_id}[{$i}]\">$desc</label>";


        $resultado->MoveNext();
        $i++;
    }


    echo "</div>

    </fieldset>";
}


//# FORMULARIO

/**
 * Gera dinamicamente um <select>
 * Trata de forma independente algumas tabelas, como por exemplo os objetos de manuten��o, materiais e equipamentos.
 * Essa fun��o usa recursos do arquivo parametros.php para relizar pesquisas dinamicas.
 *
 * Recursos Atuais:
 *
 *  Mostra 40 caracteres do campo, impedindo que o<select> fiquei maior que o formulario do sistema
 *
 *  Caso a tabela tenha mais de 300 registro, a consulta inicial(que apresenta toda tabela) � ignorada e o campo de pesquisa aparece automaticamente com uma mensagem para o usu�rio fazer sua pesquisa. Isso impede que cadastros imensos como de materiais, fiquem travando em conex�es mais lentas.
 *
 *  Se o parametro $auto_filtro for setado como "A" o campo de filtro s� ira aparecer para tabelas com mais de 50 itens, economizando espa�o.
 *
 * @param string $campo1 Primeiro campo a ser apresetando ex: COD
 * @param string $campo2 Segundo opcional, campo ex: DESCRICAO
 * @param string $tabela Tabela do Banco de DAdos
 * @param string $sel_valor Valor inicial, o sistema ira deixar selecionado como default essa entrada.
 * @param string $campo_nome Nome do campo <select>
 * @param string $campo_id ID do campo <select>
 * @param string $mid MID (Manusis ID) ou CHAVE da tabela
 * @param string $formulario Formulario caso tenha um cadastro auxiliar (sinal de +)
 * @param string $classe Classe CSS
 * @param string $acao A��o em javascript, pode ser uma chamada para um ajax, lembre-se de colocar o evento
 * @param string $sql_filtro Caso queira adicinar um filtro especial para a consulta.
 * @param string $auto_filtro Adiciona o campo de filtro, A = Automatico, S = Sempre, N = Sem Filtro o default �
 * @param string $ordem Campo que ser� ordenada a consulta
 * @param string $primeirocampo Texto que ira no primeiro campo do <select>
 */
function FormSelectD($campo1, $campo2, $tabela, $sel_valor, $campo_nome, $campo_id, $mid, $formulario, $classe = "", $acao = "", $sql_filtro = "", $auto_filtro = "A", $ordem = "", $primeirocampo = "", $campo_grupo = "", $caso = ""){
    global $manusis, $dba, $id, $tdb, $oq, $exe, $op, $form, $ling, $recb_valor, $atualiza_ajax, $texto_ajax;
    
    $linhas_limite = 300;

    if ($auto_filtro == "") {
        $auto_filtro="A";
    }

    //if (!$primeirocampo) $primeirocampo = mb_strtoupper($ling["todos"]);
    // Para cadastros com op��o de empresa global
    if (($tabela == EMPRESAS) and ($classe != "campo_select_ob")) {
        $primeirocampo = $ling['cadastro_global'];
    }

    $con=(int)$tdb[$tabela]['dba'];
    if ($tdb[$tabela]['dba']){
        $msg_erro.=$ling['tabela_nao_configurado']." ";
    }
    if ($ordem == "") {
        $ordem=$campo1;
    }

    if($campo_grupo != '') {
        $ordem = "$campo_grupo, $ordem";
    }

    // FILTRO POR STATUS DE FUNCIONARIOS
    if ($tabela == FUNCIONARIOS){
        $sql_filtro .= ($sql_filtro == "")? " WHERE " : " AND ";
        $sql_filtro .= "SITUACAO = 1";
    }

    // CAMPO DE MAQUINAS NA TELA DE SOLICITA��O
    if ($caso == "MAQSOL") {
        $perm_cc = array();
        // Apenas para usu�rios
        if ($_SESSION[ManuSess]['user']['MID'] != 'ROOT') {
            // BUSCANDO AS MAQUINAS NOS CENTRO DE CUSTOS COM PERMISS�O
            $sql = "SELECT CENTRO_DE_CUSTO FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}'";
            if (! $tmp = $dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . " <br />Linha: " . __LINE__ . " <br />" .$dba[$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['dba']] -> ErrorMsg() . "<br />" . $sql);
            }
            while (! $tmp->EOF) {
                $perm_cc[] = $tmp -> fields['CENTRO_DE_CUSTO'];
                $tmp -> MoveNext();
            }

            if (count($perm_cc) > 0) {
                $sql_filtro .= ($sql_filtro == "")? " WHERE " : " AND ";
                $sql_filtro .= "CENTRO_DE_CUSTO IN (" . implode(", ", $perm_cc) . ")";
            }
        }
    }

    //FILTRO POR EMPRESA
    $filtro = VoltaFiltroEmpresa($tabela);
    if (count($filtro) > 0 and $filtro['mid']) {
        $sql_filtro .= ($sql_filtro != "")? " AND " : " WHERE ";
        $sql_filtro .= "(" . $filtro['campo'] . " IN (" . implode(', ', $filtro['mid']) . ")";
        // Usado quando o campo n�o � obrigatorio
        if ($filtro['tipo'] == 2) {
            $sql_filtro  .= " OR " . $filtro['campo'] . " = 0 OR " . $filtro['campo'] . " IS NULL";
        }
        $sql_filtro  .= ")";
    }

    // Select
    $sql = "SELECT $mid FROM $tabela $sql_filtro ORDER BY $ordem ASC";
    $n_registros=$dba[$con] -> Execute($sql);
    if(!$n_registros) {
        $msg_erro.="Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" .$dba[$con] -> ErrorMsg() . "<br />" . $sql;
    }
    else {
        $n_registros=count($n_registros -> getrows());
    }

    if ($texto_ajax != "") {
        $sql_filtro .= ($sql_filtro == "")? " WHERE " : " AND ";
        if ($campo2) {
            $sql_filtro.="($campo1 LIKE '%$texto_ajax%' OR $campo2 LIKE '%$texto_ajax%')";
        }
        else {
            $sql_filtro.="$campo1 LIKE '%$texto_ajax%'";
        }

    }

    // Caso ajam mais de 300 registros e um valor foi selecionado
    $sql_filtro2 = "";
    if(($n_registros > $linhas_limite) and ($texto_ajax == "") and ($sel_valor != "")) {
        $sql_filtro2 = ($sql_filtro == "")? " WHERE " : " AND ";
        $sql_filtro2 .= "$mid = $sel_valor";
    }

    // Se n�o possui uma classe colocamos uma padrao
    if ($classe == "") $classe="campo_select";
    if (!$atualiza_ajax) {
        echo "<span id=\"select_$campo_id\">";
    }
    echo "<select class=\"$classe\" name=\"$campo_nome\" id=\"$campo_id\" onchange=\"$acao\">";
    
    if(!in_array($tabela, array(MAQUINAS_STATUS, EQUIPAMENTOS_STATUS))) 
        echo  "<option value=\"0\">$primeirocampo</option>\n";

    if ($campo2 != ""){
        $sql_campos.="$campo1, $campo2, ";
    }
    else {
        $sql_campos.="$campo1, ";
    }

    // Campo para agrupar
    if($campo_grupo != "") {
        $sql_campos .= "$campo_grupo, ";
    }

    // Adicionando o MID
    $sql_campos .= "$mid";

    $sql="SELECT $sql_campos FROM $tabela $sql_filtro $sql_filtro2 ORDER BY $ordem ASC";
    $resultado=$dba[$con] -> Execute($sql);
    if(!$resultado) {
        $msg_erro .= "-".$dba[$con] -> ErrorMsg().". SQL: $sql";
    }
    if ($msg_erro) {
        echo "<option value=\"0\">".$ling['err02']."</option></select>";
        erromsg($msg_erro);
    }
    elseif (($n_registros > $linhas_limite) and ($texto_ajax == "") and ($sel_valor == "")) {
        echo "<option value=\"0\">".$ling['use_campo_pesquisa']."</option></select>";
    }
    else {
        $grupo_ant = null;
        while (!$resultado->EOF) {
            $rcampo = $resultado -> fields;
            switch ($tabela){
                case MAQUINAS:
                    $maq_status=VoltaValor(MAQUINAS,"STATUS","MID",$rcampo[$mid],$con);
                    if ($maq_status == 2) {
                        $op_cor=" style=\"color:red\" ";
                    }
                    else {
                        $op_cor="";
                    }
            }

            // Agrupamento
            if (($campo_grupo != '') and ($rcampo[$campo_grupo] != $grupo_ant)) {
                if ($grupo_ant != null) {
                    echo "</optgroup>";
                }

                // Verificando se � um campo relacionado
                $rtb = VoltaRelacao($tabela, $campo_grupo);
                if($rtb != '') {
                    $grupo_desc = '';
                    if ($rtb['cod'] != '') {
                        $grupo_desc = VoltaValor($rtb['tb'], $rtb['cod'], $rtb['mid'], $rcampo[$campo_grupo]) . "-";
                    }

                    $grupo_desc .= VoltaValor($rtb['tb'], $rtb['campo'], $rtb['mid'], $rcampo[$campo_grupo], 0, 1, 0);
                }
                else {
                    $grupo_desc = $rcampo[$campo_grupo];
                }

                $grupo_title = htmlentities($grupo_desc);
                $grupo_desc = htmlentities(substr($grupo_desc, 0, 50));


                echo "<optgroup label=\"$grupo_desc\" title=\"$grupo_title\">";

                $grupo_ant = $rcampo[$campo_grupo];
            }



            $ap = $rcampo[$campo1];
            if ($campo2 != "") {
                $ap .= "-".$rcampo[$campo2];
            }

            $ap_inteiro=htmlentities($ap);
            $ap=htmlentities(substr($ap,0,50));
            $sc=$rcampo[$mid];

            if ($sel_valor == $sc) {
                echo "<option $op_cor value=\"$sc\" selected=\"selected\" title=\"$ap_inteiro\">$ap</option>\n";
            }
            else {
                echo "<option $op_cor value=\"$sc\" title=\"$ap_inteiro\">$ap</option>\n";
            }
            $resultado->MoveNext();
            $i++;
            unset($ap);
        }

        if ($campo_grupo != '') {
            echo "</optgroup>";
        }

        if (($n_registros > 300) and ($sel_valor != "")) {
            echo "<option value=\"0\">".$ling['use_campo_pesquisa']."</option>";
        }

        echo "</select>";
    }
    if (!$atualiza_ajax) {
        echo "</span>\n";

        if ((VoltaPermissao($id, $op) == 1) and ($formulario != "")) {
            echo "<a href=\"javascript:janela('form.php?id=$id&f=$formulario&act=1&form_parm=1&atualiza=$campo_id', 'parm', ".$form[$formulario][0]['largura'].",".$form[$formulario][0]['altura'].")\"><img src=\"imagens/icones/add.png\" border=\"0\"></a>";
        }
        if(($auto_filtro == "S") or (($auto_filtro == "A") and ($n_registros > 50))) {
            if ($n_registros > 300) {
                $tmp_vis="";
            }
            else {
                $tmp_vis="none";
            }
            //$sql_filtro = urlencode($sql_filtro);
            $tmp_ajax = urlencode($acao);
            echo "<a href=\"javascript:abre_div('filtro_$campo_id')\">
            <img src=\"".$manusis['url']."imagens/icones/16x16/ir.png\" border=\"0\" alt=\"Filtro\" title=\" Abrir/Fechar op&ccedil;&atilde;o de filtro \" />
            </a>
            <span id=\"filtro_$campo_id\" style=\"display:$tmp_vis;\">
            <input title=\"".$ling['pressione_enter']."\" onkeypress=\"return AutoCompletaSelect(this, event, 'select_$campo_id', '".$manusis['url']."parametros.php?id=formselect&campo1=$campo1&campo2=$campo2&tabela=$tabela&campo_nome=$campo_nome&campo_id=$campo_id&mid=$mid&sql_filtro=$sql_filtro&classe=$classe&acao=$tmp_ajax&ordem=$ordem&campo_grupo=$campo_grupo&caso=$caso&texto=')\" type=\"text\" id=\"campo_filtro_$campo_id\" class=\"campo_text\" value=\"\" name=\"campo_filtro_$campo_id\" size=\"10\" maxlength=\"75\" />
            </span>
            ";
        }
    }
}
function FormSelect($cc,$tb,$sel,$c1,$c2,$conexao,$formulario,$temCampoPesquisaDinamica='N',$tmp_class=""){
    global $dba,$id,$tdb,$oq,$exe,$op,$form,$recb_valor,$primeirocampo;

    $valorCampoPesquisaDinamica = $_GET['valorCampoPesquisaDinamica'];
    if ($tmp_class == "") $tmp_class="campo_select";
    echo "<select class=\"$tmp_class\" name=\"$cc\" id=\"$cc\"><option value=\"0\">$primeirocampo</option>\n";

    if(($temCampoPesquisaDinamica == "S")&&($valorCampoPesquisaDinamica!=""))
    {
        $query = "SELECT * FROM $tb WHERE $c1 LIKE '$valorCampoPesquisaDinamica%' ";

        switch (strtoupper($tb))
        {
            case 'AREAS':
            case 'MAQUINAS':
            case 'SETORES':
            case 'CENTRO_DE_CUSTO':
            case 'MATERIAIS':
            case 'MAQUINA_CONJUNTO':
            case 'EQUIPAMENTOS':
                $query .= " union SELECT * FROM $tb WHERE cod LIKE '$valorCampoPesquisaDinamica%' ";
                break;
        }

        $query .= " ORDER BY $c1 ASC";

        $resultado=$dba[$conexao] -> Execute($query);
    }
    else
    $resultado=$dba[$conexao] -> Execute("SELECT * FROM $tb ORDER BY $c1 ASC");

    if(!$resultado) {
        echo "<br><hr />".erromsg($dba[$conexao] -> ErrorMsg())."<hr /><br>";
    }
    else {
        $campo = $resultado -> fields;
        while (!$resultado->EOF) {
            $campo = $resultado -> fields;
            $sc2=$campo[$c2];
            if ($tb == MAQUINAS) {
                $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            }
            elseif ($tb == SETORES) $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            elseif ($tb == AREAS) $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            elseif ($tb == MAQUINAS_CONJUNTO) $sc1=htmlentities($campo['TAG']."-".$campo[$c1]);
            elseif ($tb == EQUIPAMENTOS) $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            else $sc1=htmlentities($campo[$c1]);
            if ($sel == $sc2) echo "<option value=\"$sc2\" selected>$sc1</option>\n";
            else echo "<option value=\"$sc2\">$sc1</option>\n";
            $resultado->MoveNext();
            $i++;
        }
        echo "</select>\n";

        if ($formulario != "")
        echo "<a href=\"javascript:janela('form.php?id=$id&f=$formulario&act=1&form_parm=1&atualiza=$cc', 'parm', ".$form[$formulario][0]['largura'].",".$form[$formulario][0]['altura'].")\"><img src=\"imagens/icones/add.png\" border=\"0\"></a>";

        if($temCampoPesquisaDinamica == "S")
        {
            echo "<input type=\"text\" id=\"valorCampoPesquisaDinamica\" class=\"campo_text\" value=\"$valorCampoPesquisaDinamica\" name=\"valorCampoPesquisaDinamica\" size=\"20\" maxlength=\"100\" />";
            echo "<input class=\"botao\" type=\"submit\" name=\"btPesquisaDinamica\" value=\"Filtrar\" />";
        }

    }
}

function FormData($label, $nomeObjeto, $valor, $class_label="", $java="", $class_campo="campo_text", $id_campo = ""){
    global $manusis;

    // Caso n�o seja informado
    if($id_campo == '') {
        $id_campo = $nomeObjeto;
    }

    // S� mostrar se preenchido
    if($label != '') {
        echo "\n<label class=\"$class_label\" for=\"$id_campo\">$label</label>\n";
    }

    echo "<input onkeypress=\"return ajustar_data(this, event)\" type=\"text\" id=\"$id_campo\" class=\"$class_campo\" value=\"$valor\" name=\"$nomeObjeto\" size=\"10\" maxlength=\"10\" $java/>\n ";

    echo "<a href=\"javascript:void(0);\"><img onclick=\"document.getElementById('dv$id_campo').style.visibility='visible';document.getElementById('dv$id_campo').src='{$manusis['url']}calendario.php?nomeObjeto=$id_campo';document.getElementById('dv$id_campo').style.height='200px';document.getElementById('dv$id_campo').style.width='200px';\" name=\"btPesquisar$id_campo\" type=\"image\" src=\"".$manusis['url']."imagens/icones/22x22/calendario.png\" width=\"16\" height=\"16\" border=\"0\"></a>";
    echo "<iframe scrolling=\"no\" id=\"dv$id_campo\" src=\"{$manusis['url']}calendario.php?nomeObjeto=$id_campo\" border=\"0\" style=\"border:0px;height:10px;width:10px; position:absolute; z-index:99999; visibility:hidden\"></iframe>\n";
}


// -------------------------------------------------------------
// FUN��ES DE CALCULOS E TRABALHO COM STRINGS
// -------------------------------------------------------------
function LimpaTexto ($texto) {
    if(! is_array($texto)) {
        $texto=strip_tags($texto);
        $alvos= array("'","\\",";","\"","SELECT","DROP ","`","*");
        $texto=str_replace($alvos, "", $texto);
    }
    else {
        foreach ($texto as $k => $texto_tmp) {
            $texto[$k] = LimpaTexto($texto_tmp);
        }
    }
    return $texto;
}
function de_bytes($size) {
    $kb = 1024;        // Kilobyte
    $mb = 1024 * $kb;  // Megabyte
    $gb = 1024 * $mb;  // Gigabyte
    $tb = 1024 * $gb;  // Terabyte
    if($size==0) return "n/a";
    if($size < $kb) {
        return $size." Bytes";
    } else if($size < $mb) {
        return round($size/$kb,2)." kb";
    } else if($size < $gb) {
        return round($size/$mb,2)." mb";
    } else if($size < $tb) {
        return round($size/$gb,2)." gb";
    } else {
        return round($size/$tb,2)." tb";
    }
}
function data_diff($from, $to) {
    list($from_month, $from_day, $from_year) = explode("-", $from);
    list($to_month, $to_day, $to_year) = explode("-", $to);
    $from_date = mktime(0,0,0,$from_month,$from_day,$from_year);
    $to_date = mktime(0,0,0,$to_month,$to_day,$to_year);
    $days = ($to_date - $from_date)/86400;
    return $days;
}
function rdatas($data_inicial,$data_final,$dataalvo,$freq) {
    global $dba,$tdb;
    $di=explode("-",$data_inicial);
    $df=explode("-",$data_final);
    $idia=$di[2];
    $imes=$di[1];
    $iano=$di[0];
    $fdia=$df[2];
    $fmes=$df[1];
    $fano=$df[0];
    $idata=mktime (0, 0, 0, $imes  , $idia, $iano);
    $fdata=mktime (0, 0, 0, $fmes  , $fdia, $fano);
    $data=$idata;
    if ($dataalvo == "$idia-$imes-$iano") $tmp=1;
    else {
        while ($data < $fdata) {
            $data=mktime (0, 0, 0, $imes  , $idia + $freq, $iano);
            $idia=date(d,$data);
            $imes=date(m,$data);
            $iano=date(Y,$data);
            if (date("d-m-Y",$data) == $dataalvo)  {
                $tmp=1;
                break;
            }
        }
    }
    return $tmp;
}
function SomaHora($horarray) {

    $horasoma = 0;
    if (is_array($horarray)) foreach ($horarray as $estahora) {
        list($ehora,$emin,$esec) = split(':',$estahora,3);
        $horasoma += ($esec + 60*($emin + 60*($ehora)));
    }
    $rhora = floor($horasoma/3600);
    $horasoma -= 3600*$rhora;
    $rmin = floor($horasoma/60);
    $horasoma -= 60*$rmin;
    if ($rmin < 10) $rmin = "0$rmin";
    if ($horasoma < 10) $horasoma = "0$horasoma";
    $horasoma = "$rhora:$rmin:$horasoma";
    return $horasoma;
}
function AcertaHora($decimal) {
    $inteiro = floor($decimal);
    $parte = $decimal - $inteiro;
    $minuto = 60*$parte;
    $horacerta = "$inteiro:$minuto";
    return $horacerta;
}
function DataSQL($nossadata) {
    if(strpos($nossadata, '/') !== false) {
        $d=explode("/",$nossadata);
        return $d[2]."-".$d[1]."-".$d[0];
    }

    return $nossadata;
}
function NossaData($datasql) {
    if(strpos($datasql, '-') !== false) {
        $d=explode("-",$datasql);
        return $d[2]."/".$d[1]."/".$d[0];
    }

    return $datasql;
}
function VoltaTime($hora,$data) {
    $h=explode(":",$hora);
    $d=explode("/",$data);
    return mktime((int)$h[0],(int)$h[1],(int)$h[2],(int)$d[1],(int)$d[0],(int)$d[2]);
}
/**
 * Concatena a string $concatena na string $str, mas caso a string $str n�o esteja vazia,
 * adiciona a string $separador entre elas. Salva o resultado em $str.
 *
 * @param string $str String inicial a ser trabalhada
 * @param string $separador Separador opcional caso $str n�o esteja vazia
 * @param string $concatena String a ser adicionada
 * @return string
 */
function AddStr(&$str,$separador,$concatena) {
    $tmp = $str;
    if (strlen($tmp)) {
        $tmp .= $separador;
    }
    $tmp .= $concatena;
    $str = $tmp;
    return $tmp;
}
/**
 * Abrevia uma string
 * retorna as primeiras $num_palavras abreviadas em $num_carac caracteres
 * exemplo: Abreviate('Fulano da Silva Sauro',3,3)
 * retorna 'Ful. da Sil.' (3 palavras, 3 carac)
 *
 * @param string $str String inicial a ser abreviada
 * @param string $num_palavras Quantidade de palavras a considerar
 * @param string $num_carac Quantidade maxima de caracteres em cada palavra
 * @param bool $remove_prep = false Remove "da", "de", "do", "a", "e", "o", "para", "na", "no", "em"
 * @param bool $sigla = false Retorna os caracteres juntos, sem ponto nem espan��o
 * @return string
 */
function Abreviate($str, $num_palavras, $num_carac, $remove_prep = false, $sigla = false) {
    $palavras_tmp = explode(' ',$str);
    $palavras = array();
    if ($remove_prep) {
        // lista de palavras n�o incluidas
        $lista = array("da", "de", "do", "a", "e", "o", "para", "na", "no", "em");
        foreach ($palavras_tmp as $epalavra) {
            $epal = strtolower($epalavra);
            if (!in_array($epal,$lista)) $palavras[] = $epalavra;
        }
    }
    else $palavras = $palavras_tmp;
    $retorno='';
    for ($i=0; $i<$num_palavras; $i++) {
        $tmp = substr($palavras[$i],0,$num_carac); // recorta primeiros caracteres
        if (($tmp != $palavras[$i]) and (!$sigla)) $tmp .= '.'; // se cortou peda�o da palavra, poe um ponto
        // adiciona em retorno
        if (($retorno) and (!$sigla)) $retorno .= ' ';
        $retorno .= $tmp;
    }
    return $retorno;
}
function ChecaHora($hora) {
    list($h, $m, $s) = split(':',$hora,3);
    $h = (int)$h;
    $m = (int)$m;
    $s = (int)$s;
    if (($h >= 0) and ($h <= 23) and ($m >= 0) and ($m <= 59) and ($s >= 0) and ($s <= 59)) return true;
    else return false;
}
// contrario de htmlentities, retirada de www.php.net/htmlentities
function unhtmlentities ($string)
{
    $trans_tbl = get_html_translation_table (HTML_ENTITIES);
    $trans_tbl = array_flip ($trans_tbl);
    return strtr ($string, $trans_tbl);
}
/**
 * Vers�o de time() com microssegundos em formato float
 *
 * @return float
 */
function utime(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function TurnoTotalGeral($datai, $dataf) {
    global $dba, $tdb, $ling, $manusis;
    // Total
    $total = 0;

    $filtro_emp = VoltaFiltroEmpresa(FUNCIONARIOS, 2);
    if ($filtro_emp != '') {
        $filtro_emp = "AND $filtro_emp";
    }

    // Buscando os funcionarios da Equipe
    $sql = "SELECT MID FROM " . FUNCIONARIOS . " WHERE SITUACAO = 1 $filtro_emp";
    if(! $rs = $dba[$tdb[FUNCIONARIOS]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[FUNCIONARIOS]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $total += TurnoTotal($datai, $dataf, $rs->fields['MID']);
        $rs->MoveNext();
    }

    return $total;
}

function TurnoTotalEspecialidade($datai, $dataf, $mid_esp) {
    global $dba, $tdb, $ling, $manusis;
    // Total
    $total = 0;

    $filtro_emp = VoltaFiltroEmpresa(FUNCIONARIOS, 2);
    if ($filtro_emp != '') {
        $filtro_emp = "AND $filtro_emp";
    }

    // Buscando os funcionarios da Equipe
    $sql = "SELECT MID FROM " . FUNCIONARIOS . " WHERE ESPECIALIDADE = $mid_esp $filtro_emp";
    if(! $rs = $dba[$tdb[FUNCIONARIOS]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[FUNCIONARIOS]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $total += TurnoTotal($datai, $dataf, $rs->fields['MID']);
        $rs->MoveNext();
    }

    return $total;
}

function TurnoTotalEquipe($datai, $dataf, $mid_equipe) {
    global $dba, $tdb, $ling, $manusis;
    // Total
    $total = 0;

    $filtro_emp = VoltaFiltroEmpresa(FUNCIONARIOS, 2);
    if ($filtro_emp != '') {
        $filtro_emp = "AND $filtro_emp";
    }

    // Buscando os funcionarios da Equipe
    $sql = "SELECT MID FROM " . FUNCIONARIOS . " WHERE EQUIPE = $mid_equipe $filtro_emp";
    if(! $rs = $dba[$tdb[FUNCIONARIOS]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[FUNCIONARIOS]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    while(! $rs->EOF) {
        $total += TurnoTotal($datai, $dataf, $rs->fields['MID']);
        $rs->MoveNext();
    }

    return $total;
}

//# CALCULOS USANDO DADOS DO BANCO
function TurnoTotal($datai,$dataf,$midf) {
    global $dba,$tdb;
    // obtem turno do funcionario
    $midt = VoltaValor(FUNCIONARIOS,'TURNO','MID',$midf,0);

    // obtem mtime inicial e final
    $ti = VoltaTime('0:0:0',$datai);
    $tf = VoltaTime('23:59:59',$dataf); // 23:59 para incluir dia de trabalho em dataf
    $i = $ti;
    while ($i < $tf) {
        // obtem data e dia da semana do mtime $i
        $diasem = date("w",$i)+1;
        $estadata = date('Y-m-d',$i);

        // verifica se hoje � feriado
        $feriado = VoltaValor(FERIADOS,'MID','DATA',$estadata,$tdb[FERIADOS]['dba']);

        // verifica se hoje est�o afastado
        $tmp=$dba[0] ->Execute("SELECT * FROM ".AFASTAMENTOS." WHERE MID_FUNCIONARIO = '$midf' AND DATA_INICIAL <= '$estadata' AND DATA_FINAL >= '$estadata'");
        $afastamentos = count($tmp -> getrows());

        // verifica se teve ausencia hoje
        $ausencia = 0;
        $tmp=$dba[0] ->Execute("SELECT * FROM ".AUSENCIAS." WHERE MID_FUNCIONARIO = '$midf' AND DATA = '$estadata'");
        while (!$tmp->EOF) {
            $campo = $tmp->fields;
            $ausencia += (VoltaTime($campo['HORA_FINAL'],NossaData($estadata)) - VoltaTime($campo['HORA_INICIAL'],NossaData($estadata)));
            $tmp->MoveNext();
        }
        //echo "estadata[$estadata] feriado[$feriado] afastamento[$afastamentos] ausencia[$ausencia] // ";
        if ((!$feriado) and (!$afastamentos)) {
            // obtem hora_total por dia da semana
            $hturno = VoltaValor(TURNOS_HORARIOS,'HORA_TOTAL','MID_TURNO',"$midt' AND DIA = '$diasem",0);
            //echo "hturno_antes[$hturno]";

            // se teve ausencia, subtrai
            if ($ausencia) {
                $mturno = explode(':',$hturno,3);
                $mturno = (int)($mturno[2] + $mturno[1]*60 + $mturno[0]*3600);
                $hturno = AcertaHora(($mturno-$ausencia)/3600);
            }
            //echo " hturno_depois[$hturno] htotal_antes[$htotal]";
            $htotal = SomaHora(array($htotal,$hturno));
            //echo " htotal_depois[$htotal]<br>";
        }
        // prepara pr�ximo dia pro while: soma 24h de mtime
        $i += (60*60*24);
    }
    $h = split(':',$htotal);
    $htotal = $h[0]+(($h[1]+($h[2]/60))/60);
    return $htotal;
}

function TempoServcoOS($os) {
    global $dba, $tdb, $ling, $manusis;
    $resultado=$dba[$tdb[ORDEM_MADODEOBRA]['dba']] -> Execute("SELECT DATA_INICIO,HORA_INICIO,DATA_FINAL,HORA_FINAL,MID FROM ".ORDEM_MADODEOBRA." WHERE MID_ORDEM = '$os' ORDER BY DATA_INICIO,HORA_INICIO ASC");
    $i=0;
    if (!$resultado) erromsg($dba[0] -> ErrorMsg());

    if ($resultado->EOF) {
        $sql="UPDATE ".ORDEM." SET DATA_INICIO = NULL,DATA_FINAL = NULL,HORA_INICIO = NULL, HORA_FINAL = NULL, TEMPO_TOTAL = '0.00' WHERE MID = '$os'";
    }
    else {
        while (!$resultado->EOF) {
            $campo=$resultado->fields;
            if ($i == 0) {
                $data_inicio=$campo['DATA_INICIO'];
                $hora_inicio=$campo['HORA_INICIO'];
                $data_final=$campo['DATA_INICIO'];
                $hora_final=$campo['HORA_INICIO'];
                $mktmpinicio=VoltaTime($campo['HORA_INICIO'],$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y'));
                $mktmpfinal=VoltaTime($campo['HORA_FINAL'],$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y'));
            }
            else {
                $valorinicio=VoltaTime($campo['HORA_INICIO'],$resultado -> UserDate($campo['DATA_INICIO'],'d/m/Y'));
                $valorfinal=VoltaTime($campo['HORA_FINAL'],$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y'));
                if ($valorinicio > $mktmpfinal) {
                    $tmp=$valorinicio - $mktmpfinal;
                    $acumula=$tmp + $acumula;
                    $mktmpfinal=VoltaTime($campo['HORA_FINAL'],$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y'));
                }
            }
            $i++;
            $iii++;
            $resultado->MoveNext();
        }
        $data_final=$campo['DATA_FINAL'];
        $hora_final=$campo['HORA_FINAL'];
        $mktmpfinal=VoltaTime($campo['HORA_FINAL'],$resultado -> UserDate($campo['DATA_FINAL'],'d/m/Y'));
        $valor =($mktmpfinal - $mktmpinicio) - $acumula;
        $valor = round(($valor/60)/60,4);
        $valor=str_replace(",",".",$valor);
        $sql="UPDATE ".ORDEM." SET DATA_INICIO = '$data_inicio',DATA_FINAL = '$data_final',HORA_INICIO = '$hora_inicio', HORA_FINAL = '$hora_final', TEMPO_TOTAL = '$valor' WHERE MID = '$os'";
    }
    //$valor=(float)$valor;
//    if ($data_inicio != "") {
        $r=$dba[0] -> Execute($sql);
        if (!$r) erromsg($dba[0] -> ErrorMsg());
        else logar(4, "", ORDEM, "MID", $os);
//    }
    if ($iii == 0) return 1;
    //echo "<hr>DATA INICIO: $data_inicio $hora_inicio<BR> DATA FINAL $data_final $hora_final<BR> VALOR: $valor<hr>";

}

// -------------------------------------------------------------
// FUN�OES QUE TRABALHAM OS DADOS DO MANUSIS
// -------------------------------------------------------------
/**
 * Transforma uma solicitacao em ordem de servi�o, e retorna TRUE em caso de sucesso,
 * do contr�rio retorna FALSE
 *
 * @param integer $mid_sol MID da solicita��o
 * @return bool
 */
function gera_os_solicitacao($mid_sol) {
    global $dba, $tdb, $ling, $manusis;
    $result = false;

    $obj = VoltaValor(SOLICITACOES,'MID_MAQUINA','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $obj_conj = VoltaValor(SOLICITACOES,'MID_CONJUNTO','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $obj_emp = VoltaValor(MAQUINAS,'MID_EMPRESA','MID',$obj,$tdb[MAQUINAS]['dba']);
    $obj_setor = VoltaValor(MAQUINAS,'MID_SETOR','MID',$obj,$tdb[MAQUINAS]['dba']);
    $obj_area = VoltaValor(SETORES,'MID_AREA','MID',$obj_setor,$tdb[SETORES]['dba']);

    $obj_fam = VoltaValor(SOLICITACOES,'MID_MAQUINA_FAMILIA','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $obj_cc = VoltaValor(SOLICITACOES,'MID_CENTRO_DE_CUSTO','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $obj_classe = VoltaValor(SOLICITACOES,'MID_CLASSE','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $obj_fabr = VoltaValor(SOLICITACOES,'MID_FABRICANTE','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $obj_forn = VoltaValor(SOLICITACOES,'MID_FORNECEDOR','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $usuario_solicitante = VoltaValor(SOLICITACOES,'USUARIO','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);
    $prazo = VoltaValor(SOLICITACOES,'PRAZO','MID',$mid_sol,$tdb[SOLICITACOES]['dba']);

    $data_abre = date('Y-m-d');
    $hora_abre = date('H:i:s');

    $mid_ord=GeraMid(ORDEM,"MID",$tdb[ORDEM]['dba']);

    $sql = "INSERT INTO ".ORDEM." SET
            `MID_EMPRESA` = '$obj_emp',
            `MID_AREA` = '$obj_area',
            `MID_SETOR` = '$obj_setor',
            `MID_MAQUINA` = '$obj',
            `MID_CONJUNTO` = '$obj_conj',
            `CENTRO_DE_CUSTO` = '$obj_cc',
            `MID_MAQUINA_FAMILIA` = '$obj_fam',
            `MID_CLASSE` = '$obj_classe',
            `MID_FABRICANTE` = '$obj_fabr',
            `MID_FORNECEDOR` = '$obj_forn',
            `TIPO` = '3',
            `MID_PROGRAMACAO` = '0',
            `MID_SOLICITACAO` = '$mid_sol',
            `MID_SOLICITANTE` = '$usuario_solicitante',
            `RESPONSAVEL` = '0',
            `TIPO_SERVICO` = '0',
            `NATUREZA` = '0',
            `STATUS` = '1',
            `DATA_PROG` = '$prazo',
            `DATA_PROG_ORIGINAL` = '$prazo',
            `HORA_ABRE` = '$hora_abre',
            `DATA_ABRE` = '$data_abre',
            `MID` = '$mid_ord'";

    if(! $dba[$tdb[ORDEM]['dba']] -> Execute($sql)){
        erromsg("Arquivo: mfuncoes.php Fun&ccedil;&atilde;o: gera_os_solicitacao<br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql);
    }
    else {
        logar(3, "", ORDEM, "MID", $mid_ord);
        $result = true;
    }

    // se n�o deu pai ainda, define status 1 para solicita��o
    if ($result) {
        $sql = "UPDATE ".SOLICITACOES." SET `STATUS` = '1' WHERE `MID` = '$mid_sol'";
        if(! $dba[$tdb[SOLICITACOES]['dba']] -> Execute($sql)){
            erromsg("Arquivo: mfuncoes.php Fun&ccedil;&atilde;o: gera_os_solicitacao<br />" . $dba[$tdb[SOLICITACOES]['dba']] -> ErrorMsg() . "<br />" . $sql);
            $result = false;
        }
        else {
            logar(4, "", SOLICITACOES, "MID", $mid_sol);
        }
    }

    return $result;
}
function exportar_word($titulo,$filtro,$registros,$texto,$pagina) {
    global $manusis, $ling;
    $novo=fopen("../".$manusis['dir']['temporarios']."/relatorio.doc","w");
    $doc= "
<html xmlns:o=\"urn:schemas-microsoft-com:office:office\"
xmlns:w=\"urn:schemas-microsoft-com:office:word\"
xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
<meta name=ProgId content=Word.Document>
<meta name=Generator content=\"Microsoft Word 10\">
<meta name=Originator content=\"Microsoft Word 10\">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>MANUSIS</o:Author>
  <o:LastAuthor>MANUSIS</o:LastAutshor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>3</o:TotalTime>
  <o:Created>2005-10-18T14:59:00Z</o:Created>
  <o:LastSaved>2005-10-18T14:59:00Z</o:LastSaved>
  <o:Pages>1</o:Pages>
  <o:Company>MANUSERVICE</o:Company>
  <o:Lines>1</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:GrammarState>Clean</w:GrammarState>
  <w:HyphenationZone>21</w:HyphenationZone>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]-->";

    $doc.="<style>
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-parent:\"\";
    margin:0cm;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:12.0pt;
    font-family:\"Arial\";
    mso-fareast-font-family:\"Arial\";}";
    if ($pagina == 1) $doc.="@page Section1
    {size:21.0cm 842.0pt;
    margin:1.0cm 1.0cm 45.35pt 1.0cm;
    mso-header-margin:35.45pt;
    mso-footer-margin:35.45pt;
    mso-paper-source:0;}";
    else $doc.="@page Section1
    {size:842.0pt 21.0cm;
    mso-page-orientation:landscape;
    margin:1.0cm 1.0cm 1.0cm 1.0cm;
    mso-header-margin:35.45pt;
    mso-footer-margin:35.45pt;
    mso-paper-source:0;}";
    $doc.= "
div.Section1
    {page:Section1;}

#relatorio {
    align: center;
    border: 1px solid #000000;
    font-size:11px;
    padding:5px;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: center center;
    background-color: #FFFFFF;
    background-image: url(".$manusis['url']."temas/padrao/imagens/logopb.png);
}
#cab_processados {
    width: 100%;
    border: 1px solid #000000;
    font-size:12px;
}
#cab_processados h3 {
    font-size:12px;
}
#dados_processados {
    width: 100%;
    font-size:11px;
    margin-bottom:5px;
}
#dados_processados p {
    font-size:11px;
    text-align:center;
}
#dados_processados th {
    background-color:#666666;
    font-size:11px;
    text-align:center;
    color: #FFFFFF;
    padding:3px;
}

#dados_processados td {
    text-align: left;
    padding:3px;
}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
    {mso-style-name:\"Tabela normal\";
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    mso-style-noshow:yes;
    mso-style-parent:\"\";
    mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
    mso-para-margin:0cm;
    mso-para-margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:10.0pt;
    font-family:\"Arial\";}
</style>
<![endif]-->
</head>

<body lang=PT-BR style='tab-interval:35.4pt'>

<div class=Section1 id=relatorio>";



    $doc.= "
<table border=0 cellspacing=0 cellpadding=0 style=\"font-size:9px\" width=\"100%\">
<thead><tr><td>
<table id=\"cab_processados\">
<tr>
<td width=\"100\" align=\"right\">
<img src=\"".$manusis['url']."temas/padrao/imagens/logoempresa.png\" border=0 vspace=2 hspace=2 align=center>
</td>
<td width=\"90%\" align=\"right\">
<h3>$titulo</h3>
".$ling['filtro'].": $filtro<br />
".date("d/m/Y H:i")."<br />
".$_SESSION[ManuSess]['user']['NOME']."
</td>
</tr>
</table></td></tr>
</thead><tr><td>
$texto
<p>".$ling['registros_encontrados'].": $registros </p></td></tr></table>";
fwrite($novo,"$doc");
fclose($novo);
echo "<html><body><script>this.location = '../".$manusis['dir']['temporarios']."/relatorio.doc'</script></body></html>";
}
function relatorio_padrao($titulo,$filtro,$registros,$texto,$caso,$tempoexec=0) {
    global $manusis, $ling;
    echo "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<title>{$ling['relatorio']}: $titulo</title>
<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n

<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
<style type=\"text/css\">";
    readfile("../temas/".$manusis['tema']."/estilo.css");
    echo "</style>
</head>
<body>
<table border=0 cellspacing=0 cellpadding=0 style=\"font-size:9px\" width=\"100%\">
<tr><td>
<table id=\"cab_processados\">
<tr>
<td width=\"100\" align=\"right\">
<img src=\"".$manusis['url']."temas/padrao/imagens/logoempresa.png\" border=0 vspace=2 hspace=2 align=center>
</td>
<td width=\"90%\" align=\"right\">
<h3>$titulo</h3>
".$ling['filtro'].": $filtro<br />
".date("d/m/Y H:i")."<br />
".$_SESSION[ManuSess]['user']['NOME']."
</td>
</tr>
</table>

</td></tr>
<tr><td>
$texto

<table id=\"cab_processados\">
<tr>
<td width=\"100%\" align=\"left\">
".$ling['filtro'].": $filtro<br />
{$ling['registros_encontrados']}: $registros &nbsp; &nbsp; &nbsp;
{$ling['tempoexec']}: $tempoexec
</td>
</tr>
</table>

</td></tr></table>";

}

function exportar_word_os($txt) {
    global $manusis,$form;
    $novo=fopen($manusis['dir']['temporarios']."/relatorio.doc","w");
    fwrite($novo,"$txt");
    fclose($novo);
    echo "<html><body><script>this.location = '".$manusis['dir']['temporarios']."/relatorio.doc'</script></body></html>";
}

function grava_previsao_os($osmid) {
     global $dba,$tdb;
     // TIPO DA OS
     $tipo = VoltaValor(ORDEM, 'TIPO', 'MID', $osmid);

     // STATUS DA OS
     $status = VoltaValor(ORDEM, 'STATUS', 'MID', $osmid);

     // Validando OS (PREVENTIVA OU ROTA)
     if (($status == 1) and (($tipo == 1) or ($tipo == 2))) {
         // Removendo cadastro antigos
         $sql = "DELETE FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = $osmid";
         if (! $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']] ->ErrorMsg() . "<br />" . $sql);
         }

         $sql = "DELETE FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid";
         if (! $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $sql);
         }

         $sql = "DELETE FROM " . ORDEM_MAT_PREVISTO . " WHERE MID_ORDEM = $osmid";
         if (! $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $sql);
         }

         // Buscando os novos
         if ($tipo == 1) {
             $sql = "SELECT ESPECIALIDADE, TEMPO_PREVISTO, QUANTIDADE_MO, MID_MATERIAL, QUANTIDADE FROM " . ORDEM_PREV . " WHERE MID_ORDEM = $osmid";
             $dba_atv = $tdb[ORDEM_PREV]['dba'];
         }
         elseif ($tipo == 2) {
             $sql = "SELECT ESPECIALIDADE, TEMPO_PREVISTO, QUANTIDADE_MO, MID_MATERIAL, QUANTIDADE FROM " . ORDEM_LUB . " WHERE MID_ORDEM = $osmid";
             $dba_atv = $tdb[ORDEM_LUB]['dba'];
         }
         if (! $rs = $dba[$dba_atv] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$dba_atv] ->ErrorMsg() . "<br />" . $sql);
         }

         // SALVANDO AS QUANTIDADES
         $mo_prev = array();
         $mt_prev = array();
         while (! $rs -> EOF) {
             if (! $mo_prev[$rs -> fields['ESPECIALIDADE']]) {
                 $mo_prev[$rs -> fields['ESPECIALIDADE']] = 0;
             }

             $mo_prev[$rs -> fields['ESPECIALIDADE']] += $rs -> fields['TEMPO_PREVISTO'] * $rs -> fields['QUANTIDADE_MO'];

             // Tem Material na Atividade
             if ($rs -> fields['MID_MATERIAL'] != 0) {
                 if (! $mt_prev[$rs -> fields['MID_MATERIAL']]) {
                     $mt_prev[$rs -> fields['MID_MATERIAL']] = 0;
                 }

                 $mt_prev[$rs -> fields['MID_MATERIAL']]  += $rs -> fields['QUANTIDADE'];
             }
             $rs -> MoveNext();
         }

         // M�O DE OBRA
         foreach ($mo_prev as $esp => $tempo) {
             $ins_mo = "INSERT INTO " . ORDEM_MO_PREVISTO . " (MID_ORDEM, MID_ESPECIALIDADE, TEMPO, QUANTIDADE) VALUES ($osmid, $esp, $tempo, 0)";

             if (! $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute($ins_mo)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $ins_mo);
             }
         }

         // MATERIAIS
         foreach ($mt_prev as $mat => $quant) {
             $ins_mt = "INSERT INTO " . ORDEM_MAT_PREVISTO . " (MID_ORDEM, MID_MATERIAL, QUANTIDADE) VALUES ($osmid, $mat, $quant)";

             if (! $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute($ins_mt)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $ins_mt);
             }
         }
     }
}

function grava_programacao_data($data_inicial, $data_final, $maq_list, $tipo, $plano, $prog_list, $mtipo, $pag_tam = 0, $pag_ini = 0, $feedback = false, $muda_final = false, $atualiza_prog = false) {
    global $dba,$tdb;

    // Salvando hist�rico de Ordens para exibir e logar depois
    if (! $_SESSION[ManuSess]['OS_DEL']) {
        $_SESSION[ManuSess]['OS_DEL'] = array();
    }
    if (! $_SESSION[ManuSess]['OS_ALT']) {
        $_SESSION[ManuSess]['OS_ALT'] = array();
    }
    if (! $_SESSION[ManuSess]['OS_NEW']) {
        $_SESSION[ManuSess]['OS_NEW'] = array();
    }

    // Necess�rio para trabalhar com programa��o
    if($tipo == 1) {
        $_SESSION[ManuSess]['VoltaValor']=1;
    }
    elseif($tipo == 2) {
        $_SESSION[ManuSess]['VoltaValor']=2;
    }

    // Caso esteja atualizando a programa��o limpa as OS na primeira pagina
    if(($atualiza_prog) and ($pag_tam == 0 or ($pag_tam != 0 and $pag_ini == 0))) {
        deleta_atv_programacao2($plano, $tipo);
    }

    // Registros executados
    $cont_pag = 0;

    // Modificado para que a pagina��o funcionase corretamente
    if (! is_array($prog_list)) {
        $prog_list = array($prog_list);
    }

    if (! is_array($data_inicial)) {
        $dt_ini = $data_inicial;
        $data_inicial = array();
        $dt_fim = $data_final;
        $data_final = array();

        foreach ($prog_list as $i => $prog) {
            $data_inicial[] = $dt_ini;
            $data_final[]   = $dt_fim;
        }
    }

    // Passando por todas as programa��es
    foreach ($prog_list as $k => $prog_mid) {
        // Mostrar qual programa��o esta sendo processada
        if($feedback) {
            $prog_atual = $k + 1;
            echo "Gerando programa&ccedil;&atilde;o $prog_atual de " . count($prog_list) . ".<br />";
        }


        // Data de inicio
        $di=explode("-",$data_inicial[$k]);
        $df=explode("-",$data_final[$k]);
        $idia=$di[2];
        $imes=$di[1];
        $iano=$di[0];
        $fdia=$df[2];
        $fmes=$df[1];
        $fano=$df[0];
        $idata=mktime (0, 0, 0, $imes, $idia, $iano);
        $fdata=mktime (0, 0, 0, $fmes, $fdia, $fano);
        $data=$idata;

        $i=0;

        if ($tipo == 1) $sql="SELECT * FROM ".ATIVIDADES." WHERE MID_PLANO_PADRAO = '$plano' AND PERIODICIDADE != 4 ORDER BY PERIODICIDADE, FREQUENCIA";
        if ($tipo == 2) $sql="SELECT * FROM ".LINK_ROTAS." WHERE MID_PLANO = '$plano' AND PERIODICIDADE != 4 ORDER BY PERIODICIDADE, FREQUENCIA";

        $tmp=$dba[0]->Execute($sql);
        while (!$tmp->EOF) {
            $campo=$tmp->fields;
            $mid_atv = $campo['MID'];

            $peri=$campo['PERIODICIDADE'];
            $freq=$campo['FREQUENCIA'];

            if (($peri == 1) or ($peri == 5))$freq=$freq;
            elseif (($peri == 2) or ($peri == 6)) $freq=$freq*7;

            if ($tipo == 2) {
                $campo['MID_ATV'] = $mid_atv;
                $campo['MID_EMPRESA'] = (int)VoltaValor(MAQUINAS,"MID_EMPRESA","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                $campo['MID_SETOR'] = (int)VoltaValor(MAQUINAS,"MID_SETOR","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                $campo['CENTRO_DE_CUSTO'] = (int)VoltaValor(MAQUINAS,"CENTRO_DE_CUSTO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                $campo['MID_AREA'] = (int)VoltaValor(SETORES,"MID_AREA","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba']);
                $nponto=VoltaValor(PONTOS_LUBRIFICACAO,"NPONTO","MID",$campo['MID_PONTO'],$tdb[PONTOS_LUBRIFICACAO]['dba']);
                $campo['QUANTIDADE'] = $campo['QUANTIDADE'] * $nponto;
            }

            if ((int)$freq != 0) {
                $idia=$di[2];
                $imes=$di[1];
                $iano=$di[0];
                $idata=mktime (0, 0, 0, $imes, $idia, $iano);
                $data=$idata;
                while ($data < $fdata) {
//                    echo "PT: $pag_tam PI: $pag_ini PC: $cont_pag<br />";
                    // Entra aqui se:
                    // * n�o ta usando pagina��o (seu mala!!!)
                    // * ta usando pagina��o e a pagina atual ($cont_pag) � maior que a pagina de inicio e esta entre o inicio e a quantidade por p�gina.
                    if(($pag_tam == 0) or (($cont_pag >= $pag_ini) and ($cont_pag <= ($pag_ini + $pag_tam)))) {
                        // Buscando OS nessa data para acrecentar as atividades
                        $tmp2 = $dba[0] -> SelectLimit("SELECT MID FROM ".ORDEM." WHERE DATA_PROG = '$iano-$imes-$idia' AND MID_PROGRAMACAO = '$prog_mid'", 1, 0);
                        $ca = $tmp2->fields;

                        if ($ca['MID'] == "") {
                            $os_mid = gera_os($prog_mid,"$iano-$imes-$idia" ,0);
                            $num = VoltaValor(ORDEM,"NUMERO","MID",$os_mid,0);
                            $_SESSION[ManuSess]['OS_NEW'][$prog_mid][] = $num;
                        }
                        else {
                            $num=VoltaValor(ORDEM,"NUMERO","MID",$ca['MID'],0);
                            $os_mid=$ca['MID'];
                            $_SESSION[ManuSess]['OS_ALT'][$prog_mid][] = $num;
                        }

//                        echo "OS: $os_mid ATV: $mid_atv <br/>";

                        if ($tipo == 2) {
                            $campo['MID'] = VoltaValor(ORDEM_LUB, 'MID', array('MID_ORDEM', 'MID_ATV'), array($os_mid, $mid_atv));
                            $campo['MID_ORDEM'] = $os_mid;
                            if($campo['MID'] == 0) {
                                $campo['MID'] = GeraMid(ORDEM_LUB,"MID",$tdb[ORDEM_LUB]['dba']);
                                $log_tipo = 3;
                                // INSERT
                                $sql = "INSERT INTO ".ORDEM_LUB." (" . implode(', ', array_keys($campo)) . ") VALUES ('" . implode("', '", array_values($campo)) . "')";
                            }
                            else {
                                $log_tipo = 4;
                                // Montar Update
                                $upd = "";
                                foreach($campo as $nome => $valor) {
                                    if ($nome == "MID") continue;
                                    $upd .= ($upd != "")? ", " : "";
                                    $upd .= "$nome = '$valor'";
                                }

                                // UPDATE
                                $sql = "UPDATE " . ORDEM_LUB . " SET $upd WHERE MID = {$campo['MID']}";
                            }

                            if (! $dba[$tdb[ORDEM_LUB]['dba']] -> Execute($sql)) {
                                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_LUB]['dba']] ->ErrorMsg() . "<br />" . $sql);
                            }
                        }


                        if ($tipo == 1) {
                            $campo['MID'] = VoltaValor(ORDEM_PREV, 'MID', array('MID_ORDEM', 'MID_ATV'), array($os_mid, $mid_atv));
                            $campo['MID_ORDEM'] = $os_mid;
                            $campo['MID_ATV'] = $mid_atv;

                            if($campo['MID'] == 0) {
                                $campo['MID'] = GeraMid(ORDEM_PREV,"MID",$tdb[ORDEM_PREV]['dba']);
                                $log_tipo = 3;

                                // INSERT
                                $sql = "INSERT INTO ".ORDEM_PREV." (" . implode(', ', array_keys($campo)) . ") VALUES ('" . implode("', '", array_values($campo)) . "')";
                            }
                            else {
                                $log_tipo = 4;
                                // Montar Update
                                $upd = "";
                                foreach($campo as $nome => $valor) {
                                    if ($nome == "MID") continue;
                                    $upd .= ($upd != "")? ", " : "";
                                    $upd .= "$nome = '$valor'";
                                }

                                // UPDATE
                                $sql = "UPDATE " . ORDEM_PREV . " SET $upd WHERE MID = {$campo['MID']}";
                            }

                            if (! $dba[$tdb[ORDEM_PREV]['dba']] -> Execute($sql)) {
                                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_PREV]['dba']] ->ErrorMsg() . "<br />" . $sql);
                            }
                        }

                        // ADICIONANDO AS PREVIS�ES
                        grava_previsao_os($os_mid);
                    }
                    // N�o entrou e ta usando pagina��o devolve a pagina atual
                    elseif(($pag_tam != 0) and ($cont_pag > ($pag_ini + $pag_tam))) {
                        return $cont_pag;
                    }


                    if (($peri == 3) or ($peri == 7)) {
                        $data=mktime (0, 0, 0, $imes + $freq , $idia , $iano);
                    }
                    else {
                        $data=mktime (0, 0, 0, $imes , $idia  + $freq, $iano);
                    }

                    $idia=date(d,$data);
                    $imes=date(m,$data);
                    $iano=date(Y,$data);

                    $cont_pag ++;
                }
            }
            $tmp -> MoveNext();
        }


        // Mudanddo a data final da programa��o
        if ($muda_final) {
            $sql="UPDATE ".PROGRAMACAO." SET DATA_FINAL = '$data_final' WHERE MID = '$prog_mid' ";
            $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql);

            // Montando a mensagem de Log
            $log  = "Extendendo programa��o.\r\n";

            if (count($_SESSION[ManuSess]['OS_NEW'][$prog_mid]) > 0) {
                $log .= "Ordens geradas: " . implode(", ", $_SESSION[ManuSess]['OS_NEW'][$prog_mid]) . "\r\n";
            }
            if (count($_SESSION[ManuSess]['OS_ALT'][$prog_mid]) > 0) {
                $log .= "Ordens alteradas: " . implode(", ", $_SESSION[ManuSess]['OS_ALT'][$prog_mid]) . "\r\n";
            }
            if (count($_SESSION[ManuSess]['OS_DEL'][$prog_mid]) > 0) {
                $log .= "Ordens deletadas: " . implode(", ", $_SESSION[ManuSess]['OS_DEL'][$prog_mid]) . "\r\n";
            }

            logar(4, $log, PROGRAMACAO, 'MID', $prog_mid);
        }

        // Atualizando a programa��o
        if($atualiza_prog) {
            // Limpa as OS na primeira pagina
            deleta_ordem_vazia($prog_mid, $tipo, false);

            // Montando a mensagem de Log
            $log  = "ATUALIZANDO PROGRAMA��O.";

            if (count($_SESSION[ManuSess]['OS_NEW'][$prog_mid]) > 0) {
                $log .= "\r\nORDENS GERADAS: " . implode(", ", $_SESSION[ManuSess]['OS_NEW'][$prog_mid]) . "";
            }
            if (count($_SESSION[ManuSess]['OS_ALT'][$prog_mid]) > 0) {
                $log .= "\r\nORDENS ALTERADAS: " . implode(", ", $_SESSION[ManuSess]['OS_ALT'][$prog_mid]) . "";
            }
            if (count($_SESSION[ManuSess]['OS_DEL'][$prog_mid]) > 0) {
                $log .= "\r\nORDENS DELETADAS: " . implode(", ", $_SESSION[ManuSess]['OS_DEL'][$prog_mid]) . "";
            }

            logar(4, $log, PROGRAMACAO, 'MID', $prog_mid);
        }
    }



    // Apagando hist�rico de Ordens para exibir depois
    if($feedback) {
        $_SESSION[ManuSess]['OS_DEL'] = array();
        $_SESSION[ManuSess]['OS_ALT'] = array();
        $_SESSION[ManuSess]['OS_NEW'] = array();
    }

    return true;
}

function deleta_atv_programacao($atv,$plano,$tipo) {
    global $dba,$tdb,$print;
    if ($tipo == 1) {
        $sql="SELECT * FROM ".PROGRAMACAO." WHERE STATUS != 2 AND TIPO = 1 AND MID_PLANO = '$plano'";
    }
    if ($tipo == 2) {
        $sql="SELECT * FROM ".PROGRAMACAO." WHERE STATUS != 2 AND TIPO = 2 AND MID_PLANO = '$plano'";
    }

    $tmp=$dba[0] ->Execute($sql);
    while (!$tmp->EOF) {
        $campo_prog=$tmp->fields;
        $data_final=$campo_prog['DATA_FINAL'];
        $prog_mid=(int)$campo_prog['MID'];
        if ($prog_mid != 0) {
            $tmp6=$dba[0] -> Execute("SELECT MID,DATA_PROG,NUMERO FROM ".ORDEM." WHERE MID_PROGRAMACAO = '$prog_mid' AND STATUS = '1' ORDER BY DATA_PROG ASC");
            while (!$tmp6->EOF) {
                $cao=$tmp6->fields;
                $data_inicial=$cao['DATA_PROG'];
                if ($tipo == 1) {
                    DeletaItem(ORDEM_PREV, "MID_ATV", $atv);
                }
                if ($tipo == 2) {
                    DeletaItem(ORDEM_LUB, "MID_ATV", $atv);
                }

                if ($cao['MID'] != "") {
                    if ($tipo == 1) $prev=(int)VoltaValor(ORDEM_PREV,"MID_ATV","MID_ORDEM",$cao['MID'],0);
                    if ($tipo == 2) $prev=(int)VoltaValor(ORDEM_LUB,"MID_ATV","MID_ORDEM",$cao['MID'],0);

                    if ($prev == 0) {
                        DeletaItem(ORDEM_MADODEOBRA,"MID_ORDEM",$cao['MID']);
                        DeletaItem(ORDEM_MATERIAL,"MID_ORDEM",$cao['MID']);
                        DeletaItem(ORDEM_MAQ_PARADA,"MID_ORDEM",$cao['MID']);
                        DeletaItem(ORDEM_REPROGRAMA,"MID_ORDEM",$cao['MID']);
                        DeletaItem(ORDEM_CUSTOS,"MID_ORDEM",$cao['MID']);
                        DeletaItem(ORDEM,"MID",$cao['MID']);
                        if ($print == 1) {
                            echo "DELETADA ORDEM {$ling['N']} ".$cao['NUMERO']." <br />";
                        }
                    }
                }
                $tmp6->MoveNext();
            }
        }
        $tmp->MoveNext();
    }

}
/**
 * Muda os Status de uma atividade que contenha programa��o.
 *
 * @param int $mid_atv
 * @param string $tabela
 */
function MudaStatusAtv($mid_atv, $tabela) {
    global $dba,$tdb;
    if ($tabela == ATIVIDADES) {
        $campo_plano = 'MID_PLANO_PADRAO';
        $tipo = 1;
    }
    if ($tabela == LINK_ROTAS) {
        $campo_plano = 'MID_PLANO';
        $tipo = 2;
    }
    $mid_plano = VoltaValor($tabela, $campo_plano, 'MID', $mid_atv, $tdb[$tabela]['dba']);

    if (VoltaValor(PROGRAMACAO, 'MID', "STATUS = 1 AND TIPO = $tipo AND MID_PLANO", $mid_plano) != 0) {
        $sql="UPDATE $tabela SET STATUS = '2' WHERE MID = '$mid_atv'";
        if(! $dba[$tdb[$tabela]['dba']] ->Execute($sql)){
            erromsg("Arquivo: mfuncoes.php Fun&ccedil;&atilde;o: MudaStatusAtv <br />" . $dba[$tdb[$tabela]['dba']] ->ErrorMsg() . "<br />" . $sql);
        }
        else {
            logar(4, "", $tabela, "MID", $mid_atv);
        }
    }
}


/**
 * Deleta as Atividades das Ordens
 *
 * @param int $plano
 * @param plano $tipo
 */
function deleta_atv_programacao2($plano,$tipo) {
    global $dba,$tdb,$print;
    // Atividades
    $mid_atv = array();

    if ($tipo == 1) $sql="SELECT MID FROM ".ATIVIDADES." WHERE STATUS = 2 AND MID_PLANO_PADRAO = '$plano'";
    if ($tipo == 2) $sql="SELECT MID FROM ".LINK_ROTAS." WHERE STATUS = 2 AND MID_PLANO = '$plano'";

    if(!$tmp = $dba[0]->Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
    }
    while (!$tmp->EOF) {
        $mid_atv[] = (int)$tmp->fields['MID'];
        $tmp->MoveNext();
    }

    if(count($mid_atv) > 0) {
        if ($tipo == 1) {
            // BUSCANDO ORDEM_PREV COM AS ATIVIDADES E EM OS ABERTA
            $sql = "SELECT P.MID FROM " . ORDEM_PREV . " P, " . ORDEM . " O WHERE P.MID_ORDEM = O.MID AND O.STATUS = 1 AND P.MID_ATV IN (" . implode(', ', $mid_atv) . ")";
            if(!$tmp = $dba[0]->Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
            }
            while (!$tmp->EOF) {
                DeletaItem(ORDEM_PREV, 'MID', (int)$tmp->fields['MID'], false);
                $tmp->MoveNext();
            }
        }
        if ($tipo == 2) {
            // BUSCANDO ORDEM_PREV COM AS ATIVIDADES E EM OS ABERTA
            $sql = "SELECT P.MID FROM " . ORDEM_LUB . " P, " . ORDEM . " O WHERE P.MID_ORDEM = O.MID AND O.STATUS = 1 AND P.MID_ATV IN (" . implode(', ', $mid_atv) . ")";
            if(!$tmp = $dba[0]->Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
            }
            while (!$tmp->EOF) {
                DeletaItem(ORDEM_LUB, 'MID', (int)$tmp->fields['MID'], false);
                $tmp->MoveNext();
            }
        }
    }
}

/**
 * Deleta Ordens sistemáticas sem Ativdades (vazias)
 *
 * @param int $prog_mid
 * @param int $tipo
 */
function deleta_ordem_vazia($prog_mid,$tipo, $mostra = 1) {
    global $dba,$tdb,$print;
    if ($prog_mid != 0) {
        $thcor = 'cor1';
        $tmp6=$dba[0] -> Execute("SELECT MID,DATA_PROG,NUMERO FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO = '$prog_mid' AND STATUS = '1' ORDER BY DATA_PROG ASC");
        while (!$tmp6->EOF) {
            $cao=$tmp6->fields;
            $data_inicial=$cao['DATA_PROG'];
            if ((int)$cao['MID'] != 0) {
                if ($tipo == 1) {
                    $sql = "SELECT COUNT(MID) AS TOTAL FROM " . ORDEM_PREV . " WHERE MID_ORDEM = '".$cao['MID']."' ";
                    $prev=$dba[0] -> Execute($sql);
                    $prev=$prev -> fields['TOTAL'];

                }
                if ($tipo == 2) {
                    $sql = "SELECT COUNT(MID) AS TOTAL FROM " . ORDEM_LUB . " WHERE MID_ORDEM = '".$cao['MID']."' ";
                    $prev=$dba[0] -> Execute($sql);
                    $prev=$prev -> fields['TOTAL'];
                }
                if ($prev == 0) {
                    // SALVANDO PARA MOSTRAR DEPOIS
                    $_SESSION[ManuSess]['OS_DEL'][$prog_mid][] = VoltaValor(ORDEM, 'NUMERO', 'MID', $cao['MID']);

                    DelCascata(ORDEM, $cao['MID'], 'MID', false);

                    if($mostra) {
                        echo "<tr class=\"$thcor\"><td>{$cao['NUMERO']}</td><td>".NossaData($cao['DATA_PROG'])."</td><td>DELETADA</td></tr>";
                    }
                    if ($thcor == 'cor1') $thcor = 'cor2';
                    else $thcor = 'cor1';
                }
            }
            $tmp6->MoveNext();
        }
    }
}
function gera_os ($prog_mid,$data_prog,$ch=0,$mid_sol=0) {
    global $dba,$tdb,$recomendacoes_texto;

    if ($prog_mid) {
        $resultado=$dba[$tdb[PROGRAMACAO]['dba']] -> Execute("SELECT * FROM ".PROGRAMACAO." WHERE MID = '$prog_mid'");
        $campo=$resultado -> fields;
        $i=count($resultado -> getrows());
        if ($i >= 1) {
            $plano_mid=$campo['MID_PLANO'];
            $prog_mid=$campo['MID'];
            $tipo=$campo['TIPO'];
            if ($campo['TIPO'] == 1) {
                if ($campo['MID_MAQUINA'] != 0) {
                    $maq=(int)$campo['MID_MAQUINA'];
                    $equip=0;
                    $empresa=(int)VoltaValor(MAQUINAS,"MID_EMPRESA","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $setor=(int)VoltaValor(MAQUINAS,"MID_SETOR","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                    $centro=(int)VoltaValor(MAQUINAS,"CENTRO_DE_CUSTO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                    $area=(int)VoltaValor(SETORES,"MID_AREA","MID",$setor,$tdb[SETORES]['dba']);

                    $conj=(int)$campo['MID_CONJUNTO'];
                    $fabr = (int)VoltaValor(MAQUINAS,"FABRICANTE","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $forn = (int)VoltaValor(MAQUINAS,"FORNECEDOR","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $classe = (int)VoltaValor(MAQUINAS,"CLASSE","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $fam = (int)VoltaValor(MAQUINAS,"FAMILIA","MID",$maq,$tdb[MAQUINAS]['dba']);
                }
                elseif ($campo['MID_EQUIPAMENTO'] != 0) {
                    $equip=(int)$campo['MID_EQUIPAMENTO'];
                    $maq=(int)VoltaValor(EQUIPAMENTOS,"MID_MAQUINA","MID",$campo['MID_EQUIPAMENTO'],$tdb[EQUIPAMENTOS]['dba']);
                    $empresa=(int)VoltaValor(MAQUINAS,"MID_EMPRESA","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $setor=(int)VoltaValor(MAQUINAS,"MID_SETOR","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $centro=(int)VoltaValor(MAQUINAS,"CENTRO_DE_CUSTO","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $area=(int)VoltaValor(SETORES,"MID_AREA","MID",$setor,$tdb[SETORES]['dba']);

                    $conj=(int)$campo['MID_CONJUNTO'];
                    $fabr = (int)VoltaValor(EQUIPAMENTOS,"FABRICANTE","MID",$equip,$tdb[EQUIPAMENTOS]['dba']);
                    $forn = (int)VoltaValor(EQUIPAMENTOS,"FORNECEDOR","MID",$equip,$tdb[EQUIPAMENTOS]['dba']);
                    $classe = (int)VoltaValor(MAQUINAS,"CLASSE","MID",$maq,$tdb[MAQUINAS]['dba']);
                    $fam = (int)VoltaValor(MAQUINAS,"FAMILIA","MID",$maq,$tdb[MAQUINAS]['dba']);
                }
            }

            $tmp_mid=GeraMid(ORDEM,"MID",$tdb[ORDEM]['dba']);

            if ($campo['TIPO'] == 1) {
                $osnum = GeraNumOS($empresa);
                $sql = "
                    INSERT INTO ".ORDEM." (
                        NUMERO,
                        MID_EMPRESA,
                        MID_AREA,
                        MID_SETOR,
                        CENTRO_DE_CUSTO,
                        MID_MAQUINA,
                        MID_CONJUNTO,
                        TIPO,
                        MID_PROGRAMACAO,
                        STATUS,
                        DATA_PROG,
                        DATA_PROG_ORIGINAL,
                        DATA_ABRE,
                        HORA_ABRE,
                        TEXTO,
                        MID_EQUIPAMENTO,
                        MID
                    ) VALUES (
                        '$osnum',
                        '$empresa',
                        '$area',
                        '$setor',
                        '$centro',
                        '$maq',
                        '$conj',
                        '$tipo',
                        '$prog_mid',
                        '1',
                        '$data_prog',
                        '$data_prog',
                        '" . date("Y-m-d") . "',
                        '" . date("H:i:s") . "',
                        '$recomendacoes_texto',
                        '$equip',
                        '$tmp_mid')";

                $insere_os=$dba[$tdb[ORDEM]['dba']] -> Execute($sql);
            }
            elseif ($campo['TIPO'] == 2) {
                $emp = (int)VoltaValor(PLANO_ROTAS,'MID_EMPRESA','MID',$plano_mid,0);
                $osnum = GeraNumOS($emp);
                $sql = "
                    INSERT INTO ".ORDEM." (
                        NUMERO,
                        MID_EMPRESA,
                        TIPO,
                        MID_PROGRAMACAO,
                        STATUS,
                        DATA_PROG,
                        DATA_PROG_ORIGINAL,
                        DATA_ABRE,
                        HORA_ABRE,
                        MID,
                        TIPO_SERVICO
                    ) VALUES (
                        '$osnum',
                        '$emp',
                        '$tipo',
                        '$prog_mid',
                        '1',
                        '$data_prog',
                        '$data_prog',
                        '" . date("Y-m-d") . "',
                        '" . date("H:i:s") . "',
                        '$tmp_mid',
                        '2'
                    )";
                $insere_os=$dba[$tdb[ORDEM]['dba']] -> Execute($sql);

            }
            else {
                $sql = "
                    INSERT INTO ".ORDEM." (
                        TIPO,
                        MID_PROGRAMACAO,
                        STATUS,
                        DATA_PROG,
                        DATA_PROG_ORIGINAL,
                        DATA_ABRE,
                        HORA_ABRE,
                        MID,
                        TIPO_SERVICO
                    ) VALUES (
                        '$tipo',
                        '$prog_mid',
                        '1',
                        '$data_prog',
                        '$data_prog',
                        '" . date("Y-m-d") . "',
                        '" . date("H:i:s") . "',
                        '$tmp_mid',
                        '2'
                    )";
                $insere_os=$dba[$tdb[ORDEM]['dba']] -> Execute($sql);
            }
            if (! $insere_os) {
                erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . " <br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql);
            }
            else {
                logar(3, "", ORDEM, "MID", $tmp_mid);
                return $tmp_mid;
            }
        }
    }
    // SOLICITACAO
    elseif ($mid_sol != 0){
        // Recuperando os dados
        $maq=(int)VoltaValor(SOLICITACOES,"MID_MAQUINA","MID",$mid_sol,0);
        $equip=0;
        $empresa=(int)VoltaValor(MAQUINAS,"MID_EMPRESA","MID",$maq,$tdb[MAQUINAS]['dba']);
        $setor=(int)VoltaValor(MAQUINAS,"MID_SETOR","MID",$maq,$tdb[MAQUINAS]['dba']);
        $centro=(int)VoltaValor(MAQUINAS,"CENTRO_DE_CUSTO","MID",$maq,$tdb[MAQUINAS]['dba']);
        $area=(int)VoltaValor(SETORES,"MID_AREA","MID",$setor,$tdb[SETORES]['dba']);
        $conj=(int)VoltaValor(SOLICITACOES,"MID_CONJUNTO","MID",$mid_sol,0);
        $fabr = (int)VoltaValor(MAQUINAS,"FABRICANTE","MID",$maq,$tdb[MAQUINAS]['dba']);
        $forn = (int)VoltaValor(MAQUINAS,"FORNECEDOR","MID",$maq,$tdb[MAQUINAS]['dba']);
        $classe = (int)VoltaValor(MAQUINAS,"CLASSE","MID",$maq,$tdb[MAQUINAS]['dba']);
        $fam = (int)VoltaValor(MAQUINAS,"FAMILIA","MID",$maq,$tdb[MAQUINAS]['dba']);
        $usuario = VoltaValor(SOLICITACOES,"USUARIO","MID",$mid_sol,0);
        $solicitante = ($usuario != 0)? VoltaValor(USUARIOS,"NOME","MID",$usuario,0) : "ADMINISTRADOR";

        $osnum = GeraNumOS($empresa);

        $tmp_mid=GeraMid(ORDEM,"MID",$tdb[ORDEM]['dba']);

        // Montando o insert
        $sql = "INSERT INTO ".ORDEM." (
                NUMERO,
                MID_EMPRESA,
                MID_AREA,
                MID_SETOR,
                CENTRO_DE_CUSTO,
                MID_MAQUINA,
                MID_CONJUNTO,
                TIPO,
                STATUS,
                DATA_PROG,
                DATA_PROG_ORIGINAL,
                DATA_ABRE,
                HORA_ABRE,
                TEXTO,
                MID_EQUIPAMENTO,
                MID,
                SOLICITANTE,
                MID_SOLICITACAO
            )
            VALUES (
                '$osnum',
                '$empresa',
                '$area',
                '$setor',
                '$centro',
                '$maq',
                '$conj',
                '4',
                '1',
                '$data_prog',
                '$data_prog',
                '" . date("Y-m-d") . "',
                '" . date("H:i:s") . "',
                '$recomendacoes_texto',
                '0',
                '$tmp_mid',
                '$solicitante',
                 $mid_sol
            )";

        // Executando
        if(! $dba[$tdb[ORDEM]['dba']] -> Execute($sql)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql);
        }
        else {
            logar(3, "", ORDEM, "MID", $tmp_mid);
            return $tmp_mid;
        }
    }
}
function cab_relatorio ($titulo) {
    global $manusis;
    echo "<div class=\"corpo\">
<table width=\"100%\" border=\"0\" align=\"center\" cellspacing=\"0\" cellpadding=\"0\">
<tr><td>
<table width=\"100%\" border=\"0\" cellspacing=\"5\" cellpadding=\"0\">
<tr><td width=\"150\"><img src=\"".$manusis['url']."imagens/".$manusis['logomarca']."\" border=\"0\" alt=\" \"></td>
<td valign=\"middle\" align=\"center\"><h2>$titulo</h2></td>
</tr></table>";
}
/**
 * Lanca valor de contador, verifica se atingiu o valor de disparo, e se for o caso dispara
 * Caso seja passado um mid ($mid_existente), n�o lan�a o contador, apenas os disparos
 *
 * @param int $contador
 * @param float $valor
 * @param (nossa)data $data
 * @param int $mid_existente
 */
function LancaContador($cont, $valor, $data, $ini=0, $mid_existente=0) {
    global $tdb, $dba;
    $Result=''; // $Result = no fim tem um return $Result;
    $zera=(float)VoltaValor(MAQUINAS_CONTADOR,"ZERA","MID",$cont,0);
    $cont_tipo=(int)VoltaValor(MAQUINAS_CONTADOR,"TIPO","MID",$cont,0);
    list ($dia_c, $mes_c, $ano_c) = split ("/", $data);

    $midLancamento = '0';
    if (!$mid_existente) {
        $tmp=$dba[$tdb[LANCA_CONTADOR]['dba']] -> SelectLimit("SELECT * FROM ".LANCA_CONTADOR." WHERE MID_CONTADOR = '$cont' ORDER BY MID DESC",1,0);
        $cam=$tmp->fields;
        $camvalor=(float)$cam['VALOR'];
        if ($camvalor == $zera) $camvalor=0;
        //echo "$valor - ";
        if ($valor==0) {
            if ($camvalor > 0) {
                $valor=$zera;
            }
        }
        elseif ($cont_tipo == 2) {
            $valor += $camvalor;
        }
        //die($valor);
        $midLancamento = $cam['MID'];
    }
    if ($valor > $zera) $Result .= "Valor superior ao m&aacute;ximo permitido no contador ($zera), para zerar um contador, lance o n&uacute;mero 0 (zero)";
    elseif($valor < 0)  $Result .= "Valor inv&aacute;lido";
    elseif ((!$mid_existente) and ($cont_tipo == 1) and ($valor < $camvalor)) $Result .= "Valor inferior ao ultimo lan&ccedil;amento ($camvalor), para zerar um contador, lance o n&uacute;mero 0 (zero)";
    elseif ((!$mid_existente) and ($cont_tipo == 1) and ($valor == $camvalor)) $Result .= "Valor igual ao &uacute;ltimo lan&ccedil;amento ($camvalor), lance um n&uacute;mero maior";
    elseif (!checkdate($mes_c,$dia_c,$ano_c)) $Result .= "A data informada n&atilde;o &eacute; v&aacute;lida";
    else {
        $datasql=DataSQL($data);
        $user=$_SESSION[ManuSess]['user']['MID'];
        if ($user == 'ROOT') $user = 0;
        $user = (int)$user;
        $midmaq=(int)VoltaValor(MAQUINAS_CONTADOR,"MID_MAQUINA","MID",$cont,0);

        if ((int)$mid_existente) {
            // ainda n�o � usado pra nada, mas fica aqui pra implementa��o futura
            $cont_mid = (int)$mid_existente;
        }
        else {
            $cont_mid=GeraMid(LANCA_CONTADOR,"MID",0);
            $sql="INSERT INTO ".LANCA_CONTADOR." (MID_MAQUINA,MID_CONTADOR,DATA,VALOR,MID_USUARIO,STATUS,MID) VALUES ('$midmaq','$cont','$datasql','".str_replace(',','.',$valor)."','$user','$ini','$cont_mid')";
            $tmp=$dba[0] -> Execute($sql);
            if ($tmp) logar(3, "", LANCA_CONTADOR, "MID", $cont_mid);
            else $Result .= htmlentities($dba[0]->ErrorMsg());
        }


        if ($ini == 0) {
            $sql="SELECT * FROM ".PROGRAMACAO." WHERE STATUS = 1 AND DATA_INICIAL <= '$datasql' AND DATA_FINAL >= '$datasql' AND MID_MAQUINA = '$midmaq'";
            $tmp=$dba[0] -> Execute($sql);
            while (!$tmp->EOF) {
                $campo=$tmp->fields;
                $tipo=$campo['TIPO'];
                $plano_mid=$campo['MID_PLANO'];
                $prog_mid=$campo['MID'];

                if ($tipo == 1) $tmp_atv=$dba[$tdb[ATIVIDADES]['dba']] -> Execute("SELECT * FROM ".ATIVIDADES." WHERE MID_PLANO_PADRAO = '$plano_mid'");
                if ($tipo == 2) $tmp_atv=$dba[$tdb[LINK_ROTAS]['dba']] -> Execute("SELECT * FROM ".LINK_ROTAS." WHERE MID_PLANO = '$plano_mid'");

                while (!$tmp_atv->EOF) {
                    $campo_atv=$tmp_atv->fields;
                    $contador=$campo_atv['CONTADOR'];
                    $disparo=$campo_atv['DISPARO'];
                    $atv_mid=$campo_atv['MID'];
                    if ($contador == $cont) {
                        $total = $valor - $camvalor;
                        //$total = ($cont_tipo == 1) ? $valor - $camvalor : $valor;
                        $tmp2=$dba[$tdb[CONTROLE_CONTADOR]['dba']] -> SelectLimit("SELECT * FROM ".CONTROLE_CONTADOR." WHERE MID_CONTADOR = '$cont' AND MID_PROGRAMACAO ='$prog_mid' AND MID_ATV = '$atv_mid' ORDER BY MID DESC",1,0);
                        $campo2=$tmp2->fields;
                        $total_dis=(int)$campo2['DISPAROS'];
                        $ultimo_valor=(float)$campo2['VALOR'];
                        $ultima_os=(int)$campo2['MID_ORDEM'];
                        $sql_o = "SELECT MID FROM ".ORDEM_PLANEJADO." WHERE MID = '$ultima_os' AND STATUS = '1'";
                        //die($sql_o);
                        $tmp_ordd=$dba[0] -> Execute($sql_o);
                        $tmp_ordd=$tmp_ordd->fields;
                        if ((int)$tmp_ordd['MID'] == 0) {
                            $total_plano=$ultimo_valor+$total;
                            if ($total_plano >= $disparo) { // disparou
                                $total_dis++;
                                /* // depois que dispara, aproveita o valor excedente?
                                 $total=$disparo-$total_plano;
                                 $total=str_replace("-","",$total);
                                 if ($total > $disparo) $total=$disparo;
                                 */
                                // depois que dispara, descarta o excedente?
                                $total = 0;

                                $tmp_mid=GeraMid(CONTROLE_CONTADOR,"MID",0);

                                $tmp5=$dba[0] -> Execute("SELECT MID FROM ".ORDEM_PLANEJADO." WHERE DATA_PROG = '$datasql' AND MID_PROGRAMACAO = '$prog_mid' AND STATUS = '1'");
                                $ca=$tmp5->fields;
                                if ((int)$ca['MID'] == 0) {
                                    $os_mid = gera_os($prog_mid,$datasql ,0);
                                }
                                else {
                                    $os_mid=$ca['MID'];
                                }
                                $ordmid=$os_mid;
                                if ($tipo == 2) {
                                    $campo_atv['MID_SETOR'] = (int)VoltaValor(MAQUINAS,"MID_SETOR","MID",$campo_atv['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                                    $campo_atv['CENTRO_DE_CUSTO'] = (int)VoltaValor(MAQUINAS,"CENTRO_DE_CUSTO","MID",$campo_atv['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
                                    $campo_atv['MID_AREA'] = (int)VoltaValor(SETORES,"MID_AREA","MID",$setor,$tdb[SETORES]['dba']);
                                    $campo_atv['MID_EMPRESA'] = (int)VoltaValor(AREAS,"MID_EMPRESA","MID",$area,$tdb[AREAS]['dba']);

                                    $campo_atv['MID_ATV'] = $campo_atv['MID'];
                                    $campo_atv['MID_ORDEM'] = $os_mid;
                                    $campo_atv['MID'] = GeraMid(ORDEM_LUB,"MID",$tdb[ORDEM_LUB]['dba']);

                                    $nponto=VoltaValor(PONTOS_LUBRIFICACAO,"NPONTO","MID",$campo_atv['MID_PONTO'],$tdb[PONTOS_LUBRIFICACAO]['dba']);
                                    $campo_atv['QUANTIDADE'] = $campo_atv['QUANTIDADE'] * $nponto;

                                    // INSERT
                                    $sql = "INSERT INTO ".ORDEM_LUB." (" . implode(', ', array_keys($campo_atv)) . ") VALUES ('" . implode("', '", array_values($campo_atv)) . "')";

                                    if (! $dba[$tdb[ORDEM_LUB]['dba']] -> Execute($sql)) {
                                        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_LUB]['dba']] ->ErrorMsg() . "<br />" . $sql);
                                    }
                                    else {
                                        logar(3, "", ORDEM_LUB, "MID", $campo_atv['MID']);
                                    }
                                }
                                if ($tipo == 1) {
                                    $campo_atv['MID_ATV'] = $campo_atv['MID'];
                                    $campo_atv['MID_ORDEM'] = $os_mid;
                                    $campo_atv['MID'] = GeraMid(ORDEM_PREV,"MID",$tdb[ORDEM_PREV]['dba']);

                                    // INSERT
                                    $sql = "INSERT INTO ".ORDEM_PREV." (" . implode(', ', array_keys($campo_atv)) . ") VALUES ('" . implode("', '", array_values($campo_atv)) . "')";
                                    if (! $dba[$tdb[ORDEM_PREV]['dba']] -> Execute($sql)) {
                                        erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_PREV]['dba']] ->ErrorMsg() . "<br />" . $sql);
                                    }
                                    else {
                                        logar(3, "", ORDEM_PREV, "MID", $campo_atv['MID']);
                                    }
                                    grava_previsao_os($os_mid);
                                }

                                $tmp=$dba[$tdb[LANCA_CONTADOR]['dba']] -> SelectLimit("SELECT * FROM ".LANCA_CONTADOR." WHERE MID_CONTADOR = '$cont' ORDER BY MID DESC",1,0);
                                $cam=$tmp->fields;


                                $sql="INSERT INTO ".CONTROLE_CONTADOR." (TIPO,MID_PROGRAMACAO,MID_ATV,MID_CONTADOR,MID_LANCAMENTO, DATA,DISPAROS,VALOR,MID_ORDEM,MID) VALUES ('$tipo','$prog_mid','$atv_mid','$cont','{$cam['MID']}', '$datasql','".str_replace(',','.',$total_dis)."','".str_replace(',','.',$total)."','$ordmid','$tmp_mid')";
                                if (!$tmp3=$dba[0] -> Execute($sql)) erromsg($dba[0] -> ErrorMsg());
                                else logar(3, "", CONTROLE_CONTADOR, "MID", $tmp_mid);

                            }
                            else { // n�o disparou ainda
                                $total=$ultimo_valor+$total;
                                $tmp_mid=GeraMid(CONTROLE_CONTADOR,"MID",0);
                                $sql="INSERT INTO ".CONTROLE_CONTADOR." (TIPO,MID_PROGRAMACAO,MID_ATV,MID_CONTADOR,MID_LANCAMENTO,DATA,DISPAROS,VALOR,MID_ORDEM,MID) VALUES ('$tipo','$prog_mid','$atv_mid','$cont','$midLancamento', '$datasql','".str_replace(',','.',$total_dis)."','".str_replace(',','.',$total)."','0','$tmp_mid')";
                                if (!$tmp3=$dba[0] -> Execute($sql)) erromsg($dba[0] -> ErrorMsg());
                                else logar(3, "", CONTROLE_CONTADOR, "MID", $tmp_mid);
                            }
                        }

                    }
                    $tmp_atv->MoveNext();
                }
                $tmp->MoveNext();
            }

        }
        //else $tmp=$dba[0] -> Execute($sql); // setando valor inicial // revisar esta linha!!!
    }
    return $Result;
}



// -------------------------------------------------------------
// FUN��OES DE CONTROLE INTERNO
// -------------------------------------------------------------

function logar($tipo, $texto, $tabela = "", $mid = "", $valMid = 0){
    global $dba, $tdb, $manusis, $defLog;
    // Iniciando campos
    $texto = mb_strtoupper(LimpaTexto($texto));
    $user  = (int) $_SESSION[ManuSess]['user']['MID'];
    $user  = ($user != 0)? VoltaValor(USUARIOS, "USUARIO", "MID", $user, 0) : "ADMINISTRADOR";
    $tipo_desc  = VoltaValor(LOGS_TIPOS, 'DESCRICAO', 'MID', $tipo);
    $logMid = (int) GeraMid(LOGS, "MID", 0);
    $data  = date("d/m/Y");
    $hora  = date("H:i:s");

    // Usando modelo de LOG
    if (($tabela != "") && ($mid != "") && ($valMid != 0)) {
        // Colunas
        if (! is_array($defLog[$tabela])) {
            $modelo = $defLog['DEFAULT'];
        }
        else {
            $modelo = $defLog[$tabela];
        }
        // Montando select
        if ($modelo[0] == "ALL") {
            $campos = "*";
        }
        else {
            $campos = "MID, " . implode(", ", $modelo);
        }

        if (is_array($mid) && is_array($valMid)){
            $cond = "";
            foreach ($mid as $k => $c){
                $valMid[$k] = (is_numeric($valMid[$k]))? $valMid[$k] : "'" . $valMid[$k] . "'";
                $cond .= ($cond != "")? " AND " : "";
                $cond .= "$c  = " . $valMid[$k] . "";
            }
        }
        else {
            $cond = "$mid = '$valMid'";
        }

        $sql = "SELECT $campos FROM $tabela WHERE $cond";

        // Executando
        if(! $rs = $dba[0] -> Execute($sql)){
            erromsg("Arquivo: " . __FILE__ . " Funcao: " . __FUNCTION__ . "<br />" . $dba[0] -> ErrorMsg() . "<br />" . $sql);
        }

        // Montando o texto
        while (! $rs->EOF) {
            // Melhor formatado
            $texto .= ($texto != "")? "\r\n" : "";

            foreach ($rs->fields as $campo => $valor) {
                // Nome do campo
                if ($tdb[$tabela][$campo] != ""){
                    $texto .= mb_strtoupper(unhtmlentities($tdb[$tabela][$campo])) . ": ";
                }
                else {
                    $texto .= mb_strtoupper($campo) . ": ";
                }
                // Valor do campo
                $recb = VoltaRelacao($tabela, $campo);
                if ($recb['tb'] == "") {
                    $texto .= $valor;
                }
                else {
                    if ($recb['cod'] != "") {
                        $texto .= VoltaValor($recb['tb'], $recb['cod'], $recb['mid'], $valor, $recb['dba']) . "-";
                    }

                    $texto .= VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $valor, $recb['dba'], 1, 0);
                }

                $texto .= "\r\n";
            }

            $rs->MoveNext();
        }

    }

    // Respeitando o limite do Oracle
    //$texto = substr($texto, 0, 65000);

    if (($tipo == 5) and (count($_SESSION[ManuSess]['del']) > 0)) {
        foreach ($_SESSION[ManuSess]['del'] as $tab => $def) {
            if ($def['cont'] > 0) {
                $texto .= mb_strtoupper(unhtmlentities($tdb[$tab]['DESC'])) . ": ";

                if ($tdb[$tab]['DESC_CAMPO']) {
                    $texto .= $def['ident'] . "\r\n";
                }
                else {
                    $texto .= $def['cont'] . "\r\n";
                }
            }
        }
    }

    $tabela = ($tabela == "")? "SEM TABELA DEFINIDA" : strtoupper(unhtmlentities($tdb[$tabela]['DESC']));

    $dir = "logs";
    while (! is_dir($dir)) {
        $dir = "../$dir";
    }

    $file_name = $dir . "/log_" . date('o') . "" . date('W') .  ".csv";
    $fp = fopen($file_name, "a");

    $dados = array(
        $user,
        $_SERVER['REMOTE_ADDR'],
        $data,
        $hora,
        $tipo_desc,
        $texto,
        $tabela
    );

    if (! fputcsv($fp, $dados, ';', '"')) {
        erromsg($ling['erro_log']);
        return false;
    }
}
function DeletaDir($dirName) {
    if(empty($dirName)) {
        return true;
    }
    if(file_exists($dirName)) {
        $dir = dir($dirName);
        while($file = $dir->read()) {
            if($file != '.' && $file != '..') {
                if(is_dir($dirName.'/'.$file)) {
                    delDir($dirName.'/'.$file);
                } else {
                    @unlink($dirName.'/'.$file) or die($ling['arquivo'].' '.$dirName.'/'.$file.' '.$ling['nao_deletado']);
                }
            }
        }
        $dir->close();
        @rmdir($dirName) or die($ling['diretorio'].' '.$dirName.' '.$ling['nao_deletado']);
    }
    else {
        return false;
    }
    return true;
}
/**
 * Envia um e-mail texto em formato HTML (para enviar anexos use a fun��o MandaMailAnexo)
 * Retorna TRUE se deu certo, FALSE se ocorreu um erro
 * Autor: Fernando Cosentino
 *
 * @param string $para     : Destinatario
 * @param string $de       : Remetente
 * @param string $assunto  : Assunto da mensagem e ti�tulo do cabecalho
 * @param string $mensagem : Mensagem em formato HTML
 * @return boolean
 */
function MandaMail($para,$de,$assunto,$mensagem) {
    global $manusis, $_SESSION;
    $para2 = '';
    $de2 = $de;
    #### Processo de convers�o: de "Fulano <email>" para "email"
    ## Destinat�rios
    // separa os destinat�rios em uma array
    $paralist = explode(', ',$para);
    // para cada destinat�rio, verifica se existem os caracteres < e > e caso existam,
    // recorta o que estiver entre eles. caso n�o existam, apenas considera tudo
    foreach ($paralist as $epara) {
        $posl = strpos($epara,'<');
        $posr = strpos($epara,'>');
        if (($posl !== false) and ($posr !== false)) {
            $epara_corrigido = substr($epara,$posl+1,$posr-$posl-1);
            //die($epara_corrigido); // debug
            AddStr($para2,', ',$epara_corrigido);
        }
        else AddStr($para2,', ',$epara);
    }
    // Aqui, $para2 contagem a lista sem < >. Caso algo de errado tenha acontecido, restaura:
    if (($para) and (!$para2)) {
        $para2 = $para;
    }
    //die($para2); // debug

    $conteudo = "<html><body>
        <table style=\"border:1px solid gray\" width=\"100%\"><tr><th>
        <table>
        <tr>
        <th width=\"100\" align=\"right\">
        <img src=\"".$manusis['url']."temas/padrao/imagens/".$manusis['logomarca']."\" border=0 vspace=2 hspace=2 align=center>
        </th>
        <th width=\"90%\" align=\"right\">
        <h3>$assunto</h3>
        </th>
        </tr></table></th></tr>
        </table><font size=1>&nbsp;</font>
        <table style=\"border:1px solid gray\" width=\"100%\" cellpadding=5><tr><td>
        $mensagem
        </td></tr>
        </table>
        <div align=\"right\"><font size=1>Usu&aacute;rio: ".$_SESSION[ManuSess]['user']['NOME']."</font>
        </body></html>";

        //die($conteudo);
        // Envia o e-mail propriamente dito
        $tmp = mail($para2, $assunto, $conteudo,
    "To: $para\n" .
    "From: $de\n" .
    "MIME-Version: 1.0\n" .
    "Content-type: text/html; charset=iso-8859-1");
        return $tmp;
}
//Caso a fun��o mime_content_type n�o exista, simula
if (!function_exists('mime_content_type')) {
    function mime_content_type($f) {
        return trim(exec('file -bi '.escapeshellarg($f)));
    }
}
/**
 * Envia um e-mail texto em formato HTML com Anexo (para enviar s�o texto use a fun���o MandaMail)
 * Retorna TRUE se deu certo, FALSE se ocorreu um erro
 * Autor: Fernando Cosentino
 *
 * @param string $para     : Destinatario
 * @param string $de       : Remetente
 * @param string $assunto  : Assunto da mensagem e ti�tulo do cabe�alho
 * @param string $mensagem : Mensagem em formato HTML
 * @param string $arquivos : arquivos anexos separados por virgula sem espa�o (arquivo1,arquivo2)
 * @return boolean
 */
function MandaMailAnexo($para,$de,$assunto,$mensagem, $arquivos) {
    global $manusis, $_SESSION;
    // tipos de arquivos permitidos:
    $allow = array('txt', 'pdf', 'doc', 'jpg', 'png', 'gif', 'xls', 'xml', 'csv');

    $para2 = '';
    $de2 = $de;
    #### Processo de convers�o: de "Fulano <email>" para "email"
    ## Destinat�rios
    // separa os destinat�rios em uma array
    $paralist = explode(', ',$para);
    // para cada destinat�rio, verifica se existem os caracteres < e > e caso existam,
    // recorta o que estiver entre eles. caso n�o existam, apenas considera tudo
    foreach ($paralist as $epara) {
        $posl = strpos($epara,'<');
        $posr = strpos($epara,'>');
        if (($posl !== false) and ($posr !== false)) {
            $epara_corrigido = substr($epara,$posl+1,$posr-$posl-1);
            //die($epara_corrigido); // debug
            AddStr($para2,', ',$epara_corrigido);
        }
        else AddStr($para2,', ',$epara);
    }
    // Aqui, $para2 contagem a lista sem < >. Caso algo de errado tenha acontecido, restaura:
    if (($para) and (!$para2)) {
        $para2 = $para;
    }
    //die($para2); // debug

    #### Prepara��o dos anexos
    $arqlist = explode(',',$arquivos);
    $i = 0;
    foreach ($arqlist as $earq) {
        // in_array
        $fext = substr($earq,-3);
        if (in_array($fext,$allow) and !is_executable($earq)) {
            // L� arquivo em array ('rb' = read binary)
            $file = fopen($earq,'rb');
            $data[$i]['nome'] = $earq;
            $data[$i]['data'] = fread($file,filesize($earq));
            fclose($file);
            $i++;
        } // se o arquivo n�o � permitido, � simplesmente ignorado
    } // $i contem a quantidade de arquivos anexados

    $semi_rand = md5(time());
    $boundary = "==Multipart_Boundary_x{$semi_rand}x"; // um vergonhoso ctrl+c ctrl+v
    $headers = "To: $para\n" .
    "From: $de\n" .
    "MIME-Version: 1.0\n" .
    "Content-Type: multipart/mixed;\n" .
    " boundary=\"{$boundary}\"";


    $conteudo = "This is a multi-part message in MIME format.\n\n" .
    "--{$boundary}\n" .
    "Content-type: text/html; charset=\"iso-8859-1\"\n" .
    "Content-Transfer-Encoding: 7bit\n\n" .
    "<html><body>
<table style=\"border:1px solid gray\" width=\"100%\"><tr><th>
<table>
<tr>
<th width=\"100\" align=\"right\">
<img src=\"".$manusis['url']."temas/padrao/imagens/".$manusis['logomarca']."\" border=0 vspace=2 hspace=2 align=center>
</th>
<th width=\"90%\" align=\"right\">
<h3>$assunto</h3>
</th>
</tr></table></th></tr>
</table><font size=1>&nbsp;</font>
<table style=\"border:1px solid gray\" width=\"100%\" cellpadding=5><tr><th>
$mensagem
</th></tr>
</table>
<div align=\"right\"><font size=1>Usu&aacute;rio: ".$_SESSION[ManuSess]['user']['NOME']."</font>
</body></html>\n\n";

foreach ($data as $edata) {
    // para cada arquivo anexado
    $dataline = chunk_split(base64_encode($edata['data']));
    $fname = basename($edata['nome']); // remove diretorios
    $ftype = mime_content_type($edata['nome']);

    $conteudo .= "--{$boundary}\n" .
        "Content-Type: {$ftype};\n" .
        " name=\"{$fname}\"\n" .
        "Content-Disposition: attachment;\n" .
        " filename=\"{$fname}\"\n" .
        "Content-Transfer-Encoding: base64\n\n" .
    $dataline . "\n\n";
}

// acabou mensagem e anexos, encerra mensagem
$conteudo .= "--{$boundary}--\n";

//die($conteudo);
// Envia o e-mail propriamente dito
$tmp = mail($para2, $assunto, $conteudo,
$headers);
return $tmp;
}

function parse_string($data, $formato) {
    $str = str_replace('/',' / ',$data);
    $str = str_replace('-',' - ',$str);
    $par = str_replace('/',' / ',$formato);
    $par = str_replace('-',' - ',$par);
    $params=array();
    $len = strlen($par)-1;
    for ($i=0; $i<$len; $i++) {
        if ($par[$i] == '%') {
            $params[]=$par[$i+1];
            $par[$i+1]='s';
        }
    }
    $arr = sscanf($str,$par);
    foreach ($params as $i=>$eparam) {
        $vals[$eparam]=$arr[$i];
    }
    return $vals;
}
function execInBackground($cmd) {
    if (substr(php_uname(), 0, 7) == "Windows"){
        pclose(popen("start /B ". $cmd, "r"));
    }
    else {
        exec($cmd . " > /dev/null &");
    }
}


//# DEPRECIADAS
/**
 * FUN��ES QUE ENTRARAM EM DESUSO
 */
function FormSelectMeses($label, $nomeCampo,$class=""){
    $meses = array($ling['mes'][1], $ling['mes'][2], $ling['mes'][3], $ling['mes'][4], $ling['mes'][5], $ling['mes'][6], $ling['mes'][7], $ling['mes'][8], $ling['mes'][10], $ling['mes'][11], $ling['mes'][12]);
    echo "\n<label class=\"$class\" for=\"$nomeCampo\">$label</label>\n";
    echo "<select class=\"campo_select\" name=\"$nomeCampo\" id=\"$nomeCampo\">\n";
    $valorSelecionado = $_GET[$nomeCampo];
    if($valorSelecionado == "")
    $valorSelecionado = date("m");
    for($cont =0; $cont < count($meses); $cont++)   {
        $mes = $meses[$cont];
        if (($cont+1) == $valorSelecionado)
        echo "<option value=\"".($cont+1)."\" selected>$mes</option>\n";
        else
        echo "<option value=\"".($cont+1)."\">$mes</option>\n";
    }
    echo "</select>\n";
}

function dias_atrazo($data1,$data2) {
    $data_inicio = explode("-",$data1);
    $data_fim = explode("-",$data2);
    $data_inicial = mktime("0","0","0",$data_inicio[1], $data_inicio[0], $data_inicio[2]);
    $data_final = mktime("0","0","0",$data_fim[1], $data_fim[0], $data_fim[2]);
    $tempo_unix = $data_final - $data_inicial;
    $periodo = floor($tempo_unix /(24*60*60));
    return $periodo;
}

function calc_tempo($data1,$hora1,$data2,$hora2){
    $i = split(":",$hora1);
    $j = split("-",$data1);
    $k = split(":",$hora2);
    $l = split("-",$data2);

    $tempo1 = mktime($i[0],$i[1],$i[2],$j[1],$j[2],$j[0]);
    $tempo2 = mktime($k[0],$k[1],$k[2],$l[1],$l[2],$l[0]);

    $tempo = round((($tempo2 - $tempo1)/60)/60,2);
    return $tempo;
}


function FormSelect_DEF($cc,$tb,$sel,$c1,$c2,$conexao,$formulario,$filtro='N',$classe="",$ajax="",$sql_filtro=""){
    global $manusis,$dba,$id,$tdb,$oq,$exe,$op,$form,$recb_valor;

    if ($classe == "") $classe="campo_select";
    $sql="SELECT * FROM $tb ORDER BY $c1 ASC";
    $resultado=$dba[$conexao] -> Execute($sql);
    if(!$resultado) {
        echo "<br><hr />".erromsg($dba[$conexao] -> ErrorMsg()."<br>SQL: $sql")."<hr /><br>";
    }
    else {

        echo "<span id=\"select_$cc\">
        <select class=\"$classe\" name=\"$cc\" id=\"$cc\" $ajax>
        <option value=\"0\"></option>\n";
        $campo = $resultado -> fields;
        while (!$resultado->EOF) {
            $campo = $resultado -> fields;
            $sc2=$campo[$c2];
            $sc_inteiro=$campo[$c1];
            $campo[$c1]=substr($campo[$c1],0,35);
            if ($tb == MAQUINAS) {
                $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            }
            elseif ($tb == SETORES) $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            elseif ($tb == AREAS) $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            elseif ($tb == MAQUINAS_CONJUNTO) $sc1=htmlentities($campo['TAG']."-".$campo[$c1]);
            elseif ($tb == EQUIPAMENTOS) $sc1=htmlentities($campo['COD']."-".$campo[$c1]);
            else $sc1=htmlentities($campo[$c1]);
            if ($sel == $sc2) echo "<option value=\"$sc2\" selected title=\"$sc_inteiro\">$sc1</option>\n";
            else echo "<option value=\"$sc2\" title=\"$sc_inteiro\">$sc1</option>\n";
            $resultado->MoveNext();
            $i++;
        }
        echo "</select></span>\n";
        if ($formulario != "") echo "<a href=\"javascript:janela('form.php?id=$id&f=$formulario&act=1&form_parm=1&atualiza=$cc', 'parm', ".$form[$formulario][0]['largura'].",".$form[$formulario][0]['altura'].")\"><img src=\"imagens/icones/add.png\" border=\"0\"></a>";
        if($filtro == "S") {
            echo "<span id=\"filtro_$cc\" style=\"padding-left:17px;visibility:hidden;position:absolute\">
            <input onkeypress=\"return AutoCompletaSelect(this, event, 'select_$cc', '../../parametros.php?id=formselect&cc=$cc&tb=$tb&c1=$c1&c2=$c2&conexao=$conexao&classe=$classe&texto=')\" type=\"text\" id=\"campo_filtro_$cc\" class=\"campo_text\" value=\"\" name=\"campo_filtro_$cc\" size=\"15\" maxlength=\"75\" />
            </span>
            <a href=\"javascript:abre_filtro_formselect('filtro_$cc','campo_filtro_$cc')\">
            <img src=\"".$manusis['url']."imagens/icones/16x16/ir.png\" border=\"0\" alt=\"Filtro\" title=\" Abrir/Fechar op&ccedil;&atilde;o de filtro \" /></a>";
        }
    }
}

function Volta_Permissao_Empresa() {
    global $dba, $tdb, $ling, $manusis;
    $mid = $_SESSION[ManuSess]['user']['MID'];
    $emp = VoltaFiltroEmpresa(EMPRESAS, 1);
    if(($emp['campo'] == "MID") and ($emp['mid'][0] == 0)) {
        return array();
    }
    else {
        return $emp['mid'];
    }
}

function TempoMaqOperacao($mesi,$anoi,$mesf,$anof,$mid_maq) {
    global $dba, $tdb, $ling, $manusis;
    if ($mesi != $mesf) {
        $sql = "SELECT HORAS,MID FROM ".MAQUINAS_DISPONIBILIDADE." WHERE (MES >= '$mesi' AND ANO >= '$anoi') AND (MES >= '$mesf' AND ANO >= '$anof') AND MID_MAQUINA = '$mid_maq'";
        $resultado=$dba[$tdb[MAQUINAS_DISPONIBILIDADE]['dba']] -> Execute($sql);
    }
    else {
        $sql = "SELECT HORAS,MID FROM ".MAQUINAS_DISPONIBILIDADE." WHERE MES = '$mesi' AND ANO = '$anoi' AND MID_MAQUINA = '$mid_maq'";
        $resultado=$dba[$tdb[MAQUINAS_DISPONIBILIDADE]['dba']] -> Execute($sql);
    }
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $tempo= $tempo + $campo['HORAS'];
        $resultado->MoveNext();
    }
    return $tempo;
}

// DETALHA A SOLICITA��O E SEPARA �S INFORMA�OES A SEREM ENVIADAS POR E-MAIL.
function detalha_sol ($mid_sol) {
    global $dba, $tdb, $manusis, $ling;

    $sql="SELECT *  FROM ".SOLICITACOES." WHERE MID = $mid_sol";
    if(! $re=$dba[$tdb[SOLICITACOES]['dba']] -> Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . " <br />Fun&ccedil;&atilde;o: " . __FUNCTION__ . " <br />Linha: " . __LINE__ . "<br/>" . $dba[$tdb[SOLICITACOES]['dba']]->ErrorMsg() . "<br/>" . $sql);
    }

    $campo_por_nome=$re->fields;

    // usuario que abril a solicita��o
    $sol_uid = (int)$campo_por_nome['USUARIO'];
    $sol_user = ($sol_uid) ? VoltaValor(USUARIOS,'NOME','MID',$sol_uid,0) : $manusis['admin']['nome'];

   ////////
    $cliente_desc = VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo_por_nome['MID_MAQUINA'],0);
    $crit_prod = VoltaValor(SOLICITACAO_SITUACAO_PRODUCAO,'DESCRICAO','MID',$campo_por_nome['PRODUCAO'],0);
    $crit_seg = VoltaValor(SOLICITACAO_SITUACAO_SEGURANCA,'DESCRICAO','MID',$campo_por_nome['SEGURANCA'],0);
    ////////

    $data = NossaData($campo_por_nome['DATA']);

    $email_texto = "<p align=\"left\">
    <strong>{$ling['N']}:</strong> {$campo_por_nome['NUMERO']}<br>
    <strong>{$ling['data_hora']}:</strong> {$data} {$campo_por_nome['HORA']}<br>
    <strong>{$ling['permissao3']}:</strong> $sol_user<br>
    <strong>{$ling['obj']}:</strong> $cliente_desc<br>";

    $email_texto .= "<strong>{$ling['crit_prod']}:</strong> $crit_prod <br />
    <strong>{$ling['crit_seg']}:</strong> $crit_seg <br />
    <strong>{$ling['assunto']}:</strong> ".$campo_por_nome['DESCRICAO']."<br>";

    $email_texto .= "<strong>{$ling['necessidade']}:</strong><br />".str_replace("\n",'<br>',$campo_por_nome['TEXTO'])."<br/>";

    $email_texto .= "<br /><strong>{$ling['dados_sol']}:</strong><br />";


    $email_texto .= "<strong>{$ling['nome']}:</strong> ".VoltaValor(USUARIOS,'NOME','MID',$campo_por_nome['USUARIO'],$dba[$tdb[USUARIOS]['dba']])."<br>";

    $fone = VoltaValor(USUARIOS,'CELULAR','MID',$campo_por_nome['USUARIO'],$dba[$tdb[USUARIOS]['dba']]);

    if ($fone) {
        $email_texto .= "<strong>{$ling['telefone']}:</strong> {$fone}<br>";
    }

    $email_sol = VoltaValor(USUARIOS,'EMAIL','MID',$campo_por_nome['USUARIO'],$dba[$tdb[USUARIOS]['dba']]);

    if ($email_sol) {
        $email_texto .= "<strong>{$ling['email']}:</strong> $email_sol<br>";
    }

    $res_resp=$dba[0] -> Execute("SELECT * FROM ".SOLICITACAO_RESPOSTA." WHERE MID_SOLICITACAO = '$mid_sol'");
    $ehist='';
    if (!$res_resp) erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[0] -> ErrorMsg());
    else while (!$res_resp->EOF) {
        $campo_resp = $res_resp->fields;
        $ehist .= "<br />".NossaData($campo_resp['DATA'])." {$campo_resp['HORA']} "
        .($campo_resp['USUARIO'] ? VoltaValor(USUARIOS,'NOME','MID',$campo_resp['USUARIO'],0) : $manusis['admin']['nome']);
        $ehist .= "<br>{$ling['resp_mot']}: ".htmlentities($campo_resp['TEXTO'])."<br />\n----------------\n";
        $res_resp -> MoveNext();
    }

    if($ehist != ""){
        $email_texto .= "<br />
        <strong>{$ling['resp']}:</strong>$ehist<br/>";
    }

    $email_texto .= "<br>
    </p>";

    return $email_texto;
}

// ENVIO DE E-MAIL PARA  EXECUTADAS NAS SOLICITA�OES.

function envia_email_sol ($mid_sol) {
    global $dba, $tdb, $manusis;

    $sql="SELECT *  FROM ".SOLICITACOES." WHERE MID = $mid_sol";
    if(! $re=$dba[$tdb[SOLICITACOES]['dba']] -> Execute($sql)){
        erromsg("Arquivo: " . __FILE__ . " <br />Fun&ccedil;&atilde;o: " . __FUNCTION__ . " <br />Linha: " . __LINE__ . "<br/>" . $dba[$tdb[SOLICITACOES]['dba']]->ErrorMsg() . "<br/>" . $sql);
    }

    $campo_por_nome=$re->fields;
    // Array com os emails
    $email_para = array();

    // usuario que abriu a solicita��o
    $sol_uid = (int)$campo_por_nome['USUARIO'];
    if($sol_uid != 0){
        $email_para[] = mb_strtolower(trim(VoltaValor(USUARIOS,'EMAIL','MID',$sol_uid,0)));
    }
    // caso tenha dado uma puta zica de n�o ter email nenhum para receber, manda pro usuario,
    $uid = (int)$_SESSION[ManuSess]['user']['MID'];
    $umail = VoltaValor(USUARIOS,'EMAIL','MID',$uid,0);
    if(($uid != 0) && (! in_array(mb_strtolower(trim($umail)), $email_para))) {
        $email_para[] = mb_strtolower(trim($umail));
    }
    // e na pior hipotese manda pro admin
    $email_para[] = mb_strtolower(trim($manusis['solicitacao']['informa']));

    $email_texto = detalha_sol($mid_sol);

    $cliente_desc = VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo_por_nome['MID_MAQUINA'],0);
    $email_assunto = "Solicita��o n.{$campo_por_nome['NUMERO']} para $cliente_desc";

    return MandaMail(implode(", ", $email_para), "suporte@manusis.com.br", $email_assunto, $email_texto);
}


function volta_qtd_maquianas_areas($midArea){
   global $dba, $tdb, $manusis;

   $midArea = (int) $midArea;

   if(!$midArea)return 0;

   $sql = 'SELECT COUNT(' . MAQUINAS .'.MID) as total FROM ' . AREAS .
    ' INNER JOIN ' . SETORES . ' ON '. AREAS . '.MID ='. SETORES . '.MID_AREA ' .
    'INNER JOIN ' . MAQUINAS . ' ON '. SETORES . '.MID = ' . MAQUINAS . '.MID_SETOR ' .
    'WHERE ' . AREAS . '.MID = ' . $midArea;

    $result = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);

    if(!$result->EOF)
    {
      $data = $result->fields;

      return $data['total'];
    }

    return 0;

}

function mDebug($var){

    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

/**
 *
 * Atualiza a tabela ORDEM_SPEED_CHECK de acordo com o plano Speed Check e Ordem de Servi�o informados
 *
 * @author Hudson Carlos de Souza
 *
 * @param Integer $speedCheck Mid do plano Speed Check
 * @param Integer $midOrdem Mid da Ordem
 *
 * @return true Retorna verdadeiro quando a atualiza��o foi efetuada com sucesso, falso caso haja erro.
 *
 * @uses
 * Deve ser utilizado para atualizar os registros de forma que n�o restem lixos na tabela<br>
 * Se n�o houverem atividades cadastradas na tabela ORDEM_SPEED_CHECK, os mesmos ser�o adicionados
 * Se houve mudan�a no plano, as atividades s�o atualizadas de acordo com as atividades do novo plano<br>
 * Se o par�metro $speedcheck for setado zero, todos os registros da tabela ORDEM_SPEED_CHECK ser�o removidos
 * para a ordem informada
 *
 */
function atualizaOrdemSpeedCheck($speedCheck, $midOrdem) {
    global $tdb, $dba, $ling;

    // Removendo registros da tabela
    if ($speedCheck == 0) {

        DeletaItem(ORDEM_SPEED_CHECK, 'MID_ORDEM', $midOrdem);

        $text = "ONDE MID_ORDEM = {$midOrdem}";

        logar(5, $text, ORDEM_SPEED_CHECK);

        // quando deleta retorna verdadeiro
        return true;
    }

    $sql = "SELECT * FROM " . ORDEM_SPEED_CHECK . " WHERE MID_ORDEM =  " . $midOrdem . "";
    if (!$rs = $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sql)) {
        erromsg("Erro ao localizar " . ORDEM_SPEED_CHECK . " em:<br />
				Fun��o: " . __FUNCTION__ . "<br />
				Linha: " . __LINE__ . "<br />
				erro: " . $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg() . "
				SQL: $sql
				");
    }
    // Adicionando registros na tabela
    elseif ($rs->EOF) {

        $sql = "SELECT * FROM " . SPEED_CHECK_ATIVIDADES . " WHERE SPEED_CHECK_ID = {$speedCheck}";
        if (!$rs2 = $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sql)) {
            erromsg("Erro ao localizar " . ORDEM_SPEED_CHECK . " em:<br />
					Fun��o: " . __FUNCTION__ . "<br />
					Linha: " . __LINE__ . "<br />
					erro: " . $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg() . "
					SQL: $sql
					");
        }
        elseif (!$rs2->EOF) {

            while (!$rs2->EOF) {

                $tmpMid = GeraMid(ORDEM_SPEED_CHECK, 'MID', $tdb[ORDEM_SPEED_CHECK]['dba']);

                $row2 = $rs2->fields;
                $sql = "INSERT INTO " . ORDEM_SPEED_CHECK . " (MID, MID_ORDEM, MID_SPEED_ATV, COMENTARIO, DIAGNOSTICO, NUMERO, DESCRICAO, ETAPA, TEMPO_PREVISTO) VALUES($tmpMid, $midOrdem, {$row2['MID']}, '', 1, '{$row2['NUMERO']}', '{$row2['DESCRICAO']}',  {$row2['ETAPA']},  " . (float) $row2['TEMPO_PREVISTO'] . ")";
                if (!$dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sql)) {
                    erromsg("Erro ao inserir " . ORDEM_SPEED_CHECK . " em:<br />
							Fun��o: " . __FUNCTION__ . "<br />
							Linha: " . __LINE__ . "<br />
							erro: " . $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg() . "
							SQL: $sql
							");
                    exit();
                }
                else {

                    $texto  = "MID = {$tmpMid} \n";
                    $texto .= "MID_ORDEM = {$midOrdem} \n";
                    $texto .= "MID_SPEED_ATV = {$row2['MID']} \n";
                    $texto .= "COMENTARIO = '' \n";
                    $texto .= "DIAGNOSTICO = 1 \n";
                    $texto .= "NUMERO =  {$row2['NUMERO']} \n";
                    $texto .= "DESCRICAO =  {$row2['DESCRICAO']} \n";
                    $texto .= "ETAPA =  {$row2['ETAPA']} \n";
                    $texto .= "TEMPO_PREVISTO =  {$row2['TEMPO_PREVISTO']} \n";

                    logar(3, $texto, ORDEM_SPEED_CHECK);
                }
                $rs2->MoveNext();
            }

            // retorna verdadeiro ao cadastrar todas as atividades do plano ta tabela relacional ORDEM_SPEED_CHECK
            return true;
        }
        else {

            // Retorna falso se o plano n�o possuir atividades
            return false;
        }
    }
    // Atualizando a tabela caso o plano seja diferente
    else {
        
        // Recuperando o plano atual para a ordem
        $atvSpeed = VoltaValor(ORDEM_SPEED_CHECK, 'MID_SPEED_ATV', 'MID_ORDEM', $midOrdem);
        $midSpeedCheck = VoltaValor(SPEED_CHECK_ATIVIDADES, 'SPEED_CHECK_ID', 'MID', $atvSpeed);
        
        // Para preservar os apontamentos, atualizamos apenas se for necess�rio em caso de troca de plano
        if($midSpeedCheck != $speedCheck){
            // Removendo as atividades para o plano antigo
            if (atualizaOrdemSpeedCheck(0, $midOrdem)) {

                // Inserindo as atividades para o novo plano
                if (atualizaOrdemSpeedCheck($speedCheck, $midOrdem)) {

                    // se inseriu retorna verdadeiro
                    return true;
                }
                else {

                    // caso haja erro ao inserir retorna false
                    return false;
                }
            }
            else {

                // erro ao remover as atividades
                return false;
            }
        }
        else{
            return true;
        }
    }
}

/**
 * Realiza a movimenta��o de componente conforme o procedimento do formul�rio de movimenta��o de componente.
 * 
 * @param int $midComponente Componente a ser movimentado
 * @param int $midObjDestino Objeto de Manuten��o de destino
 * @param int $posDestino Posi��o de destino (Deve estar ligada ou mesmo objeto informado)
 * @param int $motivo Motivo da movimenta��o ou descri��o do procedimento.
 * @return boolean TRUE se a movimenta��o ocorreu devidamente
 */
function MovimentaComponente($midComponente, $midObjDestino = 0, $posDestino = 0, $motivo = "") {
    global $tdb, $dba, $ling;

    // LOcaliza��es de origem
    $conjOrig   = VoltaValor(EQUIPAMENTOS, "MID_CONJUNTO", "MID", $midComponente, $tdb[EQUIPAMENTOS]['dba']);
    $objOrig    = VoltaValor(EQUIPAMENTOS, "MID_MAQUINA", "MID", $midComponente, $tdb[EQUIPAMENTOS]['dba']);

    // Buscando a empresa do destino
    $emp_tmp = ($midObjDestino != 0) ? $midObjDestino : $objOrig;
    $emp_tmp = VoltaValor(MAQUINAS, 'MID_SETOR', 'MID', $emp_tmp, $tdb[MAQUINAS]['dba']);
    $emp_tmp = VoltaValor(SETORES, 'MID_AREA', 'MID', $emp_tmp, $tdb[SETORES]['dba']);
    $emp_tmp = VoltaValor(AREAS, 'MID_EMPRESA', 'MID', $emp_tmp, $tdb[AREAS]['dba']);
    
    
    // Enviando o componente para o destino
    $set = array();
    $set[] = "MID_CONJUNTO = $posDestino";
    $set[] = "MID_MAQUINA  = $midObjDestino";
    $set[] = "MID_EMPRESA  = $emp_tmp";
    
    if (UpdateItem(EQUIPAMENTOS, implode(',', $set), 'MID', $midComponente) == 1){
    
        
        $dados['MID'] = GeraMid(MOV_EQUIPAMENTO, "MID", $tdb[MOV_EQUIPAMENTO]['dba']);
        $dados['MID_EQUIPAMENTO'] = $midComponente;
        $dados['MID_CONJUNTO'] = $conjOrig;
        $dados['MID_MAQUINA'] = $objOrig;
        $dados['MID_DCONJUNTO'] = $posDestino;
        $dados['MID_DMAQUINA'] = $midObjDestino;
        $dados['MOTIVO'] = "'{$motivo}'";
        $dados['DATA'] = "'" . date("Y-m-d") . "'";
        
        $sql = "INSERT INTO ".MOV_EQUIPAMENTO." (".implode(',', array_keys($dados)).") VALUES (".implode(',', array_values($dados)).")";
        if (!$rs = $dba[$tdb[MOV_EQUIPAMENTO]['dba']]->Execute($sql)) {
            erromsg("Erro ao processar dados de {$tdb[MOV_EQUIPAMENTO]['DESC']} em:<br />
                Arquivo: " . __FILE__ . " <br />
                Linha: " . __LINE__ . " <br />
                Erro: {$dba[$tdb[MOV_EQUIPAMENTO]['dba']]->ErrorMsg()}<br />
                SQL: $sql
                ");
        }
        else{
            logar(3, $motivo, MOV_EQUIPAMENTO, 'MID', $dados['MID']);
        }
        
        // Update nas ordens de servi�o
        $setor  = (int)VoltaValor(MAQUINAS, "MID_SETOR", "MID", $midObjDestino);
        $area   = (int)VoltaValor(SETORES, "MID_AREA", "MID", $setor, 0);
        
        $set = array();
        $set[] = "MID_SETOR = $setor";
        $set[] = "MID_CONJUNTO = $posDestino";
        $set[] = "MID_AREA = $area";
        $set[] = "MID_MAQUINA = $midObjDestino";
        UpdateItem(ORDEM_PLANEJADO, implode(',', $set), "STATUS = 1 AND MID_EQUIPAMENTO", $midComponente);
        
        // Update nos pontos de preditiva
        UpdateItem(PONTOS_PREDITIVA, "MID_MAQUINA = $midObjDestino", 'MID_EQUIPAMENTO', $midComponente);

        // Update nas Pend�ncias
        UpdateItem(PENDENCIAS, "MID_CONJUNTO = $posDestino, MID_MAQUINA = $midObjDestino", 'STATUS = 1 AND MID_EQUIPAMENTO', $midComponente);
        
        return true;
    }
    
    return false;
    
}

function HoraValida($h) {
    if (($h >= 0) and ($h <= 23))
        return true; else
        return false;
}

function MSValido($m) {
    if (($m >= 0) and ($m <= 59))
        return true; else
        return false;
}


/**
 *  RETORNA UM ARRAY COM O CAMPO, TIPO DE DADO E QUANTIDADE DE CASAS DECIMAIS
 * @param $table Nome da tabela a ser verificada
 * @param string $field Campo n�o obrigat�rio se este n�o for informado, todos os campos da tabela ser�o retornados
 * @return array Array ( [CAMPO] => Array  ( [TIPO] => TIPO  [DECIMAIS] => CASAS_DECIMAIS  [NULO] => BIN�RIO (0 OU 1) )
 * 
 */
function GetDataType($table, $field = "") {
    global $dba, $tdb;

    $dataType = array();

    if (!$rs = $dba[$tdb[$table]['dba']]->MetaColumns($table)) {
        erromsg("Erro ao buscar Tipo de dados da tabela $table <br /> " . $dba[$tdb[$table]['dba']]->ErrorMsg() . "");
    }
    else {
        // RECUPERANDO OS DATA TYPES.
        // RETORNANDO O DATATYPE PARA O CAMPO INFORMADO
        if ($field != "") {

            $dataType['TIPO'] = $rs[$field]->type;

            $dataType['DECIMAIS'] = (int) $rs[$field]->scale;

            $dataType['NAO_NULO'] = (int) $rs[$field]->not_null;
        }
        else {

            // SALVANDO OS DATATYPES EM UM ARRAY PARA RETORNO Array ( [CAMPO] => Array  ( [TIPO] => TIPO  [DECIMAIS] => CASAS_DECIMAIS )

            foreach ($rs as $field => $val) {

                $dataType[$field]['TIPO'] = $rs[$field]->type;

                $dataType[$field]['DECIMAIS'] = (int) $rs[$field]->scale;

                $dataType[$field]['NAO_NULO'] = (int) $rs[$field]->not_null;
            }
        }
    }
//    echo "<pre>";
//        print_r($rs);
//    echo "</pre>";
    return $dataType;
}
