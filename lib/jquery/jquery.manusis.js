// FUN��ES JQUERY
// CONFIGURANDO PARA APARECER A JANELA DE CARREGANDO!!!!
$.ajaxSetup({
    beforeSend: function(){
        mostraCarregando(true);
    },
    complete: function(){
        mostraCarregando(false);
    }
});


function atualiza_area3(id, destino) {
    $.ajax({
        url: destino,
        success: function(data) {
            $('#' + id).html(data);
        }
    });
}

function atualiza_area_frame3(id, destino) {
    $.ajax({
        url: destino,
        success: function(data) {
            window.parent.$('#' + id).html(data);
        }
    });
}

function ajax_get3(destino){
    $.ajax({
        url: destino,
        success: function(data) {
            if (data != '') {
                alert(data);
            }
        }
    });
}
