<?php
// Recuperando valores
if ((int)$_GET['modo_visao'] != 0) {
    $_SESSION[ManuSess]['lt']['os']['modo_visao'] = (int)$_GET['modo_visao'];
}
elseif ($_SESSION[ManuSess]['lt']['os']['modo_visao'] == 0) {
    $_SESSION[ManuSess]['lt']['os']['modo_visao'] = 2;
}
$modo_visao = $_SESSION[ManuSess]['lt']['os']['modo_visao'];

// Encontrando a semana
if($_SESSION[ManuSess]['lt']['os']['semini'] == 0) {
    $_SESSION[ManuSess]['lt']['os']['semini'] = date('W');
}
elseif ((int)$_GET['semini'] != 0) {
    $_SESSION[ManuSess]['lt']['os']['semini'] = (int)$_GET['semini'];
}
$semini = $_SESSION[ManuSess]['lt']['os']['semini'];

// Encontrando o ano
if($_SESSION[ManuSess]['lt']['os']['anoini'] == 0) {
    $_SESSION[ManuSess]['lt']['os']['anoini'] = date('o');
}
elseif((int)$_GET['anoini'] != 0) {
    $_SESSION[ManuSess]['lt']['os']['anoini'] = (int) $_GET['anoini'];
}
$anoini = $_SESSION[ManuSess]['lt']['os']['anoini'];


// Primeiro dia da semana
$mki = VoltaTime("00:00:00", VoltaDataSeg($semini, $anoini));
$dia_sem_ini = date('N', $mki);
$dias_menos = (1 - $dia_sem_ini) * -1;
$mki = $mki - ($mktime_1_dia * $dias_menos);

// Datas semana
$datas_sem[1] = date('d/m/Y', $mki);
$datas_sem[2] = date('d/m/Y', $mki + ($mktime_1_dia * 1));
$datas_sem[3] = date('d/m/Y', $mki + ($mktime_1_dia * 2));
$datas_sem[4] = date('d/m/Y', $mki + ($mktime_1_dia * 3));
$datas_sem[5] = date('d/m/Y', $mki + ($mktime_1_dia * 4));
$datas_sem[6] = date('d/m/Y', $mki + ($mktime_1_dia * 5));
$datas_sem[7] = date('d/m/Y', $mki + ($mktime_1_dia * 6));

// FILTROS
$sqlvar = "";

if (($filtro_origem != 0) and ($filtro_origem != 5)) $sqlvar.="AND O.TIPO = '$filtro_origem' ";
elseif ($filtro_origem == 5) $sqlvar.="AND O.TIPO = '0' ";

if ($filtro_empresa != 0) $sqlvar.="AND O.MID_EMPRESA = '$filtro_empresa' ";
if ($filtro_area != 0) $sqlvar.="AND O.MID_AREA = '$filtro_area' ";
if ($filtro_setor != 0) $sqlvar.="AND O.MID_SETOR = '$filtro_setor' ";
if (count($filtro_natureza) > 0) $sqlvar .= "AND O.NATUREZA IN (".implode(", ", array_values($filtro_natureza)).") ";

// Filtro por empresa
$filtro_tmp = VoltaFiltroEmpresa(ORDEM, 2, false, 'O');
if($filtro_tmp != '') {
    // MAQUINAS VISIVEIS NAS ROTAS
    $filtro_lub = VoltaFiltroEmpresa(ORDEM_LUB, 2);
    // PERMISSÃO POR EMPRESA NAS OS E EM ROTAS
    $sqlvar .= " AND ($filtro_tmp OR (O.TIPO = 2 AND (SELECT COUNT(MID) FROM " . ORDEM_LUB . " WHERE MID_ORDEM = O.MID AND $filtro_lub) > 0))";
}

// FILTRO POR CENTRO DE CUSTO
if(($_SESSION[ManuSess]['user']['MID'] != 0) and (VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, 'MID', 'USUARIO', (int)$_SESSION[ManuSess]['user']['MID']) != 0)) {
   $sqlvar .= " AND O.CENTRO_DE_CUSTO IN (SELECT CENTRO_DE_CUSTO FROM ".USUARIOS_PERMISAO_CENTRODECUSTO." WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}')";
}

// FILTRO POR DESCRIÇÃO
if ($desc != '') {
    $desc_sql = LimpaTexto($desc);

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2, false, "M");
    $fil_emp = ($fil_emp != "")? " AND " . $fil_emp : "";

    // Filtra as maquinas
    $sql = "SELECT M.MID, M.COD, M.DESCRICAO FROM ".MAQUINAS." M WHERE (M.COD LIKE '%$desc_sql%' OR M.DESCRICAO LIKE '%$desc_sql%') $fil_emp";
    $resultado=$dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
    $filsql = '';
    while (! $resultado->EOF) {
        $campo = $resultado->fields;
        $filsql .= ($filsql != "")? " OR " : "";
        $filsql .= "O.MID_MAQUINA = '{$campo['MID']}'";
        $resultado->MoveNext();
    }

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(EQUIPAMENTOS, 2);
    $fil_emp = ($fil_emp != "")? " AND " . $fil_emp : "";

    // Filtra equipamentos
    $sql = "SELECT MID FROM ".EQUIPAMENTOS." WHERE (COD LIKE '%$desc_sql%' OR DESCRICAO LIKE '%$desc_sql%')  $fil_emp";
    $resultado=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute($sql);
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $filsql .= ($filsql != "")? " OR " : "";
        $filsql .= "O.MID_EQUIPAMENTO = '{$campo['MID']}'";
        $resultado->MoveNext();
    }

    // Busca na descrição do servico
    $filsql .= ($filsql != "")? " OR " : "";
    $filsql .= "O.TEXTO LIKE '%$desc_sql%'";


    $sqlvar .= " AND ($filsql)";
}

// JUNTANDO OS VALORES
$dados = array();

// BUSCANDO INFORMAÇÕES
$sql = "SELECT O.* FROM " . ORDEM . " O WHERE O.STATUS = 1 AND O.DATA_PROG >= '" . DataSQL($datas_sem[1]) . "' AND O.DATA_PROG <= '" . DataSQL($datas_sem[7]) . "' $sqlvar";
if(! $rs = $dba[$tdb[ORDEM]['dba']] -> Execute($sql)) {
    erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM]['dba']] -> ErrorMsg() . "<br />" . $sql);
}
while(! $rs->EOF) {
    $os = $rs->fields;
    $osmid = $os['MID'];

    // Dia da semana
    $mk = VoltaTime('00:00:00', NossaData($os['DATA_PROG']));
    $dia = date('N', $mk);

    // Definindo agregador quando simples
    $agregador = 0;
    if ($modo_visao == 4) {
        $agregador = (int)$os['MID_MAQUINA'];

        // Não mostrar linhas em branco
        if (($agregador == 0) and ($os['TIPO'] != 2)) {
            $rs->MoveNext();
            continue;
        }
    }
    elseif ($modo_visao == 5) {
        $agregador = (int)$os['MID_EQUIPAMENTO'];

        // Não mostrar linhas em branco
        if ($agregador == 0) {
            $rs->MoveNext();
            continue;
        }
    }

    // Buscando tempo  PREVISTO
    if(($modo_visao == 1) or ($modo_visao == 3)) {
        $sql_tmp = "SELECT * FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = $osmid";
        if(! $rs_tmp = $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute($sql_tmp)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
        }
        while(! $rs_tmp->EOF) {
            if ($modo_visao == 1) {
                $agregador = (int) $rs_tmp->fields['MID_FUNCIONARIO'];
            }
            elseif ($modo_visao == 3) {
                $agregador = (int) VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $rs_tmp->fields['MID_FUNCIONARIO']);
            }

            // PREVISTO POR DIA
            if (! $dados[$agregador][$dia]['TP']) {
                $dados[$agregador][$dia]['TP'] = 0;
            }
            $dados[$agregador][$dia]['TP'] += $rs_tmp->fields['TEMPO'];

            // Salvando OS
            if (! $dados[$agregador][$dia]['OS']['TP'][$osmid]) {
                $dados[$agregador][$dia]['OS']['TP'][$osmid] = $os['NUMERO'];
            }

            $rs_tmp->MoveNext();
        }
    }
    // Especialidades
    if(($modo_visao == 2) or ($modo_visao == 4) or ($modo_visao == 5)) {
        $sql_tmp = "SELECT * FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid";
        if(! $rs_tmp = $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute($sql_tmp)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
        }
        while(! $rs_tmp->EOF) {
            if ($modo_visao == 2) {
                $agregador = (int) $rs_tmp->fields['MID_ESPECIALIDADE'];
            }
            // PREVISTO POR DIA
            if (! $dados[$agregador][$dia]['TP']) {
                $dados[$agregador][$dia]['TP'] = 0;
            }
            $dados[$agregador][$dia]['TP'] += $rs_tmp->fields['TEMPO'];

            // Salvando OS
            if (! $dados[$agregador][$dia]['OS']['TP'][$osmid]) {
                $dados[$agregador][$dia]['OS']['TP'][$osmid] = $os['NUMERO'];
            }

            $rs_tmp->MoveNext();
        }
    }
    // MATERIAIS
    if(($modo_visao == 6)) {
        // Buscando material  PREVISTO
        $sql_tmp = "SELECT * FROM " . ORDEM_MAT_PREVISTO . " WHERE MID_ORDEM = $osmid";
        if(! $rs_tmp = $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute($sql_tmp)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
        }
        while(! $rs_tmp->EOF) {
            if ($modo_visao == 6) {
                $agregador = (int) $rs_tmp->fields['MID_MATERIAL'];
            }

            // PREVISTO POR DIA
            if (! $dados[$agregador][$dia]['MP']) {
                $dados[$agregador][$dia]['MP'] = 0;
            }
            $dados[$agregador][$dia]['MP'] += $rs_tmp->fields['QUANTIDADE'];

            // Salvando OS
            if (! $dados[$agregador][$dia]['OS']['MP'][$osmid]) {
                $dados[$agregador][$dia]['OS']['MP'][$osmid] = $os['NUMERO'];
            }

            $rs_tmp->MoveNext();
        }

        // Buscando material UTILIZADO
        $sql_tmp = "SELECT * FROM " . ORDEM_MATERIAL . " WHERE MID_ORDEM = $osmid";
        if(! $rs_tmp = $dba[$tdb[ORDEM_MATERIAL]['dba']] -> Execute($sql_tmp)) {
            erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MATERIAL]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
        }
        while(! $rs_tmp->EOF) {
            if ($modo_visao == 6) {
                $agregador = (int) $rs_tmp->fields['MID_MATERIAL'];
            }

            // PREVISTO POR DIA
            if (! $dados[$agregador][$dia]['MU']) {
                $dados[$agregador][$dia]['MU'] = 0;
            }
            $dados[$agregador][$dia]['MU'] += $rs_tmp->fields['QUANTIDADE'];

            // Salvando OS
            if (! $dados[$agregador][$dia]['OS']['MU'][$osmid]) {
                $dados[$agregador][$dia]['OS']['MU'][$osmid] = $os['NUMERO'];
            }

            $rs_tmp->MoveNext();
        }
    }

    // Salvando OS
    if(($modo_visao == 4) or ($modo_visao == 5)) {
        // Tempo de serviço POR DIA
        $dados[$agregador][$dia]['TS'] += $os['TEMPO_TOTAL'];

        // TEMPO GERAL
        $dados[$agregador]['TS'] += $os['TEMPO_TOTAL'];

        // Ordens POR DIA
        $dados[$agregador][$dia]['OS']['TS'][$osmid] = $os['NUMERO'];
    }

    $rs->MoveNext();
}


// Para outros tipos usar tempo MO
if (($modo_visao == 1) or ($modo_visao == 2) or ($modo_visao == 3)) {
    $sql_tmp = "SELECT MO.* FROM " . ORDEM_MAODEOBRA . " MO, " . ORDEM . " O WHERE MO.DATA_INICIO >= '" . DataSQL($datas_sem[1]) . "' AND MO.DATA_INICIO <= '" . DataSQL($datas_sem[7]) . "' AND MO.MID_ORDEM = O.MID AND O.STATUS = 1 $sqlvar";
    if(! $rs_tmp = $dba[$tdb[ORDEM_MAODEOBRA]['dba']] -> Execute($sql_tmp)) {
        erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MAODEOBRA]['dba']] -> ErrorMsg() . "<br />" . $sql_tmp);
    }
    while(! $rs_tmp->EOF) {
        if ($modo_visao == 1) {
            $agregador = (int) $rs_tmp->fields['MID_FUNCIONARIO'];
        }
        elseif ($modo_visao == 2) {
            $agregador = (int) VoltaValor(FUNCIONARIOS, 'ESPECIALIDADE', 'MID', $rs_tmp->fields['MID_FUNCIONARIO']);
        }
        elseif ($modo_visao == 3) {
            $agregador = (int) VoltaValor(FUNCIONARIOS, 'EQUIPE', 'MID', $rs_tmp->fields['MID_FUNCIONARIO']);
        }

        // Calculando o tempo
        $mki_tmp = VoltaTime($rs_tmp->fields['HORA_INICIO'], NossaData($rs_tmp->fields['DATA_INICIO']));
        $mkf_tmp = VoltaTime($rs_tmp->fields['HORA_FINAL'], NossaData($rs_tmp->fields['DATA_FINAL']));

        $dia_tmp = date('N', $mki_tmp);

        $tempo = round(($mkf_tmp - $mki_tmp) / 60 / 60, 2);

        // TEMPO DE SERVIÇO POR DIA
        $dados[$agregador][$dia_tmp]['TS'] += $tempo;

        // Ordens POR DIA
        $dados[$agregador][$dia_tmp]['OS']['TS'][$rs_tmp->fields['MID_ORDEM']] = VoltaValor(ORDEM, 'NUMERO', 'MID', $rs_tmp->fields['MID_ORDEM']);

        // TEMPO DE SERVIÇO GERAL
        $dados[$agregador]['TS'] += $tempo;


        $rs_tmp->MoveNext();
    }
}

/*
echo "<pre>";
print_r($dados);
echo "</pre>";
*/

// TOTAIS
// Tempo previsto
$total_tp[1] = 0;
$total_tp[2] = 0;
$total_tp[3] = 0;
$total_tp[4] = 0;
$total_tp[5] = 0;
$total_tp[6] = 0;
$total_tp[7] = 0;

// Tempo disponivel
$total_dis[1] = 0;
$total_dis[2] = 0;
$total_dis[3] = 0;
$total_dis[4] = 0;
$total_dis[5] = 0;
$total_dis[6] = 0;
$total_dis[7] = 0;

// Tempo de serviço
$total_ts[1] = 0;
$total_ts[2] = 0;
$total_ts[3] = 0;
$total_ts[4] = 0;
$total_ts[5] = 0;
$total_ts[6] = 0;
$total_ts[7] = 0;

// Materias previstos
$total_mp[1] = 0;
$total_mp[2] = 0;
$total_mp[3] = 0;
$total_mp[4] = 0;
$total_mp[5] = 0;
$total_mp[6] = 0;
$total_mp[7] = 0;

// Materias utilizados
$total_mu[1] = 0;
$total_mu[2] = 0;
$total_mu[3] = 0;
$total_mu[4] = 0;
$total_mu[5] = 0;
$total_mu[6] = 0;
$total_mu[7] = 0;


// Semana anterior
if ($semini == 1) {
    $mk_tmp = mktime(0, 0, 0, 12, 31, $anoini - 1);
    $sem_ant = date('W', $mk_tmp);
    $ano_ant = $anoini - 1;
}
else {
    $sem_ant = $semini - 1;
    $ano_ant = $anoini;
}


// Semana anterior
if ($semini == 52) {
    $mk_tmp = mktime(0, 0, 0, 12, 31, $anoini);
    $sem_pro = date('W', $mk_tmp);
    if ($sem_pro == 53) {
        $sem_pro = $semini + 1;
        $ano_pro = $anoini;
    }
    else {
        $sem_pro = 1;
        $ano_pro = $anoini + 1;
    }
}
elseif ($semini == 53) {
    $sem_pro = 1;
    $ano_pro = $anoini + 1;
}
else {
    $sem_pro = $semini + 1;
    $ano_pro = $anoini;
}

echo "
<br clear=\"all\" />


<table width=\"100%\" cellspacing=\"0\" style=\"font-size:10px;border:0px;\">
<tr>

<td class=\"".(($modo_visao == 1)?"aba_sel":"aba")."\" id=\"prog_func\">
<img src=\"imagens/icones/22x22/funcionario.png\" border=\"0\" align=\"bottom\" /> ".$tdb[FUNCIONARIOS]['DESC']."
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 2)?"aba_sel":"aba")."\" id=\"prog_esp\">
<img src=\"imagens/icones/22x22/especialidades.png\" border=\"0\" align=\"bottom\" /> ".$tdb[ESPECIALIDADES]['DESC']."
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 3)?"aba_sel":"aba")."\" id=\"prog_equip\">
<img src=\"imagens/icones/22x22/equipe.png\" border=\"0\" align=\"bottom\" /> ".$tdb[EQUIPES]['DESC']."
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 4)?"aba_sel":"aba")."\" id=\"prog_maq\">
<img src=\"imagens/icones/22x22/local3.png\" border=\"0\" align=\"bottom\" /> ".$tdb[MAQUINAS]['DESC']."
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 5)?"aba_sel":"aba")."\" id=\"prog_comp\">
<img src=\"imagens/icones/22x22/equip.png\" border=\"0\" align=\"bottom\" /> ".$tdb[EQUIPAMENTOS]['DESC']."
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"".(($modo_visao == 6)?"aba_sel":"aba")."\" id=\"prog_mat\">
<img src=\"imagens/icones/22x22/material.png\" border=\"0\" align=\"bottom\" /> ".$tdb[MATERIAIS]['DESC']."
</td>
<td class=\"aba_entre\">&nbsp;</td>

<td class=\"aba_fim\" style=\"text-align:center;\">";

echo "<a href=\"javascript: muda_semana_prog('$modo_visao', '$ano_ant', '$sem_ant')\">
<img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" align=\"top\" id=\"prog_sem_ant\"  title=\"Semana anterior\" style=\"cursor:pointer;\" />
</a>\n";

echo "<label for=\"modo_visao\">{$ling['semana_desc']}:</label>\n";
echo "<input type=\"text\" id=\"semini\" name=\"semini\" size=\"1\" class=\"campo_text_ob\" value=\"$semini\" /> / \n";
echo "<input type=\"text\" id=\"anoini\" name=\"anoini\" size=\"2\" class=\"campo_text_ob\" value=\"$anoini\" />\n";
echo "<input type=\"button\" id=\"env_data3\" name=\"env_data3\" class=\"botao\" value=\"IR\" />\n";

echo "<a href=\"javascript: muda_semana_prog('$modo_visao', '$ano_pro', '$sem_pro')\" title=\"{$ling['ord_prox_sem']}\"><img src=\"imagens/icones/22x22/avancar.png\" border=\"0\" align=\"top\" /></a>\n";

echo "</td>

<td class=\"aba_fim\">&nbsp;</td>
</tr>
<tr><td colspan=\"20\" class=\"aba_quadro\" valign=\"top\">";



echo "<table border=\"0\" width=\"100%\" id=\"lt_tabela_\" cellspacing=\"0\" class=\"tabela_prog\">
<thead>
<tr>
<th colspan=\"2\">";

if ($modo_visao == 1) echo "" . $tdb[FUNCIONARIOS]['NOME'] . "";
if ($modo_visao == 2) echo "" . $tdb[ESPECIALIDADES]['DESCRICAO'] . "";
if ($modo_visao == 3) echo "" . $tdb[EQUIPES]['DESCRICAO'] . "";
if ($modo_visao == 4) echo "" . $tdb[MAQUINAS]['DESCRICAO'] . "";
if ($modo_visao == 5) echo "" . $tdb[EQUIPAMENTOS]['DESCRICAO'] . "";
if ($modo_visao == 6) echo "" . $tdb[MATERIAIS]['DESCRICAO'] . "";

echo "</th>
<th>{$ling['semana'][2]}.</th>
<th>{$ling['semana'][3]}.</th>
<th>{$ling['semana'][4]}.</th>
<th>{$ling['semana'][5]}.</th>
<th>{$ling['semana'][6]}.</th>
<th>{$ling['semana'][7]}.</th>
<th>{$ling['semana'][1]}.</th>
<th width=\"15%\">{$ling['prin01']}</th>
</tr>
</thead>
<tbody>";

// Valor do rowspan
$rowspan = 4;

// Não mostra linha do disponivel
if (($modo_visao == 4) or ($modo_visao == 5) or ($modo_visao == 6)) {
    $rowspan = 3;
}

// Listando os dados
foreach ($dados as $agregador => $val) {
    $cor = ($cor == "cor1")? "cor2" : "cor1";

    // FUNCIONARIOS
    if($modo_visao == 1) {
        $visual = htmlentities(VoltaValor(FUNCIONARIOS, 'NOME', 'MID', $agregador));
        $val[1]['DIS'] = round(TurnoTotal($datas_sem[1], $datas_sem[1], $agregador), 2);
        $val[1]['DIS'] = ($val[1]['DIS'] > 0)? $val[1]['DIS'] : '';
        $val[2]['DIS'] = round(TurnoTotal($datas_sem[2], $datas_sem[2], $agregador), 2);
        $val[2]['DIS'] = ($val[2]['DIS'] > 0)? $val[2]['DIS'] : '';
        $val[3]['DIS'] = round(TurnoTotal($datas_sem[3], $datas_sem[3], $agregador), 2);
        $val[3]['DIS'] = ($val[3]['DIS'] > 0)? $val[3]['DIS'] : '';
        $val[4]['DIS'] = round(TurnoTotal($datas_sem[4], $datas_sem[4], $agregador), 2);
        $val[4]['DIS'] = ($val[4]['DIS'] > 0)? $val[4]['DIS'] : '';
        $val[5]['DIS'] = round(TurnoTotal($datas_sem[5], $datas_sem[5], $agregador), 2);
        $val[5]['DIS'] = ($val[5]['DIS'] > 0)? $val[5]['DIS'] : '';
        $val[6]['DIS'] = round(TurnoTotal($datas_sem[6], $datas_sem[6], $agregador), 2);
        $val[6]['DIS'] = ($val[6]['DIS'] > 0)? $val[6]['DIS'] : '';
        $val[7]['DIS'] = round(TurnoTotal($datas_sem[7], $datas_sem[7], $agregador), 2);
        $val[7]['DIS'] = ($val[7]['DIS'] > 0)? $val[7]['DIS'] : '';
    }
    // ESPECIALIDADES
    elseif($modo_visao == 2) {
        $visual = htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $agregador));
        $visual = ($visual != '')? $visual : $ling['ord_sem_esp'];

        $val[1]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[1], $datas_sem[1], $agregador), 2);
        $val[1]['DIS'] = ($val[1]['DIS'] > 0)? $val[1]['DIS'] : '';
        $val[2]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[2], $datas_sem[2], $agregador), 2);
        $val[2]['DIS'] = ($val[2]['DIS'] > 0)? $val[2]['DIS'] : '';
        $val[3]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[3], $datas_sem[3], $agregador), 2);
        $val[3]['DIS'] = ($val[3]['DIS'] > 0)? $val[3]['DIS'] : '';
        $val[4]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[4], $datas_sem[4], $agregador), 2);
        $val[4]['DIS'] = ($val[4]['DIS'] > 0)? $val[4]['DIS'] : '';
        $val[5]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[5], $datas_sem[5], $agregador), 2);
        $val[5]['DIS'] = ($val[5]['DIS'] > 0)? $val[5]['DIS'] : '';
        $val[6]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[6], $datas_sem[6], $agregador), 2);
        $val[6]['DIS'] = ($val[6]['DIS'] > 0)? $val[6]['DIS'] : '';
        $val[7]['DIS'] = round(TurnoTotalEspecialidade($datas_sem[7], $datas_sem[7], $agregador), 2);
        $val[7]['DIS'] = ($val[7]['DIS'] > 0)? $val[7]['DIS'] : '';
    }
    // EQUIPES
    elseif($modo_visao == 3) {
        $visual = htmlentities(VoltaValor(EQUIPES, 'DESCRICAO', 'MID', $agregador));
        $visual = ($visual != '')? $visual : $ling['ord_sem_eqp'];

        $val[1]['DIS'] = round(TurnoTotalEquipe($datas_sem[1], $datas_sem[1], $agregador), 2);
        $val[1]['DIS'] = ($val[1]['DIS'] > 0)? $val[1]['DIS'] : '';
        $val[2]['DIS'] = round(TurnoTotalEquipe($datas_sem[2], $datas_sem[2], $agregador), 2);
        $val[2]['DIS'] = ($val[2]['DIS'] > 0)? $val[2]['DIS'] : '';
        $val[3]['DIS'] = round(TurnoTotalEquipe($datas_sem[3], $datas_sem[3], $agregador), 2);
        $val[3]['DIS'] = ($val[3]['DIS'] > 0)? $val[3]['DIS'] : '';
        $val[4]['DIS'] = round(TurnoTotalEquipe($datas_sem[4], $datas_sem[4], $agregador), 2);
        $val[4]['DIS'] = ($val[4]['DIS'] > 0)? $val[4]['DIS'] : '';
        $val[5]['DIS'] = round(TurnoTotalEquipe($datas_sem[5], $datas_sem[5], $agregador), 2);
        $val[5]['DIS'] = ($val[5]['DIS'] > 0)? $val[5]['DIS'] : '';
        $val[6]['DIS'] = round(TurnoTotalEquipe($datas_sem[6], $datas_sem[6], $agregador), 2);
        $val[6]['DIS'] = ($val[6]['DIS'] > 0)? $val[6]['DIS'] : '';
        $val[7]['DIS'] = round(TurnoTotalEquipe($datas_sem[7], $datas_sem[7], $agregador), 2);
        $val[7]['DIS'] = ($val[7]['DIS'] > 0)? $val[7]['DIS'] : '';
    }
    // MAQUINAS
    elseif($modo_visao == 4) {
        $visual = htmlentities(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $agregador));
        $visual = ($visual != '')? $visual : "ROTAS";
    }
    // COMPONENTES
    elseif($modo_visao == 5) {
        $visual = htmlentities(VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $agregador));
    }
    // MATERIAIS
    elseif($modo_visao == 6) {
        $uni = VoltaValor(MATERIAIS, 'UNIDADE', 'MID', $agregador);
        $uni = htmlentities(VoltaValor(MATERIAIS_UNIDADE, 'COD', 'MID', $uni));
        $uni = ($uni != "")? " <small>($uni)</small>" : "";
        $visual = htmlentities(VoltaValor(MATERIAIS, 'DESCRICAO', 'MID', $agregador)) . $uni;
    }

    echo "<tr class=\"$cor\">
    <td rowspan=\"$rowspan\" align=\"left\" valign=\"top\">$visual</td>
    </tr>";

    // Listando os TEMPOS
    if(($modo_visao == 1) or ($modo_visao == 2) or ($modo_visao == 3) or ($modo_visao == 4) or ($modo_visao == 5)) {
        echo "<tr class=\"cor_tp\">
        <td width=\"1\"><acronym title=\"{$ling['ord_tem_prev_hr']}\">P</acronym></td>";

        // 1 - SEGUNDA
        $dia_tmp = 1;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // 2 - TERÇA
        $dia_tmp = 2;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // 3 - QUARTA
        $dia_tmp = 3;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // 4 - QUINTA
        $dia_tmp = 4;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
               $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // 5 - SEXTA
        $dia_tmp = 5;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
               $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // 6 - SABADO
        $dia_tmp = 6;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
               $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // 7 - DOMINGO
        $dia_tmp = 7;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_pt_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_pt_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TP']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_tp[$dia_tmp] += $val[$dia_tmp]['TP'];

        // RESUMO
        echo "<td rowspan=\"$rowspan\" class=\"$cor\" style=\"border-right:0px;\">";

        // Só mostra nas visões relativas aos funcionários
        if (($modo_visao != 4) and ($modo_visao != 5)) {
            // PORCENTAGEM
            $porc = CalculaPorcetagemArray($val, 'DIS', $val, 'TP');

            echo BarraProgresso($porc, unhtmlentities($ling['ord_disp_prev']) . " $porc%", "", "", "", "", "margin-bottom: 5px;");

        }

        // PORCENTAGEM
        $porc = CalculaPorcetagemArray($val, 'TP', $val, 'TS');

        echo BarraProgresso($porc, unhtmlentities($ling['previsto_vs_executado']) . " $porc%", "goldenrod");

        echo "</td>
        </tr>";


        // DISPONIVEL
        if (($modo_visao != 4) and ($modo_visao != 5)) {
            echo "<tr class=\"cor_dis\">
            <td width=\"1\"><acronym title=\"{$ling['ord_tem_disp_hr']}\">D</acronym></td>";
            // 1 - SEGUNDA
            echo "<td>" . (($val[1]['DIS'] > 0)? "{$val[1]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[1] += $val[1]['DIS'];

            // 2 - TERÇA
            echo "<td>" . (($val[2]['DIS'] > 0)? "{$val[2]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[2] += $val[2]['DIS'];

            // 3 - QUARTA
            echo "<td>" . (($val[3]['DIS'] > 0)? "{$val[3]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[3] += $val[3]['DIS'];

            // 4 - QUINTA
            echo "<td>" . (($val[4]['DIS'] > 0)? "{$val[4]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[4] += $val[4]['DIS'];

            // 5 - SEXTA
            echo "<td>" . (($val[5]['DIS'] > 0)? "{$val[5]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[5] += $val[5]['DIS'];

            // 6 - SABADO
            echo "<td>" . (($val[6]['DIS'] > 0)? "{$val[6]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[6] += $val[6]['DIS'];

            // 7 - DOMINGO
            echo "<td>" . (($val[7]['DIS'] > 0)? "{$val[7]['DIS']}h" : '&nbsp;') . "</td>";
            $total_dis[7] += $val[7]['DIS'];

            echo "</tr>";
        }
        // EXECUTADO
        echo "<tr class=\"cor_ts\">
        <td width=\"1\"><acronym title=\"{$ling['ord_tem_serv_hr']}\">E</acronym></td>";

        // 1 - SEGUNDA
        $dia_tmp = 1;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];


        // 2 - TERÇA
        $dia_tmp++;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];

        // 3 - QUARTA
        $dia_tmp++;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];

        // 4 - QUINTA
        $dia_tmp++;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];

        // 5 - SEXTA
        $dia_tmp++;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];

        // 6 - SABADO
        $dia_tmp++;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];

        // 7 - DOMINGO
        $dia_tmp++;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['TS']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_ts_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_ts_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" title=\"{$ling['fechar']}\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['TS'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['TS'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['TS']}h</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_ts[$dia_tmp] += $val[$dia_tmp]['TS'];

        echo "</tr>";
    }

    if ($modo_visao == 6) {
        echo "<tr class=\"cor_mp\">
        <td width=\"1\"><acronym title=\"{$ling['ord_mat_prev']}\">P</acronym></td>";

        // 1 - SEGUNDA
        $dia_tmp = 1;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // 2 - TERÇA
        $dia_tmp = 2;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // 3 - QUARTA
        $dia_tmp = 3;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // 4 - QUINTA
        $dia_tmp = 4;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // 5 - SEXTA
        $dia_tmp = 5;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // 6 - SABADO
        $dia_tmp = 6;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // 7 - DOMINGO
        $dia_tmp = 7;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MP']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mp_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mp_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MP'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MP'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MP']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mp[$dia_tmp] += $val[$dia_tmp]['MP'];

        // RESUMO
        echo "<td rowspan=\"$rowspan\" class=\"$cor\" style=\"border-right:0px;\">";

        // PORCENTAGEM
        $porc = CalculaPorcetagemArray($val, 'MP', $val, 'MU');

        echo BarraProgresso($porc, "{$ling['ord_prev_rel']} $porc%");

        echo "</td>
        </tr>";

        echo "<tr class=\"cor_mu\">
        <td width=\"1\"><acronym title=\"{$ling['ord_mat_uti']}\">U</acronym></td>";

        // 1 - SEGUNDA
        $dia_tmp = 1;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        // 2 - TERÇA
        $dia_tmp = 2;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        // 3 - QUARTA
        $dia_tmp = 3;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        // 4 - QUINTA
        $dia_tmp = 4;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        // 5 - SEXTA
        $dia_tmp = 5;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        // 6 - SABADO
        $dia_tmp = 6;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        // 7 - DOMINGO
        $dia_tmp = 7;
        $js_tmp = "";
        $div_tmp = "";
        if(count($val[$dia_tmp]['OS']['MU']) > 0) {
            $js_tmp  = " onclick=\"abre_div('mostra_{$agregador}_os_mu_{$dia_tmp}');\" style=\"cursor:pointer\"";
            $div_tmp = "<div id=\"mostra_{$agregador}_os_mu_$dia_tmp\" class=\"mostra_os\" style=\"text-align:left; display:none;\">
            <div style=\"width:99%;text-align:right;cursor:pointer;\" $js_tmp>{$ling['fechar']} X&nbsp;</div><br />";

            foreach ($val[$dia_tmp]['OS']['MU'] as $osmid => $num) {
                $mid_maq = VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $osmid);
                $desc_maq = htmlentities(substr(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $mid_maq), 0, 50));
                $desc_maq = ($desc_maq == "")? $ling['def_rota'] : $desc_maq;
                $div_tmp .= "<a href=\"javascript: janela('detalha_ord.php?busca=$osmid', 'parm', 500, 400);\">" . $num . " | " . $desc_maq . "</a><br />\n";
            }

            $div_tmp .= "</div>";
        }

        echo "<td>" . (($val[$dia_tmp]['MU'] > 0)? "<span title=\"{$ling['ord_clic_ver_os']}\"$js_tmp>{$val[$dia_tmp]['MU']}</span>" : '&nbsp;') . "";
        echo $div_tmp;
        echo "</td>";
        $total_mu[$dia_tmp] += $val[$dia_tmp]['MU'];

        echo "</tr>";
    }
}

echo "</tbody>

<tfoot>
<tr class=\"cor1\">
<td align=\"left\" rowspan=\"$rowspan\" style=\"border-bottom:0px;\"><strong>{$ling['ord_total']}</strong></td>
</tr>";

// Listando os TEMPOS
if(($modo_visao == 1) or ($modo_visao == 2) or ($modo_visao == 3) or ($modo_visao == 4) or ($modo_visao == 5)) {
    echo "<tr class=\"cor_tp\">
    <td width=\"1\"><acronym title=\"{$ling['ord_tem_prev_hr']}\"><strong>P</strong></acronym></td>";
    // 1 - SEGUNDA
    echo "<td><strong>" . (($total_tp[1] > 0)? "{$total_tp[1]}h" : '&nbsp;') . "</strong></td>";
    // 2 - TERÇA
    echo "<td><strong>" . (($total_tp[2] > 0)? "{$total_tp[2]}h" : '&nbsp;') . "</strong></td>";
    // 3 - QUARTA
    echo "<td><strong>" . (($total_tp[3] > 0)? "{$total_tp[3]}h" : '&nbsp;') . "</strong></td>";
    // 4 - QUINTA
    echo "<td><strong>" . (($total_tp[4] > 0)? "{$total_tp[4]}h" : '&nbsp;') . "</strong></td>";
    // 5 - SEXTA
    echo "<td><strong>" . (($total_tp[5] > 0)? "{$total_tp[5]}h" : '&nbsp;') . "</strong></td>";
    // 6 - SABADO
    echo "<td><strong>" . (($total_tp[6] > 0)? "{$total_tp[6]}h" : '&nbsp;') . "</strong></td>";
    // 7 - DOMINGO
    echo "<td><strong>" . (($total_tp[7] > 0)? "{$total_tp[7]}h" : '&nbsp;') . "</strong></td>";
    // RESUMO
    echo "<td rowspan=\"$rowspan\" class=\"cor1\" style=\"border-bottom:0px;border-right:0px;\">";

    // PORCENTAGEM
    if (($modo_visao != 4) and ($modo_visao != 5)) {
        $porc = CalculaPorcetagemArray($total_dis, '', $total_tp, '');

        echo BarraProgresso($porc, unhtmlentities($ling['ord_disp_prev']) . " $porc%", "", "", "", "", "margin-bottom:5px;");
    }

    // PORCENTAGEM
    $porc = CalculaPorcetagemArray($total_tp, '', $total_ts, '');

    echo BarraProgresso($porc, unhtmlentities($ling['previsto_vs_executado']) . " $porc%", "goldenrod");


    echo "</td>
    </tr>";

    // DISPONIVEL
    if (($modo_visao != 4) and ($modo_visao != 5)) {
        echo "<tr class=\"cor_dis\">
        <td width=\"1\"><acronym title=\"{$ling['ord_tem_disp_hr']}\"><strong>D</strong></acronym></td>";
        // 1 - SEGUNDA
        echo "<td><strong>" . (($total_dis[1] > 0)? "{$total_dis[1]}h" : '&nbsp;') . "</strong></td>";
        // 2 - TERÇA
        echo "<td><strong>" . (($total_dis[2] > 0)? "{$total_dis[2]}h" : '&nbsp;') . "</strong></td>";
        // 3 - QUARTA
        echo "<td><strong>" . (($total_dis[3] > 0)? "{$total_dis[3]}h" : '&nbsp;') . "</strong></td>";
        // 4 - QUINTA
        echo "<td><strong>" . (($total_dis[4] > 0)? "{$total_dis[4]}h" : '&nbsp;') . "</strong></td>";
        // 5 - SEXTA
        echo "<td><strong>" . (($total_dis[5] > 0)? "{$total_dis[5]}h" : '&nbsp;') . "</strong></td>";
        // 6 - SABADO
        echo "<td><strong>" . (($total_dis[6] > 0)? "{$total_dis[6]}h" : '&nbsp;') . "</strong></td>";
        // 7 - DOMINGO
        echo "<td><strong>" . (($total_dis[7] > 0)? "{$total_dis[7]}h" : '&nbsp;') . "</strong></td>";
        echo "</tr>";
    }

    // EXECUTADO
    echo "<tr class=\"cor_ts\">
    <td width=\"1\" style=\"border-bottom:0px;\"><acronym title=\"{$ling['ord_tem_serv_hr']}\"><strong>E</strong></acronym></td>";
    // 1 - SEGUNDA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[1] > 0)? "{$total_ts[1]}h" : '&nbsp;') . "</strong></td>";
    // 2 - TERÇA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[2] > 0)? "{$total_ts[2]}h" : '&nbsp;') . "</strong></td>";
    // 3 - QUARTA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[3] > 0)? "{$total_ts[3]}h" : '&nbsp;') . "</strong></td>";
    // 4 - QUINTA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[4] > 0)? "{$total_ts[4]}h" : '&nbsp;') . "</strong></td>";
    // 5 - SEXTA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[5] > 0)? "{$total_ts[5]}h" : '&nbsp;') . "</strong></td>";
    // 6 - SABADO
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[6] > 0)? "{$total_ts[6]}h" : '&nbsp;') . "</strong></td>";
    // 7 - DOMINGO
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_ts[7] > 0)? "{$total_ts[7]}h" : '&nbsp;') . "</strong></td>";
    echo "</tr>";
}

if ($modo_visao == 6) {
    // PREVISTO
    echo "<tr class=\"cor_mp\">
    <td width=\"1\"><acronym title=\"{$ling['ord_mat_prev']}\"><strong>P</strong></acronym></td>";
    // 1 - SEGUNDA
    echo "<td><strong>" . (($total_mp[1] > 0)? "{$total_mp[1]}" : '&nbsp;') . "</strong></td>";
    // 2 - TERÇA
    echo "<td><strong>" . (($total_mp[2] > 0)? "{$total_mp[2]}" : '&nbsp;') . "</strong></td>";
    // 3 - QUARTA
    echo "<td><strong>" . (($total_mp[3] > 0)? "{$total_mp[3]}" : '&nbsp;') . "</strong></td>";
    // 4 - QUINTA
    echo "<td><strong>" . (($total_mp[4] > 0)? "{$total_mp[4]}" : '&nbsp;') . "</strong></td>";
    // 5 - SEXTA
    echo "<td><strong>" . (($total_mp[5] > 0)? "{$total_mp[5]}" : '&nbsp;') . "</strong></td>";
    // 6 - SABADO
    echo "<td><strong>" . (($total_mp[6] > 0)? "{$total_mp[6]}" : '&nbsp;') . "</strong></td>";
    // 7 - DOMINGO
    echo "<td><strong>" . (($total_mp[7] > 0)? "{$total_mp[7]}" : '&nbsp;') . "</strong></td>";
    // RESUMO
    echo "<td style=\"border-bottom:0px;border-right:0px;\" rowspan=\"$rowspan\" class=\"cor1\">";
    // PORCENTAGEM
    $porc = CalculaPorcetagemArray($total_mp, '', $total_mu, '');

    echo BarraProgresso($porc, unhtmlentities($ling['ord_prev_rel']) . " $porc%");


    echo "</td>
    </tr>";

    // EXECUTADO
    echo "<tr class=\"cor_mu\">
    <td width=\"1\" style=\"border-bottom:0px;\"><acronym title=\"{$ling['ord_mat_uti']}\"><strong>U</strong></acronym></td>";
    // 1 - SEGUNDA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[1] > 0)? "{$total_mu[1]}" : '&nbsp;') . "</strong></td>";
    // 2 - TERÇA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[2] > 0)? "{$total_mu[2]}" : '&nbsp;') . "</strong></td>";
    // 3 - QUARTA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[3] > 0)? "{$total_mu[3]}" : '&nbsp;') . "</strong></td>";
    // 4 - QUINTA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[4] > 0)? "{$total_mu[4]}" : '&nbsp;') . "</strong></td>";
    // 5 - SEXTA
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[5] > 0)? "{$total_mu[5]}" : '&nbsp;') . "</strong></td>";
    // 6 - SABADO
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[6] > 0)? "{$total_mu[6]}" : '&nbsp;') . "</strong></td>";
    // 7 - DOMINGO
    echo "<td style=\"border-bottom:0px;\"><strong>" . (($total_mu[7] > 0)? "{$total_mu[7]}" : '&nbsp;') . "</strong></td>";
    echo "</tr>";
}

echo "</tfoot>

</table>
<div id=\"lt_rodape\" style=\"text-align:left\">
<strong>{$ling['legenda']}:</strong>\n
<span class=\"cor_tp\" style=\"border:1px solid #999;\">&nbsp;&nbsp;&nbsp;&nbsp;</span> {$ling['ord_tem_prev_hr']}
<span class=\"cor_dis\" style=\"border:1px solid #999;\">&nbsp;&nbsp;&nbsp;&nbsp;</span> {$ling['ord_tem_disp_hr']}
<span class=\"cor_ts\" style=\"border:1px solid #999;\">&nbsp;&nbsp;&nbsp;&nbsp;</span> {$ling['ord_tem_serv_hr']}
<span class=\"cor_mp\" style=\"border:1px solid #999;\">&nbsp;&nbsp;&nbsp;&nbsp;</span> {$ling['ord_mat_prev']}
<span class=\"cor_mu\" style=\"border:1px solid #999;\">&nbsp;&nbsp;&nbsp;&nbsp;</span> {$ling['ord_mat_uti']}
</div>
</td></tr></table>";


?>
