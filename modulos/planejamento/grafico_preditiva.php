<?
/**
* Modulo planejamento, preditiva (ajax)
* 
* @author  Fernando Cosentino
* @version  3.0
* @package planejamento
* @subpackage preditiva
*/

$basedir = '../../';
$swfbasedir = '../../';

// Fun��es do Sistema
if (!require("{$basedir}lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("{$basedir}conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("{$basedir}lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("{$basedir}lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("{$basedir}lib/bd.php")) die ($ling['bd01']);

$ajax = $_GET['ajax'];
$ponto = (int)$_GET['ponto'];
$last = (int)$_GET['last'];

$queryself = "grafico_preditiva.php?ajax=$ajax&ponto=$ponto";

$sql = "SELECT * FROM ".LANC_PREDITIVA." WHERE MID_PONTO = '$ponto' ORDER BY DATA DESC";
$tmp=$dba[0] ->Execute($sql);
$num = count($tmp->getrows());

if ($last > ($num-10)) $last = $num-10;
if ($last < 0) $last = 0;

$linkstyle= "hover{color:white}";

echo "<html><head><link href=\"{$basedir}temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\" /></head><body>";

include("{$basedir}lib/swfcharts/charts.php");

echo "<table><tr><td>
<div align=\"center\">";
echo InsertChart("{$swfbasedir}lib/swfcharts/charts.swf", "{$swfbasedir}lib/swfcharts/charts_library","{$swfbasedir}modulos/planejamento/pred_graf.php?ponto=$ponto&last=$last", 550, 210, 'FFFFFF' );
echo "</div>";

###
// setas e links

echo "<table width=\"100%\" id=\"lt\"><tr><td align=\"left\" width=70>";
// seta esquerda
echo "<a class=\"link\" href=\"$queryself&last=".($num-10)."\"><img src=\"{$basedir}imagens/icones/22x22/seta_dupla_esquerda.png\" alt=\"\" border=0></a>";
echo " <a class=\"link\" href=\"$queryself&last=".($last+5)."\"><img src=\"{$basedir}imagens/icones/22x22/seta_esquerda.png\" alt=\"\" border=0></a>";


echo "</td><td width=\"60%\" align=\"center\">";
//excel
echo "<a class=\"link\" style=\"$linkstyle\" href=\"{$basedir}geragrafico.php?tb=PREDITIVA&ponto=$ponto&last=$last&excel=1\" target=\"_blank\"><img src=\"{$basedir}imagens/icones/22x22/graficos.png\" alt=\"\" border=0> {$ling['exportar_excel']}</a>";


echo "</td><td align=\"right\" width=70>";
// seta direita
echo "<a class=\"link\" href=\"$queryself&last=".($last-5)."\"><img src=\"{$basedir}imagens/icones/22x22/seta_direita.png\" alt=\"\" border=0></a>";
echo " <a class=\"link\" href=\"$queryself&last=0\"><img src=\"{$basedir}imagens/icones/22x22/seta_dupla_direita.png\" alt=\"\" border=0></a>";

echo "</td></tr></table>
</td>
<td>

<iframe name=\"pred_lista\" marginWidth=0 marginHeight=0 frameBorder=0 width=200 scrolling=\"auto\" height=210
		src=\"pred_graf.php?ponto=$ponto&last=$last&print=1\">
</iframe>

</td></tr></table>

</body></html>";



?>