<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino
* Nota: M�dulo Planejamento
*/

// tabela com as posi��es e m�quinas j� cadastradas no roteiro
echo "<table>
<tr>
<th width=32>".$ling['roteiro_mover']."</th>
<th width=32>".$ling['roteiro_posicao']."</th>
<th>".$tdb[MAQUINAS]['DESC']."</th></tr>";

// pega ultima posicao
$tmp=$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("SELECT POSICAO FROM ".ROTEIRO_ROTAS." WHERE MID_PLANO = '$oq' ORDER BY POSICAO DESC");
while (!$tmp->EOF) {
	$campo=$tmp->fields;
	if ($ultima_posicao < $campo['POSICAO']) {
		$ultima_posicao = $campo['POSICAO'];
	}
	$tmp->MoveNext();
}

$cor=1;
$tmp=$dba[$tdb[ROTEIRO_ROTAS]['dba']] -> Execute("SELECT * FROM ".ROTEIRO_ROTAS." WHERE MID_PLANO = '$oq' ORDER BY POSICAO ASC");
while (!$tmp->EOF) {
	$campo=$tmp->fields;
	$pos = $campo['POSICAO'];
	$poscima = $pos-1;
	$posbaixo = $pos+1;
	$maqdesc = htmlentities(VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo['MID_MAQUINA'],0));
	if ($pos == 1) {
		$seta_cima = '&nbsp;';
	}
	else {
		$seta_cima = "<a href=\"javascript:atualiza_area2('lt_tabela','parametros.php?id=roteiro&oq=$oq&posde=$pos&pospara=$poscima')\">
		<img src=\"imagens/icones/22x22/seta_cima.png\" border=\"0\" /></a>";
	}
	if ($pos == $ultima_posicao) {
		$seta_baixo = '&nbsp;';
	}
	else {
		$seta_baixo = "<a href=\"javascript:atualiza_area2('lt_tabela','parametros.php?id=roteiro&oq=$oq&posde=$pos&pospara=$posbaixo')\">
		<img src=\"imagens/icones/22x22/seta_baixo.png\" border=\"0\" /></a>";
	}
	echo "<tr class=\"cor$cor\"><td>$seta_cima<br>$seta_baixo</td><td>$pos</td><td>$maqdesc</td></tr>";
	$tmp->MoveNext();
	$cor++;
	if ($cor==3) {
		$cor=1;
	}
}
echo "</table>";
?>
