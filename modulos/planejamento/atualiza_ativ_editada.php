<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino
 * Nota: M�dulo Planejamento.
 */
// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("".$ling['arq_estrutura_nao_pode_ser_carregado']."");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("".$ling['arq_configuracao_nao_pode_ser_carregado']."");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("".$ling['arq_idioma_nao_pode_ser_carregado']."");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("../../lib/bd.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("../../lib/delcascata.php")) die ($ling['bd01']);

// Recuperando as variaveis
$ajax = (int)$_GET['ajax'];
$plano = (int)$_GET['plano'];
$tipo = (int)$_GET['tipo'];
$pag_ini = (int) $_GET['pag_ini'];


if ($ajax != 1) {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
    <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['prog_atualizacao']}</title>
    <link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
    <script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n
    <script>
    // Alerta ao fechar a pagina
    window.onbeforeunload = function(e) {
        var e = e || window.event;

        var msg = '{$ling['prog_fecha_janela']}';

        // For IE and Firefox
        if (e) {
            e.returnValue = msg;
        }

        // For Safari
        return msg;
    }
    
    // Atualizando a janela e fazendo a pagina��o
    function atualiza_prog() { 
        var pag = document.getElementById('pag').value;
        if (pag > 0) {
            atualiza_area2('corpo_atu', 'atualiza_ativ_editada.php?ajax=1&plano=$plano&tipo=$tipo&pag_tam=200&pag_ini=' + pag, 'atualiza_prog'); 
        }
        else {
            opener.location.reload();
            mostraCarregando (false);
            window.onbeforeunload = null;
        }
        
    }
    
    </script>
    </head>
    <body class=\"body_form\">
    <div id=\"corpo_atu\">";
}


if (($plano) and ($tipo)) {
    // RETIRANDO AS ATIVIDADES EDITADAS DAS OS
    //deleta_atv_programacao2($plano, $tipo);
    
    // PROGRAMA��ES PARA RODAR
    $progs = array();
    $data_inicial = array();
    $data_final = array();
    
    // BUSCANDO
    $sql="SELECT * FROM ".PROGRAMACAO." WHERE MID_PLANO = '$plano' AND TIPO = $tipo AND STATUS = '1'";
    $tmp_prog=$dba[0] ->Execute($sql);
    while (! $tmp_prog->EOF) {
        $campo_prog = $tmp_prog->fields;
        $prog_mid = (int)$campo_prog['MID'];
        
        // SALVANDO
        $progs[] = $prog_mid;
        $data_final[] = $campo_prog['DATA_FINAL'];
        
        $tmp6 = $dba[0] -> Execute("SELECT MID,DATA_PROG FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO = '$prog_mid' AND STATUS = 1 ORDER BY DATA_PROG ASC");
        $cao = $tmp6->fields;
        
        if ($cao['DATA_PROG'] == ""){
            $data_inicial[] = $campo_prog['DATA_INICIAL'];
        }
        else {
            $data_inicial[] = $cao['DATA_PROG'];
        }

        $tmp_prog->MoveNext();
    }

    if (count($progs) > 0) {
        echo "<h3>".$ling['ATUALIZANDO_PROG_']."</h3><br />";
        
        // Rodando atualiza��o paginada
        $pag_tam = ($pag_ini == 0)? 1 : (int)$_GET['pag_tam'];
        $pag = grava_programacao_data($data_inicial, $data_final, 0, $tipo, $plano, $progs, 1, $pag_tam, $pag_ini, false, false, true);

            
        
        // BUSCANDO
        $sql="SELECT * FROM ".PROGRAMACAO." WHERE MID_PLANO = '$plano' AND TIPO = $tipo AND STATUS = '1'";
        $tmp_prog=$dba[0] ->Execute($sql);
        while (! $tmp_prog->EOF) {
            $campo_prog = $tmp_prog->fields;

            $data_final = $campo_prog['DATA_FINAL'];
            $prog_mid = (int)$campo_prog['MID'];
            
            $tmp6 = $dba[0] -> Execute("SELECT MID,DATA_PROG FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO = '$prog_mid' AND STATUS = 1 ORDER BY DATA_PROG ASC");
            $cao = $tmp6->fields;
            
            if ($cao['DATA_PROG'] == ""){
                $data_inicial = $campo_prog['DATA_INICIAL'];
            }
            else {
                $data_inicial = $cao['DATA_PROG'];
            } 

            echo "<table id=\"lt_tabela\" class=\"tabela\">
            <tr><td align=\"left\">\n";

            if ($tipo == 1) {
                echo "<strong>".$ling['PLANO_M'].":</strong> ".htmlentities(VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$plano,0))." <br />
                <strong>".$ling['OBJ_MANUTENCAO'].":</strong> ".htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo_prog['MID_MAQUINA'],0))." <br />
                <strong>".$ling['DATA_INICIO_M'].":</strong> ".NossaData($campo_prog['DATA_INICIAL'])." <br />
                <strong>".$ling['data_ultima_os'].":</strong> ".NossaData($data_inicial)." <br />
                <strong>".$ling['DATA_FINAL_M'].":</strong> ".NossaData($data_final) . "<br />";
            }
            if ($tipo == 2) {
                echo "<strong>".$ling['ROTA_M'].":</strong> ".htmlentities(VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$plano,0))." <br>
                <strong>{$ling['data_ultima_os']}:</strong> " . NossaData($data_inicial) . " <br />
                <strong>{$ling['DATA_FINAL_M']}:</strong> ".NossaData($data_final) . "<br />";
            }
            
            // MOSTRANDO AS ORDENS
            // NOVAS
            if (count($_SESSION[ManuSess]['OS_NEW'][$prog_mid]) > 0) {
                echo "<br />
                <strong>".$ling['ordens_criadas'].":</strong> " .  count($_SESSION[ManuSess]['OS_NEW'][$prog_mid]);
                echo " <a href=\"javascript: abre_div('os_new_$prog_mid')\" style=\"text-decoration:underline;\">{$ling['prog_ver_ordens']}</a>\n";
                echo "<span id=\"os_new_$prog_mid\" style=\"display:none;padding:0px;text-align:left;\">" . implode(', ', $_SESSION[ManuSess]['OS_NEW'][$prog_mid]) . "</span>";
            }
            // ALTERADAS
            if (count($_SESSION[ManuSess]['OS_ALT'][$prog_mid]) > 0) {
                echo "<br />
                <strong>".$ling['ordens_alteradas'].":</strong> " .  count($_SESSION[ManuSess]['OS_ALT'][$prog_mid]);
                echo " <a href=\"javascript: abre_div('os_alt_$prog_mid')\" style=\"text-decoration:underline;\">{$ling['prog_ver_ordens']}</a>\n";
                echo " <span id=\"os_alt_$prog_mid\" style=\"display:none;padding:0px;text-align:left;\">" . implode(', ', $_SESSION[ManuSess]['OS_ALT'][$prog_mid]) . "</span>";
            }
            // REMOVIDAS
            if (count($_SESSION[ManuSess]['OS_DEL'][$prog_mid]) > 0) {
                echo "<br />
                <strong>".$ling['ordens_removidas'].":</strong> " .  count($_SESSION[ManuSess]['OS_DEL'][$prog_mid]);
                echo " <a href=\"javascript: abre_div('os_del_$prog_mid')\" style=\"text-decoration:underline;\">{$ling['prog_ver_ordens']}</a>\n";
                echo " <span id=\"os_del_$prog_mid\" style=\"display:none;padding:0px;text-align:left;\">" . implode(', ', $_SESSION[ManuSess]['OS_DEL'][$prog_mid]) . "</span>";
            }

            echo "</td></tr>
            </table>
            <br clear=\"all\">\n";

            $tmp_prog->MoveNext();
        }
        
        // CONTINUA NA PR�XIMA P�GINA
        if($pag !== TRUE) {
            echo "<h3>".$ling['aguarde_em_processamento']."</h3>
            <input type=\"hidden\" id=\"perm_fecha\" value=\"0\" />
            <input type=\"hidden\" id=\"pag\" value=\"$pag\" />
            <script>atualiza_prog();</script>\n";
            exit();
        }
        // ATUALIZA��O TERMINADA
        else {
            //MUDANDO O STATUS DAS ATIVIDADES
            if($tipo == 1) {
                $dba_atv = $tdb[ATIVIDADES]['dba'];
                $sql = "UPDATE " . ATIVIDADES . " SET STATUS = 0 WHERE MID_PLANO_PADRAO = $plano";
            }
            elseif($tipo == 2) {
                $dba_atv = $tdb[LINK_ROTAS]['dba'];
                $sql = "UPDATE " . LINK_ROTAS . " SET STATUS = 0 WHERE MID_PLANO = $plano";
            }
            
            if(! $dba[$dba_atv]->Execute($sql)) {
                erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$dba_atv] -> ErrorMsg() . "<br />" . $sql);
            }
            
            // ZERANDO
            $_SESSION[ManuSess]['OS_DEL'] = array();
            $_SESSION[ManuSess]['OS_ALT'] = array();
            $_SESSION[ManuSess]['OS_NEW'] = array();
            
            echo "<h3>".$ling['PROG_ATUALI_COM_SUCESSO']."</h3>
            <br /> <center><a href=\"javascript: window.self.close();\">{$ling['prog_fecha_janela2']}</a></center>
            <input type=\"hidden\" id=\"perm_fecha\" value=\"1\" />\n
            <input type=\"hidden\" id=\"pag\" value=\"0\" />";
        }
    }
}
else {
    erromsg($ling['prog_plano_nao_encontrado']);
}

if ($ajax != 1) {
    echo "</div>
    </body>
    </html>";
}

?>
