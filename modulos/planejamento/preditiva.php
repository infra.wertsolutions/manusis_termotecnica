<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino
* Nota: M�dulo Planejamento, preditiva
* 
* exe = 1: mostrando SELECT de maquinas e equipamentos para o usuario escolher
* 	- n�o possui entradas previas
* 
* exe = 2: mostrando a tela principal, isto �, lista de pontos, iframe com grafico, apontamento
* 	- um OU outro:
* 		GET['maq'] = MID_MAQUINA
* 		GET['equip'] = MID_EQUIPAMENTO
* 
*/

$phpajax = 'modulos/planejamento/preditiva_ajax.php';

$maq = (int)$_GET['maq'];
$equip = (int)$_GET['equip'];

if (($exe == 1) or ($exe == "")) {
    /**
     * Mostra lista de maquinas/equipamentos para o usu�rio escolher.
     * "onchange" atualiza tela com os pontos daquela maquina/equipamento
    */
    echo "<div id=\"mod_menu\">
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=1\">
    <img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" alt=\"".$ling['preditiva']."\" />
    <span>".$ling['preditiva']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=3\">
    <img src=\"imagens/icones/22x22/preditiva_aponta.png\" border=\"0\" alt=\"".$ling['preditiva_aponta']."\" />
    <span>".$ling['preditiva_aponta']."</span>
    </a>
    </div>
    
    <div>
        <h3>".$ling['preditiva']."</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div id=\"formularioos\">";

    echo "<br />
    <fieldset>
    <legend>".$ling['pontos_preditiva']."</legend>
    <label class=\"campo_label\" for=\"maq\">{$tdb[MAQUINAS]['DESC']}</label>";

    $sql = "SELECT MID_MAQUINA FROM ".PONTOS_PREDITIVA." WHERE MID_MAQUINA != '0' GROUP BY MID_MAQUINA";
    $tmp=$dba[0] ->Execute($sql);
    $sqlcond="MID = '-1'";
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        AddStr($sqlcond,' OR ',"MID = '{$campo['MID_MAQUINA']}'");
        $tmp->MoveNext();
    }
    
    // Filtros
    if ($sqlcond) {
        $sqlcond = "WHERE ($sqlcond)";
    }

    FormSelectD('COD', 'DESCRICAO', MAQUINAS, $_GET['maq'], 'maq', 'maq', 'MID', '', '', '', $sqlcond);

    echo "<input type=\"button\" class=\"botao\" value=\"Ver\" id=\"ver_pontos\" name=\"ver_pontos\" onclick=\"if(document.getElementById('maq').value != 0){location.href='manusis.php?id=$id&op=$op&exe=2&maq=' + document.getElementById('maq').value}\" />
    <br clear=\"all\" />";

}
if (($exe == 2) and (($maq) or ($equip))) {
    /**
     * J� escolheu uma maquina ou equipamento
     * Mostra:
     * - lista de pontos de preditiva (monitoramento) como links para um ajax
     * - no div do ajax:
     * .	- iframe com o grafico daquele ponto, selecionado automaticamente os 10 ultimos
     * .	- painel de apontamento para este ponto, que atualiza o div inteiro
    */
    if ($maq)   $title = VoltaValor(MAQUINAS,'DESCRICAO','MID',$maq,0);

    echo "<div id=\"mod_menu\">
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=1\">
    <img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" alt=\"".$ling['voltar']."\" />
    <span>".$ling['voltar']."</span>
    </a>
    </div>
    
    <div>
    <a href=\"manusis.php?id=$id&op=$op&exe=2&maq=$maq&equip=$equip\">
    <img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" alt=\"".$ling['pontos_preditiva']."\" />
    <span>".$ling['pontos_preditiva']."</span>
    </a>
    </div>
    
    <div>
        <h3>".$ling['pontos_preditiva'].":<br />$title</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";

    $sql = "SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_MAQUINA = '$maq'";

    $tmp=$dba[0] ->Execute($sql);
    $i=0;
    $np=0;
    echo "<table width=\"100%\"><tr><td valign=\"top\">";
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        $i++;
        if ($i > 3) {
            echo "</td><td valign=\"top\">";
            $i = 1;
        }
        if (!$np) $np = $campo['MID'];

        echo "<a class=\"link\" href=\"javascript:atualiza_area2('dponto','$phpajax?ponto={$campo['MID']}')\">
        <img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" alt=\"".$campo['PONTO']."\" />
        {$campo['PONTO']} - {$campo['GRANDEZA']}</a><br />";

        $tmp->MoveNext();
    }
    
    echo "</td></tr></table>
    <hr noshade=\"noshade\" color=\"#99BBDD\" />
    <div id=\"dponto\"></div>";
    
    if ($np) echo "<script>atualiza_area2('dponto','$phpajax?ponto=$np')</script>";

}
if (($exe == 3) or ($exe == 4)) {
    include("modulos/planejamento/preditiva_aponta.php");
}

?>