<?
/**
* Modulo planejamento, preditiva (ajax)
* 
* @author  Fernando Cosentino
* @version  3.0
* @package planejamento
* @subpackage preditiva
*/

// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("../../lib/bd.php")) die ($ling['bd01']);

$ajax = $_GET['ajax'];
$data = $_GET['data'];
$hora = $_GET['hora'];
$valor = str_replace(',','.',$_GET['valor']);
$ponto = (int)$_GET['ponto'];
$phpajax = 'modulos/planejamento/preditiva_ajax.php';

if (($ajax == 'add') and ($data) and ($ponto)) {
	// adiciona
	$datasql = DataSQL($data);//." $hora";
	$nmid = (int)VoltaValor(LANC_PREDITIVA,'MID',"MID_PONTO = '$ponto' AND DATA",$datasql,0);
	if (!$nmid){
		$nmid = GeraMid(LANC_PREDITIVA,'MID',0);
		$sql = "INSERT INTO ".LANC_PREDITIVA." (MID_PONTO,DATA,VALOR,MID) VALUES ('$ponto','$datasql $hora','$valor','$nmid')";
	}
	else {
		$sql = "UPDATE ".LANC_PREDITIVA." SET VALOR = '$valor' WHERE MID = '$nmid'";
	}
	//die("$sql");
	$dba[0] ->Execute($sql);
}

if (!$data) {
	$data = date("d/m/Y");
}
if (!$hora) {
	$hora = date("h:i:s");
}

echo "<center>
<iframe name=\"ponto_preditiva\" marginWidth=\"0\" marginHeight=\"0\" frameBorder=\"0\" width=\"770\" scrolling=\"auto\" height=\"260\" src=\"modulos/planejamento/grafico_preditiva.php?ponto=$ponto\">
</iframe></center>
<hr noshade=\"noshade\" color=\"#99BBDD\" />
<form method=\"GET\" action=\"\">
{$ling['novo_lanc_preditiva']}:
<table id=\"lt_tabela\"><tr><td width=160>";
FormData($tdb[LANC_PREDITIVA]['DATA'],'data',$data,'campo_label','','campo_text_ob');
echo "</td><td width=\"120\">
<label for=\"hora\" class=\"campo_label\">Hora</label>
<input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"hora\" class=\"campo_text_ob\" name=\"hora\" size=\"8\" maxlength=\"8\"  value=\"$hora\"/>
</td><td width=200>
<label for=\"valor\" class=\"campo_label\">{$ling['valor']}</label>
<input type=\"text\" class=\"campo_text_ob\" name=\"valor\" id=\"valor\" value=\"0\" />
</td><td>
<input type=\"button\" class=\"botao\" value=\"Salvar\" onclick=\"atualiza_area2('dponto','$phpajax?ponto=$ponto&data='+document.getElementById('data').value+'&hora='+document.getElementById('hora').value+'&valor='+document.getElementById('valor').value+'&ajax=add')\" />
</td></tr></table>
</form>";

//document.getElementById(id)




?>