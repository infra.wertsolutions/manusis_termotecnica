<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino
* Nota: M�dulo Cadastro.
*/
$phpself = 'modulos/planejamento/programacao_ajax.php';
// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("../../lib/bd.php")) die ($ling['bd01']);



$ajax = $_GET['ajax'];
$tipoprog = (int)$_GET['tipo'];

if ($ajax == 'setor') {
    $area = (int)$_GET['area'];
    //echo "MID_AREA = '$area'";
    if ($area) $filform = "WHERE MID_AREA = '$area'";
    else $filform = '';
    $primeirocampo = $tdb[SETORES]['DESC'];
//  FormSelectAjax('setor',SETORES,$_GET['setor'],'DESCRICAO','MID',0,'',$filform,'','');
    FormSelectD('COD','DESCRICAO',SETORES,$_REQUEST['setor'],'setor','setor','MID','','','',$filform,'A','COD',$tdb[SETORES]['DESC']);
}
if ($ajax == 'rotamaq') {
    $prog = (int)$_GET['prog'];
    $plano = VoltaValor(PROGRAMACAO,'MID_PLANO','MID',$prog,0);
    
    $sql = "SELECT DISTINCT MID_MAQUINA FROM ".LINK_ROTAS." WHERE MID_PLANO = '$plano'";
    $resultado=$dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql);
    $echovar = "";
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $maqdesc = htmlentities(strtoupper(VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo['MID_MAQUINA'],0)));
        if (!$echovar) $echovar = "<a href=\"javascript:atualiza_area2('prog$prog','$phpself?ajax=rotafecha&prog=$prog')\"><img src=\"imagens/icones/menos.gif\" border=0> $maqdesc</a>";
        else AddStr($echovar,"<br clear=\"all\" />","&nbsp;  &nbsp;".$maqdesc);
        $resultado->MoveNext();
    }
    if (!$echovar) $echovar = "<i>{$ling['nenhum']}</i>";
    echo $echovar;
}
if ($ajax == 'rotafecha') {
    $prog = (int)$_GET['prog'];
    $plano = VoltaValor(PROGRAMACAO,'MID_PLANO','MID',$prog,0);
    $sql = "SELECT DISTINCT MID_MAQUINA FROM ".LINK_ROTAS." WHERE MID_PLANO = '$plano' LIMIT 1";
    $resultado=$dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql);
    if (!$resultado->EOF) {
        $campo=$resultado->fields;
        $maqdesc = htmlentities(strtoupper(VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo['MID_MAQUINA'],0)));
        $echovar = "<a href=\"javascript:atualiza_area2('prog$prog','$phpself?ajax=rotamaq&prog=$prog')\"><img src=\"imagens/icones/mais.gif\" border=0> $maqdesc</a>";
    }
    if (!$echovar) $echovar = "<i>{$ling['nenhum']}</i>";
    echo $echovar;
}
if ($ajax == 'cron') {
    $cron = (int)$_GET['cron'];
    $ano = (int)$_GET['ano'];
    $mksem = 7*(24*(60*60));
    $fimdoano = mktime(23,59,59,12,31,$ano);
    $st = array();
    for ($tmp = mktime(12,0,0,1,1,$ano); $tmp < $fimdoano; $tmp+=$mksem) {
        $esem = date('W',$tmp);
        // se mes � janeiro e semana eh alta, adiciona tra�o
        if ((date('n',$tmp) == 1) and ($esem > 30)) $esem .= '-';
        // se mes � dezembro e semana eh baixa, adiciona tra�o
        if ((date('n',$tmp) == 12) and ($esem < 3)) $esem .= '-';
        $st[] = $esem;
    }
    // $st contem lista de semanas, as altas de janeiro (52 etc) contem um tra�o (52-)

    $sems=array(); // ordens nas semanas, formato $sems[semana] = numero de ordens
    $sql = "SELECT * FROM ".ORDEM." WHERE MID_PROGRAMACAO = '$cron' AND DATA_PROG >= '$ano-01-01' AND DATA_PROG <= '$ano-12-31'";
    $resultado=$dba[$tdb[ORDEM]['dba']] -> Execute($sql);
    while (!$resultado->EOF) {
        // PARA CADA ORDEM DESTA PROGRAMA��O
        $campo=$resultado->fields;
        list($eano, $emes, $edia) = explode('-',$campo['DATA_PROG'],3);
        $emktime = mktime(0,0,0,$emes,$edia,$eano);
        $esem = date('W',$emktime);
        if ((date('n',$emktime) == 1) and ($esem > 30)) $esem .= '-'; // semana alta de janeiro
        if ((date('n',$emktime) == 12) and ($esem < 3)) $esem .= '-'; // semana baixa de dezembro
        if (!$sems[$esem]) $sems[$esem] = $campo['STATUS']; // em branco, coloca o status
        if (($sems[$esem] == 1) and ($campo['STATUS'] == 2)) $sems[$esem] = 1; // ja tem aberta, mas tem fechada, coloca aberta
        $resultado->MoveNext();
    }
    
    // mostra tabela com lista das semanas
    $last_esem = 0; // error-free: for�a n�o repetir semana
    foreach ($st as $esem) if ($esem != $last_esem) {
        $last_esem = $esem;
        if (!$sems[$esem]) $esemimg = "";
        if ($sems[$esem] == 1) $esemimg = "<img src=\"imagens/icones/graydot.jpg\" border=\"0\" width=\"9\" />";
        if ($sems[$esem] == 2) $esemimg = "<img src=\"imagens/icones/blackdot.jpg\" border=\"0\" width=\"9\" />";
        $linha[0] .= "<td style=\"font-size:8px;\">".substr($esem,0,2)."|</td>";
        $linha[1] .= "<td>$esemimg</td>";
    }
    $linha[0] .= "<td>$ano</td>";
    $linha[1] .= "<td><a href=\"javascript:atualiza_area2('cron$cron','$phpself?ajax=cronfecha')\">
    &nbsp; <img src=\"imagens/icones/del.gif\" border=\"0\" /> {$ling['fechar']}</a></td>";
    echo "<table style=\"border: 1px solid #999999\" border=\"0\" cellpadding=\"0\"><tr>{$linha[0]}</tr><tr>{$linha[1]}</tr></table>";
}
if ($ajax == 'cronfecha') {
    echo "<img src=\"imagens/icones/whitespace.gif\" width=\"1\" height=\"1\" />";
}


function FormSelectAjax($cc,$tb,$sel,$c1,$c2,$conexao,$formulario,$cond,$atualiza,$atualizacampos,$mostra_primeiro_campo = TRUE, $class='campo_select') {
    global $dba,$id,$tdb,$oq,$exe,$op,$form,$recb_valor,$primeirocampo,$phpself;
    if ($cond) $cnd = "WHERE $cond";
    if ($atualiza) $atual = " onchange=\"atualiza_area2('$atualiza','$phpself?$atualizacampos' + this.options[this.selectedIndex].value)\"";
    echo "<select class=\"$class\" name=\"$cc\" id=\"$cc\" $atual>";
    if ($mostra_primeiro_campo) echo "<option value=\"\">$primeirocampo</option>\n";
    $resultado=$dba[$conexao] -> Execute("SELECT * FROM $tb $cnd ORDER BY $c1 ASC");
    if(!$resultado) {
        echo "<br /><hr />".erromsg($dba[$conexao] -> ErrorMsg())."<hr /><br />";
    }
    else {
        $campo = $resultado -> fields;
        while (!$resultado->EOF) {
            $campo = $resultado -> fields;
            $sc2=$campo[$c2];
            $sc1=htmlentities($campo[$c1]);
            if ($sel == $sc2) echo "<option value=\"$sc2\" selected=\"selected\">$sc1</option>\n";
            else echo "<option value=\"$sc2\">$sc1</option>\n";
            $resultado->MoveNext();
            $i++;
        }
        echo "</select>\n<br clear=\"all\" />";
    }
}
?>
