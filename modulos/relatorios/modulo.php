<?
/** 
* Modulo de Relatorios
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
*/ 

$exe=(int)$_GET['exe'];
$op=(int)$_GET['op'];
if ($op == 0) $op=1;
if ($op == 1) {
    echo "<br /><table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"menu_caixa_estrutura\">
<tr>";
    echo '<td align="left" width="33%">'."

<a class=\"link\" href=\"relatorios/relatorio_va.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/estrutural.png\" border=\"0\" /> ".$ling['relatorio_estrutural']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".AREAS."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_local1']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".SETORES."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_local2']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MAQUINAS."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/fam_maquinas.png\" border=\"0\" /> ".$ling['relatorio_obj_man']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MAQUINAS_FAMILIA."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/fam_maquinas.png\" border=\"0\" /> ".$ling['relatorio_fam_maq']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MAQUINAS_CLASSE."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/cla_maquinas.png\" border=\"0\" />Relat�rio de Classe de Objeto de Manuten&ccedil;&atilde;o</a><br />

<a class=\"link\" href=\"relatorios/relatorio_album.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio_anexo.png\" border=\"0\" /> ".$ling['relatorio_album_img']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MOV_MAQUINA."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/local3_mov.png\" border=\"0\" /> ".$ling['mov_obj_man']."</a><br />

".'</td><td align="left" width="33%">'."
<a class=\"link\" href=\"relatorio.php?tb=".CENTRO_DE_CUSTO."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/cifra.png\" border=\"0\" /> ".$ling['relatorio_ccusto']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".EQUIPAMENTOS."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/fam_equip.png\" border=\"0\" /> ".$ling['relatorio_equip']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".EQUIPAMENTOS_FAMILIA."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/fam_equip.png\" border=\"0\" /> ".$ling['relatorio_fam_equip']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MOV_EQUIPAMENTO."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/equip_mov.png\" border=\"0\" /> ".$ling['mov_equip']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MATERIAIS_FAMILIA."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/fam_materiais.png\" border=\"0\" /> ".$ling['relatorio_fam_mat']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MATERIAIS_UNIDADE."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/unidades.png\" border=\"0\" /> ".$ling['relatorio_unidade']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".MAQUINAS_CONTADOR_TIPO."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_tipos_contadores']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".FERIADOS."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/feriados.png\" border=\"0\" /> ".$ling['relatorio_feriados']."</a>

".'</td><td align="left" width="33%">'."

<a class=\"link\" href=\"relatorio.php?tb=".ESPECIALIDADES."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_especialidades']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".FABRICANTES."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_fabricantes']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".FORNECEDORES."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_fornecedores']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".TIPOS_SERVICOS."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_tipo_serv']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".NATUREZA_SERVICOS."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_natur_serv']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".CAUSA."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_causa']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".DEFEITO."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_defeito']."</a><br />

<a class=\"link\" href=\"relatorio.php?tb=".SOLUCAO."&con=0\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> ".$ling['relatorio_solucao']."</a><br />

</td></tr></table><br />";

}

// MATERIAIS
elseif ($op == 2) {
    echo "<br /><table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"menu_caixa_estrutura\">
<tr>".'<td align="left" width="33%">'."
<a class=\"link\" href=\"relatorios/relatorio_pontocompra.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> Ponto de Compra para Materiais</a><br />
<a class=\"link\" href=\"relatorios/relatorio_pecas2.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> Materiais por Localiza��o </a><br />
<a class=\"link\" href=\"relatorios/relatorio_materiais.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/doc.png\" border=\"0\" /> Localiza��o de Materiais </a><br />

".'</td><td align="left" width="33%">'."

".'</td><td align="left" width="33%">'."

</td></tr></table><br />";
}

// PLANEJAMENTO
elseif ($op == 3) {
    echo "<br /><table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"menu_caixa_estrutura\">
<tr>";
    echo '<td align="left" width="33%">'."

<a class=\"link\" href=\"relatorios/relatorio_planos.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/inspecao.png\" border=\"0\" /> Plano padr&atilde;o</a><br />

<a class=\"link\" href=\"relatorios/relatorio_rotas.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/rota.png\" border=\"0\" /> Rotas</a><br />

<a class=\"link\" href=\"relatorios/relatorio_pred.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" /> Monitoramento</a><br />

<a class=\"link\" href=\"relatorios/relatorio_contador.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio_contador.png\" border=\"0\" /> Contadores</a><br />

".'</td><td align="left" width="33%">'."

<a class=\"link\" href=\"relatorios/relatorio_geralplan.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio.png\" border=\"0\" /> Relat&oacute;rio de Programa&ccedil;&atilde;o</a><br />

<a class=\"link\" href=\"relatorios/relcronograma.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio.png\" border=\"0\" /> Cronograma Preventivo Anual</a><br />

<a class=\"link\" href=\"relatorios/relcronogramarotas.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio.png\" border=\"0\" /> Cronograma de Rotas Anual</a><br />

".'</td><td align="left" width="33%">'."

</td></tr></table><br />";

}

elseif ($op == 4) {
    echo "<br /><table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"menu_caixa_estrutura\">
<tr>";
    echo '<td align="left" width="33%">'."

<a class=\"link\" href=\"relatorios/relatorio_geralos.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio.png\" border=\"0\" /> Relat&oacute;rio Geral de O.S.</a><br />
<a class=\"link\" href=\"relatorios/relatorio_funcionario.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio.png\" border=\"0\" /> Cronograma Di&aacute;rio dos Funcion&aacute;rios </a><br />
<a class=\"link\" href=\"relatorios/relatorio_carteira.php\" target=\"_blank\">
<img src=\"imagens/icones/22x22/relatorio.png\" border=\"0\" /> Relat&oacute;rio Carteira de Servi�o</a><br />


".'</td><td align="left" width="33%">'."

".'</td><td align="left" width="33%">'."

</td></tr></table><br />";


}

elseif ($op == 5) {

    echo "<br /><table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" align=\"center\" class=\"menu_caixa_estrutura\">
<tr>";
    echo '<td align="left">';
    echo "\n<div id=\"lt_tabela\">";
    echo "<fieldset><legend>Gr�ficos Gerados Pelo Assistente</legend>";
    $dir=$manusis['dir']['graficos'];
    if (!is_dir($manusis['dir']['graficos'])) {
        erromsg("".$ling['direto_nao_foi_conf']."");
    }
    else {
        $od = opendir($dir);
        $i=0;
        while (false !== ($arq = readdir($od))) {
            $dd=explode(".",$arq);
            if (($dd[1] == "xml")) {
                $xml = new DOMDocument();
                $xml -> load("$dir/$arq");
                $titulo=$xml -> getElementsByTagName('descricao');
                $titulo=$titulo -> item(0) -> nodeValue;
                echo "
<img src=\"imagens/icones/22x22/graficos.png\" border=\"0\" hspace=\"5\" alt=\" \" align=\"middle\" \>
<a href=\"modulos/relatorios/geragraficoxml.php?arq=$arq\" target=\"_blank\">".utf8_decode($titulo)."</a>
<br clear=\"all\" />";

            }
        }
        closedir($od);
    }
    echo "
</fieldset>

</div>
</td>


</td></tr></table><br />";


}

elseif ($op == 6) {
    if (VoltaPermissao($id, $op) == 1) include("modulos/relatorios/assistentegrafico.php");
    else {
        echo "<br />";
        erromsg("".$ling['ussuario_sem_permissao_graf']."");
    }
}

elseif ($op == 7) {
    if (!include("modulos/relatorios/graficos_ger.php")) erromsg($ling['err01']);
}

else echo "<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />";
?>
