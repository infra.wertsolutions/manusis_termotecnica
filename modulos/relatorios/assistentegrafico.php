<?
/** 
* Corpo de visualiza��o dos gr�ficos e suas propriedades
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version 0.1 
* @package relatorios
* @subpackage assistentegrafico
*/ 
if (!require("modulos/relatorios/funcoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");



echo "<script>
function abre_ag_arq(id_man, op_man){
    document.getElementById('div_formulario').style.height='400px'
    document.getElementById('div_formulario').style.width='620px'
    document.getElementById('div_formulario').style.visibility=\"visible\"
    atualiza_area('div_formulario','modulos/relatorios/parametros.php?id_man=' + id_man + '&op_man=' + op_man + '&op=1')
    return;
}
function abre_ag_tabela(id,arq){
    document.getElementById('div_formulario').style.height='400px'
    document.getElementById('div_formulario').style.width='620px'
    document.getElementById('div_formulario').style.visibility=\"visible\"
    atualiza_area('div_formulario','modulos/relatorios/parametros.php?op=2&arq=' + arq + '&tb=' + id)
    return;
}
function abre_ag_filtros(id,arq){
    document.getElementById('div_formulario').style.height='400px'
    document.getElementById('div_formulario').style.width='620px'
    document.getElementById('div_formulario').style.visibility=\"visible\"
    atualiza_area('div_formulario','modulos/relatorios/parametros.php?op=4&arq=' + arq + '&tb=' + id)
    return;
}
function abre_ag_gerencia(id,arq){
    document.getElementById('div_formulario').style.height='400px'
    document.getElementById('div_formulario').style.width='620px'
    document.getElementById('div_formulario').style.visibility=\"visible\"
    atualiza_area('div_formulario','modulos/relatorios/parametros.php?op=8&arq=' + arq + '&tb=' + id + '&id=5')
    return;
}
</script>";

echo "<div id=\"mod_menu\">
    <div>
<a href=\"javascript:abre_ag_arq($id, $op)\">
<img src=\"imagens/icones/22x22/anexar.png\" width=\"22\" height=\"22\" border=\"0\" alt=\"Abrir / Criar novo Gr�fico\" />
<span>Abrir / Criar novo Gr�fico</span>
</a>
</div>


<div>";
echo "<h3>Assistente de Gr�ficos</h3>
</div>

</div>
<br clear=\"all\" />
";
$arq=LimpaTexto($_GET['arq']);
$arq=str_replace("..","",$arq);
$arq=str_replace("/","",$arq); // Retiro os ../ para que nao role de um fdp por /etc/passwd achando que estamos na decada de 80.

$dir=$manusis['dir']['graficos'];
if ($arq != "") {
    $xml = new DOMDocument();
    $xml -> load("$dir/$arq");

    $titulo=$xml -> getElementsByTagName('descricao');
    $titulo=utf8_decode($titulo -> item(0) -> nodeValue);
    $arq_envia=urlencode($arq);
    
    $ger = explode("_", $arq);

    if (($ger[0] == "ge")) {
        $ger = 1;
    }
    else {
        $ger = 0;
    }

    echo "<br clear=\"all\" />
    <div id=\"lt\">
    <div id=\"lt_cab\"><h3>Configura&ccedil;&otilde;es do Gr&aacute;fico: <u>$titulo</u></h3></div>
    <br clear=\"all\" />
    <div id=\"lt_forms\">
    <fieldset style=\"float:none\"><legend>Adicionar tabela de dados no gr&aacute;fico</legend>
    <label for=\"adicionar_tabela\">Titulo: </label>
    <input class=\"campo_text\" type=\"text\" id=\"adicionar_tabela\" name=\"adicionar_tabela\" value=\" \" size=\"25\" maxlength=\"75\" />
    <label for=\"adicionar_tipo\">Tipo: </label>
    <select class=\"campo_select\" id=\"adicionar_tipo\" name=\"adicionar_tipo\">
    <option value=\"1\">Evolu��o Anual</option>
    <option value=\"2\">Per&iacute;odo</option>
    </select> 
    <input class=\"botao\" type=\"submit\" name=\"adicionar_submit\" value=\"Ok\" onclick=\"atualiza_area('ag_tabelas','modulos/relatorios/parametros.php?op=3&arq=$arq_envia&add=1&titulo=' + document.getElementById('adicionar_tabela').value + '&tipo=' + document.getElementById('adicionar_tipo').options[document.getElementById('adicionar_tipo').selectedIndex].value)\" />
    </fieldset>
    </div>
    <br clear=\"all\">
    <form action=\"modulos/relatorios/vis.php?arq=$arq_envia\" method=\"POST\" target=\"vis_frame\" >
    <div  id=\"ag_tabelas\">
    <table id=\"lt_tabela\" width=\"100%\" cellspacing=\"1\" style=\"border:1px solid #ccc;padding:0px;\">
    <tr>
    <th>Tabela</th>
    <th>Par&acirc;metros</th>
    <th>Op&ccedil;&otilde;es</th>
    </tr>";

    $params = $xml->getElementsByTagName('tabela');
    $i=0; // Cursor dos NODES
    foreach ($params as $param) {
        $tbtipo=$param -> getAttribute('tipo');
        $tbid=$param -> getAttribute('id');
        $tbtitulo=$param->getElementsByTagName('titulo');
        $tbtitulo=utf8_decode($tbtitulo -> item(0) -> nodeValue);


        echo "<tr class=\"cor1\">
        <td>$tbtitulo</td>
        <td>";
        if ($tbtipo == 1){
            echo "<label for=\"$tbid\">Ano:</label><input type=\"text\" size=\"4\" maxlength=\"4\" name=\"ano_$tbid\" id=\"ano_$tbid\" class=\"campo_text\" />";
        }
        elseif ($tbtipo == 2) {
            FormData("Per�odo de:","datai_$tbid","","campo_label");
            FormData("a","dataf_$tbid","","campo_label");
        }
        echo "<select name=\"tdata_$tbid\" class=\"campo_select\">
        <option value=\"DATA_INICIO\">Data Inicio</option>
        <option value=\"DATA_FINAL\">Data Final</option>
        <option value=\"DATA_PROG\">Data Programado/Abertura</option>
        <option value=\"DATA_APONTA\">Data Apontamento</option>
        </select>
        </td>
        <td>
        <a href=\"javascript:abre_ag_tabela($tbid,'$arq_envia')\"> <img src=\"imagens/icones/22x22/editar.png\" border=0 align=\"middle\" alt=\"Editar \" title=\" Editar Tabela \" /> </a> 
        <a href=\"javascript:abre_ag_filtros($tbid,'$arq_envia')\"> <img src=\"imagens/icones/22x22/visualizar.png\" border=0 align=\"middle\" alt=\"Filtros \" title=\" Filtros \" /> </a>"; 
        
        if ($ger == 1) {
            echo "<a href=\"javascript:abre_ag_gerencia($tbid,'$arq_envia')\"> <img src=\"imagens/icones/22x22/checklist.png\" border=0 align=\"middle\" alt=\"Definir colunas \" title=\"Definir colunas \" /> </a>";
        }
        
        echo "<a href=\"javascript:atualiza_area('ag_tabelas','modulos/relatorios/parametros.php?op=3&arq=$arq_envia&del=1&oq=$i')\"> <img src=\"imagens/icones/22x22/del.png\" border=0 align=\"middle\" alt=\"Excluir\" title=\"Excluir\" />
        </td>
        </tr>";
        $i++;
    }
    echo "</table></div><br />";
    echo "<div id=\"barra\">
<input class=\"botao\" type=\"submit\" name=\"pre\" value=\"Previsualizar\" />
<input class=\"botao\" type=\"submit\" name=\"exp\" value=\"Exportar para MS Excel\" />
<br />
<br />
</div>
</form>";
    echo "
    <div id=\"vis\" align=\"center\">
    <iframe name=\"vis_frame\" src=\"modulos/relatorios/vis.php\" width=\"100%\" height=\"450\" frameborder=\"0\" scrolling=auto style=\"border:1px solid black\"> </iframe>
    </div>";
    unset($xml);
}
else {
    echo "<br /><strong>Nenhum gr�fico selecionado</strong><br /><br />";
}
echo "</div>";
?>
