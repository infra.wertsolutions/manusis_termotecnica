<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../../temas/".$manusis['tema']."/estilo.css")) {
	$manusis['tema']="padrao";
}

// Variaveis de direcionamento
$arq=urldecode($_GET['arq']);
$arq=LimpaTexto($_GET['arq']);
$arq=str_replace("..","",$arq);
$arq=str_replace("/","",$arq);
$dir="../../".$manusis['dir']['graficos'];


echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"Manusis Padr�o\" />
<script type=\"text/javascript\" src=\"../../lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body>";
if ($arq != "") {
	$xml = new DOMDocument();

	$xml -> load("$dir/$arq");
	// Remove tabela
	if ($_GET['del'] == 1){
		AG_DelTabela($xml,$_GET['oq']);
	}
	// Adiciona Tabela
	if ((int)$_GET['add'] == 1){
		$add_titulo=LimpaTexto($_GET['titulo']);
		$add_tipo=(int)$_GET['tipo'];
		AG_AddTabela($xml,$add_titulo,$add_tipo);
	}
	$titulo=$xml -> getElementsByTagName('descricao');
	$titulo=utf8_decode($titulo -> item(0) -> nodeValue);
}
else {
	erromsg('Par�metros Inv�lidos');
	exit;
}

echo "
<div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>Gr�fico: $titulo
</div>
<div id=\"corpo_relatorio\">

<form action=\"vis.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">";



$arq_envia=urlencode($arq);
echo "<table id=\"lt_tabela\" width=\"100%\">
		<tr>
		<th>Tabela</th>
		<th>Par&acirc;metros</th>
		</tr>";
$params = $xml->getElementsByTagName('tabela');
$i=0;
foreach ($params as $param) {
	$tbtipo=$param -> getAttribute('tipo');
	$tbid=$param -> getAttribute('id');
	$tbtitulo=$param->getElementsByTagName('titulo');
	$tbtitulo=htmlentities(utf8_decode($tbtitulo -> item(0) -> nodeValue));


	echo "<tr class=\"cor1\">
		<td>$tbtitulo </td>
		<td>";
	if ($tbtipo == 1){
		echo "<label for=\"$tbid\">Ano:</label><input type=\"text\" size=\"4\" maxlength=\"4\" name=\"ano_$tbid\" id=\"ano_$tbid\" class=\"campo_text\" />";
	}
	elseif ($tbtipo == 2) {
		FormData("Data Inicio:","datai_$tbid","","campo_label");
		echo "<br clear=all />";
		FormData("Data Final","dataf_$tbid","","campo_label");
	}
	echo "<br clear=all />";
	echo "<select name=\"tdata_$tbid\" class=\"campo_select\">
		<option value=\"DATA_INICIO\">Data Inicio</option>
		<option value=\"DATA_FINAL\">Data Final</option>
		<option value=\"DATA_PROG\">Data Programado/Abertura</option>
		</select>";
		echo "</td></tr>";
	$i++;
}

echo "</table>";
unset($xml);

echo "
<input type=\"hidden\" name=\"arq\" value=\"$arq\" />
<input class=\"botao\" type=\"submit\" name=\"exp\" value=\"Exportar para MS Excel\" />
<input class=\"botao\" type=\"submit\" name=\"print\" value=\"Imprimir dados no Navegador\" />
</form><br />
</div>
</div>
</body>
</html>";


?>