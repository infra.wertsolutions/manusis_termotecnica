<?
/**
 * Possui as tabelas para administração dos usuarios,
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @version  3.0
 * @package manusis
 * @subpackage  principal
 */


if (($exe == "") or ($exe == 1) or ($exe == 2) or ($exe == 3) or ($exe == 4) or ($exe == 44) or ($exe == 6) or ($exe == 7) or ($exe == 8)){
	$listaemp=ListaMatriz($tdb[EMPRESAS]['dba']);
	$matrix=$listaemp[0]['nome'];
	$matrix_mid=$listaemp[0]['mid'];
	$matrix_cod=$listaemp[0]['cod'];
	echo "<div id=\"mod_menu\">
    <div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/usuarios.png\" border=\"0\" alt=\"".$ling['usuarios']."\" />
<span>".$tdb[USUARIOS]['DESC']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=2\">
<img src=\"imagens/icones/22x22/grupos.png\" border=\"0\" alt=\"".$ling['usuarios_grupos']."\" />
<span>".$tdb[USUARIOS_GRUPOS]['DESC']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/permissao.png\" border=\"0\" alt=\"".$ling['permissao']."\" />
<span>".$tdb[USUARIOS_PERMISSAO]['DESC']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=4\">
<img src=\"imagens/icones/22x22/fechar.png\" border=\"0\" alt=\"".$ling['permissao_sol']."\" />
<span>".$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=44\">
<img src=\"imagens/icones/22x22/cifra.png\" border=\"0\" alt=\"".$ling['permissao_cc']."\" />
<span>".$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC']."</span>
</a>
</div>

<div>";


}
if (($exe == "") or ($exe == 1)) {
	echo "<h3>".$tdb[USUARIOS]['DESC']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	$campos[0]="NOME";
	$campos[1]="EMAIL";
	$campos[2]="CELULAR";
	$campos[3]="USUARIO";
	$campos[4]="GRUPO";
	$campos[5]="MID_FUNCIONARIO";
	ListaTabela(USUARIOS,"MID",$campos,"USUARIO","","",1,1,1,1,1,1,1,1,1,1,"");
	echo "</div>";
}
elseif ($exe == 2) {
	echo "<h3>".$tdb[USUARIOS_GRUPOS]['DESC']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	$campos[0]="DESCRICAO";
	ListaTabela(USUARIOS_GRUPOS,"MID",$campos,"USUARIO_GRUPO","","",1,1,1,1,1,1,1,1,1,1,"");
	echo "</div>";
}

elseif ($exe == 3) {
	echo "<h3>".$tdb[USUARIOS_PERMISSAO]['DESC']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	
	echo "<div id=\"lt\">
	     <div id=\"lt_cab\"><h3>{$tdb[USUARIOS_PERMISSAO]['DESC']}</h3></div>
	</div>
	<br />";

	echo "<form id=\"formularioos\" action=\"\" method=\"post\">";

	echo "<fieldset>
	<legend>{$tdb[USUARIOS_GRUPOS]['DESC']}</legend>";
	
	echo "<label for=\"MID_GRUPO\" class=\"campo_label\">{$ling['selecione_grupo']}:</label>";
	FormSelectD('DESCRICAO', null, USUARIOS_GRUPOS, $_POST['MID_GRUPO'], 'MID_GRUPO','MID_GRUPO', 'MID', null);


	echo "<input type=\"submit\" name=\"ver\" value=\"Ver\" class=\"botao\">";

	echo "</form>
	</fieldset>
	<br />";

	if(!empty($_POST['MID_GRUPO']))
	{
		$sql = 'SELECT MID, DESCRICAO FROM ' . USUARIOS_PERMISSAO_TIPO. ' ORDER BY MID ASC';
		 

		$result = $dba[$tdb[USUARIOS_MODULOS]['dba']] -> Execute($sql);

		if(!$result->EOF){

			$dataUsuariosPermissaoTipo = $result->getRows();

			$sql = "SELECT MODULO, PERMISSAO FROM " . USUARIOS_PERMISSAO . " WHERE GRUPO = " . $_POST['MID_GRUPO'];
				
			$result = $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute($sql);

			$dataUsuariosPermissao = array();
			if(!$result->EOF)
			{
				While(!$result->EOF)
				{
					$campo = $result->fields;
					$dataUsuariosPermissao[$campo['MODULO']][] = $campo['PERMISSAO'];
					$result->moveNext();
				}
			}

			$sql = 'SELECT ID, DESCRICAO FROM '. USUARIOS_MODULOS .' WHERE OP = 0 ORDER BY ID ASC';

			$result = $dba[$tdb[USUARIOS_MODULOS]['dba']] -> Execute($sql);

			if(!$result->EOF)
			{
				$dataUsuariosModulos = $result->getRows();
				
				$radioButtunsPermissao = '';
				
				foreach($dataUsuariosPermissaoTipo as $UsuariosPermissaoTipo){
					
					$radioButtunsPermissao .= "&nbsp;&nbsp;&nbsp;<input id=\"per-{$UsuariosPermissaoTipo['MID']}-{M:MID_MODULO}\" type=\"radio\" name=\"permissao-{M:MID_MODULO}-\" onclick=\"checaSubModulos({M:MID_MODULO}, {$UsuariosPermissaoTipo['MID']}, {$_POST['MID_GRUPO']})\"><label for=\"per-{$UsuariosPermissaoTipo['MID']}-{M:MID_MODULO}\">{$UsuariosPermissaoTipo['DESCRICAO']}</label>";							
				}
				 
				foreach($dataUsuariosModulos as $modulo)
				{
					if($modulo['ID'] == 0 OR $modulo['ID'] == 6)continue;
					
					$radioButtunsPermissaoTmp  = str_replace('{M:MID_MODULO}',$modulo['ID'], $radioButtunsPermissao );

					echo "<fieldset id=\"modulo-{$modulo['ID']}\">";
					echo "<legend>&nbsp;". $modulo['DESCRICAO'] ."   {$radioButtunsPermissaoTmp}</legend>";

					$sql = "SELECT MID, DESCRICAO FROM ". USUARIOS_MODULOS . " WHERE OP <> 0 AND ID = {$modulo['ID']} ORDER BY OP ASC";

					$result = $dba[$tdb[USUARIOS_MODULOS]['dba']] -> Execute($sql);

					if(!$result->EOF)
					{
						$dataUsuariosSubModulos = $result->getRows();

						foreach($dataUsuariosSubModulos as $subModulo)
						{
							echo "<fieldset class=\"submodulo\">";
							echo "<legend>". $subModulo['DESCRICAO'] ."</legend>";

							foreach($dataUsuariosPermissaoTipo as $UsuariosPermissaoTipo){
								 
								$checked = '';
								if(count($dataUsuariosPermissao[$subModulo['MID']])){
									if(in_array($UsuariosPermissaoTipo['MID'], $dataUsuariosPermissao[$subModulo['MID']] ))
									{
										$checked = "checked='checked'";
									}
								}								
								elseif($UsuariosPermissaoTipo['MID'] == 0)
								{
									$checked = "checked='checked'";
								}
								echo "<input $checked 	id=\"permissao-{$modulo['ID']}-{$UsuariosPermissaoTipo['MID']}-{$subModulo['MID']}\" name=\"permissao-{$subModulo['MID']}\" type=\"radio\" onclick=\"ajax_get('parametros.php?id=modulos_permissao&mid_grupo={$_POST['MID_GRUPO']}&mid_permissao={$UsuariosPermissaoTipo['MID']}&mid_modulo={$subModulo['MID']}')\">";
								 
								echo "<label for=\"permissao-{$modulo['ID']}-{$UsuariosPermissaoTipo['MID']}-{$subModulo['MID']}\" >{$UsuariosPermissaoTipo['DESCRICAO']}</label><br />";
							}
							echo "</fieldset>";
						}
					}
					echo "</fieldset><br />";
				}				 
			}
		}		 
	}	 
	echo "</div>";
}

elseif ($exe == 4) {
	echo "<h3>".$tdb[USUARIOS_PERMISAO_SOLICITACAO]['DESC']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	$campos[0]="USUARIO";
	$campos[1]="MID_EMPRESA";
	ListaTabela(USUARIOS_PERMISAO_SOLICITACAO,"MID",$campos,"USUARIO_SOL","","",1,1,1,1,1,1,1,1,1,1,"");
	echo "</div>";
}
elseif ($exe == 44) {
	echo "<h3>".$tdb[USUARIOS_PERMISAO_CENTRODECUSTO]['DESC']."</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	$campos[0]="USUARIO";
	$campos[1]="CENTRO_DE_CUSTO";
	ListaTabela(USUARIOS_PERMISAO_CENTRODECUSTO,"MID",$campos,"USUARIO_CC","","",1,1,1,1,1,1,1,1,1,1,"");
	echo "</div>";
}

elseif ($exe == 7) {
	echo "<h3>{$ling['log_integrar']}</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	$campos[0]="TIPO";
	$campos[1]="ESQUEMA_ARQUIVO";
	$campos[2]="ULTIMO_EVENTO";
	$campos[3]="RESULTADO";
	$campos[4]="OBS";
	ListaTabela(INTEGRADOR,"MID",$campos,'',"","",1,1,1,1,1,1,1,1,1,1,"");
	echo "</div>";
}

elseif ($exe == 8) {
	echo "<h3>{$ling['agen_integracao']}</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
	include('integracao_agenda.php');
	echo "</div>";
}

?>
