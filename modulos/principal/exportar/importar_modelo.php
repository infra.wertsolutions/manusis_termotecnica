<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/

// verifica se est� em windows ou linux
if (strpos(realpath('./'),'/')) $os_barra='/'; else $os_barra='\\';

$php_ajax = "importar_ajax.php";
$php_self = "importar_modelo.php";
$php_path = "C:\\Arquivos de programas\\Zend\\Core\\bin\\php.exe";
$php_exportar = realpath("./importar_dados.php");
//$_SERVER["SCRIPT_FILENAME"]

// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../../../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../../../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../../../conf/manusis.mod.php")) die ($ling['mod01']);

include('importar_conf.php');

$modelos_dir = "../../../{$manusis['dir']['imodelos']}";
$exportar_url = "{$manusis['url']}{$manusis['dir']['exportar']}";
$url_exportar = "{$manusis['url']}exportar";

function LimpaArquivo($arq, $combarra=1) {
    //if (strpos($arq,':') !== false) $arq = substr($arq,strrpos($arq,':')+1);
    if ($combarra) {
        if (strpos($arq,'./') !== false) $arq = substr($arq,strrpos($arq,'./')+2);
        while (strpos($arq,'//') !== false) $arq = str_replace('//','/',$arq);
        while ($arq[0] == '/') $arq = substr($arq,1);
    } else {
        if (strpos($arq,'/') !== false) $arq = substr($arq,strrpos($arq,'/')+1);
    }
    if (strpos($arq,'?') !== false) $arq = substr($arq,0,strrpos($arq,'?'));
    return $arq;
}

$tabelas_permitidas = array(
    AREAS,
    SETORES,
    MAQUINAS,
    MAQUINAS_CONJUNTO,
    EQUIPAMENTOS,
    MATERIAIS,
    ORDEM_PLANEJADO,
    CENTRO_DE_CUSTO,
    LANCA_CONTADOR
);

// Caso n�o exista um padr�o definido
if (!file_exists("../../../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$arq_xml=LimpaTexto($_GET['m']);
$c_arq_xml=LimpaTexto($_GET['c']);
$d_arq_xml=LimpaTexto($_GET['d']);
$salvar = $_POST['salvar'];
$importar = $_POST['importar'];
$enviar = $_GET['enviar'];

if ($enviar) {
    $parq = $_FILES['arq'];
    $parq_parts = pathinfo($parq['name']);
    if ($parq_parts['extension'] == 'xml') {
        $arq_xml = $parq_parts['basename'];
        //echo "move_uploaded_file({$parq['tmp_name']},".realpath("{$modelos_dir}/{$arq_xml}").")";
        move_uploaded_file($parq['tmp_name'],"{$modelos_dir}/{$arq_xml}");
        die("<script>location.href='{$php_self}?m={$arq_xml}'</script>");
    }
}

if (($d_arq_xml) and (substr($d_arq_xml,-4) == '.xml') and (file_exists("{$modelos_dir}/{$c_arq_xml}"))) {
    unlink("{$modelos_dir}/{$d_arq_xml}");
    die("<script>location.href='{$php_self}'</script>");
}
if ($c_arq_xml) {
    $tb = $_GET['tb'];
    file_put_contents("{$modelos_dir}/{$c_arq_xml}.xml","<modelo>\n<tabela>$tb</tabela>\n<arquivo>{$c_arq_xml}.xml</arquivo>\n</modelo>");
    die("<script>location.href='{$php_self}?m=".urlencode($c_arq_xml).".xml'</script>");
}

// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
$msg='';
if ($salvar or $importar){
    $cc=$_POST['cc'];
    $ler_unidade=$_POST['ler_unidade'];
    $sinc=$_POST['sinc'];
    $sinc_chave=$_POST['sinc_chave'];
    $unidade=$_POST['unidade'];
    
    // FTP
    $ler_ftp=$_POST['ler_ftp'];
    $ler_ftp_host=$_POST['ler_ftp_host'];
    $ler_ftp_local=$_POST['ler_ftp_local'];
    $ler_ftp_user=$_POST['ler_ftp_user'];
    $ler_ftp_pass=$_POST['ler_ftp_pass'];
    
    // BD
    $ler_bd       = $_POST['ler_bd'];
    $ler_bd_tipo  = $_POST['ler_bd_tipo'];
    $ler_bd_host  = $_POST['ler_bd_host'];
    $ler_bd_porta = $_POST['ler_bd_porta'];
    $ler_bd_base  = $_POST['ler_bd_base'];
    $ler_bd_sid   = $_POST['ler_bd_sid'];
    $ler_bd_user  = $_POST['ler_bd_user'];
    $ler_bd_pass  = $_POST['ler_bd_pass'];
    $ler_bd_tb    = $_POST['ler_bd_tb'];
    $ler_bd_filtro = $_POST['ler_bd_filtro'];
    
    $ignorar_titulos=$_POST['ignorar_titulos'];
    $auto_freq = $_POST['auto_freq'];
    $auto_peri = $_POST['auto_peri'];
    $auto_datai = $_POST['auto_datai'];
    $auto_horai = $_POST['auto_horai'];

    $modelo_xml = $_SESSION['ManuSess']['importar_modelo_xml'];
    if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
    $modelo = new SimpleXMLElement($modelo_xml);
    $tb = (string)$modelo->tabela;
    //file_put_contents("{$modelos_dir}/{$arq_xml}",$modelo->asXML());  
    if (!$tb) errofatal($ling['err14']);
    
    unset($modelo->campo);
    $tmp=$dba[0] -> Execute("SHOW FIELDS FROM $tb");
    while (!$tmp->EOF) {
        $campo=$tmp->fields;
        $ecampo = $campo['Field'];
        if ($cc[$ecampo]['use']) {
            $ec = $modelo->addChild('campo');
            $ec->addAttribute('nome',(string)$ecampo);
            if ($cc[$ecampo]['rel']) $ec->addAttribute('relacao',(string)$cc[$ecampo]['rel']);
            if ($cc[$ecampo]['rtb']) $ec->addAttribute('tabela',(string)$cc[$ecampo]['rtb']);
            if ($cc[$ecampo]['rmid']) $ec->addAttribute('alvo',(string)$cc[$ecampo]['rmid']);
            if ($cc[$ecampo]['dfmt']) $ec->addAttribute('data',(string)$cc[$ecampo]['dfmt']);
            if ($cc[$ecampo]['sep']) $ec->addAttribute('separador',(string)$cc[$ecampo]['sep']);
            if ($cc[$ecampo]['tam']) $ec->addAttribute('tamanho',(string)$cc[$ecampo]['tam']);
            
            // Mudando para funcionar para banco de dados
            if (! $ler_bd) {
                $ec[0] = (int)$cc[$ecampo]['campo'];
            }
            else {
                $ec[0] = $cc[$ecampo]['campo'];
            }
        }
        $tmp->MoveNext();
    }

    if ($sinc) $modelo->sincronizar = $sinc_chave; else unset($modelo->sincronizar);
    $modelo->ignorar_titulos = $ignorar_titulos;
    $modelo->ler_unidade = $ler_unidade;
    $modelo->unidade = LimpaArquivo($unidade,0);
    
    // FTP
    $modelo->ler_ftp = $ler_ftp;
    $modelo->ler_ftp_host = $ler_ftp_host;
    $modelo->ler_ftp_local = $ler_ftp_local;
    $modelo->ler_ftp_user = $ler_ftp_user;
    $modelo->ler_ftp_pass = base64_encode($ler_ftp_pass);
    
    // BD
    $modelo->ler_bd = $ler_bd;
    $modelo->ler_bd_tipo = $ler_bd_tipo;
    $modelo->ler_bd_host = $ler_bd_host;
    $modelo->ler_bd_porta = $ler_bd_porta;
    $modelo->ler_bd_base = $ler_bd_base;
    $modelo->ler_bd_sid = $ler_bd_sid;
    $modelo->ler_bd_user = $ler_bd_user;
    $modelo->ler_bd_pass = base64_encode($ler_bd_pass);
    $modelo->ler_bd_tb   = $ler_bd_tb;
    $modelo->ler_bd_filtro = $ler_bd_filtro;
    
    $modelo->auto_frequencia = $auto_freq;
    $modelo->auto_periodicidade = $auto_peri;
    $modelo->auto_datainicio = $auto_datai;
    $modelo->auto_horainicio = $auto_horai;
    
    file_put_contents("{$modelos_dir}/{$arq_xml}",$modelo->asXML());    

    if ($importar) {
        // executa script que gera os dados
        $ares=array();
 
        //file_put_contents("{$modelos_dir}/{$arq_xml}.txt",'0');
        execInBackground("php \"$php_exportar\" m={$arq_xml}");
        
        $msg = "<iframe src=\"{$php_ajax}?id=progresso&m={$arq_xml}\" width=\"100%\" height=\"250\"></iframe>";
    }

}

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"Manusis Padr�o\" />
<script type=\"text/javascript\" src=\"../../../lib/javascript.js\"> </script>
<script>
function AlternaDicasForm() {
    var div = document.getElementById('dica_format');
    if (div.style.display == 'none') {
        div.style.display = 'block';
    }
    else {
        div.style.display = 'none';
    }
}
function MostraSinc(obj) {
    var div = document.getElementById('dsinc');
    if (obj.selectedIndex == 0) {
        div.style.display = 'none';
    }
    else {
        div.style.display = '';
    }
}
</script>
";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../../../lib/movediv.js\"> </script>\n";
echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['impo_dados']} ".($arq_xml? " - ($arq_xml)" : '')."</h1>
</div>

<div id=\"corpo_relatorio\">

<table width=\"100%\"><tr><td width=\"*\" valign=\"top\">

";

if ($importar) {
    echo "$msg";
}
else {
    $modelo_xml = file_get_contents("{$modelos_dir}/{$arq_xml}");
    if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
    $_SESSION['ManuSess']['importar_modelo_xml'] = $modelo_xml;
    $modelo = new SimpleXMLElement($modelo_xml);
    $tb = (string)$modelo->tabela;
    if ($tb) {
        
        foreach ($modelo->campo as $evalor) {
            $eitem=array(
                'use' => '1',
                'nome' => (string)$evalor['nome'],
                'rel' => (string)$evalor['relacao'],
                'rtb' => (string)$evalor['tabela'],
                'rmid' => (string)$evalor['alvo'],
                'dfmt' => (string)$evalor['data'],
                'sep' => (string)$evalor['separador'],
                'tam' => (string)$evalor['tamanho'],
                'campo' => (string)$evalor[0]
            );
            $tbcampos[(string)$evalor['nome']] = $eitem;
        }
        
        $campos_da_tabela=array();
        $sql="SELECT * FROM $tb";
        if (!$resultado= $dba[$tdb[$tb]['dba']] -> SelectLimit($sql,1)){
            $err = $dba[$tdb[$tb]['dba']] -> ErrorMsg();
            erromsg("$err<br /><br />$sql");
            exit;
        }
        else {
            $ncampos=$resultado -> FieldCount();
            for ($fc=0; $fc < $ncampos; $fc++) {
                $campos_da_tabela[]=$resultado -> FetchField($fc);
            }
        }


        echo "<form action=\"{$php_self}?m=".urlencode($arq_xml)."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"POST\">
<fieldset><legend>{$ling['modelo']}</legend>

    <label for=\"tb\">{$ling['tabela']}</label>
    <span id=\"tb\">{$tdb[$tb]['DESC']}</span>
    <br clear=\"all\" />
    
    <fieldset>
    <legend>{$ling['impo_importacao']}</legend>
        <label for=\"sinc\" class=\"campo_label\">{$ling['operacoes']}</label>
        <select class=\"campo_select\" name=\"sinc\" id=\"sinc\" onchange=\"MostraSinc(this)\">
            <option value=\"0\">{$ling['impo_importar_subs']}</option>   
            <option value=\"1\" ".((string)$modelo->sincronizar?"selected=\"selected\"":'').">{$ling['impo_soncronizar']}</option>  
        </select>
        <br clear=\"all\" />
        
        <div id=\"dsinc\" ".((string)$modelo->sincronizar?'':"style=\"display: none\"").">
            <label for=\"sinc_chave\" class=\"campo_label\">{$ling['campo_chave']}</label>
            <select class=\"campo_select\" name=\"sinc_chave\" id=\"sinc_chave\">";
            foreach ($campos_da_tabela as $ecampo) if ($ecampo->name) echo "
                <option value=\"".$ecampo->name."\" ".(((string)$modelo->sincronizar == (string)$ecampo->name)?"selected=\"selected\"":'').">".($tdb[$tb][$ecampo->name]?$tdb[$tb][$ecampo->name]:$ecampo->name)."</option>";
        echo "
            </select>
        </div>
    </fieldset>

    <fieldset>
    <legend>{$ling['expo_campos']}</legend>
        <table width=\"600\" id=\"lt_tabela\" cellpadding=1>
            <tr>
                <th>{$ling['campo_tab']}</th>
                <th></th>
                <th>{$ling['impo_campo_csv']}</th>
                <th>{$ling['opcoes']}</th>
            </tr>";
        foreach ($campos_da_tabela as $campo) {
            if ($tdb[$tb][$campo -> name] != "") {
                $enome = (string)$campo -> name;
                $etipo = $campo->type;
                $etam = $campo->max_length;
                $ecampo = (array)$tbcampos[$enome];
                $rtb = VoltaRelacao($tb,$enome);
                echo "<tr class=\"cor1\">
            <td>".$tdb[$tb][$enome]."</td>
            <td><input class=\"campo_check\" type=\"checkbox\" ".($ecampo['use']?"checked=\"checked\"":'')." name=\"cc[$enome][use]\" id=\"cc[$enome][use]\" value=\"1\" /></td>
            <td><input type=\"text\" name=\"cc[$enome][campo]\" id=\"cc[$enome][campo]\" class=\"campo_text\" size=\"20\" value=\"{$ecampo['campo']}\"></td>
            <td>";
                if ($etipo == 'date') {
                    echo "Formato: <input type=\"text\" class=\"campo_text\" name=\"cc[$enome][dfmt]\" value=\"".($ecampo['dfmt']?(string)$ecampo['dfmt']:'%Y-%m-%d')."\"> ({$ling['uso_separadores']})";
                }
                if ($etipo == 'real') {
                    echo "{$ling['separador_decimais']}: <input type=\"text\" size=2 class=\"campo_text\" name=\"cc[$enome][sep]\" value=\"".($ecampo['sep']?(string)$ecampo['sep']:',')."\">";
                }
                if ($etipo == 'string') {
                    echo "<input type=\"hidden\" name=\"cc[$enome][tam]\" value=\"$etam\">";
                }
                if ($rtb and $importar_modelo[$rtb['tb']]) {
                    echo "
                {$ling['relacao']}
                <select name=\"cc[$enome][rel]\" id=\"cc[$enome][rel]\" class=\"campo_select\">
                    <option value=\"\">{$ling['nenhuma']}</option>";
                    foreach ($importar_modelo[$rtb['tb']] as $ecampo2) echo "
                    <option value=\"{$ecampo2}\" ".(($ecampo['rel'] == $ecampo2)?"selected=\"selected\"":'').">{$tdb[$rtb['tb']][$ecampo2]}</option>";
                    echo "
                </select>
                <input type=\"hidden\" name=\"cc[$enome][rtb]\" value=\"{$rtb['tb']}\" />
                <input type=\"hidden\" name=\"cc[$enome][rmid]\" value=\"{$rtb['mid']}\" />";
                }
                echo "</td>
        </tr>";
            }
        }
        echo "
        </table>
    </fieldset>
                
    <fieldset><legend>{$ling['origem_dados']}</legend>

        <label for=\"ignorar_titulos\" class=\"campo_label\">{$ling['impo_ig_linha']}</label>
        <input type=\"checkbox\" name=\"ignorar_titulos\" value=\"1\" id=\"ignorar_titulos\" ".(((int)$modelo->ignorar_titulos) ? "checked=\"checked\"" : '').">
        <br clear=\"all\" />

        <label for=\"ler_unidade\" class=\"campo_label\">{$ling['arq_disco_loca']}</label>
        <input type=\"checkbox\" name=\"ler_unidade\" value=\"1\" id=\"ler_unidade\" ".(((int)$modelo->ler_unidade) ? "checked=\"checked\"" : '').">
        <input type=\"text\" name=\"unidade\" id=\"unidade\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->unidade) ? (string)$modelo->unidade : realpath("../../../".$manusis['dir']['exportar'])."{$os_barra}$tb{$os_barra}arquivo.csv" )."\">
        <a onclick=\"AlternaDicasForm()\" style=\"cursor: pointer\">{$ling['dicas_formatacao']}</a>
        <br clear=\"all\" />
    
        <label for=\"ler_ftp\" class=\"campo_label\">{$ling['impo_baixar_serv_ftp']}</label>
        <input type=\"checkbox\" name=\"ler_ftp\" value=\"1\" id=\"ler_ftp\" ".(((int)$modelo->ler_ftp) ? "checked=\"checked\"" : '').">
        <table style=\"border: 1px solid silver\"><tr><td>
            <label for=\"ler_ftp_host\" class=\"campo_label\">{$ling['expo_host_ftp']}</label>
            <input type=\"text\" name=\"ler_ftp_host\" id=\"ler_ftp_host\" size=\"50\" class=\"campo_text\" value=\"".((string)$modelo->ler_ftp_host)."\">
            <br clear=\"all\" />
    
            <label for=\"ler_ftp_local\" class=\"campo_label\">{$ling['arq_diretorio']}</label>
            <input type=\"text\" name=\"ler_ftp_local\" id=\"ler_ftp_local\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->ler_ftp_local) ? (string)$modelo->ler_ftp_local : '/arquivo.csv' )."\">
            <a onclick=\"AlternaDicasForm()\" style=\"cursor: pointer\">{$ling['dicas_formatacao']}</a>
            <br clear=\"all\" />
    
            <label for=\"ler_ftp_user\" class=\"campo_label\">{$ling['sol_usuario']}</label>
            <input type=\"text\" name=\"ler_ftp_user\" id=\"ler_ftp_user\" size=\"18\" class=\"campo_text\" value=\"".(string)$modelo->ler_ftp_user."\">
            <label class=\"campo_label\" for=\"ler_ftp_pass\" style=\"float:none\">{$ling['senha']}</label>
            <input type=\"password\" name=\"ler_ftp_pass\" id=\"ler_ftp_pass\" size=\"18\" class=\"campo_text\" value=\"".base64_decode((string)$modelo->ler_ftp_pass)."\">
        </td></tr></table>
        
        <label for=\"ler_bd\" class=\"campo_label\">{$ling['impo_baixar_bd']}</label>
        <input type=\"checkbox\" name=\"ler_bd\" value=\"1\" id=\"ler_bd\" ".(((int)$modelo->ler_bd) ? "checked=\"checked\"" : '').">
        <table style=\"border: 1px solid silver\"><tr><td>
            <label for=\"ler_bd_tipo\" class=\"campo_label\">{$ling['banco']}</label>
            <select class=\"campo_select\" name=\"ler_bd_tipo\" id=\"ler_bd_tipo\">
                <option value=\"\"></option>
                <option value=\"1\" ".(((int)$modelo->ler_bd_tipo == 1) ? "selected=\"selected\"" : '').">{$ling['bd03']}</option>
                <option value=\"2\" ".(((int)$modelo->ler_bd_tipo == 2) ? "selected=\"selected\"" : '').">{$ling['bd04']}</option>
                <option value=\"3\" ".(((int)$modelo->ler_bd_tipo == 3) ? "selected=\"selected\"" : '').">{$ling['bd05']}</option>
            </select>
            <br clear=\"all\" />
        
            <label for=\"ler_bd_host\" class=\"campo_label\">{$ling['impo_host_ip']}</label>
            <input type=\"text\" name=\"ler_bd_host\" id=\"ler_bd_host\" size=\"50\" class=\"campo_text\" value=\"".((string)$modelo->ler_bd_host)."\">
            : <input type=\"text\" name=\"ler_bd_porta\" id=\"ler_bd_porta\" size=\"4\" class=\"campo_text\" value=\"".((string)$modelo->ler_bd_porta)."\">
            <br clear=\"all\" />
    
            <label for=\"ler_bd_base\" class=\"campo_label\">{$ling['base']}</label>
            <input type=\"text\" name=\"ler_bd_base\" id=\"ler_bd_base\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->ler_bd_base) ? (string)$modelo->ler_bd_base : '' )."\">
            <br clear=\"all\" />
            
            <label for=\"ler_bd_sid\" class=\"campo_label\">{$ling['impo_sid_oracle']}</label>
            <input type=\"text\" name=\"ler_bd_sid\" id=\"ler_bd_sid\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->ler_bd_sid) ? (string)$modelo->ler_bd_sid : '' )."\">
            <br clear=\"all\" />
    
            <label for=\"ler_bd_user\" class=\"campo_label\">{$ling['sol_usuario']}</label>
            <input type=\"text\" name=\"ler_bd_user\" id=\"ler_bd_user\" size=\"18\" class=\"campo_text\" value=\"".(string)$modelo->ler_bd_user."\">
            <label class=\"campo_label\" for=\"ler_bd_pass\" style=\"float:none\">{$ling['senha']}</label>
            <input type=\"password\" name=\"ler_bd_pass\" id=\"ler_bd_pass\" size=\"18\" class=\"campo_text\" value=\"".base64_decode((string)$modelo->ler_bd_pass)."\">
            <br clear=\"all\" />
            
            <label for=\"ler_bd_tb\" class=\"campo_label\">{$ling['tabela']}</label>
            <input type=\"text\" name=\"ler_bd_tb\" id=\"ler_bd_tb\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->ler_bd_tb) ? (string)$modelo->ler_bd_tb : '' )."\">
            <br clear=\"all\" />
            
            <label for=\"ler_bd_filtro\" class=\"campo_label\">{$ling['impo_filtro_where']}</label>
            <input type=\"text\" name=\"ler_bd_filtro\" id=\"ler_bd_filtro\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->ler_bd_filtro) ? (string)$modelo->ler_bd_filtro : '' )."\">
            
            
        </td></tr></table>
    
        <div id=\"dica_format\" style=\"display: none\">
        <br clear=\"all\" />
        {$ling['expo_texto']}:<br>
        <strong>%d</strong> - {$ling['expo_dia_mes']} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <strong>%m</strong> - {$ling['expo_mes']} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <strong>%a</strong> - {$ling['ano']} (ex: 2009) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <strong>%h</strong> - {$ling['hora2']} (ex: 14:25:37) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <strong>%s</strong> - {$ling['expo_semana_ano']} (ex: 47) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <strong>%ds</strong> - {$ling['expo_dia_sem_ex']}
        </div>
    </fieldset>

    <fieldset><legend>Agenda</legend>
        <label for=\"auto\" class=\"campo_label\">{$ling['impo_a_cada']}</label>
        <input type=\"text\" name=\"auto_freq\" id=\"auto_freq\" size=\"5\" class=\"campo_text\" value=\"".($modelo->auto_frequencia ? (int)$modelo->auto_frequencia : '1')."\">
        <select name=\"auto_peri\" class=\"campo_select\">
            <option value=\"1\" ".(($modelo->auto_periodicidade == '1') ? "selected=\"selected\"":'').">{$ling['horas']}</option>
            <option value=\"2\" ".((($modelo->auto_periodicidade == '2') or (!$modelo->auto_periodicidade)) ? "selected=\"selected\"":'').">{$ling['dias2']}</option>
            <option value=\"3\" ".(($modelo->auto_periodicidade == '3') ? "selected=\"selected\"":'').">{$ling['semanas']}</option>
        </select>
        <br clear=\"all\" />";
        FormData($ling['data_inicio'],'auto_datai',($modelo->auto_datainicio?(string)$modelo->auto_datainicio:date('d/m/Y')));
        echo "<input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"auto_horai\" class=\"campo_text\" name=\"auto_horai\" size=\"8\" maxlength=\"8\" value=\"".($modelo->auto_horainicio?(string)$modelo->auto_horainicio:date('H:i:s'))."\" />
<br clear=\"all\"/>
    </fieldset>
    ";
        echo "
    <br />
    <table width=\"100%\"><tr><td>
    <input type=\"submit\" class=\"botao\" name=\"salvar\" value=\"{$ling['expo_salvar_modelo']}\">
    </td><td align=\"right\">
        <a class=\"link\" style=\"cursor: pointer\" onclick=\"if (confirm('{$ling['expo_excluir_modelo']}')) { location.href='{$php_self}?d=".urlencode($arq_xml)."'; }\">
        <img src=\"../imagens/icones/22x22/del.png\" border=0> {$ling['expo_excluir_modelo2']}</a>
    </td></tr></table>
</fieldset>
<br />
<input type=\"submit\" class=\"botao\" name=\"importar\" value=\"{$ling['impo_salvar_modelo_importar']}\">
</form>
";
    }
    else { // nenhuma tabela selecionada
        echo "<fieldset><legend>{$ling['modelo']}</legend>{$ling['escolha_um_mode_ou_crie_novo']}<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></fieldset>";
    }
}
echo "<br />

</td><td width=\"200\" valign=\"top\">

<div>
<fieldset style=\"\"><legend>{$ling['expo_esquemas']}</legend>
    <div style=\"height: 350px; overflow: auto\">";
if ($handle = opendir($modelos_dir)) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($file = readdir($handle))) {
        if (substr($file,-4) == '.xml') 
            echo "<a href=\"{$modelos_dir}/$file\"><img src=\"../../../imagens/icones/22x22/seta_baixo.png\" border=0 title=\"{$ling['expo_baixar_esquema']}\"></a>
            <a class=\"link\" href=\"{$php_self}?m=".urlencode($file)."\"><img src=\"../../../imagens/icones/22x22/relatorio.png\" border=0> $file</a><br>";
    }
    closedir($handle);
}   
$j=1;
while(file_exists("{$modelos_dir}/esquema{$j}.xml")) $j++;
echo "
<br />
<br />
    </fieldset>
    <hr noshade=\"noshade\">
    <fieldset>
        <legend>{$ling['expo_novo_esquema']}</legend>
        <label class=\"campo_label\">{$ling['tabela']}</label><br clear=\"all\" />
        <select id=\"n_tb\" class=\"campo_select\">";
foreach ($tabelas_permitidas as $etb) echo "<option value=\"$etb\">{$tdb[$etb]['DESC']}</option>";
echo "</select><br />
        <label class=\"campo_label\">{$ling['arquivo']}</label><br clear=\"all\" />
        <input type=\"text\" id=\"n_arq\" value=\"esquema{$j}\" class=\"campo_text\"><br />
    </fieldset>
    <input type=\"button\" class=\"botao\" value=\"{$ling['criar']}\" onclick=\"location.href='{$php_self}?c='+urlencode(document.getElementById('n_arq').value)+'&tb='+document.getElementById('n_tb').options[document.getElementById('n_tb').selectedIndex].value\">

    <hr noshade=\"noshade\">
    <form method=POST action=\"{$php_self}?enviar=1\" ENCTYPE=\"multipart/form-data\">
    <fieldset>
        <legend>{$ling['expo_enviar_esquema']}</legend>
        <label class=\"campo_label\">{$ling['arquivo']}</label><br clear=\"all\" />
        <input type=\"file\" name=\"arq\" class=\"campo_text\"><br />
    </fieldset>
    <input type=\"submit\" class=\"botao\" value=\"{$ling['enviar']}\" />
    </form>

</div>

</td></tr></table>

</div>
</body>
</html>";

?>
