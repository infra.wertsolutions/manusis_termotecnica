<?
chdir(dirname(realpath($_SERVER['SCRIPT_FILENAME'])));
$php_self = "servico_gerencia.php";

// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../../../lib/autent.php")) die ($ling['autent01']);

if ($_SESSION[ManuSess]['user']['MID'] != 'ROOT') die($ling['err16']);

$set = (int)$_GET['set'];
if ($set) {
	if ($set == 1) {
		execInBackground("php \"servico.php\"");
		sleep(1);
	}
	if ($set == 2) {
		file_put_contents('parar_servico.txt',$ling['geren_parar_serv']);
		sleep(2);
		unlink('parar_servico.txt');
	}
	die("<script>location.href='{$php_self}'</script>");
}

// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"../../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"../../../lib/javascript.js\"> </script>";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../../../lib/movediv.js\"> </script>\n";
echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['geren_integracao']}</h1>
</div>

<div id=\"corpo_relatorio\">";

$status = (int)file_get_contents('servico_status.txt');

echo "<table><tr>
<td align=\"left\" valign=\"center\">{$ling['geren_estado_serv']}: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>".($status?$ling['geren_ligado']:$ling['geren_desligado'])."</td>
<td><a href=\"{$php_self}?set=".($status?'2':'1')."\"><img src=\"../../../imagens/icones/22x22/integrador_".($status?$ling['geren_ligado']:$ling['geren_desligado']).".png\" border=0></a></td>
</tr></table>";

echo "</div>
</body>
</html>";


?>