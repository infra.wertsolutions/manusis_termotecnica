<?php
/**
 * Para implementa��o futura:
 * Neste array apenas � checado se o valor � n�o-nulo. Ou seja, ao inv�s de '1',
 * pode ser usado no lugar um valor �til, como o nome de um campo ou um sub-array
 * Ex:
 * $importar_eventos_tabelas[NOME_DA_TABELA]=1;
 */

$importar_eventos_tabelas = array();


// Variavel persistente dos eventos
$importar_eventos_globais=array();

/**
 * Fun��o chamada a cada registro importado, SE E SOMENTE SE a tabela estiver no array $importar_eventos_tabelas
 * Esta fun��o � chamada ANTES de importar, ou seja, modifica��es no array $campos SER�O APLICADAS!!!
 * 
 * ATEN��O!!! Se em algum caso esta fun��o adicionar uma chave no array $campos,
 * essa chave deve ser adicionada em TODOS os registros!! (se n�o d� pau no numero de campos)
 * 
 * @param string $tabela tabela sendo importada
 * @param array  $campos array com os campos que v�o ser inseridos na tabela (pela fun��o que chamou esta)
 */
function Importar_Evento($tabela, &$campos) {
    global $tdb, $dba, $importar_eventos_globais;
    
    return true;
}

/**
 * Fun��o chamada para cada registro importado, SE E SOMENTE SE a tabela estiver no array $importar_eventos_tabelas
 * Esta fun��o � chamada DEPOIS de importar, e recebe somente o MID do registro j� importado
 *
 * @param string $tabela
 * @param int $mid
 */
function Importar_Evento_Apos($tabela, $mid) {
    global $tdb, $dba, $importar_eventos_globais;
    
    return true;
}

function Importar_Evento_Sincronizar($tabela, $campo, $valores) {
    global $tdb, $dba, $importar_eventos_globais;
    
    switch ($tabela) {
        default:
            $sql_sinc = "DELETE FROM $tabela WHERE $campo NOT IN ('" . implode("', '", $valores) . "')";
    }
    
    $sql_status = $dba[$tdb[$tabela]['dba']] -> Execute($sql_sinc);
    
    return $sql_status;
}

?>
