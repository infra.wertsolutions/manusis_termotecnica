<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
$php_ajax = "exportar_ajax.php";
$php_self = "exportar_modelo.php";
$php_path = "C:\\Arquivos de programas\\Zend\\Core\\bin\\php.exe";
$php_exportar = realpath("./exportar_dados.php");
//$_SERVER["SCRIPT_FILENAME"]

// Fun��es do Sistema
if (!require("../../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../../../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../../../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../../../conf/manusis.mod.php")) die ($ling['mod01']);

$modelos_dir = "../../../{$manusis['dir']['emodelos']}";
$exportar_url = "{$manusis['url']}{$manusis['dir']['exportar']}";
$url_exportar = "{$manusis['url']}exportar";

function LimpaArquivo($arq, $combarra=1) {
	if (strpos($arq,':') !== false) $arq = substr($arq,strrpos($arq,':')+1);
	if ($combarra) {
		if (strpos($arq,'./') !== false) $arq = substr($arq,strrpos($arq,'./')+2);
		while (strpos($arq,'//') !== false) $arq = str_replace('//','/',$arq);
		while ($arq[0] == '/') $arq = substr($arq,1);
	} else {
		if (strpos($arq,'/') !== false) $arq = substr($arq,strrpos($arq,'/')+1);
	}
	if (strpos($arq,'?') !== false) $arq = substr($arq,0,strrpos($arq,'?'));
	return $arq;
}

$tabelas_permitidas = array(
	AREAS,
	SETORES,
	MAQUINAS,
	MAQUINAS_CONJUNTO,
	EQUIPAMENTOS,
	MATERIAIS,
	ORDEM_PLANEJADO,
	CENTRO_DE_CUSTO
);

// Caso n�o exista um padr�o definido
if (!file_exists("../../../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$arq_xml=LimpaTexto($_GET['m']);
$c_arq_xml=LimpaTexto($_GET['c']);
$d_arq_xml=LimpaTexto($_GET['d']);
$salvar = $_POST['salvar'];
$exportar = $_POST['exportar'];
$enviar = $_GET['enviar'];

if ($enviar) {
	$parq = $_FILES['arq'];
	$parq_parts = pathinfo($parq['name']);
	if ($parq_parts['extension'] == 'xml') {
		$arq_xml = $parq_parts['basename'];
		move_uploaded_file($parq['tmp_name'],"{$modelos_dir}/{$arq_xml}");
		die("<script>location.href='{$php_self}?m={$arq_xml}'</script>");
	}
}

if (($d_arq_xml) and (substr($d_arq_xml,-4) == '.xml') and (file_exists("{$modelos_dir}/{$c_arq_xml}"))) {
	unlink("{$modelos_dir}/{$d_arq_xml}");
	die("<script>location.href='{$php_self}'</script>");
}
if ($c_arq_xml) {
	$tb = $_GET['tb'];
	file_put_contents("{$modelos_dir}/{$c_arq_xml}.xml","<modelo><tabela>$tb</tabela><arquivo>{$c_arq_xml}.xml</arquivo></modelo>");
	die("<script>location.href='{$php_self}?m=".urlencode($c_arq_xml).".xml'</script>");
}

// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
$msg='';
if ($salvar or $exportar){
	$cc=$_POST['cc'];
	$or=$_POST['or'];
	$float_sep=$_POST['float_sep'];
	$satis=$_POST['satis'];
	$mandar_email=$_POST['mandar_email'];
	$email=$_POST['email'];
	$mandar_unidade=$_POST['mandar_unidade'];
	$unidade=$_POST['unidade'];
	$mandar_ftp=$_POST['mandar_ftp'];
	$mandar_ftp_host=$_POST['mandar_ftp_host'];
	$mandar_ftp_local=$_POST['mandar_ftp_local'];
	$mandar_ftp_user=$_POST['mandar_ftp_user'];
	$mandar_ftp_pass=$_POST['mandar_ftp_pass'];
	$mostrar_titulos=$_POST['mostrar_titulos'];
	$auto_freq = $_POST['auto_freq'];
	$auto_peri = $_POST['auto_peri'];
	$auto_datai = $_POST['auto_datai'];
	$auto_horai = $_POST['auto_horai'];

	$modelo_xml = $_SESSION['ManuSess']['exportar_modelo_xml'];
	if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
	$modelo = new SimpleXMLElement($modelo_xml);
	$tb = (string)$modelo->tabela;
	if (!$tb) errofatal($ling['err14']);
	
	$sql="SELECT * FROM $tb";
	unset($modelo->campo);
	if (!$resultado= $dba[$tdb[$tb]['dba']] -> SelectLimit($sql,1)) errofatal($dba[$tdb[$tb]['dba']] -> ErrorMsg()."<br /><br />$sql");
	$ncampos = (!$resultado->EOF) ? array_keys($resultado->fields) : array();
	foreach ($ncampos as $ecampo) {
		$ec = $modelo->addChild('campo');
		$ec->addAttribute('nome',(string)$ecampo);
		$ec[0]=(int)$cc[$ecampo];
	}
	
	$modelo->ordenar = $or;
	$modelo->virgula = $float_sep;
	$modelo->satisfazer = ($satis ? $satis : "AND");
	$modelo->mostrar_titulos = $mostrar_titulos;
	$modelo->mandar_email = $mandar_email;
	$modelo->email = $email;
	$modelo->mandar_unidade = $mandar_unidade;
	$modelo->unidade = LimpaArquivo($unidade,0);
	$modelo->mandar_ftp = $mandar_ftp;
	$modelo->mandar_ftp_host = $mandar_ftp_host;
	$modelo->mandar_ftp_local = $mandar_ftp_local;
	$modelo->mandar_ftp_user = $mandar_ftp_user;
	$modelo->mandar_ftp_pass = base64_encode($mandar_ftp_pass);
	$modelo->auto_frequencia = $auto_freq;
	$modelo->auto_periodicidade = $auto_peri;
	$modelo->auto_datainicio = $auto_datai;
	$modelo->auto_horainicio = $auto_horai;
	
	file_put_contents("{$modelos_dir}/{$arq_xml}",$modelo->asXML());	

	if ($exportar) {
		// executa script que gera os dados
		file_put_contents("{$modelos_dir}/{$arq_xml}.txt",'0');
		execInBackground("php \"$php_exportar\" m={$arq_xml}");
		$msg = "<iframe src=\"{$php_ajax}?id=progresso&m={$arq_xml}\" width=\"100%\" height=\"200\"></iframe>";
	}
}

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"../../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"../../../lib/javascript.js\"> </script>
<script>
function AdicionaCrit() {
	var c1 = document.getElementById('cric');
	var cs = document.getElementById('cris');
	var c2 = document.getElementById('criv');
	if (c2.name == 'select') var c2v = c2.options[c2.selectedIndex].value;
	else var c2v = urlencode(c2.value);
	atualiza_area2('lista_crits','{$php_ajax}?id=lista_crits&c1='+c1.options[c1.selectedIndex].value+'&cs='+cs.options[cs.selectedIndex].value+'&c2='+c2v);
}
function RemoveCrit(id) {
	atualiza_area2('lista_crits','{$php_ajax}?id=lista_crits&del='+id);
}
function AlternaDicasForm() {
	var div = document.getElementById('dica_format');
	if (div.style.display == 'none') {
		div.style.display = 'block';
	}
	else {
		div.style.display = 'none';
	}
}
</script>
";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../../../lib/movediv.js\"> </script>\n";
echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['expo_exportar_dados']} ".($arq_xml? " - ($arq_xml)" : '')."</h1>
</div>

<div id=\"corpo_relatorio\">

<table width=\"100%\"><tr><td width=\"*\" valign=\"top\">

";

if ($exportar) {
	echo "$msg";
}
else {
	$modelo_xml = file_get_contents("{$modelos_dir}/{$arq_xml}");
	if (!$modelo_xml) $modelo_xml =  '<modelo></modelo>';
	$_SESSION['ManuSess']['exportar_modelo_xml'] = $modelo_xml;
	$modelo = new SimpleXMLElement($modelo_xml);
	$tb = (string)$modelo->tabela;
	if ($tb) {
		
		foreach ($modelo->campo as $evalor) {
			$tbcampos[(string)$evalor['nome']] = (string)$evalor[0];
		}

	echo "<form action=\"{$php_self}?m=".urlencode($arq_xml)."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"POST\">
<fieldset><legend>{$ling['modelo']}</legend>

	<label for=\"tb\">{$ling['tabela']}</label>
	<span id=\"tb\">{$tdb[$tb]['DESC']}</span>
	<br clear=\"all\" />

	<fieldset>
	<legend>{$ling['expo_campos']}</legend>
	<table id=\"campos_relatorio\">";
		$sql="SELECT * FROM $tb";
		if (!$resultado= $dba[$tdb[$tb]['dba']] -> SelectLimit($sql,1)){
			$err = $dba[$tdb[$tb]['dba']] -> ErrorMsg();
			erromsg("$err<br /><br />$sql");
			exit;
		}
		else {
			$ncampos=$resultado -> FieldCount();
			echo "<tr>\n";
			$ii=0;
			for ($fc=0; $fc < $ncampos; $fc++) {
				$campo=$resultado -> FetchField($fc);
				if ($tdb[$tb][$campo -> name] != "") {
					$ecampo = (string)$tbcampos[$campo -> name];
					$enome = (string)$campo -> name;
					if ($ecampo == '0') echo "<td><input class=\"campo_check\" type=\"checkbox\" name=\"cc[$enome]\" id=\"cc[$enome]\" value=\"1\" /><label for=\"cc[$enome]\">".$tdb[$tb][$enome]."</label></td>\n";
					else echo "<td><input class=\"campo_check\" type=\"checkbox\" checked=\"checked\" name=\"cc[$enome]\" id=\"cc[$enome]\" value=\"1\" /><label for=\"cc[$enome]\">".$tdb[$tb][$enome]."</label></td>\n";
					if ($ii == 4) {
						$ii =0;
						echo "</tr><tr>\n";
					}
					else $ii++;
				}
			}
			echo "</tr></td></table></fieldset>
	<fieldset>
	<legend>".$ling['criterios_def']."</legend>

	<label for=\"mostrar_titulos\" class=\"campo_label\">".$ling['inclui_os_titu_campos']."</label>
	<input type=\"checkbox\" name=\"mostrar_titulos\" value=\"1\" id=\"mostrar_titulos\" ".(((int)$modelo->mostrar_titulos) ? "checked=\"checked\"" : '').">
	<br clear=\"all\" />

	<label for=\"float_sep\" class=\"campo_label\">".$ling['sepa_casa_decimal']."</label>
	<input type=\"text\" name=\"float_sep\" class=\"campo_text\" size=2 id=\"float_sep\" value=\"".(($modelo->virgula) ? (string)$modelo->virgula : ',')."\" />
	<br clear=\"all\" />
	
	<label for=\"or\">".$ling['ordernar']."</label>
	<select name=\"or\" class=\"campo_select\">\n";
			for ($fc=0; $fc < $ncampos; $fc++) {
				$campo=$resultado -> FetchField($fc);
				if ($tdb[$tb][$campo -> name] != "") {
					echo "<option class=\"campo_option\" value=\"".$campo -> name."\" ".(((string)$modelo->ordenar == (string)$campo->name) ? "selected=\"selected\"" : '')." >".$tdb[$tb][$campo -> name]."</option>\n";
				}
			}
			echo "</select><br clear=\"all\" />
	
	<table><tr><td>		
	<label for=\"cric\">".$ling['criterio']."</label>
	<select onchange=\"atualiza_area2('cricd','{$php_ajax}?id=crit&tb={$tb}&cric='+this.options[this.selectedIndex].value)\" class=\"campo_select\" id=\"cric\"><option class=\"campo_option\" value=\"\">".$ling['select_opcao']."</option>\n";
			for ($fc=0; $fc < $ncampos; $fc++) {
				$campo=$resultado -> FetchField($fc);
				if ($tdb[$tb][$campo -> name] != "") {
					if ($cric == $campo -> name) echo "<option selected=\"selected\" class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
					else echo "<option class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
				}
			}
			echo "</select>
	</td><td>
	<div id=\"cricd\" style=\"float: left\"></div>
	</td></tr></table>";
			$satis = (string)$modelo->satisfazer;
			if (!$satis) $satis = 'AND';
			echo "
			<label for=\"satis\">{$ling['expo_criterios_satisfazer']}</label>
			<select name=\"satis\" class=\"campo_select\">
				<option value=\"AND\" ".(($satis == 'AND')?"selected=\"selected\"":'').">{$ling['todos_si']}</option>
				<option value=\"OR\" ".(($satis == 'OR')?"selected=\"selected\"":'').">{$ling['qualquer_um']}</option>
			</select>
			<br clear=\"all\" />
			
			<div id=\"lista_crits\"><script>atualiza_area2('lista_crits','{$php_ajax}?id=lista_crits')</script></div>
			
			</fieldset>
			
			<fieldset><legend>".$ling['dest_dos_dados']."</legend>

			<label for=\"mandar_email\" class=\"campo_label\">{$ling['env_email']}</label>
			<input type=\"checkbox\" name=\"mandar_email\" value=\"1\" id=\"mandar_email\" ".(((int)$modelo->mandar_email) ? "checked=\"checked\"" : '').">
			<input type=\"text\" name=\"email\" id=\"email\" size=\"50\" class=\"campo_text\" value=\"".($modelo->email ? (string)$modelo->email : $manusis['admin']['email'])."\">
			<br clear=\"all\" />

			<label for=\"mandar_unidade\" class=\"campo_label\">{$ling['arq_disco_loca']}</label>
			<input type=\"checkbox\" name=\"mandar_unidade\" value=\"1\" id=\"mandar_unidade\" ".(((int)$modelo->mandar_unidade) ? "checked=\"checked\"" : '').">
			<input type=\"text\" name=\"unidade\" id=\"unidade\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->unidade) ? (string)$modelo->unidade : 'arquivo.csv' )."\">
			<a onclick=\"AlternaDicasForm()\" style=\"cursor: pointer\">{$ling['dicas_formatacao']}</a>
			<br clear=\"all\" />

			<label for=\"mandar_ftp\" class=\"campo_label\">{$ling['expo_salvar_servidor_ftp']}</label>
			<input type=\"checkbox\" name=\"mandar_ftp\" value=\"1\" id=\"mandar_ftp\" ".(((int)$modelo->mandar_ftp) ? "checked=\"checked\"" : '').">
			<table style=\"border: 1px solid silver\"><tr><td>
				<label for=\"mandar_ftp_host\" class=\"campo_label\">{$ling['expo_host_ftp']}</label>
				<input type=\"text\" name=\"mandar_ftp_host\" id=\"mandar_ftp_host\" size=\"50\" class=\"campo_text\" value=\"".((string)$modelo->mandar_ftp_host)."\">
				<br clear=\"all\" />

				<label for=\"mandar_ftp_local\" class=\"campo_label\">{$ling['arq_diretorio']}</label>
				<input type=\"text\" name=\"mandar_ftp_local\" id=\"mandar_ftp_local\" size=\"50\" class=\"campo_text\" value=\"".(($modelo->mandar_ftp_local) ? (string)$modelo->mandar_ftp_local : '/arquivo.csv' )."\">
				<a onclick=\"AlternaDicasForm()\" style=\"cursor: pointer\">{$ling['dicas_formatacao']}</a>
				<br clear=\"all\" />

				<label for=\"mandar_ftp_user\" class=\"campo_label\">{$ling['sol_usuario']}</label>
				<input type=\"text\" name=\"mandar_ftp_user\" id=\"mandar_ftp_user\" size=\"18\" class=\"campo_text\" value=\"".(string)$modelo->mandar_ftp_user."\">
				<label class=\"campo_label\" style=\"float:none\">{$ling['senha']}</label>
				<input type=\"password\" name=\"mandar_ftp_pass\" id=\"mandar_ftp_pass\" size=\"18\" class=\"campo_text\" value=\"".base64_decode((string)$modelo->mandar_ftp_pass)."\">
			</td></tr></table>
			
				<div id=\"dica_format\" style=\"display: none\">
				<br clear=\"all\" />
				{$ling['expo_texto']}:<br>
				<strong>%d</strong> - {$ling['expo_dia_mes']} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>%m</strong> - {$ling['expo_mes']} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>%a</strong> - {$ling['ano']} (ex: 2009) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>%h</strong> - {$ling['hora']} (ex: 14:25:37) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>%s</strong> - {$ling['expo_semana_ano']} (ex: 47) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<strong>%ds</strong> - {$ling['expo_dia_sem_ex']}
				</div>
			</fieldset>

			<fieldset><legend>{$ling['agenda']}</legend>
				<label for=\"auto\" class=\"campo_label\">{$ling['expo_exportar_a']}</label>
				<input type=\"text\" name=\"auto_freq\" id=\"auto_freq\" size=\"5\" class=\"campo_text\" value=\"".($modelo->auto_frequencia ? (int)$modelo->auto_frequencia : '1')."\">
				<select name=\"auto_peri\" class=\"campo_select\">
					<option value=\"1\" ".(($modelo->auto_periodicidade == '1') ? "selected=\"selected\"":'').">{$ling['horas']}</option>
					<option value=\"2\" ".(((!$modelo->auto_periodicidade) or ($modelo->auto_periodicidade == '2')) ? "selected=\"selected\"":'').">".strtoupper($ling['dias'])."</option>
					<option value=\"3\" ".(($modelo->auto_periodicidade == '3') ? "selected=\"selected\"":'').">{$ling['semanas']}</option>
				</select>
				<br clear=\"all\" />";
			FormData($ling['data_inicio'],'auto_datai',($modelo->auto_datainicio?(string)$modelo->auto_datainicio:date('d/m/Y')));
			echo "<input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"auto_horai\" class=\"campo_text\" name=\"auto_horai\" size=\"8\" maxlength=\"8\" value=\"".($modelo->auto_horainicio?(string)$modelo->auto_horainicio:date('H:i:s'))."\" />
<br clear=\"all\"/>
			</fieldset>
			";
		}
		echo "
	<br />
	<table width=\"100%\"><tr><td>
	<input type=\"submit\" class=\"botao\" name=\"salvar\" value=\"{$ling['expo_salvar_modelo']}\">
	</td><td align=\"right\">
		<a class=\"link\" style=\"cursor: pointer\" onclick=\"if (confirm({$ling['expo_excluir_modelo']})) { location.href='{$php_self}?d=".urlencode($arq_xml)."'; }\">
		<img src=\"../imagens/icones/22x22/del.png\" border=0> {$ling['expo_excluir_modelo2']}</a>
	</td></tr></table>
</fieldset>
<br />
<input type=\"submit\" class=\"botao\" name=\"exportar\" value=\"{$ling['expo_salvar_modelo_exportar']}\">
</form>
";
	}
	else { // nenhuma tabela selecionada
		echo "<fieldset><legend>{$ling['modelo']}</legend>".$ling['escolha_um_mode_ou_crie_novo'].".<br /><br /><br /><br /><br /></fieldset>";
	}
}
echo "<br />

</td><td width=\"200\" valign=\"top\">

<div>
<fieldset style=\"\"><legend>{$ling['expo_esquemas']}</legend>
	<div style=\"height: 350px; overflow: auto\">";
if ($handle = opendir($modelos_dir)) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($file = readdir($handle))) {
    	if (substr($file,-4) == '.xml') 
    		echo "<a href=\"{$modelos_dir}/$file\"><img src=\"../../../imagens/icones/22x22/seta_baixo.png\" border=0 title=\"{$ling['expo_baixar_esquema']}\"></a>
    		<a class=\"link\" href=\"{$php_self}?m=".urlencode($file)."\"><img src=\"../../../imagens/icones/22x22/relatorio.png\" border=0> $file</a><br>";
    }
    closedir($handle);
}	
$j=1;
while(file_exists("{$modelos_dir}/esquema{$j}.xml")) $j++;
echo "
<br />
<br />
	</fieldset>
	<hr noshade=\"noshade\">
	<fieldset>
		<legend>{$ling['expo_novo_esquema']}</legend>
		<label class=\"campo_label\">{$ling['tabela']}</label><br clear=\"all\" />
		<select id=\"n_tb\" class=\"campo_select\">";
foreach ($tabelas_permitidas as $etb) echo "<option value=\"$etb\">{$tdb[$etb]['DESC']}</option>";
echo "</select><br />
		<label class=\"campo_label\">{$ling['arquivo']}</label><br clear=\"all\" />
		<input type=\"text\" id=\"n_arq\" value=\"esquema{$j}\" class=\"campo_text\"><br />
	</fieldset>
	<input type=\"button\" class=\"botao\" value=\"{$ling['criar']}\" onclick=\"location.href='{$php_self}?c='+urlencode(document.getElementById('n_arq').value)+'&tb='+document.getElementById('n_tb').options[document.getElementById('n_tb').selectedIndex].value\">

	<hr noshade=\"noshade\">
	<form method=POST action=\"{$php_self}?enviar=1\" ENCTYPE=\"multipart/form-data\">
	<fieldset>
		<legend>{$ling['expo_enviar_esquema']}</legend>
		<label class=\"campo_label\">{$ling['arquivo']}</label><br clear=\"all\" />
		<input type=\"file\" name=\"arq\" class=\"campo_text\"><br />
	</fieldset>
	<input type=\"submit\" class=\"botao\" value=\"{$ling['enviar']}\" />
	</form>

</div>

</td></tr></table>

</div>
</body>
</html>";

?>
