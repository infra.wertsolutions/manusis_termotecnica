<?
/**
* Visualiza��o dos graficos do assistente de Gr�ficos para pagina inicial
*
* @author  Fernando Cosentino
* @version 0.1 
* @package manusis
* @subpackage principal
*/

ob_start();

if (!include("../../lib/mfuncoes.php")) diefile ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
if (!include("../../conf/manusis.conf.php")) diefile ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
if (!include("../../lib/idiomas/".$manusis['idioma'][0].".php")) diefile ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
if (!include("../../lib/adodb/adodb.inc.php")) diefile ($ling['bd01']);
if (!include("../../lib/bd.php")) diefile ($ling['bd01']);

if (!include("../../modulos/relatorios/funcoes.php")) diefile ($ling['bd01']);
elseif (!include("../../modulos/relatorios/conf.php")) diefile ($ling['bd01']);

include("../../lib/swfcharts/charts.php");

// transi��es dos graficos
$trans = array(drop, scale, slide_left, slide_down);

// troca , por . ?
$trocavirg = 1;
$troca1 = ',';
$troca2 = '.';
// caracteres em volta dos valores
$carac1 = '';
$carac2 = '';

// este php ser� chamado por um swf, n�o pode ter echo



function FlipArray($arr) {
    $cols = count($arr[0]);
    $rows = count($arr);

    for ($i=0; $i<$cols; $i++) {
        for ($j=0; $j<$rows; $j++) {
            $arr2[$i][$j] = $arr[$j][$i];
        }
    }
    return $arr2;
}
/*function Abreviate($str, $num_palavras, $num_carac, $remove_ligacao = 0){
    // retorna as primeiras $num_palavras abreviadas em $num_carac caracteres
    // exemplo: Abreviate('Fulano da Silva Sauro',3,3) retorna 'Ful. da Sil.' (3 palavras, 3 carac)
    // se $remove_ligacao, remove: a, o, e, da, do, de, na, no, em
    // exemplo: Abreviate('Fulano da Silva Sauro',3,3, TRUE) retorna 'Ful. Sil. Sau.' (3 palavras, 3 carac)
    $palavras_a_remover = array('A','O','E','DA','DO','DE','NA','NO','EM');
    $palavras_tmp = explode(' ',$str);
    if ($remove_ligacao) foreach ($palavras_tmp as $epalavra) {
        if (!in_array(strtoupper($epalavra),$palavras_a_remover)) $palavras[] = $epalavra;
    }
    else $palavras = $palavras_tmp;
    $retorno='';
    for ($i=0; $i<$num_palavras; $i++) {
        $tmp = substr($palavras[$i],0,$num_carac); // recorta primeiros caracteres
        if ($tmp != $palavras[$i]) $tmp .= '.'; // se cortou peda�o da palavra, poe um ponto
        // adiciona em retorno
        if ($retorno) $retorno .= ' ';
        $retorno .= $tmp;
    }
    return $retorno;
}*/
$arq=urldecode($_GET['arq']);
$modo=$_GET['modo'];
if (($modo == '3d pie') or ($modo == 'pie')) {
    $ispie = 1;
}
else {
    $ispie = 0;
}
if ($arq != "") {
    $dir="../../".$manusis['dir']['graficos'];
    $xml = new DOMDocument();
    $xml->formatOutput = true;
    $xml -> load("$dir/$arq");
    $descricao_grafico=$xml -> getElementsByTagName('descricao');
    $descricao_grafico=utf8_decode($descricao_grafico -> item(0) -> nodeValue);
    $params = $xml->getElementsByTagName('tabela');
    $i=0;
    foreach ($params as $param) {
        $tbid=$param -> getAttribute('id');
        $tabela=AG_PegaTabela($xml,$tbid);
        $tbinfo=AG_InfoTabela($xml,$tbid);
        // Campo data que ser� usado para pesquisa ex: DATA_INICIO
        $tbdatat="DATA_PROG";
        if ($tbinfo['tipo'] == 1){
            $tbano=date('Y');
            $sql_filtro="WHERE DATE_FORMAT(`$tbdatat`,'%Y') = '$tbano' ";
            $datai="$tbano-01-01";
            $dataf="$tbano-12-31";


        }
        elseif ($tbinfo['tipo'] == 2){
            $chart_explain = array (
                'type'       => "text",
                'transition' => $trans[rand(0,count($trans)-1)],
                'x'          => 550, 
                'y'          => 425, 
                'width'      => 200,  
                'h_align'    => "left", 
                'text'       => utf8_encode($ling['dados_ult_dias']),
                'size'       => 12, 
                'color'      => "000066", 
            );

            $tbdatai=date('d/m/Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
            $tbdataf=date('d/m/Y');
            $tmp_datai=explode("/",$tbdatai);
            $tmp_dataf=explode("/",$tbdataf);
            $savestr = "tmp_datai=$tmp_datai / tmp_dataf=$tmp_dataf\r\n";
            if (!checkdate((int)$tmp_datai[1],(int)$tmp_datai[0],(int)$tmp_datai[2])) {
                erromsg("{$ling['a_data']} - $tbdatai {$ling['a_data_invalida']}");
                echo "<br />";
                $erro=1;
                $xls_erro=1;
            }
            elseif (!checkdate((int)$tmp_dataf[1],(int)$tmp_dataf[0],(int)$tmp_dataf[2])) {
                erromsg("{$ling['a_data']} - $tbdataf {$ling['a_data_invalida']}");
                echo "<br />";
                $erro=1;
                $xls_erro=1;
            }
            // Aqui eu uso DATE_FORMAT, pois n�o quero ficar com codigo mais limpo
            $info_data="{$ling['periodo_de']}: $tbdatai � $tbdataf";
            $datai=explode("/",$tbdatai);
            $datai=$datai[2]."-".$datai[1]."-".$datai[0];
            $dataf=explode("/",$tbdataf);
            $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
            $sql_filtro="WHERE $tbdatat >= '$datai' AND $tbdatat <= '$dataf' ";
        }

        if (!$tbinfo['coluna_mestre']){
            erromsg($ling['prin_graf_col_mestre']);
            echo "<br />";
            $erro=1;
            $xls_erro=1;
        }
        if (!$tbinfo['coluna_dados']){
            erromsg($ling['prin_graf_col_dados']);
            echo "<br />";
            $erro=1;
            $xls_erro=1;
        }
        if (!$erro) {
            // Monto o restante dos filtros definidos pelo usuario
            $filtros = $tabela ->getElementsByTagName($ling['prin_graf_filtro']);
            
            // Filtros passados a coluna mestre
            $filtro_adicional = array();

            $if_operador=0;
            foreach ($filtros as $filtro) {
                $cond=$filtro -> getAttribute($ling['prin_graf_condicao']);
                $condicao=$AG_cond_sinal[$cond];
                $campo=$filtro ->getElementsByTagName($ling['prin_graf_campo']) ->  item(0) -> nodeValue;
                $valor=$filtro ->getElementsByTagName($ling['prin_graf_valor']) -> item(0) -> nodeValue;
                $operador=$filtro ->getElementsByTagName($ling['prin_graf_operador']) -> item(0) -> nodeValue;

                // Pego rela��o do campo deo filtro na tabela ORDEM e mostro resultado
                $tmp_filtro=VoltaRelacao(ORDEM_PLANEJADO,$campo);
                $tmp_filtro=VoltaValor($tmp_filtro['tb'],$tmp_filtro['campo'],$tmp_filtro['mid'],$valor,$tmp_filtro['dba']);

                // Uma entrada para o filtro SQL outra para Apresenta��o dos filtros usados
                if ($if_operador == 0){
                    // Campo existe na OS
                    if($tdb[ORDEM][$campo]) {
                        $sql_filtro.=" AND ($campo $condicao '$valor' ";
                    }
                    // Campo l�gico
                    elseif($AG_filtros[$campo]['COND']) {
                        if(is_array($AG_filtros[$campo]['COND'])) {
                            $sql_filtro.=" AND (" . $AG_filtros[$campo]['COND'][$valor];
                        }
                        else {
                            $sql_filtro.=" AND (" . str_replace('%%VALOR%%', $valor, $AG_filtros[$campo]['COND']);
                        }
                    }

                    $info_filtro.=$AG_filtros[$campo]['DESCRICAO']." <i>".$AG_cond[$cond]."</i> $tmp_filtro<br />";
                }
                else {
                    // Campo existe na OS
                    if($tdb[ORDEM][$campo]) {
                        $sql_filtro.=" ".$AG_oper_sinal[$operador]." $campo $condicao '$valor' ";
                    }
                    // Campo l�gico
                    elseif($AG_filtros[$campo]['COND']) {
                        if(is_array($AG_filtros[$campo]['COND'])) {
                            $sql_filtro.=" ".$AG_oper_sinal[$operador]." " . $AG_filtros[$campo]['COND'][$valor];
                        }
                        else {
                            $sql_filtro.=" ".$AG_oper_sinal[$operador]." " . str_replace('%%VALOR%%', $valor, $AG_filtros[$campo]['COND']);
                        }
                    }

                    $info_filtro.="<u>".$AG_oper[$operador]."</u> ".$AG_filtros[$campo]['DESCRICAO']." <i>".$AG_cond[$cond]."</i> $tmp_filtro<br />";

                }
                
                if ($campo == "MID_EQUIPE") {
                    $filtro_adicional['MID_EQUIPE'][] = array($AG_oper_sinal[$operador], $condicao, $valor);
                }

                $if_operador++;
            }
            
            if ($if_operador != 0) {
                $sql_filtro.=")";
            }

            // Filtro por Empresa
            $fil_emp = VoltaFiltroEmpresa(ORDEM_PLANEJADO, 2);
            if ($fil_emp != "") {
                $sql_filtro .= ($sql_filtro != "")? " AND " . $fil_emp : " WHERE " . $fil_emp;
            }

            // Executo o SQL
            $sql="SELECT O.* FROM ".ORDEM_PLANEJADO." O $sql_filtro";
            $savestr .= "sql1: $sql\r\n";
            $resultado=$dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql);
            if (!$resultado) {
                erromsg("{$ling['err17']}: ".$dba[$tdb[ORDEM_PLANEJADO]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                echo "<br><br>";
                $xls_erro=1;
            }
            else {
                $chart='';
                // CARREGO O TITULO DAS TABELAS
                // POR EVOLU��O ANUAL
                if ($tbinfo['tipo'] == 1){
                    // Titulo da coluna mestre
                    //<th>".$AG_colMestre[$tbinfo['coluna_mestre']]['DESCRICAO']."</th>\n";

                    // Titulo das colunas de dados (meses)
                    $chart[0] = array('',$ling['mes_parc'][1],$ling['mes_parc'][2],$ling['mes_parc'][3],$ling['mes_parc'][4],$ling['mes_parc'][5],$ling['mes_parc'][6],$ling['mes_parc'][7],$ling['mes_parc'][8],$ling['mes_parc'][9],$ling['mes_parc'][10],$ling['mes_parc'][11],$ling['mes_parc'][12]);
                    $xsize = 14; // tamanho da letra de baixo
                    $topsize = 10; // tamanho da letra de cima
                    $ncols=12;
                    if ($ispie) {
                        $charttext[0]=array(null,null,null,null,null,null,null,null,null,null,null,null,null);
                    }
                }
                // POR PERIODO
                if ($tbinfo['tipo'] == 2){
                    // Titulo da coluna mestre
                    //<th class=\"excel_tdi\">".$AG_colMestre[$tbinfo['coluna_mestre']]['DESCRICAO']."</th>\n";
                    // Titulo das colunas de dados
                    $ncols=0;
                    $chart[0][0]='';
//                  if ($ispie) {
                        $charttext[0][0] = '';
//                  }
                    $maiornome = 0; // variavel usada para calcular tamanho da letra
                    foreach ($tbinfo['coluna_dados'] as $col => $valor){
                        $chart[0][] = utf8_encode($AG_colDados[$col]['DESCRICAO']);
//                      if ($ispie) {
                            $charttext[0][] = '';
//                      }
                        $ncols++;
                    }
                }
                switch ($tbinfo['coluna_mestre_tipo']){
                    case 1:

                        // Processo o resultado
                        $tmp=AG_ColunaDados($resultado,$xml,$tbid,$tbdatat,$datai,$dataf);
                        $mostra=$tmp['campos']; // Colunas que contem resultado
                        $dados=$tmp['dados']; // Dados das colunas
                        
                        // FILTROS ADICIONAIS
                        $filtro_sql = "";
                        if (($tbinfo['coluna_mestre'] == FUNCIONARIOS) and (count($filtro_adicional['MID_EQUIPE']) > 0)) {
                            foreach ($filtro_adicional['MID_EQUIPE'] as $valores) {
                                $filtro_sql .= ($filtro_sql == "")? " WHERE " : " " . $valores[0] . " ";
                                $filtro_sql .= "EQUIPE  " . $valores[1] . " " . $valores[2];
                            }
                        }
                        
                        // Busco na tabela da coluna mestre as informa��es recolhidas
                        $sql="SELECT * FROM ".$tbinfo['coluna_mestre'] . "" . $filtro_sql;
                        $savestr .= "sql2: $sql\r\n";
                        $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                        if (!$resultado2) {
                            // Deu revestrez!
                            erromsg("{$ling['err18']}: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                            $xls_erro=1;
                        }

                        else {
                            $d=0;
                            while (!$resultado2->EOF) {
                                $campo=$resultado2->fields;
                                if ($mostra[$campo['MID']]){
                                    $d++;
                                    // Descubro o nome de apresenta��o e monto ele
                                    $primcampo='';
                                    foreach ($AG_colMestre[$tbinfo['coluna_mestre']]['CAMPO'] as $ca => $valor) {
                                        $primcampo .= $campo[$valor]." ";
                                    }
                                    $chart[$d][0]=utf8_encode($primcampo);
                                    echo "chart[$d][0] = '{$chart[$d][0]}'\r\n";
                                    
//                                  if ($ispie) {
                                        $charttext[$d][0]='';
//                                  }
                                    if (strlen($primcampo) > $primcampo) {
                                        $maiornome = strlen($primcampo);
                                        $maiornomet = $primcampo;
                                    }                   

                                    // Preencho todas Colunas com os valores encontrados
                                    if ($tbinfo['tipo'] == 1){
                                        for ($n=1; $n <= $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n][$campo['MID']]){
                                                $dados[$n][$campo['MID']]=0;
                                            }
                                            $chart[$d][$n] = round($dados[$n][$campo['MID']],3);
                                            if ($trocavirg) $chart[$d][$n] = str_replace($troca1,$troca2,$chart[$d][$n]);
                                            if ($ispie) {
                                                $charttext[$d][$n] = $chart[$d][$n].'\r'.Abreviate($chart[0][$n],2,3, TRUE);
                                            }
                                            else $charttext[$d][$n] = $carac1.round($chart[$d][$n],1).$carac2;
                                            $ddd[$d][$n];
                                        }
                                    }
                                    if ($tbinfo['tipo'] == 2){
                                        for ($n=0; $n < $ncols; $n++){
                                            /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                            pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                            if (!$dados[$n][$campo['MID']]){
                                                $dados[$n][$campo['MID']]=0;
                                            }
                                            $chart[$d][$n+1] =  round($dados[$n][$campo['MID']],3);
                                            if ($trocavirg) $chart[$d][$n+1] = str_replace($troca1,$troca2,$chart[$d][$n+1]);
                                            if ($ispie) {
                                                $charttext[$d][$n+1] = $chart[$d][$n+1].'\r'.Abreviate($chart[$d][0],2,3, TRUE);
                                            }
                                            else $charttext[$d][$n+1] = $carac1.round($chart[$d][$n+1],1).$carac2;
                                            $ddd[$d][$n];

                                        }
                                    }

                                }
                                //fim desta linha da tabela
                                $resultado2->MoveNext();
                            }
                        }
                        break;
                        

                        
                        /* CONDI��O ESPECIAL PARA LISTAR TIPOS DE SERVI�O
                        E TIPOS DE PROGRAMA��O JUNTOS */
                        case 2:
                            // Processo o resultado
                            $tmp=AG_ColunaDados($resultado,$xml,$tbid,$tbdatat,$datai,$dataf);
                            $mostra=$tmp['campos']; // Colunas que contem resultado
                            $dados=$tmp['dados']; // Dados das colunas
                            // Busco na tabela da coluna mestre as informa��es recolhidas
    
                            $sql="SELECT * FROM ".PROGRAMACAO_TIPO;
                            $savestr .= "sql2a: $sql\r\n";
    
                            $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                            if (!$resultado2) {
                                // Deu revestrez!
                                erromsg("{$ling['err18']}: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                                $xls_erro=1;
                            }
                            else {
                                $d = 0;
                                while (!$resultado2->EOF) {
                                    $campo=$resultado2->fields;
                                    if ($mostra["P".$campo['MID']]){
                                        $d++;
                                        $i1++;
                                        $iim++;
                                        $x_lin++;
                                        // Descubro o nome de apresenta��o e monto ele
                                        $primcampo = $campo["DESCRICAO"];
                                        $chart[$d][0]=utf8_encode($primcampo);
                                        if ($ispie) {
                                            $charttext[$d][0]='';
                                        }
                                        if (strlen($primcampo) > $primcampo) {
                                            $maiornome = strlen($primcampo);
                                            $maiornomet = $primcampo;
                                        }                   
    
                                        // Preencho todas Colunas com os valores encontrados
                                        if ($tbinfo['tipo'] == 1){
                                            for ($n=1; $n <= $ncols; $n++){
                                                /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                                pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                                if (!$dados[$n]["P".$campo['MID']]){
                                                    $dados[$n]["P".$campo['MID']]=0;
                                                }

                                                $chart[$d][$n] = round($dados[$n]["P".$campo['MID']],3);
                                                if ($trocavirg) $chart[$d][$n] = str_replace($troca1,$troca2,$chart[$d][$n]);
                                                if ($ispie) {
                                                    $charttext[$d][$n] = $chart[$d][$n].'\r'.Abreviate($chart[0][$n],2,3,TRUE);
                                                }
                                                else $charttext[$d][$n] = $carac1.round($chart[$d][$n],1).$carac2;
                                                $ddd[$d][$n];
                                            }
                                        }
                                        if ($tbinfo['tipo'] == 2){
                                            for ($n=0; $n < $ncols; $n++){
                                                /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                                pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                                if (!$dados[$n]["P".$campo['MID']]){
                                                    $dados[$n]["P".$campo['MID']]=0;
                                                }

                                                $chart[$d][$n+1] =  round($dados[$n]["P".$campo['MID']],3);
                                                if ($trocavirg) $chart[$d][$n+1] = str_replace($troca1,$troca2,$chart[$d][$n+1]);
                                                if ($ispie) {
                                                    $charttext[$d][$n+1] = $chart[$d][$n+1].'\r'.Abreviate($chart[$d][0],2,3,TRUE);
                                                }
                                                else $charttext[$d][$n+1] = $carac1.round($chart[$d][$n+1],1).$carac2;
                                                $ddd[$d][$n];
                                            }
                                        }
                                    }
                                    $resultado2->MoveNext();
                                }
                            }
    
                            $sql="SELECT * FROM ".TIPOS_SERVICOS;
                            $savestr .= "sql2b: $sql\r\n";

                            $resultado2=$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] -> Execute($sql);
                            if (!$resultado2) {
                                // Deu revestrez!
                                erromsg("{$ling['err18']}: ".$dba[$tdb[$tbinfo['coluna_mestre']]['dba']] ->ErrorMsg()."<br />SQL: $sql");
                                $xls_erro=1;
                            }
                            else {
                                //$d = 0;
                                while (!$resultado2->EOF) {
                                    $campo=$resultado2->fields;
                                    if ($mostra[$campo['MID']]){
                                        $d++;
                                        $i2++;
                                        $iim++;
                                        $x_lin++;
                                        // Descubro o nome de apresenta��o e monto ele
                                        #################$primcampo .= $campo["DESCRICAO"];
                                        $primcampo = $campo["DESCRICAO"];
                                        $chart[$d][0]=utf8_encode($primcampo);
                                        if ($ispie) {
                                            $charttext[$d][0]='';
                                        }
                                        if (strlen($primcampo) > $maiornome) {
                                            $maiornome = strlen($primcampo);
                                            $maiornomet = $primcampo;
                                        }                   

                                        // Preencho todas Colunas com os valores encontrados
                                        if ($tbinfo['tipo'] == 1){
                                            for ($n=1; $n <= $ncols; $n++){
                                                /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                                pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                                if (!$dados[$n][$campo['MID']]){
                                                    $dados[$n][$campo['MID']]=0;
                                                }
                                                $chart[$d][$n] = round($dados[$n][$campo['MID']],3);
                                                if ($trocavirg) $chart[$d][$n] = str_replace($troca1,$troca2,$chart[$d][$n]);
                                                $savestr.="chart[$d][$n+1]={{$chart[$d][$n+1]}}\r\n";
                                                if ($ispie) {
                                                    $charttext[$d][$n] = $chart[$d][$n].'\r'.Abreviate($chart[0][$n],2,3,TRUE);
                                                }
                                                else $charttext[$d][$n] = $carac1.round($chart[$d][$n],1).$carac2;
                                                $ddd[$d][$n];
                                            }
                                        }
                                        if ($tbinfo['tipo'] == 2){
                                            for ($n=0; $n < $ncols; $n++){
                                                /* Se n�o existe nada... coloco o numero ZERO, pois o espa�o ou vazio pode ser interpletado
                                                pelo excel  como string e n�o como integer, podendo causar problemas visuais no grafico. */
                                                if (!$dados[$n][$campo['MID']]){
                                                    $dados[$n][$campo['MID']]=0;
                                                }
                                                $chart[$d][$n+1] =  round($dados[$n][$campo['MID']],3);
                                                if ($trocavirg) $chart[$d][$n+1] = str_replace($troca1,$troca2,$chart[$d][$n+1]);
                                                $savestr.="chart[$d][$n+1]={{$chart[$d][$n+1]}}\r\n";
                                                if ($ispie) {
                                                    $charttext[$d][$n+1] = $chart[$d][$n+1].'\r'.Abreviate($chart[$d][0],2,3,TRUE);
                                                }
                                                else $charttext[$d][$n+1] = $carac1.round($chart[$d][$n+1],1).$carac2;
                                                $ddd[$d][$n];
                                            }
                                        }
                                    }
                                    $resultado2->MoveNext();
                                }
                            }
    
    
                            break;


                }
            }
            // Finalizo, limpo as variaveis e inicio outra tabela
            if ($tbinfo['tipo'] == 2) {
                $chart=FlipArray($chart);
                $charttext=FlipArray($charttext);
                // calcula tamanho da letra baseado no tamanho do maior nome
                // para cada 5 letras, diminui um ponto do tamanho da letra
//              $xsize = 15 - round($maiornome / 3);
                $xsize = 12;
                if ($ncols < 5) {
                    $topsize = 12;
                }
                elseif ($ncols < 9) {
                    $topsize = 10;
                }           }
                elseif ($ncols < 12) {
                    $topsize = 8;
                }           unset($ncols);
            unset($sql_filtro);
            unset($info_filtro);
            unset($info_data);
            unset($erro);
        }
    }
    $i++;
}
//print_r($chart);
//echo '---';
//print_r($charttext);

$chart2['chart_data']=$chart;
//$chart2['chart_value_text']=$ddd;
$chart2['chart_value_text']=$charttext;


########
// configura��es de apar�ncia do gr�fico
if (!$modo) {
    $modo = "3d column";
}
if ($modo == "3d column") {
    $valpos = 'over';
}
if ($modo == "3d pie") {
    $valpos = 'outside';
}
if (($modo == 'area') or ($modo == 'line')) {
    $valpos = 'above';
    if ($chart_explain) $chart_explain['y'] = 7;
}
if ($modo == "bar") {
    $valpos = 'outside';
    $chart2['chart_rect']['x']=160; 
}
$chart2['chart_type'] = $modo;
$chart2['axis_category']['orientation']='diagonal_up'; // 'diagonal_up';
$chart2['axis_category']['size']=$xsize;
$chart2['axis_value']['size']=$topsize;
$chart2['chart_value']['position']=$valpos;
$chart2['chart_value']['size']=$topsize;
$chart2['legend_label']['size']=10;
// posi��o, tamanho e rotacao
$chart2['chart_rect']['y']=75;
$chart2['chart_pref']['rotation_y']=40;

// transi��o aleat�ria: array $trans cont�m as op��es
$trans = array(drop, scale, slide_left, slide_down);
$chart2['chart_transition']['type']=$trans[rand(0,count($trans)-1)];

// textos no grafico
$titulo = utf8_encode($descricao_grafico);
$chart2['draw'][] = array (
        'type'       => "text",
        'transition' => $trans[rand(0,count($trans)-1)],
        'x'          => 30, 
        'y'          => 50, 
        'width'      => 700,  
        'h_align'    => "center", 
        'text'       => $titulo,  
        'size'       => 16, 
        'color'      => "000066", 
);

// conta quantas entradas tem na primeira linha
// se s� tem a linha das descri��es, mostra que n�o tem coluna
if ((count($chart) < 2) or (count($chart[0]) < 2))
    $chart2['draw'][] = array (
            'type'       => "text",
            'transition' => $trans[rand(0,count($trans)-1)],
            'x'          => 30, 
            'y'          => 100, 
            'width'      => 700,  
            'h_align'    => "center", 
            'text'       => $ling['prin_graf_nenhum_dado'],  
            'size'       => 16, 
            'color'      => "000066", 
    );

if ($chart_explain) $chart2['draw'][] = $chart_explain;


###
// este php ser� chamado por um swf, e portanto n�o se pode usar echo para visualizar nada
// assim, usa-se o arquivo temp.txt em modulos/principal
// d� print->temp.txt com o conte�do de $chart, celulas entre colchetes

$chartstring = "<echo>\r\n{".ob_get_contents()."}\r\n</echo>\r\n"
    .$savestr.
    "tbinfo['coluna_mestre_tipo']={$tbinfo['coluna_mestre_tipo']}\r\n
i1=$i1\r\n
i2=$i2\r\n";
foreach($chart as $chartline) {
    foreach ($chartline as $chartitem) $chartstring .= "[$chartitem] ";
    $chartstring .= "\r\n";
}
file_put_contents('temp.txt',$chartstring);

ob_end_clean();

SendChartData($chart2);
?>
