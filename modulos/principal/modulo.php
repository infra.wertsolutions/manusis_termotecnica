<?

/** 
* Modulo principal, resumo de informações, Administração de usuarios e Logs
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @author  Fernando Cosentino
* @version  3.0
* @package principal
*
*/

if (($op == 1) or ($op == "")) include("modulos/principal/resumo.php");

elseif ($op == 2) include("modulos/principal/usuarios.php");

elseif ($op == 3) include ("modulos/principal/integracao_agenda.php");

elseif ($op == 4) include('teste_conexao.php');

elseif ($op == 5) include('logs.php');



?>


