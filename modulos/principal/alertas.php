<?
/** 
* Modulo principal, alertas
* 
* @author  Fernando Cosentino
* @version  3.0
* @package manusis
* @subpackage Principal
*/

echo "<style>
.link2 {
    font-size:11px;
    text-align:left;
    color:black;
    text-decoration:none;
}
.link3 {
    font-size:10px;
    text-align:left;
    color:black;
    text-decoration:none;
}
</style>";

// FILTRO POR EMPRESA
$fil_emp = VoltaFiltroEmpresa(MATERIAIS, 2);
$fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";

$sql="SELECT MID FROM ".MATERIAIS." WHERE ESTOQUE_ATUAL <= ESTOQUE_MINIMO $fil_emp ORDER BY DESCRICAO ASC";
if (!$resmaterial= $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)){
    $err = $dba[$tdb[MATERIAIS]['dba']] -> ErrorMsg();
    erromsg("SQL ERROR .<br>$err<br><br>$sql");
    exit;
}
if (count($resmaterial-> getrows())) {
    echo "<div id=\"materiais\">
    <a class=\"link2\" ondblclick=\"return false;\" href=\"javascript:janela('modulos/principal/alertas_ajax.php?ajax=materiais')\">
    <strong><img src=\"imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_mat_ponto_compra']}</a><br /></strong>
    </div><br />";
}
else {
    echo "<strong><img src=\"imagens/icones/icon_yel_light.gif\" border=\"0\" /> {$ling['prin_mat_ponto_compra']}<br /><br /></strong>";
}

// FILTRO POR EMPRESA
$fil_emp = VoltaFiltroEmpresa(SOLICITACOES, 2);
$fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";

$sql="SELECT MID_MAQUINA FROM ".SOLICITACOES." WHERE STATUS = 0 $fil_emp ORDER BY MID_MAQUINA ASC";
if (!$ressolic= $dba[$tdb[SOLICITACOES]['dba']] -> Execute($sql)){
    $err = $dba[$tdb[SOLICITACOESS]['dba']] -> ErrorMsg();
    erromsg("SQL ERROR .<br>$err<br><br>$sql");
    exit;
}
if (count($ressolic-> getrows())) {
    echo "<div id=\"solicitacoes\">
    <a class=\"link2\" ondblclick=\"return false;\" href=\"javascript:janela('modulos/principal/alertas_ajax.php?ajax=solicitacoes')\">
    <strong><img src=\"imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_solicitacoes']}</a><br /></strong>
    </div><br />";
}
else {
    echo "<strong><img src=\"imagens/icones/icon_yel_light.gif\" border=\"0\" /> {$ling['prin_solicitacoes']}<br /><br /></strong>";
}

// FILTRO POR EMPRESA
$fil_emp = VoltaFiltroEmpresa(ORDEM, 2);
$fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";

$sql="SELECT MID_MAQUINA FROM ".ORDEM." WHERE STATUS = 1 AND DATA_PROG < '".date('Y-m-d')."' $fil_emp ORDER BY MID_MAQUINA ASC";
if (!$resordens= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
    $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
    erromsg("SQL ERROR .<br>$err<br><br>$sql");
    exit;
}
if (count($resordens-> getrows())) {
    echo "<div id=\"ordens\">
    <a class=\"link2\" ondblclick=\"return false;\" href=\"javascript:janela('modulos/principal/alertas_ajax.php?ajax=ordens')\">
    <strong><img src=\"imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_orndes_atraso']}</a><br /></strong>
    </div><br />";
}
else {
    echo "<img src=\"imagens/icones/icon_yel_light.gif\" border=\"0\" /><strong> {$ling['prin_orndes_atraso']}<br /></strong><br />";
}

?>
