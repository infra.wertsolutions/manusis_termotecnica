<?
/**
* Modulo principal, alertas chamados por ajax
* 
* @author  Fernando Cosentino
* @version  3.0
* @package manusis
* @subpackage principal
*/

// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("Imposs�vel continuar, arquivo de estrutura n�o pode ser carregado.");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("Imposs�vel continuar, arquivo de configura��o n�o pode ser carregado.");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("Imposs�vel continuar, arquivo de idioma n�o pode ser carregado.");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
if (!require("../../lib/bd.php")) die ($ling['bd01']);

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"../../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"Manusis Padr�o\" />
<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
echo "</head>
<body>";

$ajax = $_GET['ajax'];
$close = (int)$_GET['close'];

if ($ajax == 'materiais') {
    echo "<div id=\"lt\">\n
    <br clear=\"all\">
    <div id=\"lt_cab\">\n
    <h3 onclick=\"window.self.close();\" style=\"cursor:pointer;\" title=\"{$ling['clique_fechar']}\"><img src=\"../../imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_mat_ponto_compra']} </h3>
    </div>
    <br clear=\"all\">
    <div id=\"lt_tabela\">";
    
    echo "<table width=\"100%\" id=\"lt_tabela_\">
    <tr>
    <th>{$tdb[MATERIAIS]['DESCRICAO']}</th>
    <th>{$tdb[MATERIAIS]['UNIDADE']}</th>
    <th>{$tdb[MATERIAIS]['ESTOQUE_ATUAL']}</th>
    <th>{$tdb[MATERIAIS]['ESTOQUE_MINIMO']}</th>
    </tr>";
    
    $cor = "cor2";

    // FILTRO POR EMPRESA
    $fil_emp = VoltaFiltroEmpresa(MATERIAIS, 2);
    $fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";
    
    $sql="SELECT COD,DESCRICAO, ESTOQUE_ATUAL, ESTOQUE_MINIMO, UNIDADE FROM ".MATERIAIS." WHERE ESTOQUE_ATUAL <= ESTOQUE_MINIMO $fil_emp ORDER BY DESCRICAO ASC";
    if (!$resultado= $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[MATERIAIS]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $cor = ($cor == "cor1")? "cor2" : "cor1";
        echo "<tr class=\"$cor\">
        <td align=\"left\">".htmlentities($campo['COD']."-".$campo['DESCRICAO'])."</td>
        <td>" . VoltaValor(MATERIAIS_UNIDADE, 'COD', 'MID', $campo['UNIDADE']) . "</td>
        <td>" . $campo['ESTOQUE_ATUAL'] . "</td>
        <td>" . $campo['ESTOQUE_MINIMO'] . "</td>
        </tr>";
        $resultado -> MoveNext();
    }
    
    echo "</table>
    </div>
    </div>";
}
if ($ajax == 'solicitacoes') {
    echo "<div id=\"lt\">\n
    <br clear=\"all\">
    <div id=\"lt_cab\">\n
    <h3 onclick=\"window.self.close();\" style=\"cursor:pointer;\" title=\"{$ling['clique_fechar']}\"><img src=\"../../imagens/icones/icon_red_light.gif\" border=\"0\" />  {$ling['prin_solicitacoes']} </h3>
    </div>
    <br clear=\"all\">
    <div id=\"lt_tabela\">";
    
    echo "<table width=\"100%\" id=\"lt_tabela_\">
    <tr>
    <th>{$ling['quant']}</th>
    <th>{$tdb[SOLICITACOES]['MID_MAQUINA']}</th>
    </tr>";
    
    $cor = "cor2";

    // FILTRO POR EMPRESA
    $fil_emp = VoltaFiltroEmpresa(SOLICITACOES, 2);
    $fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";
    
    $sql="SELECT MID_MAQUINA FROM ".SOLICITACOES." WHERE STATUS = 0 $fil_emp ORDER BY MID_MAQUINA ASC";
    if (!$resultado= $dba[$tdb[SOLICITACOES]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[SOLICITACOESS]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $solics[$campo['MID_MAQUINA']]++;
        $maqs[$campo['MID_MAQUINA']]=$campo['MID_MAQUINA']; // para indexa��o depois no famigerado foreach
        $resultado -> MoveNext();
    }
    if (is_array($maqs)){
        foreach ($maqs as $estamaq) {
            $cor = ($cor == "cor1")? "cor2" : "cor1";
            $maqdesc = htmlentities(VoltaValor(MAQUINAS,'DESCRICAO','MID',$estamaq,0));
            echo "<tr class=\"$cor\"><td>".$solics[$estamaq]."</td><td align=\"left\">$maqdesc</td></tr   >";
        }
    }
    
    echo "</table>
    </div>
    </div>";
}
if ($ajax == 'ordens') {
    echo "<div id=\"lt\">\n
    <br clear=\"all\">
    <div id=\"lt_cab\">\n
    <h3 onclick=\"window.self.close();\" style=\"cursor:pointer;\" title=\"{$ling['clique_fechar']}\"><img src=\"../../imagens/icones/icon_red_light.gif\" border=\"0\" /> {$ling['prin_orndes_atraso']}</h3>
    </div>
    <br clear=\"all\">
    <div id=\"lt_tabela\">";
    
    echo "<table width=\"100%\" id=\"lt_tabela_\">
    <tr>
    <th>{$ling['quant']}</th>
    <th>{$tdb[ORDEM]['MID_MAQUINA']} / {$ling['prin_rota']}</th>
    </tr>";
    
    // FILTRO POR EMPRESA
    $fil_emp = VoltaFiltroEmpresa(ORDEM_PLANEJADO, 2);
    $fil_emp = ($fil_emp != '')? " AND $fil_emp" : "";
    
    $sql="SELECT MID_MAQUINA, TIPO, MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." WHERE STATUS = 1 AND DATA_PROG < '".date('Y-m-d')."' $fil_emp ORDER BY MID_MAQUINA ASC";
    if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        if($campo['MID_MAQUINA']){
            $ordens[$campo['MID_MAQUINA']]++;
            $maqs[$campo['MID_MAQUINA']]=$campo['MID_MAQUINA'];
            // para indexa��o depois no famigerado foreach
        }elseif($campo['TIPO'] == 2){
            $ordens_p[$campo['MID_PROGRAMACAO']]++;
            $progs[$campo['MID_PROGRAMACAO']]=$campo['MID_PROGRAMACAO'];
        }
        $resultado -> MoveNext();
    }
        
    $cor = "cor2";
    
    if (is_array($maqs)){
        foreach ($maqs as $estamaq) {
            $cor = ($cor == "cor1")? "cor2" : "cor1";
            $maqdesc = htmlentities(VoltaValor(MAQUINAS,'DESCRICAO','MID',$estamaq,0));
            echo "<tr class=\"$cor\"><td>".$ordens[$estamaq]."</td><td align=\"left\">$maqdesc</td></tr>";
        }
    }
    if (is_array($progs)){
        foreach ($progs as $estaprog) {
            $cor = ($cor == "cor1")? "cor2" : "cor1";
            $plan_desc = htmlentities(VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",VoltaValor(PROGRAMACAO,'MID_PLANO','MID',$estaprog,0),0));
            echo "<tr class=\"$cor\"><td>".$ordens_p[$estaprog]."</td><td align=\"left\">$plan_desc</td></tr>";
        }
    }
    
    echo "</table>
    </div>
    </div>";
}

echo "</body>
</html>";
?>
