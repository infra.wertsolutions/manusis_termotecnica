<?php


// Configurando
$reqs = array(
    'maq' => array(
        'tab' => MAQUINAS,
        'fil' => "COD, DESCRICAO, MID_SETOR",
        'ord' => 'COD',
        'reg' => 500
    ),
    'ord' => array(
        'tab' => ORDEM,
        'fil' => "NUMERO, MID_MAQUINA, TIPO_SERVICO, STATUS",
        'ord' => 'NUMERO',
        'reg' => 500
    ),
    'ord_prev' => array(
        'tab' => ORDEM_PREV,
        'fil' => "NUMERO, TAREFA, MID_ORDEM",
        'ord' => 'MID_ORDEM, NUMERO',
        'reg' => 500
    )
);


// Rodando via AJAX??
$ajax = (int) $_GET['ajax'];
$tipo = $_GET['tipo'];
if ($ajax != 0) {
    // Calculando o tempo de execu��o
    // Inicio
    list($xseg, $seg)=explode(" ", microtime());
    $ini = ((float)$xseg + (float)$seg);
    
    // Dados para processar o select
    $req_sel = $reqs[$tipo];
    
    // Vendo o total de registros
    $sql = "SELECT COUNT(MID) AS TOTAL FROM " . $req_sel['tab'] . " ";
    if (! $rs = $dba[$tdb[$req_sel['tab']]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$req_sel['tab']]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    $total_reg = (int)$rs -> fields['TOTAL'];
    
    
    // Rodando o tipo SELECIONADO
    $sql = "SELECT " . $req_sel['fil'] . " FROM " . $req_sel['tab'] . " ORDER BY " . $req_sel['ord'] . " LIMIT " . $req_sel['reg'];
    if (! $rs = $dba[$tdb[$req_sel['tab']]['dba']] -> Execute($sql)) {
        erromsg("Arquivo: " . __FILE__ . "<br  />Funcao: " . __FUNCTION__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[$req_sel['tab']]['dba']]->ErrorMsg() . "<br />" . $sql);
    }
    
    $i = 0;
    while (! $rs -> EOF) {
        $cc = $rs -> fields;
        
        // Come�ando a tabela
        if ($i == 0) {
            echo "<h2>" . $tdb[$req_sel['tab']]['DESC'] . "</h2>\n";
            echo "<table border=\"0\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\" style=\"border:1px solid #666;\">";
            echo "<tr class=\"cor1\">\n";
            
            foreach (array_keys($cc) as $campo) {
                echo "<th>" . $tdb[$req_sel['tab']][$campo] . "</th>\n";
            }
            
            echo "</tr>\n";
        }
        
        echo "<tr class=\"cor2\">\n";
        foreach ($cc as $campo => $valor) {
            $recb = VoltaRelacao($req_sel['tab'], $campo);
            if ($recb != "") {
                echo "<td>" . htmlentities(VoltaValor($recb['tb'], $recb['campo'], $recb['mid'], $valor)) . "</td>\n";
            }
            else {
                echo "<td>" . htmlentities($valor) . "</td>\n";
            }
        }
        echo "</tr>\n";
        
        $i ++;
        $rs -> MoveNext();
    }
    
    // Fechando a tabela
    echo "</table>\n";
    echo "Total de itens na tabela: $total_reg / Total de itens carregados:  $i<br />\n";
    
    // Finalizando o calculo de tempo
    list($xseg, $seg)=explode(" ", microtime());
    $fim = ((float)$xseg + (float)$seg);
    
    // Calculando o tempo
    $tt = round($fim - $ini, 3) * 1000;
    
    echo "Tempo de execu&ccedil;&atilde;o servidor: " . $tt . " ms <input type=\"hidden\" id=\"" . $tipo . "\" value=\"" . $tt . "\"><br />\n";
    
    exit();
}

// Textos do bot�o
$tex_bot1 = "Iniciar testes";
$tex_bot2 = "Rodando os testes, aguarde...";

?>


<style>

#ini_teste {
    width:200px;
}

#mostra_teste {
    margin: 0 auto;
    width:1000px;
    height:350px;
    overflow-y:scroll;
    border:1px solid #666;
    padding:5px;
}

#mostra_resultado {
    text-align:center;
}

</style>

<script>

$(function(){
    // Configura��es
    var reqs = ['<?php  echo implode("', '", array_keys($reqs)); ?>'];
    var req_time = [];
    
    // Tempo
    var time = new Date();
    var ini = null;
    var fim = null;
    
    // Textos do bot�o
    var tex_bot1 = '<?php  echo $tex_bot1; ?>';
    var tex_bot2 = '<?php  echo $tex_bot2; ?>';

    // Rodando as requisi��es
    $('#ini_teste').click(function(){
        // Valores totais para fechamento
        var total_tc = 0;
        var total_ts = 0;
    
        // Zerando o resultado
        $('#mostra_resultado').html('');
        // Zerando a DIV
        $('#mostra_teste').html('');
        
        // Mudando o texto do bot�o
        $(this).attr('value', tex_bot2);
        
        // Mostrando a div de carregando
        mostraCarregando(true);
        
        $.each(reqs, function(i, v){    
            // Separa��o
            if (i != 0) {
                $('#mostra_teste').append("<br /><hr /><br />");
            }
                  
            // Salvando o tempo e qual foi
            ini = new Date();
            
            // Enviando a requisicao
            $.ajax({
                'async' : false,
                'data' : {'tipo':v, 'ajax':1, 'st':1},
                'success' : function(data) {
                    // Criando um div para o resultado
                    $('#mostra_teste').append(data);
                }
            });
            
            // Salvando o final
            fim = new Date();
            
            // Salvando o tempo de processamento no cliente
            var tc = parseInt(fim.getTime()) - parseInt(ini.getTime());
            
            $('#mostra_teste').append('Tempo de download da p�gina: ' + tc + ' ms<br />');
            
            // Recuperando o tempo no servidor
            var ts = parseFloat($('#mostra_teste #' + v).val());
            
            
            // Mostrando o AVERAGE
            var av = tc - ts;
            $('#mostra_teste').append('Diferen�a: ' + av + ' ms');
            
            // Salvando os totais
            total_tc += tc;
            total_ts += ts;
            
        });
        
        // Mostrando os totais
        $('#mostra_resultado').append("Tempo total de download: " + total_tc + " ms / Tempo total de execu��o no servidor: " + total_ts + " ms / Diferen�a: " + (total_tc - total_ts) + " ms");
        
        
        // Mudando o texto do bot�o para o original
        $(this).attr('value', tex_bot1);
        
        // Fechando a div de carregando
        mostraCarregando(false);
    });
    
});


</script>


<br clear="all" />
<div id="lt">

<div id="lt_cab">
  <h3>Testes de conex&atilde;o</h3>
</div>

<br clear="all" />

<center><input type="button" class="botao" value="<?php  echo $tex_bot1; ?>" id="ini_teste" /></center>

<br clear="all" />
<br clear="all" />

<h2 id="mostra_resultado"></h2>
<div id="mostra_teste"></div>

</div>

