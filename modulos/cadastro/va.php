<?
/**
* Movimenta��o de Obj. de Manuten��o
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @author  Fernando Cosentino <fernando@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage cadastro
*/


$_SESSION[ManuSess]['va']['op'] = $op;
$_SESSION[ManuSess]['va']['id'] = $id;
$_SESSION[ManuSess]['va']['exe']= $exe;
$_SESSION[ManuSess]['va']['oq'] = $oq;

echo "<h3>".$ling['visao_geral']."</h3>
</div>

</div>

<br clear=\"all\" />
<div id=\"mod_va\">";


$icone_mais="<img vspace=4 hspace=4 src=\"imagens/icones/mais.gif\" alt=\" \" border=\"0\" />";
$icone_menos="<img vspace=4 hspace=4 src=\"imagens/icones/menos.gif\" alt=\" \" border=\"0\" />";
$icone_vasio="<img vspace=4 hspace=4  src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />";

// verificando quais empresas o usuario tem acesso


// BUSCANDO AS EMPRESAS CADASTRADAS
$resultado = ListaMatriz();
// CASO N�O ACHE NENHUMA
if (count($resultado) == 0) echo "<br /><br /><h3>{$ling['empresas_cadastradas']}</h3>";
else {
    // MOSTRANDO UMA ESTRUTURA POR EMPRESA
    foreach ($resultado as $campo) {
        $matrix=htmlentities($campo['cod'] . "-" . $campo['nome']);
        $matrix_cod=$campo['cod'];
        $matrix_mid=$campo['mid'];

        $listaarea=ListaArea($matrix_mid);
        if ($listaarea == 0) echo "<div id=\"matrix_$matrix_mid\"><img src=\"imagens/icones/22x22/empresa.png\" alt=\" \" /> $matrix</div>\n";
        else {
            echo "<div id=\"matrix_$matrix_mid\"><a onclick=\"FecharEmpresa('matrix_$matrix_mid')\">$icone_menos</a>
            <img src=\"imagens/icones/22x22/empresa.png\" alt=\" \" /><a ondblclick=\"FecharEmpresa('matrix_$matrix_mid')\"> $matrix</a><ul style=\"display:block\">\n";
            for ($ifs=0; $listaarea[$ifs]['nome'] != ""; $ifs++) {
                $area=htmlentities(strtoupper($listaarea[$ifs]['nome']));
                $area_mid=$listaarea[$ifs]['mid'];
                $area_cod=strtoupper($listaarea[$ifs]['cod']);
                $listasetor=ListaSetor($area_mid);
                // Caso nao tenha setores por icone nulo na visao
                if ($listasetor == 0) echo "<li>$icone_vasio <img src=\"imagens/icones/22x22/vazio.png\" alt=\"\" />
                    <a  ondblclick=\"abre_janela_form('AREA','$area_mid','','2','$id','$exe','$op',".$form['AREA'][0]['altura'].",".$form['AREA'][0]['largura'].",'corpo','')\">$area_cod - $area</a>
                    </li>\n";
                else {
                    echo "<li>
                    <div id=\"area_$area_mid\">
                    <a onclick=\"FecharArea('area_$area_mid')\">$icone_mais</a>
                    <img src=\"imagens/icones/22x22/local1.png\" alt=\"\" />
                    <a  ondblclick=\"abre_janela_form('AREA','$area_mid','','2','$id','$exe','$op',".$form['AREA'][0]['altura'].",".$form['AREA'][0]['largura'].",'corpo','')\">$area_cod - $area</a>
                    <ul>\n";
                    for ($is=0; $listasetor[$is]['nome'] != ""; $is++) {
                        $setor=htmlentities(strtoupper($listasetor[$is]['nome']));
                        $setor_cod=strtoupper($listasetor[$is]['cod']);
                        $setor_mid=$listasetor[$is]['mid'];
                        $listamaq=ListaMaquina($setor_mid);
                        if ($listamaq == 0) echo "<li>
                        $icone_vasio
                            <img src=\"imagens/icones/22x22/vazio.png\" id=\"isetor_$setor_mid\" alt=\"\" />
                            <a ondblclick=\"abre_janela_form('SETOR','$setor_mid','','2','$id','$exe','$op',".$form['SETOR'][0]['altura'].",".$form['SETOR'][0]['largura'].",'corpo','')\">$setor_cod - $setor</a>
                            </li>\n";
                        else {
                            echo "<li>
                            <img vspace=4 hspace=4 src=\"imagens/icones/mais.gif\" onclick=\"FecharSetor('$setor_mid',this);\" alt=\" \" border=\"0\" />
                            <img src=\"imagens/icones/22x22/local2.png\" id=\"isetor_$setor_mid\" alt=\"\" />
                            <a  ondblclick=\"abre_janela_form('SETOR','$setor_mid','','2','$id','$exe','$op',".$form['SETOR'][0]['altura'].",".$form['SETOR'][0]['largura'].",'corpo','')\">$setor_cod - $setor</a>
                            <ul id=\"setor_$setor_mid\"></ul>\n";

                        }
                    }
                    // Setor
                    echo "</ul></div></li>\n";
                }
            }

            echo "<li>
            </ul></div></li>\n";
        }
    }

    echo "<hr />";

    $listamaq=ListaMaquina(0);
    if ($listamaq == 0){
        echo "<li>
        $icone_vasio
            <img src=\"imagens/icones/22x22/vazio.png\" alt=\"\" />
            <a>".$ling['maq_nao_alocadas']."</a>
            </li>\n";
    }
    else {
        echo "<li>
        <div id=\"area_a\">
        <a onclick=\"FecharArea('area_a')\">$icone_mais</a>
        <img src=\"imagens/icones/22x22/local1.png\" alt=\"\" />
        <a>".$ling['maq_nao_alocadas']."</a>
        <ul>\n";
        for ($im=0; $listamaq[$im]['nome'] != ""; $im++) {
            $maquina=htmlentities(strtoupper($listamaq[$im]['nome']));
            $maquina_cod=strtoupper($listamaq[$im]['cod']);
            $maquina_mid=$listamaq[$im]['mid'];
            if ($listamaq[$im]['modelo']){
                $maquina_modelo="[".htmlentities(strtoupper($listamaq[$im]['modelo']))."]";
            }
            else {
                $maquina_modelo="";
            }


            $listaconj=ListaConjunto($maquina_mid);
            if ($listaconj == 0) {
                echo "<li>$icone_vasio<img src=\"imagens/icones/22x22/vazio.png\" alt=\"\" />";
                
                if ($listamaq[$im]['status'] == 2) echo "<a style=\"color:red\" title=\"".$ling['maq_inativa']."\" ondblclick=\"abre_janela_form('MAQUINA','$maquina_mid','','2','$id','$exe','$op',".$form['MAQUINA'][0]['altura'].",".$form['MAQUINA'][0]['largura'].",'corpo','')\">$maquina_cod - $maquina $maquina_modelo</a>";
                else echo "<a ondblclick=\"abre_janela_form('MAQUINA','$maquina_mid','','2','$id','$exe','$op',".$form['MAQUINA'][0]['altura'].",".$form['MAQUINA'][0]['largura'].",'corpo','')\">$maquina_cod - $maquina $maquina_modelo</a>";
                
                echo "</li>\n";
            }
            else {
                echo "<li>
                <div>
                <img vspace=4 hspace=4 src=\"imagens/icones/mais.gif\" onclick=\"FecharMaquina('$maquina_mid',this);\" alt=\" \" border=\"0\" />
                <img id=\"imaq_$maquina_mid\" src=\"imagens/icones/22x22/local3.png\" alt=\"\" />
                <a  ondblclick=\"abre_janela_form('MAQUINA','$maquina_mid','','2','$id','$exe','$op',".$form['MAQUINA'][0]['altura'].",".$form['MAQUINA'][0]['largura'].",'corpo','')\">$maquina_cod - $maquina $maquina_modelo</a>
                <ul id=\"maq_$maquina_mid\">\n";
                
                for ($ic=0; $listaconj[$ic]['nome'] != ""; $ic++) {
                    $conjunto=htmlentities(strtoupper($listaconj[$ic]['nome']));
                    $conjunto_tag=strtoupper($listaconj[$ic]['tag']);
                    $conjunto_mid=$listaconj[$ic]['mid'];
                    $listaequip=ListaEquipamento($conjunto_mid);
                    $listasubconj=ListaSubConjunto($conjunto_mid);
                    if (($listaequip == 0) and ($listasubconj == 0)){ 
                        echo "<li>
                        $icone_vasio
                        <img src=\"imagens/icones/22x22/tag_vazio.png\" alt=\"\" />
                        <a  ondblclick=\"abre_janela_form('CONJUNTO','$conjunto_mid','','2','$id','$exe','$op',".$form['CONJUNTO'][0]['altura'].",".$form['CONJUNTO'][0]['largura'].",'corpo','')\">$conjunto_tag - $conjunto</a>
                        </li>\n";
                    }
                    else {
                        echo "<li>
                        <div id=\"conj2_$conjunto_mid\">
                        <a onclick=\"FecharConj('conj2_$conjunto_mid')\">$icone_mais</a>
                        <img src=\"imagens/icones/22x22/tag.png\" alt=\"\" />
                        <a  ondblclick=\"abre_janela_form('CONJUNTO','$conjunto_mid','','2','$id','$exe','$op',".$form['CONJUNTO'][0]['altura'].",".$form['CONJUNTO'][0]['largura'].",'corpo','')\">$conjunto_tag - $conjunto</a>
                        <ul>\n";
                        
                        for ($ie=0; $listaequip[$ie]['nome'] != ""; $ie++) {
                            $equipamento=htmlentities(strtoupper($listaequip[$ie]['nome']));
                            $equipamento_cod=strtoupper($listaequip[$ie]['cod']);
                            $equipamento_mid=$listaequip[$ie]['mid'];
                            echo "<li><div id=\"equip_$equipamento_mid\">
                            $icone_vasio
                                                <img src=\"imagens/icones/22x22/equip.png\" alt=\"\" />
                                                <a  ondblclick=\"abre_janela_form('EQUIPAMENTO','$equipamento_mid','','2','$id','$exe','$op',".$form['EQUIPAMENTO'][0]['altura'].",".$form['EQUIPAMENTO'][0]['largura'].",'corpo','')\">$equipamento_cod - $equipamento</a>
                                                </div></li>
                                                \n";
                        }

                        for ($ics=0; $listasubconj[$ics]['nome'] != ""; $ics++) {
                            $sconjunto=htmlentities(strtoupper($listasubconj[$ics]['nome']));
                            $sconjunto_tag=strtoupper($listasubconj[$ics]['tag']);
                            $sconjunto_mid=$listasubconj[$ics]['mid'];
                            $listaequip=ListaEquipamento($sconjunto_mid);
                            if ($listaequip == 0)  echo "<li>
                            $icone_vasio
                                            <img src=\"imagens/icones/22x22/tag_vazio.png\" alt=\"\" />
                                            <a  ondblclick=\"abre_janela_form('CONJUNTO','$sconjunto_mid','','2','$id','$exe','$op',".$form['CONJUNTO'][0]['altura'].",".$form['CONJUNTO'][0]['largura'].",'corpo','')\">$sconjunto_tag - $sconjunto</a>
                                            </li>\n";
                            else {
                                echo "<li>
                                            <div id=\"sconj2_$sconjunto_mid\">
                                            <a onclick=\"FecharSConj('sconj2_$sconjunto_mid')\">$icone_mais</a>
                                            <img src=\"imagens/icones/22x22/tag.png\" alt=\"\" />
                                            <a  ondblclick=\"abre_janela_form('CONJUNTO','$sconjunto_mid','','2','$id','$exe','$op',".$form['CONJUNTO'][0]['altura'].",".$form['CONJUNTO'][0]['largura'].",'corpo','')\">$sconjunto_tag - $sconjunto</a>
                                            <ul>\n";
                                for ($ie=0; $listaequip[$ie]['nome'] != ""; $ie++) {
                                    $equipamento=htmlentities(strtoupper($listaequip[$ie]['nome']));
                                    $equipamento_cod=strtoupper($listaequip[$ie]['cod']);
                                    $equipamento_mid=$listaequip[$ie]['mid'];
                                    echo "<li><div id=\"equip_$equipamento_mid\">
                                    $icone_vasio
                                                <img src=\"imagens/icones/22x22/equip.png\" alt=\"\" />
                                                <a  ondblclick=\"abre_janela_form('EQUIPAMENTO','$equipamento_mid','','2','$id','$exe','$op',".$form['EQUIPAMENTO'][0]['altura'].",".$form['EQUIPAMENTO'][0]['largura'].",'corpo','')\">$equipamento_cod - $equipamento</a>
                                                </div></li>
                                                \n";
                                }
                                // Equipamento SUB CONJUNTO
                                echo "</ul></div></li>\n";
                            }
                            // Equipamento SUB CONJUNTO
                        }
                        // Equipamento SUB CONJUNTO
                        echo "</ul></div></li>\n";
                    }
                }
                // Conjunto
                echo "</ul></div></li>\n";
            }
        }
        // Maquina n�o alocada
        echo "</ul></div></li>\n";
    }



    $listaequip=ListaEquipamento(0);
    if ($listaequip == 0){
        echo "<li>
        $icone_vasio
            <img src=\"imagens/icones/22x22/vazio.png\" alt=\"\" />
            <a>".$ling['equip_nao_alocadas']."</a>
            </li>\n";
    }
    else {
        echo "<li>
        <div id=\"area_b\">
        <a onclick=\"FecharArea('area_b')\">$icone_mais</a>
        <img src=\"imagens/icones/22x22/local1.png\" alt=\"\" />
        <a>".$ling['equip_nao_alocadas']."</a>
        <ul>\n";

        for ($ie=0; $listaequip[$ie]['nome'] != ""; $ie++) {
            $equipamento=htmlentities(strtoupper($listaequip[$ie]['nome']));
            $equipamento_cod=strtoupper($listaequip[$ie]['cod']);
            $equipamento_mid=$listaequip[$ie]['mid'];
            echo "<li><div id=\"equip_$equipamento_mid\">
            $icone_vasio
                <img src=\"imagens/icones/22x22/equip.png\" alt=\"\" />
                <a  ondblclick=\"abre_janela_form('EQUIPAMENTO','$equipamento_mid','','2','$id','$exe','$op',".$form['EQUIPAMENTO'][0]['altura'].",".$form['EQUIPAMENTO'][0]['largura'].",'corpo','')\">$equipamento_cod - $equipamento</a>
                </div></li>\n";
        }

        echo "</ul></div></li>\n";
    }
    // Equipamento n�o alocado
}
echo "</ul></div>";
?>