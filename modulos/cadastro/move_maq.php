<?
/**
* Movimenta��o de Obj. de Manuten��o;
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage cadastro
*/
$destino  = (int)$_GET['destino'];
$destino1 = (int)$_GET['destino1'];
$objeto = (int)$_GET['objeto'];
$motivo = mb_strtoupper(LimpaTexto($_GET['motivo']));



echo "<h3>".$ling['move_maq']."</h3>
</div>
</div>
<br clear=\"all\" />
<div id=\"lt\"><br />";
if (($_GET['enviar'] != "") and ($objeto != "")) {
    $err=0;
    if ($_GET['motivo'] == "") {
        erromsg("".$ling['campo_motivo_obrigatori']."");
        $err =1;
    }
    if($err!=1){
        $opc = VoltaValor(MAQUINAS, "MID_SETOR", "MID", $objeto, $tdb[MAQUINAS]['dba']);
        
        // Novo Loc 1
        $area = (int)VoltaValor(SETORES, "MID_AREA", "MID", $destino1, 0);
        
        // Nova Empresa
        $emp = (int)VoltaValor(AREAS, "MID_EMPRESA", "MID", $area, 0);
        
        // Muda se for para n�o alocados
        if ($emp != 0) {
            $emp_sql = ", MID_EMPRESA = '$emp'";
        }
        
        $sql = "UPDATE ".MAQUINAS." SET MID_SETOR = '$destino1' $emp_sql WHERE MID = '$objeto'";
        if (! $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)) {
            erromsg($dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg() . "<br />" . $sql);
        }
        else {
            logar(4, "Movimentando Obj. de Manuten��o", MAQUINAS, 'MID', $objeto);
            
            $mid=GeraMid(MOV_MAQUINA,"MID",$tdb[MOV_MAQUINA]['dba']);
            $sql = "INSERT INTO ".MOV_MAQUINA." (MID_MAQUINA, MID_SETOR, MID_DSETOR, DATA, MOTIVO, MID) VALUES ('$objeto', '$opc', '$destino1', '".date("Y-m-d")."', '$motivo', '$mid')";
            if(! $dba[$tdb[MOV_MAQUINA]['dba']] ->Execute($sql)){
                erromsg($dba[$tdb[MOV_MAQUINA]['dba']]->ErrorMsg() . "<br />" . $sql);
            }
            else {
                logar(1, "", MOV_MAQUINA, 'MID', $mid);
            }
            
            $sql = "UPDATE ".ORDEM_PLANEJADO." SET MID_SETOR = '$destino1', MID_AREA = '$area' $emp_sql WHERE STATUS = 1 AND MID_MAQUINA = '$objeto'";
            if(! $dba[$tdb[ORDEM_PLANEJADO]['dba']] ->Execute($sql)){
                erromsg($dba[$tdb[ORDEM_PLANEJADO]['dba']]->ErrorMsg() . "<br />" . $sql);
            }


            echo "<h2 align=\"center\">".$ling['maq_mover_sucess'].". <br> ".$ling['ordens_serv_abertas_obj_atulizadas']."</h2><br />";
        }
    }
    $_GET['destino1']="";
    $_GET['motivo']="";
}

echo "
<form action=\"manusis.php\" name=\"movemaq\" method=\"get\">
<input type=\"hidden\" name=\"id\" value=\"$id\">
<input type=\"hidden\" name=\"op\" value=\"$op\">
<input type=\"hidden\" name=\"exe\" value=\"$exe\">
<p>".$ling['selecionar_maq'].":<br>";

FormSelectD("COD","DESCRICAO",MAQUINAS,$_GET['objeto'],"objeto","objeto","MID",0,"","document.movemaq.submit()","","S","COD","");

if ($_GET['objeto'] != "") {
    $resultado=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MODELO,MID_SETOR,MID FROM ".MAQUINAS." WHERE MID = ".$_GET['objeto']);
    $campo = $resultado -> fields;
    echo "<div id=\"lt_tabela\"><br />
    <table>
    <tr>
    <th>".$tdb[MAQUINAS]['COD']."</th>
    <th>".$tdb[MAQUINAS]['DESCRICAO']."</th>
    <th>".$tdb[MAQUINAS]['MODELO']."</th>
    <th>".$tdb[MAQUINAS]['MID_SETOR']."</th>
    </tr>
    <tr class=\"cor2\">";
    $tmp_setor=VoltaValor(SETORES,'DESCRICAO','MID',$campo['MID_SETOR'],$tdb[SETORES]['dba']);
    echo "<td>".$campo['COD']."</td><td>".$campo['DESCRICAO']."</td><td>".$campo['MODELO']."</td><td>$tmp_setor</td></tr>
    </table></div><br />";              
    echo "<p>".$ling['selecionar_destino'].":<br>";
    
    FormSelectD("COD", "DESCRICAO", SETORES, $_GET['destino1'], "destino1", "destino1", "MID", 0, "campo_select_ob", "", "", "S", "COD", $ling['maq_nao_alocadas'], "MID_AREA");

    echo " <br /><br />
    <p>".$ling['motivo'].":<br />
    <input type=\"text\" class=\"campo_text_ob\" size=\"100\" name=\"motivo\"><br /><br /></p>\n
    <input type=\"submit\" class=\"botao\" name=\"enviar\" value=\"".$ling['movimentar']."\" /><br><br>";   

    echo "<input type=\"hidden\" name=\"id\" value=\"$id\" />
<input type=\"hidden\" name=\"oq\" value=\"$oq\" />

</td></tr></table></form> ";

}
?>
