<?
/**
 * Detalhamento do equipamento, op��o do copiar um equipamento inteiro, link de pe�as no equipamento e anexar arquivos
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @version  3.0
 * @package cadastro
 * @subpackage  estrutura
 */


$ulstyle="display:block;list-style-type:none";
$icone_mais = "<img src=\"imagens/icones/mais.gif\" alt=\" \" border=\"0\" />";
$icone_menos = "<img src=\"imagens/icones/menos.gif\" alt=\" \" border=\"0\" />";
$icone_vasio = "<img src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />";

function MostraMaterial($mid, $tabela, $campolink = "MID_CONJUNTO") {
    global $dba,$tdb, $ulstyle, $icone_menos,$icone_vasio;
    $sql = "SELECT * FROM ".$tabela." WHERE MID = '$mid' ORDER BY MID ASC";
    $resultado=$dba[$tdb[$tabela]['dba']] -> Execute($sql);
    $i2 = 0;
    if (!$resultado->EOF) {
        $campo=$resultado->fields;
        $mat_mid = (int)VoltaValor($tabela,'MID_MATERIAL','MID',$mid,0);
        $link = (int)VoltaValor($tabela,$campolink,'MID',$mid,0);
        $mat_desc = htmlentities(VoltaValor(MATERIAIS,'DESCRICAO','MID',$mat_mid,0));
        
        $classe = htmlentities(VoltaValor(MATERIAIS_CLASSE, 'DESCRICAO', 'MID', $campo['MID_CLASSE'], 0));
        $classe = ($classe != '')? "($classe)" : "";

        $tmp = "<li><div id=\"mat_$mid\" /> $icone_vasio
             <img src=\"imagens/icones/22x22/material.png\" alt=\"\" /> $mat_desc $classe";
        $tmp .= "</div></li>";
        $i2++;
    }
    if ($i2 != 0) return $tmp;
    else return '';
}

/**
 * Vis�o em Arvore baseada no equipamento, mostra todas localiza��es at� chegar no equipamento definido
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @param integer $mid_maq
 * @param integer $mid_conj
 * @param integer $mid_equip
 */
function VisaoEquip($mid_maq,$mid_conj,$mid_equip) {
    global $dba,$tdb,$ling,$op,$oq,$id,$exe,$form;
    $resultado=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID,MID_SETOR,MODELO FROM ".MAQUINAS." WHERE MID = '$mid_maq'");
    $icone_mais="<img src=\"imagens/icones/mais.gif\" alt=\" \" border=\"0\" />";
    $icone_menos="<img src=\"imagens/icones/menos.gif\" alt=\" \" border=\"0\" />";
    $icone_vasio="<img src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />";
    if (!$resultado) erromsg($dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg());
    else {
        $maq_cod=$resultado->fields['COD'];
        $maq_des=$resultado->fields['DESCRICAO'];
        $maq_mod=$resultado->fields['MODELO'];
        $maq_set=$resultado->fields['MID_SETOR'];
        $resultado=$dba[$tdb[SETORES]['dba']] -> Execute("SELECT COD,DESCRICAO,MID_AREA FROM ".SETORES." WHERE MID = '$maq_set'");
        if (!$resultado) erromsg($dba[$tdb[SETORES]['dba']]  -> ErrorMsg());
        else {
            $set_cod=$resultado->fields['COD'];
            $set_des=$resultado->fields['DESCRICAO'];
            $area_des=VoltaValor(AREAS,"DESCRICAO","MID",$resultado->fields['MID_AREA'],$tdb[AREAS]['dba']);
            $area_cod=VoltaValor(AREAS,"COD","MID",$resultado->fields['MID_AREA'],$tdb[AREAS]['dba']);
        }
    }
    if ($maq_cod != "") {
        echo "\n<div id=\"mod_va\">\n";
        if ($area_des != "") echo "<div id=\"area_$area_cod\">\n<a onclick=\"FecharArea('area_$area_cod')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/local1_abre.png\" alt=\"\" /> $area_cod - $area_des\n<ul style=\"display:block\"><li>\n";
        if ($set_des != "") echo "<div id=\"setor_$set_cod\">\n<a onclick=\"FecharSetor('setor_$set_cod')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/local2_abre.png\" alt=\"\" /> $set_cod - $set_des\n<ul style=\"display:block\"><li>\n";
        echo "<div id=\"maq_$mid\">\n<a onclick=\"FecharMaquina('maq_$mid')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/local3_abre.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('MAQUINA','$mid_maq','$oq','2','$id','$exe','$op',".$form['MAQUINA'][0]['altura'].",".$form['MAQUINA'][0]['largura'].",'corpo','')\"> $maq_cod - $maq_des </a>\n<ul style=\"display:block\">\n";
        $resultado=$dba[$tdb[MAQUINAS_CONJUNTO]['dba']] -> Execute("SELECT TAG,DESCRICAO,MID FROM ".MAQUINAS_CONJUNTO." WHERE MID = '$mid_conj'");
        if (!$resultado) erromsg($dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg());
        else {
            $conjunto_tag=$resultado->fields['TAG'];
            $conjunto=$resultado->fields['DESCRICAO'];
            echo "<li><div id=\"conj_$mid_conj\"><a onclick=\"FecharConj('conj_$mid_conj')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/tag.png\" alt=\"\" /><a  ondblclick=\"abre_janela_form('CONJUNTO','$mid_conj','$oq','2','$id','$exe','$op',".$form['CONJUNTO'][0]['altura'].",".$form['CONJUNTO'][0]['largura'].",'corpo','')\"> $conjunto_tag - $conjunto</a><ul style=\"display:block\">\n";
            $resultado=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".EQUIPAMENTOS." WHERE MID = '$mid_equip'");
            if (!$resultado) erromsg($dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg());
            else {
                $equipamento_cod=$resultado->fields['COD'];
                $equipamento=$resultado->fields['DESCRICAO'];
                echo "<li><div id=\"equip_$mid_equip\"><a onclick=\"FecharEquip('equip_$mid_equip')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/equip.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('EQUIPAMENTO','$mid_equip','$oq','2','$id','$exe','$op',".$form['EQUIPAMENTO'][0]['altura'].",".$form['EQUIPAMENTO'][0]['largura'].",'corpo','')\"> $equipamento_cod - $equipamento</a>\n<ul style=\"display:block\">\n";

                $sql2 = "SELECT MID FROM ".EQUIPAMENTOS_MATERIAL." WHERE MID_EQUIPAMENTO = '$mid_equip' ORDER BY MID ASC";
                $res4=$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> Execute($sql2);
                while (!$res4->EOF) {
                    $campo2=$res4->fields;
                    if ($campo2['MID']) echo MostraMaterial($campo2['MID'],EQUIPAMENTOS_MATERIAL,"MID_EQUIPAMENTO");
                    $res4->MoveNext();
                }
                $res4=$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute("SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_EQUIPAMENTO = '$mid_equip' ORDER BY PONTO ASC");
                while (!$res4->EOF) {
                    $campo2=$res4->fields;
                    echo "<li><div id=\"pred_{$campo2['MID']}\">$icone_vasio    <img src=\"imagens/icones/22x22/preditiva.png\" alt=\"\" /><a> {$campo2['PONTO']} ({$campo2['GRANDEZA']})</a>\n</div></li>\n";
                    $res4->MoveNext();
                }

                echo "</ul></div></li>";
            }
        }
        echo "</ul></div></li>";
        echo "</ul></div></li>";
        echo "</ul></div></li>";
        echo "</ul></div></li>\n</div>";
    }
    else echo "<br /><br /><hr /><h3 align=\"center\"> ".$ling['err07']."</h3><hr /><br clear=\"all\">";

}

$oq = (int) $_GET['oq'];

$conjunto_mid = VoltaValor(EQUIPAMENTOS,"MID_CONJUNTO","MID",$oq,$tdb[EQUIPAMENTOS]['dba']);
$maquina_mid = VoltaValor(MAQUINAS_CONJUNTO,"MID_MAQUINA","MID",$conjunto_mid,$tdb[MAQUINAS_CONJUNTO]['dba']);
$equipamento = htmlentities(VoltaValor(EQUIPAMENTOS,"DESCRICAO","MID",$oq,$tdb[EQUIPAMENTOS]['dba']));

$permissao1 = (VoltaPermissao($id, $op) == 1)? 1 : 0;
$permissao2 = ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2))? 1 : 0;

echo "<div id=\"mod_menu\">
<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/va.png\" border=\"0\" alt=\"".$ling['visa_geral']."\" />
<span>".$ling['visao_geral']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=5\">
<img src=\"imagens/icones/22x22/equip.png\" border=\"0\" alt=\"".$ling['tabela_equip3']."\" />
<span>".$ling['tabela_equip']."</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=7&oq=$oq\">
<img src=\"imagens/icones/22x22/engrenagem.png\" border=\"0\" alt=\"".$ling['detalha']."\" />
<span>".$ling['detalha']."</span>
</a>
</div>
";

if ($permissao2) echo "<div>
<a href=\"javascript:abre_janela_form('EQUIPAMENTO','$oq','$oq','2','$id','$exe','$op',".$form['EQUIPAMENTO'][0]['altura'].",".$form['EQUIPAMENTO'][0]['largura'].",'')\">
<img src=\"imagens/icones/22x22/equip_editar.png\" border=\"0\" alt=\"".$ling['editar_equip']."\" />
<span>".$ling['editar_equip']."</span>
</a>
</div>
";

echo "<div>
<a href=\"manusis.php?id=$id&op=$op&exe=73&oq=$oq\">
<img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" alt=\"" . $ling['tabela_preditiva'] . "\" />
<span>" . $ling['tabela_preditiva'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=71&oq=$oq\">
<img src=\"imagens/icones/22x22/material.png\" border=\"0\" alt=\"".$ling['pecas_equip']."\" />
<span>".$ling['pecas_equip']."</span>
</a>
</div>

<div>
<a href=\"javascript:abre_janela_arq('equipamentos','$oq', $id)\">
<img src=\"imagens/icones/22x22/anexar.png\" border=\"0\" alt=\"".$ling['anexar_arq']."\" />
<span>".$ling['anexar_arq']."</span>
</a>
</div>
";

if ($permissao2) echo "<div>
<a href=\"manusis.php?id=$id&op=$op&exe=72&oq=$oq\" onclick=\"return confirma(this, '".$ling['deseja_copiar_equip']."')\">
<img src=\"imagens/icones/22x22/copiar.png\" border=\"0\" alt=\"".$ling['copiar']."\" />
<span>".$ling['copiar']."</span>
</a>
</div>";

echo "<div>";

if ($exe == 7) {
    echo "<h3>$equipamento</h3>
</div>
</div>
<br clear=\"all\" /><div>";
    VisaoEquip($maquina_mid,$conjunto_mid,$oq);
}

if ($exe == 71) {
    echo "<h3>$equipamento</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    
    $campos = array();
    $campos[]="MID_MATERIAL";
    $campos[]="QUANTIDADE";
    $campos[]="MID_CLASSE";
    

    ListaTabela(EQUIPAMENTOS_MATERIAL,"MID",$campos,"EQUIPAMENTO_MATERIAL","MID_EQUIPAMENTO",$oq,1,1,1,1,1,1,1,1,1,1,"");
    echo "</div>";
}
if (($exe == 72) and ($oq != "") and ($_GET['confirma'] == 1)) {
    $rmaq=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute("SELECT * FROM ".EQUIPAMENTOS." WHERE MID = '$oq'");
    if (!$rmaq) erromsg($dba[$tdb[EQUIPAMENTOS]['dba']] -> ErrorMsg());
    else {
        $equip=$rmaq->fields;
        $equip_mid=GeraMid(EQUIPAMENTOS,"MID",$tdb[EQUIPAMENTOS]['dba']);
        $equip_ncod = GeraCodComponente($equip['COD'],$equip_mid);
        $equip_insere=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute ("INSERT INTO ".EQUIPAMENTOS." (MID_MAQUINA,MID_CONJUNTO,COD,DESCRICAO,FAMILIA,FABRICANTE,FORNECEDOR,FICHA_TECNICA,MARCA,MODELO,NSERIE,POTENCIA,PRESSAO,CORRENTE,VAZAO,TENSAO,MID) VALUES ('".$equip['MID_MAQUINA']."','".$equip['MID_CONJUNTO']."','$equip_ncod','".$equip['DESCRICAO']."','".$equip['FAMILIA']."','".$equip['FABRICANTE']."','".$equip['FORNECEDOR']."','".$equip['FICHA_TECNICA']."','".$equip['MARCA']."','".$equip['MODELO']."','".$equip['NSERIE']."','".$equip['POTENCIA']."','".$equip['PRESSAO']."','".$equip['CORRENTE']."','".$equip['VAZAO']."','".$equip['TENSAO']."','$equip_mid')");
        if (!$equip_insere) erromsg("".$dba[$tdb[EQUIPAMENTOS]['dba']] -> ErrorMsg());
        $resultado4=$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> Execute("SELECT * FROM ".EQUIPAMENTOS_MATERIAL." WHERE MID_EQUIPAMENTO = '$oq'");
        $ip=0;
        while (!$resultado4->EOF) {
            $mat=$resultado4->fields;
            $mat_mid=GeraMid(EQUIPAMENTOS_MATERIAL,"MID",$tdb[EQUIPAMENTOS_MATERIAL]['dba']);
            $mat_insere=$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> Execute ("INSERT INTO ".EQUIPAMENTOS_MATERIAL." (MID_EQUIPAMENTO, MID_MATERIAL, QUANTIDADE, MID) VALUES ('$equip_mid','".$mat['MID_MATERIAL']."','".$mat['QUANTIDADE']."','$mat_mid')");
            if (!$mat_insere) erromsg("".$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> ErrorMsg());
            $resultado4->MoveNext();
            $ip++;
        }
        $resultado5=$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute("SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_EQUIPAMENTO = '$oq'");
        $ip=0;
        while (!$resultado5->EOF) {
            $pre=$resultado5->fields;
            $pre_mid=GeraMid(PONTOS_PREDITIVA,"MID",$tdb[PONTOS_PREDITIVA]['dba']);
            $pre_ins=$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute ("INSERT INTO ".PONTOS_PREDITIVA." (MID_EQUIPAMENTO, PONTO, GRANDEZA, VALOR_MINIMO, VALOR_MAXIMO, OBSERVACAO, MID) VALUES ('$equip_mid','".$pre['PONTO']."','".$pre['GRANDEZA']."','".$pre['VALOR_MINIMO']."','".$pre['VALOR_MAXIMO']."','".$pre['OBSERVACAO']."','$pre_mid')");
            if (!$pre_ins) erromsg("".$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> ErrorMsg());
            $resultado5->MoveNext();
            $ip++;
        }

        echo "<h3>$equipamento</h3>
</div>
</div>
<br clear=\"all\" />
<div><br /><br /><p align=\"center\"><h2> ".$ling['objeto_copiado']." </h2><br /> <br /><a href=\"manusis.php?id=$id&op=$op&exe=7&oq=$equip_mid\">".$ling['clique_acessar_item']."</a><br /> <br /><br /> <br /></p></div>";
    }
}

if ($exe == 73) {
    echo "<h3>" . htmlentities($equipamento) . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos[0] = "MID_EQUIPAMENTO";
    $campos[1] = "PONTO";
    $campos[2] = "GRANDEZA";
    $campos[3] = "VALOR_MINIMO";
    $campos[4] = "VALOR_MAXIMO";
    $campos[5] = "OBSERVACAO";
    ListaTabela(PONTOS_PREDITIVA, "MID", $campos, "PONTO_PREDITIVA_EQUIP", "MID_EQUIPAMENTO", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}

?>
