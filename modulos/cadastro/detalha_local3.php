 <?php 
/**
* Detalhamento do objeto de manuten��o, op��o do copiar, posi��es, link de pe�as, pontos de lubrifica��o, anexar arquivos
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @author  Fernando Cosentino <fernando@manusis.com.br>
* @version  3.0
* @package cadastro
* @subpackage  estrutura
*/


$ulstyle="display:block;list-style-type:none";
$icone_mais = "<img src=\"imagens/icones/mais.gif\" alt=\" \" border=\"0\" />";
$icone_menos = "<img src=\"imagens/icones/menos.gif\" alt=\" \" border=\"0\" />";
$icone_vasio = "<img src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />";


$oq=(int)$_GET['oq'];

function MostraMaterial($mid, $tabela, $campolink = "MID_CONJUNTO") {
    global $dba,$tdb, $ulstyle, $icone_menos,$icone_vasio;
    //die("[$mid]");
    $sql = "SELECT * FROM ".$tabela." WHERE MID = '$mid' ORDER BY MID ASC";
    $resultado=$dba[$tdb[$tabela]['dba']] -> Execute($sql);
    $i2 = 0;
    if (!$resultado->EOF) {
        $campo=$resultado->fields;
        //die("$sql");
        $mat_mid = (int)VoltaValor($tabela,'MID_MATERIAL','MID',$mid,0);
        $link = (int)VoltaValor($tabela,$campolink,'MID',$mid,0);
        $mat_desc = htmlentities(VoltaValor(MATERIAIS,'DESCRICAO','MID',$mat_mid,0));
        $sql2 = "SELECT MID FROM ".$tabela." WHERE $campolink = '$link' AND MID_MATERIAL_PAI = '$mat_mid' ORDER BY MID ASC";
        //die($sql2);
        $res2=$dba[$tdb[$tabela]['dba']] -> Execute($sql2);
        if (!$res2) die('erro: '.$dba[0] -> ErrorMsg());
        $j=0;
        while (!$res2->EOF) {
            $j++;
            $res2->MoveNext();
        }
        if ($j) {
            $res2=$dba[$tdb[$tabela]['dba']] -> Execute($sql2);
            $tmp = "<li><div id=\"mat_$mid\" />
            <a onclick=\"FecharMat('mat_$mid')\">$icone_menos</a>
             <img src=\"imagens/icones/22x22/material.png\" /> $mat_desc
            <ul style=\"$ulstyle\">";
            while (!$res2->EOF) {
                $campo2=$res2->fields;
                $tmp .= MostraMaterial($campo2['MID'],$tabela,$campolink);
                $res2->MoveNext();
            }
            $tmp .= "</ul>";
        }
        else {
            $tmp = "<li><div id=\"mat_$mid\" /> $icone_vasio
             <img src=\"imagens/icones/22x22/material.png\" /> $mat_desc";
        }
        $tmp .= "</div></li>";
        $i2++;
    }
    if ($i2 != 0) return $tmp;
    else return '';
}

/**
* Vis�o em Arvore baseada no objeto de manuten��o, mostra todas localiza��es acima, pontos de lubrifica��o, pe�as e equipamentos.
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @author  Fernando Cosentino
* @param integer $mid_maq
*/
function VisaoMaq($mid)
{
    global $dba, $tdb, $ling, $op, $oq, $id, $exe, $form;
    $resultado = $dba[$tdb[MAQUINAS]['dba']]->Execute("SELECT COD,DESCRICAO,MODELO,FAMILIA,MID_SETOR,MID FROM " . MAQUINAS . " WHERE MID = '$mid'");
    $icone_mais = "<img src=\"imagens/icones/mais.gif\" alt=\" \" border=\"0\" />";
    $icone_menos = "<img src=\"imagens/icones/menos.gif\" alt=\" \" border=\"0\" />";
    $icone_vasio = "<img src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />";
    if (!$resultado) erromsg($dba[$tdb[MAQUINAS]['dba']]->ErrorMsg());
    else {
        $maq_cod = $resultado->fields['COD'];
        $maq_des = $resultado->fields['DESCRICAO'];
        $maq_mod = $resultado->fields['MODELO'];
        $maq_set = $resultado->fields['MID_SETOR'];
        $maq_fam = VoltaValor(MAQUINAS_FAMILIA, "DESCRICAO", "MID", $resultado->fields['MODELO'], $tdb[MAQUINAS_FAMILIA]['dba']);
        $resultado = $dba[$tdb[SETORES]['dba']]->Execute("SELECT COD,DESCRICAO,MID_AREA FROM " . SETORES . " WHERE MID = '$maq_set'");
        if (!$resultado) erromsg($dba[$tdb[SETORES]['dba']]->ErrorMsg());
        else {
            $set_cod = $resultado->fields['COD'];
            $set_des = $resultado->fields['DESCRICAO'];
            $area_des = VoltaValor(AREAS, "DESCRICAO", "MID", $resultado->fields['MID_AREA'], $tdb[AREAS]['dba']);
            $area_cod = VoltaValor(AREAS, "COD", "MID", $resultado->fields['MID_AREA'], $tdb[AREAS]['dba']);
        }
    }
    if ($maq_cod != "") {
        echo "\n<div id=\"mod_va\">\n";
        if ($area_des != "") echo "<div id=\"area_$area_cod\">\n<a onclick=\"FecharGenerico('area_$area_cod','local1')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/local1_abre.png\" alt=\"\" /> " . htmlentities("$area_cod " .  ($area_des ? "- $area_des": '') ) . "\n<ul style=\"display:block\"><li>\n";
        if ($set_des != "") echo "<div id=\"setor_$set_cod\">\n<a onclick=\"FecharGenerico('setor_$set_cod','local2')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/local2_abre.png\" alt=\"\" /> " . htmlentities("$set_cod " .  ($set_des ? "- $set_des": '') ) . "\n<ul style=\"display:block\"><li>\n";
        echo "
        <div id=\"maq_$mid\"><a onclick=\"FecharGenerico('maq_$mid','local3')\">$icone_menos</a>
        <img src=\"imagens/icones/22x22/local3_abre.png\" id=\"imaq_$mid\" alt=\"\" /><a ondblclick=\"abre_janela_form('MAQUINA','$mid','$mid','2','$id','$exe','$op'," . $form['MAQUINA'][0]['altura'] . "," . $form['MAQUINA'][0]['largura'] . ",'corpo','')\"> " . htmlentities("$maq_cod - $maq_des") . "<span style=\"font-size:9px\"><strong> $maq_mod</strong></span></a>\n<ul style=\"display:block\" id=\"maq_$mid\">
        ";
        $listapontopred = ListaPontoPred($mid);
        if ($listapontopred != 0) {
            echo "<div id=\"cp_$mid\">\n<a onclick=\"FecharGenerico('cp_$mid','local3')\">$icone_mais</a>
            <img src=\"imagens/icones/22x22/local3.png\" alt=\"\" /> " . $ling['pred_va'] . "
            <ul>";
            for ($ic = 0; $listapontopred[$ic]['ponto'] != ""; $ic++) {
                $pontopred = $listapontopred[$ic]['ponto'];
                $pontopred_tag = $listapontopred[$ic]['conj'];
                $pontopred_mid = $listapontopred[$ic]['mid'];
                echo "<li><div id=\"pontopred_$pontopred_mid\">$icone_vasio <img src=\"imagens/icones/22x22/preditiva.png\" alt=\"\" /><a> $pontopred_tag " .  ($pontopred ? "- $pontopred": '') ."</a>\n</div></li>\n";
                unset($pontopred_tag);
            }
            echo "</ul></div></li>";
        }
        $listapontolub = ListaPontoLub($mid);
        if ($listapontolub != 0) {
            echo "<div id=\"cl_$mid\">\n<a onclick=\"FecharGenerico('cl_$mid','local3')\">$icone_mais</a>\n<img src=\"imagens/icones/22x22/local3.png\" alt=\"\" /> " . $ling['lub_va'] . " \n<ul>\n";
            for ($ic = 0; $listapontolub[$ic]['ponto'] != ""; $ic++) {
                $pontolub = $listapontolub[$ic]['ponto'];
                $pontolub_tag = $listapontolub[$ic]['conj'];
                $pontolub_mid = $listapontolub[$ic]['mid'];
                echo "<li><div id=\"pontopred_$pontolub_mid\">$icone_vasio  <img src=\"imagens/icones/22x22/lubrificacao.png\" alt=\"\" /><a> $pontolub_tag  " .  ($pontolub ? "- $pontolub": '')  ."</a>\n</div></li>\n";
                unset($pontolub_tag);
            }
            echo "</ul></div></li>";
        }
        $listacontador = ListaContador($mid);
        if ($listacontador != 0) {
            echo "<div id=\"cc_$mid\">\n<a onclick=\"FecharGenerico('cc_$mid','local3')\">$icone_mais</a>\n<img src=\"imagens/icones/22x22/local3.png\" alt=\"\" /> " . $ling['cont_va'] . " \n<ul>\n";
            for ($ic = 0; $listacontador[$ic]['nome'] != ""; $ic++) {
                $contador = $listacontador[$ic]['nome'];
                $contador_mid = $listacontador[$ic]['mid'];
                echo "<li><div id=\"pontopred_$contador_mid\">$icone_vasio  <img src=\"imagens/icones/22x22/contador.png\" alt=\"\" /><a> $contador </a>\n</div></li>\n";
                unset($pontolub_tag);
            }
            echo "</ul></div></li>";
        }
        $listaconj = ListaConjunto($mid);
        if ($listaconj != 0) {
            echo "<div id=\"co_$mid\">\n<a onclick=\"FecharGenerico('co_$mid','local3')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/local3.png\" alt=\"\" /> " . $ling['posicao_va'] . " \n<ul style=\"display:block\">\n";
            for ($ic = 0; $listaconj[$ic]['nome'] != ""; $ic++) {
                $conjunto = $listaconj[$ic]['nome'];
                $conjunto_tag = $listaconj[$ic]['tag'];
                $conjunto_mid = $listaconj[$ic]['mid'];
                $listasubconj = ListaSubConjunto($conjunto_mid);
                $listaequip = ListaEquipamento($conjunto_mid);
                $listapecaa = ListaPecaConjunto($conjunto_mid);
                if (($listaequip == 0) and ($listasubconj == 0) and ($listapecaa == 0)) echo "<li>$icone_vasio <img src=\"imagens/icones/22x22/tag_vazio.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('CONJUNTO','$conjunto_mid','$oq','2','$id','$exe','$op'," . $form['CONJUNTO'][0]['altura'] . "," . $form['CONJUNTO'][0]['largura'] . ",'corpo','')\"> " . htmlentities("$conjunto_tag " .  ($conjunto ? "- $conjunto": '')) . "</a></li>\n";
                else {
                    echo "<li><div id=\"conj_$conjunto_mid\"><a onclick=\"FecharConj('conj_$conjunto_mid')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/tag.png\" alt=\"\" /><a  ondblclick=\"abre_janela_form('CONJUNTO','$conjunto_mid','$oq','2','$id','$exe','$op'," . $form['CONJUNTO'][0]['altura'] . "," . $form['CONJUNTO'][0]['largura'] . ",'corpo','')\"> " . htmlentities("$conjunto_tag " .  ($conjunto ? "- $conjunto": '')) . "</a><ul style=\"display:block\">\n";
                    for ($ie = 0; $listaequip[$ie]['nome'] != ""; $ie++) {
                        $equipamento = $listaequip[$ie]['nome'];
                        $equipamento_cod = $listaequip[$ie]['cod'];
                        $equipamento_mid = $listaequip[$ie]['mid'];
                        echo "<li><div id=\"equip_$equipamento_mid\"><a onclick=\"FecharEquip('equip_$equipamento_mid')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/equip.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('EQUIPAMENTO','$equipamento_mid','$oq','2','$id','$exe','$op'," . $form['EQUIPAMENTO'][0]['altura'] . "," . $form['EQUIPAMENTO'][0]['largura'] . ",'corpo','')\"> $equipamento_cod " .  ($equipamento ? "- $equipamento": '') ."</a>\n<ul style=\"display:block\">\n";
                        $listapeca = ListaPecaEquipamento($equipamento_mid);
                        if ($listapeca != 0) {
                            for ($ip = 0; $listapeca[$ip]['nome'] != ""; $ip++) {
                                $peca = htmlentities($listapeca[$ip]['nome']);
                                
                                $classe = htmlentities($listapeca[$ip]['classe']);
                                $classe = ($classe != '')? "($classe)" : "";
                                
                                $qto = $listapeca[$ip]['qto'];
                                $pmid = $listapeca[$ip]['mid'];
                                $compl = $listapeca[$ip]['compl'];
                                $un_mid = VoltaValor(MATERIAIS,'UNIDADE','MID',$pmid,0);
                                $un = VoltaValor(MATERIAIS_UNIDADE,'COD','MID',$un_mid,0);
                                echo "<li><div id=\"peca_$pmid\">$icone_vasio   <img src=\"imagens/icones/22x22/material.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('MATERIAIS','$pmid','$oq','2','$id','$exe','$op'," . $form['MATERIAIS'][0]['altura'] . "," . $form['MATERIAIS'][0]['largura'] . ",'corpo','')\"> $qto <em>$un</em> - $peca - " .  ($compl ? "- $compl": '') ." $classe</a>\n</div></li>\n";
                                unset($peca);
                            }

                        }
                        echo "</ul></div></li>";
                    }
                    if ($listapecaa != 0) {
                        for ($ip = 0; $listapecaa[$ip]['nome'] != ""; $ip++) {
                            $peca = htmlentities($listapecaa[$ip]['nome']);
                            
                            $classe = htmlentities($listapecaa[$ip]['classe']);
                            $classe = ($classe != '')? "($classe)" : "";
                            
                            $qto = $listapecaa[$ip]['qto'];
                            $pmid = $listapecaa[$ip]['mid'];
                            $compl = $listapecaa[$ip]['compl'];
                            $un_mid = VoltaValor(MATERIAIS,'UNIDADE','MID',$pmid,0);
                            $un = VoltaValor(MATERIAIS_UNIDADE,'COD','MID',$un_mid,0);
                            echo "<li><div id=\"peca_$pmid\">$icone_vasio   <img src=\"imagens/icones/22x22/material.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('MATERIAIS','$pmid','$oq','2','$id','$exe','$op'," . $form['MATERIAIS'][0]['altura'] . "," . $form['MATERIAIS'][0]['largura'] . ",'corpo','')\"> $qto <em>$un</em> - $peca " .  ($compl ? "- $compl": '') ." $classe</a>\n</div></li>\n";
                            unset($peca);
                        }
                    }

                    for ($ics = 0; $listasubconj[$ics]['nome'] != ""; $ics++) {
                        $sconjunto = htmlentities($listasubconj[$ics]['nome']);
                        $sconjunto_tag = $listasubconj[$ics]['tag'];
                        $sconjunto_mid = $listasubconj[$ics]['mid'];
                        $listapecasa = ListaPecaConjunto($sconjunto_mid);

                        $listaequip = ListaEquipamento($sconjunto_mid);
                        if (($listaequip == 0) and ($listapecasa == 0)) echo "<li>
                                            $icone_vasio 
                                            <img src=\"imagens/icones/22x22/tag_vazio.png\" alt=\"\" />
                                            <a  ondblclick=\"abre_janela_form('CONJUNTO','$sconjunto_mid','','2','$id','$exe','$op'," . $form['CONJUNTO'][0]['altura'] . "," . $form['CONJUNTO'][0]['largura'] . ",'corpo','')\">$sconjunto_tag " .  ($conjunto ? "- $conjunto": '')."</a>
                                            </li>\n";
                        else {
                            echo "<li>
                                            <div id=\"sconj_$sconjunto_mid\">
                                            <a onclick=\"FecharSConj('sconj_$sconjunto_mid')\">$icone_mais</a>
                                            <img src=\"imagens/icones/22x22/tag.png\" alt=\"\" />
                                            <a  ondblclick=\"abre_janela_form('CONJUNTO','$sconjunto_mid','','2','$id','$exe','$op'," . $form['CONJUNTO'][0]['altura'] . "," . $form['CONJUNTO'][0]['largura'] . ",'corpo','')\">$sconjunto_tag " .  ($conjunto ? "- $conjunto": '') ."</a>
                                            <ul>\n";

                            for ($ie = 0; $listaequip[$ie]['nome'] != ""; $ie++) {
                                $equipamento = htmlentities($listaequip[$ie]['nome']);
                                $equipamento_cod = $listaequip[$ie]['cod'];
                                $equipamento_mid = $listaequip[$ie]['mid'];
                                echo "<li><div id=\"equip_$equipamento_mid\"><a onclick=\"FecharEquip('equip_$equipamento_mid')\">$icone_menos</a>\n<img src=\"imagens/icones/22x22/equip.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('EQUIPAMENTO','$equipamento_mid','$oq','2','$id','$exe','$op'," . $form['EQUIPAMENTO'][0]['altura'] . "," . $form['EQUIPAMENTO'][0]['largura'] . ",'corpo','')\"> $equipamento_cod " .  ($equipamento ? "- $equipamento": '') ." </a>\n<ul style=\"display:block\">\n";
                                $listapeca = ListaPecaEquipamento($equipamento_mid);
                                if ($listapeca != 0) {
                                    for ($ip = 0; $listapeca[$ip]['nome'] != ""; $ip++) {
                                        $peca = htmlentities($listapeca[$ip]['nome']);
                                        
                                        $classe = htmlentities($listapecasa[$ip]['classe']);
                                        $classe = ($classe != '')? "($classe)" : "";
                                        
                                        $qto = $listapeca[$ip]['qto'];
                                        $pmid = $listapeca[$ip]['mid'];
                                        echo "<li><div id=\"peca_$pmid\">$icone_vasio   <img src=\"imagens/icones/22x22/material.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('MATERIAIS','$pmid','$oq','2','$id','$exe','$op'," . $form['MATERIAIS'][0]['altura'] . "," . $form['MATERIAIS'][0]['largura'] . ",'corpo','')\"> $qto - $peca $classe</a>\n</div></li>\n";
                                        unset($peca);
                                    }
                                }
                                echo "</ul></div></li>";
                            }
                            if ($listapecasa != 0) {
                                for ($ip = 0; $listapecasa[$ip]['nome'] != ""; $ip++) {
                                    $peca = htmlentities($listapecasa[$ip]['nome']);
                                    
                                    $classe = htmlentities($listapecasa[$ip]['classe']);
                                    $classe = ($classe != '')? "($classe)" : "";
                                    
                                    $qto = $listapecasa[$ip]['qto'];
                                    $pmid = $listapecasa[$ip]['mid'];
                                    echo "<li><div id=\"peca_$pmid\">$icone_vasio   <img src=\"imagens/icones/22x22/material.png\" alt=\"\" /><a ondblclick=\"abre_janela_form('MATERIAIS','$pmid','$oq','2','$id','$exe','$op'," . $form['MATERIAIS'][0]['altura'] . "," . $form['MATERIAIS'][0]['largura'] . ",'corpo','')\"> $qto - $peca $classe</a>\n</div></li>\n";
                                    unset($peca);
                                }
                            }
                            unset($listapecasa);

                            // Equipamento SUB CONJUNTO
                            echo "</ul></div></li>\n";
                        }
                    }
                    echo "</ul></div></li>";
                }
            }
            echo "</ul></div></li>";
        }
        echo "</ul></div></li>";
    }
    echo "</ul></div></li>";
    echo "</ul></div></li></div>";
}

$maquina = VoltaValor(MAQUINAS, "DESCRICAO", "MID", $oq, $tdb[MAQUINAS]['dba']);
$maquina_cod = VoltaValor(MAQUINAS, "COD", "MID", $oq, $tdb[MAQUINAS]['dba']);

echo "
<div id=\"mod_menu\">
<div>
<a href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/va.png\" border=\"0\" alt=\"" . $ling['visa_geral'] . "\" />
<span>" . $ling['visao_geral'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=4\">
<img src=\"imagens/icones/22x22/local3.png\" border=\"0\" alt=\"" . $ling['tabela_local3'] . "\" />
<span>" . $ling['tabela_local3'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=6&oq=$oq\">
<img src=\"imagens/icones/22x22/engrenagem.png\" border=\"0\" alt=\"" . $ling['detalha'] . "\" />
<span>" . $ling['detalha'] . "</span>
</a>
</div>

<div>
<a href=\"javascript:abre_janela_form('MAQUINA','$oq','$oq','2','$id','$exe','$op'," . $form['MAQUINA'][0]['altura'] . "," . $form['MAQUINA'][0]['largura'] . ",'')\">
<img src=\"imagens/icones/22x22/local3_editar.png\" border=\"0\" alt=\"" . $ling['editar_local3'] . "\" />
<span>" . $ling['editar_local3'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=61&oq=$oq\">
<img src=\"imagens/icones/22x22/tag.png\" border=\"0\" alt=\"" . $ling['posicoes'] . "\" />
<span>" . $ling['posicoes'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=62&oq=$oq\">
<img src=\"imagens/icones/22x22/preditiva.png\" border=\"0\" alt=\"" . $ling['tabela_preditiva'] . "\" />
<span>" . $ling['tabela_preditiva'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=63&oq=$oq\">
<img src=\"imagens/icones/22x22/lubrificacao.png\" border=\"0\" alt=\"" . $ling['tabela_lubrificacao'] . "\" />
<span>" . $ling['tabela_lubrificacao'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=64&oq=$oq\">
<img src=\"imagens/icones/22x22/contador.png\" border=\"0\" alt=\"" . $ling['tabela_contador'] . "\" />
<span>" . $ling['tabela_contador'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=65&oq=$oq\">
<img src=\"imagens/icones/22x22/material.png\" border=\"0\" alt=\"" . $ling['pecas_conj'] . "\" />
<span>" . $ling['pecas_conj'] . "</span>
</a>
</div>

<div>
<a href=\"manusis.php?id=$id&op=$op&exe=67&oq=$oq\">
<img src=\"imagens/icones/22x22/empresa.png\" border=\"0\" alt=\"" . $tdb[MAQUINAS_FORNECEDOR]['DESC'] . "\" />
<span>" . $tdb[MAQUINAS_FORNECEDOR]['DESC'] . "</span>
</a>
</div>";


if (VoltaPermissao($id, $op) !=3) {
    echo "<div>
    <a href=\"manusis.php?id=$id&op=$op&exe=66&oq=$oq\">
    <img src=\"imagens/icones/22x22/copiar.png\" border=\"0\" alt=\"" . $ling['copiar'] . "\" />
    <span>" . $ling['copiar'] . "</span>
    </a>
    </div>";
}

echo "
<div>
<a href=\"javascript:abre_janela_arq('maquinas','$oq', $id)\">
<img src=\"imagens/icones/22x22/anexar.png\" border=\"0\" alt=\"" . $ling['anexar_arq'] . "\" />
<span>" . $ling['anexar_arq'] . "</span>
</a>
</div>

<div>";

if ($exe == 6) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
</div>
</div>
<br clear=\"all\" /><div>";
    VisaoMaq($oq);
    echo "</div>";
}

if ($exe == 61) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos[0] = "TAG";
    $campos[1] = "DESCRICAO";
    $campos[2] = "COMPLEMENTO";
    $campos[3] = "MID_CONJUNTO";
    $campos[4] = "FABRICANTE";
    $campos[5] = "MARCA";
    $campos[6] = "MODELO";
    $campos[7] = "NSERIE";
    $campos[8] = "POTENCIA";
    $campos[9] = "PRESSAO";
    $campos[10] = "TENSAO";
    $campos[11] = "VAZAO";
    $campos[12] = "MID_MAQUINA";
    ListaTabela(MAQUINAS_CONJUNTO, "MID", $campos, "CONJUNTO2", "MID_MAQUINA", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}

if ($exe == 62) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos[0] = "MID_CONJUNTO";
    $campos[1] = "PONTO";
    $campos[2] = "GRANDEZA";
    $campos[3] = "VALOR_MINIMO";
    $campos[4] = "VALOR_MAXIMO";
    $campos[5] = "OBSERVACAO";
    ListaTabela(PONTOS_PREDITIVA, "MID", $campos, "PONTO_PREDITIVA", "MID_MAQUINA", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}


if ($exe == 63) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    
    $campos[0] = "PONTO";
    $campos[1] = "NPONTO";
    $campos[2] = "METODO";
    $campos[3] = "MID_CONJUNTO";
    $campos[4] = "MID_MAQUINA";
    ListaTabela(PONTOS_LUBRIFICACAO, "MID", $campos, "PONTO_LUBRIFICACAO", "MID_MAQUINA", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}
if ($exe == 64) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos[3] = "MID_MAQUINA";
    $campos[0] = "DESCRICAO";
    $campos[1] = "TIPO";
    $campos[2] = "ZERA";
    ListaTabela(MAQUINAS_CONTADOR, "MID", $campos, "CONTADOR_MAQ", "MID_MAQUINA", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}
if ($exe == 65) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
</div>
</div>
<br clear=\"all\" />
<div>";
    $campos = array();
    $campos[] = "MID_CONJUNTO";
    $campos[] = "MID_MATERIAL";
    $campos[] = "QUANTIDADE";
    $campos[] = "MID_CLASSE";
    
    ListaTabela(MAQUINAS_CONJUNTO_MATERIAL, "MID", $campos, "CONJUNTO_MATERIAL", "MID_MAQUINA", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}

if (($exe == 66) and ($oq != "")) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <br clear=\"all\" />
    <div id=\"lt\">\n
    <div id=\"lt_cab\">\n
    <h3>".$ling['copiar']."</h3>
    </div>
    <br clear=\"all\" />
    <div id=\"lt_forms\">
    <form class=\"form\" action=\"manusis.php?id=$id&op=$op&oq=$oq&exe=69\" method=\"POST\">
    <p align=\"center\">".$ling['msgcopiar']."</p>
    <fieldset>
    <legend>".$ling['opcoes']."</legend>
    Copiar para qual ".$tdb[MAQUINAS]['DESC']."?";
        
    $primeirocampo="Desejo criar um novo cadastro";
    FormSelectD ('COD', 'DESCRICAO', MAQUINAS, $_POST['maqc'], 'maqc', 'maqc', 'MID', '');  
    echo "<br /><label for=\"cod\">Caso seja um novo cadastro informe o c&oacute;digo do ".$tdb[MAQUINAS]['DESC'].": </label>
    <input type=\"text\" size=\"20\"  maxlength=\"120\" id=\"cod\" name=\"cod\" class=\"campo_text\"><br />
    </fieldset><br clear=\"all\" />
    <input type=\"submit\" name=\"ok\" class=\"botao\" value=\"Enviar\" />
    </form></div>";
}

if ($exe == 67) {
    echo "<h3>" . htmlentities($maquina) . "</h3>
    </div>
    </div>
    <br clear=\"all\" />
    <div>";
    
    $campos = array();
    $campos[] = "MID_FORNECEDOR";
    $campos[] = "OBS";
    
    ListaTabela(MAQUINAS_FORNECEDOR, "MID", $campos, "MAQUINAS_FORNECEDOR", "MID_MAQUINA", $oq, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "");
    echo "</div>";
}


if (($exe == 69) and ($oq != "") and ($_POST['ok'] != "")) {
    $maq_dest=(int)$_POST['maqc'];
        
    
        if ($maq_dest == "") {
            $maq_dest_cod=LimpaTexto($_POST['cod']);
        }
        else {
            $maq_dest_cod=VoltaValor(MAQUINAS,"COD","MID",$maq_dest,$tdb[MAQUINAS]['dba']);
        }

        $rmaq = $dba[$tdb[MAQUINAS]['dba']]->Execute("SELECT * FROM " . MAQUINAS . " WHERE MID = '$oq'");
            if (!$rmaq) {
            erromsg($dba[$tdb[MAQUINAS]['dba']]->ErrorMsg());
            exit;
        }
        else {
        if ($maq_dest == "") {
            $maq = $rmaq->fields;

            $maq_mid = GeraMid(MAQUINAS, "MID", $tdb[MAQUINAS]['dba']);
            $maq['MID'] = $maq_mid;
            
        if ($maq_dest_cod == ""){
            $maq_dest_cod = $maq['COD'] ." ($maq_mid)";
            }
            
            $maq['COD'] = $maq_dest_cod;
            
            $sql = "INSERT INTO " . MAQUINAS . " (" . implode(",", array_keys($maq)) . ") 
            VALUES ('" . mb_strtoupper(implode("', '", array_values($maq))) . "')";         
            
        if (!$dba[$tdb[MAQUINAS]['dba']]->Execute ($sql)){
            erromsg("$sql <Br> " . $dba[$tdb[MAQUINAS]['dba']]->ErrorMsg() . "<br />" . $sql);
            }
        }
        else {
            $maq_mid=$maq_dest;
        }
        
         // Contador
        $resultado24 = $dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute ("SELECT * FROM " . MAQUINAS_CONTADOR . " WHERE MID_MAQUINA = '$oq'");
        while (!$resultado24 -> EOF){
            $maq_conta = $resultado24 -> fields;                   
          
            $maq_cont = GeraMid(MAQUINAS_CONTADOR, "MID", $tdb[MAQUINAS_CONTADOR]['dba']);
            $maq_conta['MID'] = $maq_cont;
            $maq_conta['MID_MAQUINA'] = $maq_mid;
                          
            $maq_insere = " INSERT INTO " . MAQUINAS_CONTADOR . " ( "  . implode (",", array_keys($maq_conta)).")
            VALUES ('". implode ("','", array_values($maq_conta)) . "')";

            if (!$dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute($maq_insere)){
                erromsg ("".  $dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> ErrorMsg () . "<br/>" . $maq_insere);                   
            }   
            
            $resultado24->MoveNext();           
        } 
        
        // Conjunto
        $resultado = $dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->Execute("SELECT * FROM " . MAQUINAS_CONJUNTO . " WHERE MID_MAQUINA = '$oq' ORDER BY MID_CONJUNTO, TAG ASC");
        while (!$resultado->EOF) {
            $conj = $resultado->fields;
            
            $conj_ant = $conj['MID'];       
            $conj['MID_MAQUINA'] = $maq_mid;                        
            $conj_mid = GeraMid(MAQUINAS_CONJUNTO, "MID", $tdb[MAQUINAS_CONJUNTO]['dba']);
            $conj['MID'] = $conj_mid;
            $conj_cod = str_replace($maq['COD'], $maq_dest_cod, $conj['TAG']);
            
            if ($conj['MID_CONJUNTO'] != 0) {
                $tag_pai = VoltaValor(MAQUINAS_CONJUNTO, 'TAG', 'MID', $conj['MID_CONJUNTO']);
                $conj['MID_CONJUNTO'] = (int)VoltaValor(MAQUINAS_CONJUNTO, 'MID', "MID_MAQUINA = $maq_mid AND TAG", $tag_pai);
            }       
             
            $insere_conj = "INSERT INTO " . MAQUINAS_CONJUNTO . " ( ". implode ("," , array_keys($conj)).  ")
            VALUES ('".implode("','", array_values($conj)) . "')";                      
            if (!$dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->Execute($insere_conj)) {
                erromsg("" . $dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->ErrorMsg() . "<br />" . $insere_conj);
            }   
            
            // Lubrifica��o
            $resultado22 = $dba[$tdb[PONTOS_LUBRIFICACAO]['dba']]->Execute("SELECT * FROM " . PONTOS_LUBRIFICACAO . " WHERE MID_MAQUINA = '$oq' and MID_CONJUNTO = '" . $conj_ant . "'");
            while (!$resultado22->EOF) {
                $ponto = $resultado22->fields;  
                            
                $ponto_mid = GeraMid(PONTOS_LUBRIFICACAO, "MID", $tdb[PONTOS_LUBRIFICACAO]['dba']);
                $ponto['MID'] = $ponto_mid;             
                $ponto['MID_MAQUINA'] = $maq_mid;
                $ponto['MID_CONJUNTO'] = $conj_mid;
                
                $ponto_ins = "INSERT INTO " . PONTOS_LUBRIFICACAO . " ( " . implode (",", array_keys($ponto)).")
                VALUES ('". implode ("','" , array_values($ponto)) . "')";              
                
                if (!$dba[$tdb[PONTOS_LUBRIFICACAO]['dba']]->Execute($ponto_ins)) {
                    erromsg("" . $dba[$tdb[PONTOS_LUBRIFICACAO]['dba']]->ErrorMsg() . "<br />" . $ponto_ins);
                }   
                        
                $resultado22-> MoveNext();
            }        
            
            // Preditiva
            $resultado22 = $dba[$tdb[PONTOS_PREDITIVA]['dba']]->Execute("SELECT * FROM " . PONTOS_PREDITIVA . " WHERE MID_MAQUINA = '$oq' and MID_CONJUNTO = '" . $conj_ant . "'");
            while (!$resultado22->EOF) {
                $ponto = $resultado22->fields;
                
                $ponto_mid = GeraMid(PONTOS_PREDITIVA, "MID", $tdb[PONTOS_PREDITIVA]['dba']);
                $ponto['MID'] = $ponto_mid;             
                $ponto['MID_MAQUINA'] = $maq_mid;
                $ponto['MID_CONJUNTO'] = $conj_mid;         
                
                $ponto_ins = "INSERT INTO " . PONTOS_PREDITIVA . " ( " .  implode (",", array_keys($ponto)).")
                VALUES ('" . implode ("','", array_values($ponto)) . "')"; 
                                
                if (! $dba[$tdb[PONTOS_PREDITIVA]['dba']]->Execute($ponto_ins)) {
                    erromsg("" . $dba[$tdb[PONTOS_PREDITIVA]['dba']]->ErrorMsg() . "<br />" . $ponto_ins);
                }
                
                $resultado22 -> MoveNext();
            }
            
            // Material 
            $resultado2 = $dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]->Execute("SELECT * FROM " . MAQUINAS_CONJUNTO_MATERIAL . " WHERE MID_MAQUINA = '$oq' and MID_CONJUNTO = '" . $conj_ant . "'");
            while (!$resultado2->EOF) {
                $mat = $resultado2->fields; 
                            
                $mat_mid = GeraMid(MAQUINAS_CONJUNTO_MATERIAL, "MID", $tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']);
                $mat['MID'] = $mat_mid;         
                $mat['MID_MAQUINA'] = $maq_mid;
                $mat['MID_CONJUNTO'] = $conj_mid;           
                
                $mat_insere = "INSERT INTO " . MAQUINAS_CONJUNTO_MATERIAL . " ( " . implode ("," , array_keys($mat)).") 
                VALUES ('" . implode ("','", array_values($mat))."')";  
                 
                if (! $dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]->Execute($mat_insere)) {
                    erromsg("" . $dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]->ErrorMsg() . "<br />" . $mat_insere);
                }           
                
                $resultado2->MoveNext();            
            }           
            
            // Equipamentos
            $resultado3 = $dba[$tdb[EQUIPAMENTOS]['dba']]->Execute("SELECT * FROM " . EQUIPAMENTOS . " WHERE MID_MAQUINA = '$oq' and MID_CONJUNTO = '" . $conj_ant . "'");
            while (!$resultado3->EOF) {
                $equip = $resultado3->fields;
                                
                $equip_ant = $equip['MID'];             
                $equip_mid = GeraMid(EQUIPAMENTOS, "MID", $tdb[EQUIPAMENTOS]['dba']);
                $equip['MID'] = $equip_mid;             
                $equip['MID_MAQUINA'] = $maq_mid;
                $equip['MID_CONJUNTO'] = $conj_mid;             
                $equip['COD'] = GeraCodComponente($equip['COD'], $equip_mid);
                
                $equip_insere = "INSERT INTO " . EQUIPAMENTOS . " ( " . implode (",", array_keys($equip)). ") 
                VALUES  ('" . implode ("','", array_values($equip)). "')"; 
                
                
                if (!$dba[$tdb[EQUIPAMENTOS]['dba']]->Execute($equip_insere)) {
                    erromsg("" . $dba[$tdb[EQUIPAMENTOS]['dba']]->ErrorMsg() . "<br />" . $equip_insere);
                }               
                    
                // Material 
            $resultado4=$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> Execute("SELECT * FROM ".EQUIPAMENTOS_MATERIAL." WHERE MID_EQUIPAMENTO = '$equip_ant'");
            while (!$resultado4->EOF) {
                $mat=$resultado4->fields;
                            
                $mat_mid=GeraMid(EQUIPAMENTOS_MATERIAL,"MID",$tdb[EQUIPAMENTOS_MATERIAL]['dba']);
                $mat['MID'] = $mat_mid;                 
                $mat['MID_EQUIPAMENTO'] = $equip_mid;                   

                $mat_insere= "INSERT INTO " .EQUIPAMENTOS_MATERIAL.  " ( "  . implode ("," , array_keys($mat)) .")
                VALUES ('" . implode ("','" , array_values ($mat)) . "')"; 
                                     
                if (!$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> Execute($mat_insere)) {
                    erromsg("".$dba[$tdb[EQUIPAMENTOS_MATERIAL]['dba']] -> ErrorMsg()  . "<br />" . $mat_insere);
                    }
                                    
                    $resultado4->MoveNext();                    
                }               
                
                // Preditiva    
            $resultado5=$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute("SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_EQUIPAMENTO = '$equip_ant'");
            while (!$resultado5->EOF) {
                $pre=$resultado5->fields;
                    
                $pre_mid=GeraMid(PONTOS_PREDITIVA,"MID",$tdb[PONTOS_PREDITIVA]['dba']);
                $pre['MID'] = $pre_mid;                 
                $pre['MID_EQUIPAMENTO'] = $equip_mid ;
                    
                $pre_ins= "INSERT INTO ".PONTOS_PREDITIVA.  " ( " . implode("," , array_keys($pre)).") 
                VALUES ('" . implode ("','" , array_values($pre)) . "')";                   
                        
                if (!$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute($pre_ins)) {
                    erromsg("".$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> ErrorMsg() . "<br />" . $pre_ins);
                    }
                    
                    $resultado5->MoveNext();
                }
                
                $resultado3->MoveNext();
            }

            $resultado->MoveNext();
        }   
        
        echo "<h3>" . htmlentities($maquina) . "</h3>
        </div>
        </div>
        <br clear=\"all\" />
        <div><br /><br /><p align=\"center\"><h2> Objeto Copiado com sucesso! </h2><br /> <br /><a href=\"manusis.php?id=$id&op=$op&exe=6&oq=$maq_mid\">Clique aqui para acessar o item copiado</a><br /> <br /><br /> <br /></p></div>";
    }
}

?>
