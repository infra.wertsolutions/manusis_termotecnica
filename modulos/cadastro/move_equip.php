<?
/**
* Movimentação de Equipamento
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package manusis
* @subpackage  cadastro
*/
$destino=(int)$_GET['destino'];
$destino1=(int)$_GET['destino1'];
$objeto=(int)$_GET['objeto'];
$motivo=LimpaTexto($_GET['motivo']);
echo "<h3>".$ling['move_equip']."</h3>
</div>
</div>
<br clear=\"all\" />
<div id=\"lt\"><br />";

if (($_GET['enviar'] != "") and ($objeto != "") ) {
	if(!$_GET['destino'] and $_GET['destino1'] !=0){
		erromsg($ling['sel_pos']);
	}
	else {
		$opc=VoltaValor(EQUIPAMENTOS,"MID_CONJUNTO","MID",$objeto,$tdb[EQUIPAMENTOS]['dba']);
		$opc2=VoltaValor(EQUIPAMENTOS,"MID_MAQUINA","MID",$objeto,$tdb[EQUIPAMENTOS]['dba']);
		
		// Buscando a empresa do destino
		$emp_tmp = ($destino1 != 0)? $destino1 : $opc2;
		$emp_tmp = VoltaValor(MAQUINAS, 'MID_SETOR',   'MID', $emp_tmp, $tdb[MAQUINAS]['dba']);
        $emp_tmp = VoltaValor(SETORES,  'MID_AREA',    'MID', $emp_tmp, $tdb[SETORES]['dba']);
        $emp_tmp = VoltaValor(AREAS,    'MID_EMPRESA', 'MID', $emp_tmp, $tdb[AREAS]['dba']);
		
		$r=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute("UPDATE ".EQUIPAMENTOS." SET MID_CONJUNTO = '$destino', MID_MAQUINA = '$destino1', MID_EMPRESA = '$emp_tmp' WHERE MID = '$objeto'");
		if (!$r) erromsg($dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg());
		else {
			$mid=GeraMid(MOV_EQUIPAMENTO,"MID",$tdb[MOV_EQUIPAMENTO]['dba']);
			$dba[$tdb[MOV_EQUIPAMENTO]['dba']] -> Execute("INSERT INTO ".MOV_EQUIPAMENTO." VALUES('$objeto','$opc','$opc2','$destino','$destino1','$motivo','".date("Y-m-d")."','$mid')");

			$setor = (int)VoltaValor(MAQUINAS,"MID_SETOR","MID",$destino1,0);
			$area = (int)VoltaValor(SETORES,"MID_AREA","MID",$setor,0);
			$sql = "UPDATE ".ORDEM_PLANEJADO." SET MID_SETOR= '$setor', MID_CONJUNTO = '$destino', MID_AREA = '$area', MID_MAQUINA = '$destino1' where STATUS = 1 AND MID_EQUIPAMENTO = '$objeto'";

			$ins = $dba[0] ->Execute($sql);
			if(!$ins ){
				erromsg($dba[0]->ErrorMsg());
			}
			
			$sql = "UPDATE ".PONTOS_PREDITIVA." SET MID_MAQUINA = '$destino1' WHERE MID_EQUIPAMENTO = '$objeto'";

			$ins = $dba[0] ->Execute($sql);
			if(!$ins ){
				erromsg($dba[0]->ErrorMsg());
			}

			$sql = "UPDATE ".PENDENCIAS." SET MID_CONJUNTO = '$destino', MID_MAQUINA = '$destino1' where STATUS = 1 AND MID_EQUIPAMENTO = '$objeto'";
			$ins2 = $dba[0] ->Execute($sql);
			if(!$ins2 ){
				erromsg($dba[0]->ErrorMsg());
			}

			echo "<h2 align=\"center\">".$ling['equip_mover_sucess']."</h2><br />";
			$_GET['destino1']="";
			$_GET['destino']="";
			$_GET['motivo']="";
		}
	}
}

echo "
<form action=\"manusis.php\" name=\"movequipa\"method=\"get\">
<input type=\"hidden\" name=\"id\" value=\"$id\">
<input type=\"hidden\" name=\"op\" value=\"$op\">
<input type=\"hidden\" name=\"exe\" value=\"$exe\">
<p>".$ling['selecionar_equip'].":<br>";

FormSelectD("COD","DESCRICAO",EQUIPAMENTOS,$_GET['objeto'],"objeto","objeto","MID",0,"","document.movequipa.submit()","","S","COD","");

echo "</select></p>";
if ($_GET['objeto'] != "") {
	$resultado=$dba[$tdb[EQUIPAMENTOS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID_CONJUNTO,MID_MAQUINA,MID FROM ".EQUIPAMENTOS." WHERE MID = ".$_GET['objeto']);
	$campo = $resultado -> fields;
	echo "<div id=\"lt_tabela\"><br />
    <table>
    <tr>
    <th>".$tdb[MAQUINAS]['DESC']."</th>
    <th>".$tdb[MAQUINAS_CONJUNTO]['DESC']."</th>
    <th>".$tdb[EQUIPAMENTOS]['COD']."</th>
    <th>".$tdb[EQUIPAMENTOS]['DESCRICAO']."</th>
    </tr>
    <tr class=\"cor2\">";
	
	$tmp_pos=VoltaValor(MAQUINAS_CONJUNTO,'DESCRICAO','MID',$campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba']);
	$tmp_l3=VoltaValor(MAQUINAS,'DESCRICAO','MID',$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']);
	
	echo "<td>$tmp_l3</td><td>$tmp_pos</td><td>".$campo['COD']."</td><td>".$campo['DESCRICAO']."</td>
    </tr></table></div><br />
    ";
					
	echo "<p>".$ling['selecionar_destino'].":<br>";
	FormSelectD("COD","DESCRICAO",MAQUINAS,$_GET['destino1'],"destino1","destino1","MID",0,"","document.movequipa.submit()","","S","COD",$ling['equip_nao_alocadas']);

	if ($_GET['destino1'] != "") {
        echo "<br />";
		FormSelectD("TAG","DESCRICAO",MAQUINAS_CONJUNTO,$_GET['destino'],"destino","destino","MID",0,"","document.movequipa.submit()","WHERE MID_MAQUINA = '".$_GET['destino1']."'" ,"N","TAG","");

		$tmp=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID_SETOR,MID FROM ".MAQUINAS." WHERE MID = '".$_GET['destino1']."'");
		$campo=$tmp->fields;
		echo "<div id=\"lt_tabela\"><br /><table>
        <tr>
        <th>".$tdb[SETORES]['DESC']."</th>
        <th>".$tdb[MAQUINAS]['DESC']."</th>
        <th>".$tdb[MAQUINAS_CONJUNTO]['DESC']."</th>
        </tr>
        <tr class=\"cor2\">";
		$setor=VoltaValor(SETORES,'DESCRICAO','MID',$campo['MID_SETOR'],$tdb[SETORES]['dba']);
		if (VoltaValor(MAQUINAS_CONJUNTO,'MID_MAQUINA','MID',$_GET['destino'],$tdb[MAQUINAS_CONJUNTO]['dba']) != $_GET['destino1']) $conjunto="";
		elseif ($_GET['destino'] != "") $conjunto=VoltaValor(MAQUINAS_CONJUNTO,'DESCRICAO','MID',$_GET['destino'],$tdb[MAQUINAS_CONJUNTO]['dba']);
		echo "<td>$setor</td><td>".$campo['COD']."-".$campo['DESCRICAO']."</td><td>$conjunto</td></tr></table></div><br />";
	}
	
	echo "
	<p>".$ling['motivo'].":<br />
	<input type=\"text\" class=\"campo_text\" size=\"100\" name=\"motivo\"><br /><br /></p>\n
	<input type=\"submit\" class=\"botao\" name=\"enviar\" value=\"Movimentar\" /><br><br>";	


	echo "<input type=\"hidden\" name=\"id\" value=\"$id\" />
    <input type=\"hidden\" name=\"oq\" value=\"$oq\" />
    
    </td></tr></table></form> ";

}
?>