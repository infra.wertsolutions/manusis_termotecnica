<?
/**
* Manusis 3.0
* @author Mauricio Barbosa 
* @author Fernando Cosentino
* Parametros para o Ajax da vis�o em arvore
*/
// Fun��es do Sistema
if (!require("../../lib/mfuncoes.php")) die ("".$ling['arq_estrutura_nao_pode_ser_carregado']."");
// Configura��es
elseif (!require("../../conf/manusis.conf.php")) die ("".$ling['arq_configuracao_nao_pode_ser_carregado']."");
// Idioma
elseif (!require("../../lib/idiomas/".$manusis['idioma'][0].".php")) die ("".$ling['arq_idioma_nao_pode_ser_carregado']."");
// Biblioteca de abstra��o de dados
elseif (!require("../../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../../lib/bd.php")) die ($ling['bd01']);
elseif (!require("../../lib/forms.php")) die ("Imposs&iacute;vel continuar, arquivo de formul&aacute;rio n&atuilde;o pode ser carregado");

$op=$_SESSION[ManuSess]['va']['op'];
$id=$_SESSION[ManuSess]['va']['id'];
$exe=$_SESSION[ManuSess]['va']['exe'];
$oq=$_SESSION[ManuSess]['va']['oq'];

$icone_mais="<img vspace=4 hspace=4 src=\"imagens/icones/mais.gif\" alt=\" \" border=\"0\" />";
$icone_menos="<img vspace=4 hspace=4 src=\"imagens/icones/menos.gif\" alt=\" \" border=\"0\" />";
$icone_vasio="<img vspace=4 hspace=4  src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />";
$va_atu=$_GET['va_atu'];

// Atualizando o Setor (Localiza��o 2)
if ($va_atu == 'setor'){
	$setor_mid=(int)$_GET['mid'];
	$listamaq=ListaMaquina($setor_mid);
	for ($im=0; $listamaq[$im]['nome'] != ""; $im++) {
		$maquina=htmlentities(strtoupper($listamaq[$im]['nome']));
		$maquina_cod=$listamaq[$im]['cod'];
		$maquina_mid=$listamaq[$im]['mid'];
		$maquina_modelo=htmlentities(strtoupper($listamaq[$im]['modelo']));
		$listaconj=ListaConjunto($maquina_mid);
		if ($listaconj == 0) {
			echo "<li>
            <img vspace=4 hspace=4 src=\"imagens/icones/pi.png\" width=\"9\" heigth=\"9\" alt=\" \" border=\"0\" />
            <img src=\"imagens/icones/22x22/vazio.png\" alt=\"\" />
            <a ondblclick=\"abre_janela_form('MAQUINA','$maquina_mid','','2','$id','$exe','$op',".$form['MAQUINA'][0]['altura'].",".$form['MAQUINA'][0]['largura'].",'corpo','')\">$maquina_cod - $maquina <span style=\"font-size:9px\"><strong>$maquina_modelo</strong></span></a>
            </li>\n";
		}
		else {
			echo "<li>
            <img vspace=4 hspace=4 src=\"imagens/icones/mais.gif\" onclick=\"FecharMaquina('$maquina_mid',this);\" alt=\" \" border=\"0\" />
            <img id=\"imaq_$maquina_mid\" src=\"imagens/icones/22x22/local3.png\" alt=\"\" />
            <a ondblclick=\"abre_janela_form('MAQUINA','$maquina_mid','','2','$id','$exe','$op',".$form['MAQUINA'][0]['altura'].",".$form['MAQUINA'][0]['largura'].",'corpo','')\">$maquina_cod - $maquina <span style=\"font-size:9px\"><strong>$maquina_modelo</strong></span></a>
            <ul id=\"maq_$maquina_mid\"></ul>\n";
		}
	}
}

// Atualizando as Maquinas (Objeto de Manuten��o)
if ($va_atu == 'maq'){
	$maquina_mid=(int)$_GET['mid'];
	$listaconj=ListaConjunto($maquina_mid);
	for ($ic=0; $listaconj[$ic]['nome'] != ""; $ic++) {
		$conjunto=htmlentities(strtoupper($listaconj[$ic]['nome']));
		$conjunto_tag=htmlentities(strtoupper($listaconj[$ic]['tag']));
		$conjunto_mid=$listaconj[$ic]['mid'];
		$listaequip=ListaEquipamento($conjunto_mid);
		$listasubconj=ListaSubConjunto($conjunto_mid);
		if (($listaequip == 0) and ($listasubconj == 0)) 
		echo "<li>
		$icone_vasio 
		<img src=\"imagens/icones/22x22/tag_vazio.png\" alt=\"\" />
		<a href=\"manusis.php?id=1&op=1&exe=6&oq=$maquina_mid\">$conjunto_tag - $conjunto</a>
		</li>\n";
		else {
			echo "<li>
				<div>
				<img vspace=4 hspace=4 src=\"imagens/icones/menos.gif\" onclick=\"FecharConj('conj_$conjunto_mid',this);\" alt=\" \" border=\"0\" />

				<img src=\"imagens/icones/22x22/tag.png\" alt=\"\" />
				<a href=\"manusis.php?id=1&op=1&exe=6&oq=$maquina_mid\">$conjunto_tag - $conjunto</a>
				<ul id=\"conj_$conjunto_mid\" style=\"display:block\">\n";
			for ($ie=0; $listaequip[$ie]['nome'] != ""; $ie++) {
				$equipamento=htmlentities(strtoupper($listaequip[$ie]['nome']));
				$equipamento_cod=htmlentities(strtoupper($listaequip[$ie]['cod']));
				$equipamento_mid=$listaequip[$ie]['mid'];
				echo "<li><div id=\"equip_$equipamento_mid\">
						$icone_vasio
						<img src=\"imagens/icones/22x22/equip.png\" alt=\"\" />
						<a href=\"manusis.php?id=1&op=1&exe=7&oq=$equipamento_mid\">$equipamento_cod - $equipamento</a>
						</div></li>
						\n";
			}

			for ($ics=0; $listasubconj[$ics]['nome'] != ""; $ics++) {
				$sconjunto=htmlentities(strtoupper($listasubconj[$ics]['nome']));
				$sconjunto_tag=htmlentities(strtoupper($listasubconj[$ics]['tag']));
				$sconjunto_mid=$listasubconj[$ics]['mid'];
				$listaequip=ListaEquipamento($sconjunto_mid);
				if ($listaequip == 0)  
					echo "<li>
					$icone_vasio 
					<img src=\"imagens/icones/22x22/tag_vazio.png\" alt=\"\" />
					<a href=\"manusis.php?id=1&op=1&exe=6&oq=$maquina_mid\">$sconjunto_tag - $sconjunto</a>
					</li>\n";
				else {
					echo "<li>
						<div id=\"sconj_$sconjunto_mid\">
						<a onclick=\"FecharSConj('sconj_$sconjunto_mid')\">$icone_mais</a>
						<img src=\"imagens/icones/22x22/tag.png\" alt=\"\" />
						<a href=\"manusis.php?id=1&op=1&exe=6&oq=$maquina_mid\">$sconjunto_tag - $sconjunto</a>
						<ul>\n";
					for ($ie=0; $listaequip[$ie]['nome'] != ""; $ie++) {
						$equipamento=htmlentities(strtoupper($listaequip[$ie]['nome']));
						$equipamento_cod=htmlentities(strtoupper($listaequip[$ie]['cod']));
						$equipamento_mid=$listaequip[$ie]['mid'];
						echo "<li><div id=\"equip_$equipamento_mid\">
							  $icone_vasio
								<img src=\"imagens/icones/22x22/equip.png\" alt=\"\" />
								<a href=\"manusis.php?id=1&op=1&exe=7&oq=$equipamento_mid\">$equipamento_cod - $equipamento</a>
								</div></li>\n";
					}
					// Equipamento SUB CONJUNTO
					echo "</ul></div></li>\n";
				}
				// Equipamento SUB CONJUNTO
			}
			// Equipamento SUB CONJUNTO
			echo "</ul></div></li>\n";
		}
		echo "</ul></div></li>\n";
	}
}
?>
