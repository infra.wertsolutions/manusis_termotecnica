<?


// VARIAVEIS UTEIS
$form_almox = "ALMOXARIFADO";
$form_mat = "MATERIAIS";

// Usadas no SQL
$filtro_sql = "";
$ordem_sql = ", M.COD";
$direcao_sql = "ASC";

// FILTRO POR EMPRESA
$fil_emp = Volta_Permissao_Empresa();
$filtro_emp = (count($fil_emp) > 0)? " WHERE A.MID_EMPRESA IN (" . implode(", ", $fil_emp) . ")" : (($_SESSION[ManuSess]['user']['MID']=='ROOT')?'':"  WHERE A.MID_EMPRESA = 0");

// BUSCANDO OS ALMOXARIFADOS A SEREM MOSTRADOS
$list_amox = array();
$sql = "SELECT A.* FROM " . ALMOXARIFADO . " A $filtro_emp ORDER BY A.COD";
if (! $rs = $dba[$tdb[ALMOXARIFADO]['dba']] -> Execute($sql)) {
	erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __FILE__ . "<br />" . $dba[$tdb[ALMOXARIFADO]['dba']] -> ErrorMsg() . "<br />" . $sql);
}
if(! $rs->EOF) {
	$list_amox = $rs->getrows();
}

// FILTROS DO SELECT
$filtro_sql = (count($fil_emp) > 0)? " AND A.MID_EMPRESA IN (" . implode(", ", $fil_emp) . ")" : (($_SESSION[ManuSess]['user']['MID']=='ROOT')?'':"  AND A.MID_EMPRESA = 0");
// FILTRO DE TEXTO
if (isset ($_GET['filtro_texto'])) {
	$_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['filtro_texto'] = $_GET['filtro_texto'];
}
// Recuperando da sess�o
$filtro_texto = $_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['filtro_texto'];

// Montando os filtros
if ($filtro_texto != "") {
	$filtro_sql .= " AND (M.COD LIKE '%$filtro_texto%' OR M.DESCRICAO LIKE '%$filtro_texto%')";
}


// ORDENA��O
if (isset ($_GET['ordem'])) {
	$_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['ordem'] = $_GET['ordem'];
	$_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['direcao'] = ($_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['direcao'] == "" or $_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['direcao'] == "DESC")? "ASC" : "DESC";
}
// Recuperando da sess�o
$ordem = $_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['ordem'];
$direcao_sql = $_SESSION[ManuSess]['lt'][MATERIAIS_ALMOXARIFADO]['direcao'];

// Montando o Order By
if ($ordem != "") {
	$ordem_sql = ", $ordem";
}

$list_mat = array();
// BUSCANDO OS MATERIAIS
$sql = "SELECT M.MID, M.COD, M.DESCRICAO, M.UNIDADE, M.ESTOQUE_ATUAL AS TOTAL, MA.ESTOQUE_ATUAL, A.MID AS MID_ALMOX FROM " . MATERIAIS . " M, " . MATERIAIS_ALMOXARIFADO . " MA, " . ALMOXARIFADO . " A WHERE M.MID = MA.MID_MATERIAL AND MA.MID_ALMOXARIFADO = A.MID $filtro_sql ORDER BY A.COD $ordem_sql $direcao_sql";
if (! $rs = $dba[$tdb[MATERIAIS]['dba']] -> Execute($sql)) {
	erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __FILE__ . "<br />" . $dba[$tdb[MATERIAIS]['dba']] -> ErrorMsg() . "<br />" . $sql);
}
while (! $rs->EOF) {
	$cc = $rs->fields;
	// Salvando em array para facilitar
	$list_mat[$cc['MID']]['def'] = $cc;
	$list_mat[$cc['MID']][$cc['MID_ALMOX']] = $cc['ESTOQUE_ATUAL'];

	$rs->MoveNext();
}

echo "<div id=\"mod_menu\">
<div></div>
</div>
<br clear=\"all\" />
<div>
<div id=\"lt\">
<div id=\"lt_cab\">
<h3>" . $tdb[MATERIAIS_ALMOXARIFADO]['DESC'] . "</h3>";
if ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2)) {
	echo "<span id=\"lt_menu\" onmouseover=\"menu_sobre('ltmenu')\" onmouseout=\"menu_fora('ltmenu')\">Op&ccedil;&otilde;es
<ul id=\"m_ltmenu\">
<li><a href=\"javascript:abre_janela_form('$form_almox','','0','1','$id','0','$op', " . $form[$form_almox][0]['altura'] . ", " . $form[$form_almox][0]['largura'] . ",'')\"><img src=\"imagens/icones/22x22/novo.png\" border=0 align=\"middle\" alt=\" \" title=\" {$ling['novo']} \" /> {$ling['novo']} </a></li>
<li><a href=\"relatorio.php?tb=materiais_almoxarifado&con=0\" target=\"_blank\"><img src=\"imagens/icones/22x22/imprimir.png\" border=0 align=\"middle\" alt=\" \" title=\" {$ling['relatorio']} \" /> {$ling['relatorio']} </a></li>
</ul>
</span>";
}
echo "</div>
<br clear=\"all\" />
<div id=\"lt_forms\">
<form action=\"manusis.php\" method=\"get\">
<fieldset>
<legend>{$ling['localizar']}</legend>
<label for=\"filtro_texto\">{$ling['buscar']}</label>
<input type=\"text\" class=\"campo_text\" value=\"$filtro_texto\" id=\"filtro_texto\" name=\"filtro_texto\" size=\"40\"/>
<input class=\"botao\" type=\"submit\" value=\"Ok\" name=\"localizar\"/>
<input type=\"hidden\" value=\"$id\" name=\"id\"/>
<input type=\"hidden\" value=\"$op\" name=\"op\"/>
<input type=\"hidden\" value=\"$oq\" name=\"oq\"/>
<input type=\"hidden\" value=\"$exe\" name=\"exe\"/>
</fieldset>
</form>
</div>
<br clear=\"all\" />
<br clear=\"all\" />
<div id=\"lt_tabela\">
<table id=\"lt_tabela_\">
<tr>
<th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordem=M.COD')\" title=\"".$ling['definir_ordem']."\">" . $tdb[MATERIAIS]['COD'] . "</a></th>
<th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordem=M.DESCRICAO')\" title=\"".$ling['definir_ordem']."\">" . $tdb[MATERIAIS]['DESCRICAO'] . "</a></th>";

// Mostrando uma coluna para cada ALMOX
foreach ($list_amox as $amox) {
	echo "<th title=\"" . htmlentities($amox['DESCRICAO']) . "\">" . htmlentities($amox['COD']) . "";
	// Verificando se o usu�rio tem permiss�o
	if ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2)) {
		echo "<a href=\"javascript:abre_janela_form('$form_almox','" . $amox['MID'] . "','0','2','$id','0','$op'," . $form[$form_almox][0]['altura'] . ", " . $form[$form_almox][0]['largura'] . ",'','')\">
        <img height=\"15\" border=\"0\" title=\" {$ling['editar']} \" alt=\" \" src=\"imagens/icones/22x22/editar.png\"/>
        </a>";
	}
	if (VoltaPermissao($id, $op) == 1) {
		echo "<a onclick=\"return confirma_delete('".ALMOXARIFADO."','$oq')\" href=\"manusis.php?id=$id&op=$op&exe=$exe&oq=" . $amox['MID'] . "&deleta=" . ALMOXARIFADO . "\">
        <img height=\"15\" border=\"0\" alt=\" \" title=\" {$ling['remover']} \" src=\"imagens/icones/22x22/del.png\"/>
        </a>";
	}
	echo "</th>";
}

echo "<th><a href=\"javascript:atualiza_area('corpo','manusis.php?st=1&id=$id&op=$op&exe=$exe&oq=$oq&ordem=M.ESTOQUE_ATUAL')\" title=\"".$ling['definir_ordem']."\">" . $tdb[MATERIAIS]['ESTOQUE_ATUAL'] . "</a></th>
<th>{$ling['opcoes']}</th>
</tr>";

// Listando
$i = 0;
foreach ($list_mat as $mid_mat => $dados) {
	$cc = $dados['def'];

	echo "<tr class=\"cor2\">
    <td>" . $cc['COD'] . "</td>
    <td>" . htmlentities($cc['DESCRICAO']) . "</td>";

	// Mostrando colunas em branco
	foreach ($list_amox as $amox) {
		if($dados[$amox['MID']] != "") {
			echo "<td>" . $dados[$amox['MID']] . " " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, 'COD', 'MID', $cc['UNIDADE'])) . "</td>";
		}
		else {
			echo "<td><center>N/A</center></td>";
		}
	}


	echo "<td>" . $cc['TOTAL'] . " " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, 'COD', 'MID', $cc['UNIDADE'])) . "</td>
    <td>";
	if ((VoltaPermissao($id, $op) == 1) or (VoltaPermissao($id, $op) == 2)) {
		 
		echo "<a href=\"javascript:abre_janela_form('$form_mat','" . $cc['MID'] . "','0','2','$id','0','$op'," . $form[$form_mat][0]['altura'] . ", " . $form[$form_mat][0]['largura'] . ",'','')\">
        <img border=\"0\" title=\"{$ling['editar']}\" alt=\" \" src=\"imagens/icones/22x22/editar.png\"/>
        </a>";
	}
	echo "</td>
    </tr>";
	$i++;
}

echo "</table>
</div>
<div id=\"lt_rodape\">{$ling['registros_encontrados']}: $i
<b><a href=\"javascript:janela('logs.php?id=" . MATERIAIS_ALMOXARIFADO . "', 'parm', 500,400)\">{$ling['ultimas_alteracoes']}</a>
</div>
</div>
</div>
</div>";


?>
