<?
/** 
* Cadastros Auxiliares, tabelas que n�o pertencem a modulo algum ou parametros para uma tabela como tipo de seriv�o, classes etc.
* 
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package cadastro
* @subpackage estrutura
*/

$table = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"  align=\"center\">";
$td_a = "<td align=\"left\" valign=\"top\" width=\"20%\">";
$td_f = "</td>";

echo "<br />";
if ($exe == "") $exe=1;
echo "<div id=\"lt\">";
echo "<tr> <td align=\"center\">";

echo 
"<fieldset><legend>{$ling['estoque']}</legend>

{$table}<tr>{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=1\">
<img src=\"imagens/icones/22x22/fam_materiais.png\" border=\"0\" /> ".
(($exe == 1)?"<strong>{$tdb[MATERIAIS_FAMILIA]['DESC']}</strong>":"{$tdb[MATERIAIS_FAMILIA]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=2\">
<img src=\"imagens/icones/22x22/fam_materiais.png\" border=\"0\" /> ".
(($exe == 2)?"<strong>{$tdb[MATERIAIS_SUBFAMILIA]['DESC']}</strong>":"Sub-Familia de {$tdb[MATERIAIS_SUBFAMILIA]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=3\">
<img src=\"imagens/icones/22x22/material.png\" border=\"0\" /> ".
(($exe == 3)?"<strong>{$tdb[MATERIAIS_CRITICIDADE]['DESC']}</strong>":"{$tdb[MATERIAIS_CRITICIDADE]['DESC']}").
"</a>

{$td_f}{$td_a}

<a class=\"link\" href=\"manusis.php?id=$id&op=$op&exe=4\">
<img src=\"imagens/icones/22x22/unidades.png\" border=\"0\" /> ".
(($exe == 4)?"<strong>{$tdb[MATERIAIS_UNIDADE]['DESC']}</strong>":"{$tdb[MATERIAIS_UNIDADE]['DESC']}").
"</a>
{$td_f}
</tr>

</table>

</fieldset>
";

if ($exe == 1) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    ListaTabela (MATERIAIS_FAMILIA,"MID",$campos,"MATERIAL_FAMILIA","","",1,1,1,1,1,1,1,1,1,1,"");
}
elseif ($exe == 2) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    $campos[2]="MID_FAMILIA";
    ListaTabela (MATERIAIS_SUBFAMILIA,"MID",$campos,"MATERIAL_SUBFAMILIA","","",1,1,1,1,1,1,1,1,1,1,"");
}

elseif ($exe == 3) {
    $campos[0]="DESCRICAO";
    ListaTabela (MATERIAIS_CRITICIDADE,"MID",$campos,"MATERIAL_CRITICIDADE","","",1,1,1,1,1,1,1,1,1,1,"");
}

elseif ($exe == 4) {
    $campos[0]="COD";
    $campos[1]="DESCRICAO";
    ListaTabela (MATERIAIS_UNIDADE,"MID",$campos,"MATERIAL_UNIDADE","","",1,1,1,1,1,1,1,1,1,1,"");
}

echo "</td>
</td></tr></table></div>
<br>";

?>
