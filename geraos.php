<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Autor: Fernando Cosentino
* Nota: Arquivo Base
*/
// Fun��es de Estrutura
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);

elseif (!require("lib/autent.php")) die ($ling['autent01']);
$tb=$_GET['tb'];
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
$qto=(int)$_GET['qto'];
$cc=$_GET['cc'];
$cris=(int)$_GET['cris'];
$criv=$_GET['criv'];
$cric=$_GET['cric'];
$cris2=(int)$_GET['cris2'];
$criv2=$_GET['criv2'];
$cric2=$_GET['cric2'];
$or=$_GET['or'];
$cc=$_POST['cc'];
$filtro_tipo=(int)$_GET['filtro_tipo'];
$data_prog=$_GET['data_prog'];
$usa_imagem = (int)$_REQUEST['usa_imagem'];
//$txt.= cab_os(1);
$id=(int)$_GET['id'];
$oq=(int)$_GET['oq'];

$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);

    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}

if (($_GET['word'] == "") and ($_GET['html'] == '') and ($id != 3) and ($id != 4)){
    $tipo=(int)$_GET['tipo'];
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
    if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
    echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['imprimir_ordens']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"geraos.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>{$ling['localizacoes']}</legend>
    <label class=\"campo_label \" for=\"filtro_area\">".$tdb[AREAS]['DESC'].":</label>";
    echo " <select name=\"filtro_area\" id=\"filtro_area\" class=\"campo_select\" onchange=\"atualiza_area2('setor','parametros.php?id=2&dir=.&area=' + this.options[this.selectedIndex].value)\">";
    $tmp=$dba[$tdb[AREAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".AREAS." ORDER BY COD ASC");
    echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
    while (!$tmp->EOF) {
        $campo=$tmp->fields;
        if ($_GET['filtro_area'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
        else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
        $tmp->MoveNext();
    }
    echo "</select>
    <br clear=\"all\" />
    <div id=\"setor\"><label class=\"campo_label \" for=\"filtro_setor\">".$tdb[SETORES]['DESC'].":</label>";
    echo " <select name=\"filtro_setor\" id=\"filtro_setor\" class=\"campo_select\" onchange=\"atualiza_area2('maq','parametros.php?id=3&dir=.&setor=' + this.options[this.selectedIndex].value)\">";
    $tmp=$dba[$tdb[SETORES]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".SETORES." ORDER BY COD ASC");
    echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
    while (!$tmp->EOF) {
        $campo=$tmp->fields;
        if ($_GET['filtro_setor'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
        else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
        $tmp->MoveNext();
    }
    echo "</select></div>
    <br clear=\"all\" />";
    echo "<label class=\"campo_label \" for=\"filtro_maq_fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
    $primeirocampo=strtoupper($ling['todos']);
    FormSelect("filtro_maq_fam",MAQUINAS_FAMILIA,$_GET['filtro_maq_fam'],"DESCRICAO","MID",$tdb[MAQUINAS_FAMILIA]['dba'],0);

    echo "<br clear=\"all\" />
    <div id=\"maq\"><label class=\"campo_label \" for=\"filtro_maq\">".$tdb[MAQUINAS]['DESC'].":</label>";
    echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\">";
    $tmp=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".MAQUINAS." ORDER BY COD ASC");
    echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
    while (!$tmp->EOF) {
        $campo=$tmp->fields;
        if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
        else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
        $tmp->MoveNext();
    }
    echo "</select></div>
    <br clear=\"all\" />";
    echo "<label class=\"campo_label \" for=\"filtro_cc\">".$tdb[CENTRO_DE_CUSTO]['DESC'].":</label>";
    $primeirocampo=strtoupper($ling['todos']);
    FormSelect("filtro_cc",CENTRO_DE_CUSTO,$_GET['filtro_cc'],"DESCRICAO","MID",$tdb[CENTRO_DE_CUSTO]['dba'],0);

    echo "</fieldset>";

    echo "<fieldset><legend>".$ling['tipo_servicos']."</legend>";
    echo "<input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t1\" value=\"1\" onchange=\"atualiza_area2('serv','parametros.php?id=4&tipo=1')\" />
    <label for=\"t1\">".$ling['somente_sistematico']."</label>
    <input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t2\" value=\"2\" onchange=\"atualiza_area2('serv','parametros.php?id=4&tipo=2')\" />
    <label for=\"t2\">".$ling['somente_nao_sistematico']."</label>
    <input class=\"campo_check\" checked=\"checked\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t3\" value=\"3\" onchange=\"atualiza_area2('serv','parametros.php?id=4&tipo=3')\" />
    <label for=\"t3\">{$ling['rel_desc_todos']}</label>
    <div id=\"serv\"></div>
    </fieldset>";

    echo "<fieldset><legend>".$ling['responsavel']."</legend>";
    echo "<label class=\"campo_label \" for=\"filtro_resp\">".$tdb[FUNCIONARIOS]['DESC'].":</label>";
    $primeirocampo=strtoupper($ling['todos']);
    FormSelect("filtro_resp",FUNCIONARIOS,$_GET['filtro_resp'],"NOME","MID",$tdb[FUNCIONARIOS]['dba'],0);
    echo "</fieldset>";

    echo "<fieldset><legend>".$ling['datas']."</legend>";
    FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
    echo "<br clear=\"all\" />";
    FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");
    echo "
    </fieldset>";

    echo "<fieldset><legend>".$ling['anexos']."</legend>
<label class=\"campo_label \" for=\"usa_imagem\">{$ling['incluir_img_anexa']}:</label>
<input type=\"checkbox\" name=\"usa_imagem\" id=\"usa_imagem\" value=\"1\" ";
    if (!$usa_imagem) echo "checked=\"checked\" ";
    echo "/>
</fieldset>
<br />
<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"word\" value=\"".$ling['relatorio_doc']."\" />
<input class=\"botao\" type=\"submit\" name=\"html\" value=\"".$ling['relatorio_html']."\" />
</form><br />
</div>
</div>
</body>
</html>";
}
elseif($id ==3) {
    $sql="SELECT MID,NUMERO FROM ".ORDEM_PLANEJADO." WHERE MID = $oq ORDER BY NUMERO ASC";
    if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $os[$i]['prog']=$campo['NUMERO'];
        $os[$i]['os']=osdef($campo['MID']);
        $resultado -> MoveNext();
        $i++;
    }
    
    exos_html($os);
}
elseif($id ==4) {
    $sql="SELECT MID,NUMERO FROM ".ORDEM_PLANEJADO." WHERE MID = '$oq' ORDER BY NUMERO ASC";
    if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $os[$i]['prog']=$campo['NUMERO'];
        $os[$i]['os']=osdef($campo['MID']);
        $resultado -> MoveNext();
        $i++;
    }
    exos_html($os);
}
else {
    $datai= DataSQL($_GET['datai']);
    $dataf=DataSQL($_GET['dataf']);
    $obj=$_GET['obj'];
    $tipo=$_GET['tipo'];
    $tipo_serv=(int)$_GET['filtro_tipo_serv'];
    $filtro_servico=(int)$_GET['filtro_servico'];
    $fil = '';
    if ($_GET['filtro_area']) $fil .= " AND MID_AREA = '".(int)$_GET['filtro_area']."'";
    if ($_GET['filtro_setor']) $fil .= " AND MID_SETOR = '".(int)$_GET['filtro_setor']."'";
    if (($_GET['filtro_maq']) and ($filtro_servico != 2)) $fil .= " AND MID_MAQUINA = '".(int)$_GET['filtro_maq']."'";
    if ($_GET['filtro_maq']) $film .= " AND MID_MAQUINA = '".(int)$_GET['filtro_maq']."'";
    if ($_GET['filtro_cc']) $fil .= " AND CENTRO_DE_CUSTO = '".(int)$_GET['filtro_cc']."'";
    if ($_GET['filtro_resp']) $fil .= " AND RESPONSAVEL = '".(int)$_GET['filtro_resp']."'";
    if ($_GET['filtro_maq_fam']) {
        $maq_fam = (int)$_GET['filtro_maq_fam'];
        $fam_fil = '';
        $sql="SELECT MID FROM ".MAQUINAS." WHERE FAMILIA = '$maq_fam' ORDER BY MID ASC";
        if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
            erromsg("SQL ERROR .<br>".$dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg()."<br><br>$sql"); exit;
        }
        while (!$resultado->EOF) {
            $campo=$resultado->fields;
            AddStr($fam_fil,' OR ',"MID_MAQUINA = '".$campo['MID']."'");
            $resultado -> MoveNext();
        }
        if (($fam_fil) and ($filtro_servico != 2)) $fil .= " AND ($fam_fil)";
        if ($fam_fil) $film .= " AND ($fam_fil)";
    }
    if ($_GET['filtro_tipo_serv']) {
        $tipo_serv=(int)$_GET['filtro_tipo_serv'];
        $filtro_servico=(int)$_GET['filtro_servico'];
        if ($tipo_serv == 1) {
            if ($filtro_servico == 0) $fil .= " AND (TIPO != '0' OR TIPO != '4' OR TIPO != '')";
            else $fil.=" AND TIPO = '$filtro_servico'";
        }
        if ($tipo_serv == 2) {
            $fil .= " AND (TIPO = '0' OR TIPO IS NULL)";
            if ($filtro_servico) $fil.=" AND TIPO_SERVICO = '$filtro_servico'";
            //die($fil);
        }
    }

    if ($filtro_servico == 2) {
        $sql="SELECT MID_ORDEM FROM ".ORDEM_PLANEJADO_LUB." WHERE 1 $film";
        if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
            erromsg("SQL ERROR .<br>".$dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg()."<br><br>$sql"); exit;
        }
        while (!$resultado->EOF) {
            $campo=$resultado->fields;
            AddStr($fam_fil2,' OR ',"MID  = '".$campo['MID_ORDEM']."'");
            $resultado -> MoveNext();
        }
        if ($fam_fil2) $fil .= " AND ($fam_fil2)";
    }
    $where = "";
    $and = "";
    if($datai)
    {
      $where .= " AND DATA_PROG >= '$datai'";   

    }
    if($dataf)
    {
      $where .= " AND DATA_PROG <= '$dataf'";   
    }

    $sql="SELECT MID,NUMERO FROM ".ORDEM_PLANEJADO." WHERE STATUS = 1  $where $fil ORDER BY NUMERO ASC";


    if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    $i=0;
    while (!$resultado->EOF) {
        $campo=$resultado->fields;
        $os[$i]['prog']=$campo['NUMERO'];
        $os[$i]['os']=osdef($campo['MID']);
        $resultado -> MoveNext();
        $i++;
    }
    
    if ($_GET['word'] != ''){
        exos_word($os);
    }
    else {
        exos_html($os);
                  
    }
}

function exos_html($os) {
    global $manusis;
    $doc ="<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
xmlns:o=\"urn:schemas-microsoft-com:office:office\"
xmlns:w=\"urn:schemas-microsoft-com:office:word\"
xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
<meta name=ProgId content=Word.Document>
<meta name=Generator content=\"Microsoft Word 9\">
<meta name=Originator content=\"Microsoft Word 9\">
<link rel=File-List href=\"Doc1_arquivos/filelist.xml\">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author> HP</o:Author>
  <o:Template>Normal</o:Template>
  <o:LastAuthor> HP</o:LastAuthor>
  <o:Revision>1</o:Revision>
  <o:TotalTime>14</o:TotalTime>
  <o:LastPrinted>2006-03-24T19:52:00Z</o:LastPrinted>
  <o:Created>2006-03-24T19:40:00Z</o:Created>
  <o:LastSaved>2006-03-24T19:54:00Z</o:LastSaved>
  <o:Company>MANUSYSTEM</o:Company>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:GrammarState>Clean</w:GrammarState>
  <w:DrawingGridHorizontalSpacing>6 pt</w:DrawingGridHorizontalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>2</w:DisplayHorizontalDrawingGridEvery>
  <w:DisplayVerticalDrawingGridEvery>2</w:DisplayVerticalDrawingGridEvery>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:ApplyBreakingRules/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-parent:\"\";
    margin:0in;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:9.0pt;
    font-family:\"Arial\";
    mso-fareast-font-family:\"Arial\";}";
    for ($i=0; $os[$i]['os'] != ""; $i++) {
        $doc.="@page Section$i
    {size:595.45pt 841.7pt;
    margin:.5in .5in .5in .5in;
    mso-header-margin:35.3pt;
    mso-footer-margin:35.3pt;
    mso-paper-source:0;}
div.Section$i
    {page:Section$i;
    }\n";
    }
    $doc.="
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
    {mso-style-name:\"Tabela normal\";
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    mso-style-noshow:yes;
    mso-style-parent:\"\";
    mso-padding-alt:0in 5.4pt 0in 5.4pt;
    mso-para-margin:0in;
    mso-para-margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:9.0pt;
    font-family:verdana,arial,helvetica,sans-serif;
    }
table.MsoTableGrid
    {mso-style-name:\"Tabela com grade\";
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    border:solid windowtext 1.0pt;
    mso-border-alt:solid windowtext .5pt;
    mso-padding-alt:0in 5.4pt 0in 5.4pt;
    mso-border-insideh:.5pt solid windowtext;
    mso-border-insidev:.5pt solid windowtext;
    mso-para-margin:0in;
    mso-para-margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:9.0pt;
    font-family:\"Arial\";}
td.tdborder {
border:solid black 1.0pt;
mso-border-alt:solid black .75pt;
padding:2.25pt 2.25pt 2.25pt 2.25pt
}

</style>
<![endif]-->
<!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext=\"edit\" spidmax=\"2050\"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext=\"edit\">
  <o:idmap v:ext=\"edit\" data=\"1\"/>
 </o:shapelayout></xml>
<![endif]-->";

// Apenas se autorizado
if ($manusis['os']['impressao_nova'] == 1) {
    $doc .= "<style>
    table {
        border: 0px;
        border-collapse:collapse;
    }

    table td,
    table th {
        border: 1px solid black;
    }
    
    .linha_grande td {
        height:20px;
    }

    table th {
            background-color:#ccc;
    }
    </style>";
}
$doc .= "</head>
<body lang=EN-US style='tab-interval:.5in'>";
    $co=count($os);
    for ($i=0; $os[$i]['os'] != ""; $i++) {
        // Quebra de p�gina
        if ($i > 0) {
            $doc .= "<br style=\"page-break-after: always;\" />";
        }
        
        $doc.="<div class=Section$i>\n";
        $doc.=$os[$i]['os'];
        $doc.="</div>";
    }
    $doc.="</body>\n</html>";
    echo $doc;
}
function exos_word($os) {
    global $manusis;
    //die($manusis['dir']['temporarios']."/relatorios.doc");
    $novo=fopen($manusis['dir']['temporarios']."/relatorioos.doc","w");
    $doc ="<html xmlns:v=\"urn:schemas-microsoft-com:vml\"
xmlns:o=\"urn:schemas-microsoft-com:office:office\"
xmlns:w=\"urn:schemas-microsoft-com:office:word\"
xmlns=\"http://www.w3.org/TR/REC-html40\">

<head>
<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">
<meta name=ProgId content=Word.Document>
<meta name=Generator content=\"Microsoft Word 10\">
<meta name=Originator content=\"Microsoft Word 10\">
<link rel=File-List href=\"ordemplan_arquivos/filelist.xml\">
<link rel=Edit-Time-Data href=\"ordemplan_arquivos/editdata.mso\">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
w\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<title>{$ling['manusis_ord_serv']}</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>{$ling['manusis']}</o:Author>
  <o:LastAuthor>{$ling['manusis']}</o:LastAuthor>
  <o:Revision>5</o:Revision>
  <o:TotalTime>300</o:TotalTime>
  <o:Created>2005-11-20T19:17:00Z</o:Created>
  <o:LastSaved>2005-12-09T15:28:00Z</o:LastSaved>
  <o:Pages>".count($os)."</o:Pages>
  <o:Words>141</o:Words>
  <o:Characters>763</o:Characters>
  <o:Company>Manusis</o:Company>
  <o:Lines>6</o:Lines>
  <o:Paragraphs>1</o:Paragraphs>
  <o:CharactersWithSpaces>903</o:CharactersWithSpaces>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:HyphenationZone>21</w:HyphenationZone>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
 </w:WordDocument>
</xml><![endif]-->
<style>
<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
    {mso-style-parent:\"\";
    margin:0cm;
    margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:9.0pt;
    font-family:\"Arial\";
    mso-fareast-font-family:\"Arial\";}
p
    {mso-margin-top-alt:auto;
    margin-right:0cm;
    mso-margin-bottom-alt:auto;
    margin-left:0cm;
    mso-pagination:widow-orphan;
    font-size:12.0pt;
    font-family:\"Arial\";
    mso-fareast-font-family:\"Arial\";}
span.SpellE
    {mso-style-name:\"\";
    mso-spl-e:yes;}
span.GramE
    {mso-style-name:\"\";
    mso-gram-e:yes;}";
    for ($i=0; $os[$i]['os'] != ""; $i++) {
        $doc.="@page Section$i {
            mso-page-orientation:portrait;
            size:21.0cm 842.0pt;
            margin:1.0cm 1.0cm 45.35pt 1.0cm;
            mso-header-margin:35.45pt;
            mso-footer-margin:35.45pt;
            mso-paper-source:0;
        }
        div.Section$i{
            page:Section$i;
        }\n";
    }
    $doc .="
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
    {mso-style-name:\"Tabela normal\";
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    mso-style-noshow:yes;
    mso-style-parent:\"\";
    mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
    mso-para-margin:0cm;
    mso-para-margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:10.0pt;
    font-family:\"Arial\";}
table.MsoTableGrid
    {mso-style-name:\"Tabela com grade\";
    mso-tstyle-rowband-size:0;
    mso-tstyle-colband-size:0;
    border:solid windowtext 1.0pt;
    mso-border-alt:solid windowtext .5pt;
    mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
    mso-border-insideh:.5pt solid windowtext;
    mso-border-insidev:.5pt solid windowtext;
    mso-para-margin:0cm;
    mso-para-margin-bottom:.0001pt;
    mso-pagination:widow-orphan;
    font-size:10.0pt;
    font-family:\"Times New Roman\";}
</style>
<![endif]-->";

    // Apenas se autorizado
    if ($manusis['os']['impressao_nova'] == 1) {
        $doc .= "<style>
        table {
            border: 0px;
            border-collapse:collapse;
        }

        table td,
        table th {
            border: 1px solid black;
        }

        table th {
                background-color:#ccc;
        }
        </style>";
    }

    $doc .= "</head>
    <body lang=PT-BR style='tab-interval:35.4pt'>";
    
    for ($i=0; $os[$i]['os'] != ""; $i++) {
        $doc.="<div class=Section$i>\n";
        $doc.=$os[$i]['os'];
        $doc.="</div>
        <span style='font-size:12.0pt;font-family:\"Arial\";mso-fareast-font-family:\"Arial\";mso-ansi-language:PT-BR;mso-fareast-language:PT-BR;mso-bidi-language:AR-SA'>
        <br clear=all style='page-break-before:always;mso-break-type:section-break'>
        </span>";
    }
    
    $doc."</body>\n</html>";
    fwrite($novo,"$doc");
    fclose($novo);
    echo "<html><body><script>this.location = '".$manusis['dir']['temporarios']."/relatorioos.doc'</script></body></html>";
}
function osdef($os) {
    global $manusis,$dba, $tdb, $ling, $usa_imagem;
    $ordr=$dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO." WHERE MID = '$os'");
    $ord = $ordr -> fields;
    $numero=$ord['NUMERO'];
    $tipo=$ord['TIPO'];
    $campo = array();
    if (($tipo != 0) or ($tipo != "") ) {
        $r=$dba[$tdb[PROGRAMACAO]['dba']] -> Execute("SELECT * FROM ".PROGRAMACAO." WHERE MID = '".$ord['MID_PROGRAMACAO']."'");
        $campo = $r -> fields;
    }
    
   
    // Verifica se � uma O.S de checkList

    $speedcheck = VoltaValor(TIPOS_SERVICOS, "SPEED_CHECK", "MID", $ord['TIPO_SERVICO'], $tdb[TIPOS_SERVICOS]['dba']);
    
  $speedcheck = ($speedcheck == 1) ? true : false;
    
    
    $doc ="
<table class=MsoNormalTable border=1 style='font-family: arial; font-size: 7pt' cellspacing=0 cellpadding=2 width=\"100%\" >
 <thead>
  <tr>
   <td valign=top align=\"right\">
    <img src=".$manusis['url']."temas/padrao/imagens/logoempresa.png border=0 vspace=2 hspace=2 align=center style=\"float:left\" />
   
   <span style='font-size:9pt'>
   <strong>".htmlentities(VoltaValor(EMPRESAS, "NOME", "MID", $ord['MID_EMPRESA'], 0))."</strong><br />
   <strong>{$ling['ordem_servico_m']}: ".$ord['NUMERO']."</strong></span><br/>
   <strong>{$ling['rel_desc_dt_prog']}: </strong>" . $ordr -> UserDate($ord['DATA_PROG'], 'd/m/Y')."<br/>";
    
    // REPROGRAMA��ES
    $obj=$dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']] -> Execute("SELECT DATA FROM ".ORDEM_PLANEJADO_REPROGRAMA." WHERE MID_ORDEM = '$os' ORDER BY DATA DESC");
    if (! $obj->EOF) {
        $obj_campo = $obj -> fields;
        $doc.= "<strong>{$ling['data_reprog']}:</strong>".$obj -> UserDate($obj_campo['DATA'],'d/m/Y')."<br>";
    }
   
   $doc .= "<strong>" . mb_strtoupper(unhtmlentities($tdb[ORDEM]['RESPONSAVEL'])) . ": </strong> ".htmlentities(strtoupper(VoltaValor(FUNCIONARIOS,"NOME","MID",$ord['RESPONSAVEL'],$tdb[FUNCIONARIOS]['dba'])))."<br> \n";

    if (($tipo == 0) or ($tipo == "") OR ($tipo == 4)) {
        $doc .= "<strong>" . mb_strtoupper(unhtmlentities($tdb[ORDEM]['SOLICITANTE'])) . ":</strong> ".($ord['SOLICITANTE'])."<br>";
    }
    elseif ($tipo == 4) {
        $doc .= " <strong>" . mb_strtoupper(unhtmlentities($tdb[ORDEM]['SOLICITANTE'])) . ": </strong>
    (".htmlentities(strtoupper(VoltaValor(USUARIOS,"USUARIO","MID",VoltaValor(SOLICITACOES,"USUARIO","MID",$campo['MID_PLANO'],$tdb[SOLICITACOES]['dba']),$tdb[USUARIOS]['dba'])).") ".htmlentities(strtoupper(VoltaValor(USUARIOS,"NOME","MID",VoltaValor(SOLICITACOES,"USUARIO","MID",$campo['MID_PLANO'],$tdb[SOLICITACOES]['dba']),$tdb[USUARIOS]['dba']))))."<br>";
    }
    
    $doc.="<strong>" . mb_strtoupper(unhtmlentities($tdb[ORDEM]['TIPO_SERVICO'])) . ": </strong>";
   
    if (($tipo == 4) or ($tipo == 0) or ($tipo == "")) {
        $doc .= htmlentities(VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$ord['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba']));
    }
    else {
        $doc .= htmlentities(VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$ord['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba']));
    }

    if ($tipo == 4) {
        $doc.="&nbsp;&nbsp;<strong>{$ling['sol']} {$ling['Nordem']}:</strong> ".VoltaValor(SOLICITACOES,"NUMERO","MID",$ord['MID_SOLICITACAO'],$tdb[SOLICITACOES]['dba']);
    }
    
    $doc.= "</td>
  </tr>";
  
    if (($tipo == 1) or ($tipo == 0) or ($tipo == "") or ($tipo == 4)) {
        $status=strtoupper(VoltaValor(MAQUINAS,"STATUS","MID",$ord['MID_MAQUINA'],$tdb[MAQUINAS]['dba']));
        $doc .="
        <tr> <td valign=middle colspan=4>
        
        <strong>".$tdb[ORDEM]['MID_SETOR']."</strong>: ". VoltaValor(SETORES,"DESCRICAO","MID",$ord['MID_SETOR'],$tdb[SETORES]['dba'])."<br>
        
        <strong>".$tdb[MAQUINAS]['LOCALIZACAO_FISICA']."</strong>:  ".strtoupper(VoltaValor(MAQUINAS,"LOCALIZACAO_FISICA","MID",$ord['MID_MAQUINA'],$tdb[MAQUINAS]['dba']))."<br>
        
        <strong>".$tdb[ORDEM]['MID_MAQUINA']."</strong>: ".htmlentities(VoltaValor(MAQUINAS,"DESCRICAO","MID",$ord['MID_MAQUINA'],$tdb[MAQUINAS]['dba']));
        
        if ($status != "") $doc.=" (".htmlentities(VoltaValor(MAQUINAS_STATUS,"DESCRICAO","MID",$status,$tdb[MAQUINAS_STATUS]['dba'])).")";
        
        $doc.=" <strong>".$tdb[MAQUINAS]['MODELO']."</strong>:  ".htmlentities(VoltaValor(MAQUINAS,"MODELO","MID",$ord['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])) . "
         <strong>".$tdb[MAQUINAS]['CLASSE']."</strong>: ".htmlentities(VoltaValor(MAQUINAS_CLASSE,"DESCRICAO","MID",VoltaValor(MAQUINAS,"CLASSE","MID",$ord['MID_MAQUINA'],$tdb[MAQUINAS]['dba']),$tdb[MAQUINAS_CLASSE]['dba']))."<br>
        <strong>".$tdb[ORDEM]['MID_CONJUNTO']."</strong>:  ".htmlentities(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$ord['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba']))."<br />";

        if ($ord['MID_EQUIPAMENTO']){
            $doc.= "<strong>{$tdb[EQUIPAMENTOS]['DESC']}:</strong> ".htmlentities(VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$ord['MID_EQUIPAMENTO'],0))."<br>";
        }
   $doc.= "</td>";
    }
    $doc.="</tr></thead>
     <tr>
  <td valign=top colspan=4 height=\"70\">
  <strong>".$tdb[ORDEM]['TEXTO'].":</strong><br>";
    
    if ($tipo == 1) {
        $doc.= nl2br(htmlentities(VoltaValor(PLANO_PADRAO,"OBSERVACAO","MID",$campo['MID_PLANO'],$tdb[PLANO_PADRAO]['dba'])))."<br>";
    }
    elseif ($tipo == 2) {
        $doc.= nl2br(htmlentities(VoltaValor(PLANO_ROTAS,"OBSERVACAO","MID",$campo['MID_PLANO'],$tdb[PLANO_ROTAS]['dba'])))."<br>";
    }
    elseif ($tipo == 4) {
        $doc .= nl2br(htmlentities($ord['TEXTO']));
    }
    elseif ($ord['TEXTO'] != "") { 
        $doc.= "".nl2br(htmlentities($ord['TEXTO']));
    }
    
    $doc.="
  </td>
 </tr>";
 
    // PREVIS�O DE M�O DE OBRA E MATERIAIS
        $doc .="
 <tr>
 <td colspan=4>
 <table style=\"font-size:7pt;font-family:arial\" cellspacing=0 cellpadding=0 width=\"100%\">
 <tr>
 <td width=\"50%\" valign=top style=\"border:0px;padding-right:5px;\">

  <table style=\"font-size:7pt;font-family:arial\" border=1 cellspacing=0 cellpadding=1 width=\"100%\">
   <tr style='mso-yfti-irow:0;'>
    <th width=\"40%\" valign=top>{$tdb[ORDEM_MO_PREVISTO]['MID_ESPECIALIDADE']}</th>
    <th valign=top>Qtd.</th>
    <th valign=top>{$tdb[ORDEM_MO_PREVISTO]['TEMPO']} (h)</th>
   </tr>";
        $obj=$dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute("SELECT * FROM ".ORDEM_MO_PREVISTO." WHERE MID_ORDEM = '".$os."'");

        $imao=0;
        while (! $obj->EOF) {
            $obj_campo = $obj -> fields;
            
            $doc.= "<tr>
            <td>".htmlentities(VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$obj_campo['MID_ESPECIALIDADE'],$dba[$tdb[ESPECIALIDADES]['dba']]))."</td>
            <td align=\"right\">".$obj_campo['QUANTIDADE']."</td>
            <td align=\"right\">".$obj_campo['TEMPO']."</td>
            </tr>\n";
            
            $obj->MoveNext();
            $imao++;
        }
        if ($imao == 0) $doc.="<tr><td colspan=\"3\" >".$ling['NADA_ALOCADO']."</td></tr>";
        $doc .="</table>
  </td>
  <td width=\"50%\" valign=top style=\"border:0px;\">
  <table style=\"font-size:7pt;font-family:arial\"   border=1 cellspacing=0 cellpadding=1 width=\"100%\">
   <tr style='mso-yfti-irow:0;'>
    <th width=\"71%\" valign=top>{$tdb[ORDEM_MAT_PREVISTO]['MID_MATERIAL']}</th>
    <th width=\"13%\" valign=top>{$ling['qtd_qtd']}</th>
   </tr>";
        $imat=0;
        $obj=$dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute("SELECT * FROM ".ORDEM_MAT_PREVISTO." WHERE MID_ORDEM = '".$os."'");
        while (!$obj->EOF) {
            $obj_campo = $obj -> fields;
            $doc.="<tr>
            <td>".strtoupper(VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0))."</td>
            <td>".$obj_campo['QUANTIDADE']."</td>
            </tr>\n";
            $obj->MoveNext();
            $imat++;
        }
        if ($imat == 0) $doc.="<tr><td colspan=\"3\">".$ling['NADA_ALOCADO']."</td></tr>";
        $doc.="</table>
  </td>
 </tr></table>
 </td>
 </tr>";
    
  if($speedcheck == false){
    if (($tipo == "") or ($tipo == 0) or ($tipo == 4)) {
        $doc.="<tr style='mso-yfti-irow:3'>
        <td colspan=4 valign=\"top\">
        <strong>{$tdb[ORDEM]['DEFEITO']}:</strong>".htmlentities(VoltaValor(DEFEITO,"DESCRICAO","MID",$ord['DEFEITO'],0));
        
        for ($ic=0; $ic < $manusis['os']['defeito']; $ic++) {
            $doc.= "<br>";
        }
        
        $doc.="</td></tr><tr style='mso-yfti-irow:3'>
        <td colspan=4 valign=\"top\">";
        
        $doc .= "<strong>{$tdb[ORDEM]['CAUSA']}:</strong> ".htmlentities(VoltaValor(CAUSA,"DESCRICAO","MID",$ord['CAUSA'],0));
        for ($ic=0; $ic < $manusis['os']['causa']; $ic++) {
            $doc.= "<br>";
        }
        
        $doc.="</td></tr><tr style='mso-yfti-irow:3'>
        <td colspan=4 valign=\"top\">";
        
        $doc .= "<strong>{$tdb[ORDEM]['SOLUCAO']}:</strong> ".htmlentities(strtoupper(VoltaValor(SOLUCAO,"DESCRICAO","MID",$ord['SOLUCAO'],0)));
        for ($ic=0; $ic < $manusis['os']['solucao']; $ic++) {
            $doc.= "<br>";
        }
        
        $doc .= "   </td></tr>";
    }
  }
    
    if ($tipo == 1) {
        $doc.="
        <tr style='mso-yfti-irow:3'>
        <td colspan=4 style=\"font-size:7.5pt;font-family:arial\">
        <strong>{$tdb[PLANO_PADRAO]['DESC']}: " . htmlentities(VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$campo['MID_PLANO'],$tdb[PLANO_PADRAO]['dba'])) . "</strong>
        <table class=MsoNormalTable style=\"font-size:7pt;font-family:arial\" border=1 cellspacing=0 cellpadding=1 width=\"100%\">
        <thead>
        <tr style=\"font-family:arial\">
        <th>{$tdb[ORDEM_PREV]['NUMERO']}</th>
        <th>{$tdb[ORDEM_PREV]['PARTE']}</th>
        <th>{$tdb[ORDEM_PREV]['TAREFA']}</th>
        <th>{$tdb[ORDEM_PREV]['INSTRUCAO_DE_TRABALHO']}</th>
        <th>{$tdb[ORDEM_PREV]['ESPECIALIDADE']}</th>
        <th>{$tdb[ORDEM_PREV]['TEMPO_PREVISTO']}</th>
        <th>&nbsp;</th>
        </tr>
        </thead>";
        
        $obj=$dba[$tdb[ORDEM_PLANEJADO_PREV]['dba']] -> Execute("SELECT a.* FROM ".ORDEM_PLANEJADO_PREV." as a WHERE a.MID_ORDEM = '$os' ORDER BY a.MID_CONJUNTO,a.NUMERO ASC");
        
        $tmp_conj = 0;
        
        while (!$obj->EOF) {
            $obj_campo = $obj -> fields;
            
            if($tmp_conj != $obj_campo['MID_CONJUNTO']) {
                $conj=strtoupper(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0));
                $doc .= "<tr><td colspan=6><strong>$conj</strong></td></tr>";
            }

            $atvarr[$obj_campo['MID_ATV']] = $obj_campo['MID_ATV']; // armazena atividades para fazer album depois
            
            if ($obj_campo['MID_MATERIAL'] != 0) {
                $rowspan = "rowspan=\"2\"";
            }
            else {
                $rowspan = "";
            }
            
            $doc .= "<tr>";
            $doc .= "<td $rowspan>&nbsp;" . $obj_campo['NUMERO'] . "</td>";
            $doc .= "<td>&nbsp;".htmlentities($obj_campo['PARTE'])."</td>";
            $doc .= "<td>&nbsp;".htmlentities($obj_campo['TAREFA'])."</td>";
            $doc .= "<td>&nbsp;".htmlentities($obj_campo['INSTRUCAO_DE_TRABALHO'])."</td>";
            $doc .= "<td>&nbsp;" . htmlentities(VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$obj_campo['ESPECIALIDADE'],0)) . "</td>";
            $doc .= "<td>&nbsp;" . $obj_campo['TEMPO_PREVISTO'] . "</td>";
            $doc .= "<td $rowspan>(&nbsp;&nbsp;)&nbsp;{$ling['ok']}&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;) {$ling['nao_ok']}</td></tr>";
            
            if ($obj_campo['MID_MATERIAL'] != 0) {
                $doc .= "<tr>\n";
                $doc .= "<td colspan=2>{$tdb[ORDEM_PREV]['MID_MATERIAL']}: ".strtoupper(VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0))."</td>";
                $doc .= "<td colspan=2>{$tdb[ORDEM_PREV]['QUANTIDADE']}: ".$obj_campo['QTD']."</td>";
                $doc .= "</tr>\n";
            }
            

            $tmp_conj=$obj_campo['MID_CONJUNTO'];
            $obj->MoveNext();
        }
        
        $doc.= "</table>
      </td>
     </tr>";
    }
    if ($tipo == 2) {
        $doc.="
 <tr style='mso-yfti-irow:3'>
  <td colspan=\"4\" style='font-size:7.5pt;font-family:arial'>
  <p class=MsoNormal>
    <strong>{$tdb[PLANO_ROTAS]['DESC']}: ".htmlentities(VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$campo['MID_PLANO'],$tdb[PLANO_ROTAS]['dba']))."</strong>
    <table class=MsoNormalTable border=\"1\" style=\"font-size:7pt;font-family:arial\" cellspacing=0 cellpadding=1 width=\"100%\">";
        
        //echo "SELECT a.*, c.POSICAO  FROM ".ORDEM_PLANEJADO_LUB." as a, ".ROTEIRO_ROTAS." as c  WHERE a.MID_ORDEM = '$os' AND a.MID_MAQUINA = c.MID_MAQUINA AND a.MID_PLANO = c.MID_PLANO ORDER BY c.POSICAO,a.NUMERO ASC";
        $obj=$dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']] -> Execute("SELECT a.*, c.POSICAO  FROM ".ORDEM_PLANEJADO_LUB." as a, ".ROTEIRO_ROTAS." as c  WHERE a.MID_ORDEM = '$os' AND a.MID_MAQUINA = c.MID_MAQUINA AND a.MID_PLANO = c.MID_PLANO ORDER BY c.POSICAO,a.NUMERO ASC");
        
        while (!$obj->EOF) {
            $obj_campo = $obj -> fields;
            if ($obj_campo['MID_MAQUINA'] != 0) {
                if ($maqre != $obj_campo['MID_MAQUINA']) {
                    $doc .= "<tr>
                    <td colspan=\"8\"><span style='font-size:7pt;font-family:Arial'><strong> ".strtoupper(VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']))."</strong><br>".$tdb[MAQUINAS]['LOCALIZACAO_FISICA']." : ".strtoupper(VoltaValor(MAQUINAS,"LOCALIZACAO_FISICA","MID",$obj_campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba']))."</td>
                    </tr>
                    <tr>
                    <th>{$ling['Nordem']}</th>
                    <th>".$tdb[LINK_ROTAS]['MID_PONTO']."</th>
                    <th>{$ling['Nordem']} {$ling['pontos']}</th>
                    <th>".$tdb[LINK_ROTAS]['MID_CONJUNTO']."</th>
                    <th>".$tdb[LINK_ROTAS]['MID_MATERIAL']."</th>
                    <th>{$ling['qtd_prev_por_ponto']}</th>
                    <th>".$tdb[LINK_ROTAS]['TAREFA']."</th>
                    <th width=\"120\">{$ling['diagnostico_m']}</th>
                    </tr>";
                }
                $doc .= "<tr>";
                $doc .= "<td>".$obj_campo['NUMERO']."</td>";
                $doc .= "<td>&nbsp; ".htmlentities(VoltaValor(PONTOS_LUBRIFICACAO,"PONTO","MID",$obj_campo['MID_PONTO'],0))."</td>";
                $doc .= "<td>&nbsp; ".VoltaValor(PONTOS_LUBRIFICACAO,"NPONTO","MID",$obj_campo['MID_PONTO'],0)."</td>";
                $doc .= "<td>&nbsp; ".htmlentities(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],0))."</td>";
                $doc .= "<td>&nbsp; ".htmlentities(VoltaValor(MATERIAIS,"COD","MID",$obj_campo['MID_MATERIAL'],0))."-".htmlentities(VoltaValor(MATERIAIS,"DESCRICAO","MID",$obj_campo['MID_MATERIAL'],0))."</td>";
                $doc .= "<td>&nbsp; ".$obj_campo['QUANTIDADE']."</td>";
                $doc .= "<td>&nbsp; ".htmlentities($obj_campo['TAREFA'])."</td>";
                $doc .= "<td>&nbsp;( &nbsp; ) {$ling['ok']} &nbsp;&nbsp;&nbsp; ( &nbsp; ) {$ling['nao_ok']}</td>";
                $doc .= "</tr>\n";
            }
            $obj->MoveNext();
            $maqre=$obj_campo['MID_MAQUINA'];
        }
        $doc.= "</table>
  </td>
 </tr>";
    }
   if($speedcheck == true){
        	
    		$doc .= "<tr style='mso-yfti-irow:3'>\n";
    		$doc .= "<td colspan=4>\n<span style='font-size:7.5pt;font-family:Arial'><strong>".(html_entity_decode($tdb[SPEED_CHECK]['DESC'])).": ".htmlentities(VoltaValor(SPEED_CHECK,"DESCRICAO","MID",$ord['MID_SPEED_CHECK'],0))."</strong></span>";
    		$doc .= "<table class=MsoNormalTable style=\"font-size:7pt;font-family:arial\" border=1 cellspacing=0 cellpadding=1 width=\"100%\">\n";
    	
    		// PRIMEIRAMENTE LISTANDO AS ATIVIDAES COM ETAPA E EST�GIO DEFINIDOS
    		$sqlSpeed = "SELECT * FROM ".ORDEM_SPEED_CHECK." WHERE MID_ORDEM = {$ord['MID']} ORDER BY ETAPA, NUMERO ASC";
    		
    		if(!$rsSpeed = $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sqlSpeed)){
    			erromsg("Erro ao localizar Atividades programadas em:<br />
    					Arquivo: ".__FILE__."<br />
    					Linha: ".__LINE__."<br />
    					Erro: ".$dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg()."<br />
    					SQL: $sqlSpeed
    					");
    		}
    		elseif(!$rsSpeed->EOF){
    	
    		// Listando as atividades agrupadas pela etapa e est�gio, ordenando pelo n�mero da atividade
    		$etapa = '';
    		while(!$rsSpeed->EOF){
    		$rowSpeed = $rsSpeed->fields;
    	
    		$atvListada[] = $rowSpeed['MID'];
    	
    		if($etapa != $rowSpeed['ETAPA']){
    		$doc .= "<tr>\n";
    			$doc .= "<th colspan='6' style='text-align:left'>".VoltaValor(ETAPAS_PLANO, 'DESCRICAO', 'MID', $rowSpeed['ETAPA'], 0)."</th>";
    			$doc .= "</tr>\n";
    			$doc .= "<tr>\n";
    			$doc .= "<th style='text-align:center' width='5%'>".(html_entity_decode($ling['num_pos']))."</th>";
    			$doc .= "<th style='text-align:center' width='50%'>".(html_entity_decode($ling['descricao']))."</th>";
    			$doc .= "<th style='text-align:center' width='5%'>OK</th>";
    			$doc .= "<th style='text-align:center' width='5%'>NOK</th>";
    			$doc .= "<th style='text-align:center' width='5%'>NA</th>";
    			$doc .= "<th style='text-align:center' width='30%'>".(html_entity_decode($tdb[ORDEM_SPEED_CHECK]['COMENTARIO']))."</th>";
    			$doc .= "</tr>\n";
    		}
    	
    		$doc .= "<tr>\n";
    			$doc .= "<td style='text-align:center'>{$rowSpeed['NUMERO']}</td>\n";
    			$doc .= "<td style='text-align:left'>{$rowSpeed['DESCRICAO']}</td>\n";
    			$doc .= "<td style='text-align:center'>&nbsp;</td>\n";
    			$doc .= "<td style='text-align:left'>&nbsp;</td>\n";
    			$doc .= "<td style='text-align:left'>&nbsp;</td>\n";
    			$doc .= "<td style='text-align:left'>\n";
    			$doc .= "&nbsp;";
    			$doc .= "</td>";
    			$doc .= "</tr>\n";
    	
    			$etapa = $rowSpeed['ETAPA'];
    			$rsSpeed->MoveNext();
    		}
    		}
    		else{
	    		// Nenhuma atividade encontrada
	    		$doc .= "<tr>\n";
	    		$doc .= "<th colspan='6' style='text-align:left'>{$ling['atividades_nao_encontradas']}</th>";
	    		$doc .= "</tr>\n";
    		}
    	
    		$doc .= "</table>\n";
    		$doc .= "</td>\n";
    		$doc .= "</tr>\n";
    	
    }
    	
    
    if($speedcheck == false){
		    $obj=$dba[$tdb[PENDENCIAS]['dba']] -> Execute("SELECT MID FROM ".PENDENCIAS." WHERE MID_ORDEM_EXC = '$os'");
		    $objl=$i=count($obj -> getrows());
		    if ($objl != "") {
		        $doc.="
		         <tr style='mso-yfti-irow:3' style=\"font-size:7.5pt;font-family:arial\">
		          <td colspan=\"4\"><strong>{$ling['pendencias_anexas']}:</strong><o:p></o:p></p>
		           <table class=MsoNormalTable style=\"font-size:7pt;font-family:arial\" border=1 cellspacing=0 cellpadding=1 width=\"100%\">
		            <tr>
		            <td><strong>{$ling['Nordem']}</strong></td>
		            <td><strong>{$ling['DESCRICAO_M']}</strong></td>
		            <td><strong>{$ling['rel_desc_obj']}</strong></td>
		            <td><strong>{$ling['rel_desc_pos']}</strong></td>
		            <td width=\"120\">&nbsp;</td>
		            </tr>";
		
		        $obj=$dba[$tdb[PENDENCIAS]['dba']] -> Execute("SELECT * FROM ".PENDENCIAS." WHERE MID_ORDEM_EXC = '$os'");
		        while (!$obj->EOF) {
		            $obj_campo = $obj -> fields;
		            $doc .= "<tr>";
		            $doc .= "<td><span style='font-size:7.5pt;font-family:Arial'>".$obj_campo['NUMERO']."</td>";
		            $doc .= "<td><span style='font-size:7.5pt;font-family:Arial'>&nbsp; ".htmlentities(strtoupper($obj_campo['DESCRICAO']))."</td>";
		            $doc .= "<td><span style='font-size:7.5pt;font-family:Arial'>&nbsp; ".htmlentities(strtoupper(VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj_campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])))."</td>";
		            $doc .= "<td><span style='font-size:7.5pt;font-family:Arial'>&nbsp; ".htmlentities(strtoupper(VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$obj_campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])))."</td>";
		            $doc.="<td>(&nbsp;&nbsp;)&nbsp;{$ling['ok']}&nbsp;&nbsp;&nbsp;(&nbsp;&nbsp;) {$ling['nao_ok']}</td> ";
		            $doc .= "</tr>\n";
		            $obj->MoveNext();
		        }
		        $doc.= "</table>
		 <p class=MsoNormal><o:p></o:p></p>
		  </td>
		 </tr>";
    	}
    }
    $doc.="
 <tr style='mso-yfti-irow:4'>
  <td colspan=4 valign=top style=\"font-size:7.5pt;font-family:arial\">
  <strong>{$tdb[ORDEM_MAQ_PARADA]['DESC']}:</strong>
   <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=\"100%\" style=\"font-size:7pt;font-family:arial\">
     <tr style='mso-yfti-irow:0;'>
      <th valign=top>{$tdb[ORDEM_MAQ_PARADA]['DATA_INICIO']}</td>
      <th>{$tdb[ORDEM_MAQ_PARADA]['HORA_INICIO']}</td>
      <th>{$tdb[ORDEM_MAQ_PARADA]['DATA_FINAL']}</td>
      <th>{$tdb[ORDEM_MAQ_PARADA]['HORA_FINAL']}</td>
     </tr>";
          
     for ($ic=0; $ic < $manusis['os']['parada']; $ic++) {
         $doc .= "<tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes' class=\"linha_grande\">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
         </tr>";
    }
    
    $doc .= "</table>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5'>
  <td colspan=4 valign=top style='font-size:7.5pt;font-family:Arial'>
  
  <strong>{$tdb[ORDEM_MAODEOBRA]['DESC']}:</strong>
  
  <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=\"100%\" style='font-size:7pt;font-family:Arial'>
   <tr style='mso-yfti-irow:0;'>
    <th width=\"10%\">{$tdb[FUNCIONARIOS]['MATRICULA']}</th>
    <th width=\"50%\">{$tdb[ORDEM_MAODEOBRA]['MID_FUNCIONARIO']}</th>
    <th width=\"10%\">{$tdb[ORDEM_MAODEOBRA]['DATA_INICIO']}</th>
    <th width=\"10%\">{$tdb[ORDEM_MAODEOBRA]['HORA_INICIO']}</th>
    <th width=\"10%\">{$tdb[ORDEM_MAODEOBRA]['DATA_FINAL']}</th>
    <th width=\"10%\">{$tdb[ORDEM_MAODEOBRA]['HORA_FINAL']}</th>
   </tr>";
   
   // Buscando funcionarios alocados nessa OS
   $sql = "SELECT F.MATRICULA, F.NOME FROM " . FUNCIONARIOS . " F, " . ORDEM_MO_ALOC . " A WHERE A.MID_ORDEM = $os AND A.MID_FUNCIONARIO = F.MID ORDER BY F.NOME";
   if(! $rs_tmp = $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> Execute($sql)) {
        erromsg("{$ling['arquivo']}: " . __FILE__ . "<br />{$ling['linha']}: " . __LINE__ . "<br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']] -> ErrorMsg() . "<br />" . $sql);
        return false;
    }
    $ifunc = $manusis['os']['maodeobra'];
    while (! $rs_tmp->EOF) {
        $doc.= "
       <tr style='mso-yfti-irow:1' class=\"linha_grande\">
        <td align=\"center\">&nbsp;" . htmlentities($rs_tmp->fields['MATRICULA']) . "</td>
        <td>&nbsp;" . htmlentities($rs_tmp->fields['NOME']) . "</td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
       </tr>";
       $manusis['os']['maodeobra']--;
        $rs_tmp -> MoveNext();
    }
   
    for ($ic=0; $ic < $manusis['os']['maodeobra']; $ic++) {
        $doc.= "
       <tr style='mso-yfti-irow:1' class=\"linha_grande\">
        <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
       </tr>";
    }
    
    $doc.="</table>
    
        </td>
    </tr>

  <tr style='mso-yfti-irow:5'>
  <td colspan=4 valign=top style='font-size:7.5pt;font-family:Arial'>
  <strong>{$tdb[ORDEM_MATERIAL]['DESC']}:</strong>
    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=\"100%\" style='font-size:7pt;font-family:Arial'>
   <tr style='mso-yfti-irow:0;'>
    <th width=\"13%\">{$tdb[MATERIAIS]['COD']}</th>
    <th>{$tdb[ORDEM_MATERIAL]['MID_MATERIAL']}</th>
    <th width=\"13%\">{$tdb[ORDEM_MATERIAL]['QUANTIDADE']}</th>
    </tr>";
    
    for ($ic=0; $ic < $manusis['os']['materiais']; $ic++) {
        $doc.= "
       <tr style='mso-yfti-irow:1' class=\"linha_grande\">
       <td>&nbsp; </td>
        <td>&nbsp; </td>
        <td>&nbsp; </td>
       </tr>";
    }
    
    $doc.= "
  </table>
  </td></tr>";
    if($speedcheck == false){ 
		  $doc .= "<tr><td colspan=4 style='font-size:7.5pt;font-family:Arial'>
		  <strong>{$tdb[PENDENCIAS]['DESC']}:</strong>
		 <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=\"100%\" style='font-size:7pt;font-family:Arial'>
		   <tr style='mso-yfti-irow:0;'>
		    <th>{$tdb[PENDENCIAS]['NUMERO']}</th>
		    <th>{$tdb[PENDENCIAS]['DESCRICAO']}</th>
		    </tr>";
		    
		    for ($ic=0; $ic < $manusis['os']['pendencia']; $ic++) {
		        $doc.= "   <tr style='mso-yfti-irow:1' class=\"linha_grande\">
		        <td>&nbsp; </td>
		        <td>&nbsp; </td>
		       </tr>";
		    }
    
	}
    
    $doc.= "</table>";
    
        
    #############
    if (($usa_imagem) AND (!$_REQUEST['word'])) {
        // anexos �s atividades desta ordem
        $nums = array(); // $nums[numero da atividade] = $i da instru��o;
        if (count($atvarr) > 0) { // tem atividade
            //die('Esta p�gina se encontra em desenvolvimento, por favor aguarde<br><br>');
             $album .= "<tr style=\"page-break-inside:avoid\"><td>";
            foreach ($atvarr as $eatv) {
                $nums[$eatv] = 0; // $i da atividade = 0;
                //echo "[$eatv]";
                // anexos a esta atividade
                $arquivos = ListaImagensAtividade($eatv); // lista na pasta planopadrao pelo mid da atividade
                //print_r($arquivos);
                $dir_i=$manusis['dir']['planopadrao']."/$eatv";
                $j=0;
                if ($arquivos) foreach ($arquivos as $earq) {
                   
                  
            
                    // aumenta para contar foto atual
                    $album_cnt++;
                    
                    // coloca esta imagem:
                    $numero_atv = VoltaValor(ATIVIDADES,'NUMERO','MID',$eatv,0);
                    $nums[$eatv]++; // $i da atividade ++;
                    $album .= "<div style=\"float:left\">
                    {$ling['atividade_n']} $numero_atv, {$ling['atividade']} {$nums[$eatv]}:
                    <br>
                    <img src=\"$dir_i/$earq\" border=1 hspace=5 align=\"middle\"  style=\"margin:5px; display:block; float:left;\"\>
                    </div>";
                }
            }
            
            $album .= "</td></tr>";
            
        }   
        
        $arquivos = ListaImagensOrdem($os);
                   
           if ($arquivos) 
           {
                $dir_i=$manusis['dir']['ordens']."/$os";
                $album .= "<tr style=\"page-break-inside:avoid\"><td style='font-size:7pt;font-family:Arial'>";
                foreach ($arquivos as $earq) {
                    $album .= "<img src=\"$dir_i/$earq\" border=1 hspace=5 align=\"middle\" style=\"margin:5px; display:block; float:left;\" \>";
                }
                $album .= "</td></tr>";
            } 
            
        $midPlano = VoltaValor(ORDEM_LUB, 'MID_PLANO', 'MID_ORDEM', $os, $tdb[ORDEM_LUB]['dba'] );  
        
        $arquivos = ListaImagensPlanoLub($midPlano);
                   
           if ($arquivos) 
           {
                $dir_i=$manusis['dir']['lubrificacao']."/$midPlano";
                $album .= "<tr style=\"page-break-inside:avoid\"><td style='font-size:7pt;font-family:Arial'>";
                foreach ($arquivos as $earq) {
                    $album .= "<img src=\"$dir_i/$earq\" border=1 hspace=5 align=\"middle\" style=\"margin:5px; display:block; float:left;\" \>";
                }
                $album .= "</td></tr>";
            }   
         
        if ($album) {
            $doc .= "<br>
            
            <table  style='font-size:7pt;font-family:Arial' width=100%>
               <thead>
               <tr><th><strong>{$ling['anexos_m']}:</strong></th></tr>
               </thead><tbody>
                       $album
                       </tbody>
            </table>";
        }
    }


    #############
    
    $doc.= "</td>
    </tr></table>
    <br><br><br>
    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=\"100%\" style='font-size:7pt;font-family:Arial'>
    <tr><td align=center style=\"padding:3;border:0px;border-top:1px solid black\"><b>{$ling['supervisor']}</b></td>
    <td style=\"border:0px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td align=center style=\"padding:3;border:0px;border-top:1px solid black\"><b>{$ling['solicitante']}</b></td>
    </tr>
    </table>
    ";
    return $doc;
}

function ListaImagensAtividade($mid_atv) {
    global $manusis;
    
    $dir=$manusis['dir']['planopadrao']."/$mid_atv";

    $arquivos = array();
    $tem_arquivos = false;
    
    if (file_exists($dir)) {
        $od = opendir($dir);
        $od2 = opendir($dir);
        $cont_verarq = 0;
        while (false !== ($arq_v = readdir($od2))) {
            $cont_verarq++;
        }
        if ($cont_verarq > 2){
            $i=0;
            while (false !== ($arq = readdir($od))) { //mostra
                $dd=explode(".",$arq);
                if (($dd[1] == "jpg") or ($dd[1] == "png")) {
                    $arquivos[] = $arq;
                    if (!$tem_arquivos) $tem_arquivos = true;
                }
            }
        }
    }
    if (!$tem_arquivos) return false;
    else return $arquivos;
}

function ListaImagensOrdem($midOrdem) {
    global $manusis;
    
    $dir=$manusis['dir']['ordens']."/$midOrdem";
    $arquivos = array();
    $tem_arquivos = false;
    
    if (file_exists($dir)) {
        $od = opendir($dir);
        $od2 = opendir($dir);
        $cont_verarq = 0;
        while (false !== ($arq_v = readdir($od2))) {
            $cont_verarq++;
        }
        if ($cont_verarq > 2){
            $i=0;
            while (false !== ($arq = readdir($od))) { //mostra
                  
                 $dd=explode(".",$arq);
                   if (($dd[1] == "jpg") or ($dd[1] == "png")) {
                    
                    $arquivos[] = $arq;                 
                    if (!$tem_arquivos) $tem_arquivos = true;
                }
   
            }
        }
    }
    if (!$tem_arquivos) return false;
    else return $arquivos;
}

function ListaImagensPlanoLub($midPlano) {
    global $manusis;
    
    $dir=$manusis['dir']['lubrificacao']."/$midPlano";
    $arquivos = array();
    $tem_arquivos = false;
    
    if (file_exists($dir)) {
        $od = opendir($dir);
        $od2 = opendir($dir);
        $cont_verarq = 0;
        while (false !== ($arq_v = readdir($od2))) {
            $cont_verarq++;
        }
        if ($cont_verarq > 2){
            $i=0;
            while (false !== ($arq = readdir($od))) { //mostra
                  
                 $dd=explode(".",$arq);
                   if (($dd[1] == "jpg") or ($dd[1] == "png")) {
                    
                    $arquivos[] = $arq;                 
                    if (!$tem_arquivos) $tem_arquivos = true;
                }
   
            }
        }
    }
    if (!$tem_arquivos) return false;
    else return $arquivos;
}

?>
