<?
/**
 * Parametros para tratametos com AJAX
 *
 * @author  Mauricio Barbosa <mauricio@manusis.com.br>
 * @author  Fernando Cosentino
 * @version  3.0
 * @package manusis
 * @subpackage  egine
 */

// Fun��es do Sistema
if (! require ("lib/mfuncoes.php")) die($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (! require ("conf/manusis.conf.php")) die($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (! require ("lib/idiomas/" . $manusis['idioma'][0] . ".php")) die($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (! require ("lib/adodb/adodb.inc.php")) die($ling['bd01']);
// Informa��es do banco de dados
elseif (! require ("lib/bd.php")) die($ling['bd01']);
// Formul�rios
elseif (! require ("lib/forms.php")) die($ling['bd01']);
// Autentifica��o
elseif (! require ("lib/autent.php")) die($ling['autent01']);

if ($_GET['id'] == 1) {
    if ($_GET['buscar_qto'] != "") $_SESSION[ManuSess]['lt']['progra']['buscar_qto'] = (int) $_GET['buscar_qto'];
    $buscar_qto = (int) $_SESSION[ManuSess]['lt']['progra']['buscar_qto'];
    if ($buscar_qto == 0) $buscar_qto = 50;
    if ($_GET['fIano'] != "") {
        $fano = (int) $_GET['fIano'];
        $ano = $fano;
        $_SESSION[ManuSess]['lt']['progra']['ano'] = $ano;
    }
    elseif ($_SESSION[ManuSess]['lt']['progra']['ano'] == "") {
        $ano = date("Y");
        $fano = date("Y");
        $_SESSION[ManuSess]['lt']['progra']['ano'] = $ano;
    }
    else {
        $ano = $_SESSION[ManuSess]['lt']['progra']['ano'];
        $fano = $_SESSION[ManuSess]['lt']['progra']['ano'];
    }
    if ($_GET['buscar_texto'] != "") $_SESSION[ManuSess]['lt']['progra']['buscar_texto'] = LimpaTexto($_GET['buscar_texto']);
    if (($_GET['localizar'] != "") and ($_GET['buscar_texto'] == "")) $_SESSION[ManuSess]['lt']['progra']['buscar_texto'] = "";
    $buscar_texto = $_SESSION[ManuSess]['lt']['progra']['buscar_texto'];
    if ($_GET[' buscar_ond'] != "") $_SESSION[ManuSess]['lt']['progra']['buscar_ond'] = (int) $_GET['buscar_ond'];
    $buscar_ond = $_SESSION[ManuSess]['lt']['progra']['buscar_ond'];
    $tipoprog = (int) $_GET['tipoprog'];
    if ($_GET[' bfamilia'] != "") $_SESSION[ManuSess]['lt']['progra']['bfamilia'] = (int) $_GET['bfamilia'];
    $bfamilia = $_SESSION[ManuSess]['lt']['progra']['bfamilia'];
    
    if ($_GET['bsetor'] != "") $_SESSION[ManuSess]['lt']['progra']['bsetor'] = (int) $_GET['bsetor'];
    $bsetor = $_SESSION[ManuSess]['lt']['progra']['bsetor'];
    echo "<fieldset>";
    
    echo "
<legend>" . $ling['localizar_obj'] . "</legend>
<input class=\"campo_text\" title=\"" . $ling['localizar_obj_alt'] . "\" type=\"text\" name=\"buscar_texto\" value=\"$buscar_texto\" size=\"13\" maxlength=\"75\" />";
    $primeirocampo = $tdb[MAQUINAS_FAMILIA]['DESC'];
    FormSelect("bfamilia", MAQUINAS_FAMILIA, $bfamilia, "DESCRICAO", "MID", $tdb[MAQUINAS_FAMILIA]['dba'], 0);
    $primeirocampo = $tdb[SETORES]['DESC'];
    FormSelect("bsetor", SETORES, $bsetor, "DESCRICAO", "MID", $tdb[SETORES]['dba'], 0);
    echo "

<input type=\"hidden\" name=\"id\" value=\"3\" />
<input type=\"hidden\" name=\"op\" value=\"3\" />
<select name=\"fIano\" class=\"campo_select\">";
    $ffano = $ano + 6;
    for($i = $ano - 4; $i < $ffano; $i ++) {
        if ($i == $fano) echo "<option value=\"$fano\" selected=\"selected\">$fano</option>";
        else echo "<option value=\"$i\" >$i</option>\n";
    }
    echo "</select>
    <input class=\"botao\" type=\"submit\" name=\"localizar\" value=\"{$ling['enviar']}\" />";
    
    echo "
</fieldset>";
}
if ($_GET['id'] == 9) {
    if ($_GET['buscar_qto'] != "") $_SESSION[ManuSess]['lt']['progra']['buscar_qto'] = (int) $_GET['buscar_qto'];
    $buscar_qto = (int) $_SESSION[ManuSess]['lt']['progra']['buscar_qto'];
    if ($buscar_qto == 0) $buscar_qto = 50;
    if ($_GET['fIano'] != "") {
        $fano = (int) $_GET['fIano'];
        $ano = $fano;
        $_SESSION[ManuSess]['lt']['progra']['ano'] = $ano;
    }
    elseif ($_SESSION[ManuSess]['lt']['progra']['ano'] == "") {
        $ano = date("Y");
        $fano = date("Y");
        $_SESSION[ManuSess]['lt']['progra']['ano'] = $ano;
    }
    else {
        $ano = $_SESSION[ManuSess]['lt']['progra']['ano'];
        $fano = $_SESSION[ManuSess]['lt']['progra']['ano'];
    }
    if ($_GET['buscar_texto'] != "") $_SESSION[ManuSess]['lt']['progra']['buscar_texto'] = LimpaTexto($_GET['buscar_texto']);
    if (($_GET['localizar'] != "") and ($_GET['buscar_texto'] == "")) $_SESSION[ManuSess]['lt']['progra']['buscar_texto'] = "";
    $buscar_texto = $_SESSION[ManuSess]['lt']['progra']['buscar_texto'];
    if ($_GET[' buscar_ond'] != "") $_SESSION[ManuSess]['lt']['progra']['buscar_ond'] = (int) $_GET['buscar_ond'];
    $buscar_ond = $_SESSION[ManuSess]['lt']['progra']['buscar_ond'];
    $tipoprog = (int) $_GET['tipoprog'];
    if ($_GET[' bfamilia'] != "") $_SESSION[ManuSess]['lt']['progra']['bfamilia'] = (int) $_GET['bfamilia'];
    $bfamilia = $_SESSION[ManuSess]['lt']['progra']['bfamilia'];
    
    if ($_GET['bsetor'] != "") $_SESSION[ManuSess]['lt']['progra']['bsetor'] = (int) $_GET['bsetor'];
    $bsetor = $_SESSION[ManuSess]['lt']['progra']['bsetor'];
    echo "<fieldset>";
    
    echo "
<legend>" . $ling['localizar_rota'] . "</legend>
<input type=\"text\" name=\"buscar_texto\" value=\"$buscar_texto\" size=\"35\" maxlength=\"75\" class=\"campo_text\" />";
    echo "
<input type=\"hidden\" name=\"id\" value=\"3\" />
<input type=\"hidden\" name=\"op\" value=\"3\" />
<select name=\"fIano\" class=\"campo_select\">";
    $ffano = $ano + 6;
    for($i = $ano - 4; $i < $ffano; $i ++) {
        if ($i == $fano) echo "<option value=\"$fano\" selected=\"selected\">$fano</option>";
        else echo "<option value=\"$i\" >$i</option>\n";
    }
    echo "</select>
    <input class=\"botao\" type=\"submit\" name=\"localizar\" value=\"{$ling['enviar']}\" />";
}
if ($_GET['id'] == 2) {
    $area = (int) $_GET['area'];
    $dir = ($_GET['dir']) ? $_GET['dir'] : '..';
    echo "<label class=\"campo_label \" for=\"filtro_setor\">" . $tdb[SETORES]['DESC'] . ":</label>";
    echo " <select name=\"filtro_setor\" id=\"filtro_setor\" class=\"campo_select\" onchange=\"atualiza_area2('maq','$dir/parametros.php?id=3&dir=$dir&setor=' + this.options[this.selectedIndex].value)\">";
    $tmp = $dba[$tdb[SETORES]['dba']]->Execute("SELECT COD,DESCRICAO,MID FROM " . SETORES . " WHERE MID_AREA = '$area' ORDER BY COD ASC");
    echo "<option value=\"\">" . strtoupper($ling['todos']) . "</option>";
    while ( ! $tmp->EOF ) {
        $campo = $tmp->fields;
        if ($_GET['filtro_setor'] == $campo['MID']) echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . "</option>";
        else echo "<option value=\"" . $campo['MID'] . "\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . "</option>";
        $tmp->MoveNext();
    }
    echo "</select>";
}
if ($_GET['id'] == 3) {
    $setor = (int) $_GET['setor'];
    $dir = ($_GET['dir']) ? $_GET['dir'] : '..';
    echo "<label class=\"campo_label \" for=\"filtro_maq\">" . $tdb[MAQUINAS]['DESC'] . ":</label>";
    echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\" onchange=\"atualiza_area2('equip','$dir/parametros.php?id=31&maq=' + this.options[this.selectedIndex].value)\">";
    $tmp = $dba[$tdb[MAQUINAS]['dba']]->Execute("SELECT COD,DESCRICAO,MODELO,MID FROM " . MAQUINAS . " WHERE MID_SETOR = '$setor' ORDER BY COD ASC");
    echo "<option value=\"\">" . strtoupper($ling['todos']) . "</option>";
    while ( ! $tmp->EOF ) {
        $campo = $tmp->fields;
        if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . "</option>";
        else echo "<option value=\"" . $campo['MID'] . "\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . " </option>";
        $tmp->MoveNext();
    }
    echo "</select>";
}

if ($_GET['id'] == 31) {
    $maq = (int) $_GET['maq'];
    echo "<label class=\"campo_label \" for=\"filtro_equip\">" . $tdb[EQUIPAMENTOS]['DESC'] . ":</label>";
    echo " <select name=\"filtro_equip\" id=\"filtro_equip\" class=\"campo_select\">";
    $tmp = $dba[$tdb[EQUIPAMENTOS]['dba']]->Execute("SELECT COD,DESCRICAO,MODELO,MID FROM " . EQUIPAMENTOS . " WHERE MID_MAQUINA = '$maq' ORDER BY COD ASC");
    echo "<option value=\"\">" . strtoupper($ling['todos']) . "</option>";
    while ( ! $tmp->EOF ) {
        $campo = $tmp->fields;
        if ($_GET['filtro_equip'] == $campo['MID']) echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . "</option>";
        else echo "<option value=\"" . $campo['MID'] . "\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . " </option>";
        $tmp->MoveNext();
    }
    echo "</select>";
}

if ($_GET['id'] == 4) {
    $tipo = (int) $_GET['tipo'];
    if ($tipo == 1) {
        echo "<label class=\"campo_label \" for=\"filtro_servico\">{$ling['select_opcao']}:</label>";
        echo "<select  class=\"campo_select\" name=\"filtro_servico\" name=\"filtro_servico\">
    <option value=\"0\">{$ling['todos2']}</option>
    <option value=\"1\">{$ling['rel_desc_prev']}</option>
    <option value=\"2\">{$ling['rel_desc_rotas']}</option>
    </select>";
    }
    if ($tipo == 2) {
        echo "<label class=\"campo_label \" for=\"filtro_servico\">{$ling['select_opcao']}:</label>";
        $primeirocampo = strtoupper($ling['todos']);
        FormSelect("filtro_servico", TIPOS_SERVICOS, $_GET['filtro_servico'], "DESCRICAO", "MID", $tdb[TIPOS_SERVICOS]['dba'], 0);
    }
}
if ($_GET['id'] == 5) {
    echo "<label class=\"campo_label\" for=\"cont\">" . $tdb[MAQUINAS_CONTADOR]['DESC'] . "</label>";
    echo " <select name=\"cont\" id=\"cont\" class=\"campo_select\">";
    $obj = (int) $_GET['obj'];
    $tmp = $dba[$tdb[MAQUINAS_CONTADOR]['dba']]->Execute("SELECT * FROM " . MAQUINAS_CONTADOR . " WHERE MID_MAQUINA = '$obj' ORDER BY DESCRICAO ASC");
    echo "<option value=\"0\">" . $ling['select_opcao'] . "</option>";
    while ( ! $tmp->EOF ) {
        $campo = $tmp->fields;
        if ($_GET['cont'] == $campo['MID']) echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . strtoupper(htmlentities($campo['DESCRICAO'])) . "</option>";
        else echo "<option value=\"" . $campo['MID'] . "\">" . strtoupper(htmlentities($campo['DESCRICAO'])) . "</option>";
        $tmp->MoveNext();
    }
    echo "</select><input type=\"button\" class=\"botao\" name=\"botao\" value=\"{$ling['selecionar']}\" onclick=\"atualiza_area2('detalhes','parametros.php?id=6&cont=' + document.getElementById('cont').options[document.getElementById('cont').selectedIndex].value)\" />";
}

if ($_GET['id'] == 6) {
    $cont = (int) $_GET['cont'];
    if ($cont) {
        $contador = VoltaValor(MAQUINAS_CONTADOR, "DESCRICAO", "MID", $cont, 0);
        echo "<br clear=\"all\" /><strong>$contador</strong><br /><br />
        <table border=\"0\" cellpadding=\"0\" cellspacing=\"3\" width\"100%\">
        <tr>
        <td width=\"50%\">
        {$ling['data']}:";
        
        FormData('', 'data', date('d/m/Y'), '', '', 'campo_text_ob'); 
        
        echo "&nbsp;&nbsp;{$ling['valor']}: 
        <input type=\"text\" class=\"campo_text_ob\" id=\"valor\" name=\"valor\" size=\"9\" maxlenght=\"11\" onkeypress=\"if (is_enter(this, event)) atualiza_area2('detalhes','parametros.php?id=6&cont=$cont&valor=' + document.getElementById('valor').value + '&data=' + document.getElementById('data').value)\"/>
        
        <input type=\"button\" class=\"botao\" name=\"botao\" value=\"{$ling['lancar_valor']}\"  onclick=\"atualiza_area2('detalhes','parametros.php?id=6&cont=$cont&valor=' + document.getElementById('valor').value + '&data=' + document.getElementById('data').value)\" />&nbsp;&nbsp;
        <input type=\"button\" class=\"botao\" name=\"botao\" value=\"{$ling['setar_valor_inicial']}\"  onclick=\"atualiza_area2('detalhes','parametros.php?id=6&cont=$cont&ini=1&valor=' + document.getElementById('valor').value + '&data=' + document.getElementById('data').value)\" />";
        
        if (($_GET['valor'] !== "") and ($_GET['data'] != "")) {
            $valor = (float) str_replace(",", ".", $_GET['valor']);
            if ($cont) $msg = LancaContador($cont, $valor, $_GET['data'], (int) $_GET['ini']);
            if ($msg) echo "<p align=center><strong><font color=red>" . $msg . "</font></strong></p>";
        }
        elseif ($_GET['valor'] === "") {
            echo "<p align=center><strong><font color=red>".$ling['dados_invalidos'].".</font></strong></p>";
        }
        
        echo "<div id=\"lt_tabela\">
        <table width=\100%\" border=\"0\">
        <tr><th>".$ling['permissao3']."</th>
        <th>".$ling['data']."</th>
        <th>".$ling['valor']."</th>
        <th>".$ling['falta_para_zerar']."</th></tr>";
        
        $zera = VoltaValor(MAQUINAS_CONTADOR, "ZERA", "MID", $cont, 0);
        
        $tmp = $dba[$tdb[LANCA_CONTADOR]['dba']]->SelectLimit("SELECT * FROM " . LANCA_CONTADOR . " WHERE MID_CONTADOR = '$cont' ORDER BY MID DESC", 10, 0);
        while ( ! $tmp->EOF ) {
            $campo = $tmp->fields;
            if ($campo['MID_USUARIO']) $usuario = htmlentities(VoltaValor(USUARIOS, "USUARIO", "MID", $campo['MID_USUARIO'], 0));
            else $usuario = strtoupper($_SESSION[ManuSess]['user']['USUARIO']);
            $tmp_zera = $zera - $campo['VALOR'];
            echo "<tr class=\"cor1\">
        <td>$usuario</td>
        <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . "</td>
        <td>" . $campo['VALOR'] . "</td>
        <td>$tmp_zera</td>
        </tr>";
            $tmp->MoveNext();
        }
        echo "</table>
</td>
<td class=\"cor1\" width=\"1\"><img src=\"imagens/pi.png\" border=\"0\" alt=\"\" /></td>
<td width=\"50%\"></td>
<tr>
</table>



<div id=\"lt_tabela\">
<strong>".$ling['acomp_dispa']." </strong><br /><br />
    <table width=\100%\" border=\"0\">
    <tr><th>".$ling['data']."</th>
    <th>".$ling['atividade_a']."</th>
    <th>".$ling['disparo_em']."</th>
    <th>".$ling['Qtd_disparos']."</th>
    <th>".$ling['valor_atual']."</th>
    <th>".$ling['falta_para_disparo']."</th>
    <th>".$ling['ordem']."</th></tr>";
        $tmp = $dba[$tdb[CONTROLE_CONTADOR]['dba']]->SelectLimit("SELECT * FROM " . CONTROLE_CONTADOR . " WHERE MID_CONTADOR = '$cont' ORDER BY MID DESC", 50, 0);
        while ( ! $tmp->EOF ) {
            $campo = $tmp->fields;
            $tipo = $campo['TIPO'];
            if ($tipo == 1) {
                $prog_mid = $campo['MID_PROGRAMACAO'];
                $des_atv = htmlentities(VoltaValor(ATIVIDADES, "TAREFA", "MID", $campo['MID_ATV'], 0));
                $atv_mid = $campo['MID_ATV'];
                $plano = htmlentities(VoltaValor(PLANO_PADRAO, "DESCRICAO", "MID", VoltaValor(PROGRAMACAO, "MID_PLANO", "MID", $prog_mid, 0), 0));
                $disparo = VoltaValor(ATIVIDADES, "DISPARO", "MID", $atv_mid, $tdb[PLANO_PADRAO]['dba']);
            }
            if ($tipo == 2) {
                $prog_mid = $campo['MID_PROGRAMACAO'];
                $atv_mid = $campo['MID_ATV'];
                $des_atv = htmlentities(VoltaValor(LINK_ROTAS, "TAREFA", "MID", $campo['MID_ATV'], 0));
                $plano = htmlentities(VoltaValor(PLANO_ROTAS, "DESCRICAO", "MID", VoltaValor(PROGRAMACAO, "MID_PLANO", "MID", $prog_mid, 0), 0));
                $disparo = VoltaValor(LINK_ROTAS, "DISPARO", "MID", $atv_mid, $tdb[LINK_ROTAS]['dba']);
            }
            $osnum = VoltaValor(ORDEM_PLANEJADO, "NUMERO", "MID", $campo['MID_ORDEM'], $tdb[ORDEM_PLANEJADO]['dba']);
            $tmp_zera = $disparo - $campo['VALOR'];
            echo "<tr class=\"cor1\">
        <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . "</td>
        <td>$des_atv</td>
        <td>$disparo</td>
        <td>".$campo['DISPAROS']."</td>
        <td>".$campo['VALOR']."</td>
        <td>$tmp_zera</td>
        <td>$osnum</td>
        </tr>";
            $tmp->MoveNext();
        }
        echo "</table>";
    }
}
if ($_GET['id'] == 71) {
    $empresa = (int) $_GET['empresa'];
    if ($empresa) $filform = "WHERE MID_EMPRESA = '$empresa'";
    else $filform = '';
    FormSelectD('COD', 'DESCRICAO', AREAS, '', 'filtro_area', 'filtro_area', 'MID', '', '', '', $filform, 'A', 'COD', $tdb[AREAS]['DESC']);
    exit();
}

if ($_GET['id'] == 7) {
    $area = (int) $_GET['area'];
    if ($area) $filform = "WHERE MID_AREA = '$area'";
    else $filform = '';
    FormSelectD('COD', 'DESCRICAO', SETORES, $_REQUEST['setor'], 'filtro_setor', 'filtro_setor', 'MID', '', '', '', $filform, 'A', 'COD', $tdb[SETORES]['DESC']);
}
if ($_GET['id'] == 8) {
    $setor = (int) $_GET['setor'];
    echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\">";
    $tmp = $dba[$tdb[MAQUINAS]['dba']]->Execute("SELECT COD,DESCRICAO,MODELO,MID FROM " . MAQUINAS . " WHERE MID_SETOR = '$setor' ORDER BY COD ASC");
    echo "<option value=\"\">" . $tdb[MAQUINAS]['DESC'] . "</option>";
    while ( ! $tmp->EOF ) {
        $campo = $tmp->fields;
        if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . "</option>";
        else echo "<option value=\"" . $campo['MID'] . "\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . " </option>";
        $tmp->MoveNext();
    }
    echo "</select> <input type=\"submit\" value=\"{$ling['filtrar']}\" class=\"botao\" name=\"env\" />";
}
if ($_GET['id'] == 81) {
    $fmaq = (int) $_GET['fmaq'];
    echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\">";
    if ($fmaq != 0) $tmp = $dba[$tdb[MAQUINAS]['dba']]->Execute("SELECT COD,DESCRICAO,MODELO,MID FROM " . MAQUINAS . " WHERE FAMILIA = '$fmaq' ORDER BY COD ASC");
    else $tmp = $dba[$tdb[MAQUINAS]['dba']]->Execute("SELECT COD,DESCRICAO,MODELO,MID FROM " . MAQUINAS . " WHERE ORDER BY COD ASC");
    echo "<option value=\"\">" . $tdb[MAQUINAS]['DESC'] . "</option>";
    while ( ! $tmp->EOF ) {
        $campo = $tmp->fields;
        if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"" . $campo['MID'] . "\" selected=\"selected\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . "</option>";
        else echo "<option value=\"" . $campo['MID'] . "\">" . htmlentities($campo['COD'] . "-" . $campo['DESCRICAO']) . " </option>";
        $tmp->MoveNext();
    }
    echo "</select> <input type=\"submit\" value=\"{$ling['filtrar']}\" class=\"botao\" name=\"env\" />";
}
if ($_GET['id'] == "cron") {
    cronograma_os($_GET['mes'], $_GET['ano'], $_GET['filtro_tipo'], $_GET['valor']);
}
if ($_GET['id'] == "atv") {
    if ($_GET['filtro_tipo'] == 1) {
        $sql = "SELECT MID FROM " . SETORES . " WHERE MID_AREA = '" . $_GET['valor'] . "' ORDER BY COD";
        $tmp = $dba[0]->Execute($sql);
        if (! $tmp) erromsg($dba[0]->ErrorMsg() . "<br><br>$sql");
        $conta = 0;
        while ( ! $tmp->EOF ) {
            $ca = $tmp->fields;
            $filtro .= " A.MID_SETOR = '" . $ca['MID'] . "' AND";
            $filtro_rota .= " MID_SETOR = '" . $ca['MID'] . "' AND";
            $tmp->MoveNext();
        
        }
    }
    if ($_GET['filtro_tipo'] == 2) {
        $filtro = " A.MID_SETOR = '" . $_GET['valor'] . "' AND";
        $filtro_rota = " MID_SETOR = '" . $_GET['valor'] . "' AND";
    }
    if ($_GET['filtro_tipo'] == 3) {
        $filtro = " A.MID = '" . $_GET['valor'] . "' AND";
        $filtro_rota = " MID = '" . $_GET['valor'] . "' AND";
    }
    if ($_GET['filtro_tipo'] == 0) {
        $filtro = "";
        $filtro_rota = "";
    }
    echo "<p align=\"center\">{$ling['param_do_dia']} " . NossaData($_GET['dia_i']) . "   {$ling['param_ao']}  " . NossaData($_GET['dia_f']) . "</p>";
    $sql = "SELECT A.DESCRICAO,A.COD,A.MID,B.DATA,B.CHECKLIST,B.MID_PLANO,B.MID_PROGRAMACAO FROM maquinas AS A,  programacao_data AS B WHERE B.MID_MAQUINA = A.MID AND $filtro B.DATA >= '" . $_GET['dia_i'] . "' AND B.DATA <= '" . $_GET['dia_f'] . "' GROUP BY B.MID_PROGRAMACAO ORDER BY A.COD,B.DATA ASC ";
    $re = $dba[$tdb[MAQUINAS]['dba']]->Execute($sql);
    if (! $re) echo $dba[0]->ErrorMsg();
    echo "<form id=\"formulario\" action=\"parametros.php?id=geraos&filtro_tipo=" . $_GET['filtro_tipo'] . "&valor=" . $_GET['valor'] . "&dia_i=" . $_GET['dia_i'] . "&dia_f=" . $_GET['dia_f'] . "\" method=\"POST\" target=\"geraos\" />
    <div id=\"lt\"><div id=\"lt_tabela\">
    <a href=javascript:seleciona_tudo()><img src=\"imagens/icones/22x22/check.png\" border=\"0\" alt=\"{$ling['marcar_todos']}\" title=\"{$ling['marcar_todos']}\" /> {$ling['marcar_todos']}</a> | <a href=javascript:dseleciona_tudo()><img src=\"imagens/icones/22x22/decheck.png\" border=\"0\" alt=\"{$ling['desmarcar_todos']}\" title=\"{$ling['desmarcar_todos']}\" /> {$ling['desmarcar_todos']}</a>

    <table width=\"100%\" border=\"0\" cellspacing=\"1\ cellpadding=\"1\" align=\"center\">";
    $i = 0;
    while ( ! $re->EOF ) {
        $ca = $re->fields;
        $prog_mid = $ca['MID_PROGRAMACAO'];
        $tipo_prog = VoltaValor(PROGRAMACAO, "TIPO", "MID", $ca['MID_PROGRAMACAO'], 0);
        if ($tipo_prog == 1) {
            if ($tmp_cod != $ca['COD']) {
                unset($tmp_data);
                echo "<tr class=\"cor2\"><td colspan=\"3\"><strong>" . htmlentities($ca['COD'] . "-" . $ca['DESCRICAO']) . "</strong></td></tr>";
            }
            if ($ca['MID_PLANO'] == 0) {
                if ($tmp_data != $ca['DATA']) {
                    echo "<tr class=\"cor1\"><td width=\"10\">";
                    $tmp = $dba[$tdb[ORDEM_PLANEJADO]['dba']]->Execute("SELECT MID,NUMERO,STATUS,DATA_INICIO,DATA_FINAL FROM " . ORDEM_PLANEJADO . " WHERE MID_MAQUINA = '" . $ca['MID'] . "' AND MID_PROGRAMACAO = '$prog_mid' AND DATA_PROG = '" . $ca['DATA'] . "'");
                    $ctmp = $tmp->fields;
                    $itmp = count($tmp->getrows());
                    if ($itmp >= 1) {
                        if ($ctmp['STATUS'] == 1) echo "<img src=\"imagens/icones/16x16/verde.png\" alt=\" \"  border=\"0\" />\n";
                        elseif ($ctmp['STATUS'] == 2) echo "<img src=\"imagens/icones/16x16/ok.png\" alt=\" \" border=\"0\" />\n";
                        elseif ($ctmp['STATUS'] == 3) echo "<input type=\"checkbox\" name=\"cc[$prog_mid]\" value=\"" . $ca['DATA'] . "\" />";
                    
                    }
                    else
                        echo "<input type=\"checkbox\" name=\"cc[$prog_mid]\" value=\"" . $ca['DATA'] . "\" />";
                    echo "</td><td width=\"30\">" . NossaData($ca['DATA']) . "</td><td align=\"left\">CheckList</td></tr>";
                }
            }
            else {
                echo "<tr class=\"cor1\"><td width=\"10\">";
                $tmp = $dba[$tdb[ORDEM_PLANEJADO]['dba']]->Execute("SELECT MID,NUMERO,STATUS,DATA_INICIO,DATA_FINAL FROM " . ORDEM_PLANEJADO . " WHERE MID_MAQUINA = '" . $ca['MID'] . "' AND MID_PROGRAMACAO = '$prog_mid' AND DATA_PROG = '" . $ca['DATA'] . "' ORDER BY NUMERO DESC");
                $ctmp = $tmp->fields;
                $itmp = count($tmp->getrows());
                if ($itmp >= 1) {
                    if ($ctmp['STATUS'] == 1) echo "<img src=\"imagens/icones/16x16/verde.png\" alt=\" \"  border=\"0\" />\n";
                    elseif ($ctmp['STATUS'] == 2) echo "<img src=\"imagens/icones/16x16/ok.png\" alt=\" \" border=\"0\" />\n";
                    elseif ($ctmp['STATUS'] == 3) echo "<input type=\"checkbox\" name=\"cc[$prog_mid]\" value=\"" . $ca['DATA'] . "\" />";
                }
                else
                    echo "<input type=\"checkbox\" name=\"cc[$prog_mid]\" value=\"" . $ca['DATA'] . "\" />";
                echo "</td><td width=\"30\">" . NossaData($ca['DATA']) . "</td><td align=\"left\">" . htmlentities(VoltaValor(PLANO_PADRAO, "DESCRICAO", "MID", $ca['MID_PLANO'], 0)) . "</td></tr>";
            }
            $tmp_cod = $ca['COD'];
            $tmp_data = $ca['DATA'];
        }
        $i ++;
        $re->MoveNext();
    }
    $sql = "SELECT MID,COD FROM " . MAQUINAS . " WHERE $filtro_rota 1 = 1";
    $re = $dba[0]->Execute($sql);
    if (! $re) echo $dba[0]->ErrorMsg();
    while ( ! $re->EOF ) {
        $ca = $re->fields;
        $maq[$ca['MID']] = $ca['COD'];
        $re->MoveNext();
    }
    $sql = "SELECT * FROM programacao_data  WHERE DATA >= '" . $_GET['dia_i'] . "' AND DATA <= '" . $_GET['dia_f'] . "' ORDER BY DATA ASC ";
    $re = $dba[0]->Execute($sql);
    if (! $re) echo $dba[0]->ErrorMsg();
    echo "<tr class=\"cor2\"><td colspan=\"3\"><strong>{$ling['rel_desc_rotas']}</strong></td></tr>";
    while ( ! $re->EOF ) {
        $ca = $re->fields;
        $prog_mid = $ca['MID_PROGRAMACAO'];
        $tipo_prog = VoltaValor(PROGRAMACAO, "TIPO", "MID", $ca['MID_PROGRAMACAO'], 0);
        if ($tipo_prog != 1) {
            
            $tipop = "ROTA";
            $plano = htmlentities(VoltaValor(PLANO_ROTAS, "DESCRICAO", "MID", $ca['MID_PLANO'], 0));
            $sql_maq = "SELECT MID_MAQUINA FROM " . LINK_ROTAS . " WHERE MID_PLANO = " . $ca['MID_PLANO'] . "";
            
            if ($sql_maq != "") {
                $re2 = $dba[0]->Execute($sql_maq);
                if (! $re2) echo $dba[0]->ErrorMsg();
                while ( ! $re2->EOF ) {
                    $ca2 = $re2->fields;
                    if (@array_key_exists($ca2['MID_MAQUINA'], $maq)) {
                        $tmp_mostra = 1;
                        break;
                        echo $maq . "<br>";
                    }
                    $re2->MoveNext();
                }
                if ($tmp_mostra == 1) {
                    if ($tmp_data != $ca['DATA']) {
                        echo "<tr class=\"cor1\"><td width=\"10\">";
                        $tmp = $dba[$tdb[ORDEM_PLANEJADO]['dba']]->Execute("SELECT MID,NUMERO,STATUS,DATA_INICIO,DATA_FINAL FROM " . ORDEM_PLANEJADO . " WHERE MID_PROGRAMACAO = '$prog_mid' AND DATA_PROG = '" . $ca['DATA'] . "'");
                        $ctmp = $tmp->fields;
                        $itmp = count($tmp->getrows());
                        if ($itmp >= 1) {
                            if ($ctmp['STATUS'] == 1) echo "<img src=\"imagens/icones/16x16/verde.png\" alt=\" \"  border=\"0\" />\n";
                            elseif ($ctmp['STATUS'] == 2) echo "<img src=\"imagens/icones/16x16/ok.png\" alt=\" \" border=\"0\" />\n";
                            elseif ($ctmp['STATUS'] == 3) echo "<input type=\"checkbox\" name=\"cc[$prog_mid]\" value=\"" . $ca['DATA'] . "\" />";
                        
                        }
                        else
                            echo "<input type=\"checkbox\" name=\"cc[$prog_mid]\" value=\"" . $ca['DATA'] . "\" />";
                        echo "</td><td width=\"30\">" . NossaData($ca['DATA']) . "</td><td align=\"left\">$plano</td></tr>";
                    }
                }
            }
            unset($tmp_mostra);
        }
        $tmp_data = $ca['DATA'];
        $i ++;
        $re->MoveNext();
    }
    echo "</table><br /><input type=\"submit\" value=\"{$ling['gerar_os']}\" class=\"botao\" name=\"env\" /></form></div></div>";
}
if ($_GET['id'] == "geraos") {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"" . $ling['xml'] . "\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/" . $manusis['tema'] . "/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"" . $manusis['tema'] . "\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
    echo "</head>
<body class=\"body_form\">";
    $cc = $_POST['cc'];
    $sql = "SELECT MID FROM " . PROGRAMACAO . "";
    $tmp = $dba[0]->Execute($sql);
    if (! $tmp) erromsg($dba[0]->ErrorMsg() . "<br><br>$sql");
    $conta = 0;
    while ( ! $tmp->EOF ) {
        $ca = $tmp->fields;
        if ($cc[$ca['MID']] != "") gera_os($ca['MID'], $cc[$ca['MID']], 1);
        $tmp->MoveNext();
    }
    echo "<script>atualiza_area_frame('atv','parametros.php?id=atv&dia_i=" . $_GET['dia_i'] . "&dia_f=" . $_GET['dia_f'] . "&filtro_tipo=" . $_GET['filtro_tipo'] . "&valor=" . $_GET['valor'] . "')</script>";

}

elseif ($id == 10) {
    $di = explode("/", $_GET['di']);
    $df = explode("/", $_GET['df']);
    if (! checkdate((int) $di[1], (int) $di[0], (int) $di[2])) echo "<strong><font color=\"red\">{$ling['data_inicial_invalida']}</font></strong><br>";
    elseif ($df[0] != "") {
        if (! checkdate((int) $df[1], (int) $df[0], (int) $df[2])) echo "<strong><font color=\"red\">{$ling['data_final_invalida']}</font></strong><br>";
    }
    $di = mktime(0, 0, 0, $di[1], $di[0], $di[2]);
    $df = mktime(0, 0, 0, $df[1], $df[0], $df[2]);
    if ($di > $df) echo "<strong><font color=\"red\">".$ling['data_inicial_maior_data_final'].".</font></strong>";
}
elseif ($_GET['id'] == 11) {
    $par = $_GET['par'];
    $tb = $_GET['tb'];
    $oq = $_GET['oq'];
    $defca = $_SESSION[ManuSess]['lt'][$tb]['campos_total'][$par];
    if (strpos($defca, "DATA") !== false) {
        if ($buscar_texto != "") $datai = NossaData($buscar_texto);
        if ($buscar_texto2 != "") $dataf = NossaData($buscar_texto2);
        echo "{$ling['param_inicio']}: <input onkeypress=\"return ajustar_data(this, event)\" class=\"campo_text\" type=\"text\" name=\"buscar_texto\" value=\"$datai\" size=\"10\" maxlength=\"10\" />";
        echo "&nbsp;{$ling['param_fim']}: <input onkeypress=\"return ajustar_data(this, event)\" class=\"campo_text\" type=\"text\" name=\"buscar_texto2\" value=\"$dataf\" size=\"10\" maxlength=\"10\" />";
    } 
    else {
        echo "<input class=\"campo_text\" type=\"text\" name=\"buscar_texto\" value=\"$buscar_texto\" size=\"25\" maxlength=\"150\" />";
    }
}
elseif ($_GET['id'] == 12) {
    $equip = (int) $_GET['equip'];
    $esp = (int) $_GET['esp'];
    $where = "";
    
    // Filtros
    if (($esp == 0) and ($equip != 0)) $where = "WHERE EQUIPE=$equip";
    elseif (($esp != 0) and ($equip == 0)) $where = "WHERE ESPECIALIDADE=$esp";
    elseif (($esp != 0) and ($equip != 0)) $where = "WHERE ESPECIALIDADE=$esp AND EQUIPE=$equip";
    elseif (($esp == 0) and ($equip == 0)) $where = "";
    
    echo "<label class=\"campo_label \" for=\"filtro_func\">{$ling['funcionario']}</label>";
    FormSelectD("NOME", '', FUNCIONARIOS, "", "filtro_func", "filtro_func", "MID", 0, "", "", $where, "", "", 'TODOS');
    
    exit();
}
elseif ($_GET['id'] == 13) {
    $ponto = (int) $_GET['ponto'];
    $ponto = VoltaValor(PONTOS_LUBRIFICACAO, "NPONTO", "MID", $ponto, 0);
    $qto = (float) str_replace(",", ".", $_GET['qto']);
    $total = (float) $ponto * $qto;
    if ($ponto != 0) echo "&nbsp;&nbsp; {$ling['num_pontos']}: $ponto <strong>{$ling['qtde_total']}: $total </strong>";
    else echo "&nbsp;&nbsp; {$ling['sel_ponto']}";
}

elseif ($_GET['id'] == 'formselect') {
    $campo1 = $_GET['campo1'];
    $campo2 = $_GET['campo2'];
    $tabela = $_GET['tabela'];
    $sel_valor = $_GET['sel_valor'];
    $campo_nome = $_GET['campo_nome'];
    $campo_id = $_GET['campo_id'];
    $mid = $_GET['mid'];
    $formulario = $_GET['formulario'];
    $classe = $_GET['classe'];
    $acao = stripslashes($_GET['acao']);
    $sql_filtro = $_GET['sql_filtro'];
    $auto_filtro = $_GET['auto_filtro'];
    $ordem = $_GET['ordem'];
    $campo_grupo = $_GET['campo_grupo'];
    $classe = $_GET['classe'];
    $texto_ajax = (LimpaTexto($_GET['texto']));
    $atualiza_ajax = 1;
    FormSelectD($campo1, $campo2, $tabela, $sel_valor, $campo_nome, $campo_id, $mid, $formulario, $classe, $acao, $sql_filtro, $auto_filtro, $ordem, $primeirocampo, $campo_grupo);
    exit();
}
elseif ($_GET['id'] == 'roteiro') {
    $oq = (int) $_GET['oq'];
    $de = (int) $_GET['posde'];
    $para = (int) $_GET['pospara'];
    
    if ($de and $para) {
        $midroteirode = VoltaValor(ROTEIRO_ROTAS, 'MID', "MID_PLANO = '$oq' AND POSICAO", $de, 0);
        $midroteiropara = VoltaValor(ROTEIRO_ROTAS, 'MID', "MID_PLANO = '$oq' AND POSICAO", $para, 0);
        $dba[$tdb[ROTEIRO_ROTAS]['dba']]->Execute("UPDATE " . ROTEIRO_ROTAS . " SET POSICAO='$para' WHERE MID='$midroteirode' LIMIT 1");
        logar(4, "", ROTEIRO_ROTAS, "MID", $midroteirode);
        $dba[$tdb[ROTEIRO_ROTAS]['dba']]->Execute("UPDATE " . ROTEIRO_ROTAS . " SET POSICAO='$de' WHERE MID='$midroteiropara' LIMIT 1");
        logar(4, "", ROTEIRO_ROTAS, "MID", $midroteiropara);
    }
    include ("modulos/planejamento/mostra_roteiro.php");
}

elseif ($_GET['id'] == 'setor_autotag') {
    $valor = $_GET['valor'];
    if ($_autotag_setor['status'] == 1) {
        $cod = VoltaValor($_autotag_setor['tabela'], "COD", "MID", $valor, 0);
        $sql = "SELECT COD FROM " . SETORES . " WHERE COD LIKE '$cod%' ORDER BY COD DESC";
        $tmp = $dba[0]->SelectLimit($sql, 1, 0);
        if (!$tmp->EOF) {
            $campo = $tmp->fields;
            $num = substr($campo['COD'],strlen($cod));
            if ($num) while (!is_numeric($num[0])) {
                $num = substr($num,1);
                if (!$num) break;
            }
        }
        else $num = 0;
        $num = (int)$num + $_autotag_setor['incrementa'];
        $zerofill = $_autotag_setor['casasdecimais'] - strlen($num);
        $num = str_repeat("0", $zerofill) . "$num";
        $cod = $cod . "{$_autotag_setor['caracter']}" . $num;
        echo "<input class=\"campo_text_ob\" type=\"text\" id=\"cc[setores][2]\" name=\"cc[setores][2]\" size=\"15\" maxlength=\"15\" value=\"$cod\" />";
    }
}
elseif ($_GET['id'] == 'maq_autotag') {
    $valor = $_GET['valor'];
    if ($_autotag_maq['status'] == 1) {
        $cod = VoltaValor($_autotag_maq['tabela'], "COD", "MID", $valor, 0);
        $sql = "SELECT COD FROM " . MAQUINAS . " WHERE COD LIKE '$cod%' ORDER BY COD DESC";
        $tmp = $dba[0]->SelectLimit($sql, 1, 0);
        $campo = $tmp->fields;
        if ($_autotag_maq['caracter'] == "") {
            $c = explode($cod, $campo['COD']);
        
        }
        else {
            $c = explode($_autotag_maq['caracter'], $campo['COD']);
        }
        $num = (int) $c[1];
        $num = $num + $_autotag_maq['incrementa'];
        $zerofill = $_autotag_maq['casasdecimais'] - strlen($num);
        $num = str_repeat("0", $zerofill) . "$num";
        $cod = $cod . "{$_autotag_maq['caracter']}" . $num;
        echo "<input class=\"campo_text_ob\" type=\"text\" id=\"cc[maquinas][4]\" name=\"cc[maquinas][4]\" size=\"15\" maxlength=\"15\" value=\"$cod\" />";
    }
}

elseif ($_GET['id'] == 'conj_autotag') {
    $valor = $_GET['valor'];
    if ($_autotag_conj['status'] == 1) {
        $cod = VoltaValor(MAQUINAS_CONJUNTO, "TAG", "MID", $valor, 0);
        $sql = "SELECT TAG FROM " . MAQUINAS_CONJUNTO . " WHERE TAG LIKE '$cod%' ORDER BY TAG DESC";
        $tmp = $dba[0]->SelectLimit($sql, 1, 0);
        $campo = $tmp->fields;
        if ($_autotag_conj['caracter'] == "") {
            $c = explode($cod, $campo['TAG']);
        }
        else {
            $c = explode($_autotag_conj['caracter'], $campo['TAG']);
        }
        $num = (int) $c[2];
        $num = $num + $_autotag_conj['incrementa'];
        $zerofill = $_autotag_conj['casasdecimais'] - strlen($num);
        $num = str_repeat("0", $zerofill) . "$num";
        $cod = $cod . "{$_autotag_conj['caracter']}" . $num;
        echo "<input class=\"campo_text_ob\" type=\"text\" id=\"cc[maquinas_conjunto][2]\" name=\"cc[maquinas_conjunto][2]\" size=\"16\" maxlength=\"16\" value=\"$cod\" />";
    }
}

elseif ($_GET['id'] == 'equip_autotag') {
    $valor = $_GET['valor'];
    //die($valor);
    if ($_autotag_equip['status'] == 1) {
        $cod = VoltaValor($_autotag_equip['tabela'], "COD", "MID", $valor, 0);
        $sql = "SELECT COD FROM " . EQUIPAMENTOS . " WHERE COD LIKE '$cod%' ORDER BY COD DESC";
        $tmp = $dba[0]->SelectLimit($sql, 1, 0);
        $campo = $tmp->fields;
        if ($_autotag_equip['caracter'] == "") {
            $c = explode($cod, $campo['COD']);
        }
        else {
            $c = explode($_autotag_equip['caracter'], $campo['COD']);
        }
        
        $num = (int) $c[1];
        $num = $num + $_autotag_equip['incrementa'];
        $zerofill = $_autotag_equip['casasdecimais'] - strlen($num);
        $num = str_repeat("0", $zerofill) . "$num";
        $cod = $cod . "{$_autotag_equip['caracter']}" . $num;
        $cods[0] = substr($cod, 0, 1);
        $cods[1] = substr($cod, 1);
        $cods[0] = strtoupper($cods[0]);
        $cods[1] = strtolower($cods[1]);
        $cod = join('', $cods);
        echo "<input class=\"campo_text_ob\" type=\"text\" id=\"cc[equipamentos][4]\" name=\"cc[equipamentos][4]\" size=\"12\" maxlength=\"12\" value=\"$cod\" />";
    }
}

elseif ($_GET['id'] == 'c_unidade') {
    $valor = $_GET['valor'];
    if ($valor != 0) {
        $mat = VoltaValor(MATERIAIS, "UNIDADE", "MID", $valor, 0);
        $un = htmlentities(VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", $mat, 0));
        echo "<i>$un</i>";
    }
}
elseif ($_GET['id'] == 'select_subfam_material') {
    $valor = $_GET['fam'];
    $nome = 'cc[' . MATERIAIS . ']['.VoltaPosForm('MATERIAIS','MID_SUBFAMILIA').']';
    $sql = "WHERE MID_FAMILIA = '$valor'";
    if ($_autotag_mat['status'] == 1) $acao = "atualiza_area2('texto_autotag_fammat','parametros.php?id=texto_autotag_fammat&subfam=' + this.options[this.selectedIndex].value)";
    else $acao = "";
    FormSelectD('COD', 'DESCRICAO', MATERIAIS_SUBFAMILIA, '', $nome, $nome, "MID", "MATERIAL_SUBFAMILIA", "campo_select_ob", $acao, $sql, "A", "DESCRICAO");
}
elseif ($_GET['id'] == 'texto_autotag_fammat') {
    $valor = $_GET['subfam'];
    $nome = 'cc[' . MATERIAIS . ']['.VoltaPosForm('MATERIAIS','COD').']';
    
    if ($_autotag_mat['status'] == 1) {
        $cod = VoltaValor($_autotag_mat['tabela'], 'COD', 'MID', $valor, 0);
        $cod_tmp = $cod . $_autotag_mat['caracter'];
        
        $sql = "SELECT COD FROM " . MATERIAIS . " WHERE COD LIKE '$cod%' ORDER BY COD DESC";
        $tmp = $dba[0]->SelectLimit($sql, 1, 0);
        $campo = $tmp->fields;
        $num = (int) substr($campo['COD'], strlen($cod_tmp));

        $num = $num + $_autotag_mat['incrementa'];
        $zerofill = $_autotag_mat['casasdecimais'] - strlen($num);
        $num = str_repeat("0", $zerofill) . "$num";
        $cod = $cod . "{$_autotag_mat['caracter']}" . $num;
        echo "<input disabled=\"disabled\" class=\"campo_text_ob\" type=\"text\" id=\"\" name=\"\" size=\"25\" maxlength=\"25\" value=\"$cod\" />";
        echo "<input type=\"hidden\" id=\"$nome\" name=\"$nome\" value=\"$cod\" />";
    }
}
elseif ($_GET['id'] == 'texto_autotag_subfammat') {
    $valor = $_GET['fam'];
    $nome = 'cc[' . MATERIAIS_SUBFAMILIA . '][2]';
    
    if ($_autotag_subfammat['status'] == 1) {
        $cod = VoltaValor($_autotag_subfammat['tabela'], 'COD', 'MID', $valor, 0);
        $cod_tmp = $cod . $_autotag_subfammat['caracter'];
        
        $sql = "SELECT COD FROM " . MATERIAIS_SUBFAMILIA . " WHERE COD LIKE '$cod%' ORDER BY COD DESC";
        $tmp = $dba[0]->SelectLimit($sql, 1, 0);
        $campo = $tmp->fields;
        $num = (int) substr($campo['COD'], strlen($cod_tmp));

        $num = $num + $_autotag_subfammat['incrementa'];
        $zerofill = $_autotag_subfammat['casasdecimais'] - strlen($num);
        $num = str_repeat("0", $zerofill) . "$num";
        $cod = $cod . "{$_autotag_subfammat['caracter']}" . $num;
        echo "<input disabled=\"disabled\" class=\"campo_text_ob\" type=\"text\" id=\"\" name=\"\" size=\"25\" maxlength=\"25\" value=\"$cod\" />";
        echo "<input type=\"hidden\" id=\"$nome\" name=\"$nome\" value=\"$cod\" />";
    }
}
elseif ($_GET['id'] == 'filtra_conj') {
    // Dados para montar o select
    $mid_maq = (int) $_GET['mid_maq'];
    $campo_nome = $_GET['campo_nome'];
    $campo_id = $_GET['campo_id'];
    $null = (int) $_GET['null'];
    
    // Filtro por maquina
    $where = "";
    if ($mid_maq != 0) {
        $where = "WHERE MID_MAQUINA=$mid_maq";
    }
    
    // CSS
    $classe = ($null == 0) ? "campo_select_ob" : "campo_select";
    
    // Montando o select
    FormSelectD('TAG', 'DESCRICAO', MAQUINAS_CONJUNTO, '', $campo_nome, $campo_id, 'MID', '', $classe, "", $where);
    
    exit();
}
elseif ($_GET['id'] == 'filtro_relatorio') {
    
    // Campos a mostrar
    $mostra_emp     = (int) $_GET['mostra_emp'];
    $mostra_area    = (int) $_GET['mostra_area'];
    $mostra_setor   = (int) $_GET['mostra_setor'];
    $mostra_maq     = (int) $_GET['mostra_maq'];
    $mostra_fam_maq = (int) $_GET['mostra_fam_maq'];
    $mostra_equip   = (int) $_GET['mostra_equip'];
    $mostra_conj    = (int) $_GET['mostra_conj'];
    $multi          = (int) $_GET['multi'];
	$url_maq		= '';
    //Esse if faz com que, quando a vari�vel $filtro_maq for igual 1, esta n�o pode ser inteiro.
    if ($multi == 1) {
		$filtro_maq = $_GET['filtro_maq'];
	}
    
    // Div usado
    $div_id_filtro = ($_GET['div_id_filtro'])? $_GET['div_id_filtro'] : "filtro_relatorio";
    
    // MOSTRANDO FILTROS
    FiltrosRelatorio($mostra_emp, $mostra_area, $mostra_setor, $mostra_maq, $mostra_fam_maq, $mostra_conj, $mostra_equip, $div_id_filtro, $mostra_equipe, $multi);
    
    exit();
}
elseif ($_GET['id'] == 'filtra_cc') {
    // Dados para montar o select
    $mid_u = (int) $_GET['mid_u'];
    $campo_nome = $_GET['campo_nome'];
    $campo_id = $_GET['campo_id'];
    $null = (int) $_GET['null'];
    
    // EMPRESAS DESTE USUARIO
    $sql = "SELECT MID_EMPRESA FROM ".USUARIOS_PERMISAO_SOLICITACAO." WHERE USUARIO = '$mid_u'";
    $res = $dba[0]->Execute($sql);
    if (!$res) errofatal($dba->ErrorMsg()."<br><br>$sql");
    $emps='';
    while(!$res->EOF) {
        AddStr($emps,', ',$res->fields('MID_EMPRESA'));
        $res->MoveNext();
    }
    if (!$emps) $emps = '0';
    
    // Filtro por empresa
    $where = "WHERE MID_EMPRESA IN ($emps)";
    // CSS
    $classe = ($null == 0) ? "campo_select_ob" : "campo_select";
    
    // Montando o select
    FormSelectD('COD', 'DESCRICAO', CENTRO_DE_CUSTO, '', $campo_nome, $campo_id, 'MID', '', $classe, "", $where,'N');
    
    exit();
}
elseif ($_GET['id'] == 'filtro_ccp') {
    // Dados para montar o select
    $mid_emp = (int) $_GET['mid_emp'];
    $campo_nome = $_GET['campo_nome'];
    $campo_id = $_GET['campo_id'];
    $null = (int) $_GET['null'];
    
    // Filtro por maquina
    $where = "";
    if ($mid_emp != 0) {
        $where = "WHERE MID_EMPRESA=$mid_emp";
    }
    
    // CSS
    $classe = ($null == 0) ? "campo_select_ob" : "campo_select";
    
    // Montando o select
    FormSelectD('COD', 'DESCRICAO', CENTRO_DE_CUSTO, '', $campo_nome, $campo_id, 'MID', '', $classe, "", $where);
    
    exit();
}
elseif ($_GET['id'] == 'filtro_pend') {
    // Dados para montar o select
    $mid_maq = (int) $_GET['mid_maq'];

    ############
    // CAMPO CONJUNTO
    $campo_nome = $_GET['conj']['campo_nome'];
    $campo_id = $_GET['conj']['campo_id'];
    $null = (int) $_GET['conj']['null'];
    
    // Filtro por maquina
    $where = "";
    if ($mid_maq != 0) {
        $where = "WHERE MID_MAQUINA=$mid_maq";
    }
    
    // CSS
    $classe = ($null == 0) ? "campo_select_ob" : "campo_select";
    
    // Montando o select
    echo "<label for=\"$campo_id\">{$tdb[PENDENCIAS]['MID_CONJUNTO']}</label>";
    FormSelectD('TAG', 'DESCRICAO', MAQUINAS_CONJUNTO, '', $campo_nome, $campo_id, 'MID', '', $classe, "", $where);

    ############
    // CAMPO OS
    $campo_nome = $_GET['os']['campo_nome'];
    $campo_id = $_GET['os']['campo_id'];
    $null = (int) $_GET['os']['null'];
    
    $where = "MID_MAQUINA = $mid_maq";
    
    $oss=array();
    // ordem lub
    $sql = "SELECT DISTINCT MID_ORDEM FROM ".ORDEM_LUB." WHERE MID_MAQUINA = '$mid_maq'";
    $res = $dba[0]->Execute($sql);
    if (!$res) errofatal($dba->ErrorMsg()."<br><br>$sql");
    while(!$res->EOF) {
        $osmid=$res->fields('MID_ORDEM');
        $oss[$osmid] = $osmid;
        $res->MoveNext();
    }
    if (count($oss)) $where .= " OR MID IN (".join(', ',$oss).")";
    
    $where = "WHERE STATUS = 1 AND ($where)";
    
    // CSS
    $classe = ($null == 0) ? "campo_select_ob" : "campo_select";
    
    // Montando o select
    echo "<br clear=\"all\" />
    <label for=\"$campo_id\">{$tdb[PENDENCIAS]['MID_ORDEM']}</label>";
    FormSelectD('NUMERO', '', ORDEM, '', $campo_nome, $campo_id, 'MID', '', $classe, "", $where);
    
    exit();
}

// Filtrar as equipes por empresa no cadastro de funcionarios
if ($_GET['id'] == 'filtro_equipe') {
    // RECUPERANDO OS VALORES
    $d   = $form[$_GET['form']];
    $fil_emp = $_GET['fil_emp'];
    // FILTRO PELO EMPRESA SELECIONADA
    $fil_emp = "WHERE MID_EMPRESA=$fil_emp";
    
    // EQUIPES
    $i = VoltaPosForm($_GET['form'], $_GET['campo_equipe']);
    
    // obrigat�rio????
    if ($d[$i]['null'] == 1) $tmp_class="campo_select";
    elseif ($d[$i]['null'] == 0) $tmp_class="campo_select_ob";
    
    echo "<label for=\"cc[".$d[0]['nome']."][$i]\">" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</label>\n";
    FormSelectD("DESCRICAO", "", EQUIPES, "","cc[".$d[0]['nome']."][$i]", "cc[".$d[0]['nome']."][$i]", 'MID', 'EQUIPE', $tmp_class, $d[$i]['js'], $fil_emp, $d[$i]['autofiltro']);
    
    // RECUPERANDO OS VALORES
    $i = VoltaPosForm($_GET['form'], $_GET['campo_turno']);
    
    // TURNOS
    // obrigat�rio????
    if ($d[$i]['null'] == 1) $tmp_class="campo_select";
    elseif ($d[$i]['null'] == 0) $tmp_class="campo_select_ob";
    
    // TAMB�M PARA QUALQUER EMPRESA
    $fil_emp .= " OR MID_EMPRESA=0";
    
    echo "<br clear=\"all\" />";
    echo "<label for=\"cc[".$d[0]['nome']."][$i]\">" . $tdb[$d[0]['nome']][$d[$i]['campo']] . "</label>\n";
    FormSelectD("DESCRICAO", "", TURNOS, "","cc[".$d[0]['nome']."][$i]", "cc[".$d[0]['nome']."][$i]", 'MID', 'EQUIPE', $tmp_class, $d[$i]['js'], $fil_emp, $d[$i]['autofiltro']);
    
    exit();
}

if($_GET['id']  == 'modulos_permissao')
{
    $sql = 'DELETE FROM ' . USUARIOS_PERMISSAO . ' WHERE GRUPO = '. $_GET['mid_grupo'] . 
    ' AND MODULO = ' . $_GET['mid_modulo'];
    
    $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute($sql);
    
    if( $_GET['mid_permissao'] != 0)
    {
       $mid = GeraMid(USUARIOS_PERMISSAO, 'MID', $tdb[USUARIOS_PERMISSAO]['dba']);  
       
       $sql = "INSERT INTO " . USUARIOS_PERMISSAO . " (MID, GRUPO, PERMISSAO, MODULO) VALUES ({$mid}, {$_GET['mid_grupo']}, {$_GET['mid_permissao']}, {$_GET['mid_modulo']})";
           
       $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute($sql);
    }
}

if($_GET['id'] == 'modulos_permissao_modulo')
{
    $sql = "SELECT MID FROM ". USUARIOS_MODULOS . " WHERE ID = {$_GET['mid_modulo']} AND OP <> 0";

    $result = $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute($sql);
    
    if(!$result->EOF)
    {
       $dataUsuariosPermissao = $result->getRows();
       
       foreach($dataUsuariosPermissao as $usuariosPermissao)
       {
           $sql = "DELETE FROM ". USUARIOS_PERMISSAO . " WHERE GRUPO = {$_GET['mid_grupo']} AND MODULO = {$usuariosPermissao['MID']}";
           $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute($sql);
           
           $mid = GeraMid(USUARIOS_PERMISSAO, 'MID', $tdb[USUARIOS_PERMISSAO]['dba']);
           
           $sql = "INSERT INTO " . USUARIOS_PERMISSAO . " (MID, GRUPO, PERMISSAO, MODULO) VALUES ({$mid}, {$_GET['mid_grupo']}, {$_GET['mid_permissao']}, {$usuariosPermissao['MID']})";
           $dba[$tdb[USUARIOS_PERMISSAO]['dba']] -> Execute($sql);
       }
    }   
}
// MOSTRA OS TIPOS DE SERVI�O NO GERAL DE OS
if($_GET['id'] == 'mostra_serv') {
    $opcao = (int) $_GET['opcao'];
    // N�O SISTEMATICOS
    if ($opcao == 1) {
        echo "<label class=\"campo_label\" for=\"filtro_servico\">".$tdb[ORDEM_PLANEJADO]['TIPO_SERVICO'].":</label>";
        FormSelectD('DESCRICAO', '', TIPOS_SERVICOS, '', 'filtro_servico', 'filtro_servico', 'MID', 0 , '', '', '', 'A', '', strtoupper($ling['todos']));
        echo "<br clear=\"all\" />";
    }
    // SISTEMATICOS
    elseif ($opcao == 2) {
        echo "<label class=\"campo_label\" for=\"filtro_tipo_prog\">" . $tdb[PROGRAMACAO_TIPO]['DESC'] . ":</label>";
        FormSelectD('DESCRICAO', '', PROGRAMACAO_TIPO, '', 'filtro_tipo_prog', 'filtro_tipo_prog', 'MID', 0 , '', "atualiza_area2('plano_sel', '../parametros.php?id=mostra_plano&tipo=' + this.value)", 'WHERE MID != 4', 'A', '', strtoupper($ling['todos']));
        echo "<br clear=\"all\" />";
        echo "<div id=\"plano_sel\"></div>";
    }
    
    exit();
}

// MOSTRA OS PLANOS NO GERAL DE OS
if($_GET['id'] == 'mostra_plano') {
    $tipo = (int) $_GET['tipo'];
    if ($tipo == 1) {
        echo "<label class=\"campo_label\" for=\"filtro_plano\">" . $tdb[PLANO_PADRAO]['DESC'] . ":</label>";
        FormSelectD('DESCRICAO', '', PLANO_PADRAO, '', 'filtro_plano', 'filtro_plano', 'MID', 0 , '', '', '', 'A', '', strtoupper($ling['todos']));
    }
    elseif ($tipo == 2) {
        echo "<label class=\"campo_label\" for=\"filtro_plano\">" . $tdb[PLANO_ROTAS]['DESC'] . ":</label>";
        FormSelectD('DESCRICAO', '', PLANO_ROTAS, '', 'filtro_plano', 'filtro_plano', 'MID', 0 , '', '', '', 'A', '', strtoupper($ling['todos']));
    }
    
    exit();
}

// VERIFICA SE O CADASTRO PODE SER REMOVIDO EM CASCATA
if($_GET['id'] == 'verifica_delcascata') {
    $tabela = $_GET['tabela'];
    $oq = (int)$_GET['oq'];
    
    // Salva o motivo caso exista
    $mot_cascata = "";
    
    // N�o remover EMPRESAS
    // se tiver alguma MAQUINA
    if ($tabela == EMPRESAS) {
        // Contando as m�quinas
        $num_maq = (int)VoltaValor(MAQUINAS, 'COUNT(MID)', 'MID_EMPRESA', $oq);
        if ($num_maq > 0) {
            $mot_cascata = $ling['erro_del_empresa'];
        }
    }
    // N�o remover FAMILIA DE MAQUINA
    // se tiver alguma MAQUINA com essa familia
    elseif ($tabela == MAQUINAS_FAMILIA) {
        // Contando as m�quinas
        $num_maq = (int)VoltaValor(MAQUINAS, 'COUNT(MID)', 'FAMILIA', $oq);
        if ($num_maq > 0) {
            $mot_cascata = $ling['erro_del_fam_maq'];
        }
        elseif(in_array($oq, $manusis['familias_destino_troca'])){
            $mot_cascata = $ling['erro_del_fam_maq_dest'];
        }
    }
    // N�o remover CLASSE DE MAQUINA
    // se tiver alguma MAQUINA com essa classe
    elseif ($tabela == MAQUINAS_CLASSE) {
        // Contando as m�quinas
        $num_maq = (int)VoltaValor(MAQUINAS, 'COUNT(MID)', 'CLASSE', $oq);
        if ($num_maq > 0) {
            $mot_cascata = $ling['erro_del_classe_maq'];
        }
    }
    // N�o remover CENTRO DE CUSTO
    // se tiver alguma MAQUINA com essa classe
    elseif ($tabela == CENTRO_DE_CUSTO) {
        // Contando as m�quinas
        $num_maq = (int)VoltaValor(MAQUINAS, 'COUNT(MID)', 'CENTRO_DE_CUSTO', $oq);
        if ($num_maq > 0) {
            $mot_cascata = $ling['erro_del_cc_maq'];
        }
    }
    // N�o remover FAMILIA DE COMPONENTE
    // se tiver alguma MAQUINA com essa familia
    elseif ($tabela == EQUIPAMENTOS_FAMILIA) {
        // Contando as m�quinas
        $num_equip = (int)VoltaValor(EQUIPAMENTOS, 'COUNT(MID)', 'FAMILIA', $oq);
        if ($num_equip > 0) {
            $mot_cascata = $ling['erro_del_fam_equip'];
        }
    }
    // N�o remover FAMILIA DE MATERIAIS
    // se tiver alguma MATERIAL com essa familia
    elseif ($tabela == MATERIAIS_FAMILIA) {
        // Contando as m�quinas
        $num_equip = (int)VoltaValor(MATERIAIS, 'COUNT(MID)', 'FAMILIA', $oq);
        if ($num_equip > 0) {
            $mot_cascata = $ling['erro_del_fam_mat'];
        }
    }
    // N�o remover FAMILIA DE MATERIAIS
    // se tiver alguma MATERIAL com essa familia
    elseif ($tabela == MATERIAIS_SUBFAMILIA) {
        // Contando as m�quinas
        $num_equip = (int)VoltaValor(MATERIAIS, 'COUNT(MID)', 'MID_SUBFAMILIA', $oq);
        if ($num_equip > 0) {
            $mot_cascata = $ling['erro_del_subfam_mat'];
        }
    }
	// N�o remover PLANO PADR�O
    // se tiver alguma PROGRAMA��O ativa
    elseif ($tabela == PLANO_PADRAO) {
        // Buscando programa��es ativas no periodo para o plano
        // Tratamento para oracle
        if($manusis['db'][$tdb[PLANO_PADRAO]['dba']]['driver'] == 'oci8'){
            
            $sql = "SELECT COUNT(P.MID) as TOTAL FROM " . PROGRAMACAO . " P, " . ORDEM . " O WHERE O.MID_PROGRAMACAO = P.MID AND P.TIPO = 1 AND P.MID_PLANO = $oq AND TO_CHAR(O.DATA_PROG, 'YYYY') = " . date('Y') . " AND O.STATUS = 1";
        }
        else{
            
            $sql = "SELECT COUNT(P.MID) as TOTAL FROM " . PROGRAMACAO . " P, " . ORDEM . " O WHERE O.MID_PROGRAMACAO = P.MID AND P.TIPO = 1 AND P.MID_PLANO = $oq AND DATE_FORMAT(O.DATA_PROG, '%Y') = " . date('Y') . " AND O.STATUS = 1";
        }
        
		if (! $rs = $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql)) {
			erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
		}
		elseif ($rs->fields['TOTAL'] > 0) {
            $mot_cascata = $ling['erro_del_plano_padrao'];
        }
    }
	// N�o remover PLANO ROTAS
    // se tiver alguma PROGRAMA��O ativa
    elseif ($tabela == PLANO_ROTAS) {
        // Buscando programa��es ativas no periodo para o plano
        
        // Tratamento para data oracle
        if($manusis['db'][$tdb[PLANO_PADRAO]['dba']]['driver'] == 'oci8'){
            
            $sql = "SELECT COUNT(P.MID) as TOTAL FROM " . PROGRAMACAO . " P, " . ORDEM . " O WHERE O.MID_PROGRAMACAO = P.MID AND P.TIPO = 2 AND P.MID_PLANO = $oq AND TO_CHAR(O.DATA_PROG, 'YYYY') = " . date('Y') . " AND O.STATUS = 1";
        }
        else{
            
            $sql = "SELECT COUNT(P.MID) as TOTAL FROM " . PROGRAMACAO . " P, " . ORDEM . " O WHERE O.MID_PROGRAMACAO = P.MID AND P.TIPO = 2 AND P.MID_PLANO = $oq AND DATE_FORMAT(O.DATA_PROG, '%Y') = " . date('Y') . " AND O.STATUS = 1";
        }
		if (! $rs = $dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql)) {
			erromsg("Arquivo: " . __FILE__ . "<br />Linha: " . __LINE__ . "<br />" . $dba[$tdb[PROGRAMACAO]['dba']] -> ErrorMsg() . "<br />" . $sql);
		}
		elseif ($rs->fields['TOTAL'] > 0) {
            $mot_cascata = $ling['erro_del_plano_rota'];
        }
    }
    
    elseif($tabela == USUARIOS_GRUPOS){
        
        if((int)VoltaValor(USUARIOS_GRUPOS, 'GRUPO_MOLDES', 'MID', $oq) == 1){
            $mot_cascata = $ling['erro_del_usuarios_grupo'];
        }
        
    }
    
    elseif($tabela == TIPOS_SERVICOS){
        
        if((int)VoltaValor(TIPOS_SERVICOS, 'TROCA', 'MID', $oq) == 1){
            $mot_cascata = $ling['erro_del_tipo_servico'];
        }
        
    }
    
    elseif($tabela == MAQUINAS){
        
        if(in_array($oq, $manusis['patio_setup'])){
            $mot_cascata = $ling['erro_del_maq_patio_setup'];
        }
        
    }
    
    echo str_replace('+', ' ', urlencode(mb_strtoupper($mot_cascata)));
    
    exit();
}

// LISTA CADASTROS QUE PODEM SER REMOVIDOS EM CASCATA
if($_GET['id'] == 'lista_delcascata') {
    $tabela = $_GET['tabela'];
    
    $ret_cascata = "";
    
    // Busca por possiveis cascatas
    $tabCascata = VoltaCascata($tabela);
    if (is_array($tabCascata)) {
        foreach ($tabCascata as $tabDel => $campoDel) {
            $ret_cascata .= ($ret_cascata == '')? "" : ", ";
            $ret_cascata .= mb_strtoupper(unhtmlentities($tdb[$tabDel]['DESC']));
        }
        
        $ret_cascata = str_replace('+', ' ', urlencode($ret_cascata)) . ".";
    }
    
    echo $ret_cascata;    
    
    exit();
}

// Mostrar programa��o semanal na carteira de servi�os????
if($_GET['id'] == 'seta_ver_prog') {
    $_SESSION[ManuSess]['lt']['os']['ver_prog'] = ($_SESSION[ManuSess]['lt']['os']['ver_prog'] == 1)? 0 : 1;
    exit ();
}

// Mostrar calculadora de backlog na carteira de servi�os????
if($_GET['id'] == 'seta_ver_backlog') {
    $_SESSION[ManuSess]['lt']['os']['ver_backlog'] = ($_SESSION[ManuSess]['lt']['os']['ver_backlog'] == 1)? 0 : 1;
    exit ();
}

if ($_GET['id'] == "rotamaq") {
    $osmid = (int) $_GET['osmid'];
    
    $sql = "SELECT DISTINCT M.DESCRICAO, M.COD FROM " . ORDEM_LUB . " L, " . MAQUINAS . " M WHERE L.MID_MAQUINA = M.MID AND L.MID_ORDEM = $osmid";
    $rs = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
    $i = 0;
    while (! $rs -> EOF) {
        if ($i == 0) {
            $rs -> MoveNext();
            $i ++;
            continue;
        }
        echo "<img src=\"imagens/icones/22x22/local3.png\" border=\"0\" /> " . htmlentities($rs->fields['COD'] . "-" . $rs->fields['DESCRICAO']) . "<br />";
        $rs -> MoveNext();
    }
    
    exit ();
}
elseif($_GET['id'] == 'anti_logout') {
    $timeout_minutos = 5;
    echo "<html><head>
    <meta http-equiv=\"refresh\" content=\"".($timeout_minutos * 60).";url=parametros.php?id=anti_logout\">
    <style>
    * {
    	margin:0px;
    	padding:2px;
	}
    </style>
    </head><body>
    <span style=\"font: 8pt arial; color: #999999; padding:0; margin:0\">
    &Uacute;ltimo refresh as ".date("H:i:s")." (refresh a cada $timeout_minutos minutos).
    </span>
    </body>
    </html>";
    exit();
}
