<?php
set_time_limit(0);
// Fun��es do Sistema
if (!require("lib/mfuncoes.php"))
    die($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("conf/manusis.conf.php"))
    die($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/" . $manusis['idioma'][0] . ".php"))
    die($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php"))
    die($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("lib/bd.php"))
    die($ling['bd01']);
elseif (!require("lib/delcascata.php"))
    die($ling['bd01']);
// Autentifica��o
//elseif (!require("lib/autent.php"))
//    die($ling['autent01']);



// Caso n�o exista um padr�o definido
if (!file_exists("temas/" . $manusis['tema'] . "/estilo.css"))
    $manusis['tema'] = "padrao";

echo "<table id='dados_processados' style='border: 1px solid black' width='100%'>\n";

// T�tulo da tabela
echo "<tr>\n";
echo "<td colspan='13' style='text-align:center; font-weight: bold'>Migra&ccedil;&atilde;o dos Obj da fam&iacute;lia MOLDES para Componentes</td>";
echo "</tr>\n";

$styleTh = "style='border: 1px solid black;font-weight: bold;font-size:7.5pt;font-family:Arial; background-color: #cccccc'";

$midFamilia = 0;
    
$dados = array();
$dados['MID']           = GeraMid(EQUIPAMENTOS_FAMILIA, 'MID', $tdb[EQUIPAMENTOS_FAMILIA]['dba']);
$dados['COD']           = "'".VoltaValor(MAQUINAS_FAMILIA, 'COD', 'MID', 74)."'";
$dados['DESCRICAO']     =  "'".VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', 74)." (MIGRADOS)'";
$dados['MID_EMPRESA']   =  VoltaValor(MAQUINAS_FAMILIA, 'MID_EMPRESA', 'MID', 74);

$sql = "INSERT INTO ".EQUIPAMENTOS_FAMILIA." (".implode(',', array_keys($dados)).") VALUES (".implode(',', array_values($dados) ).")";
if (!$dba[$tdb[EQUIPAMENTOS_FAMILIA]['dba']]->Execute($sql)) {
    erromsg("Erro ao processar dados de {$tdb[EQUIPAMENTOS_FAMILIA]['DESC']} em:<br />
        Arquivo: " . __FILE__ . " <br />
        Linha: " . __LINE__ . " <br />
        Erro: {$dba[$tdb[EQUIPAMENTOS_FAMILIA]['dba']]->ErrorMsg()}<br />
        SQL: $sql
    ");
}
else{
    echo "<tr>\n";
    echo "<td colspan='13' {$styleTh}>Criado a {$tdb[EQUIPAMENTOS_FAMILIA]['DESC']}: {$dados['COD']} - {$dados['DESCRICAO']}</td>";
    echo "</tr>\n";

    $midFamilia = $dados['MID'];

}

// Cabe�alho

echo "<tr>\n";
echo "<td colspan='6' {$styleTh}>Origem</td>\n";
echo "<td colspan='6' {$styleTh}>Destino</td>\n";
echo "</tr>\n";

echo "<tr>\n";

echo "<td {$styleTh}>{$tdb[MAQUINAS]['COD']}</td>\n";
echo "<td {$styleTh}>{$tdb[MAQUINAS]['DESCRICAO']}</td>\n";
echo "<td {$styleTh}>{$tdb[MAQUINAS]['STATUS']}</td>\n";
echo "<td {$styleTh}>{$tdb[MAQUINAS_FORNECEDOR]['MID_FORNECEDOR']}</td>\n";
echo "<td {$styleTh}>{$tdb[MAQUINAS]['MID_EMPRESA']}</td>\n";
echo "<td {$styleTh}>{$tdb[MAQUINAS]['CENTRO_DE_CUSTO']}</td>\n";

echo "<td {$styleTh}>{$tdb[EQUIPAMENTOS]['COD']}</td>\n";
echo "<td {$styleTh}>{$tdb[EQUIPAMENTOS]['DESCRICAO']}</td>\n";
echo "<td {$styleTh}>{$tdb[EQUIPAMENTOS]['FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[EQUIPAMENTOS]['FORNECEDOR']}</td>\n";
echo "<td {$styleTh}>{$tdb[EQUIPAMENTOS]['MID_STATUS']}</td>\n";
echo "<td {$styleTh}>{$tdb[EQUIPAMENTOS]['MID_EMPRESA']}</td>\n";


echo "</tr>\n";

$sql = "SELECT * FROM ".MAQUINAS." WHERE FAMILIA = 74";
$i = 0;
if (!$rs = $dba[$tdb[MAQUINAS]['dba']]->Execute($sql)) {
    erromsg("Erro ao processar dados de {$tdb[MAQUINAS]['DESC']} em:<br />
        Arquivo: " . __FILE__ . "
        Linha: " . __LINE__ . " <br />
        Erro: {$dba[$tdb[MAQUINAS]['dba']]->ErrorMsg()}<br />
        SQL: $sql
    ");
}
elseif (!$rs->EOF) {
    
    $styleTd = "style='border: 1px solid black;font-size:7.5pt;font-family:Arial'";
    
    while (!$rs->EOF) {
        $row = $rs->fields;

        $maqForn = (int)VoltaValor(MAQUINAS_FORNECEDOR, 'MID_FORNECEDOR', 'MID_MAQUINA', $row['MID']);
        
        echo "<tr>\n";

        echo "<td {$styleTd}>{$row['COD']}&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($row['DESCRICAO'])."&nbsp;</td>\n";
        echo "<td {$styleTd}>".Voltavalor(MAQUINAS_STATUS, 'DESCRICAO', 'MID', $row['STATUS'])."&nbsp;</td>\n";
        echo "<td {$styleTd}>".VoltaValor(FORNECEDORES, 'NOME', 'MID', $maqForn)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".VoltaValor(EMPRESAS, 'NOME', 'MID', $row['MID_EMPRESA'])."&nbsp;</td>\n";
        echo "<td {$styleTd}>".VoltaValor(CENTRO_DE_CUSTO, 'DESCRICAO', 'MID', $row['CENTRO_DE_CUSTO'], 0, 1)."&nbsp;</td>\n";
        
        $dados = array();
        $dados['MID']           = GeraMid(EQUIPAMENTOS, 'MID', $tdb[EQUIPAMENTOS]['dba']);
        $dados['COD']           = "'{$row['COD']}'";
        $dados['DESCRICAO']     = "'{$row['DESCRICAO']}'";
        $dados['FAMILIA']       = $midFamilia;
        $dados['FORNECEDOR']    = $maqForn;
        $dados['MID_STATUS']    = $row['STATUS'];
        $dados['MID_EMPRESA']   = $row['MID_EMPRESA'];
        $dados['MID_MAQUINA']   = 0;
        $dados['MID_CONJUNTO']  = 0;
        
        $sql = "INSERT INTO ".EQUIPAMENTOS." (".implode(',', array_keys($dados)).") VALUES(".implode(',', array_values($dados)).")";
        if (!$dba[$tdb[EQUIPAMENTOS]['dba']]->Execute($sql)) {
            erromsg("Erro ao processar dados de {$tdb[EQUIPAMENTOS]['DESC']} em:<br />
                Arquivo: " . __FILE__ . " <br />
                Linha: " . __LINE__ . " <br />
                Erro: {$dba[$tdb[EQUIPAMENTOS]['dba']]->ErrorMsg()}<br />
                SQL: $sql
            ");
        }
        else{
            // Logando a inser��o
            logar(3, "MIGRA��O DA MAQUINA: {$row['COD']} - {$row['DESCRICAO']} PARA COMPONENTE \n\r ", EQUIPAMENTOS, 'MID', $dados['MID']);
            
            echo "<td {$styleTd}>".htmlentities(VoltaValor(EQUIPAMENTOS, 'COD', 'MID', $dados['MID']))."&nbsp;</td>";
            echo "<td {$styleTd}>".htmlentities(VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $dados['MID']))."&nbsp;</td>";
            echo "<td {$styleTd}>".htmlentities(VoltaValor(EQUIPAMENTOS_FAMILIA, 'COD', 'MID', VoltaValor(EQUIPAMENTOS, 'FAMILIA', 'MID', $dados['MID'])))." - ".htmlentities(VoltaValor(EQUIPAMENTOS_FAMILIA, 'DESCRICAO', 'MID', VoltaValor(EQUIPAMENTOS, 'FAMILIA', 'MID', $dados['MID'])))."&nbsp;</td>";
            echo "<td {$styleTd}>".htmlentities(VoltaValor(FORNECEDORES, 'NOME', 'MID', VoltaValor(EQUIPAMENTOS, 'FORNECEDOR', 'MID', $dados['MID'])))."&nbsp;</td>";
            echo "<td {$styleTd}>".htmlentities(VoltaValor(MAQUINAS_STATUS, 'DESCRICAO', 'MID', VoltaValor(EQUIPAMENTOS, 'MID_STATUS', 'MID', $dados['MID'])))."&nbsp;</td>";
            echo "<td {$styleTd}>".htmlentities(VoltaValor(EMPRESAS, 'NOME', 'MID', VoltaValor(EQUIPAMENTOS, 'MID_EMPRESA', 'MID', $dados['MID']), $tdb[EQUIPAMENTOS]['dba'], 1))."&nbsp;</td>";
            
            // DADOS PARA A MIGRA��O DE ORDENS E ALTERA��ES NOS CADASTROS
            $maqMigra['MID_MAQUINA'][] = $row['MID'];
            $maqMigra[$row['MID']]['COMPONENTE_DESTINO'] = $dados['MID'];
        }
        
        echo "</tr>\n";
        
        
        $rs->MoveNext();
        $i ++;
    }
}
else {
    echo "<tr>\n";
    echo "<td colspan='12' {$styleTd}>Nenhum registro encontrado</td>";
    echo "</tr>\n";
}

echo "<tr>\n";
echo "<td colspan='12' {$styleTd}>$i Registros alterados</td>";
echo "</tr>\n";
    
echo "</table>\n";

echo "<br clear='all'/>";

/**
 * Importa��o de ordens de servi�o e remo��o de �tens relacionados �s maquinas que dever�o ser removidos 
 */
echo "<table id='dados_processados' style='border: 1px solid black' width='100%'>\n";

// T�tulo da tabela
echo "<tr>\n";
echo "<td colspan='19' style='text-align:center; font-weight: bold'>Atualiza&ccedil;&atilde;o das Ordens</td>";
echo "</tr>\n";

$styleTh = "style='border: 1px solid black;font-weight: bold;font-size:7.5pt;font-family:Arial; background-color: #cccccc'";

echo "<tr>";
echo "<td colspan='6' {$styleTh}>Dados da Ordem</td>\n";
echo "<td colspan='7' {$styleTh}>Localiza&ccedil;&atilde;o anterior</td>\n";
echo "<td colspan='6' {$styleTh}>Localiza&ccedil;&atilde;o pos migra&ccedil;&atilde;o</td>\n";
echo "</tr>";

// Cabe�alho
echo "<tr>\n";

// DADOS DA ORDEM
echo "<td {$styleTh}>{$tdb[ORDEM]['NUMERO']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['DATA_ABRE']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['DATA_PROG']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['TIPO']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['TIPO_SERVICO']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['STATUS']}</td>\n";

// DADOS DE LOCALIZA��O ORIGEM
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_EMPRESA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_AREA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_SETOR']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_MAQUINA_FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_MAQUINA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_CONJUNTO']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_EQUIPAMENTO']}</td>\n";

// DADOS DA MODIFICA��O
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_AREA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_SETOR']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_MAQUINA_FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_MAQUINA']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_CONJUNTO']}</td>\n";
echo "<td {$styleTh}>{$tdb[ORDEM]['MID_EQUIPAMENTO']}</td>\n";

echo "</tr>\n";

$sql = "SELECT * FROM ".ORDEM." WHERE MID_MAQUINA IN (".implode(',', array_values($maqMigra['MID_MAQUINA'])).")";
$j = 0;
if (!$rs = $dba[$tdb[ORDEM]['dba']]->Execute($sql)) {
    erromsg("Erro ao processar dados de {$tdb[ORDEM]['DESC']} em:<br />
        Arquivo: " . __FILE__ . "
        Linha: " . __LINE__ . " <br />
        Erro: {$dba[$tdb[ORDEM]['dba']]->ErrorMsg()}<br />
        SQL: $sql
    ");
}
elseif (!$rs->EOF) {
    while (!$rs->EOF) {
        $row = $rs->fields;


        $styleTd = "style='border: 1px solid black;font-size:7.5pt;font-family:Arial'";
        
        // Dados pre update
        $midTipo        = VoltaValor(ORDEM, 'TIPO', 'MID', $row['MID']);
        $tipo           = VoltaValor(PROGRAMACAO_TIPO, 'DESCRICAO', 'MID', $midTipo);
        $midTipoServ    = VoltaValor(ORDEM, 'TIPO', 'MID', $row['MID']);
        $tipoServ       = VoltaValor(TIPOS_SERVICOS,'DESCRICAO', 'MID', $midTipoServ);
        $midStatus      = VoltaValor(ORDEM, 'STATUS', 'MID', $row['MID']);
        $status         = VoltaValor(ORDEM_STATUS, 'DESCRICAO', 'MID', $midStatus);
        
        $empresa        = VoltaValor(EMPRESAS, 'NOME', 'MID', $row['MID_EMPRESA'], $tdb[EMPRESAS]['dba'], 1);
        $area           = VoltaValor(AREAS, 'DESCRICAO', 'MID', $row['MID_AREA'], $tdb[AREAS]['dba'], 1);
        $setor          = VoltaValor(SETORES, 'DESCRICAO', 'MID', $row['MID_SETOR'], $tdb[SETORES]['dba'], 1);
        $familia        = VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', $row['MID_MAQUINA_FAMILIA'], $tdb[MAQUINAS_FAMILIA]['dba'], 1);
        $maquina        = VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $row['MID_MAQUINA'], $tdb[MAQUINAS]['dba'], 1);
        $conjunto       = VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $row['MID_CONJUNTO'], $tdb[MAQUINAS_CONJUNTO]['dba'], 1);
        $equipamento    = VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $row['MID_EQUIPAMENTO'], $tdb[EQUIPAMENTOS]['dba'], 1);
        
        // Dados para o update
        $midEquipamento = $maqMigra[$row['MID_MAQUINA']]['COMPONENTE_DESTINO'];
        
        $set = array();
        $set[] = "MID_AREA = 0";
        $set[] = "MID_SETOR = 0";
        $set[] = "MID_MAQUINA_FAMILIA = 0";
        $set[] = "MID_MAQUINA = 0";
        $set[] = "MID_CONJUNTO = 0";
        $set[] = "MID_EQUIPAMENTO = {$midEquipamento}";
        
        UpdateItem(ORDEM, implode(',', array_values($set)), 'MID', $row['MID']);
        
        // Dados p�s Update
        $novoMidArea = (int)VoltaValor(ORDEM, 'MID_AREA', 'MID', $row['MID']);
        $novaArea = ($novoMidArea != 0 ? VoltaValor(AREAS, 'DESCRICAO', 'MID', $novoMidArea, 0, 1) : "&nbsp;");
        $novoMidSetor = (int)VoltaValor(ORDEM, 'MID_SETOR', 'MID', $row['MID']);
        $novoSetor = ($novoMidSetor != 0 ? VoltaValor(SETORES, 'DESCRICAO', 'MID', $novoMidSetor, 0, 1) : "&nbsp;");
        $novoMidFamilia = (int)VoltaValor(ORDEM, 'MID_MAQUINA_FAMILIA', 'MID', $row['MID']);
        $novaFamilia = ($novoMidFamilia != 0 ? VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', $novoMidFamilia, 0, 1) : "&nbsp;");
        $novoMidMaquina = (int)VoltaValor(ORDEM, 'MID_MAQUINA', 'MID', $row['MID']);
        $novaMaquina = ($novoMidMaquina != 0 ? VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $novoMidMaquina, 0, 1) : "&nbsp;");
        $novoMidConjunto = (int)VoltaValor(ORDEM, 'MID_CONJUNTO', 'MID', $row['MID']);
        $novoConjunto = ($novoMidConjunto != 0 ? VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $novoMidConjunto, 0, 1) : "&nbsp;");
        $novoMidEquipamento = (int)VoltaValor(ORDEM, 'MID_EQUIPAMENTO', 'MID', $row['MID']);
        $novoEquipamento = ($novoMidEquipamento != 0 ? VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $novoMidEquipamento, 0, 1) : "&nbsp;");

        echo "<tr>\n";

        echo "<td {$styleTd}>".htmlentities($row['NUMERO'])."&nbsp;</td>\n";
        echo "<td {$styleTd}>".NossaData($row['DATA_ABRE'])."&nbsp;</td>\n";
        echo "<td {$styleTd}>".NossaData($row['DATA_PROG'])."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($tipo)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($tipoServ)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($status)."&nbsp;</td>\n";
        
        echo "<td {$styleTd}>".htmlentities($empresa)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($area)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($setor)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($familia)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($maquina)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($conjunto)."&nbsp;</td>\n";
        echo "<td {$styleTd}>".htmlentities($equipamento)."&nbsp;</td>\n";
        
        echo "<td {$styleTd}>".$novaArea."</td>\n";
        echo "<td {$styleTd}>".$novoSetor."</td>\n";
        echo "<td {$styleTd}>".$novaFamilia."</td>\n";
        echo "<td {$styleTd}>".$novaMaquina."</td>\n";
        echo "<td {$styleTd}>".$novoConjunto."</td>\n";
        echo "<td {$styleTd}>".htmlentities($novoEquipamento)."</td>\n";

        
        echo "</tr>\n";


        $rs->MoveNext();
        $j ++;
    }
    
    echo "<tr>\n";
    echo "<td colspan='19' {$styleTd}>$j Registro Alterados</td>";
    echo "</tr>\n";
    
}
else {
    echo "<tr>\n";
    echo "<td colspan='19' {$styleTd}>Nenhum registro encontrado</td>";
    echo "</tr>\n";
}
echo "</table>\n";

echo "<br clear='all'/>";

// Altera��o dos planos
echo "<table id='dados_processados' style='border: 1px solid black' width='100%'>\n";

// T�tulo da tabela
echo "<tr>\n";
echo "<td colspan='11' style='text-align:center; font-weight: bold'>Altera&ccedil;&atilde;o nos Planos de Manuten&ccedil;&atilde;o Preventiva</td>";
echo "</tr>\n";

$styleTh = "style='border: 1px solid black;font-weight: bold;font-size:7.5pt;font-family:Arial; background-color: #cccccc'";

echo "<tr>";
echo "<td colspan='3' {$styleTh}>Dados do plano</td>";
echo "<td colspan='4' {$styleTh}>Dados de origem</td>";
echo "<td colspan='4' {$styleTh}>Dados de destino</td>";
echo "</tr>";

// Cabe�alho
echo "<tr>\n";

// DADOS DO PLANO
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['MID_EMPRESA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['DESCRICAO']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['OBSERVACAO']}</td>\n";

// LOCALIZA��O
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['MAQUINA_FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['EQUIPAMENTO_FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['MID_MAQUINA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['TIPO']}</td>\n";

// LOCALIZA��O DESTINO
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['MAQUINA_FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['EQUIPAMENTO_FAMILIA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['MID_MAQUINA']}</td>\n";
echo "<td {$styleTh}>{$tdb[PLANO_PADRAO]['TIPO']}</td>\n";

echo "</tr>\n";

$sql = "SELECT * FROM ".PLANO_PADRAO." WHERE MAQUINA_FAMILIA = 74 OR MID_MAQUINA IN (".implode(',', array_values($maqMigra['MID_MAQUINA'])).")";
$k = 0;
if (!$rs = $dba[$tdb[PLANO_PADRAO]['dba']]->Execute($sql)) {
    erromsg("Erro ao processar dados de {$tdb[PLANO_PADRAO]['DESC']} em:<br />
        Arquivo: " . __FILE__ . "
        Linha: " . __LINE__ . " <br />
        Erro: {$dba[$tdb[PLANO_PADRAO]['dba']]->ErrorMsg()}<br />
        SQL: $sql
    ");
}
elseif (!$rs->EOF) {
    while (!$rs->EOF) {
        $row = $rs->fields;


        $styleTd = "style='border: 1px solid black;font-size:7.5pt;font-family:Arial'";

        $empresa    = VoltaValor(EMPRESAS, 'NOME', 'MID', $row['MID_EMPRESA'], 0, 1);
        $maqFam     = VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', $row['MAQUINA_FAMILIA'], 0, 1);
        $eqpmtFam   = VoltaValor(EQUIPAMENTOS_FAMILIA, 'DESCRICAO', 'MID', $row['EQIOPAMENTO_FAMILIA'], 0, 1);
        $maq        = VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $row['MID_MAQUINA'], 0, 1);
        $tipo       = VoltaValor(TIPO_PLANOS, 'DESCRICAO', 'MID', $row['TIPO']);
        
        // alterando de acordo com o tipo
        // s� alterei esse tipo porque n�o haviam outros no banco
        $set = array();
        if($row['TIPO'] == 2){
            
            $set[] = "MAQUINA_FAMILIA = 0";
            $set[] = "EQUIPAMENTO_FAMILIA = {$midFamilia}";
            $set[] = "MID_MAQUINA = 0";
            $set[] = "TIPO = 4";
            
        }
        
        UpdateItem(PLANO_PADRAO, implode(',', array_values($set)), 'MID', $row['MID']);
        
        // DADOS POS MIGRA��O
        $novaMaqFam     = VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', VoltaValor(PLANO_PADRAO, 'MAQUINA_FAMILIA', 'MID', $row['MID']), 0, 1);
        $novoEqpmtFam   = VoltaValor(EQUIPAMENTOS_FAMILIA, 'DESCRICAO', 'MID', VoltaValor(PLANO_PADRAO, 'EQUIPAMENTO_FAMILIA', 'MID', $row['MID']), 0, 1);
        $novaMaq        = VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', VoltaValor(PLANO_PADRAO, 'MID_MAQUINA', 'MID', $row['MID']), 0, 1);
        $novoTipo       = VoltaValor(TIPO_PLANOS, 'DESCRICAO', 'MID', VoltaValor(PLANO_PADRAO, 'TIPO', 'MID', $row['MID']));
        
        echo "<tr>\n";

        echo "<td {$styleTd}>".htmlentities($empresa)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($row['DESCRICAO'])."</td>\n";
        echo "<td {$styleTd}>".htmlentities($row['OBSERVACAO'])."</td>\n";
                
        echo "<td {$styleTd}>".htmlentities($maqFam)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($eqpmtFam)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($maq)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($tipo)."</td>\n";
        
        echo "<td {$styleTd}>".htmlentities($novaMaqFam)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($novoEqpmtFam)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($novaMaq)."</td>\n";
        echo "<td {$styleTd}>".htmlentities($novoTipo)."</td>\n";
        
        
        echo "</tr>\n";


        $rs->MoveNext();
        $k ++;
    }
    echo "<tr>\n";
    echo "<td colspan='11' {$styleTd}>$k Registros alterados</td>";
    echo "</tr>\n";
}
else {
    echo "<tr>\n";
    echo "<td colspan='11' {$styleTd}> Nenhum registro encontrado</td>";
    echo "</tr>\n";
}
echo "</table>\n";


echo "<br clear='all'/>";

echo "<table id='dados_processados' style='border: 1px solid black' width='100%'>\n";

// T�tulo da tabela
echo "<tr>\n";
echo "<td colspan='2' style='text-align:center; font-weight: bold'>Remo&ccedil;&atilde;o das m&aacute;quinas</td>";
echo "</tr>\n";

// removendo os dados das m�quinas
foreach ($maqMigra['MID_MAQUINA'] as $k => $midMaquina){
    DelCascata(MAQUINAS, $midMaquina, 'MID', TRUE, TRUE);
}

$sql = "SELECT * FROM ".MAQUINAS." WHERE MID IN (".implode(',', array_values($maqMigra['MID_MAQUINA'])).")";
if (!$rs = $dba[$tdb[MAQUINAS]['dba']]->Execute($sql)) {
    erromsg("Erro ao processar dados de {$tdb[MAQUINAS]['DESC']} em:<br />
    Arquivo: " . __FILE__ . "
    Linha: " . __LINE__ . " <br />
    Erro: {$dba[$tdb[MAQUINAS]['dba']]->ErrorMsg()}<br />
    SQL: $sql
    ");
}
elseif (!$rs->EOF) {
    
    // Cabe�alho
    
    $styleTh = "style='border: 1px solid black;font-weight: bold;font-size:7.5pt;font-family:Arial; background-color: #cccccc'";

    echo "<tr>\n";
    echo "<td colspan='2' {$styleTh}>Algum problema ocorreu durante a remo&ccedil;&atilde;o de um ou mais m&aacute;quinas. <br /> A seguir as m&aacute;quinas n&atilde;o removidas. Essas ser�o removidas manualmente</td>\n";
    echo "</tr>\n";
    
    while (!$rs->EOF) {
        $row = $rs->fields;


        $styleTd = "style='border: 1px solid black;font-size:7.5pt;font-family:Arial'";


        echo "<tr>\n";

        echo "<td {$styleTd}>{$row['COD']}</td>\n";
        echo "<td {$styleTd}>{$row['DESCRICAO']}</td>\n";
        
        echo "</tr>\n";


        $rs->MoveNext();
    }
}
else {
    echo "<tr>\n";
    echo "<td {$styleTd}>M&aacute;quinas removidas com sucesso.</td>";
    echo "</tr>\n";
}
echo "</table>\n";