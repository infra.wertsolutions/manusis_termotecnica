<?php

// Fun��es do Sistema
if (!require("lib/mfuncoes.php")) {
    die($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configura��es
elseif (!require("conf/manusis.conf.php")) {
    die($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/" . $manusis['idioma'][0] . ".php")) {
    die($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstra��o de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
    die($ling['bd01']);
}
// Informa��es do banco de dados
elseif (!require("lib/bd.php")) {
    die($ling['bd01']);
}
elseif (!require("lib/autent.php")) {
    die($ling['bd01']);
}
elseif (!require("lib/delcascata.php")) {
    die($ling['bd01']);
}
// Formul�rios
elseif (!require("lib/forms.php")) {
    die($ling['bd01']);
}
// Modulos
elseif (!require("conf/manusis.mod.php")) {
    die($ling['mod01']);
}


$sql = "SELECT * FROM " . EMPRESAS . "";
if (!$rows = $dba[$tdb[EMPRESAS]['dba']]->execute($sql)) {
    erromsg("

    Erro ao processar dados de " . $tdb[EMPRESAS]['DESC'] . " em <br />
    Arquivo: " . __FILE__ . " <br />
    Linha: " . __LINE__ . " <br />
    Erro: " . $dba[$tdb[EMPRESAS]['dba']]->ErrorMsg() . " <br />
    SQL: " . $sql . " <br />

    ");
}
elseif (!$rows->EOF) {

    foreach ($rows as $row) {

        echo "<br /> Processando dados da empresa {$row['NOME']}";

        $codEmp = $row['NOME'];

        $sufixoEmp = substr($codEmp, -2);

        $codLoc1 = "{$sufixoEmp}.MOD";
        $descLoc1 = "MOLDADOS";
        $midLoc1 = GeraMid(AREAS, 'MID', $tdb[AREAS]['dba']);

        if ((int) VoltaValor(AREAS, 'MID', 'COD', $codLoc1) == 0) {

            echo "<br /> A empresa {$row['NOME']} n�o possui a localiza��o $codLoc1";

            $sql = "INSERT INTO " . AREAS . " (MID, COD, DESCRICAO, MID_EMPRESA) VALUES ({$midLoc1}, '{$codLoc1}', '{$descLoc1}', {$row['MID']})";
            if (!$dba[$tdb[AREAS]['dba']]->execute($sql)) {

                erromsg("
                    Erro ao processar dados de " . $tdb[AREAS]['DESC'] . " em <br />
                    Arquivo: " . __FILE__ . " <br />
                    Linha: " . __LINE__ . " <br />
                    Erro: " . $dba[$tdb[AREAS]['dba']]->ErrorMsg() . " <br />
                    SQL: " . $sql . " <br />
                ");
            }
            else {

                echo "<br />" . htmlentities("Localiza��o 1  $codLoc1 - $descLoc1 criada com sucesso");

                $codLoc2 = "{$codLoc1}.PSU";
                $descLoc2 = "P�TIO DE SETUP";
                $midLoc2 = GeraMid(SETORES, 'MID', $tdb[SETORES]['dba']);

                echo "<br /> Cadastrando a localiza��o 2 para a $codLoc1 - $descLoc1";
                $sql = "INSERT INTO " . SETORES . " (MID, MID_AREA, COD, DESCRICAO) VALUES ({$midLoc2}, {$midLoc1}, '{$codLoc2}', '{$descLoc2}')";
                if (!$dba[$tdb[AREAS]['dba']]->execute($sql)) {

                    erromsg("
                        Erro ao processar dados de " . $tdb[SETORES]['DESC'] . " em <br />
                        Arquivo: " . __FILE__ . " <br />
                        Linha: " . __LINE__ . " <br />
                        Erro: " . $dba[$tdb[SETORES]['dba']]->ErrorMsg() . " <br />
                        SQL: " . $sql . " <br />
                    ");
                }
                else {

                    echo "<br />Localiza��o $codLoc2 - $descLoc2 criada com sucesso em $codLoc1 - $descLoc1";
                }
            }
        }
        else {

            echo "<br />A empresa {$row['NOME']} Possui a localiza��o 1 $codLoc1 - $descLoc1";

            $midLoc1 = (int) VoltaValor(AREAS, 'MID', 'COD', $codLoc1);
            $codLoc2 = "{$codLoc1}.PSU";
            $descLoc2 = "P�TIO DE SETUP";
            $midLoc2 = GeraMid(SETORES, 'MID', $tdb[SETORES]['dba']);

            echo "<br /> Cadastrando a localiza��o 2 para a $codLoc1 - $descLoc1";
            $sql = "INSERT INTO " . SETORES . " (MID, MID_AREA, COD, DESCRICAO) VALUES ({$midLoc2}, {$midLoc1}, '{$codLoc2}', '{$descLoc2}')";
            if (!$dba[$tdb[AREAS]['dba']]->execute($sql)) {

                erromsg("
                        Erro ao processar dados de " . $tdb[SETORES]['DESC'] . " em <br />
                        Arquivo: " . __FILE__ . " <br />
                        Linha: " . __LINE__ . " <br />
                        Erro: " . $dba[$tdb[SETORES]['dba']]->ErrorMsg() . " <br />
                        SQL: " . $sql . " <br />
                ");
            }
            else {
                echo "<br />Localiza��o $codLoc2 - $descLoc2 criada com sucesso";
            }
        }

        // Cadastrando o centro de custo de acordo com o padr�o da empresa
        if ((int) VoltaValor(CENTRO_DE_CUSTO, 'MID', array('MID_EMPRESA', 'COD'), array($row['MID'], '410')) == 0) {

            echo "<br /> A empresa {$row['MID']} n�o possui C.C 410";
            $midCentro = GeraMid(CENTRO_DE_CUSTO, 'MID', $tdb[CENTRO_DE_CUSTO]['dba']);

            $sql = "INSERT INTO " . CENTRO_DE_CUSTO . " (MID, MID_EMPRESA, COD, DESCRICAO, PAI) VALUES ({$midCentro}, {$row['MID']}, '410', 'MOLDADOS {$row['NOME']}', 0)";
            if (!$dba[$tdb[CENTRO_DE_CUSTO]['dba']]->Execute($sql)) {

                erromsg("
                    Erro ao processar dados de " . $tdb[CENTRO_DE_CUSTO]['DESC'] . " em <br />
                    Arquivo: " . __FILE__ . " <br />
                    Linha: " . __LINE__ . " <br />
                    Erro: " . $dba[$tdb[CENTRO_DE_CUSTO]['dba']]->ErrorMsg() . " <br />
                    SQL: " . $sql . " <br />
                ");
            }
            else {
                echo "<br /> Centro de custo 410 MOLDADOS {$row['NOME']} Criado com sucesso!";
            }
        }
        else {

            echo "<br />A empresa {$row['NOME']} J� possui o C.C 410";
        }

        // Cadastrando a m�quina em caso de sucesso do cacastro do setor
        if ((int) VoltaValor(SETORES, 'MID', 'COD', "{$codLoc1}.PSU") != 0) {

            echo "<br />Setor {$codLoc1}.PSU Localizado para cadastro da m�quina P�tio de Setup";

            $midMaq = GeraMid(MAQUINAS, "MID", $tdb[MAQUINAS]['dba']);
            $midCentroCusto = VoltaValor(CENTRO_DE_CUSTO, 'MID', array('MID_EMPRESA', 'COD'), array($row['MID'], '410'));

            $sql = "INSERT INTO " . MAQUINAS . " (MID, MID_EMPRESA, MID_SETOR, COD, DESCRICAO, STATUS, CENTRO_DE_CUSTO, FAMILIA) VALUES ($midMaq, {$row['MID']}, {$midLoc2}, '{$sufixoEmp}.PSU', 'P�TIO DE SETUP', 1, {$midCentroCusto}, 13)";
            if (!$dba[$tdb[CENTRO_DE_CUSTO]['dba']]->execute($sql)) {

                erromsg("
                    Erro ao processar dados de " . $tdb[CENTRO_DE_CUSTO]['DESC'] . " em <br />
                    Arquivo: " . __FILE__ . " <br />
                    Linha: " . __LINE__ . " <br />
                    Erro: " . $dba[$tdb[CENTRO_DE_CUSTO]['dba']]->ErrorMsg() . " <br />
                    SQL: " . $sql . " <br />
                ");
            }
            else {

                $midConj = GeraMid(MAQUINAS_CONJUNTO, 'MID', $tdb[MAQUINAS_CONJUNTO]['dba']);
                echo "<br />P�tio de setup para empresa {$row['MID']} criado!";
                
                echo "<br /> Cadastrando a posi��o no p�tio";
                $sql = "INSERT INTO " . MAQUINAS_CONJUNTO . " (
                        MID, MID_MAQUINA, TAG, DESCRICAO, COMPLEMENTO, 
                        FABRICANTE, FICHA_TECNICA, MARCA, MODELO, NSERIE,
                        POTENCIA, PRESSAO, CORRENTE, VAZAO, TENSAO, MID_CONJUNTO,
                        MID_OLD, MID_CARGA
                        )
                        VALUES (
                         $midConj, $midMaq, '{$sufixoEmp}.PSU.PAT', 'P�TIO', '',
                         0, '', '', '', '', '', '', '', '', '', 0, 0, 0    
                        )";

                if (!$dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->execute($sql)) {

                    erromsg("
                        Erro ao processar dados de " . $tdb[MAQUINAS_CONJUNTO]['DESC'] . " em <br />
                        Arquivo: " . __FILE__ . " <br />
                        Linha: " . __LINE__ . " <br />
                        Erro: " . $dba[$tdb[MAQUINAS_CONJUNTO]['dba']]->ErrorMsg() . " <br />
                        SQL: " . $sql . " <br />
                    ");
                }
                else{
                    echo "<br />Posi��o {$sufixoEmp}.PSU.PAT cadastrada com sucesso!";
                }
                
            }
        }
        else {
            echo "<br />Localiza��o 2 {$codLoc1}.PSU n�o encontrada e m�quina n�o cadastrada";
        }

        echo "<hr />";
    }
}
