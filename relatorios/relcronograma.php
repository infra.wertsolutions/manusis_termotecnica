<?
/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Nota: Relatorio
 */
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";
$ano=$_GET['ano'];
if ($ano == "") $ano=date("Y");


// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}

if (($_GET['env'] != "") and ($ano != "")){
    $doc.= "
    <div>
    <table width=\"100%\" style=\"border: 1px solid black; font-size: 8px\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
    <thead>
    <tr><td style=\"border-bottom: 1px solid black; padding-left:3px;\" width=\"200\" rowspan=\"4\" align=\"left\" bgcolor=\"#D4D4D4\"><strong>{$ling['rel_desc_obj2']} / <br />{$ling['PLANO_M']}</strong></td></tr>
    <tr><td style=\"border-left: 1px solid black;\" align=\"center\" bgcolor=\"#D4D4D4\" colspan=\"54\"><strong>$ano</strong></td></tr>
    ";

    $doc.= "<tr>\n";

    $c=mktime(0,0,0,12,31,$ano);
    $i=$c;
    $m=date('m',$i);
    $d=date('d',$i);
    $a=date('Y',$i);

    $f = mktime(0, 0, 0, 1, 1, $ano);
    $iii=0;
    $doc_mes = '';
    $mes_passado = 1;
    $colspan = 0;
	$filtro_st = (int) $_GET['filtro_st'];
    while (date('Y', $f) == $ano) {
        $s=date('W',$f);
        $ano_tmp = date('o',$f);

        if ($sf != $s) {
            if (($ano_tmp != $ano) and (($s == 51) or ($s == 52) or ($s == 53))) $st[$iii] = "$s-";
            else $st[$iii]=$s;
                
            $doc_sem.= "<th style=\"border-left: 1px solid black;border-bottom: 1px solid black;\" bgcolor=\"#D4D4D4\" align=\"center\" valign=\"middle\" width=\"14\">$s";
            $iii++;
            $colspan++;
            $em = (int)date('m',$f);
            //$doc_sem.="[$colspan]";
            if ($em > $mes_passado) {
                $doc_mes .= '<th style="border: 1px solid black;border-right:0px;' . ($mes_passado != 1 ? "border-left:0px;" : "") .'" colspan="'.($colspan - 2).'" bgcolor="#D4D4D4">'.$ling_meses[$mes_passado]."</th><th bgcolor=\"#D4D4D4\" style=\"border-bottom: 1px solid black;border-top: 1px solid black;\">|</th>";
                $mes_passado = $em;
                $colspan = 1;
            }
        }
        
        $mm=date('m',$f);
        $dd=date('d',$f);
        $sf=$s;
        $sm=$mm;
        $f=mktime(0,0,0, $mm, $dd + 7,$ano);

        if ($sf != date('W',$f)) $doc_sem .= "</th>\n";
    }

    $doc_mes .= '<th colspan="'.$colspan.'"  style="border: 1px solid black;border-right:0px;border-left:0px;" bgcolor="#D4D4D4">'.$ling_meses[$em]."</th>";
    $doc .= "$doc_mes</tr><tr>$doc_sem";
    $doc .= "</tr>
    </thead>\n";
    
    //  var_dump($st);
    $filtro_emp=(int)$_GET['filtro_emp'];
    $filtro_area=(int)$_GET['filtro_area'];
    $filtro_setor=(int)$_GET['filtro_setor'];
    $filtro_maq=(int)$_GET['filtro_maq'];
    $filtro_parada=(int)$_GET['filtro_parada'];
    
    if ($filtro_area == 0) {
        $filtro_tipo=0;
        $valor=0;
    }
    if ($filtro_emp != 0) {
        $filtro_tipo=5;
        $valor=$filtro_emp;
        $filtro_nome= $tdb[EMPRESAS]['DESC'] . ": " . VoltaValor(EMPRESAS,"COD","MID",$filtro_emp,0) . "-" . VoltaValor(EMPRESAS,"NOME","MID",$filtro_emp,0)."<br />";
    }
    if ($filtro_area != 0) {
        $filtro_tipo=1;
        $valor=$filtro_area;
        $filtro_nome.=" {$ling['rel_desc_loc1_min']}: ".VoltaValor(AREAS,"DESCRICAO","MID",$filtro_area,0)."<br />";
    }
    if ($filtro_setor != 0) {
        $filtro_tipo=2;
        $valor=$filtro_setor;
        $filtro_nome.=" {$ling['rel_desc_loc2_min']}: ".VoltaValor(SETORES,"DESCRICAO","MID",$filtro_setor,0)."<br />";
    }
    if ($filtro_maq != 0) {
        $filtro_tipo=3;
        $valor=$filtro_maq;
        $filtro_nome.=" {$ling['rel_desc_obj2_min']}: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$filtro_maq,0)."<br />";
    }
    if ($maq_fam != 0) {
        $filtro_tipo=4;
        $valor=$maq_fam;
        $filtro_nome.=" {$ling['rel_desc_fam_obj']}: ".VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$maq_fam,0)."<br />";
    }

    if ($filtro_tipo == 5) {
        // Filtro por Empresa
        $fil_emp = VoltaFiltroEmpresa(AREAS, 2);
        $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

        $sql="SELECT S.MID FROM ".AREAS." A, ".SETORES." S WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = '".$valor."' $fil_emp ORDER BY S.COD";
        $tmp=$dba[0] -> Execute($sql);
        if (!$tmp) erromsg($dba[0] -> ErrorMsg()."<br><br>$sql");
        $conta=0;
        $fil_set = '';
        while(!$tmp -> EOF) {
            $ca=$tmp -> fields;
            AddStr($fil_set,' OR ',"A.MID_SETOR = '".$ca['MID']."'");
            $tmp -> MoveNext();
        }
        if ($fil_set) $filtro .= " ($fil_set) AND";
    }

    if ($filtro_tipo == 1) {
        // Filtro por Empresa
        $fil_emp = VoltaFiltroEmpresa(SETORES, 2);
        $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

        $sql="SELECT MID FROM ".SETORES." WHERE MID_AREA = '".$_GET['filtro_area']."' $fil_emp ORDER BY COD";		
        $tmp=$dba[0] -> Execute($sql);
        if (!$tmp) erromsg($dba[0] -> ErrorMsg()."<br><br>$sql");
        $conta=0;
        $fil_set = '';
        while(!$tmp -> EOF) {
            $ca=$tmp -> fields;
            AddStr($fil_set,' OR ',"A.MID_SETOR = '".$ca['MID']."'");
            $tmp -> MoveNext();
        }
        if ($fil_set) $filtro .= " ($fil_set) AND";
	
    }

    if ($maq_fam != 0) {
        // Filtro por Empresa
        $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);
        $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

        $sql="SELECT MID FROM ".MAQUINAS." WHERE FAMILIA = '$maq_fam' $fil_emp";
        if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
            $err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
            erromsg("SQL ERROR .<br>$err<br><br>$sql");
            exit;
        }
        $mfil= "(";
        $ifmaq=0;
        while (!$resultado->EOF) {
            $campo=$resultado -> fields;
            if ($ifmaq == 0) $mfil .= "A.MID = ".$campo['MID'];
            else $mfil .= " OR A.MID = ".$campo['MID'];
            $ifmaq++;
            $resultado->MoveNext();
        }
        if ($ifmaq == 0)$mfil.="A.MID = 'A'";
        $mfil.=") AND";
    }
	
    if ($filtro_tipo == 2) $filtro=" A.MID_SETOR = '$valor' AND";
    if ($filtro_tipo == 3) $filtro=" A.MID = '$valor' AND";
    if ($filtro_tipo == 0) $filtro="";
    if ($maq_fam != 0) $filtro=$mfil;
	
	
	if($filtro_st != 0)
	{
	    if($filtro_st == 1)
		{
		    $filtro_nome .= " {$ling['situacoes']}: {$ling['em_andamento']}";
			$filtroProg  .= " B.STATUS != 2 AND B.MID IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= B.MID AND DATE_FORMAT(o.DATA_PROG, '%Y') = $ano AND o.STATUS = 1) AND"; 
			$filtroOrdem      .= " B.STATUS = 1 AND ";
		}
		elseif($filtro_st == 2)
		{
		   	$filtro_nome .= " {$ling['situacoes']}: {$ling['concluida']}";
			$filtroProg .= " ((B.STATUS = 2) OR (B.STATUS != 2 AND B.MID NOT IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= B.MID AND DATE_FORMAT(o.DATA_PROG, '%Y') = $ano AND o.STATUS = 1 ))) AND"; 
			$filtroProg .= "  B.MID IN (SELECT o.MID_PROGRAMACAO FROM ".ORDEM_PLANEJADO." o WHERE o.MID_PROGRAMACAO= B.MID AND DATE_FORMAT(o.DATA_PROG, '%Y') = $ano AND o.STATUS = 2 ) AND"; 
			$filtroOrdem .= " B.STATUS != 1 AND ";
		}
		elseif($filtro_st == 3)
		{
	         $filtro_nome  .= " {$ling['situacoes']}: {$ling['cancelada']}";
			 $filtroProg   .= "  B.STATUS = $filtro_st AND";
			 $filtroOrdem  .= " B.STATUS = 3 AND ";
		}		
	}
	

    $sql = "SELECT B.MID,B.MID_PROGRAMACAO,B.STATUS,B.DATA_PROG FROM ".MAQUINAS." AS A, ".ORDEM_PLANEJADO." AS B WHERE B.MID_MAQUINA = A.MID AND  $filtro $filtroOrdem DATE_FORMAT(B.DATA_PROG,'%Y') = '$ano' AND B.TIPO = 1 ORDER BY A.COD,B.STATUS ASC ";
    $re=$dba[$tdb[MAQUINAS]['dba']] -> Execute($sql);
    if (!$re) echo $dba[0] -> ErrorMsg();
    while (!$re->EOF) {
        $ca=$re -> fields;
        
        $mk = VoltaTime("00:00:00", NossaData($ca['DATA_PROG']));
        $s=date("W",$mk);
        $ano_tmp = date("o",$mk);
        
        if (($ano_tmp != $ano) and (($s == 51) or ($s == 52) or ($s == 53))) $s = "$s-";
        
        $status=$ca['STATUS'];
        
        $prog=$ca['MID_PROGRAMACAO'];
            
        $pen = (int)VoltaValor(PENDENCIAS,"MID","MID_ORDEM",$ca['MID'],0);

        if ($pen != 0) $tmp_pen[$prog][$s]=1;
        
        $tmp_ord[$prog][$s] = $ca['MID'];
        
        // VERIFICA SE A OS TEM PARADA
        $tmp_par[$prog][$s] += VoltaTempoParadaPrev($ca['MID']);
        
        if (($status == 1) or ($status == 0)) $tmp_var[$prog][$s]=1;
        elseif ($status == 2) $tmp_var[$prog][$s]=2;
        elseif ($status == 3) $tmp_var[$prog][$s]=3;
        
        
        $re -> MoveNext();
    }

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(PLANO_PADRAO, 2);
    $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";
	
	
    
	$sql="SELECT A.DESCRICAO,A.COD,A.MID,B.MID AS PROG,C.DESCRICAO AS PLANO FROM ".MAQUINAS." AS A, ".PROGRAMACAO." AS B, ".PLANO_PADRAO." AS C WHERE B.MID_MAQUINA = A.MID AND B.MID_PLANO = C.MID AND $filtro $filtroProg (DATE_FORMAT(B.DATA_INICIAL, '%Y') <= $ano AND  DATE_FORMAT(B.DATA_FINAL, '%Y') >= $ano) AND B.TIPO = 1  $fil_emp ORDER BY A.COD, B.MID ASC";
	$sql= str_replace('MID_EMPRESA', 'A.MID_EMPRESA', $sql);
    
    $re=$dba[$tdb[PROGRAMACAO]['dba']] -> Execute($sql);

    if (!$re) echo $dba[0] -> ErrorMsg();
    $conta=0;
    
    while (!$re->EOF) {
        $ca=$re -> fields;
        
        $doc.= "<tr>
        <td width=\"210\" style=\"font-size:8px; border-bottom:1px solid black; padding:3px;\" align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" height=\"30\">
        ".htmlentities($ca['COD']."-".$ca['DESCRICAO'])." /<br />".htmlentities($ca['PLANO'])."
        </td>";
        
        $doc.= "\n";        
        
        unset($s);
    
        for ($i=0; $st[$i] != ""; $i++) {
            $s=$st[$i];
            //if (($i == 0) and (($s == "51-") or ($s == "52-") or ($s == "53-"))) $s = "$s-";
            if ($tmp_pen[$ca['PROG']][$s] == 1){
                $doc_exc = '_exc';
            }
            else $doc_exc = '';
            
            if ($filtro_parada == 1) {
                if ($tmp_par[$ca['PROG']][$s] > 0) {
                    $img = "" . str_replace('.', ',', $tmp_par[$ca['PROG']][$s]) . "h";
                }
                else {
                    $img = "&nbsp;";
                }
            }
            elseif ($tmp_var[$ca['PROG']][$s] == 1) {
                $img = "<a href=\"javascript:janela('../detalha_ord.php?busca=".$tmp_ord[$ca['PROG']][$s]."', 'parm', 500,400)\"><img src=\"../imagens/icones/graydot$doc_exc.jpg\" width=\"10\" height=\"10\" border=\"0\" /></a>";
            }
            elseif ($tmp_var[$ca['PROG']][$s] == 2) {
                $img = "<a href=\"javascript:janela('../detalha_ord.php?busca=".$tmp_ord[$ca['PROG']][$s]."', 'parm', 500,400)\"><img src=\"../imagens/icones/blackdot$doc_exc.jpg\" width=\"10\" height=\"10\" border=\"0\" /></a>";
            }
            else {
                $img = "&nbsp;";
            }
                
            $doc.= "<td align=\"center\" valign=\"middle\" style=\"border-left: 1px solid black;border-bottom: 1px solid black;\" width=\"14\">$img</td>\n";

        }
        
        $doc.= "</tr>\n";
        
        $re -> MoveNext();
        
        $conta++;
    }
    
    $doc.= "</table></div>
    <table border=0 cellpadding=1 align=\"left\">
    <tr><td style=\"font-size:10px\"><strong>{$ling['legenda']}:</strong></td></tr>
    <tr><td><img src=\"../imagens/icones/blackball.gif\"     width=32 height=32 border=0 /></td><td valign=\"middle\" align=\"left\" bgcolor=\"#FFFFFF\" style=\"font-size:10px\">&nbsp;&nbsp; {$ling['rel_desc_serv_realizado']}</td></tr>
    <tr><td><img src=\"../imagens/icones/blackball_exc.gif\" width=32 height=32 border=0 /></td><td valign=\"middle\" align=\"left\" bgcolor=\"#FFFFFF\" style=\"font-size:10px\">&nbsp;&nbsp; {$ling['rel_desc_serv_realizado_pend']}</td></tr>
    <tr><td><img src=\"../imagens/icones/grayball.gif\"      width=32 height=32 border=0 /></td><td valign=\"middle\" align=\"left\" bgcolor=\"#FFFFFF\" style=\"font-size:10px\">&nbsp;&nbsp; {$ling['rel_desc_serv_prog']}</td></tr>
    <tr><td><img src=\"../imagens/icones/grayball_exc.gif\"  width=32 height=32 border=0 /></td><td valign=\"middle\" align=\"left\" bgcolor=\"#FFFFFF\" style=\"font-size:10px\">&nbsp;&nbsp; {$ling['rel_desc_serv_prog_pend']}</td></tr>
    </table> <br clear=\"all\">";
    
    
    if ($filtro_nome == "") $filtro_nome=$ling['nenhum'];
    relatorio_padrao($ling['cronograma_preven_anual'],$filtro_nome." {$ling['ano']}: $ano",$conta,$doc,1);
}
else {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
    <script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>
    </head>
    <body><div id=\"central_relatorio\">
    <div id=\"cab_relatorio\">
    <h1>{$ling['cronograma_preven_anual']}</h1>
    </div>
    <div id=\"corpo_relatorio\">
    <form action=\"relcronograma.php\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
    
    <fieldset style=\"font-size:11px\"><legend>".$ling['filtros']."</legend>
    
    <div id=\"filtro_relatorio\">";

    // Filtros padr�o
    FiltrosRelatorio(1, 1, 1, 1);


    echo "</div>
    <label for\"ano\">{$ling['ano']}</label>
    <select class=\"campo_select\" name=\"ano\" id=\"ano\">";
    $ffano=$ano + 6;
    for ($i=$ano - 4; $i < $ffano; $i++) {
        if ($i == $ano) echo "<option value=\"$ano\" selected=\"selected\">$ano</option>";
        else echo "<option value=\"$i\" >$i</option>\n";
    }
    echo "</select> 
    <br clear=\"all\" />";
	
	 echo "<label for=\"filtro_st\">{$ling['situacoes']}:</label> 
    <select name=\"filtro_st\" class=\"campo_select\">
    <option value=\"0\">{$ling['todos']}</option>
    <option value=\"1\" " . ($filtro_st == 1 ? "selected=\"selected\"" : "") . ">{$ling['em_andamento']}</option>
    <option value=\"2\" " . ($filtro_st == 2 ? "selected=\"selected\"" : "") . ">{$ling['concluida']}</option>
    <option value=\"3\" " . ($filtro_st == 3 ? "selected=\"selected\"" : "") . ">{$ling['cancelada']}</option>
    </select>\n
    <br clear=\"all\" />
    
    <label for\"filtro_parada\">{$ling['relcronograma_parada']}</label>
    <select class=\"campo_select\" name=\"filtro_parada\" id=\"filtro_parada\">
    <option value=\"1\">{$ling['sim']}</option>
    <option value=\"2\" selected=\"selected\">{$ling['nao']}</option>
    </select>
    </fieldset>
    
    <input type=\"submit\" value=\"{$ling['rel_gerar_cronograma']}\" class=\"botao\" name=\"env\" />
    
    </form>
    
    </div>";
}
?>
