<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
* Nota: Relat�rio localizacao por material
*/
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
#############################
$ajax = $_GET['ajax'];
$setor = $_GET['setor'];
$area = $_GET['area'];
$emp = $_GET['emp'];
$fam = $_GET['fam'];
$obj = $_GET['obj'];
$gmat = $_GET['mat'];
$gmatfam = $_GET['matfam'];


if ($ajax or $_GET['env']) {
    $condmaq = '';
    $condset = '';
    if ($obj) $condmaq = "MID = '$obj'";
    else {
        if ($fam) {
            if ($condmaq) $condmaq .= ' AND ';
            $condmaq .= "FAMILIA = '$fam'";
        }
        if ($setor) {
            $condmaq .= "MID_SETOR = '$setor'";
        }
        elseif ($area) {
            $condset = "MID_AREA = '$area'";
            // gera sequencia de or mid_setor=tal etc
            $setfil = '';
            // Filtro por Empresa
            $fil_emp = VoltaFiltroEmpresa(SETORES, 2);
            $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";

            $sql="SELECT MID FROM ".SETORES." WHERE MID_AREA = '$area' $fil_emp";
            if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
                $err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
                erromsg("SQL ERROR .<br>$err<br><br>$sql");
                exit;
            }
            while (!$resultado->EOF) {
                $campo=$resultado -> fields;
                if ($setfil) $setfil .= ' OR ';
                $setfil .= "MID_SETOR = ".$campo['MID'];
                $resultado->MoveNext();
            }
            if ($setfil) $setfil = "($setfil)";
            else $setfil = 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

            if ($condmaq) $condmaq .= ' AND ';
            $condmaq .= $setfil;
            
        }
        elseif ($emp) {
            $condarea = "MID_EMPRESA = '$emp'";
            // gera sequencia de or mid_setor=tal etc
            $setfil = '';
            
            // Filtro por Empresa
            $fil_emp = VoltaFiltroEmpresa(AREAS, 2);
            $fil_emp = ($fil_emp != "")? "AND " . $fil_emp : "";
            
            $sql="SELECT S.MID FROM ".SETORES." S, " . AREAS . " A WHERE S.MID_AREA = A.MID AND A.MID_EMPRESA = '$emp' $fil_emp";
            if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
                $err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
                erromsg("SQL ERROR .<br>$err<br><br>$sql");
                exit;
            }
            while (!$resultado->EOF) {
                $campo=$resultado -> fields;
                if ($setfil) $setfil .= ' OR ';
                $setfil .= "MID_SETOR = ".$campo['MID'];
                $resultado->MoveNext();
            }
            if ($setfil) $setfil = "($setfil)";
            else $setfil = 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

            if ($condmaq) $condmaq .= ' AND ';
            $condmaq .= $setfil;
        }
    }
}


if (!$_GET['env']) { // n�o exibindo relatorio
    $condmat = '';


    if ($ajax == '1') {

       // Filtros
        echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['fam'], "fam", "fam", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&emp=$emp&area=$area&setor=$setor&obj=$obj&fam=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"emp\">".$tdb[EMPRESAS]['DESC'].":</label>";
        FormSelectD("COD", "NOME", EMPRESAS, $_GET['emp'], "emp", "emp", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=$obj&emp=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", AREAS, $_GET['area'], "area", "area", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&setor=$setor&obj=$obj&area=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", SETORES, $_GET['setor'], "setor", "setor", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&area=$area&obj=$obj&setor=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&emp=$emp&area=$area&setor=$setor&obj=' + this.value)");
    }
    elseif ($ajax == '2') {
        if ($matfam) $condmat .= "FAMILIA = '$matfam'";

        $primeirocampo=$ling['todos2'];
        echo "<label class=\"campo_label\" for=\"matfam\">".$tdb[MATERIAIS_FAMILIA]['DESC'].":</label><br />";
        FormSelectD("COD",'DESCRICAO',MATERIAIS_FAMILIA,$_GET['matfam'],"matfam","matfam","MID",0,"","","","","",$ling['todos2']);
        echo "<label class=\"campo_label\" for=\"mat\">".$tdb[MATERIAIS]['DESC'].":</label><br />";
        FormSelectD("COD",'DESCRICAO',MATERIAIS,$_GET['mat'],"mat","mat","MID",0,"","","","","",$ling['todos2']);
    }
    else {
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
        <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
        <head>
         <meta http-equiv=\"pragma\" content=\"no-cache\" />
        <title>{$ling['manusis']}</title>
        <link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
        <script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
        if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../lib/movediv.js\"> </script>\n";
        echo "</head>
        <body><div id=\"central_relatorio\">
        <div id=\"cab_relatorio\">
        <h1>{$ling['rel_localizacao_materiais']}</h1>
        </div>
        <div id=\"corpo_relatorio\">
        <form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
        <fieldset>
        <legend>".$ling['filtros']."</legend>
        <div id=\"fil\">";

        //     Filtros
        echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS_FAMILIA, $_GET['fam'], "fam", "fam", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&area=$area&setor=$setor&obj=$obj&fam=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"emp\">".$tdb[EMPRESAS]['DESC'].":</label>";
        FormSelectD("COD", "NOME", EMPRESAS, $_GET['emp'], "emp", "emp", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=$obj&emp=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", AREAS, $_GET['area'], "area", "area", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&setor=$setor&obj=$obj&area=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", SETORES, $_GET['setor'], "setor", "setor", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&obj=$obj&setor=' + this.value)");
        echo "<br clear=\"all\" />";
        
        echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label>";
        FormSelectD("COD", "DESCRICAO", MAQUINAS, $_GET['obj'], "obj", "obj", "MID", "", "", "atualiza_area2('fil', '$ajaxdestino?ajax=1&fam=$fam&area=$area&setor=$setor&obj=' + this.value)");

        

        echo "</div><br clear=\"all\" /><font size=1>{$ling['rel_desc_loc_mat_texto']}</font>
        		</fieldset>
        <input type=\"hidden\" name=\"env\" value=\"1\" />
        <input type=\"submit\" value=\"{$ling['relatorio_html']}\" class=\"botao\">
        <input type=\"submit\" name=\"word\" value=\"{$ling['relatorio_doc']}\" class=\"botao\">
        </form><br />
        </div>
        </div>
        </body>
        </html>";
    }
}
else {
  $tempoexec_inicial = utime();
   $tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";
    $tdstyle2 = "style=\"border-bottom: 1px solid black; \"";

    unset($mats);
    $ipp=0;

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);

    // Filtros
    if ($condmaq) {
        $condmaq = "WHERE $condmaq";
        if ($fil_emp != "") {
            $condmaq .= " AND " . $fil_emp;
        }
    }
    elseif ($fil_emp != "") {
        $condmaq .= " WHERE " . $fil_emp;
    }

    $sql="SELECT * FROM ".MAQUINAS." $condmaq ORDER BY DESCRICAO ASC";
    if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    while (!$resultado->EOF) {
        $campo=$resultado -> fields;
        $maqcod = $campo['COD'];
        $maqnome = $maqcod.'-'.$campo['DESCRICAO'];
        $maqset = VoltaValor(SETORES,"COD","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba']) . "-" . VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba']);
        $maqmidarea = VoltaValor(SETORES,"MID_AREA","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba']);
        $maqarea = VoltaValor(AREAS,"COD","MID",$maqmidarea,$tdb[AREAS]['dba']) . "-" . VoltaValor(AREAS,"DESCRICAO","MID",$maqmidarea,$tdb[AREAS]['dba']);
        $maqfam = VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$campo['FAMILIA'],$tdb[MAQUINAS_FAMILIA]['dba']);

        $rpos=$dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]-> Execute("SELECT * FROM ".MAQUINAS_CONJUNTO_MATERIAL." WHERE MID_MAQUINA = '".$campo['MID']."' ORDER BY MID_CONJUNTO ASC");
        while (!$rpos->EOF) {
            //$postag = VoltaValor(MAQUINAS_CONJUNTO,"TAG","MID",$rpos ->fields['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])
            $matmid = $rpos ->fields['MID_MATERIAL'];
            $matqnt = $rpos ->fields['QUANTIDADE'];
            $matpos = $rpos ->fields['MID_CONJUNTO'];
            $matpos = VoltaValor(MAQUINAS_CONJUNTO,'DESCRICAO','MID',$matpos,$tdb[MAQUINAS_CONJUNTO]['dba']);
            $matfam = VoltaValor(MATERIAIS,"FAMILIA","MID",$matmid,$tdb[MATERIAIS]['dba']);

            $estamaq['qnt'] = $matqnt;
            $estamaq['maq'] = $maqnome;
            $estamaq['setor'] = $maqset;
            $estamaq['area'] = $maqarea;
            $estamaq['fam'] = $maqfam;
            $estamaq['pos'] = $matpos;

            if ((!$gmat or ($matmid == $gmat)) and (!$gmatfam or ($matfam == $gmatfam))) {
                $mats[$matmid]['mid'] = $matmid;
                $mats[$matmid]['maqs'][] = $estamaq;
                $ipp++;
            } // else, not in the filters

            $rpos->MoveNext();
        }

        $rpos=$dba[$tdb[EQUIPAMENTOS]['dba']]-> Execute("SELECT * FROM ".EQUIPAMENTOS." WHERE MID_MAQUINA = '".$campo['MID']."' ORDER BY DESCRICAO ASC");
        while (!$rpos->EOF) {
            $listapeca=ListaPecaEquipamento($rpos ->fields['MID']);
            if ($listapeca != 0) {
                for ($ip=0; $listapeca[$ip]['nome'] != ""; $ip++) {
                    $peca=$listapeca[$ip]['nome'];
                    $qto=$listapeca[$ip]['qto'];
                    $pmid=$listapeca[$ip]['mid'];
                    $matmid = $rpos ->fields['MID_MATERIAL'];
                    $matpos = $rpos ->fields['MID_CONJUNTO'];
                    $matpos = VoltaValor(MAQUINAS_CONJUNTO,'DESCRICAO','MID',$matpos,$tdb[MAQUINAS_CONJUNTO]['dba']);
                    
                    $matmid = $pmid;

                    $estamaq['qnt'] = $qnt;
                    $estamaq['maq'] = $maqnome;
                    $estamaq['setor'] = $maqset;
                    $estamaq['area'] = $maqarea;
                    $estamaq['fam'] = $maqfam;
                    $estamaq['pos'] = $matpos;

                    if ((!$gmat or ($matmid == $gmat)) and (!$gmatfam or ($matfam == $gmatfam))) {
                        $mats[$matmid]['mid'] = $matmid;
                        $mats[$matmid]['maqs'][] = $estamaq;
                        $ipp++;
                    }
                }
            }
            $rpos->MoveNext();
        }

        $resultado->MoveNext();
    }

    //ksort($mats);
    $txt = '';
    $txt = "<table id=\"dados_processados\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">
    <thead>
    <tr>
    <th width=\"10%\">".$tdb[AREAS]['DESC']."</th>
    <th width=\"10%\">".$tdb[SETORES]['DESC']."</th>
    <th width=\"40%\">".$tdb[MAQUINAS]['DESC']."</th>
    <th width=\"20%\">".$tdb[MAQUINAS_CONJUNTO]['DESC']."</th>
    <th width=\"18%\">".$tdb[MAQUINAS_FAMILIA]['DESC']."</th>
    <th width=\"2%\">Qtd.</th>
    </tr></thead>";
    $txt2 = $txt;

    if ($mats) {
        foreach($mats as $estemat){
            $matmid = $estemat['mid'];
            $matdesc = VoltaValor(MATERIAIS,"DESCRICAO","MID",$matmid,$tdb[MATERIAIS]['dba']);
            $matfammid = VoltaValor(MATERIAIS,"FAMILIA","MID",$matmid,$tdb[MATERIAIS]['dba']);
            $matfam = VoltaValor(MATERIAIS_FAMILIA,"DESCRICAO","MID",$matfammid,$tdb[MATERIAIS_FAMILIA]['dba']);
            if ($matfam) $matfam = " {$ling['rel_desc_familia']}: $matfam";

            $maqs = $estemat['maqs'];
            $estetxt = '';
            foreach($maqs as $estamaq){
                $maqqnt = $estamaq['qnt'];
                $maqnome = $estamaq['maq'];
                $maqarea = $estamaq['area'];
                $maqsetor = $estamaq['setor'];
                $maqfam = $estamaq['fam'];
                $matpos = $estamaq['pos'];
                $estetxt .= "<tr>
			<td $tdstyle>$maqarea</td>
			<td $tdstyle>$maqsetor</td>
			<td $tdstyle>$maqnome</td>
			<td $tdstyle>$matpos</td>
			<td $tdstyle>$maqfam</td>
			<td $tdstyle>$maqqnt</td>
			</tr>";
            }
            if ($estetxt) {
                $col = ($matfam != "")? 3 : 6;
                $txt .= "<div align=\"left\" ><strong><font size=\"3\">$matdesc</strong></font></div>";
                
                if ($matfam != ""){
                    $txt .= "<td colspan=\"3\" $tdstyle2><strong>$matfam</strong></td>";
                }
                
                $txt .= "</tr>
                $estetxt</table>".$txt2;
            }
        }

        $txt .= "";
    }
    else {
        $txt = "<font size=\"2\"><strong>{$ling['rel_desc_sem_registros']}</strong></font>";
    }

    $tempoexec_final = utime();
    $tempoexec = round($tempoexec_final - $tempoexec_inicial,4);
    
    $filtro = '';
    if ($emp) {
        $filtro .= $tdb[EMPRESAS]['DESC'].': '.VoltaValor(EMPRESAS,"NOME","MID",$emp,$tdb[EMPRESAS]['dba']);
    }
    if ($area) {
        if ($filtro) $filtro .= '; ';
        $filtro .= $tdb[AREAS]['DESC'].': '.VoltaValor(AREAS,"DESCRICAO","MID",$area,$tdb[AREAS]['dba']);
    }
    if ($setor) {
        if ($filtro) $filtro .= '; ';
        $filtro .= $tdb[SETORES]['DESC'].': '.VoltaValor(SETORES,"DESCRICAO","MID",$setor,$tdb[SETORES]['dba']);
    }
    if ($fam) {
        if ($filtro) $filtro .= '; ';
        $filtro .= $tdb[MAQUINAS_FAMILIA]['DESC'].': '.VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$fam,$tdb[MAQUINAS_FAMILIA]['dba']);
    }
    if ($obj) {
        if ($filtro) $filtro .= '; ';
        $filtro .= $tdb[MAQUINAS]['DESC'].': '.VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj,$tdb[MAQUINAS]['dba']);
    }

    if ($_GET['word']) exportar_word($ling['rel_localizacao_materiais'],$filtro,$ipp,$txt,1);
    else relatorio_padrao($ling['rel_localizacao_materiais'],$filtro,$ipp,$txt,1,$tempoexec);

} // fim OK
?>
