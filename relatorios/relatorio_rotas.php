<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
* Nota: Relat�rio Planos
*/
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
function FiltroCheck($campo,$label,$checked=0) {
    if ($checked) $check = "checked=\"checked\"";
    else $check='';
    echo "<input class=\"campo_check\" type=\"checkbox\" $check name=\"filtro[$campo]\" id=\"filtro_$campo\" value=\"$campo\">
    <label for=\"filtro_$campo\" class=\"campo_label\">$label</label>\n";
}
#############################

$alvo = $_GET['alvo'];
$where = '1';





if (!$_GET['env']) { // n�o exibindo relatorio

    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
    <script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n
    </head>
    <body>
    <div id=\"central_relatorio\">
    <div id=\"cab_relatorio\">
    <h1 />{$ling['rel_planos_rota']}
    </div><div id=\"corpo_relatorio\">
    <form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
    <fieldset>
    <legend>{$ling['rel_desc_col_mostradas']}:</legend>";

    FiltroCheck('NUMERO',         $tdb[LINK_ROTAS]['NUMERO'], 1);
    FiltroCheck('PARTE',          $tdb[LINK_ROTAS]['MID_PONTO'].' / '.$tdb[LINK_ROTAS]['MID_CONJUNTO'], 1);
    FiltroCheck('TAREFA',         $tdb[LINK_ROTAS]['TAREFA'], 1);
    FiltroCheck('ESPECIALIDADE',  $tdb[LINK_ROTAS]['ESPECIALIDADE'], 1);
    FiltroCheck('QUANTIDADE_MO',  $tdb[LINK_ROTAS]['QUANTIDADE_MO'], 1);
    FiltroCheck('TEMPO_PREVISTO', $tdb[LINK_ROTAS]['TEMPO_PREVISTO'], 1);
    echo "<br clear=\"all\" />";
    FiltroCheck('MID_MATERIAL',  $tdb[LINK_ROTAS]['MID_MATERIAL'], 1);
    FiltroCheck('QUANTIDADE',    $tdb[LINK_ROTAS]['QUANTIDADE'], 1);
    FiltroCheck('PERIODICIDADE', $tdb[LINK_ROTAS]['PERIODICIDADE'],1);
    FiltroCheck('FREQUENCIA',    $tdb[LINK_ROTAS]['FREQUENCIA'],1);
    echo "</fieldset>

    <fieldset>
    <legend>".$ling['filtros']."</legend>
    
    <label class=\"campo_label\" for=\"filtro_plano\">{$ling['descricao']}:</label>
    <input type=\"text\" size=\"50\" class=\"campo_text\" name=\"filtro_plano\" id=\"filtro_plano\">
    <br clear=\"all\" />
    
    <div id=\"filtro_relatorio\">";
    
    FiltrosRelatorio(1, 1, 1, 1, 1);
    
    
    echo "</div>";
    echo "</fieldset><br />
    <input type=\"hidden\" name=\"env\" value=\"1\" />

    <input type=\"submit\" value=\"{$ling['relatorio_html']}\" class=\"botao\">
    <input type=\"submit\" name=\"word\" value=\"{$ling['relatorio_doc']}\" class=\"botao\">
    </form><br />
    </div>
    </div>
    </body>
    </html>";

}
else { // relatorio
    // FILTROS
    $filtro_plano   = LimpaTexto($_GET['filtro_plano']);
    $filtro_emp     = (int)$_GET['filtro_emp'];
    $filtro_area    = (int)$_GET['filtro_area'];
    $filtro_setor   = (int)$_GET['filtro_setor'];
    $filtro_maq     = (int)$_GET['filtro_maq'];
    $filtro_maq_fam = (int)$_GET['filtro_maq_fam'];
    
    // CAMPOS SELECIONADOS
    $filtro = $_REQUEST['filtro'];
    
    // Buscando as maquinas para filtrar
    $filtro_sql = "";
    
    // Filtro para mostrar
    $filtro_desc = "";
    
    if ($filtro_maq != 0) {
        $filtro_sql  .= " AND L.MID_MAQUINA = $filtro_maq";
        $filtro_desc .= ($filtro_desc == "")? "" : "; ";
        $filtro_desc .= $tdb[MAQUINAS]['DESC'] . ": " . VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $filtro_maq);
    }
    elseif ($filtro_setor != 0) {
        $filtro_sql .= " AND L.MID_MAQUINA IN (SELECT MID FROM " . MAQUINAS . " WHERE MID_SETOR = $filtro_setor)";
        $filtro_desc .= ($filtro_desc == "")? "" : "; ";
        $filtro_desc .= $tdb[SETORES]['DESC'] . ": " . VoltaValor(SETORES, 'DESCRICAO', 'MID', $filtro_setor);
    }
    elseif ($filtro_area != 0) {
        $filtro_sql .= " AND L.MID_MAQUINA IN (SELECT M.MID FROM " . MAQUINAS . " M, " . SETORES . " S WHERE M.MID_SETOR = S.MID AND S.MID_AREA = $filtro_area)";
        $filtro_desc .= ($filtro_desc == "")? "" : "; ";
        $filtro_desc .= $tdb[AREAS]['DESC'] . ": " . VoltaValor(AREAS, 'DESCRICAO', 'MID', $filtro_area);
    }
    elseif ($filtro_emp != 0) {
        $filtro_sql .= " AND L.MID_MAQUINA IN (SELECT MID FROM " . MAQUINAS . " WHERE MID_EMPRESA = $filtro_emp)";
        $filtro_desc .= ($filtro_desc == "")? "" : "; ";
        $filtro_desc .= $tdb[EMPRESAS]['DESC'] . ": " . VoltaValor(EMPRESAS, 'NOME', 'MID', $filtro_emp);
    }
    
    if ($filtro_maq_fam != 0) {
        $filtro_sql .= " AND L.MID_MAQUINA IN (SELECT MID FROM " . MAQUINAS . " WHERE FAMILIA = $filtro_maq_fam)";
        $filtro_desc .= ($filtro_desc == "")? "" : "; ";
        $filtro_desc .= $tdb[MAQUINAS_FAMILIA]['DESC'] . ": " . VoltaValor(MAQUINAS_FAMILIA, 'DESCRICAO', 'MID', $filtro_maq_fam);
    }
    
    if ($filtro_plano != "") {
        $filtro_sql .= " AND L.MID_PLANO IN (SELECT MID FROM " . PLANO_ROTAS . " WHERE DESCRICAO LIKE '%" . addslashes($filtro_plano) . "%')";
        $filtro_desc .= ($filtro_desc == "")? "" : "; ";
        $filtro_desc .= $tdb[PLANO_ROTAS]['DESC'] . ": " . $filtro_plano;
    }
    
    // Iniciando os valores
    $rotas = array();
    $links = array();

    $sql = "SELECT L.*, R.POSICAO FROM ".LINK_ROTAS." AS L, ".ROTEIRO_ROTAS." AS R WHERE L.MID_MAQUINA = R.MID_MAQUINA and L.MID_PLANO = R.MID_PLANO $filtro_sql ORDER BY L.MID_PLANO, R.POSICAO, L.NUMERO ASC";
    //die($sql);
    if(! $resultadoplano=$dba[$tdb[LINK_ROTAS]['dba']] -> Execute($sql)) {
         erromsg("Arquivo: " . __FILE__ . " <br />Linha: " . __LINE__ . " <br />" . $dba[$tdb[LINK_ROTAS]['dba']] -> ErrorMsg() . "<br />" . $sql);
    }
    
    while (!$resultadoplano->EOF) {
        $plano = $resultadoplano -> fields;

        $midrota = $plano['MID_PLANO'];
        $rotas[$midrota] = $midrota;
        $links[$midrota][] = $plano;

        $resultadoplano->MoveNext();
    }

    
    
    $tempoexec_inicial = utime();
    $tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";
    
    $ii = 0;
    $iim = 0;
    $tmpi = 0;
    foreach($rotas as $estarota){
        $nomerota = VoltaValor(PLANO_ROTAS,'DESCRICAO','MID',$estarota,0);
        $send .= "<table border=0 cellspacing=0 width=\"100%\"
         style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\">
        <thead><tr><th colspan=11 $tdstyle align=\"left\"><b>".htmlentities($nomerota)."</b></th></tr>\n
            <tr>
            <th $tdstyle>".$tdb[LINK_ROTAS]['TIPO']."</th>\n";
            
        if ($filtro['NUMERO'])         $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['NUMERO']."</th>\n";
        if ($filtro['PARTE'])          $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['MID_PONTO'].' / '.$tdb[LINK_ROTAS]['MID_CONJUNTO']."</th>\n";
        if ($filtro['TAREFA'])         $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['TAREFA']."</th>\n";
        if ($filtro['ESPECIALIDADE'])  $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['ESPECIALIDADE']."</th>\n";
        if ($filtro['QUANTIDADE_MO'])  $send .= "<th $tdstyle>".$ling['qtd_qtd']."</th>\n";
        if ($filtro['TEMPO_PREVISTO']) $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['TEMPO_PREVISTO']."</th>\n";
        if ($filtro['MID_MATERIAL'])   $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['MID_MATERIAL']."</th>\n";
        if ($filtro['QUANTIDADE'])     $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['QUANTIDADE']."</th>\n";
        if ($filtro['FREQUENCIA'])     $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['FREQUENCIA']."</th>\n";
        if ($filtro['PERIODICIDADE'])  $send .= "<th $tdstyle>".$tdb[LINK_ROTAS]['PERIODICIDADE']."</th>\n";
        
        
        $send .= "</tr></thead>\n";

        $tmptd = '0:0:0';
        $i = 0;
        $estelinks = $links[$estarota];
        $ultimo_obj = 0;
        if ($estelinks) foreach($estelinks as $ativ){
            $estamaq = htmlentities(VoltaValor(MAQUINAS,'COD','MID',$plano['MID_MAQUINA'],0).' - '.VoltaValor(MAQUINAS,'DESCRICAO','MID',$plano['MID_MAQUINA'],0));
            $efam_desc = htmlentities(VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$plano['MAQUINA_FAMILIA'],0));
            if ($ativ['TIPO'] == 1) {
                $tipoimg = 'lubrificacao.png';
                $pontodesc = VoltaValor(PONTOS_LUBRIFICACAO,'PONTO','MID',$ativ['MID_PONTO'],0);
            }
            if ($ativ['TIPO'] == 2) {
                $tipoimg = 'inspecao.png';
                $pontodesc = VoltaValor(MAQUINAS_CONJUNTO,'DESCRICAO','MID',$ativ['MID_CONJUNTO'],0);
            }

            $espec = VoltaValor(ESPECIALIDADES,'DESCRICAO','MID',$ativ['ESPECIALIDADE'],0);
            $material = VoltaValor(MATERIAIS,'DESCRICAO','MID',$ativ['MID_MATERIAL'],0);
            $objeto = VoltaValor(MAQUINAS,'DESCRICAO','MID',$ativ['MID_MAQUINA'],0);
            $unidade = VoltaValor(MATERIAIS,'UNIDADE','MID',$ativ['MID_MATERIAL'],0);
            $quant = $ativ['QUANTIDADE'].' '.VoltaValor(MATERIAIS_UNIDADE,'DESCRICAO','MID',$unidade,0);
            $period = VoltaValor(PROGRAMACAO_PERIODICIDADE,'DESCRICAO','MID',$ativ['PERIODICIDADE'],0);
            $freq = $ativ['FREQUENCIA'];
            
            // verifica se come�ou outra m�quina e caso positivo mostra novo header
            if ($ultimo_obj != $ativ['MID_MAQUINA']) {
                $send .= "<tr><td colspan=11 $tdstyle align=\"left\">" . $tdb[MAQUINAS]['DESC'] . ": $objeto</td></tr>\n";
            }

            $send .= "<tr>
            <td align=\"left\" $tdstyle><img src=\"../imagens/icones/22x22/$tipoimg\" border=0></td>\n";
            
            if ($filtro['NUMERO'])         $send .= "<td align=\"left\" $tdstyle>&nbsp;".$ativ['NUMERO']."</td>\n";
            if ($filtro['PARTE'])          $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($pontodesc)."</td>\n";
            if ($filtro['TAREFA'])         $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['TAREFA'])."</td>\n";
            if ($filtro['ESPECIALIDADE'])  $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($espec)."</td>\n";
            if ($filtro['QUANTIDADE_MO'])  $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['QUANTIDADE_MO'])."</td>\n";
            if ($filtro['TEMPO_PREVISTO']) $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($ativ['TEMPO_PREVISTO'])."</td>\n";
            if ($filtro['MID_MATERIAL'])   $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($material)."</td>\n";
            if ($filtro['QUANTIDADE'])     $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($quant)."</td>\n";
            if ($filtro['FREQUENCIA'])     $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($freq)."</td>\n";
            if ($filtro['PERIODICIDADE'])  $send .= "<td align=\"left\" $tdstyle>&nbsp;".htmlentities($period)."</td>\n";
            
            $send .= "</tr>\n";
            $tmptd = SomaHora(array($tmptd,$ativ['TEMPO_PREVISTO']));
            $i++;
            $ultimo_obj = $ativ['MID_MAQUINA'];
        }
        $send .= "<tr><td colspan=11 bgcolor=\"#E7E7E7\">{$ling['rel_desc_qtd_atv']}: $i -
             Tempo previsto: $tmptd</td></tr></table><br />";
        $ii += $i;
        $tmpi = SomaHora(array($tmpi,$tmptd));
        $iim++;
    }

    if (!$ii) $send = "<font size=\"2\">{$ling['rel_desc_sem_registros']}</font><br />";
    else $send .= "{$ling['rel_desc_qtd_atv_total']}: $ii<br />{$ling['rel_desc_tmp_total_prev']}: $tmpi";

    

    $tempoexec_final = utime();
    $tempoexec = round($tempoexec_final - $tempoexec_inicial,4);

    if ($_GET['word']) exportar_word($ling['rel_planos_rota'],$filtro_desc,$iim,$send,1);
    else relatorio_padrao($ling['rel_planos_rota'],$filtro_desc,$iim,$send,1,$tempoexec);

} // fim OK

##################################

?>
