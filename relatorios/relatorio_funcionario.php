<?
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento

$mes=(int)$_GET['mes'];
$ano=(int)$_GET['ano'];
$areaf=(int)$_GET['filtro_area'];
$setorf=(int)$_GET['filtro_setor'];
$maqf=(int)$_GET['filtro_maq'];
$natuf=(int)$_GET['filtro_natureza'];
$hora_hi=(int)$_GET['hi'];
$hora_hf=(int)$_GET['hf'];
$equipe_func=(int)$_GET['filtro_equipe'];
$funcf=(int)$_GET['filtro_func'];
$tipo_serv=(int)$_GET['filtro_tipo_serv'];
$filtro_servico=(int)$_GET['filtro_servico'];

/**
 * Montagem dos Filtros
 */

if ($equipe_func != 0) {
	$fil.=" AND A.EQUIPE = '$equipe_func'";
	$mostrafiltro .= "<li>{$ling['rel_desc_equipe']}: ".
	htmlentities(VoltaValor(EQUIPES,'DESCRICAO','MID',$equipe_func,0))."</li>";
}
if ($funcf != 0) {
	$fil_funcionario .=" MID = '$funcf'";
	$mostrafiltro .= "<li>{$ling['funcionario']}: ".htmlentities(VoltaValor(FUNCIONARIOS,'NOME','MID',$funcf,0))."</li>";
}
if ($areaf != 0) {
	$fil.=" AND B.MID_AREA = '$areaf'";
	$mostrafiltro .= "<li>".$tdb[AREAS]['DESC'].': '.
	htmlentities(VoltaValor(AREAS,'DESCRICAO','MID',$areaf,0))."</li>";
}
if ($setorf != 0) {
	$fil.=" AND B.MID_SETOR = '$setorf'";
	$mostrafiltro .= "<li>".$tdb[SETORES]['DESC'].': '.
	htmlentities(VoltaValor(SETORES,'DESCRICAO','MID',$setorf,0))."</li>";
}
if ($maqf != 0) {
	$fil.=" AND B.MID_MAQUINA = '$maqf'";
	$mostrafiltro .= "<li>".$tdb[MAQUINAS]['DESC'].': '.
	htmlentities(VoltaValor(MAQUINAS,'DESCRICAO','MID',$setorf,0))."</li>";
}

if ($tipo_serv == 1) {
	$mostrafiltro .= "<li>{$ling['sistematico']}";
	if ($filtro_servico == 1) {
		$mostrafiltro .= ": {$ling['preventiva']}";
	}
	if ($filtro_servico == 2) {
		$mostrafiltro .= ": {$ling['def_rota']}";
	}
	$mostrafiltro .= "</li>";
	if ($filtro_servico == 0) {
		$fil.=" AND (B.TIPO != '0' OR B.TIPO != '4' OR B.TIPO != NULL)";
	}
	else {
		$fil.=" AND B.TIPO = '$filtro_servico'";
	}
}
if ($tipo_serv == 2) {
	$mostrafiltro .= "<li>{$ling['nao_sistematico']}";
	if ($filtro_servico) {
		$mostrafiltro .= ": "	.htmlentities(VoltaValor(TIPOS_SERVICOS,'DESCRICAO','MID',$filtro_servico,0));
	}
	$mostrafiltro .= "</li>";

	if ($filtro_servico == 0) {
		$fil.=" AND (B.TIPO_SERVICO != '0' OR B.TIPO_SERVICO != NULL)";
	}
	else {
		$fil.=" AND B.TIPO_SERVICO = '$filtro_servico'";
	}
}

/**
 * Paleta de cores
 */
$paleta[1]='lightseagreen';
$paleta[2]='lightskyblue';
$paletac[1]='maroon';
$paletac[2]='mediumseagreen';
$paletac[3]='darkgoldenrod';
$paletac[4]='darkmagenta';
$paletac[5]='gray';
$paletac[6]='firebrick';
$paletac[7]='darkslateblue';
$paletac[8]='deepskyblue';
$paletac[9]='forestgreen';
$paletac[10]='mediumorchid';


if ($mes) {

	if ($equipe_func != 0) {
		if ($funcf != 0)
			$where = "WHERE $fil_funcionario OR EQUIPE = $equipe_func";
		else
			$where = "WHERE EQUIPE = $equipe_func";
	}
	else 
		if ($fil_funcionario) $where = "WHERE $fil_funcionario";
		else $where = "";
			
		$r=$dba[0]->Execute("SELECT MID,NOME, EQUIPE FROM ".FUNCIONARIOS." $where ORDER BY NOME ASC");
		while (!$r -> EOF) {
			$ca=$r->fields;
			$doc.=GeraCronogramaFuncional($ca['MID'],$mes,$ano,$hora_hi,$hora_hf,$sql_filtro);
			$iii++;
			$r->MoveNext();
		}
	

	
	$doc.= "<br clear=\"all\" />
	<table width=\300\" border=\"0\" cellpadding=\"2\" cellspacing=\"2\"><tr>
	<th colspan=\"2\">{$ling['legenda']}</th><tr>";
	$r=$dba[0]->Execute("SELECT * FROM ".TIPOS_SERVICOS." ORDER BY DESCRICAO ASC");
	while (!$r -> EOF) {
		$ca=$r->fields;
		$doc.= "<tr><td align=\"left\" style=\"background-color:".$paletac[$ca['MID']]."\">&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>".$ca['DESCRICAO']."</td></tr>";
		$r->MoveNext();
	}
	$doc.= "</table>";
	relatorio_padrao($ling['cronograma_diario_func'],$mostrafiltro,$iii,$doc,1);

}


else {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['cronograma_diario_func']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"relatorio_funcionario.php\" name=\"form_relatorio\" id=\"form_relatorio\" method=\"GET\">
<fieldset><legend>{$ling['filtros']}</legend>	
<label class=\"campo_label \" for=\"filtro_equipe\">{$ling['rel_desc_equipe']}:</label>";
	echo "<select name=\"filtro_equipe\" id=\"filtro_equipe\" class=\"campo_select\">";
	
	$tmp=$dba[$tdb[EQUIPES]['dba']] -> Execute("SELECT MID, DESCRICAO FROM ".EQUIPES." ORDER BY DESCRICAO ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";

	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_equipe'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['DESCRICAO']." </option>";
		else echo "<option value=\"".$campo['MID']."\">".$campo['DESCRICAO']." </option>";
		$tmp->MoveNext();
	}
	echo "</select>
<br clear=\"all\" />
<label class=\"campo_label \" for=\"filtro_func\">{$ling['funcionarios']}:</label>";	
	echo "<select name=\"filtro_func\" id=\"filtro_func\" class=\"campo_select\">";
	$tmp=$dba[$tdb[FUNCIONARIOS]['dba']] -> Execute("SELECT NOME,MID FROM ".FUNCIONARIOS." ORDER BY NOME ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";

	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_func'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['NOME']." </option>";
		else echo "<option value=\"".$campo['MID']."\">".$campo['NOME']." </option>";
		$tmp->MoveNext();
	}
	echo "</select><br clear=\"all\" />";

		echo "<div id=\"maq\"><label class=\"campo_label \" for=\"filtro_maq\">".$tdb[MAQUINAS]['DESC'].":</label>";
	echo " <select name=\"filtro_maq\" id=\"filtro_maq\" class=\"campo_select\">";
	$tmp=$dba[$tdb[MAQUINAS]['dba']] -> Execute("SELECT COD,DESCRICAO,MID FROM ".MAQUINAS." ORDER BY COD ASC");
	echo "<option value=\"\">".strtoupper($ling['todos'])."</option>";
	while (!$tmp->EOF) {
		$campo=$tmp->fields;
		if ($_GET['filtro_maq'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		else echo "<option value=\"".$campo['MID']."\">".$campo['COD']."-".$campo['DESCRICAO']."</option>";
		$tmp->MoveNext();
	}
	echo "</select></div>";
	
	echo "</fieldset>";


	echo "
	<fieldset>
		<legend>{$ling['tipo_servicos']}</legend>
		<input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t1\" value=\"1\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=1')\" />
		<label for=\"t1\">{$ling['rel_desc_serv_sist']}</label>
		<input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t2\" value=\"2\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=2')\" />
		<label for=\"t2\">{$ling['rel_desc_serv_nsist']}</label>
		<input class=\"campo_check\" checked=\"checked\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t3\" value=\"3\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=3')\" />
		<label for=\"t3\">{$ling['rel_desc_todos']}</label>
		<div id=\"serv\"></div>
	</fieldset>";

	echo "
	<fieldset>
		<legend>{$ling['rel_desc_alcance_cronograma']}</legend>
		<label for=\"hi\">{$ling['hora_ini']}</label>
		<select name=\"hi\" class=\"campo_select\" id=\"hi\">
			<option value=\"0\">00:00</option>
			<option value=\"1\">01:00</option>
			<option value=\"2\">02:00</option>
			<option value=\"3\">03:00</option>
			<option value=\"4\">04:00</option>
			<option value=\"5\">05:00</option>
			<option selected=\"selected\" value=\"6\">06:00</option>
			<option value=\"7\">07:00</option>
			<option value=\"8\">08:00</option>
			<option value=\"9\">09:00</option>
			<option value=\"10\">10:00</option>
			<option value=\"11\">11:00</option>
			<option value=\"12\">12:00</option>
			<option value=\"13\">13:00</option>
			<option value=\"14\">14:00</option>
			<option value=\"15\">15:00</option>
			<option value=\"16\">16:00</option>
			<option value=\"17\">17:00</option>
			<option value=\"18\">18:00</option>
			<option value=\"19\">19:00</option>
			<option value=\"20\">20:00</option>
			<option value=\"21\">21:00</option>
			<option value=\"22\">22:00</option>
			<option value=\"23\">23:00</option>
		</select>
		<br clear=\"all\" /> 
		<label for=\"hf\">{$ling['hora_final']}</label>
		<select name=\"hf\" class=\"campo_select\" id=\"hf\">
			<option value=\"0\">00:00</option>
			<option value=\"1\">01:00</option>
			<option value=\"2\">02:00</option>
			<option value=\"3\">03:00</option>
			<option value=\"4\">04:00</option>
			<option value=\"5\">05:00</option>
			<option value=\"6\">06:00</option>
			<option value=\"7\">07:00</option>
			<option value=\"8\">08:00</option>
			<option value=\"9\">09:00</option>
			<option value=\"10\">10:00</option>
			<option value=\"11\">11:00</option>
			<option value=\"12\">12:00</option>
			<option value=\"13\">13:00</option>
			<option value=\"14\">14:00</option>
			<option value=\"15\">15:00</option>
			<option value=\"16\">16:00</option>
			<option value=\"17\">17:00</option>
			<option value=\"18\">18:00</option>
			<option selected=\"selected\" value=\"19\">19:00</option>
			<option value=\"20\">20:00</option>
			<option value=\"21\">21:00</option>
			<option value=\"22\">22:00</option>
			<option value=\"23\">23:00</option>
		</select>
	</fieldset>";

	echo "
	<fieldset>
		<legend>{$ling['rel_desc_periodo']}</legend>
		<label for=\"mes\">{$ling['mes_label']}</label>
		 	<select name=\"mes\" class=\"campo_select\" id=\"mes\">
				<option value=\"1\">$ling_meses[1]</option>
				<option value=\"2\">$ling_meses[2]</option>
				<option value=\"3\">$ling_meses[3]</option>
				<option value=\"4\">$ling_meses[4]</option>
				<option value=\"5\">$ling_meses[5]</option>
				<option value=\"6\">$ling_meses[6]</option>
				<option value=\"7\">$ling_meses[7]</option>
				<option value=\"8\">$ling_meses[8]</option>
				<option value=\"9\">$ling_meses[9]</option>
				<option value=\"10\">$ling_meses[10]</option>
				<option value=\"11\">$ling_meses[11]</option>
				<option value=\"12\">$ling_meses[12]</option>
			</select>
		<br clear=\"all\" />
		<label for=\"ano\">{$ling['ano']}:</label>
		<input type=\"text\" id=\"ano\" class=\"campo_text_ob\" value=\"".$_GET['ano']."\" name=\"ano\" size=\"4\" maxlength=\"4\" />
	</fieldset>
	<br />
	<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
	</form><br />
	</div>
	</div>
	</body>
	</html>";

}


function VoltaCorLinha($ord){
	global $paleta,$paletac;
	$tipo=(int)VoltaValor(ORDEM_PLANEJADO,"TIPO","MID",$ord,0);
	$tipo_servico=(int)VoltaValor(ORDEM_PLANEJADO,"TIPO_SERVICO","MID",$ord,0);

	if ($tipo == 1){
		return $paleta[1];
	}
	elseif ($tipo == 2) {
		return $paleta[1];
	}
	elseif ($paletac[$tipo_servico]){
		return $paletac[$tipo_servico];
	}
	else {
		return "RGB(".rand(1,255).",".rand(1,255).",".rand(1,255).")";
	}
}

function GeraCronogramaFuncional($funcionario,$mes,$ano,$hora_hi,$hora_hf,$sql_filtro="",$estatistica=0,$debug=0) {
	global $manusis, $dba,$paletac,$paleta;
	if ((!$funcionario) or (!$mes) or (!$ano)) {
		return $ling['err02'];
	}
	// Ultimo dia do m�s
	$ultimo=mktime(0,0,0,$mes+1,0,$ano);

	// Executa consulta
	$sql="SELECT A.*,B.TIPO_SERVICO,B.TIPO FROM ".ORDEM_MADODEOBRA." as A, ".ORDEM." AS B WHERE B.MID = A.MID_ORDEM AND A.DATA_INICIO >= '$ano-$mes-01' AND A.DATA_FINAL <= '$ano-$mes-".date('d',$ultimo)."' AND A.MID_FUNCIONARIO = '$funcionario' $sql_filtro ORDER BY A.DATA_INICIO,A.HORA_INICIO ASC";
	$tmp=$dba[0] -> Execute($sql);
	if (!$tmp){
		Erromsg("SQL:$sql -> ".$dba[0] -> ErrorMsg());
	}
	while (!$tmp->EOF) {
		$campo=$tmp->fields;

		$ord=$campo['MID_ORDEM'];

		$di=explode("-",$campo['DATA_INICIO']);
		$df=explode("-",$campo['DATA_FINAL']);

		$hi=explode(":",$campo['HORA_INICIO']);
		$hf=explode(":",$campo['HORA_FINAL']);

		$di_mktime=mktime($hi[0],$hi[1],$hi[2],$di[1],$di[2],$di[0]);
		$df_mktime=mktime($hf[0],$hf[1],$hf[2],$df[1],$df[2],$df[0]);
		// Obtenho o tempo de trabalho
		$tempo_traba=$df_mktime-$di_mktime;
		$tempo_traba=round($tempo_traba /(60*60),4);

		// gravo na matriz o tempo total de trabalho por dia
		$c_tempo_total[(int)date('d',$di_mktime)]+=$tempo_traba;

		// gravo na matriz o tempo total de trabalho por dia da Semana (seg,ter,etc)
		$c_tempo_total_sem[(int)date('N',$di_mktime)]+=$tempo_traba;

		// O Cronograma foi projetado para uma formata��o onde uma hora � repartida em seis blocos de 10 minutos.
		// Por isso neste loop eu crio uma matriz que representa cada bloco deste e desmembro os tempos deste registro
		// separando em multiplos de dez. N�o, n�o � confuso.

		// Somo 600 segudos ou seja 10  mintos ao mktime da data inicio em toda volta...
		for ($tempo=$di_mktime; $tempo <= $df_mktime; $tempo+=600) {
			// Enquadro o tempo dentro de um dos seis blocos

			if (date('i',$tempo) < 10) {
				$h=10;
			}
			elseif (date('i',$tempo) < 20) {
				$h=20;
			}
			elseif (date('i',$tempo) < 30) {
				$h=30;
			}
			elseif (date('i',$tempo) < 40) {
				$h=40;
			}
			elseif (date('i',$tempo) < 50) {
				$h=50;
			}
			else {
				$h=60;
			}
			$c_tempo_desmembrado[(int)date('d',$tempo)][(int)date('H',$tempo)][$h]=$ord;
		}
		$tmp->MoveNext();
	}

	$doc.=  "
	<style>
.crono td {
border:1px solid black;
}
.crono th {
border:1px solid black;
text-align:center;
}
</style>
	<div style=\"text-align=left;width:100%;display:block;margin:5px\"><h3>{$ling['funcionarios']}: ".VoltaValor(FUNCIONARIOS,"NOME","MID",$funcionario,0)." - MES:  $mes/$ano</h3></div>
	<table class=\"crono\" style=\"font-size:8px;  border-collapse: collapse;\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
	<tr>
	<th width=\"5\">{$ling['dia']}</th>";

	// Colunas de hora
	$colunas_h=0;
	for ($i=$hora_hi; $i <= $hora_hf; $i++) {
		switch ($i) {
			case 0: $hh='00:00';
			break;
			case 1: $hh='01:00';
			break;
			case 2: $hh='02:00';
			break;
			case 3: $hh='03:00';
			break;
			case 4: $hh='04:00';
			break;
			case 5: $hh='05:00';
			break;
			case 6: $hh='06:00';
			break;
			case 7: $hh='07:00';
			break;
			case 8: $hh='08:00';
			break;
			case 9: $hh='09:00';
			break;
			case 10: $hh='10:00';
			break;
			case 11: $hh='11:00';
			break;
			case 12: $hh='12:00';
			break;
			case 13: $hh='13:00';
			break;
			case 14: $hh='14:00';
			break;
			case 15: $hh='15:00';
			break;
			case 16: $hh='16:00';
			break;
			case 17: $hh='17:00';
			break;
			case 18: $hh='18:00';
			break;
			case 19: $hh='19:00';
			break;
			case 20: $hh='20:00';
			break;
			case 21: $hh='21:00';
			break;
			case 22: $hh='22:00';
			break;
			case 23: $hh='23:00';
			break;
		}
		$colunas_h++;
		$doc.= "<th colspan=\"6\">$hh</th>";
	}
	$doc.= "<th width=\"10\">{$ling['total']}</th>
		</tr>";


	for ($i=1; $i <= date('d',$ultimo); $i++) {
		$dia_sem=date('N',mktime(0,0,0,$mes,$i,$ano));
		if ($dia_sem == 6) {
			$cor_linha="#FFFFFF";
		}
		if ($dia_sem == 7) {
			$cor_linha="#D9D9D9";
		}
		else {
			$cor_linha="#FFFFFF";
		}
		$doc.= "<tr bgcolor=\"$cor_linha\"><th>$i</th>";
		$h=$hora_hi; // Hora inicial
		$c=0; // Contador de minutos multiplos de 10
		$contador_colunas=1; // contador para colspan na ultima coluna
		for ($ii=0; $ii < $colunas_h*6; $ii++){
			$c++;
			if ((int)$c_tempo_desmembrado[(int)$i][(int)$h][$c*10] != 0) {
				$doc.= "<td style=\"background-color:".VoltaCorLinha($c_tempo_desmembrado[(int)$i][(int)$h][$c*10])."\"><a href=\"javascript:janela('../detalha_ord.php?busca=".$c_tempo_desmembrado[(int)$i][(int)$h][$c*10]."', 'parm', 500,400)\">&nbsp;</a></td>";
			}
			else {
				$doc.= "<td>&nbsp;</td>";
			}
			if ($c == 6) {
				$h++;
				$c=0;
			}
			$contador_colunas++;
		}
		$doc.= "
	<th width=\"10\">".round($c_tempo_total[$i],2)."</th>
	</tr>";	
		$c_total+=$c_tempo_total[$i];
	}
	$doc.= "<tr><td colspan=".$contador_colunas.">&nbsp;</td><td>$c_total</td></tr>
	</table>";
	return $doc;
}
?>