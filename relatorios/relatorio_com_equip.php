<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$tb=LimpaTexto($_GET['tb']);
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
$qto=(int)$_GET['qto'];
$cc=$_GET['cc'];
$cris=(int)$_GET['cris'];
$criv=LimpaTexto($_GET['criv']);
$cric=LimpaTexto($_GET['cric']);
$cris2=(int)$_GET['cris2'];
$criv2=LimpaTexto($_GET['criv2']);
$cric2=LimpaTexto($_GET['cric2']);
$or=$_GET['or'];
$agrupar=$_REQUEST['agrupar'];
if ($agrupar == AREAS) $agrf = 'MID_AREA';
if ($agrupar == SETORES) $agrf = 'MID_SETOR';
//if ($agrupar) die($agrupar);

// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}

if (($relatorio != "") or ($exword != "")){
	if ($qto == "") {
		$resultado=$dba[$tdb[$tb]['dba']] -> Execute("SELECT MID FROM $tb");
		$qto=count($resultado -> getrows());
	}
	if (($cric != "") and ($cric2 != "")) {
		if (($cris == 1) or ($cris == "")) {
			$cris="=";
			$fil=$ling['igual'];
		}
		elseif ($cris == 2) {
			$cris="!=";
			$fil=$ling['diferente'];
		}
		elseif ($cris == 3) {
			$cris=">";
			$fil=$ling['maior'];
		}
		elseif ($cris == 4) {
			$cris="<";
			$fil=$ling['menor'];
		}
		if ($cris == 5) {
			$cris="LIKE";
			$asp1="'%";
			$asp2="%'";
			$fil=$ling['contem'];
		}
		else {
			$asp1="'";
			$asp2="'";
		}
		$rtb=VoltaRelacao($tb,$cric);
		if ($rtb != "") {
			$tmp_valor=VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$criv,$rtb['dba']);
			$filtro=$tdb[$tb][$cric]." $fil -> $tmp_valor ";			
		}
		else $filtro="$cric $fil -> $criv";
		$tmp_valor=$criv;

		if (($cris2 == 1) or ($cris2 == "")) {
			$fil2=$ling['igual'];
			$cris2="=";
		}
		elseif ($cris2 == 2) {
			$fil2=$ling['diferente'];
			$cris2="!=";
		}
		elseif ($cris2 == 3) {
			$fil2=$ling['maior'];
			$cris2=">";
		}
		elseif ($cris2 == 4) {
			$fil2=$ling['menor'];
			$cris2="<";
		}
		if ($cris2 == 5) {
			$fil2=$ling['contem'];
			$cris2="LIKE";
			$asp21="'%";
			$asp22="%'";
		}
		else {
			$asp21="'";
			$asp22="'";

		}
		$rtb=VoltaRelacao($tb,$cric2);
		if ($rtb != "") {
			$tmp_valor=VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$criv2,$rtb['dba']);
			$filtro.=" e ".$tdb[$tb][$cric2]." $fil -> $tmp_valor";			
		}
		else $filtro.=" e $cric2 $fil -> $criv2";
		$tmp_valor2=$criv2;
		if ($agrupar == SETORES) $sql="SELECT a.*, b.MID_SETOR as REF FROM $tb as a, ".MAQUINAS." as b WHERE $cric $cris ".$asp1."$tmp_valor".$asp2." AND $cric2 $cris2 ".$asp21."$tmp_valor2".$asp22." AND a.MID_MAQUINA = b.MID ORDER BY REF, $or ASC";
		elseif ($agrupar == AREAS) $sql="SELECT a.*, c.MID_AREA as REF FROM $tb as a, ".MAQUINAS." as b, ".SETORES." as c WHERE $cric $cris ".$asp1."$tmp_valor".$asp2." AND $cric2 $cris2 ".$asp21."$tmp_valor2".$asp22." AND a.MID_MAQUINA = b.MID AND b.MID_SETOR = c.MID ORDER BY REF, $or ASC";
		else $sql="SELECT * FROM $tb WHERE $cric $cris ".$asp1."$tmp_valor".$asp2." AND $cric2 $cris2 ".$asp21."$tmp_valor2".$asp22." ORDER BY $or ASC";
	}
	elseif (($cric != "") and ($cric2 == "")) {
		if (($cris == 1) or ($cris == "")) {
			$cris="=";
			$fil=$ling['igual'];
		}
		elseif ($cris == 2) {
			$cris="!=";
			$fil=$ling['diferente'];
		}
		elseif ($cris == 3) {
			$cris=">";
			$fil=$ling['maior'];
		}
		elseif ($cris == 4) {
			$cris="<";
			$fil=$ling['menor'];
		}
		if ($cris == 5) {
			$cris="LIKE";
			$asp1="'%";
			$asp2="%'";
			$fil=$ling['contem'];
		}
		else {
			$asp1="'";
			$asp2="'";
		}		
		$rtb=VoltaRelacao($tb,$cric);
		if ($rtb != "") {
			$tmp_valor=VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$criv,$rtb['dba']);
			$filtro=$tdb[$tb][$cric]." $fil -> $tmp_valor";			
		}
		else $filtro="$cric $fil -> $criv";

		if ($agrupar == SETORES) $sql="SELECT a.*,b.MID_SETOR as REF FROM $tb as a, ".MAQUINAS." as b WHERE $cric $cris ".$asp1."$criv".$asp2." AND a.MID_MAQUINA = b.MID ORDER BY REF, $or ASC";
		elseif ($agrupar == AREAS) $sql="SELECT a.*,c.MID_AREA as REF FROM $tb as a, ".MAQUINAS." as b, ".SETORES." as c WHERE $cric $cris ".$asp1."$criv".$asp2." AND a.MID_MAQUINA = b.MID AND b.MID_SETOR = c.MID ORDER BY REF, $or ASC";
		else $sql="SELECT * FROM $tb WHERE $cric $cris ".$asp1."$criv".$asp2." ORDER BY $or ASC";
	}
	else {
		$filtro=$ling['sem_filtro'];
		if ($agrupar == SETORES) $sql="SELECT a.*,b.MID_SETOR as REF FROM $tb as a, ".MAQUINAS." as b WHERE a.MID_MAQUINA = b.MID ORDER by REF, $or ASC";
		elseif ($agrupar == AREAS) $sql="SELECT a.*,c.MID_AREA as REF FROM $tb as a, ".MAQUINAS." as b, ".SETORES." as c WHERE a.MID_MAQUINA = b.MID AND b.MID_SETOR = c.MID ORDER by REF, $or ASC";
		else $sql="SELECT * FROM $tb ORDER by $or ASC";
	}
	//die($sql);
	if (!$resultado= $dba[$tdb[$tb]['dba']] -> SelectLimit($sql,$qto)){
		$err = $dba[$tdb[$tb]['dba']] -> ErrorMsg();
		erromsg("SQL ERROR .<br>$err<br><br>$sql");
		exit;
	}
	$ncampos=$resultado -> FieldCount();


	$txt .= "<div>";
	$opentable.="<table cellpadding=\"0\" cellspacing=\"0\"  border=\"1\" bordercolor=\"black\" id=\"dados_processados\">\n";
	if (!$agrupar) $txt.= "$opentable<thead><tr>";
	$ii=0;
	$cabec = '';
	for ($fc=0; $fc < $ncampos; $fc++) {
		$campo=$resultado -> FetchField($fc);
		if (($tdb[$tb][$campo -> name] != "") and ($cc[$fc] != "")) {
			$cabec.= "<th>".$tdb[$tb][$campo -> name]."</th>\n";
			$ii++;
		}
	}
	

	if ($ii == 0) {
		echo "<html><body><br /><br /><br /><br />";
		erromsg($ling['erro_nenhum_campo']);
		echo "</body></html>";
		exit;
	}

	if (!$agrupar) {
		$txt .= $cabec;
		$txt.= "</tr></thead>\n";
	}

	$linhas=0;
	if ($agrupar) {
		$ultimo_agr = 0;
	}
	while (!$resultado->EOF) {
		$pr=$resultado -> fields;
		if ($agrupar) {
			if ($ultimo_agr != $pr['REF']) {
				if ($ultimo_agr != 0) $txt .= "</table>";
				$txt.= "$opentable
				<thead><tr><th colspan=\"".count($cc)."\">{$tdb[$agrupar]['DESC']}: ".VoltaValor($agrupar,'COD','MID',$pr['REF'],0).' - '.VoltaValor($agrupar,'DESCRICAO','MID',$pr['REF'],0)."</th></tr><tr>$cabec</tr></thead>";
				$ultimo_agr = $pr['REF'];
			}
		}
		$txt.= "<tr>\n";
		for ($fc=0; $fc < $ncampos; $fc++) {
			$campo=$resultado -> FetchField($fc);
			if ($cc[$fc] != ""){
				$rtb=VoltaRelacao($tb,$campo -> name);
				if ($rtb != "") {
					if (VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$pr[$campo -> name],$rtb['dba']) == "") $txt.= "<td>&nbsp;</td>\n";
					else $txt.= "<td>".VoltaValor($rtb['tb'],$rtb['campo'],$rtb['mid'],$pr[$campo -> name],$rtb['dba'])."</td>\n";
				}
				else {
					$ctipo = $resultado ->FetchField($fc);
					$ctipo = $resultado->MetaType($ctipo->type);
					if ($ctipo == "D") $txt.= "<td>".$resultado -> UserDate($pr[$campo -> name],'d/m/Y')."</td>\n";
					elseif ($pr[$campo -> name] == "") $txt.= "<td>&nbsp;</td>\n";
					else $txt.= "<td>".$pr[$campo -> name]."</td>\n";
				}
			}
		}
		$resultado->MoveNext();
		$txt.= "</tr>\n";
		$linhas++;
	}
	$txt.= "</table>";
	$txt .= "</div>";
	if ($relatorio != "") {
		if ($tmp_navegador['browser'] == "MSIE") exportar_word($tdb[$tb]['DESC'],$filtro,$linhas,$txt,$_GET['papel_orientacao']);
		else relatorio_padrao($tdb[$tb]['DESC'],$filtro,$linhas,$txt,1);
	}
	else exportar_word($tdb[$tb]['DESC'],$filtro,$linhas,$txt,$_GET['papel_orientacao']);
}


elseif ($tb != "") {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>".$ling['relatorio']." - ".$tdb[$tb]['DESC']."
</div>
<div id=\"corpo_relatorio\">
<form action=\"\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>".$ling['campos_para_relatorio']."</legend>
<table id=\"campos_relatorio\">";
	$sql="SELECT * FROM $tb";
	if (!$resultado= $dba[$tdb[$tb]['dba']] -> SelectLimit($sql,1)){
		$err = $dba[$tdb[$tb]['dba']] -> ErrorMsg();
		erromsg("$err<br /><br />$sql");
		exit;
	}
	else {
		$ncampos=$resultado -> FieldCount();
		echo "<tr>\n";
		$ii=0;
		for ($fc=0; $fc < $ncampos; $fc++) {
			$campo=$resultado -> FetchField($fc);
			if ($tdb[$tb][$campo -> name] != "") {
				if ($cc[$fc] != "") echo "<td><input class=\"campo_check\" type=\"checkbox\" name=\"cc[$fc]\" id=\"cc[$fc]\" checked=\"checked\" /><label for=\"cc[$fc]\">".$tdb[$tb][$campo -> name]."</label></td>\n";
				//else echo "<td><input class=\"campo_check\" type=\"checkbox\" checked=\"checked\" name=\"cc[$fc]\" id=\"cc[$fc]\" /><label for=\"cc[$fc]\">".$tdb[$tb][$campo -> name]."</label></td>\n";
				else echo "<td><input class=\"campo_check\" type=\"checkbox\" name=\"cc[$fc]\" id=\"cc[$fc]\" /><label for=\"cc[$fc]\">".$tdb[$tb][$campo -> name]."</label></td>\n";
				if ($ii == 4) {
					$ii =0;
					echo "</tr><tr>\n";
				}
				else $ii++;
			}
		}
		echo "</tr></td></table></fieldset>
<fieldset>
<legend>".$ling['filtros']."</legend>
<label for=\"or\">".$ling['ordernar']."</label>
<select name=\"or\" class=\"campo_select\">\n";
		for ($fc=0; $fc < $ncampos; $fc++) {
			$campo=$resultado -> FetchField($fc);
			if ($tdb[$tb][$campo -> name] != "") {
				echo "<option class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
			}
		}
		echo "</select><br clear=\"all\" />
<label for=\"cric\">{$ling['rel_desc_primeiro']} ".$ling['criterio']."</label>
<select onchange=\"document.forms[0].submit();\" class=\"campo_select\" name=\"cric\"><option class=\"campo_option\" value=\"\">".$ling['sem_criterios']."</option>\n";
		for ($fc=0; $fc < $ncampos; $fc++) {
			$campo=$resultado -> FetchField($fc);
			if ($tdb[$tb][$campo -> name] != "") {
				if ($cric == $campo -> name) echo "<option selected=\"selected\" class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
				else echo "<option class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
			}
		}
		echo "</select>";
		if ($cric != "") {
			$rtb=VoltaRelacao($tb,$cric);
			if ($rtb != "") {
				echo "
<select class=\"campo_select\" name=\"cris\">
<option class=\"campo_option\" value=\"1\">".$ling['igual']."</option>
<option class=\"campo_option\" value=\"2\">".$ling['diferente']."</option>
</select>";
				FormSelect("criv",$rtb['tb'],$criv,$rtb['campo'],"MID",$rtb['dba'],0);
			}
			else {
				echo "
<select class=\"campo_select\" name=\"cris\">
<option class=\"campo_option\" value=\"1\">".$ling['igual']."</option>
<option class=\"campo_option\" value=\"2\">".$ling['diferente']."</option>
<option class=\"campo_option\" value=\"3\">".$ling['maior']."</option>
<option class=\"campo_option\" value=\"4\">".$ling['menor']."</option>
<option class=\"campo_option\" value=\"5\">".$ling['contem']."</option>
</select>
<input class=\"campo_text\" name=\"criv\" size=\"25\" maxlength=\"65\" />";
			}
		}
		echo "<br clear=\"all\" /><label for=\"cric2\">{$ling['rel_desc_segundo']} ".$ling['criterio']."</label>
<select onchange=\"document.forms[0].submit();\" class=\"campo_select\" name=\"cric2\"><option class=\"campo_option\" value=\"\">".$ling['sem_criterios']."</option>\n";
		for ($fc=0; $fc < $ncampos; $fc++) {
			$campo=$resultado -> FetchField($fc);
			if ($tdb[$tb][$campo -> name] != "") {
				if ($cric2 == $campo -> name)echo "<option selected=\"selected\" class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
				else echo "<option class=\"campo_option\" value=\"".$campo -> name."\">".$tdb[$tb][$campo -> name]."</option>\n";
			}
		}
		echo "</select>";
		if ($cric2 != "") {
			$rtb2=VoltaRelacao($tb,$cric2);
			if ($rtb2 != "") {
				echo "
<select class=\"campo_select\" name=\"cris2\">
<option class=\"campo_option\" value=\"1\">".$ling['igual']."</option>
<option class=\"campo_option\" value=\"2\">".$ling['diferente']."</option>
</select>";
				FormSelect("criv2",$rtb2['tb'],$criv2,$rtb2['campo'],"MID",$rtb2['dba'],0);
			}
			else {
				echo "
<select class=\"campo_select\" name=\"cris2\">
<option class=\"campo_option\" value=\"1\">".$ling['igual']."</option>
<option class=\"campo_option\" value=\"2\">".$ling['diferente']."</option>
<option class=\"campo_option\" value=\"3\">".$ling['maior']."</option>
<option class=\"campo_option\" value=\"4\">".$ling['menor']."</option>
<option class=\"campo_option\" value=\"5\">".$ling['contem']."</option>
</select>
<input class=\"campo_text\" name=\"criv2\" size=\"25\" maxlength=\"65\" />";
			}
		}		
echo "</fieldset>";
	}

	if ($tb == EQUIPAMENTOS) echo "
<fieldset>
<legend>".$ling['agrupar_por']."</legend>
<input class=\"campo_check\" type=\"radio\" name=\"agrupar\" value=\"".AREAS."\" id=\"agrupar_".AREAS."\" />
<label for=\"agrupar_".AREAS."\">".$tdb[AREAS]['DESC']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"agrupar\" value=\"".SETORES."\" id=\"agrupar_".SETORES."\" />
<label for=\"agrupar_".SETORES."\">".$tdb[SETORES]['DESC']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"agrupar\" value=\"\" id=\"agrupar_nenhum\" checked=\"checked\" />
<label for=\"agrupar_nenhum\">".$ling['nenhum']."</label>
</fieldset>";
	
echo "	
<fieldset>
<legend>".$ling['papel_orientacao']."</legend>
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
<label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
<label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
</fieldset>

<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
<input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
</form><br />
</div>
</div>
</body>
</html>";
}

?>