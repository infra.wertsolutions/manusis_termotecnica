<?
/**
 * Manusis 3.0
 * Autor: Fernando Cosentino
 * Nota: Relatorio
 */
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}

$tempoexec_inicia = utime();

$obj=LimpaTexto($_GET['obj']);
$tipo=(int)$_GET['tipo'];

if (($relatorio != "") or ($exword != "")){
	if ($_GET['datai']) {
		$datai=explode("/",$_GET['datai']);
		$datai=$datai[2]."-".$datai[1]."-".$datai[0];
	}
	if ($_GET['dataf']) {
		$dataf=explode("/",$_GET['dataf']);
		$dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
	}
	$empf=(int)$_GET['filtro_emp'];
	$areaf=(int)$_GET['filtro_area'];
	$setorf=(int)$_GET['filtro_setor'];
	$maqf=(int)$_GET['filtro_maq'];
	$equipf=(int)$_GET['filtro_equip'];
	$maq_fam=(int)$_GET['filtro_maq_fam'];
	if ($empf != 0) $fil.=" AND c.MID_EMPRESA = '$empf'";
	if ($areaf != 0) $fil.=" AND b.MID_AREA = '$areaf'";
	if ($setorf != 0) $fil.=" AND a.MID_SETOR = '$setorf'";
	if ($maqf != 0) $fil.=" AND a.MID = '$maqf'";
	if ($maq_fam != 0) $fil.=" AND a.FAMILIA = '$maq_fam'";
	if ($mfil) $fil.=" $mfil";
	if ($equipf) $fil = "AND a.MID = '".(int)VoltaValor(EQUIPAMENTOS,'MID_MAQUINA','MID',$equipf,$tdb[EQUIPAMENTOS]['dba'])."'";

	// Filtro por Empresa
	$fil_emp = VoltaFiltroEmpresa(MAQUINAS, 2);
	$fil .= ($fil_emp != "")? "AND " . $fil_emp : "";

	$sql="SELECT a.*,b.MID_AREA, 
	 b.COD as COD_SETOR, 
	 b.DESCRICAO as DESCRICAO_SETOR, 
	 c.COD as COD_AREA ,
	 c.DESCRICAO as DESCRICAO_AREA
	 FROM ".MAQUINAS." as a, ".
	SETORES." as b, ".
	AREAS." as c 
	WHERE 
	b.MID_AREA = c.MID 
	AND a.MID_SETOR = b.MID 
	$fil 
	ORDER BY 
	b.MID_AREA,
	a.MID_SETOR,
	a.FAMILIA,
	a.COD 
	ASC";

	if (!$resultado_maq = $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg()."<br><br>$sql");
	$iii=0;

	$header = "";
	$tdstyle="style=\"border-bottom: 1px solid black; border-right: 1px solid black\"";

	while (!$resultado_maq->EOF) {
		$campo_maq=$resultado_maq -> fields;
		$mid_maq = $campo_maq['MID'];
		$maq_desc = $campo_maq['DESCRICAO'];
		$setor_desc = $campo_maq['COD_SETOR']." - ".$campo_maq['DESCRICAO_SETOR'];
		$area_desc = $campo_maq['COD_AREA']." - ".$campo_maq['DESCRICAO_AREA'];

		if ($equipf) $equip_desc = VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$equipf,0);

		$j = 0;
		// inicio da tabela desta maquina/equipamento
		$txtj="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
				<thead>
				<tr>
				<th align=\"left\"><strong>{$tdb[AREAS]['DESC']}</strong>: $area_desc</th>
				<th align=\"left\"><strong>{$tdb[SETORES]['DESC']}</strong>: $setor_desc</th>
				</tr><tr>
				<th align=\"left\"><strong>{$tdb[MAQUINAS_FAMILIA]['DESC']}</strong>: $fam_desc</th>
				<th align=\"left\"><strong>{$tdb[MAQUINAS]['DESC']}</strong>: $maq_desc</th>
				</tr>";

		$txtj.="</thead>
				<tr>
				<td align=\"left\" colspan=\"2\">";


		if ($equipf)
		$sql="SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_EQUIPAMENTO = '$equipf' ORDER BY PONTO ASC";
		else {
			$oror = GeraOR(EQUIPAMENTOS,"MID_EQUIPAMENTO","MID","MID_MAQUINA = '$mid_maq'");
			if ($oror) $oror = "OR ($oror)";
			$sql="SELECT * FROM ".PONTOS_PREDITIVA." WHERE MID_MAQUINA = '$mid_maq' $oror ORDER BY PONTO ASC";
		}

			
		if (!$res_ponto = $dba[$tdb[PONTOS_PREDITIVA]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[PONTOS_PREDITIVA]['dba']] -> ErrorMsg()."<br><br>$sql");
          
		$tem_preditiva = !$res_ponto->EOF;
		
		if($tem_preditiva)
		{
			$header .= "
                  <table id=\"dados_processados\" cellspacing=\"0\" cellpadding=\"0\"
	                style=\"border-left: 1px solid black; border-top: 1px solid black; margin-top: 5px\" width=\"100%\">
                         <div align=\"left\" ><strong><font size=\"3\">".$campo_maq['COD'].'-'.$campo_maq['DESCRICAO']."</font></strong></div>
                  ";
		}

		while (!$res_ponto->EOF) {
			$j++;
			$campo_ponto=$res_ponto -> fields;
			$ponto = $campo_ponto['MID'];
			$ponto_desc = $campo_ponto['PONTO'];
			$ponto_grand = $campo_ponto['GRANDEZA'];
			$pequip = $campo_ponto['MID_EQUIPAMENTO'];
			$vmax = $campo_ponto['VALOR_MAXIMO'];
			$vmin = $campo_ponto['VALOR_MINIMO'];
				
			// inicio da tabela ponto
			if ($pequip) $txtequip="{$tdb[EQUIPAMENTOS]['DESC']}</strong>: ".VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$pequip,0)."<br>";
			else $txtequip='';
				
			$header .= "<thead>
			<tr>
			            <th colspan=\"2\" $tdstyle><strong>{$txtequip}$ponto_desc - $ponto_grand<br />
			            {$tdb[PONTOS_PREDITIVA]['VALOR_MINIMO']}: $vmin &nbsp; - &nbsp; {$tdb[PONTOS_PREDITIVA]['VALOR_MAXIMO']}: $vmax</strong>
			 </th>
			            </tr>
			            <tr>
			              <th $tdstyle>{$tdb[LANC_PREDITIVA]['DATA']}</th>
			              <th $tdstyle>{$tdb[LANC_PREDITIVA]['VALOR']}</th>	
			           </tr></thead>";
			            	
			            	
			            $txtj.="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
					<thead><tr>
					<th align=\"center\" colspan=3><strong>{$txtequip}$ponto_desc - $ponto_grand<br>
					{$tdb[PONTOS_PREDITIVA]['VALOR_MINIMO']}: $vmin &nbsp; - &nbsp; {$tdb[PONTOS_PREDITIVA]['VALOR_MAXIMO']}: $vmax</strong></th>
					</tr><tr>
					<th>{$tdb[LANC_PREDITIVA]['DATA']}</th><th>{$ling['hora']}</th><th>{$tdb[LANC_PREDITIVA]['VALOR']}</th></tr>
					</thead>";
						
					$fil2 = '';
					if ($datai) $fil2 .= " AND DATA >= '$datai 00:00:00'";
					if ($dataf) $fil2 .= " AND DATA <= '$dataf 23:59:59'";
					$sql="SELECT * FROM ".LANC_PREDITIVA." WHERE MID_PONTO = '$ponto' $fil2 ORDER BY DATA ASC";
					if (!$res_lanc = $dba[$tdb[LANC_PREDITIVA]['dba']] -> Execute($sql)) errofatal("SQL ERROR .<br>".$dba[$tdb[LANC_PREDITIVA]['dba']] -> ErrorMsg()."<br><br>$sql");
					$k=0;
					while (!$res_lanc->EOF) {
						$campo_lanc=$res_lanc -> fields;
						$iii++;
						$k++;
						$valor = $campo_lanc['VALOR'];
						if (($valor < $vmin) or ($valor > $vmax)) $valor_txt = "<font color=\"#FF0000\"><strong>$valor</strong></font>";
						else $valor_txt = $valor;
						list($edata,$ehora) = explode(' ',$campo_lanc['DATA'],2);
						$edata = NossaData($edata);
						$txtj .= "<tr><td $tdstyle>$edata</td><td $tdstyle>$ehora</td><td $tdstyle>$valor_txt</td></tr>";

						$header .= "<tr>
				               <td $tdstyle>$edata $ehora</td>
				               <td $tdstyle>$valor_txt</td>
				            </tr>";

						$res_lanc->MoveNext();
					}
					if (!$k){
						$txtj .= "<tr><td $tdstyle colspan=\"3\" align=\"center\"><center>{$ling['rel_desc_apontamento_disp']}</center></td></tr>";
						$header .= "<tr>
				<td colspan=\"2\" $tdstyle><center>{$ling['rel_desc_apontamento_disp']}</center></td>
				</tr>";
					}
						
					// fim da tabela ponto
					$txtj .= "</table><br clear=\"all\" />";
						
					$res_ponto->MoveNext();
		}
		
		if($tem_preditiva){
			$header .="</table><br clear=\"all\" />";
		}
		
		// fim da tabela desta maquina/equipamento
		$txtj .= "</td></tr></table><br clear=\"all\" />";
		if ($j) $txt .= $txtj;
		$resultado_maq->MoveNext();
	}



	$filtro='';
	if ($empf) AddStr($filtro,', ',"{$tdb[EMPRESAS]['DESC']}: ".VoltaValor(EMPRESAS,'COD','MID',$empf,0).' - '.VoltaValor(EMPRESAS,'NOME','MID',$empf,0));
	if ($areaf) AddStr($filtro,', ',"{$tdb[AREAS]['DESC']}: ".VoltaValor(AREAS,'COD','MID',$areaf,0).' - '.VoltaValor(AREAS,'DESCRICAO','MID',$areaf,0));
	if ($setorf) AddStr($filtro,', ',"{$tdb[SETORES]['DESC']}: ".VoltaValor(SETORES,'COD','MID',$setorf,0).' - '.VoltaValor(SETORES,'DESCRICAO','MID',$setorf,0));
	if ($maqf) AddStr($filtro,', ',"{$tdb[MAQUINAS]['DESC']}: ".VoltaValor(MAQUINAS,'DESCRICAO','MID',$maqf,0));
	if ($maq_fam) AddStr($filtro,', ',"{$tdb[MAQUINAS_FAMILIA]['DESC']}: ".VoltaValor(MAQUINAS_FAMILIA,'COD','MID',$maq_fam,0).' - '.VoltaValor(MAQUINAS_FAMILIA,'DESCRICAO','MID',$maq_fam,0));
	if ($equipf) AddStr($filtro,', ',"{$tdb[EQUIPAMENTOS]['DESC']}: ".VoltaValor(EQUIPAMENTOS,'DESCRICAO','MID',$equipf,0));
	if ($datai) AddStr($filtro,', ',$ling['rel_desc_de']." ".$_GET['datai']);
	if ($dataf) AddStr($filtro,', ',$ling['rel_desc_ate']." ".$_GET['dataf']);

	if (!$filtro) $filtro = $ling['nenhum'];

	$tempoexec_final = utime();
	$tempoexec = round($tempoexec_final - $tempoexec_inicial,4);

	if ($relatorio != "") relatorio_padrao($ling['rel_monitoramento'],$filtro,$iii,$header,1, $tempoexec);
	else exportar_word($ling['rel_monitoramento'],$filtro,$iii,$txt,$_GET['papel_orientacao']);
}


else {
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n";
	if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
	echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['rel_monitoramento']}
</div>
<div id=\"corpo_relatorio\">
<form action=\"\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>{$ling['rel_desc_localizacoes']}</legend>	
	<div id=\"filtro_relatorio\">";

	FiltrosRelatorio(1, 1, 1, 1, 1, 0, 1);

	echo "</div>
    </fieldset>";

	echo "<fieldset><legend>{$ling['rel_desc_periodo_op']}</legend>";
	FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
	echo "<br clear=\"all\" />";
	FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");
	echo "
	</fieldset>";

	echo "<fieldset>
<legend>".$ling['papel_orientacao']."</legend>
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
<label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
<br clear=\"all\" />
<input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
<label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
</fieldset>
<br />
<input type=\"hidden\" name=\"tb\" value=\"$tb\" />
<input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
<input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
</form><br />
</div>
</div>
</body>
</html>";

}

?>