<?
/**
* Manusis 3.0
* Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
* Nota: Relat�rio material por localizacao
*/
// Fun&ccedil;&otilde;es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

#Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
	$s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
	$f = $s + strlen($parent);
	$version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
	$version = preg_replace('/[^0-9,.]/','',$version);
	if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
		$tmp_navegador[browser] = $parent;
		$tmp_navegador[version] = $version;
	}
}
#############################
$ajax = $_GET['ajax'];
$setor = $_GET['setor'];
$area = $_GET['area'];
$fam = $_GET['fam'];
$obj = $_GET['obj'];

if ($ajax or $_GET['env']) {
	$condmaq = '';
	$condset = '';
	if ($obj) $condmaq = "MID = '$obj'";
	else {

		if ($setor) $condmaq .= "MID_SETOR = '$setor'";
		if ($fam) {
			if ($condmaq) $condmaq .= ' AND ';
			$condmaq .= "FAMILIA = '$fam'";
		}
		if ($area) {
			$condset = "MID_AREA = '$area'";

			// gera sequencia de or mid_setor=tal etc
			if (!$setor) {
				$setfil = '';
				$sql="SELECT MID FROM ".SETORES." WHERE MID_AREA = '$area'";
				if (!$resultado= $dba[$tdb[SETORES]['dba']] -> Execute($sql)){
					$err = $dba[$tdb[SETORES]['dba']] -> ErrorMsg();
					erromsg("SQL ERROR .<br>$err<br><br>$sql");
					exit;
				}
				while (!$resultado->EOF) {
					$campo=$resultado -> fields;
					if ($setfil) $setfil .= ' OR ';
					$setfil .= "MID_SETOR = ".$campo['MID'];
					$resultado->MoveNext();
				}
				if ($setfil) $setfil = "($setfil)";
				else $setfil = 'MID_SETOR = 0'; // se n�o tem setores nessa area, nao tem maquina

				if ($condmaq) $condmaq .= ' AND ';
				$condmaq .= $setfil;
			} // fim da sequencia em $setfil
		}
	}
}

if (!$_GET['env']) { // n�o exibindo relatorio

	if ($ajax) {

		$primeirocampo=$ling['todos2'];
		echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label><br />";
		FormSelect3("fam",MAQUINAS_FAMILIA,$_GET['fam'],"DESCRICAO","MID",$tdb[MAQUINAS_FAMILIA]['dba'],0,'','fil',"ajax=1&area=$area&setor=$setor&fam=");
		echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label><br />";
		FormSelect3("area",AREAS,$_GET['area'],"DESCRICAO","MID",$tdb[AREAS]['dba'],0,'','fil',"ajax=1&setor=$setor&fam=$fam&area=");
		echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label><br />";
		FormSelect3("setor",SETORES,$_GET['setor'],"DESCRICAO","MID",$tdb[SETORES]['dba'],0,$condset,'fil',"ajax=1&area=$area&fam=$fam&setor=");
		echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label><br />";
		FormSelect3("setor",MAQUINAS,$_GET['obj'],"DESCRICAO","MID",$tdb[MAQUINAS]['dba'],0,$condmaq,'','');
	}
	else {
		echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"../temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"../lib/javascript.js\"> </script>\n";
		if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"../lib/movediv.js\"> </script>\n";
		echo "</head>
<body><div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>{$ling['rel_localizacao_pecas']}</h1>
</div>
<div id=\"corpo_relatorio\">
<form action=\"".$_SERVER['PHP_SELF']."\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
<fieldset>
<legend>".$ling['filtros']."</legend>
<div id=\"fil\">";
		$primeirocampo=strtoupper($ling['todos']);
		echo "<label class=\"campo_label\" for=\"fam\">".$tdb[MAQUINAS_FAMILIA]['DESC'].":</label><br />";
		FormSelect3("fam",MAQUINAS_FAMILIA,$_GET['fam'],"DESCRICAO","MID",$tdb[MAQUINAS_FAMILIA]['dba'],0,'','fil',"ajax=1&area=$area&setor=$setor&obj=$obj&&fam=");
		echo "<label class=\"campo_label\" for=\"area\">".$tdb[AREAS]['DESC'].":</label><br />";
		FormSelect3("area",AREAS,$_GET['area'],"DESCRICAO","MID",$tdb[AREAS]['dba'],0,'','fil',"ajax=1&setor=$setor&fam=$fam&obj=$obj&area=");
		echo "<label class=\"campo_label\" for=\"setor\">".$tdb[SETORES]['DESC'].":</label><br />";
		FormSelect3("setor",SETORES,$_GET['setor'],"DESCRICAO","MID",$tdb[SETORES]['dba'],0,'','fil',"ajax=1&area=$area&obj=$obj&fam=$fam&setor=");
		echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC'].":</label><br />";
		FormSelect2("obj",MAQUINAS,$_GET['obj'],"DESCRICAO","MID",$tdb[MAQUINAS]['dba'],0);
		echo "</div>";
		echo "<br /><font size=1>{$ling['rel_desc_loc_mat_texto']}</font>
		</fieldset><br />
<input type=\"hidden\" name=\"env\" value=\"1\" />
<input type=\"submit\" value=\"{$ling['relatorio_html']}\" class=\"botao\">
<input type=\"submit\" name=\"word\" value=\"{$ling['relatorio_doc']}\" class=\"botao\">
</form><br />
</div>
</div>
</body>
</html>";
	}
}
else {

	$head="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\"><tr><td align=\"left\">";
	$half = "</td></tr><tr><td>";
	$foot = "</td></tr></table>";

	$head1="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
<tr>
<td align=\"left\"><font size=\"2\"><strong>".$tdb[MAQUINAS]['COD']."</strong></font></td>
<td align=\"left\"><font size=\"2\"><strong>".$tdb[MAQUINAS]['DESC']."</strong></font></td>
<td align=\"left\"><font size=\"2\"><strong>".$tdb[MAQUINAS_FAMILIA]['DESC']."</strong></font></td>
</tr>";

	$head2a="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
<tr>
<td align=\"left\"><font size=\"2\">".$tdb[MAQUINAS_CONJUNTO]['DESC']."</font></td>

<td align=\"left\"><font size=\"2\">".$tdb[MATERIAIS]['DESCRICAO']."</font></td>
<td align=\"left\"><font size=\"2\">".$tdb[MATERIAIS]['UNIDADE']."</font></td>
<td align=\"left\"><font size=\"2\">{$ling['qtd']}</font></td>
</tr>";

	$head2b="<table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
<tr>
<td align=\"left\"><font size=\"2\">".$tdb[EQUIPAMENTOS]['DESC']."</font></td>

<td align=\"left\"><font size=\"2\">".$tdb[MATERIAIS]['DESCRICAO']."</font></td>
<td align=\"left\"><font size=\"2\">".$tdb[MATERIAIS]['UNIDADE']."</font></td>
<td align=\"left\"><font size=\"2\">{$ling['qtd']}</font></td>
</tr>";

	$txt = '';
	$ipp=0;

	if ($condmaq) $condmaq = " WHERE $condmaq";

	$sql="SELECT * FROM ".MAQUINAS."$condmaq ORDER BY DESCRICAO ASC";
	if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
		$err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
		erromsg("SQL ERROR .<br>$err<br><br>$sql");
		exit;
	}
	while (!$resultado->EOF) {
		$estetxta = '';
		$estetxtb = '';
		$campo=$resultado -> fields;
		$maqcod = $campo['COD'];
		$maqdesc = $campo['DESCRICAO'];
		$maqfam = VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$campo['FAMILIA'],$tdb[MAQUINAS_FAMILIA]['dba']);
		$rpos=$dba[$tdb[MAQUINAS_CONJUNTO_MATERIAL]['dba']]-> Execute("SELECT * FROM ".MAQUINAS_CONJUNTO_MATERIAL." WHERE MID_MAQUINA = '".$campo['MID']."' ORDER BY MID_CONJUNTO ASC");
		while (!$rpos->EOF) {
			$unid = VoltaValor(MATERIAIS,"UNIDADE","MID",$rpos ->fields['MID_MATERIAL'],$tdb[MATERIAIS]['dba']);
			$unid = VoltaValor(MATERIAIS_UNIDADE,"COD","MID",$unid,$tdb[MATERIAIS_UNIDADE]['dba']);
			$estetxta .="<tr><td>".VoltaValor(MAQUINAS_CONJUNTO,"TAG","MID",$rpos ->fields['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])."-".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$rpos ->fields['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])."</td>";
			$estetxta .="<td>".VoltaValor(MATERIAIS,"DESCRICAO","MID",$rpos ->fields['MID_MATERIAL'],$tdb[MATERIAIS]['dba'])."</td>";
			$estetxta .="<td>$unid</td><td>".$rpos ->fields['QUANTIDADE']."</td>";
			$estetxta .="</tr>";
			$rpos->MoveNext();
			$ipp++;
		}

		$rpos=$dba[$tdb[EQUIPAMENTOS]['dba']]-> Execute("SELECT * FROM ".EQUIPAMENTOS." WHERE MID_MAQUINA = '".$campo['MID']."' ORDER BY DESCRICAO ASC");
		while (!$rpos->EOF) {
			$listapeca=ListaPecaEquipamento($rpos ->fields['MID']);
			if ($listapeca != 0) {
				for ($ip=0; $listapeca[$ip]['nome'] != ""; $ip++) {
					$peca=$listapeca[$ip]['nome'];
					$qto=$listapeca[$ip]['qto'];
					$pmid=$listapeca[$ip]['mid'];
					$pun = VoltaValor(MATERIAIS,"UNIDADE","MID",$rpos ->fields['MID_MATERIAL'],$tdb[MATERIAIS]['dba']);
					$pun = VoltaValor(MATERIAIS_UNIDADE,"COD","MID",$unid,$tdb[MATERIAIS_UNIDADE]['dba']);
					$pcod = VoltaValor(MATERIAIS,"COD","MID",$pmid,$tdb[MATERIAIS]['dba']);
					$estetxtb.="<tr><td>".$rpos ->fields['COD']."-".$rpos ->fields['DESCRICAO']."</td>";
					$estetxtb.="<td>$peca</td><td>$pun</td><td>$qto</td></tr>\n";
					$ipp++;
				}
			}
			$rpos->MoveNext();
		}
		$estetxt = '';
		if ($estetxta) $estetxt .= $head2a.$estetxta.'</table>';
		if ($estetxtb) $estetxt .= $head2b.$estetxtb.'</table>';

		if ($estetxt) $txt .= $head.$head1."<tr>
			<td>$maqcod</td><td>$maqdesc</td><td>$maqfam</td>
			</tr></table>".$half.$estetxt.$foot;

		$resultado->MoveNext();
	}

	$filtro = '';
	if ($area) $filtro .= $tdb[AREAS]['DESC'].': '.VoltaValor(AREAS,"DESCRICAO","MID",$area,$tdb[AREAS]['dba']);
	if ($setor) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[SETORES]['DESC'].': '.VoltaValor(SETORES,"DESCRICAO","MID",$setor,$tdb[SETORES]['dba']);
	}
	if ($fam) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[MAQUINAS_FAMILIA]['DESC'].': '.VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$fam,$tdb[MAQUINAS_FAMILIA]['dba']);
	}
	if ($obj) {
		if ($filtro) $filtro .= '; ';
		$filtro .= $tdb[MAQUINAS]['DESC'].': '.VoltaValor(MAQUINAS,"DESCRICAO","MID",$obj,$tdb[MAQUINAS]['dba']);
	}

	if ($_GET['word']) exportar_word($ling['rel_localizacao_pecas'],$filtro,$ipp,$txt,1);
	else relatorio_padrao($ling['rel_localizacao_pecas'],$filtro,$ipp,$txt,1);

} // fim OK


##################################

function FormSelect2($cc,$tb,$sel,$c1,$c2,$conexao,$formulario) {
	global $dba,$id,$tdb,$oq,$exe,$op,$form,$recb_valor,$primeirocampo;
	echo "<select class=\"campo_select\" name=\"$cc\">
	<option value=\"\">$primeirocampo</option>\n";
	$resultado=$dba[$conexao] -> Execute("SELECT * FROM $tb ORDER BY $c1 ASC");
	if(!$resultado) {
		echo "<br /><hr />".erromsg($dba[$conexao] -> ErrorMsg())."<hr /><br />";
	}
	else {
		$campo = $resultado -> fields;
		while (!$resultado->EOF) {
			$campo = $resultado -> fields;
			$sc2=$campo[$c2];
			$sc1=htmlentities($campo[$c1]);
			if ($sel == $sc2) echo "<option value=\"$sc2\" selected>$sc1</option>\n";
			else echo "<option value=\"$sc2\">$sc1</option>\n";
			$resultado->MoveNext();
			$i++;
		}
		echo "</select><br />\n";
	}
}

function FormSelect3($cc,$tb,$sel,$c1,$c2,$conexao,$formulario,$cond,$atualiza,$atualizacampos) {
	global $dba,$id,$tdb,$oq,$exe,$op,$form,$recb_valor,$primeirocampo;
	if ($cond) $cnd = "WHERE $cond";
	if ($atualiza) $atual = " onchange=\"atualiza_area2('$atualiza','".$_SERVER['PHP_SELF']."?$atualizacampos' + this.options[this.selectedIndex].value)\"";
	echo "<select class=\"campo_select\" name=\"$cc\" $atual>
	<option value=\"\">$primeirocampo</option>\n";
	$resultado=$dba[$conexao] -> Execute("SELECT * FROM $tb $cnd ORDER BY $c1 ASC");
	if(!$resultado) {
		echo "<br /><hr />".erromsg($dba[$conexao] -> ErrorMsg())."<hr /><br />";
	}
	else {
		$campo = $resultado -> fields;
		while (!$resultado->EOF) {
			$campo = $resultado -> fields;
			$sc2=$campo[$c2];
			$sc1=htmlentities($campo[$c1]);
			if ($sel == $sc2) echo "<option value=\"$sc2\" selected>$sc1</option>\n";
			else echo "<option value=\"$sc2\">$sc1</option>\n";
			$resultado->MoveNext();
			$i++;
		}
		echo "</select><br />\n";
	}
}



?>