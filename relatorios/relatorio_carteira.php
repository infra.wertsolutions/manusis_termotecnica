<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Relatorio
*/
// Fun��es do Sistema
if (!require("../lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require("../conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("../lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra��o de dados
elseif (!require("../lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require("../lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require("../lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require("../lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("../conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("../temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

// Variaveis de direcionamento
$tb=LimpaTexto($_GET['tb']);
$relatorio=$_GET['relatorio'];
$exword=$_GET['exword'];
$qto=(int)$_GET['qto'];
$cc=$_GET['cc'];
$cris=(int)$_GET['cris'];
$criv=LimpaTexto($_GET['criv']);
$cric=LimpaTexto($_GET['cric']);
$cris2=(int)$_GET['cris2'];
$criv2=LimpaTexto($_GET['criv2']);
$cric2=LimpaTexto($_GET['cric2']);
$or=$_GET['or'];
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);

$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
$obj=LimpaTexto($_GET['obj']);
$tipo=(int)$_GET['tipo'];

if (($relatorio != "") or ($exword != "")){
    $datai=explode("/",$_GET['datai']);
    $datai=$datai[2]."-".$datai[1]."-".$datai[0];
    $dataf=explode("/",$_GET['dataf']);
    $dataf=$dataf[2]."-".$dataf[1]."-".$dataf[0];
    $empf=(int)$_GET['filtro_emp'];
    $areaf=(int)$_GET['filtro_area'];
    $setorf=(int)$_GET['filtro_setor'];
    $maqf=(int)$_GET['filtro_maq'];
    $ccf=(int)$_GET['filtro_cc'];
    $defeitof=(int)$_GET['filtro_defeito'];
    $natuf=(int)$_GET['filtro_natureza'];
    $respf=(int)$_GET['filtro_resp'];
    $funcf=(int)$_GET['filtro_func'];
    $tipo_serv=(int)$_GET['filtro_tipo_serv'];
    $filtro_servico=(int)$_GET['filtro_servico'];
    $filtro_numos= LimpaTexto($_GET['filtro_numos']);
    $status_ordem=(int)$_GET['st_ordem'];
    $maq_fam=(int)$_GET['filtro_maq_fam'];
    $info_filtro = "<br>";

    $fil='';
    
    if ($maq_fam != 0) {
        $sql="SELECT MID FROM ".MAQUINAS." WHERE FAMILIA = '$maq_fam'";
        if (!$resultado= $dba[$tdb[MAQUINAS]['dba']] -> Execute($sql)){
            $err = $dba[$tdb[MAQUINAS]['dba']] -> ErrorMsg();
            erromsg("SQL ERROR .<br>$err<br><br>$sql");
            exit;
        }
        $info_filtro .= "<strong>{$ling['rel_desc_familia']}:</strong> ".VoltaValor(MAQUINAS_FAMILIA,"DESCRICAO","MID",$maq_fam,0).";  ";
        $maqs=array(0);
        while (!$resultado->EOF) {
            $campo=$resultado -> fields;
            $maqs[] = $campo['MID'];
            $resultado->MoveNext();
        }
        AddStr($fil,' AND ',"MID_MAQUINA IN (".(join(', ',$maqs)).")");
    }
    if ($empf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_empresa']}:</strong> ".VoltaValor(EMPRESAS,"NOME","MID",$empf,0).";  ";
        AddStr($fil,' AND ',"MID_EMPRESA = '$empf'");
    }
    if ($areaf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_loc1']}:</strong> ".VoltaValor(AREAS,"DESCRICAO","MID",$areaf,0).";  ";
        AddStr($fil,' AND ',"MID_AREA = '$areaf'");
    }
    if ($setorf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_loc2']}:</strong> ".VoltaValor(SETORES,"DESCRICAO","MID",$setorf,0).";  ";
        AddStr($fil,' AND ',"MID_SETOR = '$setorf'");
    }
    if ($maqf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_obj']}:</strong> ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$maqf,0).";  ";
        AddStr($fil,' AND ',"MID_MAQUINA = '$maqf'");
    }
    if ($ccf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_cc']}:</strong> ".VoltaValor(CENTRO_DE_CUSTO,"DESCRICAO","MID",$ccf,0).";  ";
        AddStr($fil,' AND ',"CENTRO_DE_CUSTO = '$ccf'");
    }
    if ($defeitof != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_def']}:</strong> ".VoltaValor(DEFEITO,"DESCRICAO","MID",$defeitof,0).";  ";
        AddStr($fil,' AND ',"DEFEITO = '$defeitof'");
    }
    if ($natuf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_natu']}:</strong> ".VoltaValor(NATUREZA_SERVICOS,"DESCRICAO","MID",$natuf,0).";  ";
        AddStr($fil,' AND ',"NATUREZA = '$natuf'");
    }
    if ($respf != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_resp']}:</strong> ".VoltaValor(FUNCIONARIOS,"NOME","MID",$respf,0).";  ";
        AddStr($fil,' AND ',"RESPONSAVEL = '$respf'");
    }
    if ($filtro_numos != 0) {
        $info_filtro .= "<strong>{$ling['rel_desc_num_os']}:</strong> $filtro_numos;  ";
        AddStr($fil,' AND ',"NUMERO = '$filtro_numos'");
    }
    if ($tipo_serv == 1) {
        if ($filtro_servico == 0) {
            AddStr($fil,' AND ',"(TIPO != '0' OR TIPO != '4' OR TIPO != '')");
        }
        else{
            AddStr($fil,' AND ',"TIPO = '$filtro_servico'");
        }
    }
    if ($tipo_serv == 2) {
        if ($filtro_servico == 0) AddStr($fil,' AND ',"(TIPO_SERVICO != '0' OR TIPO_SERVICO != '')");
        else AddStr($fil,' AND ',"TIPO_SERVICO = '$filtro_servico'");
    }
    if($tipo_serv == 1){
        if($filtro_servico==1){
            $info_filtro .= "<strong>{$ling['rel_desc_tp_serv']}:</strong> {$ling['rel_desc_prev']};  ";
        }
        elseif($filtro_servico==2){
            $info_filtro .= "<strong>{$ling['rel_desc_tp_serv']}:</strong> {$ling['rel_desc_rotas']};  ";
        }
    }
    if($tipo_serv == 2){
        $info_filtro .= "<strong>{$ling['rel_desc_tp_serv']}:</strong> ".VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$filtro_servico,0).";  ";

    }
    
    if ($_GET['datai'] != "") {
        if($fil) $and = " AND";
        $fil.="$and DATA_PROG >= '$datai' AND DATA_PROG <= '$dataf'";
    }
    if($status_ordem !=9){
        if($fil){
            $fil .= " AND STATUS = '$status_ordem'";
        }
        else{
            $fil .= " STATUS = '$status_ordem'";
        }
        if($status_ordem==1){
            $f_status = $ling['rel_desc_abertas'];
        }
        elseif($status_ordem==2){
            $f_status = $ling['rel_desc_fechadas'];
        }
        elseif ($status_ordem==3){
            $f_status = $ling['rel_desc_canceladas'];
        }
        else{
            $f_status = $ling['rel_desc_todas'];
        }
        $info_filtro .= "<strong>{$ling['rel_desc_status']}:</strong> $f_status;  ";
    }

    // Filtro por Empresa
    $fil_emp = VoltaFiltroEmpresa(ORDEM_PLANEJADO, 2);
    if ($fil_emp != "") {
        AddStr($fil, ' AND ', $fil_emp);
    }

    if($fil){
        $sql="SELECT * FROM ".ORDEM_PLANEJADO." WHERE $fil ORDER BY MID_SETOR,NUMERO ASC";
    }
    else{
        $sql="SELECT * FROM ".ORDEM_PLANEJADO." ORDER BY MID_SETOR,NUMERO ASC";
    }

    if (!$resultado= $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> Execute($sql)){
        $err = $dba[$tdb[ORDEM_PLANEJADO]['dba']] -> ErrorMsg();
        erromsg("SQL ERROR .<br>$err<br><br>$sql");
        exit;
    }
    $iii=0;
    $tmp_setor = -1; // for�a que MIDSETOR=0 tamb�m mostre cabe�alho
    while (!$resultado->EOF) {
        $campo=$resultado -> fields;
        if ($campo['MID_SETOR'] !== $tmp_setor) {
            $txt.="
            </table><div align=\"left\"><strong><font size=3>".VoltaValor(SETORES,"COD","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."-".VoltaValor(SETORES,"DESCRICAO","MID",$campo['MID_SETOR'],$tdb[SETORES]['dba'])."</font></strong></div>
            <table id=\"dados_processados\" cellpadding=\"2\" style=\"border: 1px solid black\" width=\"100%\">
            <tr>
            <th align=\"center\">{$ling['rel_desc_s']}</th>
            <th align=\"center\">{$ling['rel_desc_numero']}</th>
            <th align=\"center\">{$ling['rel_desc_obj2']}</th>
            <th align=\"center\">{$ling['rel_desc_pos']}</th>
            <th align=\"center\">{$ling['rel_desc_tp_serv']}</th>
            <th align=\"center\">{$ling['rel_desc_dt_prog']}</th>
            <th align=\"center\">{$ling['rel_desc_tempo_parada']}</th>
            <th align=\"center\">{$ling['rel_desc_tempo_serv']}</th>
            <th align=\"center\">{$ling['rel_desc_def']}</th>
            </tr>";
        }
        // PAREI AQUI
        if ($campo['STATUS'] == 1) $s=$ling['rel_desc_situ'][1];
        if ($campo['STATUS'] == 2) $s=$ling['rel_desc_situ'][2];
        if ($campo['STATUS'] == 3) $s=$ling['rel_desc_situ'][3];

        $txt.= "<tr>
        <td>$s</td>
        <td>".$campo['NUMERO']."</td>
        <td>".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo['MID_MAQUINA'],$tdb[MAQUINAS]['dba'])."</td>
        <td>".VoltaValor(MAQUINAS_CONJUNTO,"DESCRICAO","MID",$campo['MID_CONJUNTO'],$tdb[MAQUINAS_CONJUNTO]['dba'])."</td>";

        if (($campo['TIPO'] == "") or ($campo['TIPO'] == 0) or ($campo['TIPO'] == 4)) $txt.="<td>".VoltaValor(TIPOS_SERVICOS,"DESCRICAO","MID",$campo['TIPO_SERVICO'],$tdb[TIPOS_SERVICOS]['dba'])."</td>";
        else $txt.="<td>".VoltaValor(PROGRAMACAO_TIPO,"DESCRICAO","MID",$campo['TIPO'],$tdb[PROGRAMACAO_TIPO]['dba'])."</td>";

        $txt.= "<td>".$resultado -> UserDate($campo['DATA_PROG'],'d/m/Y')."</td>";

        $obj=$dba[0] -> Execute("SELECT * FROM ".ORDEM_PLANEJADO_MAQ_PARADA." WHERE MID_ORDEM = '".$campo['MID']."'");
        $obj_campo = $obj -> fields;

        $tempo_parada_maq=$obj_campo['TEMPO'];

        $serv_defeito[$campo['DEFEITO']]=$serv_defeito[$campo['DEFEITO']] + $campo['TEMPO_TOTAL'];
        $para_defeito[$campo['DEFEITO']]=$para_defeito[$campo['DEFEITO']] + $obj_campo['TEMPO'];
        $qtd_defeito[$campo['DEFEITO']]++;

        $serv_setor[$campo['MID_SETOR']]=$serv_setor[$campo['MID_SETOR']] + $campo['TEMPO_TOTAL'];
        $para_setor[$campo['MID_SETOR']]=$para_setor[$campo['MID_SETOR']] + $obj_campo['TEMPO'];
        $qtd_setor[$campo['MID_SETOR']]++;

        $serv_maq[$campo['MID_MAQUINA']]=$serv_maq[$campo['MID_MAQUINA']] + $campo['TEMPO_TOTAL'];
        $para_maq[$campo['MID_MAQUINA']]=$para_maq[$campo['MID_MAQUINA']] + $obj_campo['TEMPO'];
        $qtd_maq[$campo['MID_MAQUINA']]++;

        $serv_conj[$campo['MID_CONJUNTO']]=$serv_conj[$campo['MID_CONJUNTO']] + $campo['TEMPO_TOTAL'];
        $para_conj[$campo['MID_CONJUNTO']]=$para_conj[$campo['MID_CONJUNTO']] + $obj_campo['TEMPO'];
        $qtd_conj[$campo['MID_CONJUNTO']]++;

        $serv_natu[$campo['NATUREZA']]=$serv_natu[$campo['NATUREZA']] + $campo['TEMPO_TOTAL'];
        $para_natu[$campo['NATUREZA']]=$para_natu[$campo['NATUREZA']] + $obj_campo['TEMPO'];
        $qtd_natu[$campo['NATUREZA']]++;

        if (($campo['TIPO'] == "") or ($campo['TIPO'] == 0) or ($campo['TIPO'] == 4)) {
            $serv_tipo[$campo['TIPO_SERVICO']]=$serv_tipo[$campo['TIPO_SERVICO']] + $campo['TEMPO_TOTAL'];
            $para_tipo[$campo['TIPO_SERVICO']]=$para_tipo[$campo['TIPO_SERVICO']] +$obj_campo['TEMPO'];
            $qtd_tipo[$campo['TIPO_SERVICO']]++;
        }
        else {
            $serv_tipo2[$campo['TIPO']]=$serv_tipo2[$campo['TIPO']] + $campo['TEMPO_TOTAL'];
            $para_tipo2[$campo['TIPO']]=$para_tipo2[$campo['TIPO']] + $obj_campo['TEMPO'];
            $qtd_tipo2[$campo['TIPO']]++;
        }
        $horas_parada=$horas_parada + $obj_campo['TEMPO'];
        $horas_servico=$horas_servico + $campo['TEMPO_TOTAL'];
        $tmp_tempo=explode(".",$tempo_parada_maq);
        if ((int)$tmp_tempo[0] == 0)$tmp_hora="00:";
        elseif ($tmp_tempo[0] <= 9)$tmp_hora="0".$tmp_tempo[0].":";
        else $tmp_hora=$tmp_tempo[0].":";
        $tmp_tempo="0".$tmp_tempo[1];
        $tmp_tempo=round(($tmp_tempo*60)/1000);
        if ($tmp_tempo == 0)$tmp_tempo="00";
        elseif ($tmp_tempo <= 9)$tmp_tempo="0".$tmp_tempo;

        $tempo_parada_maq=$tmp_hora."$tmp_tempo";

        $txt.= "<td>$tempo_parada_maq </td>";

        $tempo_serv=$campo['TEMPO_TOTAL'];
        $tmp_tempo=explode(".",$tempo_serv);
        if ((int)$tmp_tempo[0] == 0)$tmp_hora="00:";
        elseif ($tmp_tempo[0] <= 9)$tmp_hora="0".$tmp_tempo[0].":";
        else $tmp_hora=$tmp_tempo[0].":";
        $tmp_tempo="0".$tmp_tempo[1];
        $tmp_tempo=round(($tmp_tempo*60)/100);
        if ($tmp_tempo == 0)$tmp_tempo="00";
        elseif ($tmp_tempo <= 9)$tmp_tempo="0".$tmp_tempo;

        $tempo_serv=$tmp_hora."$tmp_tempo";

        $txt.= "<td>$tempo_serv </td>";

        $txt.= "<td>".VoltaValor(DEFEITO,"DESCRICAO","MID",$campo['DEFEITO'],0)."</td></tr>";
        $tmp_setor=$campo['MID_SETOR'];
        $resultado->MoveNext();
        $iii++;
    }
    $txt.= "</table><br>";
    
    if ($tipo == 1) $filtro=$tdb[AREAS]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 2) $filtro=$tdb[SETORES]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 3) $filtro=$tdb[MAQUINAS]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 4) $filtro=$tdb[CENTRO_DE_CUSTO]['DESC']." (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 5) $filtro="{$ling['todos']} (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 6) $filtro=$ling['rel_desc_num_ord'];
    if ($tipo == 7) $fil="{$ling['rel_desc_defeitos']} (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 8) $fil="{$ling['rel_desc_causas']} (".$_GET['datai']." a ".$_GET['dataf'].")";
    if ($tipo == 10) $fil="{$ling['rel_desc_tp_serv2']} ()";

    if ($relatorio != "") relatorio_padrao($ling['rel_carteira_serv'],$info_filtro."<ul><li>{$ling['rel_desc_periodo']}: ".$_GET['datai']." a ".$_GET['dataf']."</li></ul>",$iii,$txt,1);
    else exportar_word($ling['rel_carteira_serv'],$info_filtro."<br>{$ling['rel_desc_periodo']}: ".$_GET['datai']." a ".$_GET['dataf'],$iii,$txt,$_GET['papel_orientacao']);
}


else {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"".$manusis['url']."temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
    <script type=\"text/javascript\" src=\"".$manusis['url']."lib/javascript.js\"> </script>\n
    </head>
    <body><div id=\"central_relatorio\">
    <div id=\"cab_relatorio\">
    <h1>{$ling['rel_carteira_serv']}
    </div>
    <div id=\"corpo_relatorio\">
    <form action=\"\" name=\"form_relatoro\" id=\"form_relatorio\" method=\"GET\">
    <fieldset>
    <legend>{$ling['rel_desc_localizacoes']}</legend>
    <div id=\"filtro_relatorio\">";
    
    // FILTROS PADR�O
    FiltrosRelatorio(1, 1, 1, 1, 1);
   

    echo "</div>";
    echo "<label class=\"campo_label \" for=\"filtro_cc\">".$tdb[CENTRO_DE_CUSTO]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'',CENTRO_DE_CUSTO,$_GET['filtro_cc'],"filtro_cc","filtro_cc","MID",0, "", "", "", "", "", 'TODOS');
    
    echo "</fieldset>";
    echo "<fieldset><legend>{$ling['filtros']}</legend>";
    echo "<label class=\"campo_label \" for=\"filtro_defeito\">".$tdb[DEFEITO]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'', DEFEITO, $_GET['filtro_defeito'],"filtro_defeito","filtro_defeito","MID",0, "", "", "", "", "", 'TODOS');

    echo "<br clear=\"all\" />
    <label class=\"campo_label \" for=\"filtro_natureza\">".$tdb[NATUREZA_SERVICOS]['DESC'].":</label>";
    FormSelectD("DESCRICAO",'', NATUREZA_SERVICOS, $_GET['filtro_natureza'], "filtro_natureza", "filtro_natureza", "MID", 0, "", "", "", "", "", 'TODOS');

    echo "<br clear=\"all\" />
    <label for=\"filtro_numos\">{$ling['rel_desc_num_os2']}</label>
    <input  type=\"text\" id=\"filtro_numos\" class=\"campo_text\" value=\"".$_GET['filtro_numos']."\" name=\"filtro_numos\" size=\"9\" maxlength=\"9\" />
    </fieldset>";

    echo "<fieldset><legend>{$ling['rel_desc_tp_serv2']}</legend>";
    echo "<input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t1\" value=\"1\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=1')\" />
    <label for=\"t1\">{$ling['rel_desc_serv_sist']}</label>
    <input class=\"campo_check\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t2\" value=\"2\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=2')\" />
    <label for=\"t2\">{$ling['rel_desc_serv_nsist']}</label>
    <input class=\"campo_check\" checked=\"checked\" type=\"radio\" name=\"filtro_tipo_serv\" id=\"t3\" value=\"3\" onchange=\"atualiza_area2('serv','../parametros.php?id=4&tipo=3')\" />
    <label for=\"t3\">{$ling['rel_desc_todos']}</label>
    <div id=\"serv\"></div>
    </fieldset>";
    echo "<fieldset><legend>{$ling['rel_desc_status_ordem']}</legend>";
    echo "<input class=\"campo_check\" type=\"radio\" name=\"st_ordem\" id=\"st1\" value=\"1\"/>
    <label for=\"st1\">{$ling['rel_desc_abertas2']}</label>
    <input class=\"campo_check\" type=\"radio\" name=\"st_ordem\" id=\"st2\" value=\"2\"/>
    <label for=\"st2\">{$ling['rel_desc_fechadas2']}</label>
    <input class=\"campo_check\" type=\"radio\" name=\"st_ordem\" id=\"st3\" value=\"3\"/>
    <label for=\"st3\">{$ling['rel_desc_canceladas2']}</label>
    <input class=\"campo_check\" checked=\"checked\" type=\"radio\" name=\"st_ordem\" id=\"st9\" value=\"9\ />
    <label for=\"st9\">{$ling['rel_desc_todas2']}</label>
    </fieldset>";
    echo "<fieldset><legend>{$ling['rel_desc_periodo']}</legend>";
    FormData($ling['data_inicio'],"datai",$_GET['datai'],"campo_label");
    echo "<br clear=\"all\" />";
    FormData($ling['data_fim'],"dataf",$_GET['dataf'],"campo_label");
    echo "
    </fieldset>";
    echo "<fieldset>
    <legend>".$ling['papel_orientacao']."</legend>
    <input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"1\" id=\"papel_retrato\" />
    <label for=\"papel_retrato\">".$ling['papel_retrato']."</label>
    <br clear=\"all\" />
    <input class=\"campo_check\" type=\"radio\" name=\"papel_orientacao\" value=\"2\" id=\"papel_paisagem\" checked=\"checked\" />
    <label for=\"papel_paisagem\">".$ling['papel_paisagem']."</label>
    </fieldset>
    <br />
    <input type=\"hidden\" name=\"tb\" value=\"$tb\" />
    <input class=\"botao\" type=\"submit\" name=\"relatorio\" value=\"".$ling['relatorio_html']."\" />
    <input class=\"botao\" type=\"submit\" name=\"exword\" value=\"".$ling['relatorio_doc']."\" />
    </form><br />
    </div>
    </div>
    </body>
    </html>";

}
?>
