<?
/**
* Manusis 3.0
* Autor: Mauricio Blackout <blackout@firstidea.com.br>
* Nota: Arquivo para Formularios
*/
// Variaveis de direcionamento
$st=(int)$_GET['st'];
$id=(int)$_GET['id']; // Modulo
$op=(int)$_GET['op']; // Operação do modulo
$exe=(int)$_GET['exe'];
$act=(int)$_GET['act'];
$form_recb_valor=$_GET['form_recb_valor'];
$atualiza=$_GET['atualiza'];
$foq=(int)$_GET['foq'];

// Funções do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Autentificação
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Formulários
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $fpos = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $$fpos, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);

    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
if ($id == 3) {
    if (($_GET['oq'] == 1) and ($_GET['esp'] != "") and ($_GET['tempo'] != "") and ($_GET['qtde'] != "")) {
        $_SESSION[ManuSess]['progra']['mo'][] = array($_GET['esp'], (float) str_replace(',', '.', $_GET['tempo']),(int)$_GET['qtde']);
    }
    if ($_GET['oq'] == 2) {
        unset($_SESSION[ManuSess]['progra']['mo'][$_GET['del']]);
        rsort($_SESSION[ManuSess]['progra']['mo']);
    }

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <th>".$ling['especialidade_']."</th>
    <th>".$ling['Qtde'].".</th>
    <th>".$ling['tempo']." (h)</th>
    <th> </th>
    </tr>";
    
    if (is_array($_SESSION[ManuSess]['progra']['mo'])) {
        foreach ($_SESSION[ManuSess]['progra']['mo'] as $ek => $emo) {
            echo "<tr class=\"cor2\">
            <td>" . htmlentities(VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$emo[0],0)) . "</td>
            <td>" . $emo[2] . "</td>
            <td>" . $emo[1] . "</td>
            <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('gravamo','sol.php?id=3&oq=2&del=".$ek."')\" />
            </td></tr>";
        }
    }
    
    echo "</table>";
}

if ($id == 5) {
    if (($_GET['oq'] == 1) and ($_GET['mat'] != "") and ($_GET['qtd'] != "")) {
        $_SESSION[ManuSess]['progra']['ma'][]= array((int)$_GET['mat'], (float) str_replace(',', '.', $_GET['qtd']));
    }
    if ($_GET['oq'] == 2) {
        unset($_SESSION[ManuSess]['progra']['ma'][$_GET['del']]);
        rsort($_SESSION[ManuSess]['progra']['ma']);
    }

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <th>".$ling['prog_material']."</th>
    <th>".$ling['qtd']."</th>
    <th> </th>
    </tr>";
    
    if (is_array($_SESSION[ManuSess]['progra']['ma'])) {
        foreach ($_SESSION[ManuSess]['progra']['ma'] as $ek => $ema) {
            $mat=htmlentities(VoltaValor(MATERIAIS,"DESCRICAO","MID",$ema[0],0));
            $qtd=$ema[1];
            echo "<tr class=\"cor2\">
            <td>$mat</td>
            <td>$qtd</td>
            <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('gravama','sol.php?id=5&oq=2&del=".$ek."')\" />
            </td></tr>";
        }
    }
    echo "</table>";
}
if ($id == "") {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
    <script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
    echo "</head>
    <body class=\"body_form\">
    <div id=\"formularioos\">";

    if ($_POST['env_form'] != "")  {
        $msg = '';

        $tipo=(int)$_POST['tipo'];
        $plano=(int)$_POST['plano'];
        $hora=$_POST['hora'];
        if (!$hora) $hora="00:00:00";
        if (!ChecaHora($hora)) $msg = $ling['hora_invalida'];
        $datai=$_POST['datai'];
        $di=explode("/",$datai);
        $datais=$di[2]."-".$di[1]."-".$di[0];
        $maq=$_POST['maq'];
        $conj=$_POST['conj'];
        $midsol=$_POST['midsol'];
        $cc=$_POST['cc'];
        $texto = $_POST['texto'];
        
        if ($_POST['datai'] == "") {
            $msg = "<li>".$ling['preencha_o_campo_p']."</li>";
        }
        elseif (! checkdate($di[1], $di[0], $di[2])){
            $msg = "<li>".$ling['preencha_corretamente_campo_p'].".</li>";
        }
        

        if (!$msg) {
            // MONTANDO O TEXTO
            $recomendacoes_texto  = mb_strtoupper(unhtmlentities($tdb[SOLICITACOES]['DESCRICAO'])) . ": " . VoltaValor(SOLICITACOES, 'DESCRICAO', 'MID', $midsol) . "\n";
            $recomendacoes_texto .= mb_strtoupper(unhtmlentities($tdb[SOLICITACOES]['TEXTO'])) . ": " . VoltaValor(SOLICITACOES, 'TEXTO', 'MID', $midsol) . "\n";
            $recomendacoes_texto .= unhtmlentities($ling['recompensacoes_m']) . ": ".strtoupper(LimpaTexto($_POST['texto']))."";
            
            // GERANDO A OS
            $osmid = gera_os(0, $datais, 0, $midsol);
            if ($osmid) {
                // Salvando MO
                if (is_array($_SESSION[ManuSess]['progra']['mo'])) {
                    foreach ($_SESSION[ManuSess]['progra']['mo'] as $mo) {
                        $mo_mid = GeraMid(ORDEM_MO_PREVISTO,"MID",0);
                        $sql = "INSERT INTO ".ORDEM_MO_PREVISTO." (MID, MID_ORDEM, MID_ESPECIALIDADE, QUANTIDADE, TEMPO) VALUES ($mo_mid, $osmid, " . $mo[0] . ", ". $mo[2] ."," . $mo[1] . ")";
                        $ins_mo = $dba[0] ->Execute($sql);
                    }
                }
                if (is_array($_SESSION[ManuSess]['progra']['ma'])) {
                    foreach ($_SESSION[ManuSess]['progra']['ma'] as $ma) {
                        $ma_mid=GeraMid(ORDEM_MAT_PREVISTO,"MID",0);
                        $sql="INSERT INTO ".ORDEM_MAT_PREVISTO." (MID, MID_ORDEM, MID_MATERIAL, QUANTIDADE) VALUES ($ma_mid, $osmid, " . $ma[0] . ", " . $ma[1] . ")";
                        $ins_ma=$dba[0] ->Execute($sql);
                    }
                }
                
                // Mudando status da solicitação
                $dba[0] -> Execute("UPDATE ".SOLICITACOES." SET STATUS = '1' WHERE MID = '$midsol'");
                logar(4, $ling['sol_aprovando'], $midsol);

                unset($_SESSION[ManuSess]['progra']['ma']);
                unset($_SESSION[ManuSess]['progra']['mo']);
                die("<br /> <br /> <br /> <br /><p align=center>{$ling['sol_gerada_sucesso']}</p><br /> <br /> <br /> <br /> <br /><script>atualiza_area_frame('corpo','manusis.php?st=1&id=4&op=1')</script>");
            }
            else {
                erromsg($ling['err22']);
            }
        }
        else {
            erromsg("<ul>$msg</ul>");
            $_POST['env_form'] = '';
        }

    }
    else {
        $datai = date("d/m/Y");
    }

    $sql="SELECT *  FROM ".SOLICITACOES." WHERE MID = $foq";
    $re=$dba[0] -> Execute($sql);
    $ca=$re->fields;

    echo "<form name=\"form\" method=\"POST\" action=\"\">
    <input type=\"hidden\" name=\"midsol\" value=\"$foq\" />
    <input type=\"hidden\" name=\"maq\" value=\"".$ca['MID_MAQUINA']."\" />
    <input type=\"hidden\" name=\"conj\" value=\"".$ca['MID_CONJUNTO']."\" />";
    
    echo "<fieldset><legend>{$ling['sol_programacao']}</legend>";

    FormData("{$ling['sol_prazo']}", "datai", $datai, "campo_label", "", "campo_text_ob");
    echo "<br clear=\"all\">
    
    <label  class=\"campo_label\" for=\"hora\">".$ling['horario']."</label>
    <input onkeypress=\"return ajustar_hora(this, event)\" class=\"campo_text\" type=\"text\" id=\"hora\" name=\"hora\" size=\"8\" maxlength=\"10\" value=\"$hora\" />
    <br clear=\"all\" />
    
    <label  class=\"campo_label\" for=\"texto\">".$ling['recompensacoes']."</label>
    <textarea row=\"10\" cols=\"50\" class=\"campo_text\" name=\"texto\" id=\"texto\" />". $texto . "</textarea>
    
    </fieldset>
    
    <div id=\"obj\"></div>
    </fieldset>";

    echo "<fieldset><legend>".$ling['aloca_maodeobra']."</legend>
    
    <label class=\"campo_label\" for=\"esp\">".$ling['especialidade_']."</label>";
    FormSelectD("DESCRICAO", "", ESPECIALIDADES, '', 'esp', 'esp', 'MID', 'ESPECIALIDADES');
    echo "<br clear=\"all\"/>
    
    <label for=\"tempo\" class=\"campo_label\">".$ling['qtd']."</label>
    <input type=\"text\" id=\"qtde_tp\" class=\"campo_text\" name=\"qtde_tp\" size=\"5\" />
    <br clear=\"all\"/>
    
    <label for=\"tempo\" class=\"campo_label\">".$ling['tempo']." (h)</label>
    <input type=\"text\" id=\"tempo\" class=\"campo_text\" name=\"tempo\" size=\"5\" />
    
    <input type=\"button\" class=\"botao\" value=\"".$ling['adicionar']."\" onclick=\"atualiza_area2('gravamo','sol.php?id=3&oq=1&qtde=' + document.getElementById('qtde_tp').value +  '&tempo=' + document.getElementById('tempo').value +  '&esp=' + document.getElementById('esp').value)\" />
    
    <br clear=\"all\"/>
    
    <div id=\"gravamo\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <th>".$ling['especialidade_']."</th>
    <th>".$ling['Qtde'].".</th>
    <th>".$ling['tempo']." (h)</th>
    <th> </th>
    </tr>";
    
    if (is_array($_SESSION[ManuSess]['progra']['mo'])) {
        foreach ($_SESSION[ManuSess]['progra']['mo'] as $ek => $emo) {
            echo "<tr class=\"cor2\">
            <td>" . htmlentities(VoltaValor(ESPECIALIDADES,"DESCRICAO","MID",$emo[0],0)) . "</td>
            <td>" . $emo[2] . "</td>
            <td>" . $emo[1] . "</td>
            <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('gravamo','sol.php?id=3&oq=2&del=".$ek."')\" />
            </td></tr>";
        }
    }
    
    echo "</table>
    </div>
    </fieldset>";
    
    echo "<fieldset><legend>".$ling['aloca_material']."</legend>
        
    <label class=\"campo_label\" for=\"mat\">{$ling['rel_desc_material']}</label>";
    FormSelectD('COD', 'DESCRICAO', MATERIAIS, '', 'mat', 'mat', 'MID', 'MATERIAIS');
    echo "<br clear=\"all\" />
    
    <label  class=\"campo_label\" for=\"qtd_mat\">{$ling['qtd']}</label>
    <input class=\"campo_text\" type=\"text\" id=\"qtd_mat\" name=\"qtd_mat\" size=\"5\" />
    
    <input class=\"botao\" type=\"button\" name=\"fadd\" value=\"".$ling['adicionar']."\" onclick=\"atualiza_area2('gravama','sol.php?id=5&oq=1&mat=' + document.getElementById('mat').value + '&qtd=' + document.getElementById('qtd_mat').value)\" />
    
    <br clear=\"all\"/> 
    
    <div id=\"gravama\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <th>".$ling['prog_material']."</th>
    <th>".$ling['qtd']."</th>
    <th> </th>
    </tr>";
        
    if (is_array($_SESSION[ManuSess]['progra']['ma'])) {
        foreach ($_SESSION[ManuSess]['progra']['ma'] as $ek => $ema) {
            $mat=VoltaValor(MATERIAIS,"DESCRICAO","MID",$ema[0],0);
            $qtd=$ema[1];
            echo "<tr class=\"cor2\">
            <td>$mat</td>
            <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('gravama','sol.php?id=5&oq=2&del=".$ek."')\" />
            </td></tr>";
        }
    }
    echo "</table>";

    echo "</div>
    </fieldset>
    <br clear=\"all\" />
    <center><input type=\"submit\" class=\"botao\" name=\"env_form\" value=\"{$ling['sol_enviar_form']}\" /> </center>
    </form></div>
    </body>
    </html>";
}
?>
