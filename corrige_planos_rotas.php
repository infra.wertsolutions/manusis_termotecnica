<?
/**
* Corpo do Manusis - Arquivo geral que administra o fluxo das informações
*
* @author  Mauricio Barbosa <mauricio@manusis.com.br>
* @version  3.0
* @package engine
*/

// Funções do Sistema
if (!require("lib/mfuncoes.php")) {
    die ($ling['arq_estrutura_nao_pode_ser_carregado']);
}
// Configurações
elseif (!require("conf/manusis.conf.php")) {
    die ($ling['arq_configuracao_nao_pode_ser_carregado']);
}
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) {
    die ($ling['arq_idioma_nao_pode_ser_carregado']);
}
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) {
    die ($ling['bd01']);
}
// Informações do banco de dados
elseif (!require("lib/bd.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/delcascata.php")) {
    die ($ling['bd01']);
}
elseif (!require("lib/forms.php")) {
    die ($ling['autent01']);
}
// Modulos
elseif (!require("conf/manusis.mod.php")) {
    die ($ling['mod01']);
}
// Caso não exista um padrão definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) {
    $manusis['tema']="padrao";
}

$sql = "SELECT * FROM " . PLANO_ROTAS ." WHERE MID = 1";
$rs = $dba[$tdb[PLANO_ROTAS]['dba']]->execute($sql);
$i = 0;

$rel = "\"EMPRESA PLANO\";\"PLANO\";\"ATIVIDADE\";\"OBJETO MANUTENCAO\";\"EMPRESA OBJ\";\"O.S\";\n";

$OSalteradas = array();
while(!$rs->EOF){	
   
	$rotas = $rs->fields;


	//Atividades do plano
	$sql = "SELECT * FROM " . LINK_ROTAS . " WHERE MID_PLANO = {$rotas['MID']} ";
	$rsA = $dba[$tdb[LINK_ROTAS]['dba']]->execute($sql);
	if(!$rsA){
		errofatal($dba[$tdb[LINK_ROTAS]['dba']]->ErrorMsg()."<br />{$sql}");
	}
	
	while(!$rsA->EOF){
		
		$liksRotas = $rsA->fields;

		
		$midEmpresaObj = VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', $liksRotas['MID_MAQUINA'], 0);
		
			if($midEmpresaObj != $rotas['MID_EMPRESA']){echo "<br />" .$liksRotas['MID_MAQUINA'];
				#echo "<br />{$rotas['DESCRICAO']}";
				#echo "<br />{$liksRotas['TAREFA']} - {$rotas['MID_EMPRESA']} -> {$midEmpresaObj}";
				
				$empresaRota = VoltaValor(EMPRESAS, 'NOME', 'MID', $rotas['MID_EMPRESA'], 0);
				$empresaObj = VoltaValor(EMPRESAS, 'NOME', 'MID', $midEmpresaObj, 0);
				$midOrdem = VoltaValor(ORDEM_LUB, 'MID_ORDEM', 'MID_ATV', $liksRotas['MID'], 0);
				$numOs 	  = VoltaValor(ORDEM, 'NUMERO', 'MID', $midOrdem, 0);
				$statusOS	  = VoltaValor(ORDEM, 'STATUS', 'MID', $midOrdem, 0);
				$codObj	  = VoltaValor(MAQUINAS, 'COD', 'MID', $liksRotas['MID_MAQUINA'], 0);
				$objManut = VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $liksRotas['MID_MAQUINA'], 0);
				$i++;
				$rel .= "\"{$empresaRota}\";\"{$rotas['DESCRICAO']}\";\"{$liksRotas['TAREFA']}\";\"{$codObj}-{$objManut}\";\"{$empresaObj}\";\"{$numOs}\";\n"; 
				if($statusOS == 1){
					$OSalteradas[$midOrdem] = $midOrdem;
				}
				
				    $sql = "DELETE FROM " . LINK_ROTAS . " WHERE MID = {$liksRotas['MID']}";
					$rsLinkRotas = $dba[$tdb[LINK_ROTAS]['dba']]->execute($sql);			
					if(!$rsLinkRotas){
						errofatal($dba[$tdb[LINK_ROTAS]['dba']]->ErrorMsg()."<br />{$sql}");
					}
				
				if($midOrdem){			
							
					$sql = "DELETE FROM " . ORDEM_LUB . " WHERE MID_ATV = {$liksRotas['MID']}";
					$rsLub = $dba[$tdb[ORDEM_LUB]['dba']]->execute($sql);			
					if(!$rsLub){
						errofatal($dba[$tdb[ORDEM_LUB]['dba']]->ErrorMsg()."<br />{$sql}");
					}
					  
					$sql = "DELETE FROM " . PENDENCIAS . " WHERE MID_ORDEM  = {$midOrdem} AND MID_MAQUINA = {$liksRotas['MID_MAQUINA']}";
					$rsPen = $dba[$tdb[PENDENCIAS]['dba']]->execute($sql);			
					if(!$rsPen){
						errofatal($dba[$tdb[PENDENCIAS]['dba']]->ErrorMsg()."<br />{$sql}");
					}
				}
			}			
		
				
		$rsA->moveNext();
	}		
	$rs->moveNext();
}

foreach($OSalteradas as $osmid){
	
	$sql = "DELETE FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid";
	if (! $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute($sql)) {
		erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $sql);
	}

	$sql = "DELETE FROM " . ORDEM_MAT_PREVISTO . " WHERE MID_ORDEM = $osmid";
	if (! $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute($sql)) {
		erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $sql);
	}

	$sql = "SELECT ESPECIALIDADE, TEMPO_PREVISTO, QUANTIDADE_MO, MID_MATERIAL, QUANTIDADE FROM " . ORDEM_LUB . " WHERE MID_ORDEM = $osmid";
	$dba_atv = $tdb[ORDEM_LUB]['dba'];

	if (! $rs = $dba[$dba_atv] -> Execute($sql)) {
		erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$dba_atv] ->ErrorMsg() . "<br />" . $sql);
	}

	// SALVANDO AS QUANTIDADES
	$mo_prev = array();
	$mt_prev = array();
	while (! $rs -> EOF) {
		if (! $mo_prev[$rs -> fields['ESPECIALIDADE']]) {
			$mo_prev[$rs -> fields['ESPECIALIDADE']] = 0;
		}

		$mo_prev[$rs -> fields['ESPECIALIDADE']] += $rs -> fields['TEMPO_PREVISTO'] * $rs -> fields['QUANTIDADE_MO'];

		// Tem Material na Atividade
		if ($rs -> fields['MID_MATERIAL'] != 0) {
			if (! $mt_prev[$rs -> fields['MID_MATERIAL']]) {
				$mt_prev[$rs -> fields['MID_MATERIAL']] = 0;
			}

			$mt_prev[$rs -> fields['MID_MATERIAL']]  += $rs -> fields['QUANTIDADE'];
		}
		$rs -> MoveNext();
	}
	// MÃO DE OBRA
	foreach ($mo_prev as $esp => $tempo) {
		$ins_mo = "INSERT INTO " . ORDEM_MO_PREVISTO . " (MID_ORDEM, MID_ESPECIALIDADE, TEMPO, QUANTIDADE) VALUES ($osmid, $esp, $tempo, 0)";

		if (! $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] -> Execute($ins_mo)) {
			erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $ins_mo);
		}
	}
	// MATERIAIS
	foreach ($mt_prev as $mat => $quant) {
		$ins_mt = "INSERT INTO " . ORDEM_MAT_PREVISTO . " (MID_ORDEM, MID_MATERIAL, QUANTIDADE) VALUES ($osmid, $mat, $quant)";

		if (! $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] -> Execute($ins_mt)) {
			erromsg("Arquivo: " . __FILE__ . "<br />Funcao: " . __FUNCTION__ . "<br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']] ->ErrorMsg() . "<br />" . $ins_mt);
		}
	}
}

echo $rel;
echo "<br />TOTAL $i";
