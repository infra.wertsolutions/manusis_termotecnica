<?

/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Autor: Fernando Cosentino
 * Nota: Arquivo para Formularios
 */
// Variaveis de direcionamento
$st = (int) $_GET['st'];
$id = (int) $_GET['id']; // Modulo
if ($id == 0)
    $id = 1;
$op = (int) $_GET['op']; // Operação do modulo
$exe = (int) $_GET['exe'];
$act = (int) $_GET['act'];
$form_recb_valor = $_GET['form_recb_valor'];
$atualiza = $_GET['atualiza'];
$foq = (int) $_GET['foq'];
$idModulo = (int) $_GET['idModulo'];

// Busca por numero de OS
if ($_GET['ap'] != 0) {
    $osmid = trim($_GET['osmid']);
}
// Apontamento de OS
else {
    $osmid = (int) $_GET['osmid'];
}


// Funções do Sistema
if (!require("lib/mfuncoes.php"))
    die($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php"))
    die($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/" . $manusis['idioma'][0] . ".php"))
    die($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php"))
    die($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php"))
    die($ling['bd01']);
elseif (!require("lib/autent.php"))
    die($ling['bd01']);
elseif (!require("lib/delcascata.php"))
    die($ling['bd01']);
// Formulários
elseif (!require("lib/forms.php"))
    die($ling['bd01']);
// Modulos
elseif (!require("conf/manusis.mod.php"))
    die($ling['mod01']);

if ($id <= 9) {
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"" . $ling['xml'] . "\">
    <head>
     <meta http-equiv=\"pragma\" content=\"no-cache\" />
    <title>{$ling['manusis']}</title>
    <link href=\"temas/" . $manusis['tema'] . "/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"" . $manusis['tema'] . "\" />
    <script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n
    <script type=\"text/javascript\" src=\"lib/jquery/jquery-1.4.2.min.js\"> </script>
    <script type=\"text/javascript\" src=\"lib/jquery/jquery.livequery.js\"> </script>
    <script type=\"text/javascript\" src=\"lib/jquery/jquery.manusis.js\"> </script>";
    echo "</head>
    <body class=\"body_form\">

    <div id=\"formularioos\">";
}

if ($id == 11) {
    $maq = (int) $_GET['maq'];
    echo "<br clear=\"all\" /><label class=\"campo_label \"for=\"cc[MID_CONJUNTO]\">" . $tdb[ORDEM]['MID_CONJUNTO'] . ":</label>";
    FormSelectD('TAG', 'DESCRICAO', MAQUINAS_CONJUNTO, $cc['MID_CONJUNTO'], "cc[MID_CONJUNTO]", "cc[MID_CONJUNTO]", 'MID', '', '', "atualiza_area2('equip','apontaosplan.php?osmid=$osmid&id=12&pos=' + this.options[this.selectedIndex].value)", "WHERE MID_MAQUINA ='$maq'", 'A', 'TAG', "");
    exit;
}

if ($id == 111) {
    $set = (int) $_GET['set'];
    echo "<br clear=\"all\" /><label class=\"campo_label\"for=\"cc[MID_MAQUINA]\">" . $tdb[ORDEM]['MID_MAQUINA'] . ":</label>";
    FormSelectD('COD', 'DESCRICAO', MAQUINAS, $cc['MID_MAQUINA'], "cc[MID_MAQUINA]", "cc[MID_MAQUINA]", 'MID', '', 'campo_select_ob', "atualiza_area2('pos','apontaosplan.php?osmid=$osmid&id=11&maq=' + this.options[this.selectedIndex].value)", "WHERE MID_SETOR ='$set'", 'A', 'COD', "");
    exit;
}

if ($id == 12) {
    $pos = (int) $_GET['pos'];
    echo "<br clear=\"all\" /><label class=\"campo_label \"for=\"cc[MID_EQUIPAMENTO]\">" . $tdb[ORDEM]['MID_EQUIPAMENTO'] . ":</label>";
    FormSelectD('COD', 'DESCRICAO', EQUIPAMENTOS, $cc['MID_EQUIPAMENTO'], "cc[MID_EQUIPAMENTO]", "cc[MID_EQUIPAMENTO]", 'MID', '', '', "", "WHERE MID_CONJUNTO ='$pos' AND MID_STATUS = 1", 'A', 'COD', "");
    exit;
}


if ($id == 13) {
    // Equipe selecionada
    $emp = (int) $_GET['emp'];
    $equip = (int) $_GET['equip'];
    $equip2 = (int) $_GET['equip2'];

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <td>" . $tdb[ORDEM_MO_ALOC]['MID_FUNCIONARIO'] . "</td>
    <td>" . $tdb[FUNCIONARIOS]['ESPECIALIDADE'] . "</td>
    <td>" . $tdb[FUNCIONARIOS]['EQUIPE'] . "</td>
    <td>" . $tdb[ORDEM_MO_ALOC]['MID_ORDEM'] . "</td>
    <td>" . $tdb[ORDEM_MO_ALOC]['TEMPO'] . "</td>
    </tr>";

    // Buscando todos os funcionarios DA EMPRESA
    $fil_emp = " AND MID_EMPRESA = $emp";

    if ($equip2 != 0) {
        $fil_emp .= " AND EQUIPE = $equip2";
    }

    $tipo = VoltaValor(ORDEM, 'TIPO', 'MID', $osmid);
    if (($tipo == 1) or ($tipo == 2)) {
        $sql = "SELECT MID, NOME, ESPECIALIDADE, EQUIPE FROM " . FUNCIONARIOS . " WHERE ESPECIALIDADE IN (SELECT MID_ESPECIALIDADE FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid) AND SITUACAO = 1 AND FORNECEDOR = 0 $fil_emp ORDER BY NOME";
    } else {
        $sql = "SELECT MID, NOME, ESPECIALIDADE, EQUIPE FROM " . FUNCIONARIOS . " WHERE SITUACAO = 1 AND FORNECEDOR = 0 $fil_emp ORDER BY NOME";
    }

    if (!$rs = $dba[$tdb[FUNCIONARIOS]['dba']]->Execute($sql)) {
        erromsg("{$ling['arquivo']}: " . __FILE__ . " <br />{$ling['linha']}: " . __LINE__ . " <br />" . $dba[$tdb[FUNCIONARIOS]['dba']]->ErrorMsg() . " <br />" . $sql);
    }
    while (!$rs->EOF) {
        // Buscando OS aberta em que o funcionario possa estar alocado
        $sql = "SELECT O.MID, O.NUMERO, O.MID_MAQUINA FROM " . ORDEM . " O, " . ORDEM_MO_ALOC . " A WHERE O.MID = A.MID_ORDEM AND O.MID != $osmid AND O.STATUS = 1 AND O.DATA_PROG = '" . DataSQL($cc['DATA_PROG']) . "' AND A.MID_FUNCIONARIO = {$rs->fields['MID']} ORDER BY O.NUMERO DESC";
        if (!$rsf = $dba[$tdb[ORDEM]['dba']]->SelectLimit($sql, 1, 0)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . " <br />{$ling['linha']}: " . __LINE__ . " <br />" . $dba[$tdb[FUNCIONARIOS]['dba']]->ErrorMsg() . " <br />" . $sql);
        }
        if (!$rsf->EOF) {
            $func_osm = $rsf->fields['MID'];
            $func_os = $rsf->fields['NUMERO'];
            $func_cli = htmlentities(VoltaValor(MAQUINAS, "DESCRICAO", "MID", $rsf->fields['MID_MAQUINA'], 0));
        } else {
            $func_osm = "";
            $func_os = "";
            $func_cli = "";
        }

        $check = "";
        if (VoltaValor(ORDEM_MO_ALOC, 'MID', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $rs->fields['MID'], 0) != "") {
            $check = "checked=\"checked\"";
        }

        // Mostrando tempo
        $tempo = VoltaValor(ORDEM_MO_ALOC, 'TEMPO', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $rs->fields['MID'], 0);

        // CARGO
        $esp = (int) VoltaValor(ORDEM_MO_ALOC, 'MID_ESPECIALIDADE', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $rs->fields['MID'], 0);

        if ($esp == 0) {
            $esp = $rs->fields['ESPECIALIDADE'];
        }

        // MOSTRANDO APENAS OS SELECIONADOS
        if (($check != '') or ($equip == $rs->fields['EQUIPE']) or ($equip == 0)) {
            // Destacando os que não são dessa equipe
            $cor = "";
            if (($equip != $rs->fields['EQUIPE']) and ($equip != 0)) {
                $cor = "style=\"background-color:#ffffc0\"";
            }

            echo "<tr class=\"cor2\" $cor>
            <td><input type=\"checkbox\" name=\"cc[FUNC_ALOC][{$rs->fields['MID']}]\" id=\"func_{$rs->fields['MID']}\" value=\"{$rs->fields['MID']}\" onclick=\"ajax_get('apontaosplan.php?id=14&osmid=$osmid&func={$rs->fields['MID']}&st=' + this.checked + '&tempo=' + document.getElementById('cc[FUNC_ALOC_TMP][{$rs->fields['MID']}]').value)\" $check /><label for=\"func_{$rs->fields['MID']}\">" . htmlentities($rs->fields['NOME']) . "</label></td>
            <td>" . htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $esp)) . "</td>
            <td>" . htmlentities(VoltaValor(EQUIPES, 'DESCRICAO', 'MID', $rs->fields['EQUIPE'])) . "</td>
            <td><a href=\"javascript:janela('detalha_ord.php?busca=$func_osm', 'parm', 500,400)\" title=\"$func_cli\">$func_os</a></td>
            <td><input type=\"text\" size=\"5\" maxlength=\"10\" class=\"campo_text\" id=\"cc[FUNC_ALOC_TMP][{$rs->fields['MID']}]\" name=\"cc[FUNC_ALOC_TMP][{$rs->fields['MID']}]\" value=\"" . $tempo . "\" onblur=\"ajax_get('apontaosplan.php?id=14&osmid=$osmid&func={$rs->fields['MID']}&st=' + document.getElementById('func_{$rs->fields['MID']}').checked + '&tempo=' + this.value)\" /></td>
            </tr>";
        }
        $rs->MoveNext();
    }

    echo "</table>";
    exit();
}

if ($id == 14) {
    // SALVANDO NO MINIMO O FUNCIONARIO COMO ALOCADO
    $st = $_GET['st'];
    $func = (int) $_GET['func'];
    $tempo = (float) str_replace(',', '.', $_GET['tempo']);

    if (($st == 'TRUE') and ($func != 0) and ($osmid != 0)) {
        $dados = array(
            'MID' => (int) VoltaValor(ORDEM_MO_ALOC, 'MID', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $func, 0),
            'MID_ORDEM' => $osmid,
            'MID_FUNCIONARIO' => $func,
            'MID_ESPECIALIDADE' => (int) VoltaValor(FUNCIONARIOS, 'ESPECIALIDADE', 'MID', $func, 0),
            'TEMPO' => $tempo
        );

        if ($dados['MID'] == 0) {
            $dados['MID'] = GeraMid(ORDEM_MO_ALOC, "MID", $tdb[ORDEM_MO_ALOC]['dba']);
            $sql = "INSERT INTO " . ORDEM_MO_ALOC . " (" . implode(", ", array_keys($dados)) . ") VALUES (" . implode(", ", array_values($dados)) . ")";
        } else {
            $sql = "UPDATE " . ORDEM_MO_ALOC . " SET TEMPO = {$dados['TEMPO']} WHERE MID = {$dados['MID']}";
        }

        if (!$dba[$tdb[ORDEM_MO_ALOC]['dba']]->Execute($sql)) {
            erromsg("{$ling['arquivo']}: apontaosplan.php <br />id == 14 <br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']]->ErrorMsg() . "<br />" . $sql);
            $erro = 1;
        }
    } elseif (($st == 'FALSE') and ($func != 0)) {
        DeletaItem(ORDEM_MO_ALOC, array('MID_ORDEM', 'MID_FUNCIONARIO'), array($osmid, $func));
    }

    exit();
}

// Materiais
if ($id == 15) {
    $osmid = (int) $_GET['osmid'];
    
    if ($_GET['add']) {
        $mid_mat = (int) $_GET['mid_mat'];
        $quant = round((float) str_replace(',', '.', $_GET['quant']), 2);
        $quant = str_replace(',', '.', $quant);
        $custo = round((str_replace(',', '.', VoltaValor(MATERIAIS, "CUSTO_UNITARIO", "MID", $mid_mat, 0))) * $quant, 2);
        $custo = str_replace(',', '.', $custo);

        if (($mid_mat != 0) and ($quant != 0)) {

            $dados = array(
                'MID_MATERIAL' => $mid_mat,
                'QUANTIDADE' => $quant,
                'CUSTO' => $custo
            );
            
            if($osmid){
                $dados['MID_ORDEM'] = $osmid;
                $sql = "insert into " . ORDEM_MAT_PREVISTO . "(" . join(',', array_keys($dados)) . ") VALUES ( " . join(',', array_values($dados)) . ");";
            
                if (!$dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->Execute($sql)) {
                    erromsg("{$ling['arquivo']}: apontaosplan.php <br />id == 15 <br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
                }                
            }else{
                if(!isset($_SESSION[ManuSess][ORDEM]['mt']))
                    $_SESSION[ManuSess][ORDEM]['mt'] = array();
                $_SESSION[ManuSess][ORDEM]['mt'][] = $dados;
            }
        }
    }

    if ($_GET['del']) {
        $del = (int) $_GET['del'];
        if($osmid)
            $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->execute("delete from " . ORDEM_MAT_PREVISTO . " where MID = '{$del}'");
         else             
             unset($_SESSION[ManuSess][ORDEM]['mt'][$del-1]);         
    }

    

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <td align=\"left\">" . $tdb[ORDEM_MATERIAL]['MID_MATERIAL'] . "</td>
    <td align=\"left\">{$tdb[ORDEM_MATERIAL]['QUANTIDADE']}</td>
    <td align=\"left\">{$tdb[ORDEM_MATERIAL]['CUSTO']}</td>
    <td>&nbsp;</td>
    </tr>";       

    if($osmid){
        $rs = $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->execute("select * from " . ORDEM_MAT_PREVISTO . " where MID_ORDEM = '{$osmid}'");
        while (!$rs->EOF) {
            $mt = $rs->fields;
            // Unidade
            $uni = VoltaValor(MATERIAIS, "UNIDADE", "MID", $mt['MID_MATERIAL'], 0);
            echo "<tr class=\"cor2\">
            <td align=\"left\">" . htmlentities(VoltaValor(MATERIAIS, "DESCRICAO", "MID", $mt['MID_MATERIAL'], 0)) . "</td>
            <td align=\"left\">" . $mt['QUANTIDADE'] . " " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", $uni, 0)) . "</td>
            <td align=\"left\">" . number_format($mt['CUSTO'], 2, ',', '.') . "</td>
            <td><a href=\"javascript: atualiza_area2('lista_mt', 'apontaosplan.php?osmid=$osmid&id=15&del={$mt['MID']}')\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" /></a></td>
            </tr>";
            $rs->MoveNext();
        }
    }else{
        if(isset($_SESSION[ManuSess][ORDEM]['mt']))
          foreach ($_SESSION[ManuSess][ORDEM]['mt'] as $i => $mt) {
                $del = $i + 1;
                // Unidade
                $uni = VoltaValor(MATERIAIS, "UNIDADE", "MID", $mt['MID_MATERIAL'], 0);

                echo "<tr class=\"cor2\">
        <td align=\"left\">" . htmlentities(VoltaValor(MATERIAIS, "DESCRICAO", "MID", $mt['MID_MATERIAL'], 0)) . "</td>
        <td align=\"left\">" . $mt['QUANTIDADE'] . " " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", $uni, 0)) . "</td>
        <td align=\"left\">" . number_format($mt['CUSTO'], 2, ',', '.') . "</td>
        <td><a href=\"javascript: atualiza_area2('lista_mt', 'apontaosplan.php?osmid=$osmid&id=15&del=$del')\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" /></a></td>
        </tr>";
        }                
    }
    echo "</table>";
    exit();
}

if ($id == 16) {

    $midEquip = (int) $_GET['midEquip'];
    if ($midEquip != 0) {
        echo "<div title='clique para esconder' style='margin-left:135px; cursor:pointer; background-color:#E7EAEF; border: 1px solid #CCCCCC; max-width:500px' onclick=\"this.style.display='none'\" >";

        if ((int) VoltaValor(EQUIPAMENTOS, 'MID_MAQUINA', 'MID', $midEquip) != 0) {
            echo "<strong>{$ling['componente_alocado_em']}:</strong> " . htmlentities(VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', VoltaValor(EQUIPAMENTOS, 'MID_MAQUINA', 'MID', $midEquip), $tdb[MAQUINAS]['dba'], 1));
            echo "<br />";
            echo "<strong>POSI&Ccedil;&Atilde;O:</strong> " . htmlentities(VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', VoltaValor(EQUIPAMENTOS, 'MID_CONJUNTO', 'MID', $midEquip), $tdb[MAQUINAS_CONJUNTO]['dba'], 1));
        } else {
            echo "{$ling['componente_nao_alocado']}";
        }
        echo "</div>";
    }

    exit();
}

if ($id == 17) {

    $midSpeedCheck = (int) $_GET['midSpeedCheck'];

    // PERMISS�O POR CENTRO-DE-CUSTO
    $filtroMaquina = "";
    if (($_SESSION[ManuSess]['user']['MID'] != 0) and (VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, 'MID', 'USUARIO', (int) $_SESSION[ManuSess]['user']['MID']) != 0)) {
        $filtroMaquina = " WHERE CENTRO_DE_CUSTO IN (SELECT CENTRO_DE_CUSTO FROM " . USUARIOS_PERMISAO_CENTRODECUSTO . " WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}')";
    }

    // N�O DEIXA MUDAR A EMPRESA DEPOIS DE GERADA A OS
    if ($osmid != 0) {

        $cc['MID_EMPRESA'] = VoltaValor(ORDEM, 'MID_EMPRESA', 'MID', $osmid);

        $filtroMaquina .= ($filtroMaquina == '') ? "WHERE " : " AND ";
        $filtroMaquina .= "MID_EMPRESA = " . (int) $cc['MID_EMPRESA'];
    }

    if ($midSpeedCheck != 0) {
        $midFamilia = VoltaValor(SPEED_CHECK, 'MID_FAMILIA', 'MID', $midSpeedCheck);
        $filtroMaquina .= ($filtroMaquina == '') ? "WHERE " : " AND ";
        $filtroMaquina .= "FAMILIA = {$midFamilia}";
    }

    echo "<br clear=\"all\" />
    <label class=\"campo_label\"for=\"cc[MID_MAQUINA]\">" . $tdb[ORDEM]['MID_MAQUINA'] . ":</label>";
    FormSelectD('COD', 'DESCRICAO', MAQUINAS, 0, "cc[MID_MAQUINA]", "cc[MID_MAQUINA]", 'MID', '', 'campo_select_ob', "this.form.submit()", $filtroMaquina, 'A', 'COD', "");

    exit();
}

if ($id == 18) {

    $midSpeedCheck = (int) $_GET['midSpeedCheck'];

    $midEmpresa = (int) $_GET['midEmpresa'];

    $whereEmp = "WHERE MID_EMPRESA = {$midEmpresa} ";

    $equip2 = 0;

    if ($midSpeedCheck != 0) {
        $midEquipe = VoltaValor(SPEED_CHECK, 'MID_EQUIPE', 'MID', $midSpeedCheck);
        $whereEmp = " WHERE MID = $midEquipe ";
        $equip2 = $midEquipe;
    }

    FormSelectD('DESCRICAO', '', EQUIPES, '', 'fil_equip', 'fil_equip', 'MID', '', '', "atualiza_area2('func_aloc', 'apontaosplan.php?id=13&osmid=$osmid&emp={$midEmpresa}&equip2=$equip2&equip=' + this.value)", $whereEmp);
    exit();
}

// Fitra os planos de speedcheck de acordo com a fam�lia de obj ou fam�lia de componente informada
if ($id == 19) {

    $midComponente = (int) $_GET['midComponente'];
    $midMaquina = (int) $_GET['midMaquina'];
    $filtroFamilia = "";

    // Fam�lia do componente
    if ($midComponente != 0) {
        $compFam = VoltaValor(EQUIPAMENTOS, 'FAMILIA', 'MID', $midComponente);
        $filtroFamilia = "WHERE MID_FAMILIA_COMPONENTE = {$compFam}";
    }

    // Fam�lia da m�quina
    if ($midMaquina != 0) {
        $maqFam = VoltaValor(MAQUINAS, 'FAMILIA', 'MID', $midMaquina);
        $filtroFamilia = "WHERE MID_FAMILIA = {$maqFam}";
    }


    FormSelectD('DESCRICAO', '', SPEED_CHECK, 0, "cc[MID_SPEED_CHECK]", "cc[MID_SPEED_CHECK]", 'MID', "", 'campo_select_ob', "", $filtroFamilia);

    exit();
}


if ($id == 21) {
    if ($_GET['it'] != "") {
        $maq = (int) $_GET['maq'];
        $plano = (int) $_GET['plano'];
        $osmid = (int) $_GET['osmid'];
        $conj = (int) $_GET['conj'];
        $atv = (int) $_GET['atv'];
        $it = (int) $_GET['it'];
        $cmid = (int) $_GET['cmid'];
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_PREV]['dba']]->Execute("UPDATE " . ORDEM_PLANEJADO_PREV . " SET DIAGNOSTICO = '$it' WHERE MID = '$atv' AND MID_ORDEM = '$osmid'");
        if (!$tmp) {
            erromsg($dba[$tdb[ORDEM_PLANEJADO_PREV]['dba']]->ErrorMsg());
        }
    }
    exit;
} elseif ($id == 22) {
    if ($_GET['it'] != "") {
        $maq = (int) $_GET['maq'];
        $plano = (int) $_GET['plano'];
        $osmid = (int) $_GET['osmid'];
        $conj = (int) $_GET['conj'];
        $atv = (int) $_GET['atv'];
        $it = (int) $_GET['it'];
        $cmid = (int) $_GET['cmid'];

        $tmp = $dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']]->Execute("UPDATE " . ORDEM_PLANEJADO_LUB . " SET DIAGNOSTICO = '$it' WHERE MID = '$atv' AND MID_ORDEM = '$osmid'");
        if (!$tmp) {
            erromsg($dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']]->ErrorMsg());
        }
    }
    exit;
} elseif ($id == 31) {

    $hi = explode(":", $_GET['hi']);
    $di = explode("/", $_GET['di']);
    $df = explode("/", $_GET['df']);
    $hf = explode(":", $_GET['hf']);
    if (!checkdate((int) $di[1], (int) $di[0], (int) $di[2]))
        echo "<strong><font color=\"red\">" . $ling['data_inicial_invalida'] . "</font></strong><br>";
    elseif ($df[0] != "") {
        if (!checkdate((int) $df[1], (int) $df[0], (int) $df[2]))
            echo "<strong><font color=\"red\">" . $ling['data_final_invalida'] . "</font></strong><br>";
    }
    if (!HoraValida($hi[0]) or !MSValido($hi[1]) or !MSValido($hi[2]))
        echo "<strong><font color=\"red\">" . $ling['hora_inicial_e_invalida'] . "</font></strong>";
    elseif (!HoraValida($hf[0]) or !MSValido($hf[1]) or !MSValido($hf[2]))
        echo "<strong><font color=\"red\">" . $ling['hora_final_e_invalida'] . "</font></strong>";
    elseif ($hf[0] != "") {
        $di = mktime($hi[0], $hi[1], $hi[2], $di[1], $di[0], $di[2]);
        $df = mktime($hf[0], $hf[1], $hf[2], $df[1], $df[0], $df[2]);
        if ($di > $df)
            echo "<strong><font color=\"red\">" . $ling['data_inicial_maior_data_final'] . "</font></strong>";
        else {
            $dt = $df - $di;
            $h = round($dt / (60 * 60), 4);
            echo "<label class=\"campo_label\" for=\"ptotal\">{$ling['ord_temp_total_parada']}</label>
            <input type=\"text\" id=\"ptotal\" class=\"campo_text\" value=\"$h\" name=\"ptotal\" size=\"8\" maxlength=\"8\" />
            <input type=\"button\" class=\"botao\" value=\"Gravar\" onclick=\"atualiza_area2('lista_Ps','apontaosplan.php?osmid=$osmid&id=32&di=' + document.getElementById('DATA_PARADA_INICIO').value + '&hi=' + document.getElementById('HORA_PARADA_INICIO').value + '&df=' + document.getElementById('DATA_PARADA_FIM').value + '&hf=' + document.getElementById('HORA_PARADA_FIM').value + '&total=' + document.getElementById('ptotal').value + '&grava=' + this.value);this.value='Gravado com Sucesso'\" />";
        }
    }
    exit;
} elseif ($id == 32) {
    $hi = $_GET['hi'];
    $hf = $_GET['hf'];
    $di = explode("/", $_GET['di']);
    $df = explode("/", $_GET['df']);
    $data_inicio = $di[2] . "-" . $di[1] . "-" . $di[0];
    $data_final = $df[2] . "-" . $df[1] . "-" . $df[0];
    $mkdi = VoltaTime($hi, NossaData($data_inicio));
    $mkdf = VoltaTime($hf, NossaData($data_final));
    $total = $_GET['total'];
    $naomostra = 0;
    //echo "$hi $hf";
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->Execute("DELETE FROM " . ORDEM_PLANEJADO_MAQ_PARADA . " WHERE MID = '$del' AND MID_ORDEM = '$osmid'");
        if (!$tmp)
            erromsg($dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->ErrorMsg());
        unset($del);
    }
    if ($total != "") {
        if ($_GET['grava'] == "Gravar") {
            $sql = "SELECT * FROM " . ORDEM_PLANEJADO_MAQ_PARADA . " WHERE MID_ORDEM = '$osmid' ORDER BY DATA_INICIO,HORA_INICIO DESC";
            $re = $dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->Execute("$sql");
            $ca = $re->fields;
            while (!$re->EOF) {
                $ca = $re->fields;
                $mktmpinicio = VoltaTime($ca['HORA_INICIO'], $re->UserDate($ca['DATA_INICIO'], 'd/m/Y'));
                $mktmpfinal = VoltaTime($ca['HORA_FINAL'], $re->UserDate($ca['DATA_FINAL'], 'd/m/Y'));
                if ((($mktmpinicio >= $mkdi) and ($mktmpinicio <= $mkdf)) ||
                        (($mktmpfinal >= $mkdi) and ($mktmpfinal <= $mkdf)) ||
                        (($mktmpinicio <= $mkdi) and ($mktmpfinal >= $mkdf))) {
                    erromsg($ling['ord_imposivel_gravar_peri']);
                    $naomostra = 1;
                    break;
                }


                $re->MoveNext();
            }

            if ($naomostra == 0) {
                $total = str_replace(",", ".", $total);
                $tmp_mid = GeraMid(ORDEM_PLANEJADO_MAQ_PARADA, "MID", $tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']);
                $tmp = $dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->Execute("INSERT INTO " . ORDEM_PLANEJADO_MAQ_PARADA . " (DATA_INICIO,DATA_FINAL,HORA_INICIO,HORA_FINAL,TEMPO,MID_ORDEM,MID) VALUES('$data_inicio','$data_final','$hi','$hf','$total','$osmid','$tmp_mid')");
                if (!$tmp)
                    erromsg($dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->ErrorMsg());
            }
        }
    }
    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>{$tdb[ORDEM_MAQ_PARADA]['DATA_INICIO']}</th>
    <th>{$tdb[ORDEM_MAQ_PARADA]['DATA_FINAL']}</th>
    <th>{$tdb[ORDEM_MAQ_PARADA]['TEMPO']}</th>
    <th></th>
    </tr>";
    $tmp = $dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_MAQ_PARADA . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        echo "<tr class=\"cor2\">
        <td>" . $tmp->UserDate($campo['DATA_INICIO'], 'd/m/Y') . " " . $campo['HORA_INICIO'] . "</td>
        <td>" . $tmp->UserDate($campo['DATA_FINAL'], 'd/m/Y') . " " . $campo['HORA_FINAL'] . "</td>
        <td>" . $campo['TEMPO'] . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('lista_Ps','apontaosplan.php?osmid=$osmid&id=32&del=" . $campo['MID'] . "')\" />
        </td>
        </tr>";
        $tmp->MoveNext();
    }
    echo "</table>";
    exit;
} elseif ($id == 33) {

    $hi2 = explode(":", $_GET['hi']);
    $di2 = explode("/", $_GET['di']);
    $df2 = explode("/", $_GET['df']);
    $hf2 = explode(":", $_GET['hf']);
    $datai = $di2[2] . "-" . $di2[1] . "-" . $di2[0];
    $dataf = $df2[2] . "-" . $df2[1] . "-" . $df2[0];
    $f = $_GET['f'];
    if (!checkdate((int) $di2[1], (int) $di2[0], (int) $di2[2])) {
        echo "<strong><font color=\"red\">" . $ling['data_inicial_invalida'] . "</font></strong><br>";
    } elseif ($df2[0] != "") {
        if (!checkdate((int) $df2[1], (int) $df2[0], (int) $df2[2]))
            echo "<strong><font color=\"red\">" . $ling['data_final_invalida'] . "</font></strong><br>";
    }
    if (!HoraValida($hi2[0]) or !MSValido($hi2[1]) or !MSValido($hi2[2]))
        echo "<strong><font color=\"red\">" . $ling['hora_inicial_e_invalida'] . "</font></strong>";
    elseif (!HoraValida($hf2[0]) or !MSValido($hf2[1]) or !MSValido($hf2[2]))
        echo "<strong><font color=\"red\">" . $ling['hora_final_e_invalida'] . "</font></strong>";
    elseif ($hf2[0] != "") {
        $di = mktime((int) $hi2[0], (int) $hi2[1], (int) $hi2[2], (int) $di2[1], (int) $di2[0], (int) $di2[2]);
        $df = mktime((int) $hf2[0], (int) $hf2[1], (int) $hf2[2], (int) $df2[1], (int) $df2[0], (int) $df2[2]);
        if ($di > $df) {
            echo "<strong><font color=\"red\">" . $ling['data_inicial_maior_data_final'] . "</font></strong>";
        } else {
            $sql = "SELECT * FROM " . ORDEM_PLANEJADO_MADODEOBRA . " WHERE MID_FUNCIONARIO = '$f' AND DATA_INICIO >= '$datai' AND DATA_INICIO <= '$dataf' ORDER BY DATA_INICIO,HORA_INICIO DESC";
            $re = $dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->Execute("$sql");
            $ca = $re->fields;
            while (!$re->EOF) {
                $ca = $re->fields;
                $status = VoltaValor(ORDEM_PLANEJADO, "STATUS", "MID", $ca['MID_ORDEM'], 0);
                if ((int) VoltaValor(FUNCIONARIOS, "FORNECEDOR", "MID", $f, 0) == 0) {
                    if ($status != 3) {
                        $no = VoltaValor(ORDEM_PLANEJADO, "NUMERO", "MID", $ca['MID_ORDEM'], 0);
                        $mktmpinicio = VoltaTime($ca['HORA_INICIO'], $re->UserDate($ca['DATA_INICIO'], 'd/m/Y'));
                        $mktmpfinal = VoltaTime($ca['HORA_FINAL'], $re->UserDate($ca['DATA_FINAL'], 'd/m/Y'));
                        if ((($mktmpinicio >= $di) and ($mktmpinicio <= $df)) ||
                                (($mktmpfinal >= $di) and ($mktmpfinal <= $df)) ||
                                (($mktmpinicio <= $di) and ($mktmpfinal >= $df))) {
                            echo "<strong><font color=\"red\">" . $ling['funcionario_ja_possui_apont_neste_periodo'] . ". OS: $no Data Inicio: " . $re->UserDate($ca['DATA_INICIO'], 'd/m/Y') . " " . $ca['HORA_INICIO'] . " Data Final: " . $re->UserDate($ca['DATA_FINAL'], 'd/m/Y') . " " . $ca['HORA_FINAL'] . "</font></strong><br>";
                            $naomostra = 1;
                            break;
                        }
                    }
                }
                $re->MoveNext();
            }
        }
        if (($f != "") and ($naomostra != 1)) {
            $resultado = $dba[$tdb[FUNCIONARIOS]['dba']]->Execute("SELECT VALOR_HORA,EQUIPE,MID FROM " . FUNCIONARIOS . " WHERE MID = '$f'");
            $campo = $resultado->fields;
            if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
                echo "<label class=\"campo_label\" for=\"fvalor_hora\">" . $ling['custo_hora'] . "</label><input type=\"text\" disabled=\"disabled\" id=\"fvalor_hora\" class=\"campo_text\" value=\"" . $campo['VALOR_HORA'] . "\" name=\"fvalor_hora\" size=\"8\" maxlenght=\"20\" />";
            }
            $dt = $df - $di;
            $h = floor($dt / (60 * 60));
            $mins = floor($dt / (60));
            $m = $dt / (60);
            $tmp_total = $campo['VALOR_HORA'] / 60;
            $tmp_total = (float) round($tmp_total * $m, 2);
            if ($_GET['fvalor_extra'] != "")
                $tmp_total = $tmp_total + $_GET['fvalor_extra'];
            if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
                echo "<label for=\"fvalor_extra\">" . $ling['custo_hora_extra'] . "</label>
            <input type=\"text\" id=\"fvalor_extra\" class=\"campo_text\" value=\"" . (float) $_GET['fvalor_extra'] . "\" name=\"fvalor_extra\" size=\"8\" maxlenght=\"20\" onblur=\"atualiza_area2('total_fs','apontaosplan.php?osmid=$osmid&f=$f&id=33&hi=" . $_GET['hi'] . "&hf=" . $_GET['hf'] . "&di=" . $_GET['di'] . "&df=" . $_GET['df'] . "&fvalor_extra=' + document.getElementById('fvalor_extra').value)\" />";

                echo "<label for=\"fvalor_total\">" . $ling['custo_total'] . "</label>
            <input type=\"text\" id=\"fvalor_total\" class=\"campo_text\" value=\"" . (float) $tmp_total . "\" name=\"fvalor_total\" size=\"8\" maxlenght=\"20\" />";
            } else {
                echo "
            <input type=\"hidden\" id=\"fvalor_extra\" value=\"" . (float) $_GET['fvalor_extra'] . "\" name=\"fvalor_extra\" />
            <input type=\"hidden\" id=\"fvalor_total\" value=\"" . (float) $tmp_total . "\" name=\"fvalor_total\" />";
            }
            $dt = $df - $di;
            $h = round($dt / (60 * 60), 2);
            $mins = floor($dt / (60));
            $m = $mins % 60;
            if ($m < 10)
                $m = "0" . $m;
            echo "<br clear=\"all\" /><strong>{$ling['total']}: $h ({$ling['horas_min']}) </strong>";
            echo "<input class=\"botao\" type=\"button\" name=\"fadd\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('lista_fs','apontaosplan.php?osmid=$osmid&f=$f&id=34&equip=" . $campo['EQUIPE'] . "&hi=" . $_GET['hi'] . "&hf=" . $_GET['hf'] . "&di=$datai&df=$dataf&total=' + document.getElementById('fvalor_total').value + '&fvalor_extra=' + document.getElementById('fvalor_extra').value + '&gravar=' + this.value);this.value='{$ling['ord_enviado_sucesso']}';\" />";
        }
    }
    exit;
}
if ($id == 34) {
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->Execute("DELETE FROM " . ORDEM_PLANEJADO_MADODEOBRA . " WHERE MID = '$del' AND MID_ORDEM = '$osmid'");
        if (!$tmp) {
            erromsg($dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->ErrorMsg());
        }
        unset($del);
        TempoServcoOS($osmid);
    }
    if (($_GET['di'] != "") and ($_GET['gravar'] == "Gravar")) {

        $_GET['fvalor_extra'] = str_replace(",", ".", $_GET['fvalor_extra']);
        $_GET['total'] = str_replace(",", ".", $_GET['total']);
        $tmp_mid = GeraMid(ORDEM_PLANEJADO_MADODEOBRA, "MID", $tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']);
        $sql = "INSERT INTO " . ORDEM_PLANEJADO_MADODEOBRA . " (MID_ORDEM, MID_FUNCIONARIO, DATA_INICIO, HORA_INICIO, DATA_FINAL, HORA_FINAL, EQUIPE, CUSTO_EXTRA, CUSTO, MID) VALUES ('$osmid','" . $_GET['f'] . "','" . $_GET['di'] . "','" . $_GET['hi'] . "','" . $_GET['df'] . "','" . $_GET['hf'] . "','" . $_GET['equip'] . "','" . $_GET['fvalor_extra'] . "','" . $_GET['total'] . "','$tmp_mid')";
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->Execute($sql);
        if (!$tmp) {
            erromsg($dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->ErrorMsg() . "<br />" . $sql);
        }
        TempoServcoOS($osmid);
    }

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>{$tdb[ORDEM_MAODEOBRA]['MID_FUNCIONARIO']}</th>
    <th>{$tdb[FUNCIONARIOS]['EQUIPE']}</th>
    <th>{$tdb[ORDEM_MAODEOBRA]['DATA_INICIO']}</th>
    <th>{$tdb[ORDEM_MAODEOBRA]['DATA_FINAL']}</th>";

    if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
        echo "<th>{$tdb[ORDEM_MAODEOBRA]['CUSTO_EXTRA']}</th>
        <th>" . $tdb[ORDEM_MAODEOBRA]['CUSTO'] . "</th>";
    }

    echo "<th></th>
    </tr>";

    $tmp = $dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_MADODEOBRA . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        $ftotal = (float) $ftotal + $campo['CUSTO'];
        $campo['CUSTO'] = (float) $campo['CUSTO'];
        $campo['CUSTO_EXTRA'] = (float) $campo['CUSTO_EXTRA'];

        echo "<tr class=\"cor2\">
        <td>" . htmlentities(strtoupper(VoltaValor(FUNCIONARIOS, "NOME", "MID", $campo['MID_FUNCIONARIO'], $tdb[FUNCIONARIOS]['dba']))) . "</td>
        <td>" . htmlentities(strtoupper(VoltaValor(EQUIPES, "DESCRICAO", "MID", $campo['EQUIPE'], $tdb[EQUIPES]['dba']))) . "</td>
        <td>" . $tmp->UserDate($campo['DATA_INICIO'], 'd/m/Y') . " " . $campo['HORA_INICIO'] . "</td>
        <td>" . $tmp->UserDate($campo['DATA_FINAL'], 'd/m/Y') . " " . $campo['HORA_FINAL'] . "</td>";

        if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
            echo "<td>" . number_format($campo['CUSTO_EXTRA'], 2, ',', '.') . "</td>
            <td>" . number_format($campo['CUSTO'], 2, ',', '.') . "</td>";
        }

        echo "
        <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('lista_fs','apontaosplan.php?osmid=$osmid&id=34&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }
    if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
        echo "</table><br /><strong>{$ling['total']}: " . number_format($ftotal, 2, ',', '.') . "</strong>";
    } else {
        echo "</table><br />";
    }
    exit;
} elseif ($id == 35) {
    $alm = (int) $_GET['alm'];
    $fmat = LimpaTexto($_GET['fmat']);
    $mat = (int) $_GET['m'];
    $qtd = (float) $_GET['qtd'];
    $mat_un = VoltaValor(MATERIAIS, "UNIDADE", "MID", $fmat, 0);
    $unidade = htmlentities(VoltaValor(MATERIAIS_UNIDADE, "DESCRICAO", "MID", $mat_un, 0));
    $cu = VoltaValor(MATERIAIS_ALMOXARIFADO, "CUSTO_UNITARIO", array("MID_ALMOXARIFADO", "MID_MATERIAL"), array($alm, $fmat), 0);
    $qtotal = $cu * $qtd;

    echo "<label class=\"campo_label\" for=\"qtd\">{$tdb[ORDEM_MATERIAL]['QUANTIDADE']} ($unidade):</label>
    <input type=\"text\" id=\"qtd\" value=\"$qtd\"class=\"campo_text\" name=\"qtd\" size=\"10\" maxlenght=\"20\" onblur=\"atualiza_area2('dmat','apontaosplan.php?&osmid=$osmid&id=35&fmat=$fmat&alm=$alm&qtd=' + document.getElementById('qtd').value)\" />
    <label for=\"qtotal\"> {$tdb[ORDEM_MATERIAL]['CUSTO_TOTAL']}:</label>
    <input type=\"text\" id=\"qtotal\" class=\"campo_text\" name=\"qtotal\" value=\"$qtotal\" size=\"10\" maxlenght=\"20\"/>
    <input class=\"botao\" type=\"button\" name=\"fadd\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('lmat','apontaosplan.php?osmid=$osmid&id=36&m=$fmat&alm=$alm&ct=' + document.getElementById('qtotal').value + '&cu=$cu' + '&qtd=' + document.getElementById('qtd').value)\" />";
    exit;
} elseif ($id == 351) {
    $alm = $_GET['alm'];
    $where = "WHERE MID IN (SELECT MID_MATERIAL FROM " . MATERIAIS_ALMOXARIFADO . " WHERE MID_ALMOXARIFADO = $alm)";

    echo "<label class=\"campo_label\" for=\"mat\">" . $tdb[ORDEM_MATERIAL]['MID_MATERIAL'] . "</label>";
    FormSelectD('COD', 'DESCRICAO', MATERIAIS, '', "mat", "mat", 'MID', '', '', "atualiza_area2('dmat','apontaosplan.php?osmid=$osmid&id=35&fmat=' + this.value + '&alm=' + document.getElementById('alm').value)", $where, 'A', 'COD', $ling['select_opcao']);
    echo "<br clear=\"all\" />";
    exit();
} elseif ($id == 36) {
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->Execute("DELETE FROM " . ORDEM_PLANEJADO_MATERIAL . " WHERE MID = '$del' AND MID_ORDEM = '$osmid'");
        if (!$tmp)
            erromsg($dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->ErrorMsg());
        unset($del);
    }
    if ($_GET['m'] != "") {
        $_GET['cu'] = str_replace(",", ".", $_GET['cu']);
        $_GET['ct'] = str_replace(",", ".", $_GET['ct']);
        $tmp_mid = GeraMid(ORDEM_PLANEJADO_MATERIAL, "MID", $tdb[ORDEM_PLANEJADO_MATERIAL]['dba']);
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->Execute("INSERT INTO " . ORDEM_PLANEJADO_MATERIAL . " (MID_ORDEM,MID_MATERIAL,QUANTIDADE,CUSTO_UNITARIO,CUSTO_TOTAL,MID,MID_ALMOXARIFADO) VALUES('$osmid','" . (int) $_GET['m'] . "','" . $_GET['qtd'] . "','" . (float) $_GET['cu'] . "','" . (float) $_GET['ct'] . "','$tmp_mid','" . (int) $_GET['alm'] . "')");
        if (!$tmp)
            erromsg($dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->ErrorMsg());
    }

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>" . $tdb[ORDEM_MATERIAL]['MID_ALMOXARIFADO'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['MID_MATERIAL'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['QUANTIDADE'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['CUSTO_UNITARIO'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['CUSTO_TOTAL'] . "</th>
    <th></th>
    </tr>";

    $tmp = $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_MATERIAL . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        $mtotal = (float) $mtotal + $campo['CUSTO_TOTAL'];
        $campo['CUSTO_UNITARIO'] = (float) $campo['CUSTO_UNITARIO'];
        $campo['CUSTO_TOTAL'] = (float) $campo['CUSTO_TOTAL'];
        $unidade = VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", VoltaValor(MATERIAIS, "UNIDADE", "MID", $campo['MID_MATERIAL'], 0), 0);

        echo "<tr class=\"cor2\">
        <td>" . htmlentities(VoltaValor(ALMOXARIFADO, "DESCRICAO", "MID", $campo['MID_ALMOXARIFADO'], $tdb[ALMOXARIFADO]['dba'])) . "</td>
        <td>" . htmlentities(VoltaValor(MATERIAIS, "DESCRICAO", "MID", $campo['MID_MATERIAL'], $tdb[MATERIAIS]['dba'])) . "</td>
        <td>" . $campo['QUANTIDADE'] . " $unidade</td>
        <td>" . number_format($campo['CUSTO_UNITARIO'], 2, ",", ".") . "</td>
        <td>" . number_format($campo['CUSTO_TOTAL'], 2, ",", ".") . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('lmat','apontaosplan.php?osmid=$osmid&id=36&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }
    echo "</table><br /><strong>{$ling['total']}: " . number_format($mtotal, 2, ",", ".") . "</strong>";
    exit;
} elseif ($id == 37) {
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']]->Execute("DELETE FROM " . ORDEM_PLANEJADO_CUSTOS . " WHERE MID = '$del' AND MID_ORDEM = '$osmid'");
        if (!$tmp) {
            erromsg($dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']]->ErrorMsg());
        }
        unset($del);
    }
    if ($_GET['des'] != "") {
        $des = LimpaTexto($_GET['des']);
        $oc = str_replace(",", ".", $_GET['oc']);

        if (is_numeric($oc)) {
            $tmp_mid = GeraMid(ORDEM_PLANEJADO_CUSTOS, "MID", $tdb[ORDEM_PLANEJADO_CUSTOS]['dba']);
            $tmp = $dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']]->Execute("INSERT INTO " . ORDEM_PLANEJADO_CUSTOS . " VALUES('$osmid','$des','$oc','$tmp_mid')");
            if (!$tmp) {
                erromsg($dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']]->ErrorMsg());
            }
        }
        else
            echo "<font color=\"#FF0000\">{$ling['nao_e_numero']}</font><br>";
    }

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>{$tdb[ORDEM_CUSTOS]['DESCRICAO']}</th>
    <th>{$tdb[ORDEM_CUSTOS]['DATA']}</th>
    <th></th>
    </tr>";

    $tmp = $dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_CUSTOS . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        $ototal = (float) $ototal + $campo['CUSTO'];
        $campo['CUSTO'] = (float) $campo['CUSTO'];

        echo "<tr class=\"cor2\">
        <td>" . $campo['DESCRICAO'] . "</td>
        <td>" . number_format($campo['CUSTO'], 2, ",", ".") . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('loc','apontaosplan.php?osmid=$osmid&id=37&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }

    echo "</table><br /><strong>{$ling['total']}: " . number_format($ototal, 2, ",", ".") . "</strong>";
    exit;
} elseif ($id == 41) {
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("DELETE FROM " . PENDENCIAS . " WHERE MID = '$del' AND MID_ORDEM = '$osmid'");
        if (!$tmp)
            erromsg($dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->ErrorMsg());
        unset($del);
    }
    if ($_GET['des'] != "") {
        $tmp_mid = GeraMid(PENDENCIAS, "MID", $tdb[PENDENCIAS]['dba']);
        $des = strtoupper(LimpaTexto($_GET['des']));
        $maq = (int) $_GET['maq'];


        // Colocando MID_EMPRESA e numero
        $emp_tmp = VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', $maq, $tdb[MAQUINAS]['dba']);
        $num = GeraNumEmp($emp_tmp, PENDENCIAS, 'NUMERO');


        $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("INSERT INTO " . PENDENCIAS . " (NUMERO, MID_ORDEM, MID_MAQUINA, MID_EMPRESA, DESCRICAO, DATA, MID) VALUES ('$num', '$osmid', '$maq', '$emp_tmp', '$des', '" . date("Y-m-d") . "', '$tmp_mid')");
        if (!$tmp)
            erromsg($dba[$tdb[PENDENCIAS]['dba']]->ErrorMsg());
        if ($_GET['alert'] == 1) {
            echo "<p align=\"center\"><a href=\"javascript:abre_div('pen" . $_GET['mid'] . "',0)\">{$ling['ord_pendencia_sucesso']}. {$ling['clique_fechar']}</a>";
            exit;
        }
    }

    echo "<table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>" . $tdb[PENDENCIAS]['NUMERO'] . "</th>
    <th>" . $tdb[PENDENCIAS]['DATA'] . "</th>
    <th>" . $tdb[PENDENCIAS]['MID_MAQUINA'] . "</th>
    <th>" . $tdb[PENDENCIAS]['DESCRICAO'] . "</th>
    <th></th>
    </tr>";

    $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("SELECT * FROM " . PENDENCIAS . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        echo "<tr class=\"cor2\">
        <td>" . htmlentities($campo['NUMERO']) . "</td>
        <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . " </td>
        <td>" . htmlentities(VoltaValor(MAQUINAS, "DESCRICAO", "MID", $campo['MID_MAQUINA'], 0)) . "</td>
        <td>" . htmlentities($campo['DESCRICAO']) . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('pls','apontaosplan.php?osmid=$osmid&id=41&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }

    echo "</table>";
    exit;
} elseif ($id == 42) {
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("UPDATE " . PENDENCIAS . " SET MID_ORDEM_EXC = '0' WHERE MID = '$del'");
        if (!$tmp)
            erromsg($dba[$tdb[PENDENCIAS]['dba']]->ErrorMsg());
        unset($del);
    }
    $pen = (int) $_GET['pen'];
    if ($pen != 0) {
        $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("UPDATE " . PENDENCIAS . " SET MID_ORDEM_EXC= '$osmid' WHERE MID = '$pen'");
        if (!$tmp)
            erromsg($dba[$tdb[PENDENCIAS]['dba']]->ErrorMsg());
    }

    echo "
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>" . $tdb[PENDENCIAS]['MID_ORDEM'] . "</th>
    <th>" . $tdb[PENDENCIAS]['DESCRICAO'] . "</th>
    <th>" . $tdb[PENDENCIAS]['DATA'] . "</th>
    <th></th>
    </tr>";

    $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("SELECT * FROM " . PENDENCIAS . " WHERE MID_ORDEM_EXC = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        echo "<tr class=\"cor2\">
        <td>" . $campo['NUMERO'] . "</td>
        <td>" . htmlentities($campo['DESCRICAO']) . "</td>
        <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . "</td>
        <td><img src=\"imagens/icones/22x22/pendencias.png\" style=\"cursor:hand\" title=\"{$ling['ord_continua_pendente_n_exec']}\" border=\"0\" onclick=\"atualiza_area2('apen','apontaosplan.php?osmid=$osmid&id=42&del=" . $campo['MID'] . "')\" />
        </td></tr>";
        $tmp->MoveNext();
    }

    echo "</table><br />";
    exit;
} elseif ($id == 51) {
    if ($_GET['del'] != "") {
        $del = (int) $_GET['del'];
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']]->Execute("DELETE FROM " . ORDEM_PLANEJADO_REPROGRAMA . " WHERE MID = '$del' AND MID_ORDEM = '$osmid'");
        if (!$tmp)
            erromsg($dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']]->ErrorMsg());
        unset($del);
    }
    if ($_GET['mot'] != "") {
        if ($manusis['caixaalta'] == 1)
            $des = strtoupper(LimpaTexto($_GET['mot']));
        else
            $des = LimpaTexto($_GET['mot']);
        $di = explode("/", $_GET['data']);
        $data = $di[2] . "-" . $di[1] . "-" . $di[0];
        $data_prog = VoltaValor(ORDEM, 'DATA_PROG', 'MID', $osmid);


        if (!checkdate((int) $di[1], (int) $di[0], (int) $di[2]))
            echo "<br clear=\"all\" /><strong><font color=\"red\">{$ling['data_invalida']}</font></strong><br /><br clear=\"all\" />";
        else {
            $tmp_mid = GeraMid(ORDEM_PLANEJADO_REPROGRAMA, "MID", $tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']);
            $tmp = $dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']]->Execute("INSERT INTO " . ORDEM_PLANEJADO_REPROGRAMA . " (MID_ORDEM, MOTIVO, DATA, DATA_ORIGINAL, MID) VALUES('$osmid', '$des', '$data', '$data_prog', '$tmp_mid')");
            if (!$tmp)
                erromsg($dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']]->ErrorMsg());
            $sql = "UPDATE " . ORDEM . " SET DATA_PROG = '$data' WHERE MID = '$osmid'";
            $tmp = $dba[$tdb[ORDEM]['dba']]->Execute($sql);
            if (!$tmp)
                erromsg($dba[$tdb[ORDEM]['dba']]->ErrorMsg());
        }
    }

    echo "
    <br /><table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>" . $tdb[ORDEM_REPROGRAMA]['MOTIVO'] . "</th>
    <th>" . $tdb[ORDEM_REPROGRAMA]['DATA'] . "</th>
    <th>" . $tdb[ORDEM_REPROGRAMA]['DATA_ORIGINAL'] . "</th>
    <th></th>
    </tr>";

    $tmp = $dba[$tdb[ORDEM_PLANEJADO_REPROGRAMA]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_REPROGRAMA . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        echo "<tr class=\"cor2\">
        <td>" . htmlentities($campo['MOTIVO']) . "</td>
        <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . "</td>
        <td>" . $tmp->UserDate($campo['DATA_ORIGINAL'], 'd/m/Y') . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('repro','apontaosplan.php?osmid=$osmid&id=51&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }

    echo "</table><br />";
    exit;
} elseif ($id == 99901) {
    $func = (int) $_GET['func'];
    $forn = (int) VoltaValor(FUNCIONARIOS, 'FORNECEDOR', 'MID', $func, 0);
    if ($forn) {
        echo "<br clear=\"all\" />
        <label class=\"campo_label\" for=\"manutentor\">" . $ling['manutentor'] . "</label>
        <input type=\"text\" size=\"40\" name=\"cc[manutentor]\" id=\"manutentor\" class=\"campo_text_ob\" />";
    } else {
        echo "<input type=\"hidden\"  id=\"manutentor\" value=\"\">";
    }
    exit;
}




if ($_POST['gravaos'] != "") {
    $cc = $_POST['cc'];
    $cc['TEXTO'] = strtoupper(LimpaTexto($cc['TEXTO']));
    $cc['TIPO'] = (int) $cc['TIPO'];
    $cc['SOLICITANTE'] = strtoupper(LimpaTexto($cc['SOLICITANTE']));
    $fam = (int) VoltaValor(MAQUINAS, "FAMILIA", "MID", $cc['MID_MAQUINA'], 0);
    $classe = (int) VoltaValor(MAQUINAS, "CLASSE", "MID", $cc['MID_MAQUINA'], 0);
    $forne = (int) VoltaValor(MAQUINAS, "FORNECEDOR", "MID", $cc['MID_MAQUINA'], 0);
    $fabr = (int) VoltaValor(MAQUINAS, "FABRICANTE", "MID", $cc['MID_MAQUINA'], 0);
    $empresa = (int) VoltaValor(MAQUINAS, "MID_EMPRESA", "MID", $cc['MID_MAQUINA'], 0);
    $setor = (int) VoltaValor(MAQUINAS, "MID_SETOR", "MID", $cc['MID_MAQUINA'], 0);
    $area = (int) VoltaValor(SETORES, "MID_AREA", "MID", $setor, 0);

    $centro = (int) VoltaValor(MAQUINAS, "CENTRO_DE_CUSTO", "MID", $cc['MID_MAQUINA'], 0);

    // Verificando o speedcheck
    $isSpeedCheck = FALSE;
    if ((int) VoltaValor(TIPOS_SERVICOS, 'SPEED_CHECK', 'MID', $cc['TIPO_SERVICO']) == 1) {
        $isSpeedCheck = TRUE;
    }

    $isTroca = FALSE;
    if (VoltaValor(TIPOS_SERVICOS, 'TROCA', 'MID', $cc['TIPO_SERVICO']) == 1) {
        $isTroca = TRUE;
    }

    $toSetup = FALSE;
    if (in_array($cc['TIPO_SERVICO'], $manusis['tpo_serv_troca_setup']) AND VoltaValor(TIPOS_SERVICOS, 'TROCA', 'MID', $cc['TIPO_SERVICO']) == 1) {
        $toSetup = TRUE;
    }
    // CONTINUAR FUNCIONANDO
    $cc['DATA_ABRE'] = $_POST['DATA_ABRE'];

    $d = explode("/", $cc['DATA_ABRE']);
    if (!$_POST['data_prog']) {
        $dprog = explode("/", $cc['DATA_PROG']);
    } else {
        $dprog = explode("/", $_POST['data_prog']);
    }
    $dprog_compara = @mktime(0, 0, 0, $dprog[1], $dprog[0], $dprog[2]);
    $dabre_compara = @mktime(0, 0, 0, $d[1], $d[0], $d[2]);
    $cc['TIPO'] = (int) $cc['TIPO'];
    if (!checkdate((int) $d[1], (int) $d[0], (int) $d[2])) {
        $msg.= "" . $ling['data_ab_inva'] . "";
    }
    if (!checkdate((int) $dprog[1], (int) $dprog[0], (int) $dprog[2])) {
        $msg.= "<li>{$ling['data_prog_invalida']}</li>";
    }
    if ($dabre_compara > $dprog_compara) {
        $msg.= "<li>" . $ling['data_ab_nao_pod_ser_maior_q_data_prog'] . "</li>";
    }
    if ($cc['HORA_ABRE'] == "") {
        $msg.= "<li>{$ling['hora_abre']}: {$ling['form01']} </li>";
    }
    if (((int) $cc['TIPO_SERVICO'] == 0) and ($cc['TIPO'] != 1 and $cc['TIPO'] != 2)) {
        $msg.= "<li>{$ling['rel_desc_tp_serv2']}: {$ling['form01']} </li>";
    }

    if (((int) $cc['MID_MAQUINA'] == 0) and ($cc['TIPO'] != 1 and $cc['TIPO'] != 2) and (!$isTroca)) {
        $msg.= "<li>{$tdb[MAQUINAS]['DESC']}: {$ling['form01']}</li> ";
    }

    if ($isTroca == TRUE AND $toSetup == FALSE && (int) $cc['MID_MAQUINA'] == 0) {
        $msg.= "<li>{$ling['volta_de_setup_para_nao_alocado_nao_prmitido']}</li> ";
    }

    if ($isSpeedCheck == TRUE AND $cc['MID_SPEED_CHECK'] == 0) {
        $msg.= "<li>{$tdb[ORDEM]['MID_SPEED_CHECK']}: {$ling['form01']}</li> ";
    }
    // Caso esteja atualizando a ordem e a mesma era de speedcheck, removemos o plano antigo atrelado a ela
    if ($isSpeedCheck == FALSE AND VoltaValor(ORDEM, 'MID_SPEED_CHECK', 'MID', $osmid) != 0) {
        if (!atualizaOrdemSpeedCheck(0, $osmid)) {
            $msg .= "<li>{$tdb[SPEED_CHECK_ATIVIDADES]['DESC']}: Um problema ocorreu ao eliminar as atividades do plano anterior.<br />Por favor entre em contato com o administrador do sistema</li>";
        }
    }

    if ($isTroca == TRUE and $osmid == 0) {

        $alocando = TRUE;
        // Equipamento � obrigat�rio
        if ((int) $cc['MID_EQUIPAMENTO'] == 0) {
            $msg.= "<li>{$tdb[ORDEM]['MID_EQUIPAMENTO']}: {$ling['form01']} para o tipo de servi&ccedil;o TROCA</li> ";
        }
        // Se houver uma ordem de troca aberta para o componente n�o � permitido abrir uma nova, se n�o for envio para o p�tio de setup
        elseif ((int) VoltaValor(ORDEM, 'MID', 'TIPO_SERVICO IN (SELECT MID FROM ' . TIPOS_SERVICOS . ' WHERE TROCA = 1) AND STATUS = 1 AND MID_EQUIPAMENTO', $cc['MID_EQUIPAMENTO']) != 0 AND $toSetup == FALSE) {
            $msg.= "<li><strong>{$ling['impossivel_abrir_ordem']}:</strong> {$ling['molde_possui_os_aberta']}<br />
            <strong>Num. O.S:</strong> " . VoltaValor(ORDEM, 'NUMERO', 'TIPO_SERVICO IN (SELECT MID FROM ' . TIPOS_SERVICOS . ' WHERE TROCA = 1) AND STATUS = 1 AND MID_EQUIPAMENTO', $cc['MID_EQUIPAMENTO']) . "</li> ";
        }
        // N�o � permitido alocar no mesmo lugar
        elseif ($cc['MID_MAQUINA'] == VoltaValor(EQUIPAMENTOS, 'MID_MAQUINA', 'MID', $cc['MID_EQUIPAMENTO'])) {
            $msg.= "<li><strong>{$ling['impossivel_alocar']}:</strong> {$ling['molde_ja_alocado']}</li> ";
        }

        /**
         *  Caso n�o haja algum impedimento de movimenta��o,
         *  salvamos a ordem para a m�quina de acordo com as regras a seguir
         */ else {

            // Quando estiver desalocando o Molde da m�quina, salvamos o MID_MAQUINA pela m�quina em que o Molde
            // estava alocado antes ou seja m�quina de origem
            if ((int) $cc['MID_MAQUINA'] == 0) {
                $cc['MID_MAQUINA'] = (int) VoltaValor(EQUIPAMENTOS, 'MID_MAQUINA', 'MID', $cc['MID_EQUIPAMENTO']);
                $cc['MID_CONJUNTO'] = (int) VoltaValor(EQUIPAMENTOS, 'MID_CONJUNTO', 'MID', $cc['MID_EQUIPAMENTO']);
                $cc['MID_EMPRESA'] = (int) VoltaValor(EQUIPAMENTOS, 'MID_EMPRESA', 'MID', $cc['MID_EQUIPAMENTO']);
                $empresa = $cc['MID_EMPRESA'];
                $alocando = FALSE;
            }
            // N�o � poss�vel abrir ordem para um molde que est� sendo enviado para um destino, no qual ainda possui
            // uma ordem de troca aberta para ele
            else {

                $midOrdem = (int) VoltaValor(ORDEM, 'MID', "TIPO_SERVICO IN (SELECT MID FROM " . TIPOS_SERVICOS . " WHERE TROCA = 1) AND STATUS = 1 AND MID_MAQUINA", $cc['MID_MAQUINA']);
                if ($midOrdem != 0 AND $toSetup == FALSE) {
                    $msg.= "<li><strong>{$ling['impossivel_abrir_ordem']}:</strong> O Obj. Destino possui uma ordem para troca aberta<br />
                    <strong>Num. O.S:</strong> " . VoltaValor(ORDEM, 'NUMERO', 'MID', $midOrdem) . "</li> ";
                }
            }
        }
    }
    // Para que n�o fique movimentando sem necessidade ao editar a ordem
    // como n�o ser� poss�vel modificar o molde e os dados da troca anulamos aqui
    elseif ($isTroca and $osmid != 0) {
        $isTroca = FALSE;
    }

    if (!$msg) {
        $cc['DATA_ABRE'] = (int) $d[2] . "-" . (int) $d[1] . "-" . (int) $d[0];
        $cc['DATA_PROG'] = (int) $dprog[2] . "-" . (int) $dprog[1] . "-" . (int) $dprog[0];

        if (($cc['TIPO'] == 1) or ($cc['TIPO'] == 2)) {
            $sql = "DATA_ABRE = '" . $cc['DATA_ABRE'] . "',
            DATA_PROG = '" . $cc['DATA_PROG'] . "',
            DATA_PROG_ORIGINAL = '" . $cc['DATA_PROG'] . "',
            HORA_ABRE = '" . $cc['HORA_ABRE'] . "',
            TEXTO = '" . $cc['TEXTO'] . "',
            NATUREZA = '" . (int) $cc['NATUREZA'] . "',
            SOLICITANTE = '" . $cc['SOLICITANTE'] . "',
            RESPONSAVEL = '" . (int) $cc['RESPONSAVEL'] . "',
            STATUS = '1'";
        } else {
            $sql = "
            MID_EMPRESA = '{$empresa}',
            MID_AREA = '{$area}',
            MID_SETOR = '{$setor}',
            MID_MAQUINA = '{$cc['MID_MAQUINA']}',
            MID_CONJUNTO = '{$cc['MID_CONJUNTO']}',
            MID_EQUIPAMENTO = '{$cc['MID_EQUIPAMENTO']}',
            CENTRO_DE_CUSTO = '{$centro}',
            MID_MAQUINA_FAMILIA = '{$fam}',
            MID_CLASSE = '{$classe}',
            MID_FABRICANTE = '{$fabr}',
            MID_FORNECEDOR = '{$forne}',
            DATA_PROG = '" . $cc['DATA_PROG'] . "',
            DATA_PROG_ORIGINAL = '" . $cc['DATA_PROG'] . "',
            DATA_ABRE = '" . $cc['DATA_ABRE'] . "',
            HORA_ABRE = '" . $cc['HORA_ABRE'] . "',
            TEXTO = '" . $cc['TEXTO'] . "',
            TIPO_SERVICO = '" . (int) $cc['TIPO_SERVICO'] . "',
            TIPO = '" . (int) $cc['TIPO'] . "',
            NATUREZA = '" . (int) $cc['NATUREZA'] . "',
            SOLICITANTE = '" . $cc['SOLICITANTE'] . "',
            RESPONSAVEL = '" . (int) $cc['RESPONSAVEL'] . "',
            CAUSA = '" . (int) $cc['CAUSA'] . "',
            CAUSA_TEXTO = '" . strtoupper(LimpaTexto($cc['CAUSA_TEXTO'])) . "',
            DEFEITO = '" . (int) $cc['DEFEITO'] . "',
            DEFEITO_TEXTO = '" . strtoupper(LimpaTexto($cc['DEFEITO_TEXTO'])) . "',
            SOLUCAO = '" . (int) $cc['SOLUCAO'] . "',
            MID_SPEED_CHECK = '" . (int) $cc['MID_SPEED_CHECK'] . "',
            SOLUCAO_TEXTO = '" . strtoupper(LimpaTexto($cc['SOLUCAO_TEXTO'])) . "',
            STATUS = '1'";
        }
        if ($osmid != 0) {
            $resultado = $dba[0]->Execute("UPDATE " . ORDEM_PLANEJADO . " SET $sql WHERE MID = '$osmid'");
            // Logando
            logar(4, "", ORDEM, 'MID', $osmid);
        } else {
            $osnum = GeraNumOS($empresa);
            $osmid = GeraMid(ORDEM_PLANEJADO, "MID", 0);
            $resultado = $dba[0]->Execute("INSERT INTO " . ORDEM_PLANEJADO . " SET NUMERO = '$osnum', $sql , MID = '$osmid'");
            // Logando
            logar(3, "", ORDEM, 'MID', $osmid);
        }
        if (!$resultado) {
            erromsg($dba[0]->ErrorMsg() . " $sql");
        } else {

            if ($isSpeedCheck == TRUE) {
                atualizaOrdemSpeedCheck($cc['MID_SPEED_CHECK'], $osmid);
            }

            // Tratamento para ordens de troca de molde
            if ($isTroca == TRUE) {

                /*
                 * Caso esteja alocando o componente na m�quina, � necess�rio realocar o componente (molde) que possa estar alocado no
                 * objeto de destino.
                 */
                if ($alocando == TRUE) {

                    // Verificamos se existe algum componente alocado no destino e realocamos ele em caso de nova aloca��o
                    $eqpAnt = (int) VoltaValor(EQUIPAMENTOS, 'MID', "FAMILIA IN (" . implode(',', array_values($manusis['moldes_familia'])) . ") AND MID_MAQUINA", $cc['MID_MAQUINA']);
                    if ($eqpAnt != 0 AND $toSetup == FALSE) {

                        $motivo = "Molde movimentado para n&atilde;o alocado durante procedimento de troca realizado ";
                        $motivo .= "atrav&eacute;s de Ordem de Servi&ccedil;o.<br />";
                        $motivo .= "Ordem: " . VoltaValor(ORDEM, 'NUMERO', 'MID', $osmid) . "<br />";
                        $motivo .= "Substituido por: " . VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $cc['MID_EQUIPAMENTO'], $tdb[EQUIPAMENTOS]['dba'], 1) . "";

                        MovimentaComponente($eqpAnt, 0, 0, $motivo);
                    }

                    $motivo = "Molde movimentado para a M&aacute;quina durante procedimento de troca realizado ";
                    $motivo .= "atrav&eacute;s de Ordem de Servi&ccedil;o.<br />";
                    $motivo .= "Ordem: " . VoltaValor(ORDEM, 'NUMERO', 'MID', $osmid) . "<br />";

                    MovimentaComponente($cc['MID_EQUIPAMENTO'], $cc['MID_MAQUINA'], $cc['MID_CONJUNTO'], $motivo);
                }

                /*
                 * Caso esteja apenas removendo o molde, realizamos a movimenta��o para n�o alocado
                 */ else {

                    $motivo = "Molde movimentado para n&atilde;o alocado durante procedimento de troca realizado ";
                    $motivo .= "atrav&eacute;s de Ordem de Servi&ccedil;o.<br />";
                    $motivo .= "Ordem: " . VoltaValor(ORDEM, 'NUMERO', 'MID', $osmid) . "<br />";

                    MovimentaComponente($cc['MID_EQUIPAMENTO'], 0, 0, $motivo);
                }
            }

            // MO ALOCADO
            // POR ESPECIALIDADE
            $aloc = array();
            // APAGANDO AS MÃO DE OBRA SALVA
            $sql = "DELETE FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = $osmid";
            if (!$dba[$tdb[ORDEM_MO_ALOC]['dba']]->Execute($sql)) {
                erromsg("{$ling['arquivo']}: apontaosplan.php <br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']]->ErrorMsg() . "<br />" . $sql);
                $erro = 1;
            } else {
                logar(5, $ling['ord_limpando_registros'], ORDEM_MO_ALOC, 'MID_ORDEM', $osmid);
            }
            // ERRO PELO TEMPO
            $erro_tmp = 0;

            // SALVANDO AS NOVAS
            if (is_array($cc['FUNC_ALOC'])) {
                foreach ($cc['FUNC_ALOC'] as $moa) {
                    // Formatando
                    $cc['FUNC_ALOC_TMP'][$moa] = round((float) str_replace(',', '.', $cc['FUNC_ALOC_TMP'][$moa]), 2);
                    $cc['FUNC_ALOC_TMP'][$moa] = str_replace(',', '.', $cc['FUNC_ALOC_TMP'][$moa]);

                    // Validando o tempo
                    if ($cc['FUNC_ALOC_TMP'][$moa] > 0) {
                        $esp = (int) VoltaValor(FUNCIONARIOS, 'ESPECIALIDADE', 'MID', $moa, 0);

                        // Validando tempo em relação ao previsto
                        if ((VoltaValor(ORDEM_MO_PREVISTO, 'SUM(TEMPO)', 'MID_ORDEM', $osmid) > 0) and ($cc['TIPO'] == 1 or $cc['TIPO'] == 2) and (($aloc[$esp]['t'] + $cc['FUNC_ALOC_TMP'][$moa]) > VoltaValor(ORDEM_MO_PREVISTO, 'TEMPO', array('MID_ORDEM', 'MID_ESPECIALIDADE'), array($osmid, $esp)))) {
                            erromsg("<li><strong>{$ling['mao_de_obra_prevista']}</strong>: {$ling['ord_temp_total_espec_n_maior_prev']}</li>");
                            break;
                        }

                        // Somando os tempos por especialidade
                        $aloc[$esp]['c']++;
                        $aloc[$esp]['t'] += $cc['FUNC_ALOC_TMP'][$moa];

                        $dados = array(
                            'MID' => GeraMid(ORDEM_MO_ALOC, "MID", $tdb[ORDEM_MO_ALOC]['dba']),
                            'MID_ORDEM' => $osmid,
                            'MID_FUNCIONARIO' => $moa,
                            'MID_ESPECIALIDADE' => (int) VoltaValor(FUNCIONARIOS, 'ESPECIALIDADE', 'MID', $moa, 0),
                            'TEMPO' => $cc['FUNC_ALOC_TMP'][$moa]
                        );

                        $sql = "INSERT INTO " . ORDEM_MO_ALOC . " (" . implode(", ", array_keys($dados)) . ") VALUES (" . implode(", ", array_values($dados)) . ")";
                        if (!$dba[$tdb[ORDEM_MO_ALOC]['dba']]->Execute($sql)) {
                            erromsg("{$ling['arquivo']}: apontaosplan.php <br />" . $dba[$tdb[ORDEM_MO_ALOC]['dba']]->ErrorMsg() . "<br />" . $sql);
                            $erro = 1;
                        } else {
                            logar(3, "", ORDEM_MO_ALOC, 'MID', $dados['MID']);
                        }
                    } else {
                        DeletaItem(ORDEM_MO_ALOC, array('MID_ORDEM', 'MID_FUNCIONARIO'), array($osmid, $moa));
                    }
                }
            }

            // MO previsto
            if (($cc['TIPO'] != 1) and ($cc['TIPO'] != 2)) {
                // APAGANDO AS MÃO DE OBRA SALVA
                $sql = "DELETE FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid";
                if (!$dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute($sql)) {
                    erromsg("{$ling['arquivo']}: apontaosplan.php <br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
                    $erro = 1;
                } else {
                    logar(5, $ling['ord_limpando_registros'], ORDEM_MO_PREVISTO, 'MID_ORDEM', $osmid);
                }
                // SALVANDO AS NOVAS
                foreach ($aloc as $cargo => $valores) {
                    $mo['MID'] = GeraMid(ORDEM_MO_PREVISTO, "MID", $tdb[ORDEM_MO_PREVISTO]['dba']);
                    $mo['MID_ORDEM'] = $osmid;
                    $mo['MID_ESPECIALIDADE'] = $cargo;
                    $mo['QUANTIDADE'] = $valores['c'];
                    $mo['TEMPO'] = str_replace(',', '.', $valores['t']);

                    $sql = "INSERT INTO " . ORDEM_MO_PREVISTO . " (" . implode(", ", array_keys($mo)) . ") VALUES (" . implode(", ", array_values($mo)) . ")";
                    if (!$dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute($sql)) {
                        erromsg("{$ling['arquivo']}: apontaosplan.php <br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
                        $erro = 1;
                    } else {
                        logar(3, "", ORDEM_MO_PREVISTO, 'MID', $mo['MID']);
                    }
                }
            }
            // EDITANDO AS QUANTIDADES
            else {
                // SALVANDO AS NOVAS
                foreach ($aloc as $cargo => $valores) {
                    $sql = "UPDATE " . ORDEM_MO_PREVISTO . " SET QUANTIDADE = {$valores['c']} WHERE MID_ORDEM = $osmid AND MID_ESPECIALIDADE = $cargo";
                    if (!$dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute($sql)) {
                        erromsg("{$ling['arquivo']}: apontaosplan.php <br />" . $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
                        $erro = 1;
                    }
                }
            }


            // SALVANDO AS NOVAS
            if(isset($_SESSION[ManuSess][ORDEM]['mt']))
                foreach ($_SESSION[ManuSess][ORDEM]['mt'] as $mt) {
                    $mt['MID'] = GeraMid(ORDEM_MAT_PREVISTO, "MID", $tdb[ORDEM_MAT_PREVISTO]['dba']);
                    $mt['MID_ORDEM'] = $osmid;
                    $sql = "INSERT INTO " . ORDEM_MAT_PREVISTO . " (" . implode(", ", array_keys($mt)) . ") VALUES ('" . implode("', '", array_values($mt)) . "')";
                    if (!$dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->Execute($sql)) {
                        erromsg("{$ling['arquivo']}: apontaosplan.php <br />" . $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->ErrorMsg() . "<br />" . $sql);
                        $erro = 1;
                    } else {
                        logar(3, "", ORDEM_MAT_PREVISTO, 'MID', $mt['MID']);
                    }
                }

            // Atualizando a listagem
            echo "<script>
            atualiza_area_frame3('corpo_os', 'manusis.php?st=1&id=4&op=2');
            </script>";


            if ($_POST['imprimeos'] == 1) {
                echo "<script>window.open('geraos.php?id=3&oq=$osmid','','');</script>   ";
            }
        }
    } else {
        erromsg($msg);
        $cc['DATA_PROG'] = $_POST['data_prog'];
        $cc['DATA_ABRE'] = $_POST['data_abre'];
    }
}

if ($osmid == 0) {
    if (!$cc['DATA_PROG']) {
        $cc['DATA_PROG'] = date("d/m/Y");
    }
    if (!$cc['DATA_ABRE']) {
        $cc['DATA_ABRE'] = date("d/m/Y");
    }
    $cc['HORA_ABRE'] = date("H:i:s");
    //$cc['TIPO']=0;
    echo "<h2>" . $ling['nova_ordem_serv'] . "</h2>";

    // BUSCANDO A EMPRESA CASO APENAS O TIPO DE SERVI�O TENHA SIDO SELECIONADO
    if (($cc['TIPO_SERVICO'] != 0) and ($cc['MID_EMPRESA'] == 0)) {
        $cc['MID_EMPRESA'] = (int) VoltaValor(TIPOS_SERVICOS, 'MID_EMPRESA', 'MID', $cc['TIPO_SERVICO']);
    }

    // PARA USAR NO FORMSELECD
    $whereEmp = "WHERE (MID_EMPRESA=" . (int) $cc['MID_EMPRESA'] . " OR MID_EMPRESA=0)";

    // MO PREVISTA
    $_SESSION[ManuSess][ORDEM]['mo'] = array();
    // MATERIAL PREVISTO
    if(!isset($_SESSION[ManuSess][ORDEM]['mt']))
        $_SESSION[ManuSess][ORDEM]['mt'] = array();
    
} elseif ($osmid != '') {
    $co = $tdb[ORDEM_PLANEJADO]['dba'];

    if ($_GET['ap'] != 0) {
        // Tratando o numero enviado
        $osmid = LimpaTexto($osmid);
        // Buscando
        $resultado = $dba[$co]->Execute("SELECT * FROM " . ORDEM_PLANEJADO . " WHERE NUMERO LIKE '$osmid' ORDER BY MID");
        $cc = $resultado->fields;
        if ($resultado->EOF) {
            erromsg("" . $ling['erro_os_nao_encontrada'] . "");
            exit;
        }

        $cc['TIPO'] = (int) $cc['TIPO'];

        $cc['TIPO_SERVICO'] = (int) $cc['TIPO_SERVICO'];
        $cc['DATA_ABRE'] = $resultado->UserDate($cc['DATA_ABRE'], 'd/m/Y');
        $cc['DATA_PROG'] = $resultado->UserDate($cc['DATA_PROG'], 'd/m/Y');

        $osmid = $cc['MID'];

        if ($cc['STATUS'] == 2) {
            erromsg("" . $ling['ordem_fechada_e_nao_pode_apontada'] . "");
            exit;
        }
        if ($cc['STATUS'] == 3) {
            erromsg("" . $ling['ordem_cancelada_e_nao_pode_apontada'] . "");
            exit;
        } else {
            echo "<h2>" . $ling['ordem_serv_nr'] . " " . $cc['NUMERO'] . "</h2>";
        }
    }
    // NÃO BUSCA DENOVO SE O FORMULÁRIO FOI ENVIADO
    elseif (empty($cc)) {
        // Buscando
        $resultado = $dba[$co]->Execute("SELECT * FROM " . ORDEM_PLANEJADO . " WHERE MID = '$osmid'");
        $cc = $resultado->fields;

        if ($resultado->EOF) {
            erromsg("" . $ling['nao_exist_reg_para_nr_os'] . "");
            exit;
        } else {
            if ($id <= 9) {
                echo "<h2>" . $ling['ordem_serv_nr'] . " " . $cc['NUMERO'] . "</h2>";
            }
        }

        $cc['TIPO'] = (int) $cc['TIPO'];
        $cc['TIPO_SERVICO'] = (int) $cc['TIPO_SERVICO'];
        $cc['DATA_ABRE'] = $resultado->UserDate($cc['DATA_ABRE'], 'd/m/Y');
        $cc['DATA_PROG'] = $resultado->UserDate($cc['DATA_PROG'], 'd/m/Y');

        $osmid = $cc['MID'];
    }
    // MOSTRANDO SÓ O NUMERO DA OS
    else {
        // Buscando
        $resultado = $dba[$co]->Execute("SELECT TIPO, NUMERO FROM " . ORDEM . " WHERE MID = '$osmid'");

        if ($resultado->EOF) {
            erromsg("" . $ling['nao_exist_reg_para_nr_os'] . "");
            exit;
        } else {
            echo "<h2>" . $ling['ordem_serv_nr'] . " " . $resultado->fields['NUMERO'] . "</h2>";
        }

        $cc['TIPO'] = (int) $resultado->fields['TIPO'];
        $cc['DATA_ABRE'] = $_POST['DATA_ABRE'];
        $cc['DATA_PROG'] = $_POST['data_prog'];
    }


    // BUSCANDO A EMPRESA CASO APENAS A MAQUINA TENHA SIDO SELECIONADA
    if (((int) $cc['MID_MAQUINA'] != 0) and ((int) $cc['MID_EMPRESA'] == 0)) {
        $cc['MID_EMPRESA'] = (int) VoltaValor(MAQUINAS, 'MID_EMPRESA', 'MID', $cc['MID_MAQUINA']);
    }

    // PARA USAR NO FORMSELECD
    $whereEmp = "WHERE (MID_EMPRESA = " . (int) $cc['MID_EMPRESA'] . " OR MID_EMPRESA=0)";


    // MO previstos
    $sql = "SELECT * FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid";
    $rs = $dba[$co]->Execute($sql);
    if (!$rs->EOF) {
        $_SESSION[ManuSess][ORDEM]['mo'] = $rs->getrows();
    } else {
        $_SESSION[ManuSess][ORDEM]['mo'] = array();
    }
}

$isSpeedCheck = FALSE;
if (VoltaValor(TIPOS_SERVICOS, 'SPEED_CHECK', 'MID', $cc['TIPO_SERVICO']) == 1) {
    $isSpeedCheck = TRUE;
}

$aplicacaoSpeedCheck = 0;
if ((int) $cc['MID_SPEED_CHECK'] != 0) {
    $aplicacaoSpeedCheck = VoltaValor(SPEED_CHECK, 'MID_TIPO_PLANO', 'MID', (int) $cc['MID_SPEED_CHECK']);
}

$isTroca = FALSE;
if (VoltaValor(TIPOS_SERVICOS, 'TROCA', 'MID', $cc['TIPO_SERVICO']) == 1) {
    $isTroca = TRUE;
}

/**
 * Esta vari�vel serve para sabermos se o molde ser� enviado para um p�tio de setup
 * no momento da troca do molde.
 * Quando esta variavel for TRUE apenas p�tios de setupo poder�o ser exibidos como destino.
 * Tamb�m ser�o permitidos apenas a exibi��o de moldes n�o alocados na ABERTURA de ordem
 */
$toSetup = FALSE;
if (in_array($cc['TIPO_SERVICO'], $manusis['tpo_serv_troca_setup']) AND VoltaValor(TIPOS_SERVICOS, 'TROCA', 'MID', $cc['TIPO_SERVICO']) == 1) {
    $toSetup = TRUE;
}

if ($id <= 9) {

    echo "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
<tr>";

    echo "<td class=\"" . (($id == 1) ? "aba_sel" : "aba") . "\" onclick=\"Aba('apontaosplan.php?id=1&osmid=$osmid')\">" . $ling['info_geral'] . "</td>
<td class=\"aba_entre\">&nbsp;</td>";



    if ((int) $osmid != 0) {
        if (($cc['TIPO'] == 1) or ($cc['TIPO'] == 2)) {
            echo "<td class=\"" . (($id == 2) ? "aba_sel" : "aba") . "\" onclick=\"Aba('apontaosplan.php?id=2&osmid=$osmid')\">" . $ling['atividades'] . "</td>
        <td class=\"aba_entre\">&nbsp;</td>";
        }

        echo "<td class=\"" . (($id == 3) ? "aba_sel" : "aba") . "\" onclick=\"Aba('apontaosplan.php?id=3&osmid=$osmid')\">" . $ling['tempos_recursos'] . "</td>
    <td class=\"aba_entre\">&nbsp;</td>";

        if ($isSpeedCheck == FALSE) {
            echo "<td class=\"" . (($id == 4) ? "aba_sel" : "aba") . "\" onclick=\"Aba('apontaosplan.php?id=4&osmid=$osmid')\">" . $ling['pendencias'] . "</td>
	    <td class=\"aba_entre\">&nbsp;</td>";
        } else {
            echo "<td align=\"center\" class=\"" . (($id == 9) ? "aba_sel" : "aba") . "\" width=\"19%\" onclick=\"Aba('apontaosplan.php?id=9&osmid=$osmid')\">
    		{$tdb[SPEED_CHECK_ATIVIDADES]['DESC']}
    	</td>
    	<td class=\"aba_entre\">&nbsp;</td>";
        }
        if ($isSpeedCheck == 0) {
            echo "<td class=\"" . (($id == 5) ? "aba_sel" : "aba") . "\" onclick=\"Aba('apontaosplan.php?id=5&osmid=$osmid')\">" . $ling['opcoes'] . "</td>
	    <td class=\"aba_entre\">&nbsp;</td>";
        }

        echo "<td class=\"" . (($id == 6) ? "aba_sel" : "aba") . "\" onclick=\"Aba('apontaosplan.php?id=6&osmid=$osmid')\">" . $ling['fechar'] . "</td>
    <td class=\"aba_entre\">&nbsp;</td>";
    }
    echo "<td class=\"aba_fim\">&nbsp;</td>
</tr>
<tr>
<td colspan=\"20\" class=\"aba_quadro\" valign=\"top\">
<form name=\"form\" method=\"POST\" action=\"apontaosplan.php?id=$id&osmid=$osmid&idModulo=$idModulo&op=$op \">
";
}
if ($id == 1) {
    echo "

    <fieldset><legend>" . $ling['informacoes_gerais'] . "</legend>

    <input type=\"hidden\" name=\"cc[MID]\" value=\"" . $cc['MID'] . "\" />
    <input type=\"hidden\" name=\"cc[TIPO]\" value=\"" . $cc['TIPO'] . "\" />";

    if (VoltaPermissao($idModulo, $op) == 1) {
        FormData("{$tdb[ORDEM]['DATA_PROG']}:", "data_prog", $cc['DATA_PROG'], "campo_label", "", "campo_text_ob");

        echo "<br clear=\"all\" />";
        FormData("{$tdb[ORDEM]['DATA_ABRE']}:", "DATA_ABRE", $cc['DATA_ABRE'], "campo_label", "", "campo_text_ob");

        echo "<br clear=\"all\" />";
        echo "<label class=\"campo_label\" for=\"cc[HORA_ABRE]\">" . $tdb[ORDEM]['HORA_ABRE'] . "</label>
        <input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"cc[HORA_ABRE]\" class=\"campo_text\" name=\"cc[HORA_ABRE]\" size=\"8\" maxlength=\"8\"  value=\"" . $cc['HORA_ABRE'] . "\"/>";
    } else {
        echo "<input type=\"hidden\" name=\"cc[DATA_PROG]\" value=\"" . $cc['DATA_PROG'] . "\" />
        <input type=\"hidden\" name=\"DATA_ABRE\" value=\"" . $cc['DATA_ABRE'] . "\" />
        <input type=\"hidden\" name=\"cc[HORA_ABRE]\" value=\"" . $cc['HORA_ABRE'] . "\" />";
    }

    // Mostrando o MID_EMPRESA para ROTA e PREVENTIVA
    if (($cc['TIPO'] == 1) or ($cc['TIPO'] == 2)) {
        echo "
        <input type=\"hidden\" name=\"cc[MID_MAQUINA]\" value=\"" . $cc['MID_MAQUINA'] . "\" />\n
        <input type=\"hidden\" name=\"cc[MID_EMPRESA]\" value=\"" . $cc['MID_EMPRESA'] . "\" />\n
        ";
    }

    if ($cc['TIPO'] == 1) {
        echo "<br clear=\"all\" />
        <label class=\"campo_label\" for=\"cc[MID_MAQUINA]\">" . $tdb[ORDEM]['MID_MAQUINA'] . ": </label>
        <input type=\"text\" id=\"cc[MID_MAQUINA]\" class=\"campo_text_ob\" value=\"" . VoltaValor(MAQUINAS, "DESCRICAO", "MID", $cc['MID_MAQUINA'], 0) . "\"  size=\"60\" disabled=\"disabled\" />";

        if ($cc['MID_CONJUNTO'] != 0) {
            echo "<br clear=\"all\" />
            <label class=\"campo_label\" for=\"cc[MID_CONJUNTO]\">" . $tdb[ORDEM]['MID_CONJUNTO'] . ": </label>
            <input type=\"text\" id=\"cc[MID_CONJUNTO]\" class=\"campo_text\" value=\"" . VoltaValor(MAQUINAS_CONJUNTO, "DESCRICAO", "MID", $cc['MID_CONJUNTO'], 0) . "\"  size=\"60\" disabled=\"disabled\" />";
        }
        if ($cc['MID_EQUIPAMENTO'] != 0) {
            echo "<br clear=\"all\" />
            <label class=\"campo_label\" for=\"cc[MID_EQUIPAMENTO]\">" . $tdb[ORDEM]['MID_EQUIPAMENTO'] . ": </label>
            <input type=\"text\" id=\"cc[MID_EQUIPAMENTO]\" class=\"campo_text\" value=\"" . VoltaValor(EQUIPAMENTOS, "DESCRICAO", "MID", $cc['MID_EQUIPAMENTO'], 0) . "\"  size=\"60\" disabled=\"disabled\" />";
        }
    } elseif (($cc['TIPO'] == 0) or ($cc['TIPO'] == 4) or ($osmid == 0)) {

        // PERMISSÃO POR CENTRO-DE-CUSTO
        $filtroMaquina = "";
        if (($_SESSION[ManuSess]['user']['MID'] != 0) and (VoltaValor(USUARIOS_PERMISAO_CENTRODECUSTO, 'MID', 'USUARIO', (int) $_SESSION[ManuSess]['user']['MID']) != 0)) {
            $filtroMaquina = " WHERE CENTRO_DE_CUSTO IN (SELECT CENTRO_DE_CUSTO FROM " . USUARIOS_PERMISAO_CENTRODECUSTO . " WHERE USUARIO = '{$_SESSION[ManuSess]['user']['MID']}')";
        }

        // N�O DEIXA MUDAR A EMPRESA DEPOIS DE GERADA A OS
        if ($osmid != 0) {
            $filtroMaquina .= ($filtroMaquina == '') ? "WHERE " : " AND ";
            $filtroMaquina .= "MID_EMPRESA = " . (int) $cc['MID_EMPRESA'];
            // Caso a ordem n�o seja de troca, n�o ser� poss�vel modificar para troca
            $filtroServ = $whereEmp;
        }else{
            if ($_SESSION[ManuSess]['user']['MID'] != "ROOT") {
               $filtroServ = "WHERE ".VoltaFiltroEmpresa(TIPOS_SERVICOS, 2, false);
            }
            elseif((int) $cc['MID_EMPRESA'] == 0) {
                $filtroServ = '';
            }
            else{
                $filtroServ = $whereEmp;
            }
        }
        echo "<br clear=\"all\" />";



        // Caso o usu�rio seja do grupo de moldes, ver� apenas os tipos de servi�o de checklist
        if ((int) VoltaValor(USUARIOS_GRUPOS, 'GRUPO_MOLDES', 'MID', (int) $_SESSION[ManuSess]['user']['GRUPO']) == 1) {

            if ($filtroServ != '')
                $filtroServ .= " AND ";
            else
                $filtroServ .= "  WHERE ";
            $filtroServ .= "SPEED_CHECK = 1";
        }

        if (!$isTroca AND $osmid != 0) {
            if ($filtroServ != '')
                $filtroServ .= " AND ";
            else
                $filtroServ .= "  WHERE ";
            $filtroServ .= "(TROCA != 1 OR TROCA IS NULL)";

            echo "<label class=\"campo_label\" for=\"cc[TIPO_SERVICO]\">" . $tdb[ORDEM]['TIPO_SERVICO'] . " </label>";
            FormSelectD('DESCRICAO', '', TIPOS_SERVICOS, $cc['TIPO_SERVICO'], "cc[TIPO_SERVICO]", "cc[TIPO_SERVICO]", 'MID', "TIPO_SERVICO", 'campo_select_ob', "this.form.submit() ", $filtroServ);
        }

        // Caso seja uma nova ordem mostra todas as op��es de tipo de servi�o
        elseif ($osmid == 0) {

            echo "<label class=\"campo_label\" for=\"cc[TIPO_SERVICO]\">" . $tdb[ORDEM]['TIPO_SERVICO'] . " </label>";
            FormSelectD('DESCRICAO', '', TIPOS_SERVICOS, $cc['TIPO_SERVICO'], "cc[TIPO_SERVICO]", "cc[TIPO_SERVICO]", 'MID', "TIPO_SERVICO", 'campo_select_ob', "this.form.submit() ", $filtroServ);
        }

        // em caso de abertura de ordem de troca a prosi��o dos elementos e labels muda
        if ($isTroca and $osmid == 0) {

            $atualizaAreaSpeedCheck = "";
            // Caso seja um speedcheck atualiza o plano pela fam�lia do componente
            if ($isSpeedCheck) {
                $atualizaAreaSpeedCheck = "atualiza_area2('planoSpeedCheck', '?id=19&osmid=$osmid&midComponente=' + this.value + '')";
            }
            echo "<br clear=\"all\" />";
            echo "<label class=\"campo_label \"for=\"cc[MID_EQUIPAMENTO]\">Molde:</label>";
            $filtro_equip = "WHERE MID_STATUS = 1 AND FAMILIA IN (" . implode(',', array_values($manusis['moldes_familia'])) . ")";

            /**
             * Quando o tipo de servi�o for $toSetup exibem-se apenas moldes n�o alocados
             */
            if ($toSetup == TRUE) {
                $filtro_equip .= $filtro_equip == "" ? " WHERE " : " AND ";
                $filtro_equip .= " (MID_MAQUINA IS NULL OR MID_MAQUINA = 0)";
            }
            /**
             * Quando isso n�o acontecer, apenas os moldes j� revisados, ou seja,
             * que est�o alocados nos p�tios de setup poder�o ser selecionados
             */ else {
                $filtro_equip .= $filtro_equip == "" ? " WHERE " : " AND ";
                $filtro_equip .= " MID_MAQUINA IN (" . implode(',', array_values($manusis['patio_setup'])) . ")";
            }


            FormSelectD('COD', 'DESCRICAO', EQUIPAMENTOS, $cc['MID_EQUIPAMENTO'], "cc[MID_EQUIPAMENTO]", "cc[MID_EQUIPAMENTO]", 'MID', '', 'campo_select_ob', "atualiza_area2('dadosEquip', '?id=16&osmid=$osmid&midEquip=' + this.value + '');{$atualizaAreaSpeedCheck}", "$filtro_equip", 'A', 'COD', "");

            echo "<br clear=\"all\" />";

            echo "<span id='dadosEquip' style='text-align:left'></span>";


            // Se for SpeedCheck ou Troca Filtra pela Familia MDL
            if ($isSpeedCheck OR $isTroca) {
                $filtroMaquina .= $filtroMaquina == "" ? " WHERE " : " AND ";
                $filtroMaquina .= " FAMILIA IN (" . implode(',', $manusis['familias_destino_troca']) . ") ";
            }

            $primeiro_campo = "";
            // Caso seja um tipo de servi�o de revis�o, os �nicos destinos poss�veis s�o os p�tios de revis�o
            if ($toSetup == TRUE) {
                $filtroMaquina .= $filtroMaquina == "" ? " WHERE " : " AND ";
                $filtroMaquina .= " MID IN (" . implode(',', array_values($manusis['patio_setup'])) . ")";
            }
            // Molde n�o pode sair da m�quina e voltar diretamente para o p�tio de setup
            else {
                $filtroMaquina .= $filtroMaquina == "" ? " WHERE " : " AND ";
                $filtroMaquina .= " MID NOT IN (" . implode(',', array_values($manusis['patio_setup'])) . ")";
            }
            echo "<label class=\"campo_label\"for=\"cc[MID_MAQUINA]\">Obj. Destino:</label>";
            FormSelectD('COD', 'DESCRICAO', MAQUINAS, $cc['MID_MAQUINA'], "cc[MID_MAQUINA]", "cc[MID_MAQUINA]", 'MID', '', 'campo_select_ob', "this.form.submit()", $filtroMaquina, 'A', 'COD', $primeiro_campo);

            echo "<br clear='all' />";

            echo "<label class=\"campo_label\" for=\"cc[MID_CONJUNTO]\">Pos. Destino:</label>";
            FormSelectD('TAG', 'DESCRICAO', MAQUINAS_CONJUNTO, $cc['MID_CONJUNTO'], "cc[MID_CONJUNTO]", "cc[MID_CONJUNTO]", 'MID', '', 'campo_select_ob', "", "WHERE MID_MAQUINA ='" . $cc['MID_MAQUINA'] . "'", 'A', 'TAG', "");
        }

        // Na edi��o de ordem de troca n�o � poss�vel modificar os campos pois afetaria tora a movimenta��o
        elseif ($isTroca and $osmid != 0) {

            echo "<br clear=\"all\" />";

            echo "<label class=\"campo_label\" for=\"cc[TIPO_SERVICO]\">" . $tdb[ORDEM]['TIPO_SERVICO'] . " </label>";
            echo "<span class='campo_text_ob'>" . VoltaValor(TIPOS_SERVICOS, 'DESCRICAO', 'MID', $cc['TIPO_SERVICO'], $tdb[TIPOS_SERVICOS]['dba'], 1) . "</span>";
            echo "<input type='hidden' name='cc[TIPO_SERVICO]' value='{$cc['TIPO_SERVICO']}'/>";

            echo "<br clear=\"all\" />";

            echo "<label class=\"campo_label \"for=\"cc[MID_EQUIPAMENTO]\">Molde:</label>";
            echo "<span class='campo_text_ob'>" . VoltaValor(EQUIPAMENTOS, 'DESCRICAO', 'MID', $cc['MID_EQUIPAMENTO'], $tdb[EQUIPAMENTO]['dba'], 1) . "</span>";
            echo "<input type='hidden' name='cc[MID_EQUIPAMENTO]' value='{$cc['MID_EQUIPAMENTO']}'/>";

            echo "<br clear=\"all\" />";

            echo "<label class=\"campo_label\"for=\"cc[MID_MAQUINA]\">Obj. Destino:</label>";
            $midMaqDest = (int) Voltavalor(EQUIPAMENTOS, 'MID_MAQUINA', 'MID', $cc['MID_EQUIPAMENTO']);
            $midConjDest = (int) Voltavalor(EQUIPAMENTOS, 'MID_CONJUNTO', 'MID', $cc['MID_EQUIPAMENTO']);
            if ($midMaqDest != 0) {
                $maq = VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $midMaqDest, $tdb[MAQUINAS]['dba'], 1);
            } else {
                $maq = $ling['componente_nao_alocado'];
            }

            echo "<span class='campo_text_ob'>" . $maq . "</span>";
            echo "<input type='hidden' name='cc[MID_MAQUINA]' value='{$cc['MID_MAQUINA']}'/>";

            echo "<br clear=\"all\" />";

            $conj = '&nbsp;';
            if ($midConjDest != 0) {
                $conj = VoltaValor(MAQUINAS_CONJUNTO, 'DESCRICAO', 'MID', $midConjDest, $tdb[MAQUINAS_CONJUNTO]['dba'], 1);
            } else {
                $conj = $maq = $ling['componente_nao_alocado'];
            }
            echo "<label class=\"campo_label \"for=\"cc[MID_CONJUNTO]\">Pos. Destino:</label>";
            echo "<span class='campo_text_ob'>" . $conj . "</span>";
            echo "<input type='hidden' name='cc[MID_CONJUNTO]' value='{$cc['MID_CONJUNTO']}'/>";
        } else {

            echo "<span style=\"text-align:left\" id=\"equip\">";
            echo "<br clear=\"all\" />
            <label class=\"campo_label \"for=\"cc[MID_EQUIPAMENTO]\">" . $tdb[ORDEM]['MID_EQUIPAMENTO'] . ":</label>";

            $filtro_equip = "";
            $formSubmit = "";

            if ((int) $cc['MID_CONJUNTO'] != 0) {
                $filtro_equip .= " AND MID_CONJUNTO = " . (int) $cc['MID_CONJUNTO'];
            }

            if ((int) $cc['MID_MAQUINA'] != 0) {
                $filtro_equip .= " AND MID_MAQUINA = " . (int) $cc['MID_MAQUINA'];
            }

            FormSelectD('COD', 'DESCRICAO', EQUIPAMENTOS, $cc['MID_EQUIPAMENTO'], "cc[MID_EQUIPAMENTO]", "cc[MID_EQUIPAMENTO]", 'MID', '', '', "this.form.submit();", "WHERE MID_STATUS = 1 AND MID_MAQUINA != 0 $filtro_equip", 'A', 'COD', "");
            echo "</span>";

            /*
              Caso seja selecionado o componente primeiro, a m�quina e o conjunto deve
              ser preenchido autom�ticamente com a m�quina e o conjunto onde o componente est� alocado
             */
            if ((int) $cc['MID_MAQUINA'] == 0 AND (int) $cc['MID_CONJUNTO'] == 0 AND (int) $cc['MID_EQUIPAMENTO'] != 0) {

                $cc['MID_MAQUINA'] = (int) VoltaValor(EQUIPAMENTOS, 'MID_MAQUINA', 'MID', $cc['MID_EQUIPAMENTO']);
                $cc['MID_CONJUNTO'] = (int) VoltaValor(EQUIPAMENTOS, 'MID_CONJUNTO', 'MID', $cc['MID_EQUIPAMENTO']);
            }

            // Se for SpeedCheck ou Troca Filtra pela Familia MDL
            if ($isSpeedCheck OR $isTroca) {
                $filtroMaquina .= $filtroMaquina == "" ? " WHERE " : " AND ";
                $filtroMaquina .= " FAMILIA IN (" . implode(',', $manusis['familias_destino_troca']) . ") ";
            }
            echo "<span style=\"text-align:left\" id=\"maq\">";
            echo "<br clear=\"all\" />
            <label class=\"campo_label\"for=\"cc[MID_MAQUINA]\">" . $tdb[ORDEM]['MID_MAQUINA'] . ":</label>";
            FormSelectD('COD', 'DESCRICAO', MAQUINAS, $cc['MID_MAQUINA'], "cc[MID_MAQUINA]", "cc[MID_MAQUINA]", 'MID', '', 'campo_select_ob', "this.form.submit();", $filtroMaquina, 'A', 'COD', "");
            echo "</span>";

            echo "<span style=\"text-align:left\" id=\"pos\">";
            echo "<br clear=\"all\" />
            <label class=\"campo_label \"for=\"cc[MID_CONJUNTO]\">" . $tdb[ORDEM]['MID_CONJUNTO'] . ":</label>";
            FormSelectD('TAG', 'DESCRICAO', MAQUINAS_CONJUNTO, $cc['MID_CONJUNTO'], "cc[MID_CONJUNTO]", "cc[MID_CONJUNTO]", 'MID', '', '', "atualiza_area2('equip','apontaosplan.php?osmid=$osmid&id=12&pos=' + this.options[this.selectedIndex].value)", "WHERE MID_MAQUINA ='" . $cc['MID_MAQUINA'] . "'", 'A', 'TAG', "");
            echo "</span>";
        }
    }

    if ($isSpeedCheck == TRUE) {

        $midComponente = (int) $_GET['midComponente'];
        $midMaquina = (int) $_GET['midMaquina'];
        $filtroFamilia = "";

        // Fam�lia da m�quina
        if ((int) $cc['MID_MAQUINA'] != 0) {

            $maqFam = (int) VoltaValor(MAQUINAS, 'FAMILIA', 'MID', (int) $cc['MID_MAQUINA']);
            $filtroFamilia .= "WHERE MID_FAMILIA = {$maqFam}";
        }

        // Fam�lia do componente
        if ((int) $cc['MID_EQUIPAMENTO'] != 0) {

            if ($filtroFamilia != "")
                $filtroFamilia .= " OR ";
            else
                $filtroFamilia .= " WHERE ";

            $compFam = VoltaValor(EQUIPAMENTOS, 'FAMILIA', 'MID', (int) $cc['MID_EQUIPAMENTO']);
            $filtroFamilia .= "MID_FAMILIA_COMPONENTE = {$compFam}";
        }

        echo "<br clear=\"all\" />";
        echo "<label class=\"campo_label\" for=\"cc[MID_SPEED_CHECK]\">" . $tdb[SPEED_CHECK]['DESC'] . " </label>";
        echo "<span id='planoSpeedCheck' style='text-align:left;'>";


        $filtroFamilia .= ($filtroFamilia == "") ? " WHERE  " : " AND ";
        $midPlanoTmp = VoltaValor(TIPOS_SERVICOS, "MID_PLANO", "MID", $cc['TIPO_SERVICO']);
        $filtroFamilia .= "  MID = $midPlanoTmp";

        FormSelectD('DESCRICAO', '', SPEED_CHECK, $cc['MID_SPEED_CHECK'], "cc[MID_SPEED_CHECK]", "cc[MID_SPEED_CHECK]", 'MID', "", 'campo_select_ob', "", $filtroFamilia);
        echo "</span>";
    }


    if (($osmid == 0) or ($cc['TIPO'] != 2)) {
        echo "<br clear=\"all\" />
        <label class=\"campo_label\" for=\"cc[NATUREZA]\">" . $tdb[ORDEM]['NATUREZA'] . "</label>";
        FormSelectD('DESCRICAO', '', NATUREZA_SERVICOS, $cc['NATUREZA'], "cc[NATUREZA]", "cc[NATUREZA]", 'MID', "NATUREZA", '', '', $whereEmp);
    }

    //if ($cc['TIPO'] == 0) {
    if ((($osmid == 0) or ($cc['TIPO'] == 0)) and (VoltaPermissao($idModulo, $op) == 1) or (VoltaPermissao($idModulo, $op) == 2)) {
        echo "<br clear=\"all\"/>
        <label class=\"campo_label\" for=\"cc[SOLICITANTE]\">" . $tdb[ORDEM]['SOLICITANTE'] . "</label>
        <input type=\"text\" id=\"cc[SOLICITANTE\" name=\"cc[SOLICITANTE]\" class=\"campo_text\" value=\"" . $cc['SOLICITANTE'] . "\"  size=\"50\"  /> ";
    } elseif ($cc['TIPO'] == 4) {
        echo "<br clear=\"all\"/>
        <label class=\"campo_label\" for=\"cc[SOLICITANTE]\">" . $tdb[ORDEM]['SOLICITANTE'] . "</label>
        <input type=\"text\" id=\"cc[SOLICITANTE\" name=\"cc[SOLICITANTE]\" class=\"campo_text\" value=\"" . $cc['SOLICITANTE'] . "\" readonly=\"readonly\" size=\"50\"  /> ";
    }


    if (((VoltaPermissao($idModulo, $op) == 1) or (VoltaPermissao($idModulo, $op) == 2)) AND $isSpeedCheck == 0) {
        echo "<br clear=\"all\" />
        <label class=\"campo_label\" for=\"cc[RESPONSAVEL]\">" . $tdb[ORDEM]['RESPONSAVEL'] . "</label>";
        FormSelectD('MATRICULA', 'NOME', FUNCIONARIOS, $cc['RESPONSAVEL'], "cc[RESPONSAVEL]", "cc[RESPONSAVEL]", 'MID', "FUNCIONARIO", '', '', $whereEmp, 'A', 'NOME');
    }

    echo"<br clear=\"all\"/>
    <label class=\"campo_label\" for=\"cc[TEXTO]\">" . $tdb[ORDEM]['TEXTO'] . "</label>
    <textarea class=\"campo_text\" name=\"cc[TEXTO]\" id=\"cc[TEXTO]\" rows=\"7\" cols=\"50\">" . $cc['TEXTO'] . "</textarea>

    </fieldset>";


    if ($isSpeedCheck == 0) {

        echo "<fieldset><legend>" . $ling['mao_de_obra_prevista'] . "</legend>";

        // Filtro por equipe
        echo "<label class=\"campo_label\" for=\"fil_equip\">" . $tdb[EQUIPES]['DESC'] . ":</label>";

        if ($cc['MID_SPEED_CHECK'] != 0) {
            $midEquipe = VoltaValor(SPEED_CHECK, 'MID_EQUIPE', 'MID', $cc['MID_SPEED_CHECK']);
            $whereEmp .= " AND MID = $midEquipe ";
            $equip2 = $midEquipe;
        }

        echo "<div id='equipes' style='text-align:left'>";
        FormSelectD('DESCRICAO', '', EQUIPES, '', 'fil_equip', 'fil_equip', 'MID', '', '', "atualiza_area2('func_aloc', 'apontaosplan.php?id=13&osmid=$osmid&emp={$cc['MID_EMPRESA']}&equip2=$equip2&equip=' + this.value)", $whereEmp);
        echo "</div>";
        echo "<br clear=\"all\" />";

        // Alerta sobre a necessidade de mais de um funcionário
        if (($cc['TIPO'] == 1) or ($cc['TIPO'] == 2)) {
            if ($cc['TIPO'] == 1) {
                $sql_atv = "SELECT NUMERO FROM " . ORDEM_PREV . " WHERE MID_ORDEM = $osmid AND QUANTIDADE_MO > 1";
                $dba_atv = $tdb[ORDEM_PREV]['dba'];
            } elseif ($cc['TIPO'] == 2) {
                $sql_atv = "SELECT NUMERO FROM " . ORDEM_LUB . " WHERE MID_ORDEM = $osmid AND QUANTIDADE_MO > 1";
                $dba_atv = $tdb[ORDEM_LUB]['dba'];
            }
            if (!$rs_tmp = $dba[$dba_atv]->Execute($sql_atv)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . " <br />{$ling['linha']}: " . __LINE__ . " <br />" . $dba[$dba_atv]->ErrorMsg() . " <br />" . $sql_atv);
            }
            $atv_alerta = "";
            while (!$rs_tmp->EOF) {
                $atv_alerta .= ($atv_alerta != "") ? ", " : "";
                $atv_alerta .= $rs_tmp->fields['NUMERO'];
                $rs_tmp->MoveNext();
            }
            if ($atv_alerta != "") {
                echo "<strong>{$ling['ord_alerta_atv_num']} $atv_alerta {$ling['ord_alerta_atv_num_continuacao']}</strong>";
            }
        }
        echo "<div id=\"func_aloc\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <td>" . $tdb[ORDEM_MO_ALOC]['MID_FUNCIONARIO'] . "</td>
    <td>" . $tdb[ORDEM_MO_ALOC]['MID_ESPECIALIDADE'] . "</td>
    <td>" . $tdb[FUNCIONARIOS]['EQUIPE'] . "</td>
    <td>" . $tdb[ORDEM_MO_ALOC]['MID_ORDEM'] . "</td>
    <td>" . $tdb[ORDEM_MO_ALOC]['TEMPO'] . " (h)</td>
    </tr>";

        // Mostrar todos apenas se menos que
        $maxFunc = 15;

        // Buscando todos os funcionarios DA EMPRESA
        $fil_emp = " AND MID_EMPRESA = '" . (int) $cc['MID_EMPRESA'] . "'";

        if (($cc['TIPO'] == 1) or ($cc['TIPO'] == 2)) {
            $sql = "SELECT MID, NOME, ESPECIALIDADE, EQUIPE FROM " . FUNCIONARIOS . " WHERE ESPECIALIDADE IN (SELECT MID_ESPECIALIDADE FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = $osmid) AND SITUACAO = 1 AND FORNECEDOR = 0 $fil_emp ORDER BY NOME";
        } else {
            $sql = "SELECT MID, NOME, ESPECIALIDADE, EQUIPE FROM " . FUNCIONARIOS . " WHERE SITUACAO = 1 AND FORNECEDOR = 0 $fil_emp ORDER BY NOME";
        }

        if (!$rs = $dba[$tdb[FUNCIONARIOS]['dba']]->Execute($sql)) {
            erromsg("{$ling['arquivo']}: " . __FILE__ . " <br />{$ling['linha']}: " . __LINE__ . " <br />" . $dba[$tdb[FUNCIONARIOS]['dba']]->ErrorMsg() . " <br />" . $sql);
        }
        while (!$rs->EOF) {
            // Buscando OS aberta em que o funcionario possa estar alocado
            $sql = "SELECT O.MID, O.NUMERO, O.MID_MAQUINA FROM " . ORDEM . " O, " . ORDEM_MO_ALOC . " A WHERE O.MID = A.MID_ORDEM AND O.MID != $osmid AND O.STATUS = 1 AND O.DATA_PROG = '" . DataSQL($cc['DATA_PROG']) . "' AND A.MID_FUNCIONARIO = {$rs->fields['MID']} ORDER BY O.NUMERO DESC";
            if (!$rsf = $dba[$tdb[ORDEM]['dba']]->SelectLimit($sql, 1, 0)) {
                erromsg("{$ling['arquivo']}: " . __FILE__ . " <br />{$ling['linha']}: " . __LINE__ . " <br />" . $dba[$tdb[FUNCIONARIOS]['dba']]->ErrorMsg() . " <br />" . $sql);
            }
            if (!$rsf->EOF) {
                $func_osm = $rsf->fields['MID'];
                $func_os = $rsf->fields['NUMERO'];
                $func_cli = htmlentities(VoltaValor(MAQUINAS, "DESCRICAO", "MID", $rsf->fields['MID_MAQUINA'], 0));
            } else {
                $func_osm = "";
                $func_os = "";
                $func_cli = "";
            }

            $check = "";
            if (VoltaValor(ORDEM_MO_ALOC, 'MID', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $rs->fields['MID'], 0) != "") {
                $check = "checked=\"checked\"";
            }

            // Mostrando tempo
            $tempo = VoltaValor(ORDEM_MO_ALOC, 'TEMPO', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $rs->fields['MID'], 0);


            // CARGO
            $esp = (int) VoltaValor(ORDEM_MO_ALOC, 'MID_ESPECIALIDADE', "MID_ORDEM = $osmid AND MID_FUNCIONARIO", $rs->fields['MID'], 0);

            if ($esp == 0) {
                $esp = $rs->fields['ESPECIALIDADE'];
            }

            if (($check != '') or ($rs->RowCount() <= $maxFunc)) {
                echo "<tr class=\"cor2\">
            <td><input type=\"checkbox\" name=\"cc[FUNC_ALOC][{$rs->fields['MID']}]\" id=\"func_{$rs->fields['MID']}\" value=\"{$rs->fields['MID']}\" onclick=\"ajax_get('apontaosplan.php?id=14&osmid=$osmid&func={$rs->fields['MID']}&st=' + this.checked + '&tempo=' + document.getElementById('cc[FUNC_ALOC_TMP][{$rs->fields['MID']}]').value)\" $check /><label for=\"func_{$rs->fields['MID']}\">" . htmlentities($rs->fields['NOME']) . "</label></td>
            <td>" . htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $esp)) . "</td>
            <td>" . htmlentities(VoltaValor(EQUIPES, 'DESCRICAO', 'MID', $rs->fields['EQUIPE'])) . "</td>
            <td><a href=\"javascript:janela('detalha_ord.php?busca=$func_osm', 'parm', 500,400)\" title=\"$func_cli\">$func_os</a></td>
            <td><input type=\"text\" size=\"5\" maxlength=\"10\" class=\"campo_text\" id=\"cc[FUNC_ALOC_TMP][{$rs->fields['MID']}]\" name=\"cc[FUNC_ALOC_TMP][{$rs->fields['MID']}]\" value=\"" . $tempo . "\" onblur=\"ajax_get('apontaosplan.php?id=14&osmid=$osmid&func={$rs->fields['MID']}&st=' + document.getElementById('func_{$rs->fields['MID']}').checked + '&tempo=' + this.value)\" /></td>
            </tr>";
            }

            $rs->MoveNext();
        }

        if ($rs->RowCount() > $maxFunc) {
            echo "<tr>
        <td colspan=\"5\" align=\"center\">" . $ling['apenas_os_func_aloca_mostra_use_filtro_eq_ve_dema'] . "</td>
        </tr>";
        }

        echo "</table>
    </div>
    </fieldset>";
    }

    if ($isSpeedCheck == 0) {
        echo "<fieldset><legend>" . $tdb[ORDEM_MO_PREVISTO]['DESC'] . "</legend>";

        echo "<div id=\"lista_mo\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <td>" . $tdb[ORDEM_MO_PREVISTO]['MID_ESPECIALIDADE'] . "</td>
    <td>" . $tdb[ORDEM_MO_PREVISTO]['QUANTIDADE'] . "</td>
    <td>" . $tdb[ORDEM_MO_PREVISTO]['TEMPO'] . " (h)</td>
    </tr>";

        foreach ($_SESSION[ManuSess][ORDEM]['mo'] as $i => $mo) {
            $esp = VoltaValor(ESPECIALIDADES, "DESCRICAO", "MID", $mo['MID_ESPECIALIDADE'], 0);
            $esp = ($esp != "") ? $esp : $ling['ord_sem_esp'];
            echo "<tr class=\"cor2\">
        <td>" . htmlentities($esp) . "</td>
        <td>" . $mo['QUANTIDADE'] . "</td>
        <td>" . $mo['TEMPO'] . "</td>
        </tr>";
        }

        echo "</table>
    </div>";

        echo "</fieldset>";
    }
    if ($isSpeedCheck == 0) {
        echo "<fieldset><legend>" . $tdb[ORDEM_MAT_PREVISTO]['DESC'] . "</legend>\n";

        // Apenas para não sistematicas
        if (($cc['TIPO'] != 1) and ($cc['TIPO'] != 2)) {
            echo "<label for=\"mid_mat\" class=\"campo_label\">" . $tdb[ORDEM_MAT_PREVISTO]['MID_MATERIAL'] . "</label>";
            FormSelectD('COD', 'DESCRICAO', MATERIAIS, '', 'mid_mat', 'mid_mat', 'MID', 'MATERIAIS', 'campo_select', "atualiza_area2('uni_mat', 'parametros.php?id=c_unidade&valor=' + this.value)");
            echo "<br clear=\"all\" />";

            echo "<label for=\"quant\" class=\"campo_label\">" . $tdb[ORDEM_MAT_PREVISTO]['QUANTIDADE'] . "</label>";
            echo "<input type=\"text\" class=\"campo_text\" name=\"quant\" id=\"quant\" size=\"5\" value=\"\"  /> <span id=\"uni_mat\"></span>";

            echo "<input type=\"button\" class=\"botao\" name=\"env_mat\" id=\"env_mat\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('lista_mt', 'apontaosplan.php?osmid=$osmid&id=15&add=1&mid_mat=' + document.getElementById('mid_mat').value + '&quant=' + document.getElementById('quant').value)\" />";
            echo "<br clear=\"all\" />";
            echo "<br clear=\"all\" />";
        }

        echo "<div id=\"lista_mt\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <td align=\"left\">" . $tdb[ORDEM_MAT_PREVISTO]['MID_MATERIAL'] . "</td>
    <td align=\"left\">{$tdb[ORDEM_MAT_PREVISTO]['QUANTIDADE']}</td>
    <td align=\"left\">{$tdb[ORDEM_MAT_PREVISTO]['CUSTO']}</td>
    <td>&nbsp;</td>
    </tr>";
    
    if($osmid){
        
            $rs = $dba[$tdb[ORDEM_MAT_PREVISTO]['dba']]->execute($q= "select * from " . ORDEM_MAT_PREVISTO . " where MID_ORDEM = '{$osmid}'");
            while (!$rs->EOF) {
                $mt = $rs->fields;
                // Unidade
                $uni = VoltaValor(MATERIAIS, "UNIDADE", "MID", $mt['MID_MATERIAL'], 0);
                echo "<tr class=\"cor2\">
                <td align=\"left\">" . htmlentities(VoltaValor(MATERIAIS, "DESCRICAO", "MID", $mt['MID_MATERIAL'], 0)) . "</td>
                <td align=\"left\">" . $mt['QUANTIDADE'] . " " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", $uni, 0)) . "</td>
                <td align=\"left\">" . number_format($mt['CUSTO'], 2, ',', '.') . "</td>
                <td><a href=\"javascript: atualiza_area2('lista_mt', 'apontaosplan.php?osmid=$osmid&id=15&del={$mt['MID']}')\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" /></a></td>
                </tr>";
                $rs->MoveNext();
            }
            
        }else{
            foreach ($_SESSION[ManuSess][ORDEM]['mt'] as $i => $mt) {
                $del = $i + 1;
                // Unidade
                $uni = VoltaValor(MATERIAIS, "UNIDADE", "MID", $mt['MID_MATERIAL'], 0);

                echo "<tr class=\"cor2\">
                <td align=\"left\">" . htmlentities(VoltaValor(MATERIAIS, "DESCRICAO", "MID", $mt['MID_MATERIAL'], 0)) . "</td>
                <td align=\"left\">" . $mt['QUANTIDADE'] . " " . htmlentities(VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", $uni, 0)) . "</td>
                <td align=\"left\">" . number_format($mt['CUSTO'], 2, ',', '.') . "</td>
                <td><a href=\"javascript: atualiza_area2('lista_mt', 'apontaosplan.php?osmid=$osmid&id=15&del=$del')\"><img src=\"imagens/icones/22x22/del.png\" border=\"0\" /></a></td>
                </tr>";
               }             
            
        }
        echo "</table>
    </div>
    </fieldset>";
    }

    if ((($cc['TIPO'] == 0) or ($osmid == 0) or ($cc['TIPO'] == 4)) and (VoltaPermissao($idModulo, $op) == 1) or (VoltaPermissao(4, $op) == 2)) {

        if ($isSpeedCheck == 0) {

            echo "
	        <fieldset><legend>{$ling['ord_causa_defeito_sol']}</legend>\n";

            echo "<label class=\"campo_label\" for=\"cc[DEFEITO]\">{$tdb[ORDEM]['DEFEITO']}:</label>";
            FormSelectD('DESCRICAO', '', DEFEITO, $cc['DEFEITO'], "cc[DEFEITO]", "cc[DEFEITO]", 'MID', "DEFEITO", '', '', $whereEmp);
            echo "<br clear=\"all\" />

	        <label class=\"campo_label\" for=\"cc[DEFEITO_TEXTO]\">{$tdb[ORDEM]['DEFEITO_TEXTO']}:</label>
	        <textarea class=\"campo_text\" name=\"cc[DEFEITO_TEXTO]\" id=\"cc[DEFEITO_TEXTO]\" rows=\"4\" cols=\"50\">" . $cc[DEFEITO_TEXTO] . "</textarea>

	        <br clear=\"all\" />";

            echo "<label class=\"campo_label\" for=\"cc[CAUSA]\">{$tdb[ORDEM]['CAUSA']}:</label>";
            FormSelectD('DESCRICAO', '', CAUSA, $cc['CAUSA'], "cc[CAUSA]", "cc[CAUSA]", 'MID', "CAUSA", '', '', $whereEmp);
            echo "<br clear=\"all\" />

	        <label class=\"campo_label\" for=\"cc[CAUSA_TEXTO]\">{$tdb[ORDEM]['CAUSA_TEXTO']}:</label>
	        <textarea class=\"campo_text\" name=\"cc[CAUSA_TEXTO]\" id=\"cc[CAUSA_TEXTO]\" rows=\"4\" cols=\"50\">" . $cc[CAUSA_TEXTO] . "</textarea>

	        <br clear=\"all\" />";

            echo "<label class=\"campo_label\" for=\"cc[SOLUCAO]\">{$tdb[ORDEM]['SOLUCAO']}:</label>";
            FormSelectD('DESCRICAO', '', SOLUCAO, $cc['SOLUCAO'], "cc[SOLUCAO]", "cc[SOLUCAO]", 'MID', "SOLUCAO", '', '', $whereEmp);
            echo "<br clear=\"all\" />

	        <label class=\"campo_label\" for=\"cc[SOLUCAO_TEXTO]\">{$tdb[ORDEM]['SOLUCAO_TEXTO']}:</label>
	        <textarea class=\"campo_text\" name=\"cc[SOLUCAO_TEXTO]\" id=\"cc[SOLUCAO_TEXTO]\" rows=\"4\" cols=\"50\">" . $cc[SOLUCAO_TEXTO] . "</textarea>

	        </fieldset>";
        }
    }

    echo"
    <br />
    <div align=\"center\">
    <input type=\"submit\" class=\"botao\" name=\"gravaos\" value=\"{$ling['ord_salvar_ord_serv']}\" />
    </div><br /> <br />";
}

if ($id == 2) {
    if ($cc['TIPO'] == 1) {
        $mid_plano = VoltaValor(PROGRAMACAO, "MID_PLANO", "MID", $cc['MID_PROGRAMACAO'], 0);
        $plano = htmlentities(VoltaValor(PLANO_PADRAO, "DESCRICAO", "MID", $mid_plano, 0));

        echo "<fieldset><legend>{$tdb[PLANO_PADRAO]['DESC']}: $plano</legend>
        <div id=\"lista_fm2\"></div>
        <div id=\"pls\"></div>

        <table class=\"tabela\" style=\"font-size:10px\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
        <th width=\"10\">{$tdb[ORDEM_PREV]['NUMERO']}</th>
        <th>" . $tdb[ORDEM_PREV]['TAREFA'] . "</th>
        <th>" . $tdb[ORDEM_PREV]['PARTE'] . "</th>";
        /* <th>{$tdb[ORDEM_PREV]['ESPECIALIDADE']}</th>
          <th>".$ling['qtd_qtd']."</th> */
        echo "<th> </th>
        </tr>";
        // echo "SELECT * FROM ".ORDEM_PLANEJADO_PREV." WHERE MID_ORDEM = '$osmid' AND MID_ATV != 0 AND MID_PLANO_PADRAO  = $mid_plano";
        $obj = $dba[$tdb[ORDEM_PLANEJADO_PREV]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_PREV . " WHERE MID_ORDEM = '$osmid' AND MID_ATV != 0 AND MID_PLANO_PADRAO  = $mid_plano");
        while (!$obj->EOF) {
            $obj_campo = $obj->fields;
            if ($obj_campo['DIAGNOSTICO'] == 1)
                $check1 = "checked=\"checked\" ";
            elseif ($obj_campo['DIAGNOSTICO'] == 2)
                $check2 = "checked=\"checked\" ";
            else {
                $check1 = "";
                $check2 = "";
            }
            $n_atv = VoltaValor(ATIVIDADES, "NUMERO", "MID", $obj_campo['MID_ATV'], 0);
            if ($obj_campo['MID_CONJUNTO'] != 0)
                $parte = VoltaValor(MAQUINAS_CONJUNTO, "DESCRICAO", "MID", $obj_campo['MID_CONJUNTO'], 0);
            else
                $parte = $obj_campo['PARTE'];

            echo "<tr class=\"cor2\">
            <td>$n_atv</td>
            <td>" . htmlentities($obj_campo['TAREFA']) . "<br />
            <td>" . htmlentities($obj_campo['PARTE']) . "<br />
            <span id=\"pen" . $obj_campo['MID'] . "\" style=\"visibility:hidden;position:absolute;border:1px solid gray; background:white; padding:3px\">
            " . $ling['preencha_aq_sua_obser_pendenc_cliq_grav'] . "<br /><br>
            <input title=\"\" class=\"campo_text\" type=\"text\" name=\"des" . $obj_campo['MID'] . "\" id=\"des" . $obj_campo['MID'] . "\" size=\"50\" />
            <input type=\"button\" value=\"{$ling['gravar']}\" class=\"botao\" onclick=\"atualiza_area2('pen" . $obj_campo['MID'] . "','apontaosplan.php?osmid=$osmid&id=41&des=Parte:" . $obj_campo['PARTE'] . " Tarefa:" . $obj_campo['TAREFA'] . " Obs: ' + document.getElementById('des" . $obj_campo['MID'] . "').value + '&maq=" . $cc['MID_MAQUINA'] . "&mid={$obj_campo['MID']}&alert=1')\" />
            </span>
            </td>";
            /* <td>".htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $obj_campo['ESPECIALIDADE']))."</td>
              <td>".$obj_campo['QUANTIDADE_MO']."</td> */
            echo "<td width=100>
            <input type=\"radio\" $check1 value=\"1\" name=\"diag" . $obj_campo['MID'] . "\" id=\"diag" . $obj_campo['MID'] . "-1\" onclick=\"atualiza_area2('lista_fm2','apontaosplan.php?osmid=$osmid&atv=" . $obj_campo['MID'] . "&plano=$mid_plano&conj=" . $obj_campo['MID_CONJUNTO'] . "&maq=" . $obj_campo['MID_MAQUINA'] . "&id=21&it=1&cmid=" . $ca2['MID'] . "')\" /> <label for=\"diag" . $obj_campo['MID'] . "-1\">{$ling['ok']}</label>
             <br /> <input type=\"radio\" $check2 value=\"2\" name=\"diag" . $obj_campo['MID'] . "\" id=\"diag" . $obj_campo['MID'] . "-2\" onclick=\"atualiza_area2('lista_fm2','apontaosplan.php?osmid=$osmid&atv=" . $obj_campo['MID'] . "&plano=$mid_plano&conj=" . $obj_campo['MID_CONJUNTO'] . "&maq=" . $obj_campo['MID_MAQUINA'] . "&id=21&it=2&cmid=" . $ca2['MID'] . "')\" /> <label for=\"diag" . $obj_campo['MID'] . "-2\">{$ling['nao_ok']}</label>
             <br />
            <img src=\"imagens/icones/22x22/pendencias.png\" border=0 title=\"{$ling['pendencia']}\" onclick=\"abre_div('pen" . $obj_campo['MID'] . "','des" . $obj_campo['MID'] . "')\" />
            </td></tr>";

            $obj->MoveNext();
            $check1 = "";
            $check2 = "";
        }
        echo "</table> </div>";
        echo "</fieldset>";
    }
    if ($cc['TIPO'] == 2) {
        echo "  <fieldset><legend>" . $ling['option_rotas'] . "</legend>

        <div id=\"lista_fm2\"></div>
        <div id=\"lista_fm\">
        <div id=\"pls\"></div>
        <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">";

        $tmp = $dba[$tdb[ORDEM_PLANEJADO_LUB]['dba']]->Execute("SELECT a.*,c.POSICAO FROM " . ORDEM_PLANEJADO_LUB . " as a, " . ROTEIRO_ROTAS . " as c WHERE a.MID_ORDEM = '$osmid' and a.MID_MAQUINA = c.MID_MAQUINA and a.MID_PLANO = c.MID_PLANO ORDER BY c.POSICAO,a.NUMERO,a.MID_MAQUINA ASC");
        while (!$tmp->EOF) {
            $campo = $tmp->fields;
            if ($campo['DIAGNOSTICO'] == 1)
                $check1 = "checked=\"checked\" ";
            elseif ($campo['DIAGNOSTICO'] == 2)
                $check2 = "checked=\"checked\" ";
            else {
                $check1 = "";
                $check2 = "";
            }
            if ($campo['MID_MAQUINA'] != 0) {
                if ($maqre != $campo['MID_MAQUINA']) {
                    echo "<tr>
                    <td colspan=\"7\" class=\"cor1\" style=\"border-bottom:1px solid #666;\">
                    <strong>" . htmlentities(VoltaValor(MAQUINAS, "DESCRICAO", "MID", $campo['MID_MAQUINA'], $tdb[MAQUINAS]['dba'])) . "</strong>
                    </td></tr>
                    <tr class=\"cor1\">
                    <th>" . $tdb[PONTOS_LUBRIFICACAO]['PONTO'] . "</th>
                    <th>" . $tdb[ORDEM_LUB]['MID_MATERIAL'] . "</th>
                    <th>" . $tdb[ORDEM_LUB]['QUANTIDADE'] . "</th>
                    <th>" . $tdb[ORDEM_LUB]['TAREFA'] . "</th>";
                    /* <th>{$tdb[ORDEM_LUB]['ESPECIALIDADE']}</th>
                      <th>".$ling['qtd_qtd']."</th> */
                    echo "<th> </th>
                    </tr>";
                }

                if ($campo['MID_PONTO'] != 0) {
                    $ponto = VoltaValor(PONTOS_LUBRIFICACAO, "PONTO", "MID", $campo['MID_PONTO'], $tdb[PONTOS_LUBRIFICACAO]['dba']);
                } else {
                    $ponto = VoltaValor(MAQUINAS_CONJUNTO, "DESCRICAO", "MID", $campo['MID_CONJUNTO'], $tdb[MAQUINAS_CONJUNTO]['dba']);
                }

                echo "<tr class=\"cor2\">
                <td>" . $ponto . "</td>
                <td>" . VoltaValor(MATERIAIS, "DESCRICAO", "MID", $campo['MID_MATERIAL'], 0) . "</td>
                <td>";
                if ((int) $campo['MID_MATERIAL'] != 0)
                    echo "<input type=\"text\" id=\"qtd" . $campo['MID'] . "\" class=\"campo_text\" value=\"" . $campo['QUANTIDADE'] . "\" name=\"qtd" . $campo['MID'] . "\" size=\"4\" maxlength=\"8\" onblur=\"atualiza_area2('lista_fm2','apontaosplan.php?osmid=$osmid&f=" . $campo['MID'] . "&id=9&it=' + document.getElementById('qtd" . $campo['MID'] . "').value)\" />";
                echo "</td>
                <td>" . htmlentities($campo['TAREFA']) . "</td>";
                /* <td>".htmlentities(VoltaValor(ESPECIALIDADES, 'DESCRICAO', 'MID', $campo['ESPECIALIDADE']))."</td>
                  <td>".$campo['QUANTIDADE_MO']."</td> */
                echo "<td width=100>
                <input type=\"radio\" $check1 value=\"1\" name=\"diag" . $campo['MID'] . "\" id=\"diag" . $campo['MID'] . "-1\" onclick=\"atualiza_area2('lista_fm2','apontaosplan.php?osmid=$osmid&atv=" . $campo['MID'] . "&plano=$mid_plano&conj=" . $campo['MID_CONJUNTO'] . "&maq=" . $campo['MID_MAQUINA'] . "&id=22&it=1&cmid=" . $ca2['MID'] . "')\" /> <label for=\"diag" . $campo['MID'] . "-1\">OK</label>
                 <br /> <input type=\"radio\"  $check2 value=\"2\" name=\"diag" . $campo['MID'] . "\" id=\"diag" . $campo['MID'] . "-2\" onclick=\"atualiza_area2('lista_fm2','apontaosplan.php?osmid=$osmid&atv=" . $campo['MID'] . "&plano=$mid_plano&conj=" . $campo['MID_CONJUNTO'] . "&maq=" . $campo['MID_MAQUINA'] . "&id=22&it=2&cmid=" . $ca2['MID'] . "')\" /> <label for=\"diag" . $campo['MID'] . "-2\">N&Atilde;O OK</label>
                 <br />
                <img src=\"imagens/icones/22x22/pendencias.png\" border=0 title=\"{$ling['pendencia']}\" onclick=\"abre_div('pen" . $campo['MID'] . "','des" . $campo['MID'] . "')\" />

                <span id=\"pen" . $campo['MID'] . "\"
                style=\"left:0px;visibility:hidden;position:absolute;border:1px solid gray; background:white; padding:3px\">
                {$ling['ord_obs_sobre_pendencia']}<br /><br>
                <input title=\"\" class=\"campo_text\" type=\"text\" name=\"des" . $campo['MID'] . "\" id=\"des" . $campo['MID'] . "\" size=\"50\" />
                <input type=\"button\" value=\"{$ling['gravar']}\" class=\"botao\" onclick=\"atualiza_area2('pen" . $campo['MID'] . "','apontaosplan.php?osmid=$osmid&id=41&des=Parte:" . $campo['PARTE'] . " Tarefa:" . $campo['TAREFA'] . " Obs: ' + document.getElementById('des" . $campo['MID'] . "').value + '&maq=" . $campo['MID_MAQUINA'] . "&mid={$campo['MID']}&alert=1')\" />
                </span>
                </td>
                </tr>";
            }
            $maqre = $campo['MID_MAQUINA'];
            $tmp->MoveNext();
            $check1 = "";
            $check2 = "";
        }
        echo "</table> </div>";
        echo "</fieldset>";
    }
}
if ($id == 3) {
    $opj = "atualiza_area2('total_Ps','apontaosplan.php?osmid=$osmid&id=31&di=' + document.getElementById('DATA_PARADA_INICIO').value + '&hi=' + document.getElementById('HORA_PARADA_INICIO').value + '&df=' + document.getElementById('DATA_PARADA_FIM').value + '&hf=' + document.getElementById('HORA_PARADA_FIM').value)";
    echo "
    <fieldset><legend>" . $tdb[ORDEM_MAQ_PARADA]['DESC'] . "</legend>";

    FormData($tdb[ORDEM_MAQ_PARADA]['DATA_INICIO'], "DATA_PARADA_INICIO", $fdatai, "campo_label");

    echo "<label for=\"HORA_PARADA_INICIO\">{$tdb[ORDEM_MAQ_PARADA]['HORA_INICIO']}</label>
    <input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"HORA_PARADA_INICIO\" class=\"campo_text\" name=\"cc[HORA_PARADA_INICIO]\" size=\"8\" maxlength=\"8\" onblur=\"$opj\" />
    <br clear=\"all\"/>";

    FormData($tdb[ORDEM_MAQ_PARADA]['DATA_FINAL'], "DATA_PARADA_FIM", $fdataf, "campo_label", " onfocus=\"this.value=document.getElementById('DATA_PARADA_INICIO').value\" ");

    echo "<label for=\"HORA_PARADA_FIM\">{$tdb[ORDEM_MAQ_PARADA]['HORA_FINAL']}&nbsp;</label>
    <input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"HORA_PARADA_FIM\" class=\"campo_text\" name=\"cc[HORA_PARADA_FIM]\" size=\"8\" maxlength=\"8\" onblur=\"$opj\" />

    <input type=\"button\" class=\"botao\" value=\"{$ling['calcular'] }\"  onclick=\"atualiza_area2('total_Ps','apontaosplan.php?osmid=$osmid&id=31&di=' + document.getElementById('DATA_PARADA_INICIO').value + '&hi=' + document.getElementById('HORA_PARADA_INICIO').value + '&df=' + document.getElementById('DATA_PARADA_FIM').value + '&hf=' + document.getElementById('HORA_PARADA_FIM').value)\" />

    <br clear=\"all\"/>
    <div id=\"total_Ps\">
    </div>"; //
    echo "<br clear=\"all\" />
    <div id=\"lista_Ps\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>{$tdb[ORDEM_MAQ_PARADA]['DATA_INICIO']}</th>
    <th>{$tdb[ORDEM_MAQ_PARADA]['DATA_FINAL']}</th>
    <th>{$tdb[ORDEM_MAQ_PARADA]['TEMPO']}</th>
    <th></th>
    </tr>";
    $tmp = $dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_MAQ_PARADA . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        echo "<tr class=\"cor2\">
        <td>" . $tmp->UserDate($campo['DATA_INICIO'], 'd/m/Y') . " " . $campo['HORA_INICIO'] . "</td>
        <td>" . $tmp->UserDate($campo['DATA_FINAL'], 'd/m/Y') . " " . $campo['HORA_FINAL'] . "</td>
        <td>" . $campo['TEMPO'] . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('lista_Ps','apontaosplan.php?osmid=$osmid&id=32&del=" . $campo['MID'] . "')\" />
        </td>
        </tr>";
        $tmp->MoveNext();
    }
    echo "</table>
    </div>

    </fieldset>";



    /// APONTAMENTO DE MÃO-DE-OBRA
    echo"<fieldset><legend>" . $ling['maodeobra'] . "</legend>\n";

    echo "<label class=\"campo_label\" for=\"func\">" . $tdb[ORDEM_MAODEOBRA]['MID_FUNCIONARIO'] . "</label>";
    FormSelectD('MATRICULA', 'NOME', FUNCIONARIOS, '', 'func', 'func', 'MID', '', '', "atualiza_area2('d_manutentor','apontaosplan.php?osmid=$osmid&id=99901&func='+this.options[this.selectedIndex].value)", "WHERE MID_EMPRESA=" . $cc['MID_EMPRESA'] . "", "A", "NOME");

    echo "<div id=\"d_manutentor\">
    <input type=\"hidden\"  id=\"manutentor\" value=\"\">
    </div>

    <br clear=\"all\"/>";


    FormData($tdb[ORDEM_MAODEOBRA]['DATA_INICIO'], "fdatai", $fdatai, "campo_label");

    echo "<label for=\"fhorai\">" . $tdb[ORDEM_MAODEOBRA]['HORA_INICIO'] . "</label>
    <input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"fhorai\" class=\"campo_text\" value=\"" . $_POST['fhorai'] . "\" name=\"fhorai\" size=\"8\" maxlength=\"8\" />
    <br clear=\"all\"/>";

    FormData($tdb[ORDEM_MAODEOBRA]['DATA_FINAL'], "fdataf", $fdataf, "campo_label", " onfocus=\"this.value=document.getElementById('fdatai').value\" ");

    echo "<label for=\"fhoraf\">{$tdb[ORDEM_MADODEOBRA]['DATA_FINAL']}&nbsp;</label>
    <input onkeypress=\"return ajustar_hora(this, event)\" type=\"text\" id=\"fhoraf\" class=\"campo_text\" value=\"" . $_POST['fhoraf'] . "\" name=\"fhoraf\" size=\"8\" maxlength=\"8\" />

    <input type=\"button\" class=\"botao\" value=\"{$ling['calcular']}\" onclick=\"atualiza_area2('total_fs','apontaosplan.php?osmid=$osmid&id=33&di=' + document.getElementById('fdatai').value + '&hi=' + document.getElementById('fhorai').value + '&df=' + document.getElementById('fdataf').value + '&hf=' + document.getElementById('fhoraf').value + '&f=' + document.getElementById('func').options[document.getElementById('func').selectedIndex].value + '&manut='+ document.getElementById('manutentor').value)\" />

    <br clear=\"all\" />
    <div id=\"total_fs\"></div>

    <br clear=\"all\" />

    <div id=\"lista_fs\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>" . $tdb[ORDEM_MAODEOBRA]['MID_FUNCIONARIO'] . "</th>
    <th>" . $tdb[FUNCIONARIOS]['EQUIPE'] . "</th>
    <th>" . $tdb[ORDEM_MAODEOBRA]['DATA_INICIO'] . "</th>
    <th>" . $tdb[ORDEM_MAODEOBRA]['DATA_FINAL'] . "</th>";

    if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
        echo "<th>{$tdb[ORDEM_MAODEOBRA]['CUSTO_EXTRA']}</th>
        <th>{$tdb[ORDEM_MAODEOBRA]['CUSTO']}</th>";
    }

    echo "<th></th>
    </tr>";

    $tmp = $dba[$tdb[ORDEM_PLANEJADO_MADODEOBRA]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_MADODEOBRA . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;
        $ftotal = (float) $ftotal + $campo['CUSTO'];
        $campo['CUSTO'] = (float) $campo['CUSTO'];
        $campo['CUSTO_EXTRA'] = (float) $campo['CUSTO_EXTRA'];

        echo "<tr class=\"cor2\">
        <td>" . htmlentities(strtoupper(VoltaValor(FUNCIONARIOS, "NOME", "MID", $campo['MID_FUNCIONARIO'], $tdb[FUNCIONARIOS]['dba']))) . "</td>
        <td>" . htmlentities(strtoupper(VoltaValor(EQUIPES, "DESCRICAO", "MID", $campo['EQUIPE'], $tdb[EQUIPES]['dba']))) . "</td>
        <td>" . $tmp->UserDate($campo['DATA_INICIO'], 'd/m/Y') . " " . $campo['HORA_INICIO'] . "</td>
        <td>" . $tmp->UserDate($campo['DATA_FINAL'], 'd/m/Y') . " " . $campo['HORA_FINAL'] . "</td>";

        if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1)) {
            echo "<td>" . number_format($campo['CUSTO_EXTRA'], 2, ',', '.') . "</td>
            <td>" . number_format($campo['CUSTO'], 2, ',', '.') . "</td>";
        }

        echo "
        <td><img src=\"imagens/icones/22x22/del.png\" border=\"0\" onclick=\"atualiza_area2('lista_fs','apontaosplan.php?osmid=$osmid&id=34&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }





    if ((VoltaPermissao($idModulo, $op) == 1) or ($manusis['ordem']['custo'] == 1))
        echo "</table><br /><strong>{$ling['total']}: " . number_format($ftotal, 2, ',', '.') . "</strong></div>";
    else
        echo "</table><br /></div>";

    echo "</fieldset>";



    echo "<fieldset><legend>{$tdb[ORDEM_MATERIAL]['DESC']}</legend>";

    echo "<label class=\"campo_label\" for=\"alm\">{$tdb[ORDEM_MATERIAL]['MID_ALMOXARIFADO']}</label>";
    FormSelectD('COD', 'DESCRICAO', ALMOXARIFADO, '', "alm", "alm", 'MID', '', '', "atualiza_area2('fmat', 'apontaosplan.php?osmid=$osmid&id=351&alm=' + this.value)", $whereEmp, 'A', 'COD', $ling['select_opcao']);
    echo "<br clear=\"all\" />";

    echo "<div id=\"fmat\"></div>

    <div id=\"dmat\"></div>

    <br clear=\"all\" />

    <div id=\"lmat\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>" . $tdb[ORDEM_MATERIAL]['MID_ALMOXARIFADO'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['MID_MATERIAL'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['QUANTIDADE'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['CUSTO_UNITARIO'] . "</th>
    <th>" . $tdb[ORDEM_MATERIAL]['CUSTO_TOTAL'] . "</th>
    <th></th>
    </tr>";

    $tmp = $dba[$tdb[ORDEM_PLANEJADO_MATERIAL]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_MATERIAL . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        $mtotal = (float) $mtotal + $campo['CUSTO_TOTAL'];
        $campo['CUSTO_UNITARIO'] = (float) $campo['CUSTO_UNITARIO'];
        $campo['CUSTO_TOTAL'] = (float) $campo['CUSTO_TOTAL'];
        $unidade = VoltaValor(MATERIAIS_UNIDADE, "COD", "MID", VoltaValor(MATERIAIS, "UNIDADE", "MID", $campo['MID_MATERIAL'], 0), 0);

        echo "<tr class=\"cor2\">
        <td>" . htmlentities(VoltaValor(ALMOXARIFADO, "DESCRICAO", "MID", $campo['MID_ALMOXARIFADO'], $tdb[ALMOXARIFADO]['dba'])) . "</td>
        <td>" . VoltaValor(MATERIAIS, "DESCRICAO", "MID", $campo['MID_MATERIAL'], $tdb[MATERIAIS]['dba']) . "</td>
        <td>" . $campo['QUANTIDADE'] . " $unidade</td>
        <td>" . number_format($campo['CUSTO_UNITARIO'], 2, ",", ".") . "</td>
        <td>" . number_format($campo['CUSTO_TOTAL'], 2, ",", ".") . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('lmat','apontaosplan.php?osmid=$osmid&id=36&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }
    echo "</table><br /><strong>{$ling['total']}: " . number_format($mtotal, 2, ",", ".") . "</strong></div>";
    echo "</fieldset>";

    if ($isSpeedCheck == 0) {

        echo "<fieldset><legend>" . $tdb[ORDEM_CUSTOS]['DESC'] . "</legend>\n";

        echo "<label class=\"campo_label\" for=\"descc\"> " . $tdb[ORDEM_CUSTOS]['DESCRICAO'] . "</label>
    <input type=\"text\" id=\"descc\" value=\"$descc\"class=\"campo_text\" name=\"descc\" size=\"30\" maxlenght=\"60\" />

    <label for=\"oc\"> " . $tdb[ORDEM_CUSTOS]['CUSTO'] . "</label>
    <input type=\"text\" id=\"oc\" class=\"campo_text\" name=\"oc\" value=\"$oc\" size=\"10\" maxlenght=\"20\"/>

    <input class=\"botao\" type=\"button\" name=\"fadd\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('loc','apontaosplan.php?osmid=$osmid&id=37&des=' + document.getElementById('descc').value + '&oc=' + document.getElementById('oc').value)\" />
    <br clear=\"all\" />

    <div id=\"loc\"><br clear=\"all\" />
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>{$tdb[ORDEM_CUSTOS]['DESCRICAO']}</th>
    <th>{$tdb[ORDEM_CUSTOS]['CUSTO']}</th>
    <th></th>
    </tr>";

        $tmp = $dba[$tdb[ORDEM_PLANEJADO_CUSTOS]['dba']]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_CUSTOS . " WHERE MID_ORDEM = '$osmid'");
        while (!$tmp->EOF) {
            $campo = $tmp->fields;
            $ototal = (float) $ototal + $campo['CUSTO'];
            $campo['CUSTO'] = (float) $campo['CUSTO'];

            echo "<tr class=\"cor2\">
        <td>" . $campo['DESCRICAO'] . "</td>
        <td>" . number_format($campo['CUSTO'], 2, ",", ".") . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('loc','apontaosplan.php?osmid=$osmid&id=37&del=" . $campo['MID'] . "')\" />
        </td></tr>";

            $tmp->MoveNext();
        }

        echo "</table><br /><strong>{$ling['total']}: " . number_format($ototal, 2, ",", ".") . "</strong></div>";
        echo "</fieldset>";
    }
}
if ($id == 4) {
    echo "<fieldset><legend>" . $tdb[PENDENCIAS]['DESC'] . "</legend>

    <label class=\"campo_label\" for=\"p_maq\">" . $tdb[PENDENCIAS]['MID_MAQUINA'] . "</label>";

    if ($cc['TIPO'] == 2) {
        $emaqs = array();
        $tmp = $dba[$tdb[ORDEM_LUB]['dba']]->Execute("SELECT DISTINCT MID_MAQUINA FROM " . ORDEM_LUB . " WHERE MID_ORDEM = '$osmid'");
        while (!$tmp->EOF) {
            $emaqs[] = $tmp->fields('MID_MAQUINA');
            $tmp->MoveNext();
        }
        $emaqs = join(', ', $emaqs);
        FormSelectD('COD', 'DESCRICAO', MAQUINAS, $cc['MID_MAQUINA'], "p_maq", "p_maq", 'MID', '', '', '', "WHERE MID IN ($emaqs)");
    } else {
        echo "<input type=\"hidden\" name=\"p_maq\" value=\"{$cc['MID_MAQUINA']}\">
        <input type=\"text\" readonly=\"readonly\" class=\"campo_text_ob\" size=40 value=\"" . VoltaValor(MAQUINAS, 'DESCRICAO', 'MID', $cc['MID_MAQUINA'], 0) . "\">";
    }

    echo "<br clear=\"all\" />

    <label class=\"campo_label\" for=\"des\">" . $tdb[PENDENCIAS]['DESCRICAO'] . "</label>
    <input type=\"text\" id=\"des\" value=\"$des\" class=\"campo_text\" name=\"des\" size=\"65\" maxlenght=\"255\" />";

    if ($cc['TIPO'] == 2) {
        echo "<input class=\"botao\" type=\"button\" name=\"padd\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('pls','apontaosplan.php?osmid=$osmid&id=41&des=' + document.getElementById('des').value + '&maq=' + document.getElementById('p_maq').options[document.getElementById('p_maq').selectedIndex].value)\" />";
    } else {
        echo "<input class=\"botao\" type=\"button\" name=\"padd\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('pls','apontaosplan.php?osmid=$osmid&id=41&des=' + document.getElementById('des').value + '&maq={$cc['MID_MAQUINA']}')\" />";
    }
    echo "<br clear=\"all\" /><br />
    <div id=\"pls\">
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\">
    <tr class=\"cor1\">
    <th>" . $tdb[PENDENCIAS]['NUMERO'] . "</th>
    <th>" . $tdb[PENDENCIAS]['DATA'] . "</th>
    <th>" . $tdb[PENDENCIAS]['MID_MAQUINA'] . "</th>
    <th>" . $tdb[PENDENCIAS]['DESCRICAO'] . "</th>
    <th></th>
    </tr>";
    $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("SELECT * FROM " . PENDENCIAS . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        echo "<tr class=\"cor2\">
        <td>" . $campo['NUMERO'] . "</td>
        <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . " </td>
        <td>" . VoltaValor(MAQUINAS, "DESCRICAO", "MID", $campo['MID_MAQUINA'], 0) . "</td>
        <td>" . htmlentities($campo['DESCRICAO']) . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('pls','apontaosplan.php?osmid=$osmid&id=41&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }
    echo "</table>";

    echo "</fieldset>";

    if (($cc['TIPO'] == 1) or ($cc['TIPO'] == 0)) {
        echo "<fieldset><legend>{$ling['ord_exec_anexar_pendencias']}</legend>\n";
        echo "<p align=\"center\"><img src=\"imagens/icones/16x16/ir.png\" alt=\"\" border=\"0\"><a class=\"link\" href=\"javascript:janela('procura_pen.php?osmid=$osmid&id=" . $cc['MID_MAQUINA'] . "')\"> Procurar Pêndencias</a><p><br clear=\"all\" />";
        echo "<div id=\"apen\">
        <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
        <th>{$tdb[PENDENCIAS]['MID_ORDEM']}</th>
        <th>{$tdb[PENDENCIAS]['DESCRICAO']}</th>
        <th>{$tdb[PENDENCIAS]['DATA']}</th>
        <th></th>
        </tr>";

        $tmp = $dba[$tdb[PENDENCIAS]['dba']]->Execute("SELECT * FROM " . PENDENCIAS . " WHERE MID_ORDEM_EXC = '$osmid'");
        while (!$tmp->EOF) {
            $campo = $tmp->fields;

            echo "<tr class=\"cor2\">
            <td>" . $campo['NUMERO'] . "</td>
            <td>" . $campo['DESCRICAO'] . "</td>
            <td>" . $tmp->UserDate($campo['DATA'], 'd/m/Y') . "</td>
            <td><img src=\"imagens/icones/22x22/pendencias.png\" title=\"{$ling['ord_continua_pendente_n_exec']}\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('apen','apontaosplan.php?osmid=$osmid&id=42&del=" . $campo['MID'] . "')\" />
            </td></tr>";

            $tmp->MoveNext();
        }
        echo "</table><br /></div>";

        echo "</fieldset>";
    }
}
if ($id == 5) {
    echo "<fieldset><legend>" . $tdb[ORDEM_REPROGRAMA]['DESC'] . "</legend>\n";

    echo "<p align=\"center\">{$ling['ord_prog_inicial']}: <strong>{$cc['DATA_PROG']}</strong>.</p>
    <br clear=\"all\" />

    <label class=\"campo_label\" for=\"mot\">{$tdb[ORDEM_REPROGRAMA]['MOTIVO']}:</label>
    <input type=\"text\" id=\"mot\"  class=\"campo_text\" name=\"mot\" size=\"30\" maxlenght=\"120\" />

    <br clear=\"all\" />";

    FormData("{$tdb[ORDEM_REPROGRAMA]['DATA']}:", "rdata", $fdataf, 'campo_label');

    echo "<input class=\"botao\" type=\"button\" name=\"fadd\" value=\"{$ling['gravar']}\" onclick=\"atualiza_area2('repro','apontaosplan.php?osmid=$osmid&id=51&mot=' + document.getElementById('mot').value + '&data=' + document.getElementById('rdata').value)\" />

    <br clear=\"all\" />
    <div id=\"repro\">
    <br clear=\"all\" />
    <table class=\"tabela\" width=\"100%\" cellpadding=\"2\" cellspacing=\"1\"><tr class=\"cor1\">
    <th>{$tdb[ORDEM_REPROGRAMA]['MOTIVO']}</th>
    <th>{$tdb[ORDEM_REPROGRAMA]['DATA']}</th>
    <th>{$tdb[ORDEM_REPROGRAMA]['DATA_ORIGINAL']}</th>
    <th></th>
    </tr>";

    $tmp = $dba[0]->Execute("SELECT * FROM " . ORDEM_PLANEJADO_REPROGRAMA . " WHERE MID_ORDEM = '$osmid'");
    while (!$tmp->EOF) {
        $campo = $tmp->fields;

        echo "<tr class=\"cor2\">
        <td>" . $campo['MOTIVO'] . "</td>
        <td>" . NossaData($campo['DATA']) . "</td>
        <td>" . NossaData($campo['DATA_ORIGINAL']) . "</td>
        <td><img src=\"imagens/icones/22x22/del.png\" style=\"cursor:hand\" border=\"0\" onclick=\"atualiza_area2('repro','apontaosplan.php?osmid=$osmid&id=51&del=" . $campo['MID'] . "')\" />
        </td></tr>";

        $tmp->MoveNext();
    }
    echo "</table><br /></div>";

    echo "</fieldset>";
}
if ($id == 6) {
    $fecha = (int) $_GET['fecha'];
    if ($fecha != 2) {
        echo "<fieldset><legend>" . $ling['resumo_ord_servico'] . "</legend>\n";

        echo "<label class=\"campo_label\"><strong>" . $tdb[ORDEM]['DATA_INICIO'] . ":</strong></label> " . ( ($cc['DATA_INICIO']) ? NossaData($cc['DATA_INICIO']) : '') . " <br clear=\"all\" />
        <label class=\"campo_label\"><strong>" . $tdb[ORDEM]['DATA_FINAL'] . ":</strong></label> " . ( ($cc['DATA_FINAL']) ? NossaData($cc['DATA_FINAL']) : '' ) . " <br clear=\"all\" />
        <label class=\"campo_label\"><strong>" . $tdb[ORDEM]['TEMPO_TOTAL'] . ":</strong></label> " . $cc['TEMPO_TOTAL'] . " <br clear=\"all\" />";

        // Tempo de parada
        $tmp = $dba[$tdb[ORDEM_PLANEJADO_MAQ_PARADA]['dba']]->Execute("SELECT SUM(TEMPO) AS TOTAL FROM " . ORDEM_MAQ_PARADA . " WHERE MID_ORDEM = '$osmid'");
        if ((!$tmp->EOF) and ($tmp->fields['TOTAL'] > 0)) {
            echo "<label class=\"campo_label\"><strong>" . $tdb[ORDEM_MAQ_PARADA]['TEMPO'] . ":</strong></label> {$tmp->fields['TOTAL']}<br clear=\"all\" />";
        }

        echo "</fieldset>\n";

        // Validando OS
        $erro = "";
        $naomostra = 0;

        // Apontamento de MO
        $re = $dba[$tdb[ORDEM_MAODEOBRA]['dba']]->Execute("SELECT COUNT(*) AS TOTAL FROM " . ORDEM_MAODEOBRA . " WHERE MID_ORDEM = '$osmid'");
        if ($re->fields['TOTAL'] == 0) {
            $nmostra = 1;
            $erro .= "A <B>" . $tdb[ORDEM_MAODEOBRA]['DESC'] . "</B>" . $ling['nao_foi_preenchida'] . "<br />";
        }

        // Alocado
        $re = $dba[$tdb[ORDEM_MO_ALOC]['dba']]->Execute("SELECT SUM(TEMPO) TOTAL_ALOC FROM " . ORDEM_MO_ALOC . " WHERE MID_ORDEM = '$osmid'");
        if ((($cc['TIPO'] == 1) or ($cc['TIPO'] == 2)) and ($re->fields['TOTAL_ALOC'] == 0)) {
            $nmostra = 1;
            $erro .= "A <B>" . $tdb[ORDEM_MO_ALOC]['DESC'] . "</B> " . $ling['nao_foi_preenchida'] . "<br />";
        }

        // Previsto
        $re2 = $dba[$tdb[ORDEM_MO_PREVISTO]['dba']]->Execute("SELECT SUM(TEMPO) TOTAL_PREV FROM " . ORDEM_MO_PREVISTO . " WHERE MID_ORDEM = '$osmid'");
        if (($re2->fields['TOTAL_PREV'] > 0) and ($re->fields['TOTAL_ALOC'] < $re2->fields['TOTAL_PREV'])) {
            $nmostra = 1;
            $erro .= "A <B>" . $tdb[ORDEM_MO_ALOC]['DESC'] . "</B> " . $ling['nao_pode_inferior_previsto'] . "<br />";
        }

        // Sem erros mostrar botão para fechar
        if ($nmostra != 1) {
            echo "<center><a href=\"apontaosplan.php?osmid=$osmid&id=6&fecha=2\" style=\"font-size:14px;color:red;\"><strong>{$ling['fechar_ordem']}</strong></a></center>
            <br /> <br /> <br />";
        } else {
            echo "<fieldset><legend>" . $ling['vali_ord_servi'] . "</legend>\n";

            echo $erro;

            echo "</fieldset>\n";
        }
    } else {
        $sql = "UPDATE " . ORDEM_PLANEJADO . " SET STATUS = '2' WHERE MID = '$osmid'";
        $re = $dba[$tdb[ORDEM_PLANEJADO]['dba']]->Execute($sql);

        // Logando o fechamento
        logar(4, $ling['ord_fechamento_os'], ORDEM, 'MID', $osmid);



        echo "<script>
				window.parent.location.reload();
                fecha_janela_form_frame();
              </script>";
    }
}

// Aba speed check
if ($id == 9) {
    echo "<fieldset><legend>{$tdb[SPEED_CHECK_ATIVIDADES]['DESC']}</legend>\n";

    // Span utilizado apenas para ter um par�metro valido para o atualiza �rea
    echo "<span id='ajaxScape'></span>";

    echo "<table class='tabela' cellspacing='1' cellpadding='2'>\n";
    echo "<tbody>\n";

    $sql = "SELECT * FROM " . ORDEM_SPEED_CHECK . " WHERE MID_ORDEM = {$osmid} ORDER BY ETAPA, NUMERO ASC";

    if (!$rs = $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sql)) {
        erromsg("Erro ao localizar registros de " . ORDEM_SPEED_CHECK . " em:<br />
				Arquivo: " . __FILE__ . "<br />
				Linha: " . __LINE__ . "<br />
				Erro: " . $dba[$tdb[SPEED_CHECK_ATIVIDADES]['dba']]->ErrorMsg() . "<br />
				SQL: " . $sql . "
				");
    } elseif (!$rs->EOF) {
        $etapa = 0;
        while (!$rs->EOF) {
            $row = $rs->fields;

            if ($etapa != $row['ETAPA']) {
                echo "<tr class='cor1'>\n";
                echo "<th colspan='4'>" . VoltaValor(ETAPAS_PLANO, 'DESCRICAO', 'MID', $row['ETAPA'], 0) . "</th>\n";
                echo "</tr>\n";
                echo "<tr class='cor2'>\n";
                echo "<th width='5%'>{$tdb[ORDEM_SPEED_CHECK]['NUMERO']}</th>\n";
                echo "<th width='90%' colspan='2'>{$tdb[ORDEM_SPEED_CHECK]['DESCRICAO']}</th>\n";
                echo "<th width='5%'>{$tdb[ORDEM_SPEED_CHECK]['DIAGNOSTICO']}</th>\n";
                echo "</tr>\n";
            }
            echo "</tr>\n";
            echo "<tr class='cor2'>\n";
            echo "<td>{$row['NUMERO']}";
            echo "<span id=\"comentario" . $row['MID'] . "\"
			style=\"visibility:hidden;position:absolute;border:1px solid gray; background:white; padding:3px\">
			<span id='div_formulario_cab'>
			<span style='float:left;'>Preencha aqui seu coment�rio e clique em gravar</span>
			<img src='imagens/icones/fecha.gif' style='cursor:pointer' title='Fechar' onclick=\"abre_div('comentario" . $row['MID'] . "','comment" . $row['MID'] . "')\" />
			</span><br />
			<input title=\"\" class=\"campo_text\" type=\"text\" id=\"comment" . $row['MID'] . "\" size=\"50\" value='{$row['COMENTARIO']}' />
			<input type=\"button\" value=\"Gravar\" class=\"botao\" onclick=\"atualiza_area2('ajaxScapeObs_{$row['MID']}', 'apontaosplan.php?osmid=$osmid&mid=" . $row['MID'] . "&id=92&comment='+escape(document.getElementById('comment" . $row['MID'] . "').value)+'')\" />
			<span id='ajaxScapeObs_{$row['MID']}'></span>
			</span>";
            echo "</td>\n";
            echo "<td colspan='2'>{$row['DESCRICAO']}</td>\n";
            echo "<td>";
            echo "<input " . ($row['DIAGNOSTICO'] == 1 ? "checked='checked'" : "") . " type='radio' name='diag_{$row['MID']}' id='diag_{$row['MID']}_1' onclick=\"atualiza_area2('ajaxScape','apontaosplan.php?osmid=$osmid&mid=" . $row['MID'] . "&id=91&it=1')\" /><label for='diag_{$row['MID']}_1'>OK</label>\n<br />";
            echo "<input " . ($row['DIAGNOSTICO'] == 2 ? "checked='checked'" : "") . " type='radio' name='diag_{$row['MID']}' id='diag_{$row['MID']}_2' onclick=\"atualiza_area2('ajaxScape','apontaosplan.php?osmid=$osmid&mid=" . $row['MID'] . "&id=91&it=2')\" /><label for='diag_{$row['MID']}_2'>NOK &nbsp;&nbsp;</label><br />\n";
            echo "<input " . ($row['DIAGNOSTICO'] == 0 ? "checked='checked'" : "") . " type='radio' name='diag_{$row['MID']}' id='diag_{$row['MID']}_0' onclick=\"atualiza_area2('ajaxScape','apontaosplan.php?osmid=$osmid&mid=" . $row['MID'] . "&id=91&it=0')\" /><label for='diag_{$row['MID']}_0'>NA &nbsp;&nbsp;</label><br />\n";
            echo "<img src='imagens/icones/22x22/balao.png' style='cursor:pointer' title='{$tdb[ORDEM_SPEED_CHECK]['COMENTARIO']}' alt='{$tdb[ORDEM_SPEED_CHECK]['COMENTARIO']}' onclick=\"abre_div('comentario" . $row['MID'] . "','comment" . $row['MID'] . "')\" />\n";
            echo "</td>\n";
            echo "</tr>\n";

            $etapa = $row['ETAPA'];

            $rs->MoveNext();
        }
    } else {
        echo "<tr class='cor1'>\n";
        echo "<th colspan='4'>" . $ling['atividades_nao_encontradas'] . "</th>\n";
        echo "</tr>\n";
        echo "<tr class='cor2'>\n";
        echo "<th width='5%'>{$tdb[ORDEM_SPEED_CHECK]['NUMERO']}</th>\n";
        echo "<th width='75%' colspan='2'>{$tdb[ORDEM_SPEED_CHECK]['DESCRICAO']}</th>\n";
        echo "<th width='20%'> {$tdb[ORDEM_SPEED_CHECK]['DIAGNOSTICO']}</th>\n";
        echo "</tr>";
    }
    echo "</tbody>\n";
    echo "</table>\n";
    echo "</fieldset>\n";
}
if ($id == 91) {

    $mid = $_GET['mid'];
    $it = $_GET['it'];

    $sql = "UPDATE " . ORDEM_SPEED_CHECK . " SET DIAGNOSTICO = $it WHERE MID = $mid";
    if (!$dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sql)) {
        erromsg("Erro ao atualizar " . ORDEM_SPEED_CHECK . " em:<br />
				Arquivo: " . __FILE__ . "<br />
				Linha: " . __LINE__ . "<br />
				Erro: " . $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg() . "<br />
				SQL: " . $sql . "
				");
    }

    exit();
}

if ($id == 92) {

    $mid = $_GET['mid'];
    $comment = LimpaTexto(urldecode($_GET['comment']));
    $comment = mb_strtoupper($comment);
    $sql = "UPDATE " . ORDEM_SPEED_CHECK . " SET COMENTARIO = '$comment' WHERE MID = $mid";
    if (!$dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->Execute($sql)) {
        erromsg("Erro ao atualizar " . ORDEM_SPEED_CHECK . " em:<br />
				Arquivo: " . __FILE__ . "<br />
				Linha: " . __LINE__ . "<br />
				Erro: " . $dba[$tdb[ORDEM_SPEED_CHECK]['dba']]->ErrorMsg() . "<br />
				SQL: " . $sql . "
				");
    }
    echo "<span><br />{$ling['comentario_adicionado']}</span>";
    exit();
}



echo "
</td>
</tr>
</table>
</form></div>
</body>
</html>";
?>
