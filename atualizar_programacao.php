<?
/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Nota: Relatorio
 */
// Funções do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Formulários
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Autentificação
elseif (!require("lib/autent.php")) die ($ling['autent01']);

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>Manusis</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"".$manusis['tema']."\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body class=\"body_form\">";

echo "<div id=\"lt_tabela\">
<h2>".$ling['ATUALIZANDO_PROG']."</h2>
<br />\n";

// Atualizando
if (($_POST['ano'] != '') or ($_GET['ano'] != '')) {
    $pro = ($_POST['aprog'])? $_POST['aprog'] : $_GET['aprog'];
    $ano = ($_POST['ano'])? DataSQL($_POST['ano']) : DataSQL($_GET['ano']);
    $pag_ini = (int) $_GET['pag_ini'];

    $progs = array();
    foreach ($pro as $prog2 => $key) {
        $sql="SELECT * FROM ".PROGRAMACAO." WHERE MID = '$prog2' AND STATUS = '1'";
        $tmp_prog=$dba[0] ->Execute($sql);
        $print=1;
        $thcor = 'cor1';
        $campo_prog=$tmp_prog->fields;
        $tipo=$campo_prog['TIPO'];
        $plano=$campo_prog['MID_PLANO'];
        $data_final=$ano;
        $prog_mid=(int)$campo_prog['MID'];

        if ($prog_mid != 0) {
            $tmp6=$dba[0] -> Execute("SELECT MID,DATA_PROG FROM ".ORDEM_PLANEJADO." WHERE MID_PROGRAMACAO = '$prog_mid' ORDER BY DATA_PROG DESC");
            $cao=$tmp6->fields;
            if ($cao['DATA_PROG'] == ""){
                $data_inicial=$campo_prog['DATA_INICIAL'];
            }
            else {
                $data_inicial=$cao['DATA_PROG'];
            }

            echo "<table id=\"lt_tabela\" class=\"tabela\"><tr><td colspan=\"3\" align=\"left\">\n";

            if (($tipo == 1) || ($tipo == 3)) {
                echo "{$ling['PLANO_M']}: ".VoltaValor(PLANO_PADRAO,"DESCRICAO","MID",$plano,0)." <br />
                {$ling['OBJ_MANUTENCAO']}: ".VoltaValor(MAQUINAS,"DESCRICAO","MID",$campo_prog['MID_MAQUINA'],0)." <br />
                {$ling['HORA_INICIO_M']}: ".NossaData($campo_prog['DATA_INICIAL'])." <br />
                {$ling['data_ultima_os']}: ".NossaData($data_inicial)." <br />
                {$ling['DATA_FINAL_M']}: ".NossaData($ano);
            }
            if (($tipo == 2) || ($tipo == 5)) {
                echo "{$ling['ROTA_M']}: ".VoltaValor(PLANO_ROTAS,"DESCRICAO","MID",$plano,0)." <br>
                {$ling['data_ultima_os']}: " . NossaData($data_inicial) . " <br />
                {$ling['DATA_FINAL_M']}: ".NossaData($ano);
            }

            echo "</th></tr>\n";

            if (VoltaTime("00:00:00",NossaData($campo_prog['DATA_FINAL'])) >= VoltaTime("00:00:00",NossaData($ano)) and $pag_ini == 0){
                echo "<tr><td colspan=\"4\" style=\"color:red\"><h3>{$ling['ord_data_final_programada_maior_estabelecida']}</h3></td></tr>\n";
            }
            else {
                $progs[] = $prog_mid;
            }

            echo "</table>
            <br clear=\"all\">\n";
        }	//fim desta programação
    }

    if (count($progs) > 0) {
        // Rodando atualização paginada
        $pag = grava_programacao_data($data_inicial, $data_final, 0, $tipo, $plano, $progs, 1, 200, $pag_ini, false, true);

        if($pag !== TRUE) {
            $prog_url = "";
            foreach ($pro as $prog2 => $key) {
                $prog_url .= "&aprog[$prog2]=$key";
            }
            
            echo "<script>
            mostraCarregando (true);
            self.window.location = 'atualizar_programacao.php?ano=". NossaData($ano) . "&pag_tam=200&pag_ini=$pag$prog_url';
            </script>\n";
            exit();
        }
        else {
            echo "<br /><br /><h3>{$ling['PROG_ATUALI_COM_SUCESSO']}</h3>\n";
            echo "<script>
            mostraCarregando (false);
            </script>\n";
        }
    }

    echo "</div>";
}
?>
