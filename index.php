<?php

// encerra alguma sess�o anterior
session_start();
unset($_SESSION['ManuSess']);
session_destroy();

// Configura��es
require 'conf/manusis.conf.php';
require 'lib/idiomas/portuguesbr.php';

if ((isset($manusis['infoclick_ip_acesso'])) and (count($manusis['infoclick_ip_acesso']) == 0 or in_array($_SERVER['REMOTE_ADDR'], $manusis['infoclick_ip_acesso']))) {
	header ("Location: infoclick.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- saved from url=(0035)http://vendas.wolk.com.br/produsis/ -->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Wert - ManuSis</title>
        <link rel="shortcut icon" href="<?=$manusis['url'];?>imagens/wolk_icone.gif" type="image/x-icon">
        <!-- set viewport to native resolution (android) - disable zoom !-->
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
        
            <link rel="stylesheet" type="text/css" href="<?=$manusis['url'];?>temas/<?=$manusis['tema'];?>/normalize.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?=$manusis['url'];?>temas/<?=$manusis['tema'];?>/md-screen.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?=$manusis['url'];?>temas/<?=$manusis['tema'];?>/sm-screen.css" media="screen" />
            
        <script type="text/javascript" src="<?=$manusis['url'];?>lib/functions_login.js"></script>
        
        <style>
            @font-face {
                font-family: 'Ubuntu-regular';
                    src:  url('<?=$manusis['url']?>temas/<?=$manusis['tema']?>/Ubuntu-Regular.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3?5 */
            }
            @font-face {
                font-family: 'Ubuntu-bold';
                    src:  url('<?=$manusis['url']?>temas/<?=$manusis['tema']?>/Ubuntu-Bold.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3?5 */
              }
        </style>
    </head>
    
    <body onload="setFocusUser();">
        <div class="border-top-body"></div>

        <center>
            <form id="form" method="post" action="manusis.php" onsubmit="return validate()" autocomplete="off">
                <table class="container" border="0">
                    <tbody><tr>
                        <td colspan="2">
                            <img class="logo" src="<?=$manusis['url'];?>imagens/logo_manusis.png">
                        </td>
                    </tr>
                    
                    <tr>
                        <td><img class="input-icon" src="<?=$manusis['url'];?>imagens/User-gray.jpg"></td>
                        <td>
                            <input name="usuario" type="text" size="50" id="user" onclick="setDisplayDiv();" autofocus="autofocus" placeholder="USU&Aacute;RIO" value="" />
                        </td>
                    </tr>

                    <tr>
                        <td><img class="input-icon" src="<?=$manusis['url'];?>imagens/Lock-gray.jpg"></td>
                        <td>
                            <input name="senha" type="password" size="50" id="password" onclick="setDisplayDiv();" placeholder="SENHA" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input class="form-input-button" type="submit" name="logar" value="<?="Entrar"?>" /> 
                        </td>
                    </tr>

<!--                    <tr>
                        <td colspan="2" class="help-box">
                            <a href="http://vendas.wolk.com.br/produsis/resetpassword.php" class="text-style"><b>Precisa de ajuda?</b></a>
                            <br>
                        </td>
                    </tr>-->

                    <tr>
                        <td colspan="2">
                    	</td>
                    </tr>

                </tbody></table>
            </form>
        </center>
<?php
    // APONTAMENTO SIMPLIFICADO
    // Verifica o acesso ao apontamento simplificado
    if ((isset($manusis['acesso_os_simplificado'])) and (count($manusis['acesso_os_simplificado']) == 0 or in_array($_SERVER['REMOTE_ADDR'], $manusis['acesso_os_simplificado']))) {
        echo '<br/ ><br/ ><center><a href="apontaosplan_sp.php" style="font-size: 18px; margin-top:25px"/><img src="imagens/icones/22x22/apontaos.png" align="top" border="0" alt="' . $ling['apont_simp_titulo'] . '" />' . $ling['apont_simp_titulo'] . '</center></a>';
    }

?>
        <br>
        <div class="footer-container" style="position: fixed; bottom: 0px; width: 100%; left: 0px;">
            <img class="footer-logo" src="<?=$manusis['url'];?>imagens/footer-logo.png">
        </div>

        <script>
            checkUserAgent();
            stickyFooter();
            window.onresize = function(event) {
                mobileFooterAdjust();
            };
        </script>
    
</body></html>
