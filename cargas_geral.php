<?


// Fun��o do Sistema
if (!require ("lib/mfuncoes.php"))
die("".$ling['arq_estrutura_nao_pode_ser_carregado']."");
// Configura��es
elseif (!require ("conf/manusis.conf.php")) die("".$ling['arq_configuracao_nao_pode_ser_carregado']."");
// Idioma
elseif (!require ("lib/idiomas/" . $manusis['idioma'][0] . ".php")) die("".$ling['arq_idioma_nao_pode_ser_carregado']."");
// Biblioteca de abstra��o de dados
elseif (!require ("lib/adodb/adodb.inc.php")) die($ling['bd01']);
// Informa��es do banco de dados
elseif (!require ("lib/bd.php")) die($ling['bd01']);
elseif (!require ("lib/delcascata.php")) die($ling['bd01']);
elseif (!require ("lib/autent.php")) die($ling['bd01']);

// Fazendo carga de Programa��o
$_SESSION[ManuSess]['VoltaValor'] = 1;

/**
 * @author Felipe Matos Malinoski <felipe@manusis.com.br>
 *
 *
 * Cria��o de codigos de forma generica.
 *
 *
 *
 */
function criaCod($desc, $campoCod, $numDec = 0, $tabela = "", $tabPai = "", $midPai = 0, $codPai = "", $separador = ""){
    global $dba, $tdb, $ling, $manusis;
    // Coisas importantes
    $vog   = array('A','E','I','O','U','�','�','�','�','�','�','�');
    $entre = array("DE ", "DO ", "DA ", "COM ", "/ ", " - ");
    // Tabela principal, AREAS por exemplo
    if(($desc != "") and ($campoCod != "")) {
        // Pegando a primeira letra
        $codAlfa = substr($desc, 0, 1);
        // Retirando a primeira letra
        $desc = trim(substr($desc, 1));
        //echo $desc . " - ";
        // Retirando as liga&ccedil;&eth;es
        $desc_limp = str_replace($entre, "", $desc);
        //echo $desc_limp . " - ";
        // Retirando os espaços em branco
        $desc_sep = explode(" ", $desc_limp);

        // Parte alfa do codigo
        $codAlfa .= substr(str_replace($vog, "", $desc_sep[0]), 0, 1);
        if (count($desc_sep) > 1){
            $codAlfa .= substr($desc_sep[1], 0, 1);
        }
        else {
            $codAlfa .= substr(str_replace($vog, "", $desc_sep[0]), 1, 1);
        }
        // Sequencia numerica
        if($numDec != 0){
            // buscando por mais ocorrencias do mesmo
            $sql = "SELECT $campoCod FROM $tabela WHERE $campoCod LIKE '$codAlfa%' ORDER BY $campoCod DESC";
            if(! $rs = $dba[0]->SelectLimit($sql,1,0)){
                erromsg("{$ling['err20']} <br />" . $dba[0]->ErrorMsg() . "<br />" . $sql);
                return "";
            }
            // Salvando o numero
            $codNum = str_replace($codAlfa, "", $rs->fields[$campoCod]);
            $codNum = sprintf("%0{$numDec}d", $codNum + 1);
            $codAlfa .= $codNum;
        }

        return $codAlfa;

    }
    elseif(($tabPai != "") and ($midPai != 0) and ($codPai != "")) {
        $codPai = VoltaValor($tabPai, $codPai, "MID", $midPai, 0);

        // buscando por mais ocorrencias do mesmo
        $sql = "SELECT $campoCod FROM $tabela WHERE $campoCod LIKE '$codPai%' ORDER BY $campoCod DESC";

        if(! $rs = $dba[0]->SelectLimit($sql,1,0)){
            erromsg("{$ling['err20']} <br />" . $dba[0]->ErrorMsg() . "<br />" . $sql);
            return "";
        }

        // Salvando o numero
        $codNum = (int) str_replace($codPai . $separador, "", $rs->fields[$campoCod]);
        $codNum = sprintf("%0{$numDec}d", $codNum + 1);

        return $codPai . $separador . $codNum;
    }
    return false;
}

function filtrar($linha, $tb, $campos, $data = "", $hora = "", $auto_tag) {
    global $dba, $tdb, $ling, $manusis;

    $data = (is_array($data))? $data : array();
    $hora = (is_array($hora))? $hora : array();
    $erro = 0;
    $sqlData = array();
    foreach ($campos as $k => $f) {

        if($f == $auto_tag[0]){
            $rtb = VoltaRelacao($tb, $auto_tag[2]);
            if($rtb != ""){
                $desc = $linha[array_search($auto_tag[2], $campos)];
                if($rtb['cod']) {
                    $codPai = $rtb['cod'];
                    $midPai = (int) VoltaValor($rtb['tb'], $rtb['mid'], $rtb['cod'], LimpaTexto(trim($desc)), $rtb['dba']);
                }
                else {
                    $codPai = $rtb['campo'];
                    $midPai = (int) VoltaValor($rtb['tb'], $rtb['mid'], $rtb['campo'], LimpaTexto(trim($desc)), $rtb['dba']);
                }

                $linha[$k] = criaCod("", $f, $auto_tag[1], $tb, $rtb['tb'], $midPai, $codPai);
                if (! $linha[$k]){
                    echo "$desc<BR />";
                }
            }
            else {
                // valor da descricao
                if(! is_array($auto_tag[2])){
                    $desc = $linha[array_search($auto_tag[2], $campos)];
                    $linha[$k] = criaCod($desc, $f, $auto_tag[1]);
                }
                else {
                    $desc = $linha[1] . $linha[2] . $linha[3] . $linha[4];
                    $linha[$k] = numeraCod($desc, $f, $auto_tag[1], $tb);
                }
                
            }
        }
        
        $rtb = VoltaRelacao($tb, $f);
        if($rtb != ""){
            if($rtb['cod']) {
                $m = (int) VoltaValor($rtb['tb'], $rtb['mid'], $rtb['cod'], LimpaTexto(trim($linha[$k])), $rtb['dba']);
            }
            else {
                $m = (int) VoltaValor($rtb['tb'], $rtb['mid'], $rtb['campo'], LimpaTexto(trim($linha[$k])), $rtb['dba']);
            }

            if (($m == 0) && ($linha[$k] != "")) {
                $m= GeraMid($rtb['tb'], $rtb['mid'], 0);
                if ($rtb['cod']) {
                    $sql = "INSERT INTO " . $rtb['tb'] . " (MID, " . $rtb['cod'] . ", " . $rtb['campo'] . ") VALUES ('" . $m . "', '" . LimpaTexto(trim($linha[$k])) . "', {$ling['criado_dinamicamente']})";
                }
                else {
                    $sql = "INSERT INTO " . $rtb['tb'] . " (MID, " . $rtb['campo'] . ") VALUES ('" . $m . "', '" . LimpaTexto(trim($linha[$k])) . "')";
                }
                if (!$dba[0]->Execute($sql)) {
                    erromsg("{$ling['arquivo']}: cargas_geral.php {$ling['linha']}: ".__LINE__." <br />" . $dba[0]->ErrorMsg() . "<br />" . $sql);
                    exit ();
                }
            }
            if ($m != 0) {
                $linha_bkp[$k]= $linha[$k];
                $linha[$k]= $m;
            }
        }

        // Atribuindo o valor
        if(($f != "") && ($linha[$k] != "")){
            // ORACLE
            if(in_array($f, $data) !== false){
                $sqlData[$f]= "to_date('" . LimpaTexto(trim($linha[$k])) . "', 'yyyy')";
            }
            elseif(in_array($f, $hora) !== false){
                $sqlData[$f]= "to_date('" . LimpaTexto(trim($linha[$k])) . "', 'HH24:MI:SS')";
            }
            elseif ((is_numeric(LimpaTexto(trim($linha[$k]))) !== false) AND ($f != "NUMERO"))  {
                $sqlData[$f]= LimpaTexto(trim($linha[$k]));
            }
            else {
                $sqlData[$f]= "'" . LimpaTexto(trim($linha[$k])) . "'";
            }
        }
        elseif($linha[$k] != "") {
            $erro = 1;
        }
    }

    return $sqlData;
}



if ($_POST['enviar'] != "") {
    $campos_tmp = explode(",", $_POST['campos']);
    // Alto Tag
    $auto_pai = explode(",", $_POST['cod_pai']);
    $auto_pai = (count($auto_pai) > 1)? $auto_pai : (count($auto_pai) == 1)? $auto_pai[0] : "DESCRICAO";
    
    $auto_tag = array (
    $_POST['cod_campo'],
    $_POST['cod_dec'],
    $auto_pai,
    $_POST['cod_sep']
    );
    // Oracle
    $camposD_tmp = explode(",", $_POST['campos_data']);
    $camposH_tmp = explode(",", $_POST['campos_hora']);
    $tabela     = strtolower($_POST['tabela']);
    $erro       = 0;

    // Verifica se a tabela existe
    if(isset($tdb[$tabela])){
        // Verifica se foram passados campos a serem salvos
        if(count($campos_tmp) > 0){
            $campos = array();
            foreach($campos_tmp as $c){
                $c = trim($c);
                $cc = LimpaTexto(strtoupper($c));

                if((isset($tdb[$tabela][trim($cc)])) || (trim($cc) == "MID")){
                    $campos[] = trim($cc);
                }
                else {
                    erromsg("{$ling['campo_indicado_n_existe_tab']}: $cc - $tabela.");
                    $erro = 1;
                }
            }

            foreach($camposD_tmp as $c) {
                $camposD[] = LimpaTexto(strtoupper(trim($c)));
            }
            foreach($camposH_tmp as $c) {
                $camposH[] = LimpaTexto(strtoupper(trim($c)));
            }
            if (is_uploaded_file($_FILES['csv']['tmp_name']) && $erro == 0) {
                // Recupera arquivo
                $handle= fopen($_FILES['csv']['tmp_name'], "r");

                // Montando o array com os dados do arquivo
                $sqlData= array ();
                while ($linha= fgetcsv($handle, 1000, ";")) {
                    $sqlData[]= filtrar($linha, $tabela, $campos, $camposD, $camposH, $auto_tag);
                }


                // Inserindo no banco de dados
                $i= 0;
                foreach ($sqlData as $data) {
                    $mid= GeraMid($tabela, "MID", 0);
                    $tmp_mid = 0;
                    // Verifica se o cadastro ja existe
                    if ($data['MID'] != "") {
                        $tmp_mid = (int) str_replace("'", "", $data['MID']);
                        $mid = (int) str_replace("'", "", $data['MID']);
                        unset($data['MID']);
                    }
                    elseif ($data['COD'] != "") {
                        $tmp_mid = (int) VoltaValor($tabela, "MID", "COD", str_replace("'", "", $data['COD']), 0);
                        if ($tmp_mid != 0){
                            $mid = $tmp_mid;
                        }
                    }
                    elseif ($data['TAG'] != "") {
                        $tmp_mid = (int) VoltaValor($tabela, "MID", "TAG", str_replace("'", "", $data['TAG']), 0);
                        if ($tmp_mid != 0){
                            $mid = $tmp_mid;
                        }
                    }
                    elseif ($data['DESCRICAO'] != "") {
                        $tmp_mid = (int) VoltaValor($tabela, "MID", "DESCRICAO", str_replace("'", "", $data['DESCRICAO']), 0);
                        if ($tmp_mid != 0){
                            $mid = $tmp_mid;
                        }
                    }
                    elseif ($data['NUMERO'] != "") {
                        $tmp_mid = (int) VoltaValor($tabela, "MID", array("NUMERO", "ATIVO"), array(str_replace("'", "", $data['NUMERO']), str_replace("'", "", $data['ATIVO'])), 0);
                        if ($tmp_mid != 0){
                            $mid = $tmp_mid;
                        }
                    }


                    $sql = "";
                    // N�o encontrou nenhum cadastro
                    if ($tmp_mid == 0){
                        $sql= "INSERT INTO " . $tabela . " (MID, " . implode(", ", array_keys($data)) . ") VALUES ('{$mid}', " . implode(", ", array_values($data)) . ")";
                        $logTipo = 3;
                        echo " .";
                    }
                    else {
                        $cc = "";
                        foreach ($data as $k => $v) {
                            $cc .= ($cc != "")? ", " : "";
                            $cc .= "$k = $v";
                        }
                        $sql = "UPDATE " . $tabela . " SET " . $cc . " WHERE MID = $mid";
                        $logTipo = 4;
                        echo " #";
                    }

                    if (!$dba[0]->Execute($sql)) {
                        erromsg("{$ling['arquivo']}: cargas_geral.php {$ling['linha']}: ".__LINE__." <br />" . $dba[0]->ErrorMsg() . "<br />{$ling['registro']} $i <br />SQL: " . $sql);
                        exit ();
                    }
                    else {
                        logar($logTipo, "FEITO EM CARGA:", $tabela, "MID", $mid);
                        $i++;
                    }
                }

                echo "<h3>{$ling['total']}: $i</h3>";
            }
            elseif($erro == 0) {
                erromsg("".$ling['erro_upload_arq']."<br />".$ling['tente_novamente']."");
            }
        }
        else {
            erromsg("".$ling['indiq_camp_serem_salvo_usando_virgula']."");
        }
    }
    else {
        erromsg("".$ling['tabela_ind_nao_existe']."");
    }
}
?>
<html>
    <head>
        <title>Cargas </title>
    </head>
    <body>
        <h1>Cargas </h1>
        <form action="cargas_geral.php" method="POST" enctype="multipart/form-data">
            <label for="tabela">Tabela:</label>
            <select id="tabela" class="campo_select" name="tabela" />
                <?php
                foreach($tdb as $k => $tab) {
                    echo "<option value=\"$k\">" . $tab['DESC'] . "</option>";
                }
                ?>
            </select> 

            <br clear="all" />
            <label for="campos">Campos:</label>
            <input id="campos" class="campo_text" type="text" name="campos" size="65" value="<?php echo $_POST['campos'] ?>"/>
            <br clear="all" />
            <label for="file">Arquivo .csv:</label>
            <input id="csv" class="campo_text" type="file" name="csv" size="40"/>
            <fieldset><legend>Auto C&oacute;digo</legend>
                <label for="cod_campo">Campo Cod:</label>
                <input id="cod_campo" class="campo_text" type="text" name="cod_campo" size="65" value="<?php echo $_POST['cod_campo'] ?>"/>
                <br clear="all" />
                <label for="cod_dec">Casas decimais:</label>
                <input id="cod_dec" class="campo_text" type="text" name="cod_dec" size="65" value="<?php echo $_POST['cod_dec'] ?>"/>
                <br clear="all" />
                <label for="cod_pai">Pai:</label>
                <input id="cod_pai" class="campo_text" type="text" name="cod_pai" size="65" value="<?php echo ($_POST['cod_pai'] == "")? "DESCRICAO" : $_POST['cod_pai']; ?>"/>
                <br clear="all" />
                <label for="cod_sep">Separador:</label>
                <input id="cod_sep" class="campo_text" type="text" name="cod_sep" size="65" value=""/>
                <br clear="all" />
            </fieldset>
            <fieldset><legend>Oracle</legend>
                <label for="campos_data">Campos data:</label>
                <input id="campos_data" class="campo_text" type="text" name="campos_data" size="65" value="<?php echo $_POST['campos_data'] ?>"/>
                <br clear="all" />
                <label for="campos_hora">Campos hora:</label>
                <input id="campos_hora" class="campo_text" type="text" name="campos_hora" size="65" value="<?php echo $_POST['campos_hora'] ?>"/>
                <br clear="all" />
            </fieldset>
            <br/>
            <br/>
            <input class="botao" type="submit" id="enviar" name="enviar" value="Iniciar carga"/>
        </form>

    </body>
</html>
