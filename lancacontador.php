<?
/***
* Manusis 3.0
* Autor: Fernando Cosentino <reverendovela@yahoo.com.br>
* Nota: Lanca contador
*/
// Fun&ccedil;&otilde;es do Sistema
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura&ccedil;&otilde;es
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstra&ccedil;&atilde;o de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa&ccedil;&otilde;es do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
// Formul&aacute;rios
elseif (!require("lib/forms.php")) die ($ling['bd01']);
// Autentifica&ccedil;&atilde;o
elseif (!require("lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require("conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n&atilde;o exista um padr&atilde;o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";

//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}


$id = (int) $_GET['id'];
$op = (int) $_GET['op'];

echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";
echo "</head>
<body>
<div id=\"central_relatorio\">
<div id=\"cab_relatorio\">
<h1>".$ling['lanca_contadores']."</h1>
</div>
<div id=\"corpo_relatorio\">
<form action=\"lancacontador.php\" name=\"form_contador\" id=\"form_contador\" method=\"GET\">
<div id=\"obj\">";

if (VoltaPermissao($id, $op) != 3) {
    $maqs = array();
    $tmp = $dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute("SELECT MID_MAQUINA FROM ".MAQUINAS_CONTADOR."");
    while(! $tmp->EOF) {
        $maqs[] = $tmp->fields['MID_MAQUINA'];
        $tmp->MoveNext();
    }
    
    $where = (count($maqs) > 0)? "WHERE MID IN(" . implode(', ', $maqs) . ")" : "";
    
    echo "<label class=\"campo_label\" for=\"obj\">".$tdb[MAQUINAS]['DESC']."</label>";
    FormSelectD('COD', 'DESCRICAO', MAQUINAS, $_GET['obj'], 'obj', 'obj', 'MID', '', '', "atualiza_area2('contador','parametros.php?id=5&obj=' + this.options[this.selectedIndex].value)", $where);
    echo "<br clear=\"all\" />
    
	<div id=\"contador\">";
    
    if ($_GET['obj'] != '') {
        $obj=(int)$_GET['obj'];
        $tmp=$dba[$tdb[MAQUINAS_CONTADOR]['dba']] -> Execute("SELECT * FROM ".MAQUINAS_CONTADOR." WHERE MID_MAQUINA = '$obj' ORDER BY DESCRICAO ASC");
        
        echo "<label class=\"campo_label\" for=\"cont\">".$tdb[MAQUINAS_CONTADOR]['DESC']."</label>";
        echo " <select name=\"cont\" id=\"cont\" class=\"campo_select\">";
        echo "<option value=\"0\">".$ling['select_opcao']."</option>";
        
        while (! $tmp->EOF) {
            $campo=$tmp->fields;
            if ($_GET['cont'] == $campo['MID']) echo "<option value=\"".$campo['MID']."\" selected=\"selected\">".strtoupper(htmlentities($campo['DESCRICAO']))."</option>";
            else echo "<option value=\"".$campo['MID']."\">".strtoupper(htmlentities($campo['DESCRICAO']))."</option>";
            $tmp->MoveNext();
        }
        
        echo "</select>
		<input type=\"button\" class=\"botao\" name=\"botao\" value=\"{$ling['selecionar']}\" onclick=\"atualiza_area2('detalhes','parametros.php?id=6&cont=' + document.getElementById('cont').options[document.getElementById('cont').selectedIndex].value)\" />";
    }
    
    echo "</div>
	<div id=\"detalhes\"> </div>";
}
else {
	erromsg(htmlentities("".$ling['sem_permissao'].""));
}

?>
