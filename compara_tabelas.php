<?php
// Funções de Estrutura
if (!require("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configurações
elseif (!require("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstração de dados
elseif (!require("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informações do banco de dados
elseif (!require("lib/bd.php")) die ($ling['bd01']);
elseif (!require("lib/autent.php")) die ($ling['autent01']);

// Comparativo entre as tabelas
$sql = "SHOW TABLES";
// Modelo
$rs_v3 = $dba[0]->Execute($sql);
$tab_v3_tmp = $rs_v3->getrows();
$tab_v3 = array();
foreach ($tab_v3_tmp as $k => $v) {
    $tab_v3[] = $v['Tables_in_'. $manusis['db'][0]['base']];
}


// Conectando com outra base de dados
$base_dest = $_GET['base_dest'];
$base = "manusis_" . $_GET['base_dest'];
$dba[1] = ADONewConnection($manusis['db'][0]['driver']);
$dba[1]->SetFetchMode(ADODB_FETCH_ASSOC);

if (! $dba[1]->Connect($manusis['db'][0]['host'], "manusis_manusis", $manusis['db'][0]['senha'], $base)){
    errofatal($ling['bd02']);
}

// Cliente
$rs_dest = $dba[1]->Execute($sql);
$tab_dest_tmp = $rs_dest->getrows();
$tab_dest = array();
foreach ($tab_dest_tmp as $k => $v) {
    $tab_dest[] = $v['Tables_in_'. $base];
}

echo "<table border=1><tr>
<td>".$ling['tab_v3_tem']."</td><td>{$ling['tab_tem']} $base_dest {$ling['tem']}</td></tr>
<tr><td>";
foreach ($tab_v3 as $etab) {
	if (!in_array($etab,$tab_dest)) echo "$etab<br>";
}
echo "</td><td>";
foreach ($tab_dest as $etab) {
	if (!in_array($etab,$tab_v3)) echo "$etab<br>";
}
echo "</td></tr></table>";

/*if ($_POST['env_form']) {
    $altera = $_POST['altera'];
    foreach ($tab_v3 as $tab => $cols) {
        foreach ($cols as $col => $def) {
            if (($tab_dest[$tab][$col] != $def) and ($altera[$tab][$col] == 1)) {
                $default = ($def['Default'] != "")? "DEFAULT '{$def['Default']}'" :"";
                
                if ($def['Null'] == "NO"){
                    $notNull = "NOT NULL";
                }
                else {
                    $notNull = "NULL";
                }
                if ($tab_dest[$tab][$col]) {
                	$sql = "ALTER TABLE $tab MODIFY $col {$def['Type']} $notNull $default";
                }
                else {
                    $sql = "ALTER TABLE $tab ADD $col {$def['Type']} $notNull $default";
                }

                if(! $dba[1]->Execute($sql)){
                    erromsg($dba[1]->ErrorMsg() . "<br />" . $sql);
                }
                else {
                    $tab_dest[$tab][$col] = $def;
                }
            }
        }
    }
}

echo "<script>";
echo "function seleciona_tudo() {
	id=document.getElementsByTagName('INPUT');
	idm=document.getElementById('total');
	for(var i=0; id[i] != 0; i++) {
		if(id[i].type == 'checkbox'){
			id[i].checked = idm.checked;
		}
	}
}";
echo "</script>";
echo "<h1>Diferenças encontradas</h1>";
echo "<form method=\"POST\" action=\"atualiza_v3.php?base_dest=$base_dest\">";
echo "<input type=\"submit\" value=\"APLICAR ALTERAÇÕES\" name=\"env_form\" />";
echo "<br clear=\"all\" />";
echo "<br clear=\"all\" />";
echo "<table border=\"1\" width=\"100%\">";
echo "<tr><td><input type=\"checkbox\" id=\"total\" name=\"total\" onclick=\"seleciona_tudo()\" /></td><td>Tabela</td><td>Coluna</td><td>Definição V3</td><td>Definição Atual</td></tr>";

foreach ($tab_v3 as $tab => $cols) {
    foreach ($cols as $col => $def) {
        if ($tab_dest[$tab][$col] != $def) {
            echo "<tr>
            <td><input type=\"checkbox\" id=\"altera[$tab][$col]\" name=\"altera[$tab][$col]\" value=\"1\" /></td>
			<td>{$tab}</td>
			<td>{$col}</td>
			<td>" . @implode("<br />", $def) . "</td>
			<td>" . @implode("<br />", $tab_dest[$tab][$col]) . "</td>
			</tr>";
        }
    }
}

echo "</table>";
echo "<br clear=\"all\" />";
echo "<br clear=\"all\" />";
echo "<input type=\"submit\" value=\"APLICAR ALTERAÇÕES\" name=\"env_form\" />";
echo "</form>";*/
?>
