<?
/**
 * Manusis 3.0
 * Autor: Mauricio Blackout <blackout@firstidea.com.br>
 * Nota: Relatorio
 */
// Fun��es do Sistema
if (!require_once("lib/mfuncoes.php")) die ($ling['arq_estrutura_nao_pode_ser_carregado']);
// Configura��es
elseif (!require_once("conf/manusis.conf.php")) die ($ling['arq_configuracao_nao_pode_ser_carregado']);
// Idioma
elseif (!require_once("lib/idiomas/".$manusis['idioma'][0].".php")) die ($ling['arq_idioma_nao_pode_ser_carregado']);
// Biblioteca de abstro de dados
elseif (!require_once("lib/adodb/adodb.inc.php")) die ($ling['bd01']);
// Informa��es do banco de dados
elseif (!require_once("lib/bd.php")) die ($ling['bd01']);
// Formul�rios
elseif (!require_once("lib/forms.php")) die ($ling['bd01']);
// Autentifica��o
elseif (!require_once("lib/autent.php")) die ($ling['autent01']);
// Modulos
elseif (!require_once("conf/manusis.mod.php")) die ($ling['mod01']);

// Caso n�o exista um padr�o definido
if (!file_exists("temas/".$manusis['tema']."/estilo.css")) $manusis['tema']="padrao";
// Montando XML do Arquivo
//Header("Content-Type: application/xhtml+xml");
$Navegador = array (
"MSIE",
"OPERA",
"MOZILLA",
"NETSCAPE",
"FIREFOX",
"SAFARI"
);
$info[browser] = "OTHER";
foreach ($Navegador as $parent) {
    $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
    $f = $s + strlen($parent);
    $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 5);
    $version = preg_replace('/[^0-9,.]/','',$version);
    if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent)) {
        $tmp_navegador[browser] = $parent;
        $tmp_navegador[version] = $version;
    }
}
$id=LimpaTexto($_GET['id']);

if(isset($_GET['idTabela']))
{
   $id=LimpaTexto( mb_strtolower($_GET['idTabela']));
}

$datai = date("d/m/Y");

// Encontrando a semana
if((int)$_GET['semini'] == 0) {
    $mki = VoltaTime("00:00:00", NossaData($datai));
    $semini = date('W', $mki);
}
else {
    $semini = (int)$_GET['semini'];
}

// Encontrando o ano
if((int)$_GET['anoini'] == 0) {
    $mki = VoltaTime("00:00:00", NossaData($datai));
    $anoini = date('o', $mki);
}
else {
    $anoini = (int)$_GET['anoini'];
}

// Primeiro dia da semana
$mki = VoltaTime("00:00:00", NossaData($datai));
$dia_sem_ini = date('N', $mki);
$dias_menos = (1 - $dia_sem_ini) * -1;
$mki = $mki - ($mktime_1_dia * $dias_menos);

// Semana anterior
if ($semini == 1) {
    $mk_tmp = mktime(0, 0, 0, 12, 31, $anoini - 1);
    $sem_ant = date('W', $mk_tmp);
    $ano_ant = $anoini - 1;
}
else {
    $sem_ant = $semini - 1;
    $ano_ant = $anoini;
}


if($semini < 10)
{
    if(strlen($semini) < 2)
    $semini = '0'.$semini;
}

$file = 'logs/log_'.$anoini.$semini.'.csv';

$rows = 0;

$break = 8;

if(file_exists($file))
{
    $tabela = true;
    $cor = 'cor2';
    $rows = 0;
    $txt = '';
    if (($handle = fopen($file, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
        {
            if($id)
            {
                $tabela = ($data[6] == strtoupper( unhtmlentities($tdb[$id]['DESC'])));
            }
            else
            {
                $tabela = true;
            }
                
            if($tabela)
            {
                $cor = ($cor == 'cor1')? 'cor2' : 'cor1';
                $txt .= "<tr class=\"$cor\">";

                foreach($data as $key => $csv)
                {
                    if($key == $break)break;

                    
               
                    $txt .= "<td align=\"left\" valign=\"top\">". nl2br( htmlentities($data[$key]))."</td>";
                }
                $rows++;
                $txt .= "</tr>";
            }
            
        }
        fclose($handle);
    }
}

if($rows > 0)
{
    $download = "<span onmouseout=\"menu_fora('ltmenu')\" onmouseover=\"menu_sobre('ltmenu')\" id=\"lt_menu\">{$ling['opcoes']}
<ul id=\"m_ltmenu\" style=\"visibility: hidden;\">
<li><a href=\"{$file}\"><img align=\"middle\" src=\"imagens/icones/22x22/xls.png\" border=\"0\" />&nbsp;{$ling['log_download_comp']}</a></li>
</ul></span>";
}


echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"".$ling['xml']."\">
<head>
 <meta http-equiv=\"pragma\" content=\"no-cache\" />
<title>{$ling['manusis']}</title>
<link href=\"temas/".$manusis['tema']."/estilo.css\" rel=\"stylesheet\" type=\"text/css\"  title=\"{$ling['manusis_padrao']}\" />
<script type=\"text/javascript\" src=\"lib/javascript.js\"> </script>\n";
if ($tmp_navegador['browser'] == "MSIE") echo "<script type=\"text/javascript\" src=\"lib/movediv.js\"> </script>\n";

echo "<script>

 if(window.opener)
 {
    window.resizeTo(680, 500);
 }

</script>";


echo "</head>
<body>";
echo "<br />
<div id=\"lt\">
<div id=\"lt_cab\">";
if(!isset($_GET['idTabela'])){
    echo "<h3>{$ling['log_ultimas_alteracao']}: {$tdb[$id]['DESC']}</h3>{$download}";
}
else{
    echo "<h3>{$tdb[LOGS]['DESC']}</h3>{$download}";
}
echo "
</div>
</div>
\n";




$tabela = array();

foreach($tdb as $key => $tbl)
{
    $tabela[$key] = $tbl['DESC'];
}

asort($tabela);


echo "<div id=\"lt_forms\"><fieldset>";
echo "<legend>{$ling['localizar']}</legend>";

echo "<form action=\"\" method=\"get\">";

echo "<label for=\"id\" class=\"campo_label\">{$ling['tabela']}:</label>\n";

echo "<select name=\"idTabela\" id=\"idTabela\" class=\"campo_select\">";
foreach($tabela as $key => $tbl)
{
    echo "<option value=\"{$key}\"".(($tdb[$id]['DESC'] == $tbl)?'selected="selected"':'').">{$tbl}</option>";
}
echo "</select>&nbsp;&nbsp;<input value=\"ok\" type=\"button\" class=\"botao\" onclick=\"javacript:document.getElementById('ir').click();\">";
echo "</form>";
echo "</fieldset><br clear=\"all\" />";

echo "<div style=\"text-align:center;\">";

echo "<a href=\"javascript: window.location = '?idTabela='+ document.getElementById('idTabela').value + '&op=$op&modo_visao=$modo_visao&anoini=$ano_ant&semini=$sem_ant'\" title=\"{$ling['ord_ant_sem']}\"><img src=\"imagens/icones/22x22/voltar.png\" border=\"0\" align=\"top\" /></a>&nbsp;&nbsp;&nbsp;\n";
echo "<label for=\"modo_visao\" class=\"campo_label\">{$ling['semana_desc']}:</label>\n";
echo "<input type=\"text\" id=\"semini\" name=\"semini\" size=\"1\" class=\"campo_text_ob\" value=\"$semini\" /> / \n";
echo "<input type=\"text\" id=\"anoini\" name=\"anoini\" size=\"2\" class=\"campo_text_ob\" value=\"$anoini\" />\n";

echo "<input type=\"submit\" id=\"ir\" name=\"ir\" class=\"botao\" value=\"{$ling['log_ir']}\" onclick=\"window.location = '?idTabela='+ document.getElementById('idTabela').value +'&op=$op&modo_visao=$modo_visao&anoini=' + document.getElementById('anoini').value + '&semini=' + document.getElementById('semini').value\" />\n";

// Semana anterior
if ($semini == 52) {
    $mk_tmp = mktime(0, 0, 0, 12, 31, $anoini);
    $sem_pro = date('W', $mk_tmp);
    if ($sem_pro == 53) {
        $sem_pro = $semini + 1;
        $ano_pro = $anoini;
    }
    else {
        $sem_pro = 1;
        $ano_pro = $anoini + 1;
    }
}
elseif ($semini == 53) {
    $sem_pro = 1;
    $ano_pro = $anoini + 1;
}
else {
    $sem_pro = $semini + 1;
    $ano_pro = $anoini;
}


echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript: window.location = '?idTabela='+ document.getElementById('idTabela').value +'&op=$op&modo_visao=$modo_visao&anoini=$ano_pro&semini=$sem_pro'\" title=\"{$ling['ord_prox_sem']}\"><img src=\"imagens/icones/22x22/avancar.png\" border=\"0\" align=\"top\" /></a>\n";

if($rows == 0)
{
    erromsg("{$ling['log_n_encontrado']}: {$semini} - {$anoini}");
}
echo "<br />
</div></div>";
echo "<div id=\"lt_tabela\">
<table widht=\"100%\" cellpadding=\"3\" cellspacing=\"1\">
<tr>
    <th>".$ling['permissao3']."</th>
    <th>".$ling['IP_']."</th>
    <th>".$ling['data']."</th>
    <th>".$ling['Hora_']."</th>
    <th>".$ling['tipo']."</th>
    <th>".$ling['Dados_']."</th>
    <th>".$ling['titulo_tabela']."</th>
</tr>";

echo $txt;

echo "</table></fiedset>
    </form><br />
    </body>
    </html>";
